// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target x86_64-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name AOFKit
@_exported import AOFKit
import Foundation
import Swift
import UIKit
import WebKit
import _Concurrency
extension UIKit.UIViewController {
  @_Concurrency.MainActor(unsafe) public func aof__showMessage(title: Swift.String, message: Swift.String)
  @_Concurrency.MainActor(unsafe) public func aof__showError(message: Swift.String)
  @_Concurrency.MainActor(unsafe) public func aof__showWarning(message: Swift.String)
  @_Concurrency.MainActor(unsafe) public func aof__showInformation(message: Swift.String)
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class AOFWebViewController : UIKit.UIViewController, WebKit.WKNavigationDelegate, WebKit.WKUIDelegate {
  @_Concurrency.MainActor(unsafe) open var showCancel: Swift.Bool
  @_Concurrency.MainActor(unsafe) public var wkWebView: WebKit.WKWebView
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) open func beginOAuthWithClient()
  @_Concurrency.MainActor(unsafe) @objc open func webView(_ webView: WebKit.WKWebView, decidePolicyFor navigationAction: WebKit.WKNavigationAction, decisionHandler: @escaping (WebKit.WKNavigationActionPolicy) -> Swift.Void)
  @_Concurrency.MainActor(unsafe) @objc open func webView(_ webView: WebKit.WKWebView, didFinish navigation: WebKit.WKNavigation!)
  @_Concurrency.MainActor(unsafe) @objc open func webView(_ webView: WebKit.WKWebView, didFailProvisionalNavigation navigation: WebKit.WKNavigation!, withError error: Swift.Error)
  @objc @_Concurrency.MainActor(unsafe) open func dismiss(cancelButton: UIKit.UIBarButtonItem)
  @objc @_Concurrency.MainActor(unsafe) open func dismissController()
  @objc @_Concurrency.MainActor(unsafe) open func dismissControllerOnly()
  @objc @_Concurrency.MainActor(unsafe) open func showErrorAlertWithCancel(message: Swift.String)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc public protocol AOFOAuthDelegate {
  @objc optional func willBegin()
  @objc optional func didBegin()
  @objc optional func cancel()
  @objc optional func success(withAccessToken accessToken: Swift.String?)
  @objc optional func success(withAccessToken accessToken: Swift.String?, with511Id user511Id: Swift.String?)
  @objc optional func successForUserDetails(withAccessToken accessToken: Swift.String?, withUserData data: Foundation.NSDictionary?)
  @objc optional func failedGettingUserDetails(withErrorMessage errorMessage: Swift.String?)
  @objc optional func failureFromAOFKit(error: Swift.Error?, msg: Swift.String?)
  @objc optional func didEnd()
  @objc optional func activityIndicator(isShow: Swift.Bool)
}
public struct AOFClientParams {
  public var clientID: Swift.String?
  public var clientPassword: Swift.String?
  public var tokenUrl: Swift.String?
  public var authorizeUrl: Swift.String?
  public var accessTokenUrl: Swift.String?
  public var callBackUrl: Swift.String?
  public var redirectUri: Swift.String?
  public var state: Swift.String?
  public var scope: Swift.String?
  public var grantType: Swift.String?
}
public enum AOFEnvironment : Swift.String {
  case dev
  case qa
  case prod
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
@objc @_inheritsConvenienceInitializers public class AOFManager : ObjectiveC.NSObject {
  public static var showErrorAlerts: Swift.Bool {
    get
  }
  public static var showWaitingHud: Swift.Bool {
    get
  }
  public static var currentEnvironment: AOFKit.AOFEnvironment {
    get
  }
  public static var oauthClient: AOFKit.AOFOAuthClient? {
    get
  }
  public static var user511Id: Swift.String? {
    get
  }
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withRequestTokenUrl tokenUrl: Swift.String, withAuthorizeUrl authorizeUrl: Swift.String, withAccessTokenUrl accessTokenUrl: Swift.String, withCallBackUrl callBackUrl: Swift.String, withRedirectUri redirectUri: Swift.String, withState state: Swift.String?, withScope scope: Swift.String?, withGrantType grantType: Swift.String?)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withRequestTokenUrl tokenUrl: Swift.String, withAuthorizeUrl authorizeUrl: Swift.String, withAccessTokenUrl accessTokenUrl: Swift.String, withCallBackUrl callBackUrl: Swift.String, withRedirectUri redirectUri: Swift.String, withState state: Swift.String?, withScope scope: Swift.String?, withGrantType grantType: Swift.String?)
  public class func handleShowErrorAlerts(handleShowErrorAlerts: Swift.Bool)
  public class func showWaitingHud(showWaitingHud: Swift.Bool)
  @objc override dynamic public init()
  @objc deinit
}
extension AOFKit.AOFManager {
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withRedirectUri redirectUri: Swift.String)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withCallBackUrl callBackUrl: Swift.String)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withCallBackUrl callBackUrl: Swift.String, withRedirectUri redirectUri: Swift.String)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withRedirectUri redirectUri: Swift.String, withScope scope: Swift.String?, withGrantType grantType: Swift.String?)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withRedirectUri redirectUri: Swift.String, withState state: Swift.String?, withScope scope: Swift.String?, withGrantType grantType: Swift.String?)
  public class func register(withClientID clientID: Swift.String, withClientPassword clientPassword: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withRequestTokenUrl tokenUrl: Swift.String, withAuthorizeUrl authorizeUrl: Swift.String, withAccessTokenUrl accessTokenUrl: Swift.String, withCallBackUrl callBackUrl: Swift.String, withRedirectUri redirectUri: Swift.String, withState state: Swift.String?, withScope scope: Swift.String?)
  public class func register(withClientID clientID: Swift.String, withEnvironment environment: AOFKit.AOFEnvironment, withDelegate delegate: AOFKit.AOFOAuthDelegate?, withScope scope: Swift.String, withRedirectUri redirectUri: Swift.String, withGrantType grantType: Swift.String)
  public class func authorize(withPresenter presenter: UIKit.UIViewController, completion: @escaping (_ presentedViewController: UIKit.UIViewController) -> Swift.Void?)
  @discardableResult
  public class func clearKeyChain() -> Swift.Bool
  public class func hasValidAccessToken() -> Swift.Bool
  public class func accessToken() -> Swift.String?
  public class func refreshToken() -> Swift.String?
}
public typealias HUDCompletedBlock = () -> ()
public enum HUDType {
  case loading
  case success
  case error
  case info
  case none
  public static func == (a: AOFKit.HUDType, b: AOFKit.HUDType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension AOFKit.AOFHUD {
  public class func show(_ type: AOFKit.HUDType, text: Swift.String, time: Foundation.TimeInterval? = nil, completion: AOFKit.HUDCompletedBlock? = nil)
  public class func dismiss()
}
@objc @_inheritsConvenienceInitializers open class AOFHUD : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
extension Swift.Array {
  public func toDictionary(withSeparator separator: Swift.String) -> [Swift.String : Swift.String]
}
extension Foundation.URL {
  public func params() -> [Swift.String : Swift.String]?
}
@objc public class AOFOAuthClient : ObjectiveC.NSObject {
  public var params: AOFKit.AOFClientParams? {
    get
  }
  public var delegate: AOFKit.AOFOAuthDelegate? {
    get
  }
  public init(withParams params: AOFKit.AOFClientParams, withDelegate delegate: AOFKit.AOFOAuthDelegate?)
  public class var hasValidAccessToken: Swift.Bool {
    get
  }
  public class var accessToken: Swift.String? {
    get
  }
  public class var refreshToken: Swift.String? {
    get
  }
  public func showORHideNetworkActivityIndicator(_ showORHide: Swift.Bool)
  public func refetchAccessToken()
  public func fetchAccessToken(withParams params: [Swift.String : Swift.String]?, success: @escaping (_ data: [Swift.String : Any]?) -> Swift.Void?, failure: @escaping (_ error: Swift.Error) -> Swift.Void?)
  public func fetchUserDetails()
  public func stringEncryption(stringToEncrypt: Swift.String) -> [Swift.UInt8]
  public func stringDecryption(encryptedValue: [Swift.UInt8]) -> Swift.String
  public func generateAndSaveDictionaryOfEncryptedData(jsonData: Foundation.NSDictionary)
  public func decryptDictionaryAndReturnDetails() -> (Foundation.NSDictionary)
  public func clearUserDetails()
  @objc deinit
}
extension AOFKit.AOFEnvironment : Swift.Equatable {}
extension AOFKit.AOFEnvironment : Swift.Hashable {}
extension AOFKit.AOFEnvironment : Swift.RawRepresentable {}
extension AOFKit.HUDType : Swift.Equatable {}
extension AOFKit.HUDType : Swift.Hashable {}
