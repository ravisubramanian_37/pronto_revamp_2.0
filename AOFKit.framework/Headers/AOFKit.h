//
//  AOFKit.h
//  AOFKit
//
//  Created by Prathap Dodla on 18/07/17.
//  Copyright © 2017 AbbVie. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AOFKit.
FOUNDATION_EXPORT double AOFKitVersionNumber;

//! Project version string for AOFKit.
FOUNDATION_EXPORT const unsigned char AOFKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AOFKit/PublicHeader.h>


