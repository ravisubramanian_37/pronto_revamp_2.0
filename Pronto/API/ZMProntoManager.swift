//
//  ZMProntoManager.swift
//  Pronto
//
//  Created by osmarogo on 1/6/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//

import Foundation
import MagicalRecord

@objc class ZMProntoManager : NSObject {
    @objc static let sharedInstance = ZMProntoManager()
    var selectedFranchiseBrands = [FranchiseAndBrands]()
    @objc var franchisesAndBrands = [AnyObject]()
    @objc var franchisesToUpdateHeader = [AnyObject]() // franchise Collection to update Header
    @objc var categoriesToFilter = [AnyObject]() // Collection of type Category to filter data into CoreDataDB
    @objc var selectedCategories = [AnyObject]() // Collection of type Category to populate data into Category Meny
    @objc var selectedCategoriesToUpdateHeader = [AnyObject]() //Collection of type Cateories to update selection into header
    @objc var sortBy = [AnyObject]() // Collection to handle sort filter into CoreData Database
    @objc var textToSearch = String() // Text to perform search into Coredata DataBAse
    @objc var assetsIdToFilter = [AnyObject]() // Collection of assetsId to filter data on MYBfriefcase view
    @objc var resetFranchiseIsSelected = false // Bool to indicate if franchises needs reset.
    
    @objc var assetArray = [AnyObject]() // Collection of Assets
    @objc var assetArrayIds = [AnyObject]() // Collection of Asset IDs
    @objc var favArrayIds = [AnyObject]() // Collection of Asset IDs

    /// Properties to do mapping into CoreData DB
    @objc var brandsArray = [AnyObject]() // Array of type Brand
    @objc var franchisesArray = [AnyObject]() // Array of type Franchise
    @objc var categoriesArray = [AnyObject]() // Array of type Categories
//    @objc var library = ZMLibrary() //Global library
    @objc var search = Search()
    
    @objc var currentLibraryView = MainView.library.rawValue {
        didSet{
            switch currentLibraryView {
            case 1:
                search.currentView = MainView.myBriefcase.rawValue
            default:
                search.currentView = MainView.library.rawValue
            }        
        }
    }
  
  /// Reset franchise and brands collection
    @objc func resetFranchiseAndBrandsCollection(){
        selectedFranchiseBrands = []
    }
  
  ///Get franchise and brands collection count
  /// - Returns: return total count
    @objc func getFranchiseAndBrandsCollectionCount() -> Int {
        return selectedFranchiseBrands.count
    }
  /**
   Reset all shared variables
   */
    @objc func resetManager(){        
        selectedFranchiseBrands = []
        franchisesAndBrands = []
        franchisesToUpdateHeader = []
        categoriesToFilter = []
        selectedCategories = []
        selectedCategoriesToUpdateHeader = []
        sortBy = []
        textToSearch = ""
        assetsIdToFilter = []
        resetFranchiseIsSelected = false
        brandsArray = []
        franchisesArray = []
        categoriesArray = []
//        library.resestFranchises = true
//        library.pageIndex = 0
        search.reset()
        currentLibraryView = MainView.library.rawValue
        assetArray = []
    }
    
  /**
   Setup coredata Stack
   */
    @objc func setupDB()
    {
//        MagicalRecord.shouldAutoCreateDefaultPersistentStoreCoordinator();
//        MagicalRecord.setShouldDeleteStoreOnModelMismatch(true)
        MagicalRecord.setupAutoMigratingCoreDataStack()
        MagicalRecord.setDefaultModelNamed("Pronto.momd")
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed:"Pronto")
    }
    
    /**
     Clean up Coredata Stack.
     */
    @objc func cleanAndResetupDB()
    {
        
        DispatchQueue.main.asyncAfter(deadline: .now() , execute: {
          
            self.resetManager()
            
            Constant.removeAllAssetsFromDB()
            Constant.removeAllCategoriesFromDB()
            Constant.removeAllOfflineFromDB()
            Constant.removeAllFranchiseFromDB()
            Constant.removeAllFavFranchiseFromDB()
            Constant.removeAllBrandFromDB()
            Constant.removeAllSavedPreferencesFromDB()
            Constant.removeAllEventsFromDB();
            Constant.removeAllEventsDurationFromDB()
            Constant.removeAllEventOnlyAssetFromDB()
            Constant.removeAllSessionsFromDB()
            
            MagicalRecord.cleanUp()
            Constant.removeFile("assets.db")

            let result = true
            if result
            {
                self.setupDB()
            }
            else
            {
//                print("An error has occured while deleting \(dbStore)")
//                print("Error description: \(String(describing: removeError?.description))")
            }
            
        })
    }
  
  /// init Pront filters
    @objc func initProntoFilters()
    {
        self.search = Search()
    }
  
  /// clean up all entities on app migration
    @objc func cleanupEntitiesOnAppMigration()
    {
        self.resetManager()
        Constant.removeAssetsFromDB {
            print("Removed all assets")
             Constant.removeAllCategoriesFromDB()
            print("Removed all category")
        }
    }
  
  /// check selected asset is available locally
  /// - Parameters:
  ///   - assetID: asset_ID
  ///   - title: asset title
  /// - Returns: return whether
  @objc func checkAssetIDAvailableLocally(assetID: NSNumber, title: String) -> Bool {
       let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(assetID)
       for data in offlineAssets {
         if let offData = data as? Asset {
           if offData.assetID == assetID && offData.title == title && offData.isDownloaded == true {
             print("ASSETS AVAILABILITY", offData)
             return true
           }
         }
       }
       return false
     }
  
  /// retrieve asset from db
  /// - Parameter assetID: asset_ID
  /// - Returns: return locally stored asset
  @objc func retrieveAsset(assetID: NSNumber) -> Asset {
     let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(assetID)
     for data in offlineAssets {
       if let offData = data as? Asset {
         if offData.assetID == assetID && offData.isDownloaded == true {
           return offData
         }
       }
     }
    return Asset()
   }
  
  /// validate asset based on file path
  /// - Parameter asset: asset whose path to be validated
  @objc func validateAsset(asset: Asset) {
    if asset.savedPath != nil {
      let fileSizePdf = fileSize(path: asset.savedPath ?? "")
      if fileSizePdf == asset.file_size?.intValue ?? 0 {
        ProntoAssetHandlingClass.sharedInstance().open(asset)
      } else {
        ProntoAssetHandlingClass.sharedInstance().validate(asset)
      }
    } else if asset.path != nil {
      let pdfSize = fileSize(path: asset.path ?? "")
      if pdfSize == asset.file_size?.intValue ?? 0 {
        ProntoAssetHandlingClass.sharedInstance().open(asset)
      } else {
        ProntoAssetHandlingClass.sharedInstance().validate(asset)
      }
    } else {
      ProntoAssetHandlingClass.sharedInstance().validate(asset)
    }
  }
  /// validate saved file path
  /// - Parameter asset: selected asset
  /// - Returns: return file path from directory
  @objc func validatePdf(asset: Asset) -> String {
    if asset.savedPath != nil {
      let fileSizePdf = fileSize(path: asset.savedPath ?? "")
      if fileSizePdf == asset.file_size?.intValue ?? 0 {
        return asset.savedPath ?? ""
      }
    } else if asset.path != nil {
      let pdfSize = fileSize(path: asset.path ?? "")
      if pdfSize == asset.file_size?.intValue ?? 0 {
        return asset.path ?? ""
      }
    }
    return ""
  }
  /// actual pdf file size
  /// - Parameter path: pdf file path
  /// - Returns: return stored file size
    @objc func fileSize(path: String) -> UInt64 {
      let fileManager = FileManager.default
      do {
        let attributes = try fileManager.attributesOfItem(atPath: path)
        var fileSize = attributes[FileAttributeKey.size] as! UInt64
        let dict = attributes as NSDictionary
        fileSize = dict.fileSize()
        return fileSize
      }
      catch let error as NSError {
        print("Something went wrong: \(error)")
        return 0
      }
    }
}
