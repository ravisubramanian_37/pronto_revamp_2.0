//
//  ZMAbbvieAPI.h
//  Pronto
//
//  Created by Carlos Arenas on 2/8/15.
//  Copyright (c) 2015 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMFranchise.h"
#import "ZMAsset.h"
#import "Asset.h"
#import <AFNetworking/AFNetworking.h>

@class ZMLibrary;
@class EventOnlyAsset;

@interface ZMAbbvieAPI : NSObject {
    BOOL assetAdded;
}

#define ZMLoginService @"ZM_LOGIN_SERVICE"
#define ZMUserRegistrationService @"ZM_USER_REGISTRATION_SERVICE"
#define ZMListOfAssetsService @"ZM_LIST_OF_ASSETS_SERVICE"
#define ZMListOfFranchisesService @"ZM_LIST_OF_FRANCHISES_SERVICE"
#define ZMEventsPostService @"ZM_EVENTS_POST_SERVICE"

@property(nonatomic, strong) NSString *token;
@property (nonatomic) int lastServerCodeResponse;
@property (nonatomic) int _currentPage;
@property (nonatomic) BOOL _syncingInProgress;
@property (nonatomic) BOOL _syncingCompletion;
@property (nonatomic) int totalAssets;
@property (strong, nonatomic) ZMLibrary *library;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property(nonatomic, strong) NSString *searchKey;
@property (nonatomic) int counter;
@property(nonatomic, strong) NSMutableArray *totalSearchedAssets;
@property(nonatomic, strong) NSMutableArray * initialDownloadAssetArray;
@property(nonatomic, strong) NSArray * initialDownloadDbArray;
@property (nonatomic) int totalPages;
@property (nonatomic) NSNumber* tappedAsset;

@property (weak, nonatomic) ZMLibrary *libraryVC;

+ (id)sharedAPIManager;

/**
 *  Perform login on the CMS whit AOF data
 *
 *  @param complete block complete
 *  @param onError  block error
 */
- (void)userLogin:(void(^)(NSString *token))complete error:(void(^)(NSError *error))onError;

/**
 *  Get notifications about messages.
 *
 *  @param date     Notifications greather than this date
 *  @param complete Return block complete if there's not errors
 *  @param onError  Return block if there is error
 */
//- (void)getNotificationsWithDate:(NSString *)date completeBlock:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError;

/**
 *  Get Asset mark as popular
 *
 *  @param complete Return block complete if there's not errors
 *  @param onError  Return block if there is error
 */
- (void)getPopularAsset:(void(^)(NSDictionary *popularAsset))complete error:(void(^)(NSError *error))onError;

/**
 *  Search keyword from CMS - SOLR */
// New Search Implementation
- (void)searchAssetsFromCMS:(void(^)(void))complete successList:(void (^)(NSArray *))success error:(void(^)(NSError *error))onError;

/**
 *  Synchronize data with CMS (Folders-Brand-Assets-Franchises)
 */
- (void)syncWithCMS;

- (void)synchronizeFolders:(void(^)(void))complete error:(void(^)(NSError *error))onError;

/**
 *  Download asset inform progress, this method using AFHTTPRequestOperation to tracking progres. Each time a error comes, the request will try execute itself max 3 times.
 *
 *  @param asset           Asset to download
 *  @param progressBlock   Progress block
 *  @param completionBlock Complete block
 *  @param errorBlock      Error block
 */

- (void) downloadAsset: (Asset *) asset
          withProgress:(void (^)(CGFloat progress))progressBlock
            completion:(void (^)(void)) completionBlock
               onError:(void (^)(NSError *error))errorBlock;

/**
 *  Mark a message as read
 *
 *  @param notificationId Unique Id notification
 *  @param complete       Return block complete if there's not errors
 *  @param onError        Return block if there is error
 */
//- (void)markNotificationsAsRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError;

/**
 *  Mark a message as read
 *
 *  @param notificationId Unique Id notification
 *  @param complete       Return block complete if there's not errors
 *  @param onError        Return block if there is error
 */
//- (void)markNotificationsAsUNRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError;

/**
 *  Download thumbnail to asset
 *
 *  @param path      Path's asset to contain thumbnail address
 *  @param complete  Return block complete if there's not errors
 *  @param cache     Bool to give cache to download Asset
 *  @param waterMark Bool to include water mark on thumbnail
 */
- (void)downloadThumbnail:(NSString *)path complete:(void(^)(UIImage *thumbnail, NSString *p))complete fromCache:(BOOL)cache includeWaterMark:(NSString *)waterMark;

/**
 *  Upload new event each time a asset has been updated or folder has been created
 *
 *  @param complete Return block complete if there's not errors
 *  @param onError  Return block if there is error
 */
- (void)uploadEvents:(void(^)(void))complete error:(void(^)(NSError *error))onError;

/**
 *  Register new token againt CMS to pass around WS calls
 *
 *  @param deviceToken Device token to register
 *  @param complete    Return block complete if there's not errors
 *  @param onError     Return block if there is error
 */
- (void)registerToken:(NSString *)deviceToken complete:(void(^)(void))complete error:(void(^)(NSError *error))onError;

/**
 *  UnRegister token againt CMS
 *
 *  @param deviceToken Device token to unregister
 *  @param complete    Return block complete if there's not errors
 *  @param onError     Return block if there is error
 */
- (void)unRegisterToken:(NSString *)deviceToken complete:(void(^)(void))complete error:(void(^)(NSError *error))onError;

/**
 *  UnRegister token againt CMS if session is broken
 *
 *  @param deviceToken Device token to unregister
 *  @param complete    Return block complete if there's not errors
 *  @param onError     Return block if there is error
 */
//- (void)unRegisterTokenNoSession:(NSString *)deviceToken complete:(void (^)(void))complete error:(void (^)(NSError *))onError;

/**
 *  Create MD5 algorithm to download a new path to Asset
 *
 *  @param input URL to asset
 *
 *  @return New encrypted name
 */
- (NSString *)MD5HashForString:(NSString *)input;

/**
 *  Get thumnail of selected Asset
 *
 *  @param externalPath path of asset saved
 *
 *  @return Thumbnail path
 */
- (NSString *)getThumbnailPath:(NSString *)externalPath;

/**
 *  Get image of selected Asset
 *
 *  @param externalPath path of asset saved
 *
 *  @return image path
 */
- (NSString *)getCoverflowImagePath:(NSString *)externalPath;

/**
 *  Get image for coverflow
 *
 *  @param externalPath path of asset saved
 *
 *  @return Image path
 */
- (void)downloadCoverflowImage:(NSString *)path complete:(void(^)(UIImage *image, NSString *p))complete fromCache:(BOOL)cache;

/**
 *  Get status conection
 *
 *  @return Bool to indicate wheter the conection is available or not
 */
- (BOOL)isConnectionAvailable;
- (void)searchAssets:(void(^)(void))complete progress:(void(^)(float progressValue))progress successList:(void (^)(NSArray *))success error:(void(^)(NSError *error))onError;
-(NSArray *)sortedArray: (NSString*)sortingTerm andArray:(NSMutableArray *)array;
-(void)getSearchedAssets:(NSArray *)assetIds success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError;
// New Revamping changes - getPromotedContent
- (void)getPromotedContent:(NSString*)franchiseId complete:(void(^)(NSDictionary *promotedAssets))complete error:(void(^)(NSError *error))onError;
// New Revamping changes - sendFeedback
//-(void)sendFeedback:(NSString *)assetId type:(NSString*)feedbackType comments:(NSString*)feedbackComments success:(void (^)(NSString *))success error:(void (^)(NSError *))onError;
// New Revamping changes - send mail to self
//-(void)sendEmailToself:(NSString *)assetId success:(void (^)(NSDictionary *))success error:(void (^)(NSError *))onError;

-(void) offlineDownload: (NSMutableArray *)assetDownloadArray;

-(void) downloadAssetFor:(Asset *)asset;
-(void) downloadEventOnlyAsset:(EventOnlyAsset *)asset;
// New Revamping changes - fetch the fav assets from CMS
- (void)fetchEvents:(void(^)(NSDictionary *FavAssets))complete error:(void(^)(NSError *error))onError;
- (void)synchronizeBrands:(void(^)(void))complete error:(void(^)(NSError *error))onError;

// Saving the selected Preferences
- (void)savePreferences:(NSArray*)franchiseNode complete:(void(^)(NSDictionary *preferencesSavedStatus))complete error:(void(^)(NSError *error))onError;
// Fetch the Preferences from CMS
//- (void)fetchPrefernces:(void(^)(NSArray *preferencesFetched))complete error:(void(^)(NSError *error))onError;
// getEvents for Landing screen
- (void)getEvents:(void(^)(NSDictionary *eventsFetched))complete error:(void(^)(NSError *error))onError;
// session Details for CalendarView
- (void)getEventDetails:(NSString*)eventId complete:(void(^)(NSDictionary *sessionDetails))complete error:(void(^)(NSError *error))onError;

- (void) downloadEventOnlyAsset: (EventOnlyAsset *) eventOnlyAsset
                   withProgress:(void (^)(CGFloat progress))progressBlock
                     completion:(void (^)(void)) completionBlock
                        onError:(void (^)(NSError *error))errorBlock;
// fetchViewed - fetching the already visited assets
- (void)fetchViewed:(void(^)(NSDictionary *assetDetailsFetched))complete error:(void(^)(NSError *error))onError;
// saveViewed - Saving the already visited assets
- (void)saveViewed:(NSString*) eventId session:(NSString*) sessionId asset:(NSString*) assetId type:(NSString*)assetType complete:(void(^)(NSDictionary *savedAssetDetails))complete error:(void(^)(NSError *error))onError;
- (void)saveViewedAsset:(NSNumber*)assetId type:(NSString*)assetType complete:(void(^)(NSDictionary *savedAssetDetails))complete error:(void(^)(NSError *error))onError;
- (void)syncAllEvents:(void(^)(NSDictionary *allEvents))complete error:(void(^)(NSError *error))onError;
- (NSString *)getServiceResource:(NSString*)root;
- (void)syncAssets:(void(^)(void))complete progress:(void(^)(float progressValue))progress error:(void(^)(NSError *error))onError;
-(void)broadcastingToProvider: (Asset *) asset;
@end
