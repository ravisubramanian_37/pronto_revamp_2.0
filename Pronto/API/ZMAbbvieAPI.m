//
//  ZMAbbvieAPI.m
//  Pronto
//
//  Created by Carlos Arenas on 2/8/15.
//  Copyright (c) 2015 Abbvie. All rights reserved.
//

#import "ZMAbbvieAPI.h"
#import "NSString+Util.h"
#import "ZMAssetsLibrary.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonDigest.h>
#import "AssetDuration.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Categories.h"
#import "Franchise.h"
#import "Brand.h"
#import "Asset.h"
//#import "ZMLibrary.h"
#import "Pronto-Swift.h"
#import "AppDelegate.h"
#import "ZMUserActions.h"
#import <objc/runtime.h>
#import "Constant.h"
#import "ProntoUserDefaults.h"
#import "BucketDetailDataControl.h"
#import "UIImage+ImageDirectory.h"
#import "NSMutableDictionary+Utilities.h"
#import "EventCoreDataManager.h"


#import "EventOnlyAsset+CoreDataProperties.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventCoreDataManager.h"

#import "Encryptor.h"
#import "RNEncryptor.h"


@implementation ZMAbbvieAPI

typedef void (^AssetCompletionBlock) (void);
typedef void (^CallbackBlock)(NSError* error, NSObject* response);

NSMutableArray * assetArray;

AssetCompletionBlock assetsQueue;

#pragma mark - Setup

//+ (id)sharedAPIManager {
//
//
//    static ZMAbbvieAPI *manager = nil;
//    static dispatch_once_t onceToken;
//
//    dispatch_once(&onceToken, ^{
//        manager = [[self alloc] init];
//    });
//
//    return manager;
//}

- (void)resetAbbvieApiSharedInstance
{
    
}

+ (id)sharedAPIManager {
    
    @synchronized(self)
    {
        static ZMAbbvieAPI *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}

/**
 *  Init RequestOperation Manager to get data from WS
 *
 *  @param hasToken Bool to indicate if a user has a valid token
 *  @param headers  Headers of request
 *
 *  @return Instance of AFHTTPRequestOperationManager
 */

- (AFHTTPRequestOperationManager *)requestManagerWithToken:(BOOL)hasToken headers:(NSDictionary *)headers {
    //added by Prathap: Now this reads the data from Config file automatically based on the scheme it is being build.
    NSDictionary* serverCOnfig = [self getServerConfig:@"ServerConfig"];
    NSURL *baseURL = [NSURL URLWithString:[serverCOnfig objectForKey:CONFIG]];
    //End of added by Prathap.
    
    // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setStringEncoding:NSUTF8StringEncoding];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer = /*[AFJSONResponseSerializer serializer]*/responseSerializer;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
   
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
        if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
        {
            [manager.requestSerializer setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
        }
        else{
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
        }
    }
    if (hasToken && self.token) {
        [manager.requestSerializer setValue:self.token forHTTPHeaderField:@"X-CSRF-Token"];
    }
    
    for (NSString *key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    }
    
    return manager;
}

/**
 *  Handle WS response, in case a WS give a error, the function will try sending the request three times, if the error is still present after 3 times, the function will return the error, in ohter case, will return the resonse object.
 *
 *  @param blockToExecute Block with request to perform
 *  @param ntimes         number of times to try request in case on error comes
 *  @param onCompletion   Block completion.
 */
- (void) performBlock:(void (^)(CallbackBlock callback)) blockToExecute retryingNumberOfTimes:(NSUInteger)ntimes onCompletion:(void (^)(NSError* error, NSObject* response)) onCompletion {
    blockToExecute(^(NSError* error, NSObject* response){
        if (error == nil) {
            onCompletion(nil, response);
        }else{
            if (ntimes <= 0) {
                if (onCompletion) {
                    onCompletion(error, nil);
                }
            } else {
                if (error.code == 3840 || error.code == -1011 || error.code == -1001 || error.code == -1005) {
                    [AbbvieLogging logInfo:@"** trying renew sesion"];
                        [self performBlock:blockToExecute retryingNumberOfTimes:(ntimes - 1) onCompletion:onCompletion];

                }else{
                    [self performBlock:blockToExecute retryingNumberOfTimes:(ntimes - 1) onCompletion:onCompletion];
                }
            }
        }
    });
}

//Seperate block function for offline download
- (void) performBlockForOfflineDownload:(void (^)(CallbackBlock callback)) blockToExecute retryingNumberOfTimes:(NSUInteger)ntimes onCompletion:(void (^)(NSError* error, NSObject* response)) onCompletion {
    blockToExecute(^(NSError* error, NSObject* response){
        if (error == nil) {
            onCompletion(nil, response);
        }else{
            if (ntimes <= 0) {
                if (onCompletion) {
                    onCompletion(error, nil);
                }
            } else {
                if (error.code == 3840 || error.code == -1011 || error.code == -1001 || error.code == -1005) {
                    [AbbvieLogging logError:@"** trying renew sesion"];
                        [self performBlockForOfflineDownload:blockToExecute retryingNumberOfTimes:(ntimes - 1) onCompletion:onCompletion];
                }else{
                    [self performBlock:blockToExecute retryingNumberOfTimes:(ntimes - 1) onCompletion:onCompletion];
                }
            }
        }
    });
}

#pragma mark - Endpoints

- (void)userLogin:(void(^)(NSString *token))complete error:(void(^)(NSError *error))onError{

    if ([AOFLoginManager sharedInstance].upi) {

        NSError *err;
        
       //dev & stage Env - With Encryption
        NSDictionary *UsernamePasswordDict = @{@"username":kUsername, @"password":kPassword};
        
        //Forming Json array for username and password
        NSData *jsonUserNameAndPasswordData = [NSJSONSerialization dataWithJSONObject:UsernamePasswordDict options:0 error:&err];

        //Encrypting User Login data
        NSData *encryptedUsernamePasswordDictData = [RNEncryptor encryptData:jsonUserNameAndPasswordData
        withSettings:kRNCryptorAES256Settings
              password:kRNEncryptionKeyForStage
                 error:&err];
        
        //Forming Params dict for login Service call
        NSString* accessToken = [AOFLoginManager sharedInstance].getAccessToken;
        
        //For Dev & Stage Environment - With encryption & new params list
        NSDictionary *params = @{kTokenKey:accessToken,@"info":[encryptedUsernamePasswordDictData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]};
        
        NSString *url = [self getServiceResource:@"ws.login.url"];
        
        
       // working
        [self performBlock:^(CallbackBlock callback) {
            
            AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:NO headers:nil];
            
            [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        [[NSUserDefaults standardUserDefaults]setValue:[responseObject valueForKey:@"sessid"] forKey:@"sessionId"];
                        
                        [[NSUserDefaults standardUserDefaults]setValue:[responseObject valueForKey:@"session_name"] forKey:@"sessionName"];
                        
                        
                        NSString *path = [[NSBundle mainBundle] pathForResource:@"Pronto-Config" ofType:@"plist"];
                        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
                        
                        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:[dict valueForKey:@"server"]]];
                        for (NSHTTPCookie *cookie in cookies)
                        {
                            NSLog(@"COOKIE :%@\n",cookie);
                            if([cookie.name isEqualToString:@"SimpleSAMLSessionID"])
                                [[NSUserDefaults standardUserDefaults]setValue:cookie.value forKey:@"SimpleSAMLSessionID"];
                        }
                        
                        if([[[NSUserDefaults standardUserDefaults]valueForKey:@"isFirstTime"] isEqualToString:@"YES"]){
                            [[ProntoUserDefaults singleton]isReadyToMakeServiceCalls:1];
                            [(AppDelegate *)[[UIApplication sharedApplication] delegate] redirectToLibrary]; //In any case redirect to the library
//                            self.token = [responseObject valueForKey:@"token"];
                        }
                        
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                    callback(errorCode, nil);
                }
            }];
            
        } retryingNumberOfTimes:0 onCompletion:^(NSError *error, NSObject *response) {
            
            
            if (error) {
                [ZMUserActions sharedInstance].isLoginSuccess = NO;
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS login uri", (long)error.code, error.localizedDescription]];
                if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004 || error.code ==-1003)
                {
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }
                else
                {
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:error.localizedDescription]);
                }

                self._syncingCompletion = YES;
                
                
            }else
            {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"logiinnn WS Response: %@", response]];
                [ZMUserActions sharedInstance].isLoginSuccess = YES;
                complete((NSString *)response);
            }
        }];
        
       
    }
}

//- (void)getNotificationsWithDate:(NSString *)date completeBlock:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError{
//    
//    if ([AOFLoginManager sharedInstance].upi) {
//
//        NSDictionary *params = @{@"page":@"0",
//                                        @"per_page":@"80",
//                                        @"new":@"1",
//                                        kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
//               
//        
//        if (date) {
//            params = @{@"page":@"0",
//                       @"per_page":@"80",
//                       @"new":@"1",
//                       @"date":date,
//                       kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
//        }
//        
//        NSString *url = [self getServiceResource:@"notification.uri"];
//        
//        [self performBlock:^(CallbackBlock callback) {
//            
//            AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:nil];
//            
//            [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                
//                dispatch_async(dispatch_get_main_queue(), ^(void){
//                    if (callback) {
//                        callback(nil, responseObject);
//                    }
//                });
//                
//            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                
//                if (callback) {
//                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
//                    callback(errorCode, nil);
//                }
//            }];
//            
//        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
//            
//            if (error) {
//                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS notification.uri", (long)error.code, error.localizedDescription]];
//                
//                if (error.code == 107) {
//                    onError([self getErrorWithCode:107
//                                  alternateMessage:@"The aplication is unavailible at this moment."]);
//                }else if (error.code == 401) {
//                    onError([self getErrorWithCode:401
//                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
//                }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
//                    onError([self getErrorWithCode:error.code
//                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
//                }
//                else {
//                    onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
//                }
//                
//            }else
//            {
//                [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS Response notification: %@", response]];
//                complete((NSDictionary *)response);
//            }
//        }];
//    }
//}

- (void)getPopularAsset:(void(^)(NSDictionary *popularAsset))complete error:(void(^)(NSError *error))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
                                 @"sf":[AOFLoginManager sharedInstance].salesFranchiseID,
                                 @"ssid":[AOFLoginManager sharedInstance].spaceStructureID};
        
        NSString *url = [self getServiceResource:@"popular.asset.uri"];
        
        [self performBlock:^(CallbackBlock callback) {
            
            AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:nil];
          
            [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                    callback(errorCode, nil);
                }
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS popular.asset.uri", (long)error.code, error.localizedDescription]];
                
                if (error.code == 107) {
                    onError([self getErrorWithCode:107
                                  alternateMessage:@"The aplication is unavailible at this moment."]);
                }else if (error.code == 401) {
                    onError([self getErrorWithCode:401
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }
                else
                {
                    [AbbvieLogging logError:[NSString stringWithFormat:@"err:%ld %@",(long)error.code , error.localizedDescription]];
                    onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }

            }else{
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS Response - popular asset: %@", response]];
                complete((NSDictionary *)response);
            }
        }];

    }
}

// New Search Implementation
- (void)searchAssetsFromCMS:(void(^)(void))complete successList:(void (^)(NSArray *))success error:(void(^)(NSError *error))onError
{
    [AbbvieLogging logInfo:@"Searching Assets.."];
    
     self._syncingInProgress = NO; //TO AVOID ISSUES WHEN SYNCING
    //Adding parameter for type of progress
    [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Refreshing Events" progress:0 progressFor:@"search"];
     self._currentPage = 0;
    
    dispatch_queue_t q = dispatch_queue_create("com.my.queue", NULL);
    
    dispatch_sync(q, ^{
        [self searchAssets:^{
           
            [AbbvieLogging logInfo:@"Completed Search"];
           
            [ZMUserActions sharedInstance].searchCompletion = YES;
            if(([ZMUserActions sharedInstance].assetsCount == 0) && [ZMAssetsLibrary defaultLibrary].completionStatus)
            {
                [AbbvieLogging logInfo:@"trying.."];
            }
            
            
            [[ZMAssetsLibrary defaultLibrary] notifyFinish];
        } progress:^(float progressValue) {
            //Adding parameter for type of progress
            [[ZMAssetsLibrary defaultLibrary] notifyStep:[NSString stringWithFormat:@"Getting Assets %d%%", (int)progressValue] progress:progressValue progressFor:@"search"];
         
            [ZMUserActions sharedInstance].enableSearch = NO;
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"Search asset progress %f", progressValue]];
        }successList:^(NSArray *success)
         {
             [AbbvieLogging logInfo:[NSString stringWithFormat:@"Search asset success %@",success]];
         }
         error:^(NSError *error) {
             //Adding parameter for type of progress
             [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Refreshing Events" progress:0 progressFor:@"search"];
            self._syncingInProgress = NO;
             [AbbvieLogging logError:[NSString stringWithFormat:@">>>> Error getting the assets %@", error]];
            self.lastServerCodeResponse = (int)error.code;
            [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
        }];
    });
}

- (void)syncWithCMS
{
    [AbbvieLogging logInfo:@"syncWithCMS started"];
    self._syncingInProgress = YES; //TO AVOID ISSUES WHEN SYNCING
    
    //Adding parameter for type of progress
    [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Refreshing Events" progress:0 progressFor:@"mainSync"];
    
    [ZMUserActions sharedInstance].selectLeftMenu = @"";
    
    dispatch_group_t syncGroup = dispatch_group_create();

    __block BOOL isBrandsSyncCompleted = NO;
    __block BOOL isStatsSyncCompleted = NO;
    __block BOOL isCategoriesSyncCompleted = NO;
    
    dispatch_group_enter(syncGroup);
    
    [self synchronizeBrands:^{
        [AbbvieLogging logInfo:@">>>> SyncCMS Finished sycronizing brands"];

        isBrandsSyncCompleted = YES;
        dispatch_group_leave(syncGroup);
    } error:^(NSError *error) {
        self._syncingInProgress = NO;
        [AbbvieLogging logError:[NSString stringWithFormat:@">>>> Error with sycronizing brands %@", error]];
        self.lastServerCodeResponse = (int)error.code;
        [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
        dispatch_group_leave(syncGroup);
    }];
    
    dispatch_group_enter(syncGroup);
    [self refreshStats:^{
        [AbbvieLogging logInfo:@">>>> SyncCMS Finished sync of the stats"];

        isStatsSyncCompleted = YES;
        [[ZMAssetsLibrary defaultLibrary] notifyStart];
        self._currentPage = 0;
        //Adding parameter for type of progress
        [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Getting Assets" progress:0 progressFor:@"mainSync"];
        [ZMUserActions sharedInstance].enableSearch = NO;
        dispatch_group_leave(syncGroup);
    } error:^(NSError *error) {
        self._syncingInProgress = NO;
        [AbbvieLogging logError:[NSString stringWithFormat:@">>>> SyncCMS Error retrieving stats %@", error]];
        self.lastServerCodeResponse = (int)error.code;
        [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
        dispatch_group_leave(syncGroup);
    }];
    
    dispatch_group_enter(syncGroup);
    [self getCategories:^{
        [AbbvieLogging logInfo:@">>>> SyncCMS Finished sync of the categories"];
        [[ZMAssetsLibrary defaultLibrary] setProperty:@"CATEGORIES_LASTUPDATE" value:[NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]]];
        isCategoriesSyncCompleted = YES;
        
        // NSLog(@"********Status library %d", self._syncingInProgress);
        dispatch_group_leave(syncGroup);
    } error:^(NSError *error) {
        self._syncingInProgress = NO;
        [AbbvieLogging logError:[NSString stringWithFormat:@">>>> Error getting the categories %@", error]];
        self.lastServerCodeResponse = (int)error.code;
        [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
        dispatch_group_leave(syncGroup);
    }];
    
    
    dispatch_group_wait(syncGroup, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(45 * NSEC_PER_SEC))); //60
    
    dispatch_async( dispatch_get_main_queue(), ^{
        
        BOOL isAllRequestHappendSuccessFully = NO;
        
        if (isStatsSyncCompleted == YES &&
            isBrandsSyncCompleted == YES &&
            isCategoriesSyncCompleted == YES)
        {
            isAllRequestHappendSuccessFully = YES;
        }
        
        if (isAllRequestHappendSuccessFully == NO)
        {
            //  creating custom timeout error
            [AbbvieLogging logInfo:@">>>> SyncCMS isAllRequestHappendSuccessFully == NO"];
            NSError *timeOutError = [NSError errorWithDomain:@"com.pronto.SyncCMS" code:-1001 userInfo:nil];
            
            //Adding parameter for type of progress
            [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Refreshing Events" progress:0 progressFor:@"mainSync"];
            [ZMUserActions sharedInstance].selectLeftMenu = @"";
            self._syncingInProgress = NO;
            [ZMUserActions sharedInstance].enableSearch = NO;
            self.lastServerCodeResponse = (int)timeOutError.code;
            [[ZMAssetsLibrary defaultLibrary] notifyError:@"IsCustomTimeOut"];
            
            if(self.libraryVC.isShowingIndicatorViewWithOverlay == YES) { return ;}
        }
        
        [AbbvieLogging logInfo:@">>>> SyncCMS sync of Asset starts"];
        [self syncAssets:^{

            [AbbvieLogging logInfo:@">>>> SyncCMS   sync of Asset completes"];
            self._syncingInProgress = NO;
            [ZMUserActions sharedInstance].enableSearch = YES;
            [[ZMAssetsLibrary defaultLibrary] notifyFinish];
            self._syncingCompletion = YES;
        } progress:^(float progressValue) {
            //Adding parameter for type of progress
            [[ZMAssetsLibrary defaultLibrary] notifyStep:[NSString stringWithFormat:@"Getting Assets %d%%", (int)progressValue] progress:progressValue progressFor:@"mainSync"];
            [ZMUserActions sharedInstance].enableSearch = NO;
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download asset progress %f", progressValue]];
        } error:^(NSError *error) {
            //Adding parameter for type of progress
            [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Refreshing Events" progress:0 progressFor:@"mainSync"];
            [ZMUserActions sharedInstance].selectLeftMenu = @"";
            self._syncingInProgress = NO;
            [ZMUserActions sharedInstance].enableSearch = NO;
            [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> Error getting the assets %@", error]];
            self.lastServerCodeResponse = (int)error.code;
            [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
        }];
    });
}


- (void)uploadEvents:(void(^)(void))complete error:(void(^)(NSError *error))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        //AOFKit Implementation
        NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//        NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
        
        NSArray *events = [[ZMAssetsLibrary defaultLibrary] events:@"JSON" withSaleFranchiseId:franchiseId];
        
        if ([events count] > 0) {
            
            NSMutableArray *toJSON = [NSMutableArray array];
            for (ZMEvent * event in events){
                
                //if value is not available, May be api fails and it will be taken care next time.
                NSString *sf = event.sf != nil ? event.sf : @"";
                NSString *eventValue = event.eventValue != nil ? event.eventValue : @"";
               
                NSDictionary *eventToSend = [NSDictionary dictionaryWithObjectsAndKeys:
                                             [NSString stringWithFormat:@"%ld",event.assetId], @"id",
                                             [NSString stringWithFormat:@"%ld",event.eventType], @"type",
                                             eventValue, @"value",
                                             [NSString stringWithFormat:@"%ld", event.eventDate],@"date",
                                             [NSString stringWithFormat:@"%ld",event.createdFromBucketId], @"bid",
                                             sf,@"sf", nil];
                [toJSON addObject:eventToSend];
            }
            
            NSError *err;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:toJSON options:0 error:&err];
            NSString *JSONToPost = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
//            [params setObjectIfValueAvailable:[AOFLoginManager sharedInstance].upi forKey:@"upi"];
//            [params setObjectIfValueAvailable:franchiseId forKey:@"sf"];
//            [params setObjectIfValueAvailable:spaceStructureId forKey:@"ssid"];
             //New Implementation with AccessToken
            [params setObjectIfValueAvailable:[AOFLoginManager sharedInstance].getAccessToken forKey:kTokenKey];
            [params setObjectIfValueAvailable:JSONToPost forKey:@"events"];
           
            
            NSString *url = [self getServiceResource:@"events.uri"];
            
            [self performBlock:^(CallbackBlock callback) {
                
                [[self requestManagerWithToken:YES headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (callback) {
                            callback(nil, responseObject);
                        }
                    });
                    
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (callback) {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        callback(errorCode, nil);
                    }
                    
                }];
                
            } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
               
                if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS notification.uri", (long)error.code, error.localizedDescription]];
                    
                    if (error.code == 107) {
                        onError([self getErrorWithCode:107
                                      alternateMessage:@"The aplication is unavailible at this moment."]);
                    }else if (error.code == 401) {
                        onError([self getErrorWithCode:401
                                      alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                    }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                        onError([self getErrorWithCode:error.code
                                      alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                    }
                    else {
                        onError([self getErrorWithCode:000 alternateMessage:@"The application cannot send user information."]);
                    }

                }else{
                
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS Upload Events Response: %@", response]];
                    
                    for (ZMEvent *event in events) {
                        [[ZMAssetsLibrary defaultLibrary] deleteEvent:event];
                    }
                    
                    complete();
                }
            }];

        }
        else {
            complete();
        }
    }
}

/**
 *  Synch Folders from CMS into Coredata
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */

- (void)synchronizeFolders:(void(^)(void))complete error:(void(^)(NSError *error))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        if ([[ZMAssetsLibrary defaultLibrary] folderWithId:0]) {
            
            NSError *err;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self currentFolders] options:0 error:&err];
            NSString *postJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            //AOFKit Implementation
            
            NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
            NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
            
            
            
            NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
                                     @"sf":franchiseId,
                                     @"ssid":spaceStructureId,
                                     @"data":postJSON};
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"sync params:%@",params]];
            NSString *url = [self getServiceResource:@" "];
            
            
            [self performBlock:^(CallbackBlock callback) {
                
                [[self requestManagerWithToken:YES headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (callback) {
                            callback(nil, responseObject);
                        }
                    });
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (callback) {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        callback(errorCode, nil);
                    }
                    
                }];
                
            } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
                
                if (error) {
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"params:%@",params]];
                    
                    [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS sync.uri", (long)error.code, error.localizedDescription]];
                    if (error.code == 107) {
                        onError([self getErrorWithCode:107
                                      alternateMessage:@"The aplication is unavailible at this moment."]);
                    }else if (error.code == 401) {
                        onError([self getErrorWithCode:401
                                      alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                    }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                        onError([self getErrorWithCode:error.code
                                      alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                    }
                    else {
                        onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                    }
                }else{
                    if (response) {
                        
                        NSDictionary *responseObject = (NSDictionary *)response;
                        NSMutableArray *remoteFolders = [NSMutableArray array];
                        long remoteBriefDate = 0;
                        NSString *refreshTime = nil;
                        
                        long localBriefDate = (long)[[self getFolderDate] longLongValue];
                        
                        if ([responseObject objectForKey:@"briefcase_date"]) {
                            long remoteBriefDate = (long)[[responseObject objectForKey:@"briefcase_date"] longLongValue];
                            
                            if (remoteBriefDate > localBriefDate) {
                                NSDictionary *folders = [responseObject objectForKey:@"briefcase_data"];
                                
                                for (NSDictionary *data in folders) {
                                    ZMFolder *folder = [[ZMFolder alloc]init];
                                    folder.fldr_id = [[data objectForKey:@"id"] intValue];
                                    folder.fldr_name = [data objectForKey:@"name"];
                                    folder.fldr_qty = [[data objectForKey:@"qty"] intValue];
                                    folder.fldr_date_created =[[data objectForKey:@"date"]intValue];
                                    folder.fldr_is_briefcase = [[data objectForKey:@"briefcase"] intValue];
                                    folder.metadata = [data objectForKey:@"metadata"];
                                    
                                    NSMutableDictionary *assets = [NSMutableDictionary dictionary];
                                    
                                    if([[data objectForKey:@"assets"] count]>0){
                                        NSArray *folderAssets = [[data objectForKey:@"assets"] allKeys];
                                        
                                        for(NSString *key in folderAssets){
                                            NSNumber *assetId = [NSNumber numberWithInt:[key intValue]];
                                            [assets setObject:assetId forKey:assetId.stringValue];
                                        }
                                    }
                                    folder.asset_list= assets;
                                    [remoteFolders addObject:folder];
                                }
                            }
                            
                            refreshTime = [responseObject objectForKey:@"refreshTime"];
                        }
                        else{
                            remoteBriefDate = -1;
                        }
                        
                        NSDictionary *toReplace = [[NSDictionary alloc] init];
                        if (refreshTime) {
                            if (remoteFolders.count) {
                                toReplace = [NSDictionary dictionaryWithObjectsAndKeys:
                                             remoteFolders, @"folders",
                                             [NSString stringWithFormat:@"%ld",remoteBriefDate], @"folders_date",
                                             refreshTime ,@"refreshTime", nil];
                            }
                        }
                        else{
                            toReplace = @{@"folders":remoteFolders, @"folders_date":[NSString stringWithFormat:@"%ld",remoteBriefDate]};
                        }
                        
                        if (toReplace) {
                            
                            if ([[toReplace objectForKey:@"folders"] count] > 0) {
                                [[ZMAssetsLibrary defaultLibrary] replaceBriefcase:[toReplace objectForKey:@"folders"]];
                                [[ZMAssetsLibrary defaultLibrary] setProperty:@"FOLDERS_LASTUPDATE" value:[toReplace objectForKey:@"folders_date"]];
                            }
                            
                            complete();
                        }

                    }
                }
            }];
        }
        else {
            onError([self getErrorWithCode:301 alternateMessage:@"The authentication service cannot be reached."]);
        }
        
        // PRONTO-88 - Backlog issue - Asset selection not highlighting the Folder added
        [ZMUserActions sharedInstance].existingFolders = [self currentFolders];
    }
    
    
}

/**
 *  Sync Franchises and Brands fro WS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */

- (void)synchronizeBrands:(void(^)(void))complete error:(void(^)(NSError *error))onError{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> SyncCMS calling: %s", __PRETTY_FUNCTION__]];
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        NSString *lastUpdate = [[ZMAssetsLibrary defaultLibrary] property:@"BRANDS_LASTUPDATE"];
        NSArray * currentFranchise = [[ZMCDManager sharedInstance] getFranchises];
        long lupdate = 0;

        if (lastUpdate && !currentFranchise) {
            lupdate = (long)[lastUpdate longLongValue];
        }
        
        //AOFKit Implementation
//        NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//        NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
//        [params setObjectIfValueAvailable:[AOFLoginManager sharedInstance].upi forKey:@"upi"];
//        [params setObjectIfValueAvailable:franchiseId forKey:@"sf"];
//        [params setObjectIfValueAvailable:spaceStructureId forKey:@"ssid"];
        [params setObjectIfValueAvailable:[NSNumber numberWithLong:lupdate] forKey:@"date"];
        
        //New Implementation with token
        [params setObjectIfValueAvailable:[AOFLoginManager sharedInstance].getAccessToken forKey:kTokenKey];
        
        NSString *url = [self getServiceResource:@"brands.uri"];
        NSDictionary *headers = [NSDictionary dictionaryWithObjectsAndKeys:@"1.1",@"services_asset_getvocabulary_version", nil];
        
        [self performBlock:^(CallbackBlock callback) {
            
            [[self requestManagerWithToken:YES headers:headers] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        callback(nil, responseObject);
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"brands uri:%@",responseObject]];
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                    callback(errorCode, nil);
                }
                
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {

                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS brands.uri", (long)error.code, error.localizedDescription]];

                if (error.code == 107) {
                    onError([self getErrorWithCode:107
                                  alternateMessage:@"The aplication is unavailible at this moment."]);
                }else if (error.code == 401) {
                    onError([self getErrorWithCode:401
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }
                else {
                    onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
            }else{
                if (response) {

                    NSDictionary *brands = (NSDictionary *)response;
                    NSString *lastDate = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
                    
                    if ([brands count] > 0) {
                        if ([brands objectForKey:@"date"]) {
                            lastDate = [brands objectForKey:@"date"];
                        }
                    }
                    else {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"-> No brands error: %@", ZMListOfFranchisesService]];
                    }
                    
                    [[ZMAssetsLibrary defaultLibrary] setProperty:@"BRANDS_LASTUPDATE" value:lastDate];
                    
                    [self getFranchise:brands success:^(NSArray *franchiseArray, NSArray *brandsArray) {

                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"successed imported franchises %lu , brands %lu", (unsigned long)franchiseArray.count, (unsigned long)brandsArray.count]];
                        [ZMProntoManager sharedInstance].franchisesArray = franchiseArray;
                        [ZMProntoManager sharedInstance].brandsArray = brandsArray;
                        complete();
                    } error:^(NSError *error) {
                        [AbbvieLogging logError:[NSString stringWithFormat:@"error at importing franchises %@", error.localizedDescription]];
                        onError(error);
                    }];
                }
            }
        }];
    }
}

/**
 *  Get latets Assets updated from WS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */

- (void)refreshStats:(void(^)(void))complete error:(void(^)(NSError *error))onError{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> SyncCMS calling: %s", __PRETTY_FUNCTION__]];
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        NSString *date = [[ZMAssetsLibrary defaultLibrary] property:@"STATS_LASTUPDATE"];
        
        //AOFKit Implementation
//        NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//        NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
//        [params setObjectIfValueAvailable:[AOFLoginManager sharedInstance].upi forKey:@"upi"];
//        [params setObjectIfValueAvailable:franchiseId forKey:@"sf"];
//        [params setObjectIfValueAvailable:spaceStructureId forKey:@"ssid"];
        
        //New Implementation for Token
        [params setObject:[AOFLoginManager sharedInstance].getAccessToken forKey:kTokenKey];
        
        if ([date longLongValue] > 0) {
            [params setObject:date forKey:@"date"];
        }
        
        NSString *url = [self getServiceResource:@"statistics.uri"];
        
        [self performBlock:^(CallbackBlock callback) {
            
            [[self requestManagerWithToken:YES headers:nil] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                    callback(errorCode, nil);
                }
                
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS statistics.uri", (long)error.code, error.localizedDescription]];
                if (error.code == 107) {
                    onError([self getErrorWithCode:107
                                  alternateMessage:@"The aplication is unavailible at this moment."]);
                }else if (error.code == 401) {
                    onError([self getErrorWithCode:401
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }
                else {
                    onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
            }else{
                if (response) {
                    
                    NSDictionary *responseObject = (NSDictionary *)response;
                    NSDictionary *assets = [responseObject objectForKey:@"assets"];
                    if ([assets count]) {
                        
                        for (id key in assets) {
                            id value = [assets objectForKey:key];
                            
                            if ([value isKindOfClass:[NSDictionary class]]) {
                                
                                NSDictionary *stats = (NSDictionary*)value;
                                
                                if (stats) {
                                    
                                    if ([stats objectForKey:@"nid"]) {
                                        
                                        NSArray * assetArray = [[ZMCDManager sharedInstance] getAssetArrayById:[[stats objectForKey:@"nid"] stringValue]];
                                        if (assetArray.count > 0) {
                                            
                                            Asset * assetCache = assetArray[0];
                                            
                                            if ([stats objectForKey:@"views"]) {
                                                NSNumber *viewCount = [NSNumber numberWithInt:[[stats objectForKey:@"views"] intValue]];
                                                assetCache.view_count = viewCount;
                                            }
                                            
                                            if ([stats objectForKey:@"votes"]) {
                                                NSNumber *votes = [NSNumber numberWithInt:[[stats objectForKey:@"votes"] intValue]];
                                                assetCache.votes = votes;
                                            }
                                            
                                            [[ZMCDManager sharedInstance]updateAsset:assetCache success:^{
                                                [[ZMAssetsLibrary defaultLibrary] notifyStatsChanged:assets];
                                            } error:^(NSError * error) {
                                                [AbbvieLogging logError:[NSString stringWithFormat:@"Error at updating staticts asset %@", error.localizedDescription]];
                                            }];
                                            
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                    
                    [[ZMAssetsLibrary defaultLibrary] setProperty:@"STATS_LASTUPDATE"
                                                            value:[NSString stringWithFormat:@"%ld",[[responseObject objectForKey:@"date"] longValue]]];
                    
                    complete();

                }
            }
        }];
    }
}


/**
 *  Synch categories from WS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */

- (void)getCategories:(void(^)(void))complete error:(void(^)(NSError *error))onError{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> SyncCMS calling: %s", __PRETTY_FUNCTION__]];
    if ([AOFLoginManager sharedInstance].upi) {
        
        //AOFKit Implementation
        NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
        NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"upi":[AOFLoginManager sharedInstance].upi,
                                                                                      @"sf":franchiseId,
                                                                                      @"ssid":spaceStructureId}];

        
        NSString *categoryLastUpdate = [[ZMAssetsLibrary defaultLibrary] property:@"CATEGORIES_LASTUPDATE"];
        NSArray * currentCategories = [[ZMCDManager sharedInstance]getCategories];
        
        if (categoryLastUpdate && !currentCategories) {
            [params setObject:categoryLastUpdate forKey:@"date"];
        }
        
        NSString *url = [self getServiceResource:@"category.uri"];
        
        
        [self performBlock:^(CallbackBlock callback) {
           
            [[self requestManagerWithToken:YES headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"resp of category.uri:%@",responseObject]];
                        callback(nil, responseObject);
                        
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                    callback(errorCode, nil);
                }
                
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS category.uri", (long)error.code, error.localizedDescription]];
                if (error.code == 107) {
                    onError([self getErrorWithCode:107
                                  alternateMessage:@"The aplication is unavailible at this moment."]);
                }else if (error.code == 401) {
                    onError([self getErrorWithCode:401
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }else {
                    onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
            }else{
                if (response) {
                    NSDictionary *categories = (NSDictionary *)response;
                    [[[BucketDetailDataControl alloc] init] saveBucketDetailFromCategories:categories];
                    [self getCategories:categories success:^(NSArray *categories) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"successed imported categories %lu", (unsigned long)categories.count]];
                        [ZMProntoManager sharedInstance].categoriesArray = categories;
                        complete();
                    } error:^(NSError *error) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"error at importing categories %@", error.localizedDescription]];
                        onError(error);
                    }];
                }
            }
        }];

    }
}

/**
 *  Sync Assets from CMS
 *
 *  @param complete Block complete
 *  @param progress Block to track process
 *  @param onError  Block error
 */


- (void)syncAssets:(void(^)(void))complete progress:(void(^)(float progressValue))progress error:(void(^)(NSError *error))onError{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> SyncCMS calling: %s", __PRETTY_FUNCTION__]];
    
    _initialDownloadDbArray = [[NSArray alloc] init];
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        assetsQueue = ^(void){
            
            if ([AOFLoginManager sharedInstance].upi) {
                
                NSString *lastUpdate = [[ProntoUserDefaults userDefaults] getCMSLastSynchronization];
                if ([lastUpdate isEqualToString:@"(null)"] || !lastUpdate || lastUpdate == nil || lastUpdate.length <= 0) {
                    lastUpdate = @"0";
                }
                
                
//                //AOFKit Implementation
//                NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//                NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
//
//                NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
//                                         @"sf":franchiseId,
//                                         @"ssid":spaceStructureId,
//                                         @"date":lastUpdate,
//                                         @"per_page":@"100",
//                                         @"page":[NSString stringWithFormat:@"%d", self._currentPage]};
                
                NSString* accessToken = [AOFLoginManager sharedInstance].getAccessToken;

                NSDictionary *params = @{kTokenKey:accessToken,
                                         @"date":lastUpdate,
                                         @"per_page":@"100",
                                         @"page":[NSString stringWithFormat:@"%d", self._currentPage]};
                
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"asset params:%@",params]];
                NSDictionary *headers = @{@"1.1":@"services_asset_getvocabulary_version", @"keep-alive":@"Connection"};
                
                NSString *url = [self getServiceResource:@"assets.uri"];
                
                self._syncingCompletion = NO;
                
                [self performBlock:^(CallbackBlock callback) {
                   
                    AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:headers];
                    NSOperationQueue *operationQueue = manager.operationQueue;
                    [manager.requestSerializer setTimeoutInterval:25];  //Time out after 25 seconds
                   // NSOperationQueue *operationQueue = manager.operationQueue;
                    
                    [manager.reachabilityManager startMonitoring];
                    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status)]];
                        switch (status) {
                            case AFNetworkReachabilityStatusReachableViaWWAN:
                            case AFNetworkReachabilityStatusReachableViaWiFi:
                                [operationQueue setSuspended:NO];
                                break;
                            case AFNetworkReachabilityStatusNotReachable:
                            default:
                                [operationQueue setSuspended:YES];
                                break;
                        }
                    }];
                    
                    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            if (callback) {

                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"resp of assets.uri:%@",responseObject]];
//                                NSLog(@"AssetV2Response:%@",responseObject);
                                callback(nil, responseObject);
                            }
                        });
                 
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        
                        if (callback) {
                            NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                            callback(errorCode, nil);
                            
                            self._syncingCompletion = YES;
                        }
                    }];
                    
                } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
                    
                    if (error) {
                        
                        [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS assets.uri", (long)error.code, error.localizedDescription]];

                        if (error.code == 107) {
                            onError([self getErrorWithCode:107
                                          alternateMessage:@"The aplication is unavailible at this moment."]);
                        }else if (error.code == 401) {
                            onError([self getErrorWithCode:401
                                          alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                        }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                            onError([self getErrorWithCode:error.code
                                          alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                        }else {
                            onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                        }
                    }else{
                        
                        NSDictionary *assets = (NSDictionary *)response;
                        
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Current Page %d, asset Page %d", self._currentPage, [[assets objectForKey:@"pages"] intValue]]];
                        
                        
                        if ([[assets objectForKey:@"pages"] integerValue] == 0) {
                            NSArray * assetsToDeleted = [assets valueForKey:@"deleted"];
                            NSArray * assetsToUnpublish = [assets valueForKey:@"unpublished"];
                            
                            if (assetsToDeleted.count > 0  || assetsToUnpublish.count > 0) {
                                
                                NSMutableArray * assetsToProcess = [[NSMutableArray alloc]init];
                                
                                for (id key in assetsToDeleted) {
                                    [assetsToProcess addObject:key];
                                }
                                for (id key in assetsToUnpublish) {
                                    [assetsToProcess addObject:key];
                                }
                                
                                [self processUnpublishOrDeleteAsset:assetsToProcess success:^{
                                    [[ZMAssetsLibrary defaultLibrary] notifyAssetsChanged];
                                    self._currentPage = 0;
                                    [ZMUserActions sharedInstance].enableSearch = YES;
                                    complete();
                                    return;
                                }];
                                
                            }else{
                                self._currentPage = 0;
                                complete();
                                return;
                            }
                        }
                        
                        float progressV = 0.0;
                        
                        if ([[assets objectForKey:@"pages"] integerValue] != 0){
                            progressV = (self._currentPage * 100) / [[assets objectForKey:@"pages"] integerValue];
                        }
                        
                        if (self._currentPage < [[assets objectForKey:@"pages"] integerValue]){
                            
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"count -> From server Assets -> getAssets %lu",(unsigned long)assets.count]];

                            [self getAssets:assets success:^(NSArray *assetsLoaded) {
                                
                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"successed imported assets %lu", (unsigned long)assetsLoaded.count]];
                                _totalAssets = (int)assetsLoaded.count;
                                self._currentPage ++;
                                progress(progressV);
                                assetsQueue();
                            } error:^(NSError *error) {
                                [AbbvieLogging logError:[NSString stringWithFormat:@"error at importing assets %@", error.localizedDescription]];
                            }];
                        }else if (self._currentPage == [[assets objectForKey:@"pages"] integerValue])
                        {
                            [[ProntoUserDefaults userDefaults] setCMSLastSynchronization:[assets objectForKey:@"changed"]];
                            progress(progressV);
                            complete();
                        }else{
                            complete();
                        }
                    }
                }];
            }else{
                onError([self getErrorWithCode:107 alternateMessage:@"Broken Session, Authentication Error."]);
            }
        };assetsQueue();
    }
}

-(void) offlineDownload: (NSMutableArray *)assetDownloadArray {
    for(Asset * asset in assetDownloadArray) {
        //change already present path
        if(asset.path != nil || ![asset.path  isEqual: @""]) {
            NSString *baseDir = AOFLoginManager.sharedInstance.documentsDir;
            NSString *filename = [self MD5HashForString:asset.uri_full];
            NSString *extension = [[asset.uri_full componentsSeparatedByString:@"."] lastObject];
            NSString *dest = [[NSString alloc] initWithFormat:@"%@/%@.%@", baseDir, filename, extension];
            asset.path = dest;
            
        }
        //check if asset needs download
        [self initialDownloadValidate:asset completion:^(BOOL assetNeedsDownload){
            if(assetNeedsDownload) {
                [AbbvieLogging logInfo:@"Needs Download"];
                
                //keep in track of an array of dictionary
                NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
                if(_initialDownloadAssetArray.count == 0) {
                    _initialDownloadAssetArray = [[NSMutableArray alloc]init];
                }
                BOOL check = NO;
                for(NSDictionary *dict in [_initialDownloadAssetArray copy])
                {
                    if([dict objectForKey:@"assetID"] == [asset valueForKey:@"assetID"])
                    {
                        check = YES;
                        break;
                    }
                }
                if(!check) {
                    [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                    [initialDownloadAsset setObject:@"in_progress" forKey:@"status"];
                    [_initialDownloadAssetArray addObject:initialDownloadAsset];
                    
                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                }
                
//                [self downloadAssetFor:asset];
                
            }
        } failure:^(NSString *messageFailure) {
        }];
    }
}

//Download call for assets
-(void) downloadAssetFor:(Asset *)asset {
    
    __weak ZMAbbvieAPI *weakSelf = self;
    [weakSelf initialDownloadValidate:asset completion:^(BOOL assetNeedsDownload)
    {
        if(assetNeedsDownload == YES)
        {
            NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
            //   Download in background
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [weakSelf downloadAsset:asset completion:^{
                    if([asset title] != nil){
                        
                        if([asset valueForKey:@"assetID"] != nil) {
                            [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                            [initialDownloadAsset setObject:@"completed" forKey:@"status"];
                            NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
                        
                            if(weakSelf.initialDownloadAssetArray.count > 0) {
                                for(NSDictionary *dict in [weakSelf.initialDownloadAssetArray copy])
                                {
                                    if([dict objectForKey:@"assetID"] == [asset valueForKey:@"assetID"]) {
                                        
                                        [weakSelf.initialDownloadAssetArray replaceObjectAtIndex:[weakSelf.initialDownloadAssetArray indexOfObject:dict] withObject:initialDownloadAsset];
                                        
                                    }
                                }
                            }
                            
                            [weakSelf initialDownloadValidate:asset completion:^(BOOL assetNeedsDownload){
                                if(assetNeedsDownload == YES) {
                                    [initialDownloadAsset setObject:@"failed" forKey:@"status"];
                                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Fail completion updated for: %@ %@",[asset valueForKey:@"assetID"],[asset title]]];
                                }
                                else
                                {
                                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download Complete: %@ %@, size is %@",[asset valueForKey:@"assetID"],[asset title],[asset file_size]]];
                                }
                                
                                if (offlineData.count > 0)
                                {
                                    //if  insertation and deletetion has to be happen, it will happen one by one
                                    [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                                        
                                        [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                             [self broadcastingToProvider:asset];
                                        }];
                                    }];
                                }
                                else
                                {
                                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                         [self broadcastingToProvider:asset];
                                    }];
                                }
                                
                            }failure:^(NSString *messageFailure) {
                                [AbbvieLogging logError:[NSString stringWithFormat:@"Updating in CD failed: %@ %@",[asset valueForKey:@"assetID"],[asset title]]];
                            }];
                        }
                    }
                }
                onError:^(NSError *error)  {

                     if([asset title] != nil)
                     {
                         [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download Failed: %@ %@",[asset valueForKey:@"assetID"],[asset title]]];
                         
                         if([asset valueForKey:@"assetID"] != nil) {
                             [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                             [initialDownloadAsset setObject:@"failed" forKey:@"status"];
                             NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
                             
                             if(weakSelf.initialDownloadAssetArray.count > 0) {
                                 for(NSDictionary *dict in [weakSelf.initialDownloadAssetArray copy])
                                 {
                                     if([dict objectForKey:@"assetID"] == [asset valueForKey:@"assetID"]) {
                                         
                                         [weakSelf.initialDownloadAssetArray replaceObjectAtIndex:[weakSelf.initialDownloadAssetArray indexOfObject:dict] withObject:initialDownloadAsset];
                                         
                                     }
                                 }
                             }
                             if (offlineData.count > 0)
                             {
                                 //if  insertation and deletetion has to be happen, it will happen one by one
                                 [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                                     [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                          [self broadcastingToProvider:asset];
                                     }];
                                 }];
                             }
                             else
                             {
                                 [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                      [self broadcastingToProvider:asset];
                                 }];
                             }
                         }
                     }
                 }];
            });
        }
        else
        {
            //updating core data
            NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
            [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
            [initialDownloadAsset setObject:@"completed" forKey:@"status"];
            NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];

            if (offlineData.count > 0)
            {
                //if  insertation and deletetion has to be happen, it will happen one by one
                [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                         [self broadcastingToProvider:asset];
                    }];
                }];
            }
            else
            {
                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                     [self broadcastingToProvider:asset];
                }];
            }

        }
    }failure:^(NSString *messageFailure) {
    }];
}

-(void) downloadEventOnlyAsset:(EventOnlyAsset *)asset
{
    __weak ZMAbbvieAPI *weakSelf = self;
    [weakSelf initialDownloadValidateForEventOnlyAsset:asset completion:^(BOOL assetNeedsDownload)
     {
         if(assetNeedsDownload == YES)
         {
             NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
             //   Download in background
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                 
                 [weakSelf downloadEventOnlyAsset:asset withProgress:^(CGFloat progress) {
                     //[AbbvieLogging logInfo:[NSString stringWithFormat:@"Progress for EOA offline download:%f",progress]];
                 } completion:^{
                     if(asset.eoa_title != nil)
                     {
                         if(asset.eoa_id != nil)
                         {
                             NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[NSNumber numberWithInteger:[asset.eoa_id integerValue]]];

                             [weakSelf initialDownloadValidateForEventOnlyAsset:asset completion:^(BOOL assetNeedsDownload){
                                 NSString *status = keyCompleted;
                                 if(assetNeedsDownload == YES)
                                 {
                                     status = keyFailed;
                                     [AbbvieLogging logInfo:[NSString stringWithFormat:@"Fail completion updated for: %@ %@",asset.eoa_id,asset.eoa_title]];
                                 }
                                 else
                                 {
                                     [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download Complete: %@ %@, size is %@",asset.eoa_id,asset.eoa_title,asset.eoa_file_size]];
                                 }
                                 
                                 if (offlineData.count > 0)
                                 {
                                     //if  insertation and deletetion has to be happen, it will happen one by one
                                     OfflineDownload *offlineAsset = (OfflineDownload*)[offlineData lastObject];
                                     offlineAsset.status = status;
                                     [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:^(BOOL success) {
                                         [self broadcastingToProviderForEventOnlyAsset:asset];
                                     }];
                                 }
                                 else
                                 {
                                     [initialDownloadAsset setObject:[NSNumber numberWithInteger:[asset.eoa_id integerValue]] forKey:@"assetID"];
                                     [initialDownloadAsset setObject:status forKey:@"status"];
                                     [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                         [self broadcastingToProviderForEventOnlyAsset:asset];
                                     }];
                                 }
                                 
                             }failure:^(NSString *messageFailure) {
                                 [AbbvieLogging logError:[NSString stringWithFormat:@"Updating in CD failed: %@ %@",asset.eoa_id,asset.eoa_title]];
                             }];
                         }
                     }
                 } onError:^(NSError *error) {
                     
                     if(asset.eoa_title != nil)
                     {
                         [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download Failed: %@ %@",asset.eoa_id,asset.eoa_title]];
                         
                         if(asset.eoa_id != nil)
                         {
                             NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[NSNumber numberWithInteger:[asset.eoa_id integerValue]]];
                             
                             if (offlineData.count > 0)
                             {
                                 //if  insertation and deletetion has to be happen, it will happen one by one
                                 OfflineDownload *offlineAsset = (OfflineDownload*)[offlineData lastObject];
                                 offlineAsset.status = keyFailed;
                                 [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:^(BOOL success) {
                                     [self broadcastingToProviderForEventOnlyAsset:asset];
                                 }];
                             }
                             else
                             {
                                 [initialDownloadAsset setObject:[NSNumber numberWithInteger:[asset.eoa_id integerValue]] forKey:@"assetID"];
                                 [initialDownloadAsset setObject:keyFailed forKey:@"status"];
                                 [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                     [self broadcastingToProviderForEventOnlyAsset:asset];
                                 }];
                             }
                         }
                     }
                 }];
             });
         }
         else
         {
             //updating core data
             
             NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[NSNumber numberWithInteger:[asset.eoa_id integerValue]]];
             
             if (offlineData.count > 0)
             {
                 //if  insertation and deletetion has to be happen, it will happen one by one
                 OfflineDownload *offlineAsset = (OfflineDownload*)[offlineData lastObject];
                 offlineAsset.status = keyCompleted;
                 [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:^(BOOL success) {
                     [self broadcastingToProviderForEventOnlyAsset:asset];
                 }];
             }
             else
             {
                 NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
                 [initialDownloadAsset setObject:[NSNumber numberWithInteger:[asset.eoa_id integerValue]] forKey:@"assetID"];
                 [initialDownloadAsset setObject:keyCompleted forKey:@"status"];
                 [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                     [self broadcastingToProviderForEventOnlyAsset:asset];
                 }];
             }
             
         }
     }failure:^(NSString *messageFailure) {
     }];
}

//Assets validate before the initial download
-(void)initialDownloadValidate:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    NSString * uri = [asset valueForKey:@"uri_full"];
    NSString * file_mime = [asset valueForKey:@"file_mime"];
    if (![UIApplication sharedApplication].protectedDataAvailable) {
        [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
        /// This should occur during application:didFinishLaunchingWithOptions:
        //AOFKit Implementation
        NSError *error;
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];
        
        
        if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
        {
            attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
            if (!success)
            [AbbvieLogging logError:@"Set ~/Documents attrsAssetPath NOT successfull"];
        }
        
        /// ....
        NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
        [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
        [AbbvieLogging logError:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
    }
    
    // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
    if (![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && [uri rangeOfString:@"brightcove://"].location == 0 && ![file_mime isEqualToString:@"weblink" ])
    {
        if ([self isConnectionAvailable]) {
            //                _isDownloadignAsset = YES;
            completion(YES);
        }
        else {
            //            _isDownloadignAsset = NO;
            [AbbvieLogging logError:@"The internet connection is not available, please check your network connectivity"];
        }
    }else{
        //Check if the asset size matches the metada info meaning the asset is correct
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"** size Asset %d size %d", asset.file_size.intValue, (int)fileSize]];
        if (asset.file_size.intValue == (int)fileSize) {
            //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
            completion(NO);
        }else{
            completion(YES);
        }
    }
}

//EventOnlyAssets validate before the initial download
-(void)initialDownloadValidateForEventOnlyAsset:(EventOnlyAsset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.eoa_path error:nil] fileSize];
    NSString * uri = asset.eoa_uri_full;
    NSString * file_mime = asset.eoa_file_mime;
    if (![UIApplication sharedApplication].protectedDataAvailable)
    {
        [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
        /// This should occur during application:didFinishLaunchingWithOptions:
        //AOFKit Implementation
        NSError *error;
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.eoa_path error:&error];
        
        
        if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
        {
            attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.eoa_path error:&error];
            if (!success)
                [AbbvieLogging logError:@"Set ~/Documents attrsAssetPath NOT successfull"];
        }
        
        /// ....
        NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
        [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.eoa_path error:nil];
        [AbbvieLogging logError:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
    }
    
    // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
    if (![[NSFileManager defaultManager] fileExistsAtPath:asset.eoa_path] && [uri rangeOfString:@"brightcove://"].location == 0 && ![file_mime isEqualToString:@"weblink" ])
    {
        if ([self isConnectionAvailable])
        {
            completion(YES);
        }
    }
    else
    {
        //Check if the asset size matches the metada info meaning the asset is correct
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"** size Asset %d size %d", asset.eoa_file_size.intValue, (int)fileSize]];
        if (asset.eoa_file_size.integerValue == (NSInteger)fileSize)
        {
            //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
            completion(NO);
        }
        else
        {
            completion(YES);
        }
    }
}


/**
 *  Search Assets from CMS - SOLR
 *
 *  @param complete Block complete
 *  @param progress Block to track process
 *  @param onError  Block error
 */


- (void)searchAssets:(void(^)(void))complete progress:(void(^)(float progressValue))progress successList:(void (^)(NSArray *))success error:(void(^)(NSError *error))onError{

    _counter = 0;
    if ([AOFLoginManager sharedInstance].upi) {
        
        assetsQueue = ^(void){
            
            if ([AOFLoginManager sharedInstance].upi && [[ZMProntoManager sharedInstance].search getTitle].length > 0)
            {
//                NSString *lastUpdate = [[ProntoUserDefaults userDefaults] getCMSLastSynchronization];
//                if ([lastUpdate isEqualToString:@"(null)"] || !lastUpdate || lastUpdate == nil || lastUpdate.length <= 0) {
//                    lastUpdate = @"0";
//                }
                
                //AOFKit Implementation
//                NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//                NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
                
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"searchKey:%@",[[ZMProntoManager sharedInstance].search getTitle]]];
                NSString* count_page = [NSString stringWithFormat:@"%@", @"60"];
                
                _searchKey = [[ZMProntoManager sharedInstance].search getTitle];
                
//                NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
//                                         @"sf":franchiseId,
//                                         @"ssid":spaceStructureId,
//                                         @"key":[[ZMProntoManager sharedInstance].search getTitle],
//                                         @"per_page": count_page,
//                                         @"page":[NSString stringWithFormat:@"%d", self._currentPage]
//                                         };
                
                //New Change for token
                NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
                                                       @"key":[[ZMProntoManager sharedInstance].search getTitle],
                                                       @"per_page": count_page,
                                                       @"page":[NSString stringWithFormat:@"%d", self._currentPage]
                                                       };
                
                [ZMUserActions sharedInstance].isServiceHitted = YES;

                [AbbvieLogging logInfo:[NSString stringWithFormat:@"search asset params:%@",params]];
                NSDictionary *headers = @{@"1.1":@"services_asset_getvocabulary_version", @"keep-alive":@"Connection"};
 
                
                 NSString *url = [self getServiceResource:@"search.uri"];
            
                
                [self performBlock:^(CallbackBlock callback) {
                    
                    [ZMUserActions sharedInstance].isServiceHitted = YES;
                    [ZMUserActions sharedInstance].searchCompletion = NO;
                    
                    AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:headers];
                    NSOperationQueue *operationQueue = manager.operationQueue;
                    
                    [manager.reachabilityManager startMonitoring];
                    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status)]];
                        switch (status) {
                            case AFNetworkReachabilityStatusReachableViaWWAN:
                            case AFNetworkReachabilityStatusReachableViaWiFi:
                                [operationQueue setSuspended:NO];
                                break;
                            case AFNetworkReachabilityStatusNotReachable:
                            default:
                                [operationQueue setSuspended:YES];
                                break;
                        }
                    }];
                 
                    
                    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            if (callback) {

                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"resp of search.uri for key %@:%@",[[ZMProntoManager sharedInstance].search getTitle],responseObject]];
                                
                                [ZMUserActions sharedInstance].isServiceSuccess = YES;
                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Search Resp:%@",responseObject]];
                                   callback(nil, responseObject);
                            }
                        });
                        
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            if (callback)
                            {
                                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                                callback(errorCode, nil);
                                [ZMUserActions sharedInstance].isFinalSearch = YES;
                            }
                        });
                    }];
                    
                } retryingNumberOfTimes:0 onCompletion:^(NSError *error, NSObject *response) {
                    
                    if (error)
                    {
                        
                        [ZMUserActions sharedInstance].searchCompletion = YES;
                        [ZMUserActions sharedInstance].isServiceHitted = NO;
                        
                        [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS search.uri", (long)error.code, error.localizedDescription]];
                        
                        if (error.code == 107) {
                            onError([self getErrorWithCode:107
                                          alternateMessage:@"The aplication is unavailible at this moment."]);
                        }else if (error.code == 401) {
                            onError([self getErrorWithCode:401
                                          alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                        }else if (error.code == -1005 || error.code == -1001 || error.code == -1004){
                            onError([self getErrorWithCode:error.code
                                          alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                        }else {
                            onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                        }
                    }
                    else
                    {
                        
                        
//                        NSDictionary *responseObject = (NSDictionary *)response;
                        NSDictionary *responseObjectWithEventsResponse = (NSDictionary *)response;
                        
                        // In plist response will be saved same as it's coming from CMS.
                        NSDictionary *copyOfResponseToStoreInPlist      = (NSDictionary *)response;
                        
                        
                        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        // check if the app received valid response
                    if (responseObjectWithEventsResponse != (NSDictionary*) [NSNull null] || ![responseObjectWithEventsResponse isKindOfClass:[NSNull class]])
                    {
                        
                        // Logic: First Get the values of Events from response and then remove
                        // events response from actual response. By doing so we don't need to
                        // touch actual implementation of Global search
                        //Events Response
                        NSDictionary *eventsResponse = responseObjectWithEventsResponse[@"events"];
                        if (eventsResponse != nil &&
                            [eventsResponse isKindOfClass:[NSDictionary class]] &&
                            eventsResponse.count > 0)
                        {
                            //While adding the element we need to know it's result for first time for particular search
                            //It's continous of previous search
                            
                            //If it is fresh search we need to add and need to remove the previous one.
                            //Otherwise we need to add it in previous one.
                            
                            if (self._currentPage == 0)
                            {
                                //It means fresh search, Remove previous one.
                                [[EventCoreDataManager sharedInstance] resetEventSearchedResults];
                                [[EventCoreDataManager sharedInstance] resetEventOnlyAssets];
                                [[EventCoreDataManager sharedInstance] resetAssets];
                            }
                            [[EventCoreDataManager sharedInstance] prepareGlobalSearchResultsForEvent:eventsResponse];
                        }
                        else if (eventsResponse != nil &&
                                 [eventsResponse isKindOfClass:[NSArray class]] &&
                                 eventsResponse.count <= 0 &&
                                 [responseObjectWithEventsResponse count] == 3 &&
                                 [responseObjectWithEventsResponse[@"pages"] integerValue] <= 0 &&
                                 [responseObjectWithEventsResponse[@"total"] integerValue] <= 0)
                        {
                            //It means that we found zero element for events for particular search.
                            //Reset the previous searched values.
                            [[EventCoreDataManager sharedInstance] resetEventSearchedResults];
                            [[EventCoreDataManager sharedInstance] resetEventOnlyAssets];
                            [[EventCoreDataManager sharedInstance] resetAssets];
                        }
                    
                        NSMutableDictionary *responseObject = [NSMutableDictionary dictionaryWithDictionary:responseObjectWithEventsResponse];
                        
                        [responseObject removeObjectForKey:@"events"];
                        
                        
                        // keys from response
                        NSArray* assetsIdArray = [responseObject allKeys];
                        NSArray* assetValues = [responseObject allValues]; // from all values needs to remove events
                        
                        
                        //Send the EventResponse to Core Data Manager
                        //Get all eoa_asset from there and have one array of eoa_Assets
                        //and directly get it to GlobalSearch Data Model
                        //Find right place to reset it
                        
                        NSMutableDictionary *dictKeys = [[[NSMutableDictionary alloc] initWithObjects:assetValues forKeys:assetsIdArray]mutableCopy]; //create dict
                        
                        NSString* total = [dictKeys objectForKey:@"total"];
                        NSString* pages = [dictKeys objectForKey:@"pages"];
                        
                        // needs to remove, events from response
                            
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"Current Page %d, search asset Page %d", self._currentPage, [pages intValue]]];
                            
                            if(self._currentPage < [pages intValue])
                            {
                                [[ZMAssetsLibrary defaultLibrary] searchCompleted:NO];
                                [ZMUserActions sharedInstance].isFinalSearch = NO;
                            }
                            else
                            {
                                if(self._currentPage == [pages intValue])
                                {
                                    [[ZMAssetsLibrary defaultLibrary] searchCompleted:YES];
                                    
                                    [ZMUserActions sharedInstance].searchCompletion = YES;
                                    [ZMUserActions sharedInstance].isFinalSearch = YES;
                                    
                                }
                            }
                                
                           
                            
                        [ZMUserActions sharedInstance].searchCompletion = NO;
                    
                        // check if the response is only having this count or both count is "0"
                        if([dictKeys count] == 2 && [total intValue] == 0 && [pages intValue] == 0)
                        {
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"searchedCount -> %d",[ZMUserActions sharedInstance].searchedCount]];
                            [ZMUserActions sharedInstance].searchedCount = 0;
                            [ZMUserActions sharedInstance].assetsCount = 0;
                            _totalPages = [pages intValue];
                            
                            [ZMUserActions sharedInstance].searchCompletion = YES;
                            
                            [[ZMUserActions sharedInstance] setResultantAssets:nil];
                            
                            [[ZMAssetsLibrary defaultLibrary] searchCompleted:YES];
                            
                            complete();
                            
                            //Handling No search result found scenario
                            
                            return;
                            
                           
                        }
                        // check if the response is only having this count
                        else if([dictKeys count] == 2 && [total intValue] != 0 && [pages intValue] != 0)
                        {
                            [ZMUserActions sharedInstance].searchedCount = [total intValue];
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"searchedCount -> %d",[ZMUserActions sharedInstance].searchedCount]];
                            [ZMUserActions sharedInstance].searchCompletion = YES;
                            
                            _totalPages = [pages intValue];

                            [[ZMAssetsLibrary defaultLibrary] searchCompleted:YES];
                            
                            complete();
                            return;
                        }
                        else
                        {
                            
                            _counter = _counter + 1;
                            
                            if([pages intValue] > 0 &&  _counter == 1)
                            {
                                [ZMUserActions sharedInstance].totalSearchedAssets = [[NSMutableArray alloc] initWithCapacity:[total intValue]];
                                
                            }
                            else if([pages intValue] == 0 ||  _counter == 0)
                            {
                                [ZMUserActions sharedInstance].totalSearchedAssets = nil;
                                
                            }
                            
                            
                            [ZMUserActions sharedInstance].searchCompletion = NO;
                          
                            
                            dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                // check if the app received valid response
                                if (copyOfResponseToStoreInPlist != (NSDictionary*) [NSNull null] || ![copyOfResponseToStoreInPlist isKindOfClass:[NSNull class]])
                                {
                                    // check in plist
                                    [self storeLocally:copyOfResponseToStoreInPlist  filename:[NSString stringWithFormat:@"%@_%d",[[ZMProntoManager sharedInstance].search getTitle],self._currentPage]];
                                }
                            });
                      
                        
                        [dictKeys removeObjectForKey:@"total"]; //remove object
                        [dictKeys removeObjectForKey:@"pages"]; //remove object
                        // store the value and remove object for events//
                        
                        
                        NSMutableArray* fieldBrands = [[NSMutableArray alloc] initWithCapacity:[[dictKeys allKeys] count]];
                            
                           
                            self.totalSearchedAssets = [[dictKeys allKeys] copy];
                            
                            [[ZMUserActions sharedInstance].totalSearchedAssets addObjectsFromArray:self.totalSearchedAssets];
                            
                           
                            [ZMUserActions sharedInstance].searchedKeys = [self removeDuplicates:[ZMUserActions sharedInstance].totalSearchedAssets];

                        [fieldBrands addObject:[[dictKeys allValues] valueForKey:@"field_brand"]];
                        
                        
                        NSArray* francArray =  [[[dictKeys allValues] valueForKey:@"field_brand"] valueForKey:@"franchises"];
                        
                        
                        for (NSDictionary *franchise in francArray)
                        {
                             if ([franchise isKindOfClass: [NSDictionary class]])
                             {
                            
                                 for (NSMutableDictionary *francKey in franchise)
                                 {
                                
                                     if ([francKey isKindOfClass: [NSMutableDictionary class]])
                                     {
                                    
                                         NSString* updatedFrancId = [[NSString stringWithFormat:@"%@",[[francKey allValues] valueForKey:@"id"]] description];
                                
                                         NSString *newFrancId = [[updatedFrancId componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];

                                         [ZMUserActions sharedInstance].franchiseId = newFrancId;
                              
//                                         NSString* updatedFrancName = [[NSString stringWithFormat:@"%@",[[francKey allValues] valueForKey:@"name"]] description];

//                                         NSString *newFrancName = [[updatedFrancName componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
//
//                                         if([newFrancName hasPrefix:@""""])
//                                         {
//                                             newFrancName = [newFrancName substringFromIndex:1];
//                                         }
//
//                                //truncate first and last character
//                                         if([newFrancName hasSuffix:@""""])
//                                         {
//                                             newFrancName = [newFrancName substringToIndex:[newFrancName length]-1];
//
//                                         }
                                     }
                            
                                 }
                             }
                        }
                            
                            
                       
                        if([ZMUserActions sharedInstance].searchedKeys.count > 0)
                        {
                    
                        
                       [[ZMCDManager sharedInstance]getSearchedAssets:0 limit:-1 success:^(NSArray* totalAssets) {
                         
                            
                           totalAssets = [ZMUserActions sharedInstance].totalSearchedAssets;
                           [ZMUserActions sharedInstance].searchedCount = (int)totalAssets.count;
                           [AbbvieLogging logInfo:[NSString stringWithFormat:@"searchedCount -> %d",[ZMUserActions sharedInstance].searchedCount]];
                           
                           ZMProntoManager.sharedInstance.assetsIdToFilter = totalAssets;
                           
                           [AbbvieLogging logInfo:[NSString stringWithFormat:@"Current search Page %d, asset Page %d", self._currentPage, [pages intValue]]];
                           
                           _totalPages = [pages intValue];
                                
                                if ([pages integerValue] == 0)
                                {
                                    self._currentPage = 0;
                                    complete();
                                    return;
                                }
                          
                               
                                float progressV = 0.0;
                            
                            if ([pages integerValue] != 0)
                            {
                                progressV = (self._currentPage * 100) / [pages integerValue];
                            }
                            
                            if(self._currentPage < [pages integerValue])
                            {
                                
                                [self getSearchedAssets:[ZMUserActions sharedInstance].searchedKeys success:^(NSArray * assetsLoaded) {
                                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"successed imported searched assets1 %lu", (unsigned long)assetsLoaded.count]];
                                    ZMProntoManager.sharedInstance.assetsIdToFilter = assetsLoaded;
                                    
                                    // check this has been updated
                                    [ZMUserActions sharedInstance].updatedAssets = NO;
                                    
                                    
                                    [[ZMUserActions sharedInstance] setResultantAssets:
                                     [self sortedArray:[ZMUserActions sharedInstance].sortingKey andArray:[NSMutableArray arrayWithArray:assetsLoaded]]];
                                    
                                    
                                    self._currentPage ++;
                                    progress(progressV);
                                    assetsQueue();
                                    success(assetsLoaded);
                                    
                                    
                                    
                                } error:^(NSError *error) {
                                    [AbbvieLogging logError:[NSString stringWithFormat:@"error at importing assets %@", error.localizedDescription]];
                                }];
                        }
                   
                        else if (self._currentPage == [pages integerValue])
                        {
                            progress(progressV);
                            complete();
                        }
                        else
                        {
                            complete();
                        }
                           
                        }];
                    }
                    
                    }
                            
                    }
                    
                        
                    });
                    }
                }];
                
                
            }
            else
            {
                onError([self getErrorWithCode:107 alternateMessage:@"Broken Session, searching Error."]);
            }
        };assetsQueue();
    }
}


/**
 *  New Revamping changes - get promoted asset ids from CMS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */

- (void)getPromotedContent:(NSString*)franchiseId complete:(void(^)(NSDictionary *promotedAssets))complete error:(void(^)(NSError *error))onError{
    if ([AOFLoginManager sharedInstance].upi) {
        
         [self performBlock:^(CallbackBlock callback) {
             
//    NSString* salesFranchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//    NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
        
        NSString *url = [NSString stringWithFormat:@"%@?token=%@&franchise=%@",[self getServiceResource:@"promoted.uri"],[AOFLoginManager sharedInstance].getAccessToken,franchiseId];
        [[self requestManagerWithToken:NO headers:nil] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                    
                }
            });
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
            
        }];
             
         } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
        
          if (error) {
              [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS Promoted uri", (long)error.code, error.localizedDescription]];
              
              if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004 || error.code ==-1003 || error.code ==-1011 || error.code ==1011 || error.code ==-1200)
              {
                  onError([self getErrorWithCode:error.code
                                alternateMessage:@"Server connection is not available for displaying promoted content, please try again later."]);
              }
              
              [ZMUserActions sharedInstance].isPromotedLoaded = NO;
              
          }else{
              [AbbvieLogging logInfo:[NSString stringWithFormat:@"promoted WS Response: %@", response]];
              [ZMUserActions sharedInstance].isPromotedLoaded = YES;
               complete((NSDictionary *)response);
              
          }
      }];
        
    }
}

///**
// *  New Revamping changes - send feedback details to CMS
// *
// *  @param complete Block complete
// *  @param onError  Block error
// */
//-(void)sendFeedback:(NSString *)assetId type:(NSString*)feedbackType comments:(NSString*)feedbackComments success:(void (^)(NSString *))success error:(void (^)(NSError *))onError{
//    
//    if ([AOFLoginManager sharedInstance].upi) {
//        
//        // From Profile, app should send only "App" type
//        if(!(assetId.length > 0) || [assetId isEqualToString:@"0"])
//        {
//            assetId = @"0";
//            feedbackType = @"App";
//        }
//        
////        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"nid":assetId,
////                                                                                      @"type":feedbackType,
////                                                                                      @"upi":[AOFLoginManager sharedInstance].upi,
////                                                                                      @"feedback":feedbackComments
////                                                                                      }];
//        //New Change for Token
//        
//        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"nid":assetId,
//                                                                                             @"type":feedbackType,
//                                                                                             kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
//                                                                                             @"feedback":feedbackComments
//                                                                                             }];
//        NSString *url = [self getServiceResource:@"feedback.uri"];
//        
//        [[self requestManagerWithToken:NO headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            
//            [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS feedback Response: %@", responseObject]];
//            
//            success((NSString *)success);
//            
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            
//            [AbbvieLogging logError:[NSString stringWithFormat:@"feedback WS Error: %@", error]];
//            
//        }];
//    }
//    
//}

/**
 *  New Revamping changes - send feedback details to CMS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */
-(void)sendEmailToself:(NSString *)assetId success:(void (^)(NSDictionary *))success error:(void (^)(NSError *))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {
        
//        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"aid":assetId,
//                                                                                      @"upi":[AOFLoginManager sharedInstance].upi
//                                                                                      }];
        
        //Changes for token
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"aid":assetId,
        kTokenKey:[AOFLoginManager sharedInstance].getAccessToken
        }];
        NSString *url = [self getServiceResource:@"sendmail.uri"];
        
        [[self requestManagerWithToken:NO headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS sendmail Response: %@", responseObject]];
            
            success((NSDictionary *)success);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [AbbvieLogging logError:[NSString stringWithFormat:@"sendmail WS Error: %@", error]];
            
        }];
    }
    
}

/**
 *  getFranchises
 *  @return array of franchises
 */
-(NSArray *) getFranchises
{
    ZMAssetsGroup *franchise = [[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:@"Franchise" uid:0 corporate_id:0 related:nil];
    NSArray *searchedFranchises;
    
    if((franchise.uid > 0) || (franchise.uid<0)){
        searchedFranchises = [[ZMAssetsLibrary defaultLibrary] groups];
    }
    else {
        searchedFranchises = [[ZMAssetsLibrary defaultLibrary] groupsWithRelatedUID:0];
    }
    return searchedFranchises;
}


/**
 *  Get the brands with franchise id and franchise name
 *  @param uid
 *  @param franchiseName
 *  @return
 */
-(NSArray *) getBrandsWithFranchiseUid:(long)uid andFranchiseName:(NSString *)franchiseName
{
    NSArray *searchedBrands;
    NSMutableArray *filters = [[NSMutableArray alloc] init];
    if(![franchiseName isEqualToString:@""])
    {
        NSArray *foundFranchises = [[ZMAssetsLibrary defaultLibrary] franchiseWithName:franchiseName];
        ZMAssetsGroup *brands;
        if([foundFranchises count] > 0)
        {
            for(ZMAssetsGroup *franchiseResult in foundFranchises)
            {
                if([franchiseResult.name isEqualToString:franchiseName])
                {
                    brands = [[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:@"Brands" uid:franchiseResult.uid corporate_id:0 related:nil];
                    [filters addObject:brands];
                } else {
                    brands = [[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:@"Brands" uid:-1 corporate_id:0 related:nil];
                    [filters addObject:brands];
                }
                searchedBrands = [self getBrands:brands withUID:uid];
            }
        }
        else
        {
            ZMAssetsGroup *brands = [[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:@"Brands" uid:-1 corporate_id:-1 related:nil];
            [filters addObject:brands];
            searchedBrands = [self getBrands:brands withUID:uid];
        }
        
    }
    return searchedBrands;
}


/**
 *  Gets the brands with the given filter and id
 *  @param filter filter description
 *  @param uid
 *  @return returns the found brands
 */
-(NSArray *) getBrands:(ZMAssetsGroup *) filter withUID:(long)uid
{
    NSArray *searchedFranchises;
    if((filter.uid > 0) || (filter.uid<0))
    {
        searchedFranchises = [[ZMAssetsLibrary defaultLibrary] groupsWithRelatedUID:uid];
    } else {
        searchedFranchises = [[ZMAssetsLibrary defaultLibrary] groupsWithRelatedUID:0];
    }
    return searchedFranchises;
}


//- (void)markNotificationsAsRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError {
//    
//    //AOFKit Implementation
//    
//    NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//    NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
//    NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
//                             @"sf":franchiseId,
//                             @"ssid":spaceStructureId};
//
//    
//    NSString *url = [NSString stringWithFormat:@"%@/%d", [self getServiceResource:@"notification.uri"], notificationId];
//    
//    [[self requestManagerWithToken:YES headers:nil] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS Response: %@", responseObject]];
//        complete((NSDictionary *)responseObject);
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        [AbbvieLogging logError:[NSString stringWithFormat:@"Notification WS Error: %@", error]];
//        
//        onError([self getErrorWithCode:108 alternateMessage:@"markNotificationsAsRead situation"]);
//    }];
//}
//
//- (void)markNotificationsAsUNRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError {
//    
//    //AOFKit Implementation
//    NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//    NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
//    
//    NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
//                             @"sf":franchiseId,
//                             @"ssid":spaceStructureId};
//
//    
//    NSString *url = [NSString stringWithFormat:@"services/privatemsg_unread/%d", notificationId];
//    
//    [[self requestManagerWithToken:YES headers:nil] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        complete((NSDictionary *)responseObject);
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        [AbbvieLogging logError:[NSString stringWithFormat:@"markNotificationsAsRead WS Error: %@", error]];
//        
//        onError([self getErrorWithCode:108 alternateMessage:@"markNotificationsAsRead situation"]);
//    }];
//}





- (void)registerToken:(NSString *)deviceToken complete:(void(^)(void))complete error:(void(^)(NSError *error))onError {
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        //AOFKit Implementation
        
//        NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//        NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
//        NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
//                                 @"sf":franchiseId,
//                                 @"ssid":spaceStructureId,
//                                 @"token": deviceToken,
//                                 @"type":@"ios"};
        
        //After Changes
        NSDictionary *params = @{@"accessToken":[AOFLoginManager sharedInstance].getAccessToken,
        @"token": deviceToken,
        @"type":@"ios"};
        
        NSString *url = [self getServiceResource:@"apssave.uri"];
        
        [[self requestManagerWithToken:YES headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            complete();
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            onError([self getErrorWithCode:000 alternateMessage:@"APNS Token couldn't be registered"]);
        }];
    }
}

- (void)unRegisterToken:(NSString *)deviceToken complete:(void(^)(void))complete error:(void(^)(NSError *error))onError {
    
    //AOFKit Implementation

//    NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//    NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;

//    NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
//                             @"sf":franchiseId,
//                             @"ssid":spaceStructureId,
//                             @"token": deviceToken};

    //After token changes
    NSDictionary *params = @{@"accessToken":[AOFLoginManager sharedInstance].getAccessToken,
                                @"token": deviceToken};
    
    NSString *url = [self getServiceResource:@"apsdelete.uri"];
    
    [[self requestManagerWithToken:YES headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        complete();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        onError([self getErrorWithCode:000 alternateMessage:@"APNS Token couldn't be UNRegistered"]);
    }];
}

//- (void)unRegisterTokenNoSession:(NSString *)deviceToken complete:(void (^)(void))complete error:(void (^)(NSError *))onError {
//    
//    NSString *url = [NSString stringWithFormat:@"services/push_notification_token/%@", deviceToken];
//    
//    [[self requestManagerWithToken:YES headers:nil] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        complete();
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        onError([self getErrorWithCode:000 alternateMessage:@"APNS Token couldn't be UNRegistered"]);
//    }];
//}

#pragma mark - Private

//added by Prathap: Now this reads the data from Config file automatically based on the scheme it is being build.
- (NSDictionary *)getServerConfig:(NSString*)key {
    NSString *file = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Pronto-Config.plist"];
    return [[[NSMutableDictionary alloc] initWithContentsOfFile:file] objectForKey:key];
}

- (NSString *)getServiceResource:(NSString*)root {
    
    NSString *file = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Pronto-Config.plist"];
    NSMutableDictionary *config = [NSMutableDictionary dictionaryWithContentsOfFile:file];
    NSString *valueRoot = [config objectForKey:root];
    
    return valueRoot;
}

/**
 *  Load user info from Security Framework
 *
 *  @return Dictionary with user data
 */
- (NSDictionary *) loadUserData {
    
    //AOFKit Implementation
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:[AOFLoginManager sharedInstance].adDomain forKey:@"adDomain"];
    [data setValue:[AOFLoginManager sharedInstance].adLogon forKey:@"adLogon"];
    [data setValue:[AOFLoginManager sharedInstance].country forKey:@"c"];
    [data setValue:[AOFLoginManager sharedInstance].company forKey:@"company"];
    [data setValue:[(NSString*)[AOFLoginManager sharedInstance].department urlEncode] forKey:@"department"];
    [data setValue:[AOFLoginManager sharedInstance].displayName forKey:@"displayName"];
    [data setValue:[AOFLoginManager sharedInstance].divisionName forKey:@"division"];
    [data setValue:[AOFLoginManager sharedInstance].divisionCode forKey:@"divisionCode"];
    [data setValue:[AOFLoginManager sharedInstance].divisionName forKey:@"divisionName"];
    [data setValue:[AOFLoginManager sharedInstance].employeeType forKey:@"employeeType"];
    [data setValue:[AOFLoginManager sharedInstance].displayName forKey:@"fullName"];
    [data setValue:[AOFLoginManager sharedInstance].givenName forKey:@"givenName"];
    [data setValue:[AOFLoginManager sharedInstance].initials forKey:@"initial"];
    [data setValue:[(NSString*)[AOFLoginManager sharedInstance].location urlEncode] forKey:@"location"];
    [data setValue:[AOFLoginManager sharedInstance].mail forKey:@"mail"];
    [data setValue:[AOFLoginManager sharedInstance].managerDisplayName forKey:@"managerDisplayName"];
    [data setValue:[AOFLoginManager sharedInstance].managerEmail forKey:@"managerEmail"];
    [data setValue:[AOFLoginManager sharedInstance].managerUPI forKey:@"managerUPI"];
    [data setValue:[AOFLoginManager sharedInstance].preferredLanguage forKey:@"preferredLanguage"];
    [data setValue:[(NSString*)[AOFLoginManager sharedInstance].salesArea urlEncode] forKey:@"salesArea"];
    [data setValue:[AOFLoginManager sharedInstance].salesCategoryID forKey:@"salesCategoryID"];
    [data setValue:[AOFLoginManager sharedInstance].salesDistrict forKey:@"salesDistrict"];
    [data setValue:[(NSString*)[AOFLoginManager sharedInstance].salesForceName urlEncode] forKey:@"salesForceName"],
    [data setValue:[AOFLoginManager sharedInstance].salesForceNumber forKey:@"salesForceNumber"],
    [data setValue:[AOFLoginManager sharedInstance].salesFranchise forKey:@"salesFranchise"],
    [data setValue:[AOFLoginManager sharedInstance].salesFranchiseID forKey:@"salesFranchiseID"],
    [data setValue:[AOFLoginManager sharedInstance].salesPMI forKey:@"salesPMI"];
    [data setValue:[AOFLoginManager sharedInstance].salesRegion forKey:@"salesRegion"];
    [data setValue:[AOFLoginManager sharedInstance].salesSampling forKey:@"salesSampling"];
    [data setValue:[AOFLoginManager sharedInstance].sn  forKey:@"sn"];
    [data setValue:[AOFLoginManager sharedInstance].spaceStructureID forKey:@"spaceStructureID"];
    [data setValue:[AOFLoginManager sharedInstance].telephoneNumber forKey:@"telephoneNumber"];
    [data setValue:[AOFLoginManager sharedInstance].territory forKey:@"territory"];
    [data setValue:[(NSString*)[AOFLoginManager sharedInstance].title urlEncode] forKey:@"title"];
    [data setValue:[AOFLoginManager sharedInstance].upi forKey:@"upi"];
    [data setValue:@"" forKey:@"sessionIndex"];
    [data setValue:@"" forKey:@"subjectNameID"];
    [data setValue:[AOFLoginManager sharedInstance].salesTerritoryTypeID forKey:@"salesTerritoryTypeID"];
    [data setValue:[AOFLoginManager sharedInstance].salesSalesForce forKey:@"salesSalesForce"];
    
    if([AOFLoginManager sharedInstance].salesFranchiseID.length > 0)
    {
        [ZMUserActions sharedInstance].isFieldUser = YES;
    }
    else
    {
        [ZMUserActions sharedInstance].isFieldUser = NO;
    }
    
    return data;
}

- (NSError *)getErrorWithCode:(NSInteger)code alternateMessage:(NSString *)aMessage {
    
    NSString *domain = @"com.pronto.security.library";
    NSString *messageString = [NSString stringWithFormat:@"ERROR_CODE_%ld", (long)code];
    NSString *desc = NSLocalizedString(messageString, aMessage);
    NSDictionary *info = @{ NSLocalizedDescriptionKey : desc };
    
    [AbbvieLogging logError:[NSString stringWithFormat:@"actual error code:%@",messageString]];
    return [NSError errorWithDomain:domain code:code userInfo:info];
}

- (NSDictionary *)currentFolders{
    
    NSMutableArray *folders = [NSMutableArray arrayWithArray:[[ZMAssetsLibrary defaultLibrary] getAllFolders:TRUE]];
    [folders addObject:[[ZMAssetsLibrary defaultLibrary] folderWithId:0]];
    
    NSString *folderDate = [self getFolderDate];
    
    NSMutableArray *JSONArray =[NSMutableArray array];
    
    if ([folders count] > 0) {
        
        for(ZMFolder *folder in folders){
            
            NSMutableDictionary *folderAssets = [NSMutableDictionary dictionary];
            for(id key in folder.asset_list){
                NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:[[folder.asset_list objectForKey:key]stringValue]];
                if (assetArray.count > 0) {
                    Asset *asset = assetArray[0];
                    [folderAssets setObject:asset.title forKey:[NSString stringWithFormat:@"%ld",asset.assetID.longValue]];
                }
            }
            NSDictionary *folderToSend = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [NSString stringWithFormat:@"%ld",folder.fldr_id], @"id",
                                          folder.fldr_name, @"name",
                                          [NSString stringWithFormat:@"%d",folder.fldr_qty], @"qty",
                                          [NSString stringWithFormat:@"%ld",folder.fldr_date_created], @"date",
                                          [NSString stringWithFormat:@"%d",folder.fldr_is_briefcase], @"briefcase",
                                          folderAssets,@"assets",
                                          folder.metadata,@"metadata", nil];
            [JSONArray addObject:folderToSend];
            
        }
    }
    
    NSDictionary *dicJSON = [NSDictionary dictionaryWithObjectsAndKeys:@"-1", @"briefcase_date", JSONArray, @"briefcase_data", nil];
    

    if (folderDate) {
        
        dicJSON = [NSDictionary dictionaryWithObjectsAndKeys:folderDate, @"briefcase_date", JSONArray,@"briefcase_data", nil];
    }
    
  
    return dicJSON;
}

- (NSString *)getFolderDate{
    
    NSString *folderDate = [[ZMAssetsLibrary defaultLibrary] property:@"FOLDERS_LASTUPDATE"];
    
    if ((long)[folderDate longLongValue] > (long)[[NSDate date] timeIntervalSince1970]){
        folderDate = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
    }
    
    return folderDate;
}

- (BOOL)processAssetsResponse:(NSDictionary *)assets {
    
    BOOL assetsChanged = NO;
    
    for (id item in assets) {
        
        if ([item isEqualToString:@"deleted"] || [item isEqualToString:@"unpublished"]) {
            
            NSArray *deleted = [assets objectForKey:item];
            for (id assetId in deleted) {
                
                ZMAsset *assetToDelete = [[ZMAssetsLibrary defaultLibrary] assetWithUID:[assetId intValue]];
                NSError *error;
                
                if (assetToDelete.path) {
                    
                    if ([[NSFileManager defaultManager] fileExistsAtPath:assetToDelete.path]) {
                        
                        if ([[NSFileManager defaultManager] removeItemAtPath:assetToDelete.path error:&error] == YES) {
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"Deleted %@",assetToDelete.path]];
                        }
                        else {
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"Unable to delete %@,%@",assetToDelete.path,error]];
                        }
                    }
                }
                assetsChanged = YES;
                [[ZMAssetsLibrary defaultLibrary] removeAssetWithUID:assetToDelete.uid];
            }
        }
    }
    
    return assetsChanged;
}



/**
 * check if theres a internet connection available
 */
- (BOOL)isConnectionAvailable {
    
    SCNetworkReachabilityFlags flags;
    BOOL receivedFlags;
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [@"google.com" UTF8String]);
    receivedFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    
    if (!receivedFlags || (flags == 0)) {
        return FALSE;
    }
    
    return TRUE;
}

/**
 *  Download asset inform progress, this method using AFHTTPRequestOperation to tracking progres. Each time a error comes, the request will try execute itself max 3 times.
 *
 *  @param asset           Asset to download
 *  @param progressBlock   Progress block
 *  @param completionBlock Complete block
 *  @param errorBlock      Error block
 */
- (void) downloadAsset: (Asset *) asset
                 withProgress:(void (^)(CGFloat progress))progressBlock
                   completion:(void (^)(void)) completionBlock
                      onError:(void (^)(NSError *error))errorBlock {

    NSString *baseDir = AOFLoginManager.sharedInstance.documentsDir;
    NSString *filename = [self MD5HashForString:asset.uri_full];
    NSString *extension = [[asset.uri_full componentsSeparatedByString:@"."] lastObject];
    NSString *dest = [[NSString alloc] initWithFormat:@"%@/%@.%@", baseDir, filename, extension];
    
    NSURL *assetURL;
    // for testing in staging
    if(nil != asset.uri_full && ([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame))
    {
        assetURL = [[NSURL alloc] initWithString:asset.uri_full];
    }
    else if(nil != asset.uri_full && ([extension isEqualToString:@"mp3"] || [extension caseInsensitiveCompare:@"mp3"] == NSOrderedSame))
    {
        assetURL = [[NSURL alloc] initWithString:asset.uri_full];
    }
    else
    {
        assetURL = [[NSURL alloc] initWithString:@"https://stage.dlpronto.com/system/files/assets/URL-Text-Feb19.pdf"];
        extension = @"pdf";
    }
    
    // PRONTO-29 - Fix Crashes - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: sccollateralname)'
    if([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame)
        asset.path = dest;
    else if([extension isEqualToString:@"mp3"] || [extension caseInsensitiveCompare:@"mp3"] == NSOrderedSame)
        asset.path = dest;
    
//    __block NSString * range = [[NSString alloc]init];
    
    /// This should occur during application:didFinishLaunchingWithOptions: [ASFFileProtectionManager stopProtectingLocation:path];
    NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
    [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:dest error:nil];
    
    
    [self performBlock:^(CallbackBlock callback) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:assetURL];
        [request setTimeoutInterval:60];
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
            if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
            {
                [request setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
            else{
            [request setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:dest append:NO]];
        
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            CGFloat written = totalBytesRead;
            CGFloat total = totalBytesExpectedToRead;
            CGFloat percentageCompleted = written/total;
            progressBlock(percentageCompleted);
        }];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
        }];
        
        [operation.outputStream open];
        [operation start];
        
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
      
        if (error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"ERR: %@", [error description]]];
            errorBlock(error);
        } else {
            if (dest) {
                if (asset) {
                    [self initialDownloadValidate:asset completion:^(BOOL assetNeedsDownload){
                        if(!assetNeedsDownload) {
                            asset.path = dest;
                            [[ZMCDManager sharedInstance] updateAsset:asset success:^{
                                completionBlock();
                            } error:^(NSError * error) {
                                [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset %@", error.localizedDescription]];
                            }];
                        }
                        else
                        {
                            errorBlock(error);
                        }
                    }failure:^(NSString *messageFailure) {
                        errorBlock(error);
                    }];
                }
            }
        }
    }];

}



/**
 *  Download eventOnlyAsset inform progress, this method using AFHTTPRequestOperation to tracking progres. Each time a error comes, the request will try execute itself max 3 times.
 *
 *  @param asset           EventOnlyAsset to download
 *  @param progressBlock   Progress block
 *  @param completionBlock Complete block
 *  @param errorBlock      Error block
 */
- (void) downloadEventOnlyAsset: (EventOnlyAsset *) eventOnlyAsset
          withProgress:(void (^)(CGFloat progress))progressBlock
            completion:(void (^)(void)) completionBlock
               onError:(void (^)(NSError *error))errorBlock
{
    NSString *baseDir = AOFLoginManager.sharedInstance.documentsDir;
    NSString *filename = [self MD5HashForString:eventOnlyAsset.eoa_uri_full];
    NSString *extension = [[eventOnlyAsset.eoa_uri_full componentsSeparatedByString:@"."] lastObject];
    NSString *dest = [[NSString alloc] initWithFormat:@"%@/%@.%@", baseDir, filename, extension];
    
    NSURL *assetURL;
    // for testing in staging
    if(nil != eventOnlyAsset.eoa_uri_full && ([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame))
    {
        assetURL = [[NSURL alloc] initWithString:eventOnlyAsset.eoa_uri_full];
    }
    else
    {
        assetURL = [[NSURL alloc] initWithString:@"https://stage.dlpronto.com/system/files/assets/URL-Text-Feb19.pdf"];
        extension = @"pdf";
    }
    
    // PRONTO-29 - Fix Crashes - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: sccollateralname)'
    if([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame)
    {
        eventOnlyAsset.eoa_path = dest;
    }
    
    //    __block NSString * range = [[NSString alloc]init];
    
    /// This should occur during application:didFinishLaunchingWithOptions: [ASFFileProtectionManager stopProtectingLocation:path];
    NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
    [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:dest error:nil];
    
    
    [self performBlock:^(CallbackBlock callback) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:assetURL];
        [request setTimeoutInterval:60];
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
            if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
            {
                [request setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
            else{
            [request setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:dest append:NO]];
        
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            CGFloat written = totalBytesRead;
            CGFloat total = totalBytesExpectedToRead;
            CGFloat percentageCompleted = written/total;
            progressBlock(percentageCompleted);
        }];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
        }];
        
        [operation.outputStream open];
        [operation start];
        
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
        
        if (error)
        {
            [AbbvieLogging logError:[NSString stringWithFormat:@"ERR: %@", [error description]]];
            errorBlock(error);
        }
        else
        {
            if (dest)
            {
                if (eventOnlyAsset)
                {
                    [self initialDownloadValidateForEventOnlyAsset:eventOnlyAsset completion:^(BOOL assetNeedsDownload){
                        if(!assetNeedsDownload)
                        {
                            [eventOnlyAsset updateEventOnlyAssetPath:dest withCompletionHandler:^(BOOL success) {
                                 completionBlock();
                            }];
                        }
                        else
                        {
                            errorBlock(error);
                        }
                    }failure:^(NSString *messageFailure) {
                        errorBlock(error);
                    }];
                }
            }
        }
    }];
    
}


- (void)broadcastingToProvider:(Asset*)asset
{
    if(asset != nil && asset.assetID != nil)
    {
        NSDictionary* userInfo = @{@"asset": asset};
        [[NSNotificationCenter defaultCenter]postNotificationName:kAssetDownloadProviderCompletionNotification object:nil userInfo:userInfo];
    }
}

- (void)broadcastingToProviderForEventOnlyAsset:(EventOnlyAsset*)asset
{
    if(asset != nil && asset.eoa_id != nil)
    {
        NSDictionary* userInfo = @{@"asset": asset};
        [[NSNotificationCenter defaultCenter]postNotificationName:kEventOnlyAssetDownloadProviderCompletionNotification object:nil userInfo:userInfo];
    }
}

//Download assets for the first time

- (void) downloadAsset: (Asset *) asset
            completion:(void (^)(void)) completionBlock
               onError:(void (^)(NSError *error))errorBlock {
    
    NSString *baseDir = AOFLoginManager.sharedInstance.documentsDir;
    NSString *filename = [self MD5HashForString:asset.uri_full];
    NSString *extension = [[asset.uri_full componentsSeparatedByString:@"."] lastObject];
    NSString *dest = [[NSString alloc] initWithFormat:@"%@/%@.%@", baseDir, filename, extension];
    
    NSURL *assetURL;
    // for testing in staging
    if(nil != asset.uri_full && ([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame))
    {
        assetURL = [[NSURL alloc] initWithString:asset.uri_full];
    }
    /*else
    {
        
        NSString *str= [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"samplePDF"];
        assetURL = [[NSURL alloc] initWithString:str];
        
        //[[NSURL alloc] initWithString:@"https://stage.dlpronto.com/system/files/assets/URL-Text-Feb19.pdf"];
        extension = @"pdf";
    }*/
    //NSURL *assetURL = [[NSURL alloc] initWithString:asset.uri_full];
    
    // PRONTO-29 - Fix Crashes - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: sccollateralname)'
    //Nil check to avoid crash
    if(([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame) &&
       asset != nil && (asset.assetID != nil) )
    {
        asset.path = dest;
    }
    
    //    __block NSString * range = [[NSString alloc]init];
    
    /// This should occur during application:didFinishLaunchingWithOptions: [ASFFileProtectionManager stopProtectingLocation:path];
    NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
    [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:dest error:nil];
    
    
    [self performBlockForOfflineDownload:^(CallbackBlock callback) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:assetURL];
        [request setTimeoutInterval:60];
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
            if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
            {
                [request setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
            else{
            [request setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:dest append:NO]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
        }];
        
        [operation.outputStream open];
        [operation start];
        
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
        
        if (error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"ERR: %@", [error description]]];
            errorBlock(error);
        } else {
            if (dest) {
                if (asset && (asset.path != nil)) {
                    asset.path = dest;
                    //Removing update for offline download to avoid duplicates
                    //                    [[ZMCDManager sharedInstance] updateAsset:asset success:^{
                    completionBlock();
                    //                    } error:^(NSError * error) {
                    //                    }];
                }
            }
        }
    }];
    
}

-(void) downloadFileFromURL:(NSURL *)url toFilePath:(NSString *)filepath completionHandler:(void(^)(NSString *responsePath, NSError *error)) completionHandler
{
    [self performBlockForOfflineDownload:^(CallbackBlock callback) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
        [request setTimeoutInterval:60];
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
            if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
            {
                [request setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
            else{
            [request setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:filepath append:NO]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
        }];
        
        [operation.outputStream open];
        [operation start];
        
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
        
        if (error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"ERR: %@", [error description]]];
            completionHandler(@"",error);
        } else {
            completionHandler(@"",nil);
        }
    }];
}

#pragma mark - Public

- (NSString *)MD5HashForString:(NSString *)input {
    
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    
    // Convert NSString into C-string and generate MD5 Hash
    CC_MD5([input UTF8String], (int)[input length], result);
    
    // Convert C-string (the hash) into NSString
    NSMutableString *hash = [NSMutableString string];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [hash appendFormat:@"%02X", result[i]];
    }
    
    return [hash lowercaseString];
}

- (NSString *)getThumbnailPath:(NSString *)externalPath {
    
    NSString *path;
    NSString *basePath = [NSString stringWithFormat:@"%@/com.abbvie.grid.thumbnails/", AOFLoginManager.sharedInstance.cachesDir];
    //@ABT-138 Fix to force the reload of all thumbnails. Added extra 2 character. Not recommended.
    path = [NSString stringWithFormat:@"%@%@2", basePath, [externalPath lastPathComponent]];
    return path;
}

- (NSString *)getCoverflowImagePath:(NSString *)externalPath
{
    NSString *path;
    NSString *basePath = [NSString stringWithFormat:@"%@/%@/", AOFLoginManager.sharedInstance.cachesDir,ProntoEventsCoverflowImageFolderName];
    path = [NSString stringWithFormat:@"%@%@", basePath, [externalPath lastPathComponent]];
    return path;
}

#pragma mark Core-Data and Mapping implementation

/**
 *  Mapping franchises into CoreData Database
 *
 *  @param franchises Dictionary with data franchise-brands
 *  @param success    Block success returned franchises array and brands array
 *  @param onError    Block error
 */

-(void)getFranchise:(NSDictionary *)franchises success:(void (^)(NSArray *franchiseArray, NSArray* brandsArray))success error:(void (^)(NSError *))onError{
    
    NSDictionary * franchisesToImport = [[self formatFranchises:franchises] valueForKey:@"franchises"];
    NSDictionary * franchisesOrBrandsToDelete = [[self formatFranchises:franchises] valueForKey:@"deleted"];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        for (NSDictionary * franchise in franchisesToImport) {
           
            NSString * franchiseId = [franchise valueForKey:@"field_franchise_id"];
            NSArray * franchisesArray = [self getFranchise:franchiseId withContext:localContext];
            
            if (franchisesArray.count == 0) {
                Franchise * newFranchise = [Franchise MR_createEntityInContext:localContext];
                [newFranchise MR_importValuesForKeysWithObject:franchise];
            }else{
//                Franchise * currentFranchise = franchisesArray[0];
//                NSArray * brandsInFranchiseDictionary = [franchise valueForKey:@"brands"];
//                if (currentFranchise.brands.count != brandsInFranchiseDictionary.count) {
//                    [currentFranchise MR_deleteEntityInContext:localContext];
//                    currentFranchise = [Franchise MR_createEntityInContext:localContext];
//                    [currentFranchise MR_importValuesForKeysWithObject:franchise];
//                }
            }
        }
        
        if (franchisesOrBrandsToDelete) {
            for (id key in franchisesOrBrandsToDelete) {
                [self deletedFranchiseOrBrandWithKey:key withContext:localContext];
            }
        }
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        if (error) {
            onError(error);
        }else{
            NSFetchRequest * fetchrequestFranchise = [Franchise MR_requestAll];
            NSArray *importedFranchises = [Franchise MR_executeFetchRequest:fetchrequestFranchise];
            
            NSFetchRequest * fetchrequestBrands = [Brand MR_requestAll];
            NSArray *importedBrands = [Brand MR_executeFetchRequest:fetchrequestBrands];
            
            success(importedFranchises, importedBrands);
        }
    }];
    
}

/**
 *  Mapping categories into CoreData Model
 *
 *  @param categories Dictionary with data to mapping
 *  @param success    Array on Categories format
 *  @param onError    Error
 */

-(void)getCategories:(NSDictionary *)categories success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{
    
    NSDictionary * categoriesToImport = [categories valueForKey:@"categories"];
    NSDictionary * categoriesToDeleted = [categories valueForKey:@"deleted"];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        

        for (id key in categoriesToImport) {
            
            id category = [categoriesToImport objectForKey:key];
            NSString * categoryId = [category valueForKey:@"tid"];
            NSArray * categoriesArray = [self getCategory:categoryId withContext:localContext];
            
            if (categoriesArray.count == 0) {
                Categories * categoryToCheck = [Categories MR_createEntityInContext:localContext];
                [categoryToCheck setValue:[category valueForKey:@"bucketname"] forKey:@"bucketName"];
                NSNumber * bId = [NSNumber numberWithInt:[[category valueForKey:@"bucketid"] intValue]];
                [categoryToCheck setValue:bId forKey:@"bucketId"];
                [categoryToCheck MR_importValuesForKeysWithObject:category];
            }
        }
        
        if (categoriesToDeleted) {
            for (id key in categoriesToDeleted) {
                
                NSString * categoryId = key;
                NSArray * categoriesArray = [self getCategory:categoryId withContext:localContext];
                
                if (categoriesArray.count > 0) {
                    Categories * categoryToCheck = categoriesArray[0];
                    [categoryToCheck MR_deleteEntityInContext:localContext];
                    [localContext MR_saveToPersistentStoreAndWait];
                }
            }
        }
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        if (error) {
            onError(error);
        }else{
            NSFetchRequest * fetchrequest = [Categories MR_requestAll];
            NSArray *importedCategories = [Categories MR_executeFetchRequest:fetchrequest];
            success(importedCategories);
        }
    }];
}




// New Search Implementation
/**
 *  Mapping Assets from CoreData Database using assetID
 *
 *  @param assets  Dictionary with JSON to parse assets
 *  @param success Block success
 *  @param onError Block error
 */


-(void)getSearchedAssets:(NSArray *)assetIds success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{

    NSFetchRequest * fetchrequest = [Asset MR_requestAll];
    NSArray *importedAssets = [Asset MR_executeFetchRequest:fetchrequest];
//    NSArray *filteredArray = [[ZMCDManager sharedInstance] getFilteredAssetsAccordingToActiveBucketFromAllAssets:importedAssets];
//    importedAssets = filteredArray;
    NSMutableArray* checkAssets = [[NSMutableArray alloc] init];

    
    for (NSDictionary * asset in importedAssets) {
        
        NSString * assetId = [[asset valueForKey:@"assetID"] stringValue];
        
        for(NSString* aID in assetIds)
        {
            if([assetId isEqualToString:aID])
            {
                [checkAssets addObject:asset];
            }
        }
    }
    
 
    
    checkAssets = [self removeDuplicates:checkAssets];
    
    if(self.isConnectionAvailable)
    {
       checkAssets = [[self sortedArray:[ZMUserActions sharedInstance].sortingKey  andArray:checkAssets] copy];
    }
    
    [ZMProntoManager sharedInstance].assetsIdToFilter = checkAssets;
    ZMProntoManager.sharedInstance.assetArray = checkAssets;
    ZMProntoManager.sharedInstance.assetArrayIds = [checkAssets valueForKey:@"assetID"];
    
    success(checkAssets);
}

- (NSDictionary *)compact:(NSDictionary *)aDictionary {
    
    NSDictionary *answer = [NSMutableDictionary dictionary];
    
    for (NSString *key in [aDictionary allKeys]) {
        id value = [aDictionary valueForKey:key];
        if (value && value != [NSNull null]) {
            [answer setValue:value forKey:key];
        }
    }
    return [NSDictionary dictionaryWithDictionary:answer];
}


- (void)storeLocally:(NSDictionary *)data filename:(NSString*)keyword
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fName = [NSString stringWithFormat:@"%@.plist",keyword];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:fName];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"plistPath:%@",plistPath]];
    
    dispatch_queue_t q = dispatch_queue_create("com.my.global_search", NULL);
    dispatch_sync(q, ^{
        [data writeToFile:plistPath atomically: YES];
    });
    
    //    [data writeToFile:plistPath atomically: YES];
//    [data writeToFile:plistPath atomically: YES];
    
}


- (NSMutableArray *)arrayByEliminatingDuplicatesWithoutOrder:(NSMutableArray *)array{
    
    NSArray *uniqueObjects = [[NSSet setWithArray:[array copy]] allObjects];
    
    NSMutableArray* newArray  = [uniqueObjects mutableCopy];
    return newArray;
    
}


// Remove duplicate asset details
- (NSMutableArray *)removeDuplicates:(NSMutableArray *)array{
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:array];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    NSMutableArray * newArray = [[[NSOrderedSet orderedSetWithArray:arrayWithoutDuplicates] array] copy];
    return newArray;
    
}

// New Search Implementation - Sort By
-(NSArray *)sortedArray: (NSString*)sortingTerm andArray:(NSMutableArray *)array{

   NSSortDescriptor * sortDescriptor;
   sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortingTerm ascending:NO];
   NSArray * sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
   NSArray * sortedArray;
   sortedArray = [array sortedArrayUsingDescriptors:sortDescriptors];

    return sortedArray;

}



/**
 *  Mapping Assets into CoreData Database
 *
 *  @param assets  Dictionary with JSON to parse assets
 *  @param success Block success
 *  @param onError Block error
 */


-(void)getAssets:(NSDictionary *)assets success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError
{
        NSDictionary * assetsFormated = [self formatAssets:assets];
        NSDictionary * assetsToImport = [assetsFormated valueForKey:@"assets"];
    
        NSArray * assetsToDeleted = [assetsFormated valueForKey:@"deleted"];
        NSArray * assetsToUnpublish = [assetsFormated valueForKey:@"unpublished"];
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"asset to import %@", assetsFormated]];
    
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            int i= 0;
            for (NSDictionary * asset in assetsToImport) {
                
                NSString * assetId = [asset valueForKey:@"nid"];
                NSArray * assetArray = [self getAsset:assetId withContext:localContext];
                
                if (assetArray.count == 0)
                {
                    Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                    [newAsset MR_importValuesForKeysWithObject:asset];
                }
                else
                {
                    
                    Asset * assetToCheck = assetArray[0];
//                    NSArray * brandsInAssetToImport = [asset valueForKey:@"brands"];
//                    NSArray * franchisesInAssetToImport = [asset valueForKey:@"franchise"];
                    NSArray * categoiresInAssetToImport = [asset valueForKey:@"categories"];
                    NSString * valueChanged = [asset valueForKey:@"changed"];
                    
                    // PRONTO-1 - Likes not Working
                    NSString * user_vote = [asset valueForKey:@"user_vote"];
                    NSNumber * valueCount = [NSNumber numberWithInt:[[asset valueForKey:@"view_count"] intValue]];
                    NSNumber * valueVotes = [NSNumber numberWithInt:[[asset valueForKey:@"votes"] intValue]];
                    
                    // New Revamping - check the Update Available
                    if(![assetToCheck.changed.stringValue isEqualToString: valueChanged])
                    {
                        NSNumber  *lastUpdated = [NSNumber numberWithInteger:[assetToCheck.changed.stringValue integerValue]];
                        NSNumber  *currentUpdate = [NSNumber numberWithInteger:[valueChanged integerValue]];
                        
                        if(currentUpdate > lastUpdated)
                        {
                            assetToCheck.isUpdateAvailable = YES;
                            [assetToCheck setValue:[NSNumber numberWithBool:YES] forKey:@"isUpdateAvailable"];
                            [asset setValue:[NSNumber numberWithBool:YES] forKey:@"isUpdateAvailable"];
                            [asset setValue:currentUpdate forKey:@"changed"];
                            
                            [[ZMCDManager sharedInstance]updateAsset:assetToCheck success:^{
                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"count -> Asset updated %d ", i]];
                                [[ZMAssetsLibrary defaultLibrary] notifyAssetsChanged];
                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Succesfully updated Asset with Id%@ and title %@",assetToCheck.assetID,assetToCheck.title]];
                            } error:^(NSError * error) {
                                [AbbvieLogging logError:[NSString stringWithFormat:@"Error at updating asset %@", error.localizedDescription]];
                            }];
                        }
                        else
                        {
                            assetToCheck.isUpdateAvailable = NO;
                            [asset setValue:[NSNumber numberWithBool:NO] forKey:@"isUpdateAvailable"];
                            [assetToCheck setValue:[NSNumber numberWithBool:NO] forKey:@"isUpdateAvailable"];
                        }
                    }
                    
                    if (
                        //assetToCheck.brands.count != brandsInAssetToImport.count
//                        || assetToCheck.franchise.count != franchisesInAssetToImport.count
//                        ||
                        assetToCheck.categories.count != categoiresInAssetToImport.count
                        || !([assetToCheck.changed.stringValue isEqualToString: valueChanged])
                        || assetToCheck.view_count.intValue != valueCount.intValue
                        || assetToCheck.votes.intValue != valueVotes.intValue
                        || assetToCheck.user_vote.intValue != user_vote.intValue) {
                        
                        [assetToCheck MR_deleteEntityInContext:localContext];
                        [localContext MR_saveToPersistentStoreAndWait];
                        assetToCheck = [Asset MR_createEntityInContext:localContext];
                        [assetToCheck MR_importValuesForKeysWithObject:asset];
                        
                    }
                }
                i++;
            }

            NSArray *previousAssetIds = [[ProntoUserDefaults userDefaults] getCouldNotDeleteOrUnpulishAssetIds];
            
            if (assetsToDeleted || assetsToUnpublish || previousAssetIds)
            {
                NSMutableArray * assetsToProcess = [[NSMutableArray alloc]init];
                NSMutableArray *assetsCouldNotBeProcessed = [NSMutableArray array];
                for (id key in assetsToDeleted) {
                    [assetsToProcess addObject:key];
                }
                
                for (id key in assetsToUnpublish) {
                    [assetsToProcess addObject:key];
                }
                
                [assetsToProcess addObjectsFromArray:previousAssetIds];
                
                for (id key in assetsToProcess)
                {
                    NSArray * assetArray = [self getAsset:key withContext:localContext];
                    if (assetArray.count > 0)
                    {
                        Asset * assetToCheck = assetArray[0];
                        if (assetToCheck.path)
                        {
                            NSError *error;
                            if ([[NSFileManager defaultManager] fileExistsAtPath:assetToCheck.path])
                            {
                                if ([[NSFileManager defaultManager] removeItemAtPath:assetToCheck.path error:&error] == YES)
                                {
                                    // Need to check if the asset is successfully removed
                                    [AbbvieLogging logError:[NSString stringWithFormat:@"Deleted %@",assetToCheck.path]];
                                }
                                else
                                {
                                    [AbbvieLogging logError:[NSString stringWithFormat:@"Unable to delete %@,%@",assetToCheck.path,error]];
                                }
                            }
                        }
                        
                        BOOL didSave = [assetToCheck MR_deleteEntityInContext:localContext];
                        [localContext MR_saveToPersistentStoreAndWait];
                        if (didSave == NO)
                        {
                            [assetsCouldNotBeProcessed addObject: key];
                        }
                    }
                }
                
                //save to userdefaults
                [[ProntoUserDefaults userDefaults] setCouldNotDeleteOrUnpulishAssetIds:assetsCouldNotBeProcessed];
                
            }
        } completion:^(BOOL contextDidSave, NSError *error) {
            if (error) {
                onError(error);
            }else{
                NSFetchRequest * fetchrequest = [Asset MR_requestAll];
                NSArray *importedCategories = [Asset MR_executeFetchRequest:fetchrequest];
                success(importedCategories);
            }
        }];
//    }
}

/**
 *  Delete Asset to CoreData when if were deleted in CMS server.
 *
 *  @param assetsToDelete Array to Ids to delete.
 *  @param success        success Block
 */

-(void)processUnpublishOrDeleteAsset:(NSMutableArray*)assetsToDelete success:(void (^)())success {
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * localContext) {
      
        NSArray *previousAssetIds = [[ProntoUserDefaults userDefaults] getCouldNotDeleteOrUnpulishAssetIds];
        [assetsToDelete addObjectsFromArray:previousAssetIds];
        
        NSMutableArray *assetsCouldNotBeProcessed = [NSMutableArray array];

        for (id key in assetsToDelete) {
            NSArray * assetArray = [self getAsset:key withContext:localContext];
            if (assetArray.count > 0) {
                Asset * assetToCheck = assetArray[0];
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"assetToCheck.path:%@",assetToCheck.path]];
                
                if (assetToCheck.path) {
                    NSError *error;
                    if ([[NSFileManager defaultManager] fileExistsAtPath:assetToCheck.path]) {
                        // Need to check this whether this is successfully removed
                        if ([[NSFileManager defaultManager] removeItemAtPath:assetToCheck.path error:&error] == YES) {
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"Deleted %@",assetToCheck.path]];
                        }
                        else {
                            [AbbvieLogging logError:[NSString stringWithFormat:@"Unable to delete %@,%@",assetToCheck.path,error]];
                        }
                    }
                }
                
                BOOL didSave =  [assetToCheck MR_deleteEntityInContext:localContext];
                [localContext MR_saveToPersistentStoreAndWait];
                if (didSave == NO)
                {
                    [assetsCouldNotBeProcessed addObject: key];
                }
            }
        }
        
        //save to userdefaults
        [[ProntoUserDefaults userDefaults] setCouldNotDeleteOrUnpulishAssetIds:assetsCouldNotBeProcessed];
        
    } completion:^(BOOL contextDidSave, NSError * error) {
        success();
    }];
}

/**
 *  Get Array to categories from Categories entity
 *
 *  @param object  CategoryId to find
 *  @param context Context to performed query
 *
 *  @return Array of categories
 */

-(NSArray*)getCategory:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *categoryFilter = [NSPredicate predicateWithFormat:@"categoryID == %@", object];
    NSArray * catetory = [Categories MR_findAllWithPredicate:categoryFilter inContext:context];
    return catetory;
}


/**
 *  Get Array to franchises from Franchise entity
 *
 *  @param object  franchiseId to find
 *  @param context Context to performed query
 *
 *  @return Array of Franchies
 */

-(NSArray*)getFranchise:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *franchisesFilter = [NSPredicate predicateWithFormat:@"franchiseID == %@", object];
    NSArray * franchises = [Franchise MR_findAllWithPredicate:franchisesFilter inContext:context];
    return franchises;
}

/**
 *  Get Array to Brands from Brand entity
 *
 *  @param object  brandID to find
 *  @param context Context to performed query
 *
 *  @return Array of Brands
 */

-(NSArray*)getBrands:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *brandsFilter = [NSPredicate predicateWithFormat:@"brandID == %@", object];
    NSArray * brands = [Brand MR_findAllWithPredicate:brandsFilter inContext:context];
    return brands;
}

/**
 *  Get Array to Assets from Asset entity
 *
 *  @param object  assetID to find
 *  @param context Context to performed query
 *
 *  @return Array of Assets
 */

-(NSArray*)getAsset:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *assetFilter = [NSPredicate predicateWithFormat:@"assetID == %@", object];
    NSArray * asset = [Asset MR_findAllWithPredicate:assetFilter inContext:context];
    return asset;
}

/**
 *  Delete Branchise or Brand to Franchise or Brand entity when exists a key in WS
 *
 *  @param key     FranchiseID or BrandID
 *  @param context Context to performed query
 */

-(void)deletedFranchiseOrBrandWithKey:(NSString*)key withContext:(NSManagedObjectContext*)context{
    
    NSArray * franchiseArray = [self getFranchise:key withContext:context];
    
    if (franchiseArray.count == 0) {
        NSArray * brandsArray = [self getBrands:key withContext:context];
        if (brandsArray.count > 0) {
            Brand * brand = brandsArray[0];
            [brand MR_deleteEntityInContext:context];
        }
    }else{
        Franchise * franchise = franchiseArray[0];
        [franchise MR_deleteEntityInContext:context];
    }
    
    [context MR_saveToPersistentStoreAndWait];
    
}

-(void)insertNewSearch:(Franchise*)franchise withData:(NSDictionary*)franchiseData withContext:(NSManagedObjectContext*)context{
    franchise = [Franchise MR_createEntityInContext:context];
    [franchise MR_importValuesForKeysWithObject:franchiseData];
}


/**
 *  Save new Franchise into Franchise entity
 *
 *  @param franchise     Franchise Object
 *  @param franchiseData Franchise data to save
 *  @param context       Context to performed query
 */
-(void)insertNewFranchise:(Franchise*)franchise withData:(NSDictionary*)franchiseData withContext:(NSManagedObjectContext*)context{
    franchise = [Franchise MR_createEntityInContext:context];
    [franchise MR_importValuesForKeysWithObject:franchiseData];
}

/**
 *  Format JSON Asset to get struct to mapping data from CoreData
 *
 *  @param assets Asset Dictionary
 *
 *  @return new Asset format
 */

-(NSDictionary*)formatSearchAssets:(NSDictionary*)assets{
    
    NSMutableArray * assetObject = [[NSMutableArray alloc]init];
    NSMutableDictionary * newAssets = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary * assetsToFormat = [[NSMutableDictionary alloc]initWithDictionary:assets];

    
    for (id key in assetsToFormat) {
        id asset = [assetsToFormat objectForKey:key];
       
        NSMutableDictionary * newAssetObject = [[NSMutableDictionary alloc]initWithDictionary:asset];
        
        [assetObject addObject:newAssetObject];
    }
    
    
    return newAssets;
}


/**
 *  Format JSON Asset to get struct to mapping data into CoreData
 *
 *  @param assets Asset Dictionary
 *
 *  @return new Asset format
 */

-(NSDictionary*)formatAssets:(NSDictionary*)assets{
    
    NSMutableArray * assetObject = [[NSMutableArray alloc]init];
    NSMutableDictionary * newAssets = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary * assetsToFormat = [[NSMutableDictionary alloc]initWithDictionary:assets];
    [assetsToFormat removeObjectForKey:@"pages"];
    [assetsToFormat removeObjectForKey:@"unpublished"];
    [assetsToFormat removeObjectForKey:@"deleted"];
    [assetsToFormat removeObjectForKey:@"total"];
    [assetsToFormat removeObjectForKey:@"changed"];
    
    for (id key in assetsToFormat) {
        id asset = [assetsToFormat objectForKey:key];
        NSDictionary * brandObject = [self formatBrandsObject:asset];
        NSMutableDictionary * newAssetObject = [[NSMutableDictionary alloc]initWithDictionary:asset];
        [newAssetObject removeObjectForKey:@"field_brand"];
        [newAssetObject removeObjectForKey:@"field_categories"];
        [newAssetObject removeObjectForKey:@"rating"];
        [newAssetObject setObject:[brandObject valueForKey:@"brands"] forKey:@"brands"];
        [newAssetObject setObject:[brandObject valueForKey:@"franchise"] forKey:@"franchise"];
        [newAssetObject setObject:[brandObject valueForKey:@"categories"] forKey:@"categories"];
        [newAssetObject setObject:[self setRating:[asset valueForKey:@"rating"]] forKey:@"votes"];
        // PRONTO-1 - Likes not Working
        [newAssetObject setObject:[self setRatingUserVote:[asset valueForKey:@"rating"]] forKey:@"user_vote"];
        [assetObject addObject:newAssetObject];
    }
    
    [newAssets setObject:assetObject forKey:@"assets"];
    [newAssets setObject:[assets valueForKey:@"changed"] forKey:@"changed"];
    [newAssets setObject:[assets valueForKey:@"total"] forKey:@"total"];
    [newAssets setObject:[assets valueForKey:@"deleted"] forKey:@"deleted"];
    [newAssets setObject:[assets valueForKey:@"unpublished"] forKey:@"unpublished"];
    [newAssets setObject:[assets valueForKey:@"pages"] forKey:@"pages"];
    
    return newAssets;
}

-(NSDictionary*)formatBrandsObject:(NSDictionary*)object{
    
    NSMutableArray * brandsKeys = [[NSMutableArray alloc]init];
    NSMutableArray * franchisesKeys = [[NSMutableArray alloc]init];
    NSMutableArray * categoriesKeys = [[NSMutableArray alloc]init];
    NSMutableDictionary *endObject = [[NSMutableDictionary alloc]init];
    
    if ([ZMProntoManager sharedInstance].brandsArray.count <= 0) {
        NSFetchRequest * fetchrequestBrands = [Brand MR_requestAll];
        [ZMProntoManager sharedInstance].brandsArray = [Brand MR_executeFetchRequest:fetchrequestBrands];
    }
    if ([ZMProntoManager sharedInstance].franchisesArray.count <= 0) {
        NSFetchRequest * fetchrequestFranchise = [Franchise MR_requestAll];
        [ZMProntoManager sharedInstance].franchisesArray = [Franchise MR_executeFetchRequest:fetchrequestFranchise];
    }
    if ([ZMProntoManager sharedInstance].categoriesArray.count <= 0) {
        NSFetchRequest * fetchrequestCategories = [Categories MR_requestAll];
        [ZMProntoManager sharedInstance].categoriesArray = [Categories MR_executeFetchRequest:fetchrequestCategories];
    }
    
    for (id brand in [object valueForKey:@"field_brand"]) {
        
        NSString * predicateFormat =[NSString stringWithFormat:@"brandID == %@",[brand valueForKey:@"brand_id"]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat];
        
        if ([self findObjectWithPredicate:predicate onArray:brandsKeys].count == 0) {
            NSArray * brandObject = [self findObjectWithPredicate:predicate onArray:[ZMProntoManager sharedInstance].brandsArray];
            if (brandObject.count > 0) {
                for (Brand *brand in brandObject) {
                    NSMutableDictionary * newBrand = [[NSMutableDictionary alloc]init];
                    [newBrand setObject:brand.brandID forKey:@"brandID"];
                    [newBrand setObject:brand.name forKey:@"name"];
                    [newBrand setObject:brand.vid forKey:@"vid"];
                   // [newBrand setObject:@"2E8B57" forKey:@"field_franchise_color"];
                    [brandsKeys addObject: newBrand];
                }
            }
        }
        
        for (id franchise in [brand valueForKey:@"franchises"]) {
            
            NSString * predicateFormat =[NSString stringWithFormat:@"tid == %@",franchise];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat];
            
            if ([self findObjectWithPredicate:predicate onArray:franchisesKeys].count == 0) {
                NSArray * franchiseObject = [self findObjectWithPredicate:predicate onArray:[ZMProntoManager sharedInstance].franchisesArray];
                if (franchiseObject.count > 0) {
                    for (Franchise *franchise in franchiseObject) {
                        NSMutableDictionary * newFranchise = [[NSMutableDictionary alloc]init];
                        [newFranchise setObject:franchise.field_active forKey:@"field_active"];
                        // New Pronto Revamp - color code
                        if(franchise.field_franchise_color.length == 0)
                            franchise.field_franchise_color = @"0d1c49";
                            
                        [newFranchise setObject:franchise.field_franchise_color forKey:@"field_franchise_color"];
                        [newFranchise setObject:franchise.tid forKey:@"tid"];
                        [newFranchise setObject:franchise.field_private forKey:@"field_private"];
                        [newFranchise setObject:franchise.franchiseID forKey:@"franchiseID"];
                        [newFranchise setObject:franchise.name forKey:@"name"];
                        [newFranchise setObject:franchise.vid forKey:@"vid"];
                        [franchisesKeys addObject: newFranchise];
                    }
                }
            }
        }
    }
    
    for (NSDictionary * category in [object valueForKey:@"field_categories"]) {
        
        NSString * predicateFormat =[NSString stringWithFormat:@"categoryID == %@",[category valueForKey:@"id"]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat];
        
        if ([self findObjectWithPredicate:predicate onArray:categoriesKeys].count == 0) {
            NSArray * categoryObject = [self findObjectWithPredicate:predicate onArray:[ZMProntoManager sharedInstance].categoriesArray];
            if (categoryObject.count > 0) {
                for (Categories *category in categoryObject) {
                    NSMutableDictionary * newCategory = [[NSMutableDictionary alloc]init];
                    [newCategory setObject:category.categoryID forKey:@"categoryID"];
                    [newCategory setObject:category.name forKey:@"name"];
                    [newCategory setObject:category.vid forKey:@"vid"];
                     // Revamp - added for bucket selection
                    [newCategory setObject:category.bucketId forKey:@"bucketid"];
                    if(category.bucketName.length > 0)
                        [newCategory setObject:category.bucketName forKey:@"bucketname"];
                    else
                        [newCategory setObject:@"Franchise" forKey:@"bucketname"];
                    [categoriesKeys addObject: newCategory];
                }
            }
        }
    }
    
    [endObject setObject:brandsKeys forKey:@"brands"];
    [endObject setObject:franchisesKeys forKey:@"franchise"];
    [endObject setObject:categoriesKeys forKey:@"categories"];
    
    return endObject;
    
}

-(NSString*)setRating:(NSDictionary*)rating{
    return [rating valueForKey:@"votes"];
}

// PRONTO-1 - Likes not Working
-(NSString*)setRatingUserVote:(NSDictionary*)rating{
    return [rating valueForKey:@"user_vote"];
}

-(NSArray*)findObjectWithPredicate:(NSPredicate*)predicate onArray:(NSArray*)array{
    NSMutableArray * object = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:predicate]];
    return object;
}

-(NSDictionary*)formatFranchises:(NSDictionary*)franchises{
    
    NSMutableDictionary * newFranchises = [[NSMutableDictionary alloc]init];
    
    NSDictionary * franchiseObject = [franchises valueForKey:@"franchises"];
//    franchiseObject = [franchises valueForKey:@"franchises"];
    
    NSMutableArray * newFranchiseObject = [self formatFranchisesObject:franchiseObject];
    
    [newFranchises setObject:newFranchiseObject forKey:@"franchises"];
    [newFranchises setObject:[franchises valueForKey:@"deleted"] forKey:@"deleted"];
    [newFranchises setObject:[franchises valueForKey:@"brands"] forKey:@"brands"];
    
    return newFranchises;
}

-(NSMutableArray*)formatFranchisesObject:(NSDictionary*)object{
    
    NSMutableArray * newObject = [[NSMutableArray alloc]init];
    NSMutableArray * endObject = [[NSMutableArray alloc]init];
    
    for (id key in object) {
        NSMutableDictionary *franchise = [object objectForKey:key];
        [newObject addObject:franchise];
    }
    
    for (id key in newObject) {
        NSMutableDictionary * franchise = [[NSMutableDictionary alloc]initWithDictionary:key];
        if ([franchise objectForKey:@"brands"]) {
            NSMutableDictionary * brands = [franchise objectForKey:@"brands"];
            NSMutableArray * newBrands = [[NSMutableArray alloc]init];
            for (id key in brands) {
                NSMutableDictionary * brand = [[NSMutableDictionary alloc]initWithDictionary:key];
                [brand removeObjectForKey:@"franchises"];
                [newBrands addObject:brand];
            }
            [franchise removeObjectForKey:@"brands"];
            [franchise setObject:newBrands forKey:@"brands"];
            [endObject addObject:franchise];
        }else{
            [endObject addObject:franchise];
        }
    }
    
    return endObject;
}


/*
 * Fetch the Fav Assets
 * fetch the fav assets from CMS
 *
 */

- (void)fetchEvents:(void(^)(NSDictionary *FavAssets))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi) {
        
        NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
    
        //AOFKit Implementation
        NSMutableArray *toJSON = [NSMutableArray array];
        NSDictionary *eventToSend = [NSDictionary dictionaryWithObjectsAndKeys:
                                     @"", @"id",
                                     [NSString stringWithFormat:@"%d",5], @"type",
                                     @"", @"value",
                                     @"",@"date",
                                     franchiseId,@"sf", nil];
        [toJSON addObject:eventToSend];
        
        NSError *err;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:toJSON options:0 error:&err];
        NSString *JSONToPost = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi,
//                                 @"events":JSONToPost};
        //New Implementation with AccessToken
        NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
        @"events":JSONToPost};
        
        NSString *url = [self getServiceResource:@"fetchEvents.uri"];
        
        [self performBlock:^(CallbackBlock callback) {
            
            [[self requestManagerWithToken:YES headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback)
                    {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS of fetchEvents.uri:%@",responseObject]];
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        [AbbvieLogging logError:[NSString stringWithFormat:@"Error of fetchEvents.uri:%@",error.localizedDescription]];
                        callback(errorCode, nil);
                    }
                });
            }];
            
        }
     retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
         
         if (error) {
             
             [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS Fetch Events", (long)error.code, error.localizedDescription]];
             if (error.code == 107) {
                 onError([self getErrorWithCode:107
                               alternateMessage:@"The aplication is unavailible at this moment."]);
             }else if (error.code == 401) {
                 onError([self getErrorWithCode:401
                               alternateMessage:@"Server connection is not available at the moment, please try again later."]);
             }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                 onError([self getErrorWithCode:error.code
                               alternateMessage:@"Server connection is not available at the moment, please try again later."]);
             }
             else {
                 onError([self getErrorWithCode:000 alternateMessage:@"The application cannot send user information."]);
             }
             
         }else{
             
             [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS Upload Fetch Events Response: %@", response]];
             complete((NSDictionary *)response);
         }
     }];
    }
}

///*
// * Save the Preferences
// * Save the selected Preferences sending to CMS
// *
// */
//// savePreferences to send and receive the updated preference details
//- (void)savePreferences:(NSArray*)franchiseNode complete:(void(^)(NSDictionary *preferencesSavedStatus))complete error:(void(^)(NSError *error))onError{
//
//    if ([AOFLoginManager sharedInstance].upi) {
//        
//
//            
//        //New Changes for Token
//        NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
//        @"data":franchiseNode};
//        [AbbvieLogging logInfo:[NSString stringWithFormat:@"sync params:%@",params]];
//            
//            NSString *url = [self getServiceResource:@"savePreferences.uri"];
//            
//            [self performBlock:^(CallbackBlock callback) {
//                
//                [[self requestManagerWithToken:YES headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^(void){
//                        if (callback) {
//                            callback(nil, responseObject);
//                        }
//                    });
//                    
//                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                    
//                    if (callback) {
//                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
//                        callback(errorCode, nil);
//                    }
//                    
//                }];
//                
//            } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
//                
//                if (error) {
//                    
//                    [AbbvieLogging logInfo:@"Save Preferences Request Not Successfull"];
//                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"params:%@",params]];
//                    [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS saved Preferences.uri", (long)error.code, error.localizedDescription]];
//                    
//                    if (error.code == 107) {
//                        onError([self getErrorWithCode:107
//                                      alternateMessage:@"The aplication is unavailible at this moment."]);
//                    }else if (error.code == 401) {
//                        onError([self getErrorWithCode:401
//                                      alternateMessage:@"Server connection is not available at the moment, please try again later."]);
//                    }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
//                        onError([self getErrorWithCode:error.code
//                                      alternateMessage:@"Server connection is not available at the moment, please try again later."]);
//                    }
//                    else {
//                        onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
//                    }
//                }else{
//                    if (response)
//                    {
//                        [AbbvieLogging logInfo:@"Save Preferences Request Successfull"];
//                        complete((NSDictionary *)response);
//                    }
//                }
//            }];
//    }
//}

///*
// * Fetch the Preferences
// * Fetch the Preferences from CMS
// *
// */
//
//- (void)fetchPrefernces:(void(^)(NSArray *preferencesFetched))complete error:(void(^)(NSError *error))onError;
//{
//    if ([AOFLoginManager sharedInstance].upi) {
//
//        [self performBlock:^(CallbackBlock callback) {
//
//            //Logout crash fix
//            if([AOFLoginManager sharedInstance].upi != nil) {
//            //TO-DO Use a safer model to log in
////            NSDictionary *params = @{@"upi":[AOFLoginManager sharedInstance].upi};
//
//            //New Change for Token
//            NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
//
//            NSString *url = [self getServiceResource:@"fetchPreferences.uri"];
//
//            [[self requestManagerWithToken:NO headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//
//                dispatch_async(dispatch_get_main_queue(), ^(void){
//                    if (callback)
//                    {
//                        callback(nil, responseObject);
//                    }
//                });
//
//            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//
//                if (callback)
//                {
//                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
//                    callback(errorCode, nil);
//                }
//            }];
//        }
//
//        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
//
//            if (error) {
//                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS fetchPreferences uri", (long)error.code, error.localizedDescription]];
//
//                if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004 || error.code ==-1003 || error.code ==-1011 || error.code ==1011)
//                {
//                    onError([self getErrorWithCode:error.code
//                                  alternateMessage:@"Server connection is not available for displaying promoted content, please try again later."]);
//                }
//                else
//                {
//                    onError([self getErrorWithCode:999
//                                  alternateMessage:@"Preferences Failed"]);
//                }
//
//                [ZMUserActions sharedInstance].isPromotedLoaded = NO;
//
//            }
//            else
//            {
//                [AbbvieLogging logInfo:[NSString stringWithFormat:@"fetchPreferences WS Response: %@", response]];
//                complete((NSArray *)response);
//            }
//        }];
//    }
//}


// Webservices for Events
// getEvents for Landing screen
- (void)getEvents:(void(^)(NSDictionary *eventsFetched))complete error:(void(^)(NSError *error))onError
{
    NSDate *date = [NSDate date];
    if ([AOFLoginManager sharedInstance].upi != nil)
    {
        [self performBlock:^(CallbackBlock callback) {
            
            if ([AOFLoginManager sharedInstance].upi != nil)
            {
                NSString *lastUpdate = @"0";
                lastUpdate = [[ProntoUserDefaults userDefaults] getCMSLastSynchronizationForEvents];
                if ([lastUpdate isEqualToString:@"(null)"] || lastUpdate == nil || lastUpdate.length <= 0)
                {
                    lastUpdate = @"0";
                }
                
                NSDictionary *params = @{@"date":lastUpdate};

                
                //Changes for new token change
                NSString *url = [NSString stringWithFormat:@"%@?token=%@",[self getServiceResource:@"eventsLanding.uri"],[AOFLoginManager sharedInstance].getAccessToken];
                
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"events url:%@",url]];
                
                [[self requestManagerWithToken:NO headers:nil] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (callback)
                        {
                            NSInteger timeStamp = [date timeIntervalSinceNow];
                            
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS of eventsLanding.uri:%@ and took time  %ld",responseObject, (long)(-timeStamp)]];
                            callback(nil, responseObject);
                        }
                    });
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (callback)
                    {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        callback(errorCode, nil);
                    }
                }];
            }
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error)
            {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS getEvents uri", (long)error.code, error.localizedDescription]];
                
                if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004 || error.code ==-1003 || error.code ==-1011 || error.code ==1011)
                {
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available for displaying Events, please try again later."]);
                }
            }
            else
            {
                NSDictionary *events = (NSDictionary *)response;
                 complete(events);
            }
        }];
    }
}


// session Details for CalendarView
- (void)getEventDetails:(NSString*)eventId complete:(void(^)(NSDictionary *sessionDetails))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi)
    {
        [self performBlock:^(CallbackBlock callback) {
            
            if([AOFLoginManager sharedInstance].upi != nil)
            {
                
//                NSString *url = [NSString stringWithFormat:@"%@?id=%@&upi=%@",[self getServiceResource:@"sessionDetails.uri"],eventId,[AOFLoginManager sharedInstance].upi];
                
                //Changes for new token chnage
                NSString *url = [NSString stringWithFormat:@"%@?id=%@&token=%@",[self getServiceResource:@"sessionDetails.uri"],eventId,[AOFLoginManager sharedInstance].getAccessToken];
                
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"sessions url:%@",url]];
                
                [[self requestManagerWithToken:NO headers:nil] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        if (callback)
                        {
                            callback(nil, responseObject);
                        }
                    });
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (callback)
                    {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        callback(errorCode, nil);
                    }
                }];
            }
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS get Sessions uri", (long)error.code, error.localizedDescription]];
                
                if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004 || error.code ==-1003 || error.code ==-1011 || error.code ==1011)
                {
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available for displaying Sessions, please try again later."]);
                }
                
            }
            else
            {
                NSDictionary *sessions = (NSDictionary *)response;
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Sessions WS Response: %@", sessions]];
                
                complete(sessions);
            }
        }];
    }
}

// fetchViewed - fetching the already visited assets
- (void)fetchViewed:(void(^)(NSDictionary *assetDetailsFetched))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi != nil)
    {
        [self performBlock:^(CallbackBlock callback) {
        
            //changes made for Token
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObjectIfValueAvailable:[AOFLoginManager sharedInstance].getAccessToken forKey:kTokenKey];
            
            NSString *url = [self getServiceResource:@"fetchViewed.uri"];
            
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"viewed assets url:%@",url]];
            
                [[self requestManagerWithToken:NO headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (callback)
                        {
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS of fetchViewed.uri:%@",responseObject]];
                            callback(nil, responseObject);
                        }
                    });
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (callback)
                    {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        callback(errorCode, nil);
                    }
                }];
            //}
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error)
            {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS fetchViewed uri", (long)error.code, error.localizedDescription]];
                
                if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004 || error.code ==-1003 || error.code ==-1011 || error.code ==1011)
                {
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available for displaying Events, please try again later."]);
                }
            }
            else
            {
                NSDictionary *assetsViewed = (NSDictionary *)response;
                complete(assetsViewed);
            }
        }];
    }
}


// saveViewed - Saving the already visited assets
- (void)saveViewed:(NSString*) eventId session:(NSString*) sessionId asset:(NSString*) assetId type:(NSString*)assetType complete:(void(^)(NSDictionary *savedAssetDetails))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi)
    {
 
        //Chnages made for new Access token
        NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
        @"aid":assetId,@"type":assetType};
        
        NSString *url = [self getServiceResource:@"saveViewed.uri"];
        
        [self performBlock:^(CallbackBlock callback) {
            
            [[self requestManagerWithToken:NO headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback)
                    {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS of saveViewed.uri:%@",responseObject]];
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        [AbbvieLogging logError:[NSString stringWithFormat:@"Error of saveViewed.uri:%@",error.localizedDescription]];
                        callback(errorCode, nil);
                    }
                });
            }];
            
        }
     retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
         
         if (error)
         {
             
             [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS saveViewed:", (long)error.code, error.localizedDescription]];
             if (error.code == 107) {
                 onError([self getErrorWithCode:107
                               alternateMessage:@"The aplication is unavailible at this moment."]);
             }else if (error.code == 401) {
                 onError([self getErrorWithCode:401
                               alternateMessage:@"Server connection is not available at the moment, please try again later."]);
             }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                 onError([self getErrorWithCode:error.code
                               alternateMessage:@"Server connection is not available at the moment, please try again later."]);
             }
             else {
                 onError([self getErrorWithCode:000 alternateMessage:@"The application cannot send user information."]);
             }
             
         }
         else
         {
             
             [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS Upload saveViewed Response: %@", response]];
             complete((NSDictionary *)response);
         }
     }];
    }
}

- (void)syncAllEvents:(void(^)(NSDictionary *allEvents))complete error:(void(^)(NSError *error))onError;
{
    if ([AOFLoginManager sharedInstance].upi)
    {
        NSString *lastUpdate = [[ProntoUserDefaults userDefaults] getCMSLastSynchronizationForEvents];
        
        if ([lastUpdate isEqualToString:@"(null)"] || !lastUpdate || lastUpdate == nil || lastUpdate.length <= 0) {
            lastUpdate = @"0";
        }

        NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
                                       @"date":lastUpdate,
                                       @"per_page":@"50",
                                       @"page":[NSString stringWithFormat:@"0"]};
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"eventsAll params:%@",params]];
        
        NSDictionary *headers = @{@"1.1":@"services_asset_getvocabulary_version", @"keep-alive":@"Connection"};
        
        NSString *url = [self getServiceResource:@"eventsAll.uri"];
        
        
        [self performBlock:^(CallbackBlock callback) {
            
            AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:headers];
            NSOperationQueue *operationQueue = manager.operationQueue;
            
            [manager.reachabilityManager startMonitoring];
            [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status)]];
                switch (status) {
                    case AFNetworkReachabilityStatusReachableViaWWAN:
                    case AFNetworkReachabilityStatusReachableViaWiFi:
                        [operationQueue setSuspended:NO];
                        break;
                    case AFNetworkReachabilityStatusNotReachable:
                    default:
                        [operationQueue setSuspended:YES];
                        break;
                }
            }];
            
            [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"resp of eventsAll.uri:%@",responseObject]];
                        
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                    callback(errorCode, nil);
                }
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {
                
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS eventsAll.uri", (long)error.code, error.localizedDescription]];
                
                if (error.code == 107) {
                    onError([self getErrorWithCode:107
                                  alternateMessage:@"The aplication is unavailible at this moment."]);
                }else if (error.code == 401) {
                    onError([self getErrorWithCode:401
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                    onError([self getErrorWithCode:error.code
                                  alternateMessage:@"Server connection is not available at the moment, please try again later."]);
                }else {
                    onError([self getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
            }else{
                
                NSDictionary *allEvents = (NSDictionary *)response;
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"eventsAll:%@",allEvents]];
                complete(allEvents);
            }
        }];
    }
}
- (void)saveViewedAsset:(NSNumber*)assetId type:(NSString*)assetType complete:(void(^)(NSDictionary *savedAssetDetails))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi)
    {
 
        //Chnages made for new Access token
        NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
        @"nid":assetId,@"type":assetType};
        
        NSString *url = [self getServiceResource:@"saveViewed.uri"];
        
        [self performBlock:^(CallbackBlock callback) {
            
            [[self requestManagerWithToken:NO headers:nil] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback)
                    {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS of saveViewed.uri:%@",responseObject]];
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        [AbbvieLogging logError:[NSString stringWithFormat:@"Error of saveViewed.uri:%@",error.localizedDescription]];
                        callback(errorCode, nil);
                    }
                });
            }];
            
        }
     retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
         
         if (error)
         {
             
             [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS saveViewed:", (long)error.code, error.localizedDescription]];
             if (error.code == 107) {
                 onError([self getErrorWithCode:107
                               alternateMessage:@"The aplication is unavailible at this moment."]);
             }else if (error.code == 401) {
                 onError([self getErrorWithCode:401
                               alternateMessage:@"Server connection is not available at the moment, please try again later."]);
             }else if (error.code == -1005 || error.code == -1001 ||  error.code ==-1004){
                 onError([self getErrorWithCode:error.code
                               alternateMessage:@"Server connection is not available at the moment, please try again later."]);
             }
             else {
                 onError([self getErrorWithCode:000 alternateMessage:@"The application cannot send user information."]);
             }
             
         }
         else
         {
             
             [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS Upload saveViewed Response: %@", response]];
             complete((NSDictionary *)response);
         }
     }];
    }
}

@end
