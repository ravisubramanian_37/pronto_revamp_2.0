//
//  FadeTransitionAnimator.h
//  Pronto
//
//  Created by m-666346 on 19/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TransitionType)
{
    TransitionTypeFade,
    TransitionTypeDropDown
};

@interface FadeTransitionAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign, getter = isPresenting) BOOL presenting;
@property (nonatomic, assign) CGFloat duration;
@property (nonatomic, assign) TransitionType transitionType;

@end
