
//
//  FadeTransitionAnimator.m
//  Pronto
//
//  Created by m-666346 on 19/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "FadeTransitionAnimator.h"

@implementation FadeTransitionAnimator

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return (self.duration) ? self.duration : 0.33f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    UIView  * containerView = [transitionContext containerView];
    UIViewController * fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
   	UIViewController * toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    if(self.presenting)
        
    {
        [containerView addSubview:fromViewController.view];
        [containerView addSubview:toViewController.view];
        CGRect frame = fromViewController.view.frame;
        toViewController.view.frame = frame;
        [self presetToOriginalValue:YES of:toViewController];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext]
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             
                             [self presetToOriginalValue:NO of:toViewController];
                         }
                         completion:^(BOOL finished){
                             
                             [transitionContext completeTransition:YES];
                         }];
    }
    else
        
    {
        [containerView addSubview:toViewController.view];
        [containerView addSubview:fromViewController.view];
        [self presetToOriginalValue:NO of:fromViewController];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext]
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             
                             [self presetToOriginalValue:YES of:fromViewController];
                         }
                         completion:^(BOOL finished){
                             
                             [transitionContext completeTransition:YES];
                             
                             if(![[UIApplication sharedApplication].keyWindow.subviews containsObject:toViewController.view])
                             {
                                 [[UIApplication sharedApplication].keyWindow addSubview:toViewController.view];
                             }
                         }
         ];
    }
}

- (void)presetToOriginalValue:(BOOL)reset of:(UIViewController *)controller
{
    if (self.transitionType == TransitionTypeFade)
    {
        controller.view.alpha = reset ? 0.0f : 1.0f;
    }
    else
    {
        CGRect frame = controller.view.frame;
        frame.size.height = reset ? 64.0f : [[UIScreen mainScreen] bounds].size.height;
        controller.view.frame = frame;
    }
}


@end
