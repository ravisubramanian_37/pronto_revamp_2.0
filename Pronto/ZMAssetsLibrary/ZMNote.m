//
//  ZMNote.m
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZMNote.h"

@implementation ZMNote
@synthesize nte_id,nte_content,asset_id,nte_weight,nte_page,nte_xcord,nte_ycord,nte_date,nte_height,nte_width;
- (void)dealloc
{
    [nte_content release];
    [super dealloc];
}
@end
