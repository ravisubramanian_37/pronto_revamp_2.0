//
//  ZMSynchronizeManager.h
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/26/11.
//  Copyright (c) 2011 Zemoga Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "ZMRemoteLibraryService.h"
#include "ZMAssetsGroup.h"

/**
 * Handles the instance and a Library service to be only on on the system (Singleton)
 */
@interface ZMSynchronizeManager : NSObject{
@private
    //used to know if the current thread is running or not.
    BOOL state;
    // stores the thread wich handles the Asset/Brands And Queue Administration
    id thread;
    // stores the current library that is running on this manager
    ZMRemoteLibraryService *library;
}

/**
 * return the current instance (Singleton)
 * @return ZMSynchronizeManager current instance
 */
+ (ZMSynchronizeManager *)instance;

/**
 * returns the current thread state
 * @return BOOL with 1 if thread is stopped or 2 if thread is running
 */
- (BOOL) state;

/**
 * stops the thread and removes it from the Manager
 */
- (void) stop;

/**
 * creates a thread with ZMRemoteLibraryService that runs once
 */
- (void) oneRunWithRemoteLibrary:(ZMRemoteLibraryService *)remoteLibrary;

/**
 * Save or update a category
 */
//- (void) saveCategoryToDao:(NSDictionary *)categories isToUpdate:(BOOL) isUpdate;

/**
 * creates a thread with ZMRemoteLibraryService and sets the "Time to Update" meaning the time when the thread launchs it run state
 */
- (void) startWithRemoteLibrary:(ZMRemoteLibraryService *)remoteLibrary timeToUpdate:(long)t;

/**
 * sends the download asset call to the ZMRemoteLibraryService attached to this Manager
 */
- (int) downloadAsset:(NSDictionary *)dictionary;

/**
 * sends the download asset call to the ZMRemoteLibraryService attached to this Manager with a delegate that support UIWebProgress Delegate
 */
- (int) downloadAsset:(ZMAsset *)asset withDelegate:(id)delegate;



@end
