//
//  ZMRating.m
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 25/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZMRating.h"

@implementation ZMRating

@synthesize votes,average,userVote;

-(ZMRating *) initWithRating:(int)votesQty average:(float) voteAverage userVote:(int) usrVote{
    if (self = [super init]) {
        votes=votesQty;
        average=voteAverage;
        userVote=usrVote;
    }
    return self;
}

-(id) copyWithZone: (NSZone *)zone{
    ZMRating *newRating = [[[self class] allocWithZone:zone]init];
    [newRating setVotes:[self votes]];
    [newRating setAverage:[self average]];
    [newRating setUserVote:[self userVote]];
    return(newRating);
}
@end
