//
//  ZMAssetGroup.h
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/1/11.
//  Copyright (c) 2011 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * this object represent a Franchise/Brand in the system.
 * Basically, a Franchise is a ZMAssetsGroup with no related, Otherwise is a Brand and the related is the Franchise
 */

@interface ZMAssetsGroup : NSObject{
}

/**
 * Name of the Franchise/Brand
 */
@property (retain)NSString *name;

/**
 * Dictionary with the additional properties of the Franchise/Brand
 */
@property (retain)NSDictionary *properties;

/**
 * id of the Franchise/BRand
 */
@property long uid;

/**
 * corporate id
 */
@property long corporate_id;

/**
 * refers to a related ZMAssetGroup of this Brand (this will refer a franchise) if null, it means that the current ZMAssetGroup is a franchise
 */
@property (retain)NSArray *related;


@property BOOL selected;

/**
 * starts an ZMassetGroup with the provided parameters
 * @param NSDictionary wiht the aditional properties of the Franchise/Brand
 * @param NSString with the name of the Franchise/Brand
 * @param long with the id of the Franchise/Brand
 * @param NSArray with the related groups / None means a Franchise, array means a Brand with a Franchises in its related.
 */

-(ZMAssetsGroup *) initWithProperties:(NSDictionary *)props name:(NSString *)name uid:(long)i corporate_id:(long)corporate_id related:(NSArray *)sub;

/**
 * return a description of the ZMAssetGroup, concatenating its id, name, properties and related. Usefull on debug puurposes
 * @return NSString with the desciription of the asset
 */
-(NSString *)description;


@end
