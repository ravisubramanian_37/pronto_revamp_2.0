//
//  ZMCategory.h
//  ZMAssetsLibrary
//
//  Created by Andres Ramirez on 12/13/13.
//
//

#import <Foundation/Foundation.h>

@interface ZMCategory : NSObject
    @property long categoryId;
    @property (nonatomic,retain) NSString *categoryName;
    // Revamp - added for bucket selection
    @property (nonatomic,retain) NSString *bucketName;
    @property long bucketId;
@end
