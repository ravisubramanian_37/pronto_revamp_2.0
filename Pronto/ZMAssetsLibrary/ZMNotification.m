//
//  ZMNotification.m
//  ZMAssetsLibrary
//
//  Created by Andres Ramirez on 12/30/13.
//
//

#import "ZMNotification.h"

@implementation ZMNotification

@synthesize assetId;
@synthesize subject;
@synthesize message;
@synthesize action;
@synthesize date;
@synthesize viewed, url;


@end
