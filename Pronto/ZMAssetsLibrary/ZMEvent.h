//
//  ZMEvent.h
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 4/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ZMEventView 1
#define ZMEventRate 2
#define ZMEventLike 3
#define ZMEventUnlike 4

@interface ZMEvent : NSObject

@property long assetId;
@property long eventType;
@property long eventDate;
@property long createdFromBucketId;
@property (nonatomic,retain) NSString *sf;
@property (nonatomic,retain) NSString *eventValue;
@property (nonatomic,retain) ZMEvent *recorded;
-(void) processRecorded;
@end
