//
//  ZMFolder.m
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 22/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZMFolder.h"

@implementation ZMFolder
@synthesize fldr_id,fldr_name,fldr_qty,fldr_date_created,asset_list,metadata,fldr_is_briefcase;
- (void)dealloc
{
    [fldr_name release];
    [asset_list release];[metadata release];
    [super dealloc];
}
@end
