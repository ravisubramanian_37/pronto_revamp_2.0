//
//  ZMAssetsDAO.h
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/2/11.
//  Copyright (c) 2011 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ZMAssetsGroup.h"
#import "ZMAsset.h"
#import "ZMRating.h"
#import "ZMEvent.h"
#import "ZMFolder.h"
#import "ZMNote.h"
#import "ZMCategory.h"
#import "ZMNotification.h"
#import "Asset.h"

/**
 * Object wich connects directly to sqlite database in order to read/write data
 */
@interface ZMAssetsDAO : NSObject{
    @private
        /**
         * stores the database filepath (needed to save sqlite database file)
         */
        NSString *databasePath;
}

/**
 * stores the database filepath
 */
@property (retain) NSString *databasePath;

/**
 * starts the database and creates schema if doesn't exists
 * @param NSString with the path of the sqlite database file
 * @return ZMAssetsDAO with database started
 */
-(ZMAssetsDAO *) initWithPath:(NSString *)path;

/**
 * execute a query in the database
 * @param NSString with the query to execute
 * @param sqlite3 pointer with the active database connection
 * @return bool with the result of the operation
 */
- (BOOL)executeUpdate:(NSString *)query coreDB:(sqlite3 *)coreDB;

/**
 * returns a list with all properties stored in the database
 * @return NSDictinary with the stored key/values from the database
 */
-(NSDictionary *) listProperties;

/**
 * look for a property stored on database. Used to persist variables that can't be saved via plist.
 * @param NSString with the property to look on database
 * @return NSString with the value stored on the property
 */
-(NSString *) findProperty:(NSString *)key;

/**
 * save a property to database. Used to persists variables that can't be saved via plist.
 * @param NSString with the property string to save.
 * @param NSString with the value to save with the property key
 */
-(void) saveProperty:(NSString *)key value:(NSString *)value;


/**
 * add franchise/brand to the database.
 * @param ZMAssetsGroup group to be added on database
 * @param BOOL force update of brands Franchises
 * @return BOOL indicating that the group was created on database
 */
- (BOOL)addGroup:(ZMAssetsGroup *)group full:(BOOL)full;

/**
 * remove a brand from database
 * @param ZMAssetsGroup to delete
 * @return BOOl indicating that the group was deleted successfully
 */
- (BOOL) deleteGroup:(ZMAssetsGroup *)group;

/**
 * remove a franchise from database
 * @param ZMAssetsGroup to delete
 * @return BOOl indicating that the group was deleted successfully
 */
- (BOOL) deleteFranchise:(ZMAssetsGroup *)group;


/**
 * searchs a Franchise/Brand by the id given.
 * @param integer with de id of the franchise/Brand to load
 * @return ZMAssetsGroup the franchise/brand loaded, nil otherwise
 */
- (ZMAssetsGroup *)findGroupById:(long)uid;

/**
 * search the Franchises of a selected brand
 * @param ZMAssetsGroup of the brand selected
 */
-(NSArray *)getBrandsFranchises:(ZMAssetsGroup *)group;


/**
 *  saves the seach on the given folder
 *  @param assetsList list of assets
 *  @param newFolder new folder
 *  @return reference of the folder created
 */
- (ZMFolder *) saveSearchToFolder:(NSMutableArray *) assetsList newFolder:(ZMFolder *)newFolder;

/**
 * search a group on the database based of its group name
 * @param NSString with name of the group to search
 * @return ZMAssetGroup that matchs the search, NIL afterwards
 */
-(ZMAssetsGroup *)groupByName:(NSString *)name;

/**
 * search groups with the specified properties.
 * @param NSDictionary with the properties to be searched on database.
 * @return NSArray with ZMAssetsGroup that match the properties entered
 */
- (NSArray *)findGroupsWithProps:(NSDictionary *)props;

/**
 * get the brands that have the assets provided.
 * @param int Asset id to perform the search
 * @return NSArray with the brands associated to this asset.
 */
- (NSArray *)findGroupsInAsset:(long)uid;

/**
 * get all the groups brands stored on database
 */
- (NSArray *)findGroups;

/**
 * get the franchises/brands asociated with the uid provided. 
 * @param int with the franchise/brand to search. Setting this value in 0 will return the list of the franchises stored, any other number will bring the brands asociated with that id.
 * @return NSArray with the Franchise/Brands that are related with the id provided (0 will return franchises any other number will return the brands of that franchise id)
 */
- (NSArray *)findGroupByRelatedId:(long)uid;


/**
 *  get the franchise from de database according to de name provided.
 *  @param NSString with the franchise name.
 *  @return NSArray with the search results.
 */

- (NSArray *)findGroupByFranchiseName:(NSString *)franchiseName;

/**
 * get a group with the corporateId provided
 * @param NSString with the corporate ID
 * @return ZMAssetsGroup found on database, FALSE afterwards
 */
-(ZMAssetsGroup *)findFranchiseByCorporateID:(NSString *)corporateId;

#pragma mark - Categories

/*
 * Get one category by assetID
 * @param int with the category ID
 * @result NSArray with the categories
 */

- (ZMCategory *) findCategorieByAssetId:(long)uid;

/*
 * Add a new category
 * @param NSString with the new category name
 * @return BOOL indicating that the group was created on database
 */

- (BOOL)addCategory:(ZMCategory *)category;

/*
 * Add a new category Related
 * @param NSInteger with the new category Id
 * @return BOOL indicating that the group was created on database
 */
- (BOOL)addCategoryRelated:(ZMCategory *)category;
/*
 * Delete a category (Only works if this category doesn't have any asset assigned)
 * @param int with the category ID
 * @return BOOL indicating that the category was deleted
 */

- (BOOL)deleteCategory:(long)uid;

/*
 * Update a category
 * @param int with the category ID, NSString with the new cateogry value
 * @return BOOL indicating that the category was updated
 */

-(BOOL) updateCategory:(ZMCategory *)category;

/*
 * Delete all fields of a related category
 * @param int with the asset ID
 * @return BOOL indicating that the category was updated
 */
- (BOOL)deleteRelatedCategory:(long)uid;

/*
 * Return all categories
 * @return BOOL indicating that the category was updated
 */
-(int) getAllCategoriesCount;

/*
 * Return total of unread notifications
 * @return int number of unread notidcations
 */
-(int) getTotalUnreadNotifications;

/*
 * Return category Last Update
 * @return NSString with null or the Last Category Update
 */
- (NSString *)getCategoryDate;

/*
 * Update/insert category Date
 * @return BOOL indicating that the category was updated
 */
- (BOOL)updateCategoryDate:(NSString *)date;

//-(ZMFolder *)addCategorytoAsset:(ZMAsset *)asset folder:(ZMFolder *)folder loadDetails:(BOOL)loadDetails;

#pragma mark - notifications
/*
 * Get one notification by assetID
 * @param int with the notification ID
 * @result ZMNotification with the notifications
 */

- (ZMNotification *) findNotificationById:(long)uid;

/*
 * Insert notification
 * @param ZMNotification Object
 * @result BOOL with the response
 */

- (BOOL) insertNotification:(ZMNotification *)notification;

/*
 * Update notification
 * @param ZMNotification Object
 * @result BOOL with the response
 */

- (BOOL) updateNotification:(ZMNotification *)notification;

/*
 * Update viewed notification
 * @param ZMNotification Object
 * @result BOOL with the response
 */
- (BOOL) updateViewedNotification:(ZMNotification *)notification;


/*
 * Delete notification
 * @param notification ID
 * @result BOOL with the response
 */

- (BOOL) deleteNotification:(long)notificationId;

/*
 * Select last 20 notifications
 * @result Array with notifications objects
 */
- (NSMutableArray *) getLastNotifications;


-(NSArray *) getAllNotifications;


#pragma mark - assets

-(int) totalStoredAssets;

/**
 * search an asset in the database by its uid
 * @param long with the asset id
 @ return ZMAsset with the respective id, nil afterwars
 */
-(ZMAsset *) findAssetById:(long)uid;

/**
 * get POPULAR field of ASSETS Table
 * @param long with the asset id
 @ return POPULAR field of ASSETS Table
 */

-(int)isPopularAsset:(long)uid;

/**
 * search assets from the database with the filters entered in Filter
 * @param ZMAsset with the attributes setted, this will perform a search with the same attributes
 * @param int with the max ammount of results.
 * @return NSArray with the ZMAssets found
 */
-(NSMutableArray *) assetsByFilter:(ZMAsset *)filter withSearchText:(NSString *)searchText sort:(NSString *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit;

-(NSMutableArray *) assetsByFilter:(ZMAsset *)filter withSearchText:(NSString *)searchText sorts:(NSMutableArray *)sorts page:(int) page limit:(int)limit;

-(int) assetsByFilterCount:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sort:(NSString *)sort sortDir:(NSString *)sortDir;

-(int) folderAssetsByFilterCount:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sort:(NSString *)sort sortDir:(NSString *)sortDir;

//OMR
-(int) folderAssetsByCategory:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sorts sortDir:(NSString *)sortDir page:(int) page limit:(int)limit ;
-(NSMutableArray *) afolderAssetsByCategory:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sorts sortDir:(NSString *)sortDir page:(int) page limit:(int)limit ;


/**
 * search assets contained on briefcases with the filters entered in filter
 * @param ZMAsset with the attributes setted, this will permorm a search with the same attrivutes
 * @param long Folder id wich will be searched
 * @param NSString with the sort field desired
 * @param NSString with the sord direction DESC/ASC
 * @param int with the limit of results desired.
 */
-(NSMutableArray *) folderAssetsByFilter:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sort:(NSString *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit;

-(NSMutableArray *) folderAssetsByFilter:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sorts page:(int) page limit:(int)limit;

-(NSMutableArray *) folderAssetsByFilterIds:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sort:(NSString *)sort sortDir:(NSString *)sortDir;

-(NSMutableArray *) selectCategoriesMenu:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id asDictionary:(BOOL) asDictionary;

-(NSMutableArray *) folderAssetsByCategorySelection:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sorts sortDir:(NSString *)sortDir page:(int) page limit:(int)limit;

/**
 * searhs on database for folders that the requested asset was added
 * @param ZAsset with the asset to search
 * @return NSArray with the ZMFolders found
 */
-(NSArray *)assetInFolders:(ZMAsset *)asset;


/**
 * search on database for folder with the same name
 * @param NSString with the folder name
 * @return BOOL Yes for duplicated name
 */
-(BOOL)duplicatedFolderName:(NSString *)folderName;

/**
 * save an assets search into a folder
 * @param NSMUtableDictionary with the assets list
 * @return ZMFolder Created
 */
//- (ZMFolder *) saveSearchToFolder:(NSMutableArray *) assetsList folderName:(NSString *)folderName;

/**
 * search assets from the database with the tags entered in Filter
 * @param NSString with the name of the tag to search
 * @param BOOL TRUE if you want to search from briefcase. FALSE if you want to search from database
 * @param int with the max ammount of results.
 * @return NSArray with the ZMAssets found
 */
-(NSMutableArray *) assetsByTagOrTitle:(NSString *)filter withFilters:(ZMAsset *)assetFilter useBriefCase:(ZMFolder *)folder limit:(int)limit;

/**
 * get tags asociated with the asset id entered
 * @param int asset id to search in the database
 * @return NSArray with NSString tags of the asset
 */
-(NSArray *) getAssetTags:(long)uid;

/**
 * save an asset to the database, replacing all previous content if its necesary
 * @param ZMAsset to save in database
 * @return BOOL with the result of the operation
 */
-(BOOL) addAsset:(ZMAsset *)asset;

/**
 * update an asset to the database, replacion sensitive data (changed, state, path, filechanged, size)
 * @param ZMAsset with the info to update
 * @return BOOL with the result of the operation
 */
-(BOOL) updateAsset:(ZMAsset *)asset;

/**
 * deletes an asset from the database.
 * @param long with the id of the asset to delete
 * @return BOOL with the result of the operation.
 */
-(BOOL) removeAsset:(long)uid;

#pragma mark - Assets Categories Definitions

/**
 * get categories asociated with the asset id entered
 * @param int asset id to search in the database
 * @return NSArray with NSString categories of the asset
 */
-(NSArray *) getAssetCategories:(long)uid;



-(NSMutableDictionary *) getAssetCategoriesByFilter:(NSMutableDictionary *)assets;


/**
 * retrieves a list of events
 */
-(NSArray *)events:(NSString *)type withSaleFranchiseId:(NSString *)saleFranchiseId;

/**
 * creates/updates an event from the asset, first, search from an existing event in the database, later the ZMEvent Object will process it and later the result will be saved on database;
 */

-(BOOL) saveEvent:(ZMEvent *)event;

/**
 * delete an event from the database
 * @param ZMEvent the event to delete
 */
-(BOOL) deleteEvent:(ZMEvent *)event;

/**
 * get a list folders from the database
 * @param BOOL get the folders with the assets withing, FALSE if the folder description is needed only.
 * @return NSArray with ZMFolders on it
 */
-(NSArray *) getAllFolders:(bool)withDetail;

/**
 * get a folder with all its list of assets
 * @param Long with the id to search on the database
 */

-(ZMFolder *) folderWithId:(long)fldr_id;

/**
 * get a list of assets found from a folder
 * @param long folder id to look on database
 * @return NSDictionary with the list of ZMAssets asociated 
 */
-(NSDictionary *) folderAssets:(long)fldr_id;
-(NSArray *) folderAssetsIds:(long)fldr_id;



/**
 * save a folder on database
 * @param ZMFolder with all the infromation to save
 * @param BOOL that will save all the assets form this folder
 * @return BOOL result of the operation
 */

-(ZMFolder *) saveFolder:(ZMFolder *)folder withDetails:(BOOL) details;

/**
 * remove a folder on database
 * @param ZMFolder with will be removed from database
 * @return BOOL result of the operation
 */

-(BOOL) removeFolder:(ZMFolder *)folder;

/**
 * adds an asset to the folder (in terms of database saving)
 * @param ZMAsset, the asset to be added on database
 * @param ZMFolder, the folder that stores this asset
 * @param BOOL if the folder has loaded the header only, please set TRUE on this in order to load the previously added assets.
 * @return ZMFolder with the asset recently added
 */
-(ZMFolder *)addAssetToFolder:(Asset *)asset folder:(ZMFolder *)folder loadDetails:(BOOL)loadDetails;

/**
 * removes an asset to the folder (in terms of database saving)
 * @param ZMAsset, the asset to be removed on database
 * @param ZMFolder, the folder that has this asset
 * @return ZMFolder with the asset recently deleted
 */
-(ZMFolder *)removeAssetToFolder:(Asset *)asset folder:(ZMFolder *)folder;
-(void)deleteAssetFromFolderAssets:(long)assetId folderId:(long)folderid;

/**
 * search for all ZMNotes stored on database
 * @return NSArray of ZMNotes found on database
 */
-(NSArray *)allNotes;

/**
 * adds a note into database
 * @param ZMNote, the note to save on database (nte_id in nil means a new note, if not, it will update that note)
 @ @return the ZMNote that was saved, if it was a creation, nte_id will return the stored id, nil if there was an error
 */ 
-(ZMNote *)saveNote:(ZMNote *)note;

/**
 * deletes a note from the database
 * @param ZMNote with the note to delete, (just the object with the nte_id is enough)
 @return BOOL with TRUE if it was deleted. FALSE afterwards
 */
-(BOOL) deleteNote:(ZMNote *)note;

/**
 * search for a note from the database according to the entered id
 * @param long with the note id
 * @return ZMNote found on database, nil afterwards
 */
-(ZMNote *)noteWithId:(long)id;

/**
 * replace the current folders of the datase int the entered on the parameter folders
 * @param NSArray with the ZMfolders to replace
 */
-(BOOL)replaceBriefcase:(NSArray*)folders;

/**
 * replace the current notes of the datase int the entered on the parameter notes
 * @param NSArray with the ZMfolders to replace
 */
-(BOOL)replaceNotes:(NSArray*)notes;
@end
