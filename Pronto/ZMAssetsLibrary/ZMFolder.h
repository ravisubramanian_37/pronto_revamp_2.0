//
//  ZMFolder.h
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 22/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZMFolder : NSObject

@property (nonatomic) long fldr_id;
@property (nonatomic,retain) NSString *fldr_name;
@property (nonatomic) int fldr_qty;
@property (nonatomic) long fldr_date_created;
@property (nonatomic,retain) NSDictionary *asset_list;
@property (nonatomic,retain) NSDictionary *metadata;
@property (nonatomic) BOOL fldr_is_briefcase;
@end
