//
//  ZMCategory.m
//  ZMAssetsLibrary
//
//  Created by Andres Ramirez on 12/13/13.
//
//

#import "ZMCategory.h"

@implementation ZMCategory
    @synthesize categoryId;
    @synthesize categoryName;
    // Revamp - added for bucket selection
    @synthesize bucketName;
    @synthesize bucketId;
- (void)dealloc
{
    [categoryName release];
    [bucketName release];
    [super dealloc];
}
@end
