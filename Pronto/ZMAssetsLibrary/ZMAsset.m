//
//  ZMAsset.m
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/22/11.
//  Copyright (c) 2011 Abbvie Inc. All rights reserved.
//

#import "ZMAsset.h"

@implementation ZMAsset

@synthesize properties,uid,changed,url,path,fileChanged,size,tags,name,description,state,mimetype,franchises,groups,viewCount,rating,totalVotes,unpublishDate,lastView,thumbnail, publishDate, isPopular;

-(id)copyWithZone:(NSZone *)zone{
    ZMAsset *asset = [[[self class] allocWithZone:zone] init];
    [asset setUid:[self uid]];
    [asset setChanged:[self changed]];
    [asset setUrl:[self url]];
    [asset setPath:[self path]];
    [asset setFileChanged:[self fileChanged]];
    [asset setSize:[self size]];
    [asset setName:[self name]];
    [asset setDescription:[self description]];
    [asset setState:[self state]];
    [asset setMimetype:[self mimetype]];
    [asset setViewCount:[self viewCount]];
    //[asset setRating:[self rating]];
    [asset setTotalVotes:[self totalVotes]];
    [asset setUnpublishDate:[self unpublishDate]];
    [asset setLastView:[self lastView]];
    //complex objects
    [asset setTags:[self tags]];
    [asset setProperties:[self properties]];
    [asset setGroups:[self groups]];
    [asset setRating:[self rating]];
    [asset setCategories:[self categories]];
    [asset setThumbnail:[self thumbnail]];
    [asset setPublishDate:[self publishDate]];
    [asset setIsPopular:[self isPopular]];
    return(asset);
}

-(NSString *)getAssetType{
    NSString *extension = [url pathExtension];
    BOOL isVideo = ([url rangeOfString:@"brightcove://"].location == 0);
    BOOL isImage = [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"jpg|png|gif|jpeg|bmp"] evaluateWithObject:extension];
    // PRONTO-25 - Web View - Ability to provide a Web link as an asset type.
    BOOL isMimeType = ([mimetype isEqualToString:@"weblink"]);
    
    if(isVideo)
    {
        return @"Video";
    }
    if(isImage)
    {
        return @"Image";
    }
    // PRONTO-25
    if(isMimeType)
       return @"weblink";
    
    return @"Document";
}
- (void)dealloc
{
    [properties release];
    [_categories release];
    [description release];
    [groups release];
    [franchises release];[url release];
    [mimetype release];[thumbnail release];
    [name release];[tags release];
    [path release];[rating release];
    [super dealloc];
}
@end
