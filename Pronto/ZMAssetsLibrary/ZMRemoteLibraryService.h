//
//  ZMServiceManager.h
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 12/16/11.
//  Copyright (c) 2011 Zemoga, Inc. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#ifndef ZMRemoteLibraryService_H
#define ZMRemoteLibraryService_H
#import "ZMAsset.h"
#import "ZMFranchise.h"

/**
 A Human String to define the Login Service
 */
#define ZMLoginService @"ZM_LOGIN_SERVICE"
/**
 A Human String to define the User Registration Service
 */
#define ZMUserRegistrationService @"ZM_USER_REGISTRATION_SERVICE"
/**
 A Human String to define the Assets List Service
 */
#define ZMListOfAssetsService @"ZM_LIST_OF_ASSETS_SERVICE"
/**
 A Human String to define the Franchise List Service
 */
#define ZMListOfFranchisesService @"ZM_LIST_OF_FRANCHISES_SERVICE"
/**
 * A Human String to define the event post service
 */
#define ZMEventsPostService @"ZM_EVENTS_POST_SERVICE"
/**
 A Number to represent a HTTP POST REQUEST
 */
#define ZMRequestPOST 1
/**
 A Number to represent a HTTP GET REQUEST
 */
#define ZMRequestGET 2
/**
 A Number to represent a HTTP PUT REQUEST
 */
#define ZMRequestPUT 3
/**
 A Number to represent a HTTP DELETE REQUEST
 */
#define ZMRequestDELETE 4
/**
 Defines common function to translate HTTP REQUEST Type from Integer to String
 */
#define ZMRequestTypeName(x) (x==ZMRequestPOST?@"POST":(x==ZMRequestGET?@"GET":(x==ZMRequestPUT?@"PUT":@"DELETE")))

/**
 * default plotocol methods to login in the remote server (assets server)
 */
@protocol ZMSLibraryServiceDelegate <NSObject>
@optional
- (void)serviceSessionDidStart;
- (void)serviceSessionDidEnd;
- (void)serviceDidFailWithError:(NSString *)service  error:(NSError *)error;
@end

/**
 * Provides methods to synchronize information between server and Application, this includes auth, franchises/brands lists and asset list&download
 */
@interface ZMRemoteLibraryService : NSObject{
    @protected
        /** authenticated flag*/
        BOOL authenticated;
        /** contains a dictionary list with basic parameters, see initWithProperties to view the properties needed */
        NSDictionary* props;
        /** delegate that respond to ZMSLibraryServiceDelegate Protocol */
        id delegate;
    /** used to have in memory the received response */
    NSDictionary *uriResponse;
}

/** */
@property (nonatomic) int lastServerCodeResponse;
@property (nonatomic, retain) NSString *salesFranchiseID;




/**
 *  Init with a list fo properties to connect to the remote server with a list of service to get assets and detail info. 
 * @param props Dictionary with the properties. The properties needed are:
 *  login.uri string with the URL to request a session
 *  login.username string with the login username
 *  login.password string with the login password
 *  assets.uri string with the URL to get assets list.
 *  brands.uri string with the URL to get franchises/brands list.
 *
 */
-(ZMRemoteLibraryService *) initWithProperties:(NSDictionary *)props;

/** returns the current delegate 
 *  @return id of the stored Delegate
 */
- (id)delegate;

/** sets a delegate that have implemented the ZMSLibraryServiceDelegate protocol*/
- (void)setDelegate:(id)newDelegate;

/**
 * check in the loaded props dictionary for an attribute
 * @param NSString with the parameter to look for
 * @param NSError to fill when the property is not found
 * @return NSString with the value of the parameter requested
 */
-(NSString *)checkParameter:(NSString *)parameter error:(NSError **)error;

/**
 * adds a paramerter to the props dictionary
 * @param NSString with the parameter to add
 * @param NSString with the value to add
 * @param NSError to fill if the operation fails
 * @return BOOL if the opearion was successfull
 */
-(BOOL) addParameter:(NSString *)parameter withValue:(NSString *)value error:(NSError **)error;

/**
 * Authenticates the user to the cms, also checks if the session cookie still valid.
 */
-(BOOL) authenticate:(NSError **)error;

/**
 * Transfer the user data to the cms and gets an ID of the user in the CMS
 * @param NSDictionary key/value with the data to pass to the CMS
 */
- (int) loadUser:(NSDictionary *)userData error:(NSError **)error;

/**
 *  Get the content of an uri with the specifics params. This method can overwrite to change the request, or change to local data
 *  @param uri
 *  @param parameters NSDictionary with the parameters to send in the URI
 *  @param requestType integer that represent the request type (1 through 4). See the top of this file to see the Request types
 *  @param error reference to set when can't be retrieved the URL response. 
 *  @return NSString with the server URI response
 */
-(NSString *) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int)requestType error:(NSError **)error;

-(NSString *) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int)requestType headers:(NSDictionary *)headers error:(NSError **)error;

-(void) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int) requestType headers:(NSDictionary *)headers error:(NSError *)error complete:(void(^)(NSString *response, NSError *error))complete;


-(NSString *) requestUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int) requestType headers:(NSDictionary *)headers error:(NSError **)error;


/**
 *  Downloads a given thumbnail and adds a water mark
 *  @param path
 *  @param complete
 *  @param cache
 *  @param waterMark
 */
-(void) downloadThumbnail:(NSString *)path complete:(void(^)(UIImage *thumbnail, NSString *p))complete fromCache:(BOOL)cache includeWaterMark:(NSString *)waterMark;

- (void)downloadThumbnail:(NSString *)path complete:(void(^)(UIImage *thumbnail))complete;


/**
 * used to check if theres a internet connection available
 */
- (BOOL) isConnectionAvailable;

/**
 * Used to create a MD5 hash of the entered string
 * @param input String with the text to make hash
 * @return String with the MD% hash of input
 */
- (NSString *)MD5HashForString:(NSString *)input;

/**
 *  Return asset's Metadata of the remote library.
 *  @param id, dummy variable, not used
 *  @param NSError to be filled if there's one
 *  @return an array with ZMAsset's objects
 */
- (NSArray *) assets:(id)dummy withError:(NSError **)error;

/**
 *   Return asset's Metadata of the remote library. This only returns data that has been created/changed since the param date.
 *   @param date in Unix Format with the desired starting date of creation/update of the assets
 *  @param NSError to be filled if there's one
 *   @return an array with ZMAsset's objects
 */
- (NSArray *) assetsAfterDate:(long)date withError:(NSError **)error;

/** converts a Dictionary to an Asset Object
 *  @param data, a NSDictionary with the values of the asset 
 *  @return ZMAsset with data.
 */
- (ZMAsset*) parseDataToAsset:(NSDictionary*)data;

/**
 * Download the fisical asset into de local device. THIS METHOD MUST BE IMPLEMENTED IN YOUR OWN PROJECT, (due to abbott secure library)
 * @param asset Asset to download
 * @param error a reference to a NSError when the asset can't be downloaded
 * @return Path to de local asset file.
 */
-(NSString *) downloadAsset:(ZMAsset *)asset error:(NSError **)error;

/**
 * The same as downloadAsset but sending a delegate compatible with UIProgressView. @see http://allseeing-i.com/ASIHTTPRequest/How-to-use section Tracking-Progress
 */
-(NSString *) downloadAsset:(ZMAsset *)asset error:(NSError **)error withDelegate:(id)progressDelegate;

-(NSString *) downloadAsset:(ZMAsset *)asset error:(NSError **)error withDelegate:(id)progressDelegate completionHandler:(void(^)(NSString *path, NSError *error)) completionHandler;

/**
 *  Return the brands list of the remote library
 *  @param long. timestamp if you want only the modifications after this date, 0 will bring all the brands
 *  @param NSError error to report in case of it
 *  @return array with ZMBrand's objects
 */
- (NSDictionary *) franchises:(long)date withError:(NSError **)err;

/**
 * synchronize local data with cms
 * @param NSArray with current state of folders in case that we need to synchronize.
 * @param NSString with the date of the folder update date
 * @return NSDictionary with @"folders" and @"notes" if a sync is needed
 */
-(NSDictionary *) syncronize:(NSArray *)folders folderDate:(NSString *)folderDate;

/**
 * uploads a list of ZMEvents to server via JSON POST format
 * @param NSArray with the ZMEvents to send to CMS
 * @return BOOL with TRUE if it was a success operation. FALSE afterwards
 */

-(BOOL)uploadEvents:(NSArray *)events;

/** converts a Dictionary to a Franchise/Brand List
 *  @param data, a NSDictionary with the values of the franchise list
 *  @return ZMFranchise with the data formated
 */
- (ZMFranchise *) parseDataToFranchise:(NSDictionary *)data;

/**
 * return the last Changed timestamp. usefull on assetsAfterDate in order to avoid
 * the same data returned on assets function. This number is generally returned by server and is stored on the uriResponse variable (as a dictionary object) when a Assets list is requested
 * @return an integer with the Unix Format date with the last changed date.
 */
-(int) lastChanged;

/**
 * return an array of assets id that were deleted on server. This array is generaly returned by server and is stored on the uriResponse variable (as a dictionary object) when a Assets list is requested
 *  @return ans Array of integer with the assets id deleted on server.
 */
-(NSArray *) deleted;

/**
 * return an array of assets id that were deleted on server. This array is generaly returned by server and is stored on the uriResponse variable (as a dictionary object) when a Assets list is requested
 *  @return int of integer with the assets that the server reports.
 */
-(int) total;



/**
 *  returns an array of assets id that were unpublished on server. This array is generally returned by server and is stored on the uriResponse variable (as a dictionary object) when a Assets list is requested
 *  @return ans Array of integer with the assets id unpublished on server.
 */
-(NSArray *) unpublished;

/**
 * gets a list of the latest statistics from the CMS (currently ratings and viewcount)
 *  @param id dummy variable, not userd
 *  @param NSError to be filled if there's one
 */
-(NSDictionary *)statistics:(id)dummy withError:(NSError **)error;


/**
 * register the device to the system for push notification
 * @param NSString device token
 * @param NSString upi shared from the security framework
 * @param NSError to be filled if there's one
 */
-(NSDictionary *)registerToken:(NSString *)deviceToken upi:(NSString *)upi withError:(NSError **)error;



/**
 * unregister the device to the system for push notification
 * @param NSString device token
 * @param NSError to be filled if there's one
 */
-(NSDictionary *)unRegisterToken:(NSString *)deviceToken withError:(NSError **)error;


/**
 * check the application version on the cms
 * @param [Block] onComplete callback function that is executed if the request is successfully call
 * @param [Block] onComplete callback function that is executed there is an error
 **/
-(void)checkVersion:(void(^)(NSDictionary *response))oncomplete onerror:(void (^) (NSError **response))onerror;


/**
 *
 **/
//This method is depreciated (replace by userLogin)
//-(void) authenticate:(void(^)(NSString *token))complete error:(void(^)(NSError *error))onError;

/**
 *
 **/
- (void) loadUser:(NSDictionary *)userData complete:(void(^)(int userId))complete error:(void(^)(NSError *error))onError;


/**
 * Gets the categories from the server
 * @param [Block] onComplete callback function that is executed if the request is successfully call
 * @param [Block] onComplete callback function that is executed there is an error
 **/
//-(void) getCategories:(void(^)(NSDictionary *categories))complete error:(void(^)(NSError *error))onError;
-(void) getCategories:(NSString *) categoriesLastUpdate complete:(void(^)(NSDictionary *categories))complete error:(void(^)(NSError *error))onError;

/**
 * Gets the notifications from the server
 * @param [Block] onComplete callback function that is executed if the request is successfully call
 * @param [Block] onComplete callback function that is executed there is an error
 **/
- (void) getNotifications:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError whithDate:(NSString *)date;


/**
 * Gets the assets from the server
 * @param NSDictionary options parameter sent to the server example: @{@"date": @"0", @"per_page" : @"2", @"page", @"20"};
 * @param [Block] onComplete callback function that is executed if the request is successfully call
 * @param [Block] onComplete callback function that is executed there is an error
 **/
-(void) getAssetsWithOptions:(NSDictionary *)options complete:(void(^)(NSDictionary *assets))complete error:(void(^)(NSError *error))onError;


/**
 *  Marks the notification as read
 *  @param notificationId
 *  @param complete
 *  @param onError
 */
- (void)markNotificationsAsRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError;

- (void)markNotificationsAsUNRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError;

/**
 *  Gets the most popular asses
 *  @param error reference for error
 *  @return asset information as a dictionary
 */
-(NSDictionary *) getMostPopularAsset:(NSError **)error;

- (NSError *) getErrorWithCode:(NSInteger)code alternateMessage: (NSString *) aMessage;

-(NSString *) getThumbnailPath:(NSString *)externalPath;


@end
#endif 
