//
//  ZMAssetGroup.m
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/1/11.
//  Copyright (c) 2011 Abbvie Inc. All rights reserved.
//

#import "ZMAssetsGroup.h"

@implementation ZMAssetsGroup

@synthesize name, properties, uid, related, corporate_id, selected;

-(ZMAssetsGroup *) initWithProperties:(NSMutableDictionary *)props name:(NSString *)n uid:(long)i corporate_id:(long)corp_id related:(NSArray *)sub{
    if((self=[super init])!=nil){
        properties = [props copy];
        name = [n copy];
        uid = i;
        corporate_id = corp_id;
        related = [sub copy];
    }
    return self;
}

-(void) dealloc{
    [name release];
    [properties release];
    [related release];
    [super dealloc];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%ld (%ld) - %@ \n \
                      properties: %@\n\
                      related: %@",
                      [self uid],
                      [self corporate_id],
                      [self name],       
                      [self properties],
                      [self related]];     
}

-(id)copyWithZone:(NSZone *)zone{
    ZMAssetsGroup *group = [[[self class] allocWithZone:zone] init];
    [group setUid:[self uid]];
    [group setCorporate_id:[self corporate_id]];
    [group setProperties:[self properties]];
    [group setName:[self name]];
    [group setRelated:[self related]];    
    return(group);
}

@end
