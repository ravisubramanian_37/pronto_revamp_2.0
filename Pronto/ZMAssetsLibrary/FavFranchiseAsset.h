//
//  FavFranchiseAsset.h
//  Pronto
//
//  Created by cccuser on 28/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavFranchiseAsset : NSObject

@property (retain)NSNumber *asset_ID;

@property (retain)NSString *bucketName;

@property (retain)NSNumber *bucketID;

@end
