//
//  ZMEvent.m
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 4/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZMEvent.h"
#import "Pronto-Swift.h"

@implementation ZMEvent
@synthesize assetId,eventType,eventDate,eventValue,recorded,sf,createdFromBucketId;

-(void) processRecorded{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Event bucket id -> %ld",self.createdFromBucketId]];
    switch (eventType) {
        case ZMEventRate:
            //we just do nothing, it will override the recorded rating with the new one;
            break;
        case ZMEventView:
            //lets add a new view (+1) to recorded value
            eventValue = [NSString stringWithFormat:@"%d",[recorded.eventValue intValue]+[eventValue intValue]];
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"EventValue %@", eventValue]];
            break;
        case ZMEventLike:
            [AbbvieLogging logInfo:@"Event Like!"];
            break;
        case ZMEventUnlike:
            [AbbvieLogging logInfo:@"Event UnLike!"];
            break;

    }
}
- (void)dealloc
{
    [sf release];
    [eventValue release];[recorded release];
    [super dealloc];
}
@end
