//
//  ZMAsset.h
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/22/11.
//  Copyright (c) 2011 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMRating.h"

#define ZMAssetStateRemote              1
#define ZMAssetStateLocal               2
#define ZMAssetStateToUpdate            3
#define ZMAssetStateToDownload          4
#define ZMAssetStateDownloading         5

/**
 * Used to model the ZMAsset Object. It only has its attributes and its accesed via @synthesize function
 */
@interface ZMAsset : NSObject{
}

/**
 * used to store aditional properties that will not need any type of sort or querying
 */
@property (retain) NSDictionary *properties;

/**
 * id (code) of the asset
 */
@property long uid;

/**
 * last update date
 */
@property long changed;

/**
 * remote url wich the asset file is stored
 */
@property (retain)NSString *url;

/**
 * local filepath wich the asset file is stored.
 */
@property (retain)NSString *path;

/**
 * last file update date
 */
@property long fileChanged;

/**
 * registered unpublish date
 */
@property long unpublishDate;

/**
 * file size of the asset file
 */
@property long size;

/**
 * array with the tags that has the asset
 */
@property (retain)NSArray *tags;

/**
 * name of the asset
 */
@property (retain)NSString *name;

/**
 * description of the asset
 */
@property (retain)NSString *description;

/**
 * state if the asset (See the top of this file to see the available states
 */
@property int state;

/**
 * mimetype of the asset file
 */
@property (retain)NSString *mimetype;

/**
 * array with the franchises that has the asset (used on filters only)
 */
@property (retain)NSArray *franchises;

/**
 * array with the brands that has the asset
 */
@property (retain)NSArray *groups;

/**
 * how many times the asset was viewed
 */
@property long viewCount;

/**
 * the overall rating of the asset 
 */
@property (retain)ZMRating *rating;

/**
 * the total of votes that rated the asset
 */
@property long totalVotes;

/**
 * the timestamp with last viewed on the session
 */
@property long lastView;

/**
 * returns the asset type
 */
-(NSString *)getAssetType;

/**
 * categories assigned to one asset
 */
@property (retain)NSArray *categories;

/**
 * thumbnail of the asset
 */
@property (retain)NSString *thumbnail;

/**
 * publish date of the asset
 */
@property long publishDate;

/**
 * determine if the asset is popular or not
 */
@property BOOL isPopular;

@end
