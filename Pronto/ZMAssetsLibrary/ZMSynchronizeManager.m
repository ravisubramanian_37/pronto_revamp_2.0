//
//  ZMSynchronizeManager.m
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/26/11.
//  Copyright (c) 2011 Zemoga Inc. All rights reserved.
//

#import "ZMSynchronizeManager.h"
#import "ZMAssetsLibrary.h"
#import "ZMAsset.h"
#import "SBJsonWriter.h"

/**
 * a number to represent the thread is running
 */
#define ZMSynchronizeThreadRun 1
/**
 * a number to represent the thread is stopped
 */
#define ZMSynchronizeThreadStop 2

/**
 * a number to limit the quantity of download to queue
 */
#define ZMQueueLimit 100

/**
 * a number to limit the parallel downloads allowed at the same time
 */
#define ZMConcurrentDownloadLimit 4

static ZMSynchronizeManager *instance = nil;


typedef void (^AssetCompletionBlock) (void);



/**
 * used to handle a thread that synchronizes assets/franchises/brands and downloads
 */

@interface ZMSynchronizeThread : NSObject{
@private
    
    AssetCompletionBlock assetsQueue;
    
    
    /**
     * defines if the current Thread is running or not
     */
    BOOL state;
    
    ZMAsset * assetInDownload;
    
    /**
     * the ZMRemoteLibraryService wich helps to connect with remote server
     */
    ZMRemoteLibraryService *remoteLibrary;
    
    /**
     * amount of seccond in order to do the start function again
     */
    long timeToUpdate;
    
    /**
     * an array that stores the current asset downloads an its progress. also stores the pending downloads. Used to mantain the asset file downloads on background
     */
    NSMutableArray *queue;
    
    /**
     * used to store the ammount of active process (threads) downloading assets
     */
    int iConcurrentProcess;
    
    
    int popularAsset;
    
}
/**
 * Starts a Thread with the ZMRemoteLibraryService entered and synchronize assets/brands in periods of enterd time
 * @param remoteLibrary ZMRemoteLibraryService that helps to connect with the remote server
 * @param timeToUpdate long in secconds used to trigger the start function. 
 * @param noSync BOOL that checks if the thread starts inmediately or later by manual execution. 
 * @return the ZMSynchronizeThread with a thread started
 */
- (id)initWithRemoteLibrary:(ZMRemoteLibraryService *)r timeToUpdate:(long)t noSync:(BOOL)noSync;
/* same as initWithRemoteLibrary but just asks for a remotelibrary and if the thread starts inmediately*/
-(id)initWithRemoteLibrary:(ZMRemoteLibraryService *)r noSync:(BOOL)noSync;

/* same as initWithRemoteLibrary but just asks for a remotelibrary and the thread dont start inmediately*/
- (id)initWithRemoteLibrary:(ZMRemoteLibraryService *)r;
/* same as initWithRemoteLibrary but just asks for a remotelibrary and thread time, the thread starts inmediately*/
- (id)initWithRemoteLibrary:(ZMRemoteLibraryService *)r timeToUpdate:(long)t;

/**
 * starts the queue by check on database wich assets are pending to download (using ZMAssetsLibrary Singleton class) with a limit defined on ZMQueueLimit
 */
-(void)initQueue;

/**
 * check database looking for assets to be download/update and add them to the queue
 **/
-(void)checkQueue;

/**
 * check the current queue and starts a download asset thread if its available
 */
- (void)processQueue;

/**
 * adds and asset to a download queue in order to download its file
 * @param ZMAsset asset with the file url to download file
 */
- (void)addQueue:(ZMAsset *)asset;


/**
 * downloads an asset file to local filesystem and updates its state to downloaded
 * @param ZMAsset the asset with the file url to download
 */
- (void)downloadAsset:(ZMAsset *)asset;

/**
 * same as downloadAsset but with a delegate that behaves like UIProgressViews. @see http://allseeing-i.com/ASIHTTPRequest/How-to-use section Tracking Progress
 * @param ZMAsset the asset with the file url to download
 * @param UIProgressView Delegate
 */
- (void)downloadAsset:(ZMAsset *)asset withDelegate:(id)delegate;

/**
 * replaces the queue as a download only-one asset tpye
 */
-(int) startDownload:(ZMAsset *)asset withDelegate:(id)delegate;

/**
 * prepares a JSON string to upload to server in order to notify updates.
 * @return BOOL with TRUE if it was a success operation. FALSE afterwards
 */
-(BOOL)refreshEvents;

/**
 * gets the recent views and ratings in order to sincronize them on assets
 * @return BOOL with TRUE if it was a success operation. FALSE afterwards
 */
-(BOOL) refreshStats;

/**
 * synchronize the user data stored on ipad to CMS.
 */
-(BOOL) synchronizeUserData;


/** 
 * downloads the Franchise/Brands Tree and save it on the database (as Singleton of ZMAssetsLibrary)
 * @return BOOL with TRUE if it was a success operation. FALSE afterwards
 */
-(BOOL)synchronizeBrands;

/**
 * connects to remotelibrary and downloads the assets metadata (title, dates, url, etc) and save/update it to the database (as Singleton of ZMAssetsLibrary). Also deletes the unpublished and deleted assets send form the server.
 * @param BOOL with FALSE if you want to sync only updates
 * @return BOOL with TRUE if it was a success operation. FALSE afterwards
 */
-(BOOL)synchronizeAssets:(BOOL)fullSync;

/**
 * starts function send from thead in order to update Brands, Assets and DownloadQueue
 * it a infinite loop (unless the variable state is on ZMSynchronizeThreadStop) with a timeToUpdate sleep.
 */
-(void)start;

/**
 * executes synchronization only once
 */
-(void)oneTimeSync;

/**
 * sends a stop command that puts state on ZMSynchronizeThreadStop and make the start function loop to end.
 */
-(void)stop;



@end

@implementation ZMSynchronizeThread
BOOL inSync;
// stores the current page of the assets
int currentPage;
BOOL assetAdded;
NSMutableDictionary *mutableAssets;
NSArray *allKeys;
int currentIndex;

- (id)initWithRemoteLibrary:(ZMRemoteLibraryService *)r timeToUpdate:(long)t noSync:(BOOL)noSync{
    self = [[super init] retain];
    if (self) {
        remoteLibrary = r;
        state = ZMSynchronizeThreadRun;
        if(t>0){
            timeToUpdate=t;
        }
        if(!noSync){
            [self performSelectorInBackground:@selector(start) withObject:nil];
        }else{
            [self oneTimeSync];
            
        }
    }
    return self;
}

- (id)initWithRemoteLibrary:(ZMRemoteLibraryService *)r{
    return [self initWithRemoteLibrary:r timeToUpdate:0 noSync:TRUE];
}
- (id)initWithRemoteLibrary:(ZMRemoteLibraryService *)r timeToUpdate:(long)t{
    return [self initWithRemoteLibrary:r timeToUpdate:t noSync:FALSE];
}

-(id)initWithRemoteLibrary:(ZMRemoteLibraryService *)r noSync:(BOOL)noSync{
    return [self initWithRemoteLibrary:r timeToUpdate:0 noSync:noSync];
}

-(void)initQueue{
    queue = [[[NSMutableArray alloc] init] retain];
    /*
     * queue was disabled by request. This code could be usefull in future. Instead of deleting, lets comment it.
     */
    /*
    iConcurrentProcess = 0;
    //    limit:(int)limit
    ZMAsset *filter = [[ZMAsset alloc] init];
    filter.state = ZMAssetStateDownloading;
    NSArray *assets = [[[ZMAssetsLibrary defaultLibrary] assetsWithProperties:filter limit:ZMQueueLimit] copy];
    [filter release];
    for(ZMAsset *asset in assets){
        asset.state = ZMAssetStateToDownload;
        [[ZMAssetsLibrary defaultLibrary] updateAsset:asset];
    }
    [queue addObjectsFromArray:assets];
    [assets release];
    [self processQueue];
     */
}


-(void)checkQueue{
    /*
     * queue was disabled by request. this code could be usefull in future. instead of deleting, lets comment it
     */
    /*
    @synchronized(self){
        if([queue count] == 0){
            ZMAsset *filter = [[ZMAsset alloc] init];
            filter.state = ZMAssetStateToUpdate;
            [queue addObjectsFromArray:[[ZMAssetsLibrary defaultLibrary] assetsWithProperties:filter limit:ZMQueueLimit]];
            [filter release];
        }
    }
    [self processQueue];
    */
}

- (void) processQueue{
    /*
     * queue was disabled by request. this code could be usefull in future. instead of deleting, lets comment it
     */
    /*
    while(state == ZMSynchronizeThreadRun){
        @synchronized(self){
            if(iConcurrentProcess >= ZMConcurrentDownloadLimit || [queue count] == 0){
                return;
            }
            ZMAsset *next = [[queue lastObject] retain];
            [queue removeLastObject];
            if(next == nil){
                return;
            }
            iConcurrentProcess++;
            if([next.mimetype rangeOfString:@"video"].location == NSNotFound){
                [self performSelectorInBackground:@selector(downloadAsset:) withObject:next];
            }else{
                iConcurrentProcess--;
            }
            [next release];
        }
    }
     */
}

- (void)addQueue:(ZMAsset *)asset{
    /*
     * queue was disabled by request. this code could be usefull in future. instead of deleting, lets comment it.
     */
    NSException* myException = [NSException
                                exceptionWithName:@"QueueDisabled"
                                reason:@"Queue Disabled By Request"
                                userInfo:nil];
    @throw myException;
    // [myException raise]; /* equivalent to above directive */
    /*
    @synchronized(self){
        [queue addObject:asset];
    }
    [self processQueue];
     */
}

-(void)downloadAsset:(ZMAsset *)asset withDelegate:(id)delegate{
    @autoreleasepool{
        // check the asset
        if (remoteLibrary) {
            if (![remoteLibrary isConnectionAvailable]) {
                [[ZMAssetsLibrary defaultLibrary] notifyDownloadFailed:asset error:@"No internet connection detected"];
                assetInDownload = nil;
                return;
            }
        }
        @synchronized(self){
            asset = [[[ZMAssetsLibrary defaultLibrary] assetWithUID:[asset uid]] retain];
            
            if (asset.state != ZMAssetStateToDownload && asset.state != ZMAssetStateRemote && asset.state != ZMAssetStateToUpdate){
                assetInDownload = nil;
                [asset release];
                return;
            }
            asset.state = ZMAssetStateDownloading;
            [[ZMAssetsLibrary defaultLibrary] updateAsset:asset];
        }

        // download the asset
        NSError *error = nil;
        NSLog(@"start thread download asset %@ - %@", asset.name, asset.url);
        
        [remoteLibrary downloadAsset:asset error:&error withDelegate:delegate completionHandler:^(NSString *path, NSError *error) {
            NSLog(@"The asset %@", asset);
            
            if (!error) {
                if (path) {
                    if (asset) {
                        asset.path = path;
                        asset.state = ZMAssetStateLocal;
                        [[ZMAssetsLibrary defaultLibrary] notifyDownloadCompleted:asset];
                        [[ZMAssetsLibrary defaultLibrary] updateAsset:asset];
                        @synchronized(self){
                            iConcurrentProcess--;
                            assetInDownload = nil;
                        }
                    }
                }
            }
            else {
                [[ZMAssetsLibrary defaultLibrary] notifyDownloadFailed:asset error:[error localizedDescription]];
                @synchronized(self){
                    iConcurrentProcess--;
                    assetInDownload=nil;
                }
            }
        }];
        
        [asset release];
        NSLog(@"end thread download asset %@ - %@", asset.name, asset.url);
    }
}

-(void)downloadAsset:(NSDictionary *)dictionary{
    [self downloadAsset:[dictionary objectForKey:@"asset"] withDelegate:[dictionary objectForKey:@"delegate"]];
}

-(int) startDownload:(ZMAsset *)asset{
    return [self startDownload:asset withDelegate:nil];
}

-(int) startDownload:(ZMAsset *)asset withDelegate:(id)delegate{
    @synchronized(self){
        if (assetInDownload == nil) {
            
            if (asset.state == 5) {
                asset.state = ZMAssetStateRemote;
                [[ZMAssetsLibrary defaultLibrary] updateAsset:asset];
            }
            assetInDownload = [asset copy];
            
            if([asset.mimetype rangeOfString:@"video"].location == NSNotFound){
                [self performSelectorInBackground:@selector(downloadAsset:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:asset, @"asset", delegate, @"delegate", nil]];
            }
            else{
                assetInDownload = nil;
                return -1;
            }
            return 0;
        }
        else{
            return 2;
        }
    }
}

-(BOOL)refreshEvents{
    @synchronized([ZMSynchronizeManager instance]){
        //get events data from database
        
        if (remoteLibrary && remoteLibrary.salesFranchiseID) {
            NSArray *events = [[ZMAssetsLibrary defaultLibrary] events:@"JSON" withSaleFranchiseId:remoteLibrary.salesFranchiseID];
            if ([events count] > 0) {
                
                if([remoteLibrary uploadEvents:events]){
                    
                    for(ZMEvent *event in events){
                        [[ZMAssetsLibrary defaultLibrary] deleteEvent:event];
                    }
                    return YES;
                }
                else{
                    return NO;
                }
            }
            
            return YES; //no events to SYNC
        }
        
        return NO;
    }
}

-(BOOL)synchronizeUserData{
    @synchronized([ZMSynchronizeManager instance]){
        BOOL completed = TRUE;
        NSMutableArray *folders = [NSMutableArray arrayWithArray:[[ZMAssetsLibrary defaultLibrary] getAllFolders:TRUE]];
        [folders addObject:[[ZMAssetsLibrary defaultLibrary]folderWithId:0]];
        NSString *folderDate = [[ZMAssetsLibrary defaultLibrary] property:@"FOLDERS_LASTUPDATE"];
        if((long)[folderDate longLongValue]>(long)[[NSDate date] timeIntervalSince1970]){
            folderDate = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
        }
        NSDictionary *toReplace = [remoteLibrary syncronize:folders folderDate:folderDate];
        if(toReplace!=nil){
            if([[toReplace objectForKey:@"folders"] count]>0){
                completed &=[[ZMAssetsLibrary defaultLibrary] replaceBriefcase:[toReplace objectForKey:@"folders"]];
                [[ZMAssetsLibrary defaultLibrary] setProperty:@"FOLDERS_LASTUPDATE" value:[toReplace objectForKey:@"folders_date"]];
            }
            NSString *refreshTime = [toReplace objectForKey:@"refreshTime"];
            if(refreshTime){
                timeToUpdate = (long)[refreshTime longLongValue];
            }
        }else{
            completed = FALSE;
        }
        return completed;
    }
}

-(BOOL)synchronizeBrands{
    @synchronized([ZMSynchronizeManager instance]){
        NSDictionary *franchises = nil;
        NSError *error = nil;
        NSString *lastUpdate = [[ZMAssetsLibrary defaultLibrary] property:@"BRANDS_LASTUPDATE"];
        
        franchises = [remoteLibrary franchises:([lastUpdate isEqualToString:@""]?0:(long)[lastUpdate longLongValue]) withError:&error];
        if(error){
            return FALSE;
        }
        for(ZMFranchise *franchise in [franchises objectForKey:@"franchises"]){
            //preprare the group for related and save it first
            ZMAssetsGroup *group = [[[ZMAssetsGroup alloc] initWithProperties:[[[NSDictionary alloc] init] autorelease] name:franchise.name uid:franchise.tid corporate_id:franchise.corporate_id related:nil] autorelease];
            [[ZMAssetsLibrary defaultLibrary] addGroup:group full:NO];
            for(NSMutableDictionary *brand in franchise.brands){
                ZMAssetsGroup *subGroup = [[[ZMAssetsGroup alloc] initWithProperties:[[[NSMutableDictionary alloc] init] autorelease] name:[brand objectForKey:@"name"] uid:[[brand objectForKey:@"tid"] intValue] corporate_id:0 related:[NSArray arrayWithObject:group]] autorelease];
                [[ZMAssetsLibrary defaultLibrary] addGroup:subGroup full:NO];
            }
        }
        if([[franchises objectForKey:@"brands"] count]>0){
            for(NSNumber *group_id in [franchises objectForKey:@"brands"]){
                NSDictionary *group = [[franchises objectForKey:@"brands"] objectForKey:group_id];
                ZMAssetsGroup *newGroup = [[[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:[group objectForKey:@"name"] uid:(long)[[group objectForKey:@"tid"] longLongValue] corporate_id:0 related:nil] autorelease];
                NSMutableArray *groupFranchises = [NSMutableArray array];
                for (NSString *franchiseID in [group objectForKey:@"franchises"]){
                    ZMAssetsGroup *newFranchise = [[ZMAssetsLibrary defaultLibrary] groupWithUID:(long)[franchiseID longLongValue]];
                    if(newFranchise!=nil){
                        [groupFranchises addObject:newFranchise];
                    }
                }
                newGroup.related=groupFranchises;
                [[ZMAssetsLibrary defaultLibrary] addGroup:newGroup full:YES];
            }
        }
        for(NSNumber *franchise_id in [franchises objectForKey:@"deleted"]){
            ZMAssetsGroup *toDelete;
            toDelete = [[ZMAssetsLibrary defaultLibrary] groupWithUID:[franchise_id longValue]];
            if([toDelete.related count]>0){
                [[ZMAssetsLibrary defaultLibrary] deleteGroup:toDelete];
            }else{
                [[ZMAssetsLibrary defaultLibrary] deleteFranchise:toDelete];
            }
        }
        [[ZMAssetsLibrary defaultLibrary] setProperty:@"BRANDS_LASTUPDATE" value:[franchises objectForKey:@"lastDate"]];
        return TRUE;
    }
}

-(BOOL)refreshStats{
    @synchronized([ZMSynchronizeManager instance]){
        NSError *error = nil;
        NSDictionary *updates = [remoteLibrary statistics:[[ZMAssetsLibrary defaultLibrary]property:@"STATS_LASTUPDATE"]  withError:&error];
        if(error){
            return FALSE;
        }
        NSDictionary *assets = [updates objectForKey:@"assets" ];
        if([assets count]){
            for( id key in assets){
                id value = [assets objectForKey:key];
                if([value isKindOfClass:[NSDictionary class]]){
                    NSDictionary *stats = (NSDictionary*)value;
                    
                    if (stats) {
                        
                        if ([stats objectForKey:@"nid"]) {
                            ZMAsset *assetCache = [[ZMAssetsLibrary defaultLibrary] assetWithUID:[[stats objectForKey:@"nid"] intValue]];
                            
                            if ([stats objectForKey:@"views"]){
                               assetCache.viewCount = [[stats objectForKey:@"views"] intValue];
                            }
                            
                            if ([stats objectForKey:@"votes"]){
                                assetCache.rating.votes = [[stats objectForKey:@"votes"] intValue];
                            }
                            
                            if ([stats objectForKey:@"average"]){
                                assetCache.rating.average = [[stats objectForKey:@"average"] intValue];
                            }
                            
                            if ([stats objectForKey:@"userVote"]){
                                assetCache.rating.userVote = [[stats objectForKey:@"userVote"] intValue];
                            }
                         
                            [[ZMAssetsLibrary defaultLibrary] updateAsset:assetCache];
                        }
                    }
                }
            }
        }
        [[ZMAssetsLibrary defaultLibrary] setProperty:@"STATS_LASTUPDATE" value:[NSString stringWithFormat:@"%ld",[[updates objectForKey:@"date"] longValue]]];
        return TRUE;
    }
}


-(void)refreshStats:(void(^)(void))complete error:(void(^)(NSError *error))onError
{
    @synchronized([ZMSynchronizeManager instance]){
        NSError *error = nil;
        NSDictionary *updates = [remoteLibrary statistics:[[ZMAssetsLibrary defaultLibrary]property:@"STATS_LASTUPDATE"]  withError:&error];
        if(error){
            if(error.code){
                onError([remoteLibrary getErrorWithCode:304 alternateMessage:@"The application cannot refresh stats."]);
            }
            else {
                onError([remoteLibrary getErrorWithCode:206 alternateMessage:@"The request was canceled before it could be fulfilled."]);
            }
            return;
        }
        
        NSDictionary *assets = [updates objectForKey:@"assets"];
        if([assets count]){
            for( id key in assets){
                id value = [assets objectForKey:key];
                
                if ([value isKindOfClass:[NSDictionary class]]) {
                    
                    NSDictionary *stats = (NSDictionary*)value;
                    
                    if (stats) {
                        
                        if ([stats objectForKey:@"nid"]) {
                            ZMAsset *assetCache = [[ZMAssetsLibrary defaultLibrary] assetWithUID:[[stats objectForKey:@"nid"] intValue]];
                            
                            if ([stats objectForKey:@"views"]){
                                assetCache.viewCount = [[stats objectForKey:@"views"] intValue];
                            }
                            
                            if ([stats objectForKey:@"votes"]){
                                assetCache.rating.votes = [[stats objectForKey:@"votes"] intValue];
                            }
                            
                            if ([stats objectForKey:@"average"]){
                                assetCache.rating.average = [[stats objectForKey:@"average"] intValue];
                            }
                            
                            if ([stats objectForKey:@"userVote"]){
                                assetCache.rating.userVote = [[stats objectForKey:@"userVote"] intValue];
                            }
                            
                            [[ZMAssetsLibrary defaultLibrary] updateAsset:assetCache];
                        }
                    }
                }
            }
            [[ZMAssetsLibrary defaultLibrary] notifyStatsChanged:assets];
        }
        [[ZMAssetsLibrary defaultLibrary] setProperty:@"STATS_LASTUPDATE" value:[NSString stringWithFormat:@"%ld",[[updates objectForKey:@"date"] longValue]]];
        complete();
    }
}


-(void)synchronizeBrands:(void(^)(void))complete error:(void(^)(NSError *error))onError
{
    //&
    @synchronized([ZMSynchronizeManager instance]){
        NSDictionary *franchises = nil;
        NSError *error = nil;
        NSString *lastUpdate = [[ZMAssetsLibrary defaultLibrary] property:@"BRANDS_LASTUPDATE"];
        franchises = [remoteLibrary franchises:([lastUpdate isEqualToString:@""]?0:(long)[lastUpdate longLongValue]) withError:&error];
        if(error){
            if(error.code){
                onError([remoteLibrary getErrorWithCode:303 alternateMessage:@"The application cannot synchronize brands."]);
            }
            else {
                onError([remoteLibrary getErrorWithCode:206 alternateMessage:@"The request was canceled before it could be fulfilled."]);
            }
            return;
        }
        for(ZMFranchise *franchise in [franchises objectForKey:@"franchises"]){
            //preprare the group for related and save it first
            ZMAssetsGroup *group = [[[ZMAssetsGroup alloc] initWithProperties:[[[NSDictionary alloc] init] autorelease] name:franchise.name uid:franchise.tid corporate_id:franchise.corporate_id related:nil] autorelease];
            [[ZMAssetsLibrary defaultLibrary] addGroup:group full:NO];
            
            for(NSMutableDictionary *brand in franchise.brands){
                ZMAssetsGroup *subGroup = [[[ZMAssetsGroup alloc] initWithProperties:[[[NSMutableDictionary alloc] init] autorelease] name:[brand objectForKey:@"name"] uid:[[brand objectForKey:@"tid"] intValue] corporate_id:0 related:[NSArray arrayWithObject:group]] autorelease];
                [[ZMAssetsLibrary defaultLibrary] addGroup:subGroup full:NO];
            }
        }
        if([[franchises objectForKey:@"brands"] count]>0){
            for(NSNumber *group_id in [franchises objectForKey:@"brands"]){
                NSDictionary *group = [[franchises objectForKey:@"brands"] objectForKey:group_id];
                ZMAssetsGroup *newGroup = [[[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:[group objectForKey:@"name"] uid:(long)[[group objectForKey:@"tid"] longLongValue] corporate_id:0 related:nil] autorelease];
                NSMutableArray *groupFranchises = [NSMutableArray array];
                for (NSString *franchiseID in [group objectForKey:@"franchises"]){
                    ZMAssetsGroup *newFranchise = [[ZMAssetsLibrary defaultLibrary] groupWithUID:(long)[franchiseID longLongValue]];
                    if(newFranchise!=nil){
                        [groupFranchises addObject:newFranchise];
                    }
                }
                newGroup.related=groupFranchises;
                [[ZMAssetsLibrary defaultLibrary] addGroup:newGroup full:YES];
            }
        }
        for(NSNumber *franchise_id in [franchises objectForKey:@"deleted"]){
            ZMAssetsGroup *toDelete;
            toDelete = [[ZMAssetsLibrary defaultLibrary] groupWithUID:[franchise_id longValue]];
            if([toDelete.related count]>0){
                [[ZMAssetsLibrary defaultLibrary] deleteGroup:toDelete];
            }else{
                [[ZMAssetsLibrary defaultLibrary] deleteFranchise:toDelete];
            }
        }
        [[ZMAssetsLibrary defaultLibrary] setProperty:@"BRANDS_LASTUPDATE" value:[franchises objectForKey:@"lastDate"]];
        complete();
    }
}


-(void) synchronizeUserData:(void(^)(void))complete error:(void(^)(NSError *error))onError
{
    @synchronized([ZMSynchronizeManager instance]){
        
        //@ERROR Blank screen
        if ([[ZMAssetsLibrary defaultLibrary] folderWithId:0]) {
            
            NSMutableArray *folders = [NSMutableArray arrayWithArray:[[ZMAssetsLibrary defaultLibrary] getAllFolders:TRUE]];
            [folders addObject:[[ZMAssetsLibrary defaultLibrary] folderWithId:0]];
            
            NSString *folderDate = [[ZMAssetsLibrary defaultLibrary] property:@"FOLDERS_LASTUPDATE"];
            
            if((long)[folderDate longLongValue]>(long)[[NSDate date] timeIntervalSince1970]){
                folderDate = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
            }
            NSLog(@"folders: %@",folders);
            NSLog(@"folderDate: %@",folderDate);
            NSDictionary *toReplace = [remoteLibrary syncronize:folders folderDate:folderDate];
            if(toReplace!=nil){
                if([[toReplace objectForKey:@"folders"] count]>0){
                    [[ZMAssetsLibrary defaultLibrary] replaceBriefcase:[toReplace objectForKey:@"folders"]];
                    [[ZMAssetsLibrary defaultLibrary] setProperty:@"FOLDERS_LASTUPDATE" value:[toReplace objectForKey:@"folders_date"]];
                }
                NSString *refreshTime = [toReplace objectForKey:@"refreshTime"];
                if(refreshTime){
                    timeToUpdate = (long)[refreshTime longLongValue];
                }
                complete();
            }
            else {
                onError([remoteLibrary getErrorWithCode:305 alternateMessage:@"The application could not load assets."]);
            }
        }
        
        else {
            onError([remoteLibrary getErrorWithCode:301 alternateMessage:@"The authentication service cannot be reached."]);
        }
        
    }
}


- (void)refreshEvents:(void(^)(void))complete error:(void(^)(NSError *error))onError {
    
    @synchronized([ZMSynchronizeManager instance]){
        
        //@ERROR Blank screen
        if (remoteLibrary && remoteLibrary.salesFranchiseID) {
            
            NSArray *events = [[ZMAssetsLibrary defaultLibrary] events:@"JSON" withSaleFranchiseId:remoteLibrary.salesFranchiseID];
            if ([events count]>0) {
                
                if([remoteLibrary uploadEvents:events]){
                    for(ZMEvent *event in events){
                        [[ZMAssetsLibrary defaultLibrary] deleteEvent:event];
                    }
                    complete();
                }
                else{
                    onError([remoteLibrary getErrorWithCode:301 alternateMessage:@"The application cannot send user information."]);
                }
            }
            else {
                complete();
            }
        }
    }
}


-(BOOL) processAssetsResponse:(NSDictionary *)assets
{
    BOOL assetsChanged = NO;
    for(id item in assets)
    {
        if([item isEqualToString:@"deleted"] ||
           [item isEqualToString:@"unpublished"])
        {
            NSArray *deleted = [assets objectForKey:item];
            for (id assetId in deleted)
            {
                ZMAsset *assetToDelete = [[ZMAssetsLibrary defaultLibrary] assetWithUID:[assetId intValue]];
                NSError *error;
                if(assetToDelete.path){
                    if([[NSFileManager defaultManager] fileExistsAtPath:assetToDelete.path]){
                        if([[NSFileManager defaultManager] removeItemAtPath:assetToDelete.path error:&error] == YES){
                            NSLog(@"Deleted %@",assetToDelete.path);
                        }else{
                            NSLog(@"Unable to delete %@,%@",assetToDelete.path,error);
                        }
                    }
                }
                assetsChanged = YES;
                [[ZMAssetsLibrary defaultLibrary] removeAssetWithUID:assetToDelete.uid];
            }
        }
    }
    return assetsChanged;
}



-(void) loadAssets:(NSDictionary *)assets withCompleteHandler:(void(^)(void))complete
{
    mutableAssets = [[[NSMutableDictionary alloc] initWithDictionary:assets] retain];
    currentIndex = 0;
    [mutableAssets removeObjectForKey:@"changed"];
    [mutableAssets removeObjectForKey:@"total"];
    [mutableAssets removeObjectForKey:@"deleted"];
    [mutableAssets removeObjectForKey:@"unpublished"];
    [mutableAssets removeObjectForKey:@"pages"];
    allKeys = [[mutableAssets allKeys] retain];
    /** Validates if there is any asset to update just to avoid a crash */
    if( [allKeys count] > 0){
        __block void (^loadAsset)(void) = ^(void){
            id key = [allKeys objectAtIndex:currentIndex];
            NSDictionary *asset = [mutableAssets objectForKey:key];
            ZMAsset *assetObject = [remoteLibrary parseDataToAsset:asset];
            [self addAsset:assetObject];
            currentIndex++;
            if(currentIndex < [allKeys count]){
                loadAsset();
            } else {
                [mutableAssets release];
                [allKeys release];
                complete();
            }
        };
        loadAsset();
    } else {
        complete();
        
    }
}



-(void) synchronizeAssets:(void(^)(void))complete progress:(void(^)(float progressValue))progress error:(void(^)(NSError *error))onError
{
    assetsQueue = ^(void){
        NSString *lastUpdate = [[ZMAssetsLibrary defaultLibrary] property:@"LAST_SYNCHRONIZATION"];
        NSDictionary *options;
        currentPage = (!currentPage)?0:currentPage;
        if(!lastUpdate)
        {
            lastUpdate = @"0";
        }
        options = @{
                    @"date": lastUpdate,
                    @"per_page": @"50",
                    @"page": [NSString stringWithFormat:@"%d", currentPage]
                    };
        [remoteLibrary getAssetsWithOptions:options complete:^(NSDictionary *assets) {
            //ZMAsset *asset;
            if([[assets objectForKey:@"pages"] integerValue] == 0)
            {
                currentPage = 0;
                if([self processAssetsResponse:assets])
                {
                    [[ZMAssetsLibrary defaultLibrary] notifyAssetsChanged];
                }
                complete();
                return;
            }
            float progressV = (currentPage * 100)/[[assets objectForKey:@"pages"] integerValue];
            if(currentPage < [[assets objectForKey:@"pages"] integerValue])
            {
                currentPage++;
                [self loadAssets:assets withCompleteHandler:^{
                    if([self processAssetsResponse:assets]){
                        assetAdded = YES;
                    }
                    [assets release];
                    progress(progressV);
                    assetsQueue();
                }];
               
            } else if(currentPage == [[assets objectForKey:@"pages"] integerValue])
            {
                progress(progressV);
                if(assetAdded)
                {
                    [[ZMAssetsLibrary defaultLibrary] notifyAssetsChanged];
                }
                
                [[ZMAssetsLibrary defaultLibrary] setProperty:@"LAST_SYNCHRONIZATION" value:[assets objectForKey:@"changed"]];
                //[assets release];
                currentPage = 0;
                complete();
            }
        } error:^(NSError *error) {
            currentPage = 0;
            if(error.code){
                onError(error);
            } else {
                onError([remoteLibrary getErrorWithCode:206 alternateMessage:@"The request was canceled before it could be fulfilled."]);
            }
        }];
    };
    assetsQueue();
}


/**
 * Helper for adding an Asset
 **/
-(void) addAsset:(ZMAsset *)asset
{
    ZMAsset *assetCache = [[ZMAssetsLibrary defaultLibrary] assetWithUID:asset.uid];
    if(assetCache.uid == asset.uid)
    {
        //check file and if there is an update, update local
        if((asset.changed > assetCache.changed) || ([asset.groups count] != [assetCache.groups count])){
            //whole asset but update internal markers
            //only if one of the chages are a modified file
            if(asset.fileChanged > assetCache.fileChanged)
            {
                asset.state = ZMAssetStateToUpdate;
            } else {
                //we need to update iPad Local Data
                asset.state = assetCache.state;
                asset.path = assetCache.path;
                asset.lastView = assetCache.lastView;
            }
        }
    }
    else {
        asset.state=ZMAssetStateRemote;
    }
    
    if(asset.state == ZMAssetStateToUpdate || asset.state == ZMAssetStateRemote) {
        asset.state = ZMAssetStateDownloading;
    }
    [[ZMAssetsLibrary defaultLibrary] addAsset:asset];
    assetAdded = YES;
}



-(BOOL)synchronizeAssets:(BOOL)fullSync{
    @synchronized([ZMSynchronizeManager instance]){
        NSString *lastUpdate = [[ZMAssetsLibrary defaultLibrary] property:@"LAST_SYNCHRONIZATION"];
        NSArray* assets = nil;  
        NSError *error = nil;
        if([lastUpdate isEqualToString:@""]||[lastUpdate isEqualToString:@"0"]||fullSync==TRUE){
            assets = [remoteLibrary assets:nil withError:&error];
        }else{
            assets = [remoteLibrary assetsAfterDate:[lastUpdate intValue] withError:&error];
        }
        if(error){
            return FALSE;
        }
        for(ZMAsset *asset in assets){
            //check if it allready exists on database
            ZMAsset *assetCache = [[ZMAssetsLibrary defaultLibrary] assetWithUID:asset.uid];
            if(assetCache.uid == asset.uid){
                //check file and if there is an update, update local
                if((asset.changed > assetCache.changed) || ([asset.groups count] != [assetCache.groups count])){
                    //whole asset but update internal markers
                    //only if one of the chages are a modified file
                    if(asset.fileChanged > assetCache.fileChanged){
                        asset.state = ZMAssetStateToUpdate;
                    }
                    else{
                        //we need to update iPad Local Data
                        asset.state = assetCache.state;
                        asset.path = assetCache.path;
                        asset.lastView = assetCache.lastView;
                    }
                }
            }else{
                asset.state=ZMAssetStateRemote;
            }
            if(asset.state == ZMAssetStateToUpdate || asset.state == ZMAssetStateRemote){
                asset.state = ZMAssetStateDownloading;
            }
            [[ZMAssetsLibrary defaultLibrary] addAsset:asset];
        }
        //delete the CNS deleted assets
        NSArray *deleted = [remoteLibrary deleted];
        if([deleted count]){
            NSError *error;
            for(NSString *toDelete in deleted){
                ZMAsset *assetToDelete = [[ZMAssetsLibrary defaultLibrary] assetWithUID:[toDelete intValue]];
                if(assetToDelete.path){
                    if([[NSFileManager defaultManager] fileExistsAtPath:assetToDelete.path]){
                        if([[NSFileManager defaultManager] removeItemAtPath:assetToDelete.path error:&error] == YES){
                            NSLog(@"Deleted %@",assetToDelete.path);
                        }else{
                            NSLog(@"Unable to delete %@,%@",assetToDelete.path,error);
                        }
                    }
                }
                [[ZMAssetsLibrary defaultLibrary] removeAssetWithUID:[toDelete intValue]];
            }
        }
        //now with unpublished
        NSArray *unpublished = [remoteLibrary unpublished];
        if([unpublished count]){
            NSError *error;
            for(NSString *toDelete in unpublished){
                ZMAsset *assetToDelete = [[ZMAssetsLibrary defaultLibrary] assetWithUID:[toDelete intValue]];
                if(assetToDelete.path){
                    if([[NSFileManager defaultManager] fileExistsAtPath:assetToDelete.path]){
                        if([[NSFileManager defaultManager] removeItemAtPath:assetToDelete.path error:&error] == YES){
                            NSLog(@"Deleted %@",assetToDelete.path);
                        }else{
                            NSLog(@"Unable to delete %@,%@",assetToDelete.path,error);
                        }
                    }
                }
                [[ZMAssetsLibrary defaultLibrary] removeAssetWithUID:[toDelete intValue]];
            }
        }
        [[ZMAssetsLibrary defaultLibrary] setProperty:@"LAST_SYNCHRONIZATION" value:[NSString stringWithFormat:@"%d",[remoteLibrary lastChanged]]];
        int totalAssets = [remoteLibrary total];
        
        int storedAssets = [[ZMAssetsLibrary defaultLibrary] totalStoredAssets];
        if(totalAssets>storedAssets){
            if(!fullSync){
                [self synchronizeAssets:TRUE];
            }else{
                return TRUE;
            }
        }
        return TRUE;
    }
}

-(void)start{
    @autoreleasepool {
        [self initQueue];
    }
    if(timeToUpdate<0){
        [self oneTimeSync];
    }else{
        while(state == ZMSynchronizeThreadRun){
            NSLog(@"IN THREAD");
            [self oneTimeSync];
            sleep(timeToUpdate);
        }
    }
}

- (void)oneTimeSync{
    
    //@Fix 102214
    if ([remoteLibrary isConnectionAvailable]) {
        
        if(!inSync){
            inSync = YES;
            [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Refreshing Events" progress:0];
            [self refreshEvents:^(void) {
                
                NSLog(@">>>> Finished with Events");
                
                [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Synchronizing User Data" progress:0];
                
                [self synchronizeUserData:^(void) {
                    
                    NSLog(@">>>> Finished sycronizing user data ");
                    
                    [self synchronizeBrands:^(void) {
                        
                        NSLog(@">>>> Finished sycronizing brands ");
                        
                        [self refreshStats:^(void) {
                            
                            NSLog(@">>>> Finished sync of the stats");
                            
                            [[ZMAssetsLibrary defaultLibrary] notifyStart];
                            currentPage = 0;
                            [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Getting Assets" progress:0];
                            
                            NSString *categoryLastUpdate = [[ZMAssetsLibrary defaultLibrary] property:@"CATEGORIES_LASTUPDATE"];
                            [remoteLibrary getCategories:categoryLastUpdate complete:^(NSDictionary *categories) {
                                if (categoryLastUpdate != nil){
                                    [self saveCategoryToDao:categories isToUpdate:YES];
                                }else{
                                    [self saveCategoryToDao:categories isToUpdate:NO];
                                }
                                inSync = NO;
                                [[ZMAssetsLibrary defaultLibrary] setProperty:@"CATEGORIES_LASTUPDATE" value:[NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]]];
                                //Sync the assets
                                [self synchronizeAssets:^{
                                    [[ZMAssetsLibrary defaultLibrary] notifyFinish];
                                } progress:^(float progressValue) {
                                    [[ZMAssetsLibrary defaultLibrary] notifyStep:[NSString stringWithFormat:@"Getting Assets %d%%", (int)progressValue] progress:progressValue];
                                    NSLog(@"Download asset progress %f", progressValue);
                                } error:^(NSError *error) {
                                    inSync = NO;
                                    [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
                                }];
                            } error:^(NSError *error) {
                                inSync = NO;
                                NSLog(@">>>> Error getting the categories %@", error);
                                [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
                            }];
                            
                        } error:^(NSError *error) {
                            NSLog(@">>>> Error retrieving stats %@", error);
                            inSync = NO;
                            [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
                        }];
                    } error:^(NSError *error) {
                        NSLog(@">>>> Error with sycronizing brands %@", error);
                        inSync = NO;
                        [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
                    }];
                } error:^(NSError *error) {
                    NSLog(@">>>> Error with sycronizing user data %@", error);
                    inSync = NO;
                    [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
                }];
            } error:^(NSError *error) {
                NSLog(@">>>> Error with Events%@", error);
                inSync = NO;
                [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
            }];
        }
    }
    
}


/*
 * Save the categories obtained from the service to the local Database
 * @param: NSDictionary
 */
- (void) saveCategoryToDao:(NSDictionary *)categories isToUpdate:(BOOL) isUpdate{
    //Save to table Categories_assets
    BOOL response = NO;
    
    NSDictionary *categoriesObject = [categories objectForKey:@"categories"];
    NSDictionary *deletedCategoriesObject =[categories objectForKey:@"deleted"];
    NSDictionary *deletedRelatedCategoriesObject =[categories objectForKey:@"deleted_related"];
    //Insert Action
    for(id key in categoriesObject){
        ZMCategory *category = [[[ZMCategory alloc]init] autorelease];
        id obj = [categoriesObject objectForKey:key];
        category.categoryId = [[obj objectForKey:@"tid"] integerValue];
        category.categoryName = [obj objectForKey:@"name"];
        response = [[ZMAssetsLibrary defaultLibrary] updateCategory:category];
    }
    
    
    
    //Delete action
    for(NSString *key in deletedCategoriesObject){
        response = [[ZMAssetsLibrary defaultLibrary] deleteCategory:[key integerValue]];
    }
    //Delete info in related tables
    for(NSString *key in deletedRelatedCategoriesObject){
        response = [[ZMAssetsLibrary defaultLibrary] deleteRelatedCategory:[key integerValue]];
    }
    
    if (response == YES){
        [[ZMAssetsLibrary defaultLibrary] notifyCategoryChanged];
    }
    
    
    [categories release];
}

-(void)stop{
    state = ZMSynchronizeThreadStop;
}

- (void)dealloc
{
	if(state == ZMSynchronizeThreadRun){
        [self stop];
        state = ZMSynchronizeThreadStop;
    }
    [queue dealloc];
    [[ZMAssetsLibrary defaultLibrary] notifyThreadStop];
	[super dealloc];
}
@end

@implementation ZMSynchronizeManager

+ (ZMSynchronizeManager*)instance 
{
    @synchronized (self) {
        if (instance == nil) {
            instance = [[[self alloc] init] retain];
        }
    }
    return instance;
}

+ (id)allocWithZone:(NSZone *)zone 
{
    @synchronized(self) {
        if (instance == nil) {
            instance = [super allocWithZone:zone];
            return instance;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone 
{
    return self;
}


- (id)init
{
    @synchronized(self) {
        return self;
    }
}

- (void)dealloc
{
    if(state == ZMSynchronizeThreadRun){
        [self stop];
        state = ZMSynchronizeThreadStop;
    }
    [instance dealloc];
	[super dealloc];
}


- (BOOL) state{
    return state;
}
- (void)stop{
    [thread stop];
    thread = nil;
}

-(void) threadStoped{
    [self stop];
}

- (void) oneRunWithRemoteLibrary:(ZMRemoteLibraryService *)remoteLibrary{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    library=remoteLibrary;
    thread = [[ZMSynchronizeThread alloc] initWithRemoteLibrary:remoteLibrary noSync:TRUE];
    [pool release];
}

- (void) startWithRemoteLibrary:(ZMRemoteLibraryService *)remoteLibrary timeToUpdate:(long)t{
    library=remoteLibrary;
    if(thread!=nil)
        [thread stop];
    thread = [[ZMSynchronizeThread alloc] initWithRemoteLibrary:remoteLibrary timeToUpdate:t];
}

//since queue is disabled, i'm changing this to a simple task that holds the current asset downloading an return an int instead of void.
- (int) downloadAsset:(ZMAsset *)asset{
    return [self downloadAsset:asset withDelegate:nil];
}

- (int)downloadAsset:(ZMAsset *)asset withDelegate:(id)delegate{
    if(thread==nil){
        thread = [[ZMSynchronizeThread alloc] initWithRemoteLibrary:library];
    }
    return [thread startDownload:asset withDelegate:delegate];
}

@end
