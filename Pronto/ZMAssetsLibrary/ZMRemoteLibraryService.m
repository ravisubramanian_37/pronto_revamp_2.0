//
//  ZMServiceManager.m
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 12/16/11.
//  Copyright (c) 2011 Zemoga Inc. All rights reserved.
//

#import "ZMRemoteLibraryService.h"
#import "SBJson.h"
#import "ZMAsset.h"
#import "ZMFranchise.h"
#import "ZMEvent.h"
#import "ZMFolder.h"
#import "ZMNote.h"
#import "ZMAssetsGroup.h"
#import "ZMCategory.h"
#import <CommonCrypto/CommonDigest.h>
#import <Foundation/NSBundle.h>

/**
 * contanins an implementation of an NSArray that loads the assets when its needed, also, calls to ZMRemoteLIbraryService in order to transform an NSArray to ZMAsset
 */
@interface ZMParserArrayAssets : NSArray{
@private
    /**
     * stores the initial NSArray content that will be parsed to ZMAsset
     */
    NSArray *result;
    /**
     * service active that process the parsing request
     */
    ZMRemoteLibraryService *service;
}
/**
 * starts the object with the NSArray to ZMAsset based on the ZMRemoteLibraryService provided
 * @param NSArray with the asset content, this will need to be in the same structure of the ZMAsset Object
 * @param ZMRemoteLibraryService service loaded and ready to receive commands.
 * @return self loaded an ready to respond to objectAtIndex
 */
-(ZMParserArrayAssets *) initWithArray:(NSArray *)res service:(ZMRemoteLibraryService *)s;
@end

@implementation ZMParserArrayAssets

-(ZMParserArrayAssets *) initWithArray:(NSArray *)res service:(ZMRemoteLibraryService *)s{
    self = [[super init] retain];
    if (self) {
        self->service = s;
        self->result = res;
    }
    return self;
}

-(void) dealloc{
    [super dealloc];
}

- (NSUInteger)count{
    return [result count];
}
- (id)objectAtIndex:(NSUInteger)index{
    @try{
        return [service parseDataToAsset:[result objectAtIndex:index]];
    }
    @catch (NSException *e) {
        NSLog(@"Exception parsing asset at index %lu: %@",(unsigned long)index,e);
        return [[ZMAsset alloc] init];
    }
}
- (id)lastObject{ return [service parseDataToAsset:[result lastObject]];}
@end

/**
 * contanins an implementation of an NSArray that loads the franchises when its needed, also, calls to ZMRemoteLIbraryService in order to transform an NSArray to ZMFranchise
 */
@interface ZMParserArrayFranchises : NSArray{
@private
    /**
     * stores the initial NSArray content that will be parsed to ZMFranchise
     */
    NSArray *result;
    /**
     * service active that process the parsing request
     */
    ZMRemoteLibraryService *service;
}
/**
 * starts the object with the NSArray to ZMFranchise based on the ZMRemoteLibraryService provided
 * @param NSArray with the asset content, this will need to be in the same structure of the ZMFranchise Object
 * @param ZMRemoteLibraryService service loaded and ready to receive commands.
 * @return self loaded an ready to respond to objectAtIndex
 */
-(ZMParserArrayFranchises *) initWithArray:(NSArray *)res service:(ZMRemoteLibraryService *)s;
@end

@implementation ZMParserArrayFranchises

-(ZMParserArrayFranchises *) initWithArray:(NSArray *)res service:(ZMRemoteLibraryService *)s{
    self = [super init];
    if (self) {
        self->service = s;
        self->result = res;
    }
    return self;
}

-(void)dealloc{
    [super dealloc];
}

- (NSUInteger)count{
    return [result count];
}
- (id)objectAtIndex:(NSUInteger)index{
    return [service parseDataToFranchise:[result objectAtIndex:index]];
}
- (id)lastObject{ return [service parseDataToFranchise:[result lastObject]];}
@end

@implementation ZMRemoteLibraryService

@synthesize lastServerCodeResponse = _lastServerCodeResponse;

- (id)init
{
    @synchronized(self) {
        return self;
    }
}

-(void) dealloc{
    [super dealloc];
}

-(ZMRemoteLibraryService *) initWithProperties:(NSDictionary *)p{
    self = [super init];
    if (self) {
        props = [p copy];
        authenticated = false;
    }
    return self;
}

- (id)delegate {
    return delegate;
}

- (void)setDelegate:(id)newDelegate {
    delegate = newDelegate;
}

-(NSString *)checkParameter:(NSString *)parameter error:(NSError **)error{
    NSString *res = [props objectForKey:parameter];
    if(res==nil){
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:[NSString stringWithFormat:@"Not property found <%@>",parameter] forKey:NSLocalizedDescriptionKey];
        if(error!=nil)
            *error = [NSError errorWithDomain:@"AssetsLibrary" code:101 userInfo:errorDetail];    
    }
    return res;
}

-(BOOL) addParameter:(NSString *)parameter withValue:(NSString *)value error:(NSError **)error{
    BOOL saved = FALSE;
    if([parameter length]==0){
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:[NSString stringWithFormat:@"Not property set <%@>",parameter] forKey:NSLocalizedDescriptionKey];
        if(error!=nil)
            *error = [NSError errorWithDomain:@"AssetsLibrary" code:102 userInfo:errorDetail];
        return saved;
    }
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionaryWithDictionary:props];
    [tempDic setValue:value forKey:parameter];
    props = [[NSDictionary dictionaryWithDictionary:tempDic] copy];
    return true;
}


/**
 *
 **/
//-(void) getCategories:(void(^)(NSDictionary *categories))complete error:(void(^)(NSError *error))onError
-(void) getCategories:(NSString *) categoriesLastUpdate complete:(void(^)(NSDictionary *categories))complete error:(void(^)(NSError *error))onError
{
    
    NSError *error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"category.uri" error:&error]];
    NSString *response = nil;
    if (!categoriesLastUpdate) {
        //Insert Mode
        response = [self requestUri:uri parameters:nil requestType:ZMRequestPOST headers:nil error:&error];
    }
    else {
        //Update
        response = [self requestUri:uri parameters:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",categoriesLastUpdate],@"date", nil] requestType:ZMRequestPOST headers:nil error:&error];
    }
    
    if (error == nil) {
        
        if (response) {
            NSDictionary* categoriesDic = [[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error] retain];
            complete(categoriesDic);
        }
        else {
            onError([self getErrorWithCode:107 alternateMessage:@"The categories are unavailible at this moment."]);
        }
    }
    else {
        
        if (error.code) {
            onError([self getErrorWithCode:107 alternateMessage:@"The categories are unavailible at this moment."]);
        }
        else {
            onError([self getErrorWithCode:206 alternateMessage:@"The request was canceled before it could be fulfilled."]);
        }
    }
}


- (void) getNotifications:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError whithDate:(NSString *)date {
    
    NSError *error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"notification.uri" error:&error]];
    NSString *response = nil;
    NSDictionary *params = @{@"page":@"0", @"per_page":@"80", @"new":@"1" };
    
    if (date) {
        params = @{@"page":@"0", @"per_page":@"80", @"new":@"1", @"date":date};
    }
    
    response = [self requestUri:uri parameters:params requestType:ZMRequestGET headers:nil error:&error];
    
    if (error == nil) {
        
        if (response) {
            NSDictionary *notifications = [[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error] retain];
            complete(notifications);
        }
    }
    else {
        onError([self getErrorWithCode:107 alternateMessage:@"The notifications are unavailible at this moment."]);
    }
    
}

- (void)markNotificationsAsRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError {
    
    NSError *error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@/%d", [self checkParameter:@"server" error:&error], [self checkParameter:@"notification.uri" error:&error], notificationId];
    NSString *response = nil;
    
    response = [self requestUri:uri parameters:nil requestType:ZMRequestGET headers:nil error:&error];
    
    if(error == nil) {
        if (response) {
            NSDictionary *notifications = [[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                           options:0
                                                                             error:&error] retain];
            complete(notifications);
        }
    }
    else {
        onError([self getErrorWithCode:108 alternateMessage:@"markNotificationsAsRead situation"]);
    }
}

- (void)markNotificationsAsUNRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError {
    
    NSError *error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@/%d", [self checkParameter:@"server" error:&error], @"services/privatemsg_unread", notificationId];
    NSString *response = nil;
    
    response = [self requestUri:uri parameters:nil requestType:ZMRequestGET headers:nil error:&error];
    
    if(error == nil) {
        if (response) {
            NSDictionary *notifications = [[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                           options:0
                                                                             error:&error] retain];
            complete(notifications);
        }
    }
    else {
        onError([self getErrorWithCode:108 alternateMessage:@"markNotificationsAsUNRead situation"]);
    }
}

/**
 *
 **/
- (NSError *)getErrorWithCode:(NSInteger)code alternateMessage: (NSString *) aMessage {
    
    NSString *domain = @"com.pronto.security.library";
    NSString *messageString = [NSString stringWithFormat:@"ERROR_CODE_%ld", (long)code];
    NSString *desc = NSLocalizedString(messageString, aMessage);
    NSDictionary *info = @{ NSLocalizedDescriptionKey : desc };
    return [NSError errorWithDomain:domain code:code userInfo:info];
}

/**
 * This method gets the application token necessary to connect and interact with services
 **/
- (void)getToken:(void(^)(NSString *token))complete error:(void(^)(NSError *error))onError {
    NSError *error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"token.uri" error:&error]];
    
    [self contentByUri:uri parameters:nil requestType:2 headers:nil error:error complete:^(NSString *response, NSError *error) {
        
        if (error == nil && response!= nil) {
            uriResponse = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
            NSString *token = (NSString *)[uriResponse valueForKey:@"token"];
            
            //@issue
            [self addParameter:@"login.token" withValue:token error:&error];
            
            complete(token);
        }
        else {
            if (!error.code) {
                onError([self getErrorWithCode:206 alternateMessage:@"The request was canceled before it could be fulfilled."]);
            }
            else {
                onError([self getErrorWithCode:104 alternateMessage:@"The session cannot be initiated."]);
            }
        }
    }];
}

/**
 * Starts the session to Pronto Application
 **/
- (void) startUserSession:(void(^)(NSString *token))complete error:(void(^)(NSError *error))onError
{
    NSError *error = nil;
    NSString *response = nil;
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[self checkParameter:@"login.username" error:&error],@"username", [self checkParameter:@"login.password" error:&error],@"password",nil];
    NSString *uri;
    
    if (error==nil) {
        
        uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"login.uri" error:&error]];
        
        response = [self requestUri:uri parameters:dic requestType:ZMRequestPOST headers:nil error:&error];
        
        if([response length] > 0) {
            
            //Checking if we are getting response from the server
            if ([[[SBJsonParser new] autorelease] objectWithString:response]){
                uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
                uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"token.uri" error:&error]];
                
                
                [self contentByUri:uri parameters:dic requestType:2 headers:nil error:error complete:^(NSString *response, NSError *error) {
                    if(error == nil){
                        uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
                        
                        //@issue
                        [self addParameter:@"login.token" withValue:[uriResponse valueForKey:@"token"] error:&error];
                        
                        complete([uriResponse valueForKey:@"token"]);
                        
                        if([delegate respondsToSelector:@selector(serviceSessionDidStart)]) {
                            //Triggering the ZMRemoteLibrary session start
                            [[self delegate] serviceSessionDidStart];
                        }
                    }
                }];
        
                
            } else
            {
                //There is a problem with the json service error 101 sent
                onError([self getErrorWithCode:101 alternateMessage:@"The session cannot be initiated due to a corrupted json service."]);
            }
        } else
        {
            //If there is an error we need to check if there is a valid session
            if ([[error localizedDescription] rangeOfString:@"Already logged in as"].location != NSNotFound) {
                authenticated = YES;
                error = nil;
                uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"token.uri" error:&error]];
                
                //response = [self requestUri:uri parameters:dic requestType:ZMRequestPOST headers:nil error:&error];
                
                [self contentByUri:uri parameters:dic requestType:2 headers:nil error:error complete:^(NSString *response, NSError *error) {
                
                    if(error == nil){
                        uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
                
                        //@issue
                        [self addParameter:@"login.token" withValue:[uriResponse valueForKey:@"token"] error:&error];
                
                        if([delegate respondsToSelector:@selector(serviceSessionDidStart)]){
                            [[self delegate] serviceSessionDidStart];
                        }
                        complete([uriResponse valueForKey:@"token"]);
                    }
                
                }];
                
            }
            else{
                //If there is an error here means that the server has issues
                onError([self getErrorWithCode:103 alternateMessage:@"The authenticate service cannot be reach."]);
                
                if ([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]) {
                    [[self delegate] serviceDidFailWithError:ZMLoginService error:error];
                }
            }
        }
    } else {
        onError([self getErrorWithCode:102 alternateMessage:@"The session cannot be initiated due to problems with server. (null) response."]);
    }

}

- (void)authenticate:(void(^)(NSString *token))complete error:(void(^)(NSError *error))onError {
    
    [self getToken:^(NSString *token) {
        [self startUserSession:^(NSString *token) {
            complete(token);
        }
        error:^(NSError *error) {
            [self authenticate:complete error:onError];
        }];
    }
    error:^(NSError *error) {
        onError(error);
    }];
}

/**
 *
 **/
- (BOOL)authenticate:(NSError **)error{
    
    NSDictionary *dic = nil;
    NSString *response = nil;
    NSString *uri = [self checkParameter:@"login.uri" error:error];

    //we need a server token first
    response = [self contentByUri:[self checkParameter:@"token.uri" error:error] parameters:dic requestType:ZMRequestPOST error:error];
    if (*error == nil) {
        uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
        [self addParameter:@"login.token" withValue:[uriResponse valueForKey:@"token"] error:error];
    }
    
    //now we can auth.
    if(*error == nil){
        dic = [NSDictionary dictionaryWithObjectsAndKeys:
               [self checkParameter:@"login.username" error:error],@"username",
               [self checkParameter:@"login.password" error:error],@"password",nil];
        if(*error!=nil){
            NSLog(@"ERROR SETTING PROPERTIES");
            return FALSE;

        }
    }
    if(*error == nil){
        response = [self 
                    contentByUri:uri parameters:dic requestType:ZMRequestPOST error:error] ;
    }
    if(*error == nil){
        if([response length] == 0){
            NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
            [errorDetail setValue:[NSString stringWithFormat:@"Can't login into the CMS"] forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"AssetsLibrary" code:102 userInfo:errorDetail];    
        }
        else {
            
            if ([[[SBJsonParser new] autorelease] objectWithString:response]){
                 uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
                
                response = [self contentByUri:[self checkParameter:@"token.uri" error:error] parameters:dic requestType:ZMRequestPOST error:error];
                
                if(*error == nil){
                    uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
                    [self addParameter:@"login.token" withValue:[uriResponse valueForKey:@"token"] error:error];
                    if([delegate respondsToSelector:@selector(serviceSessionDidStart)]){
                        [[self delegate] serviceSessionDidStart];
                    }
                }
            }
        }
    }
    if(*error != nil){
        NSLog(@"--> login response %@ ", [*error localizedDescription]);
        if([[*error localizedDescription] rangeOfString:@"Already logged in as"].location != NSNotFound){
            authenticated = YES;
            *error = nil;
            response = [self contentByUri:[self checkParameter:@"token.uri" error:error] parameters:dic requestType:ZMRequestPOST error:error];
            
            if(*error == nil){
                uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
                [self addParameter:@"login.token" withValue:[uriResponse valueForKey:@"token"] error:error];
                if([delegate respondsToSelector:@selector(serviceSessionDidStart)]){
                    [[self delegate] serviceSessionDidStart];
                }
            }
        }else{
            if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){
                [[self delegate] serviceDidFailWithError:ZMLoginService error:*error];
            }
            NSLog(@"Error ocured: %@",*error);
        }
    }
    authenticated = (*error == nil);
    return authenticated;    
}

- (int)loadUser:(NSDictionary *)userData error:(NSError **)error{
    int userId=0;
    NSString *response = nil;
    NSString *uri = [self checkParameter:@"userload.uri" error:error];
    NSString *userDataString = [[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userData options:NSJSONWritingPrettyPrinted error:error] encoding:NSUTF8StringEncoding] autorelease];

    NSDictionary *dict = @{@"userData": userDataString};
    if(*error == nil){
        response = [self contentByUri:uri parameters:dict requestType:ZMRequestPOST error:error];
        NSLog(@"Response: %@",response);
    }
    if(*error == nil){
        if([response length] == 0){
            NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
            [errorDetail setValue:[NSString stringWithFormat:@"Can't register user in the CMS. No Response from Server"] forKey:NSLocalizedDescriptionKey];
            *error = [NSError errorWithDomain:@"AssetsLibrary" code:104 userInfo:errorDetail];
            
            if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){
                [[self delegate] serviceDidFailWithError:ZMUserRegistrationService error:*error];
            }
            NSLog(@"Error ocured: %@",*error);
        }
        else{
            uriResponse = nil;
            uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
            userId = [[uriResponse valueForKey:@"user"] intValue];
        }
    }
    else {
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:[NSString stringWithFormat:@"Can't register user in the CMS. Connection Error"] forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"AssetsLibrary" code:103 userInfo:errorDetail];
        if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){
            [[self delegate] serviceDidFailWithError:ZMUserRegistrationService error:*error];
        }
        NSLog(@"Error ocured: %@",*error);
    }
    return userId;
}


- (void)loadUser:(NSDictionary *)userData complete:(void(^)(int userId))complete error:(void(^)(NSError *error))onError {
    
    __block int userId=0;
    NSString *response;
    NSError *error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"userload.uri" error:&error]];
    NSString *userDataString = [[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userData options:NSJSONWritingPrettyPrinted error:&error]
                                                      encoding:NSUTF8StringEncoding] autorelease];
    
    NSDictionary *dict = @{@"userData":userDataString};
    
    if (*&error == nil) {
        
        response = [self requestUri:uri parameters:dict requestType:ZMRequestPOST headers:nil error:&error];
        NSLog(@"Response: %@",response);

        if (response) {

            if ([response length] == 0) {
                
                NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
                [errorDetail setValue:[NSString stringWithFormat:@"Can't register user in the CMS. No Response from Server"] forKey:NSLocalizedDescriptionKey];
                //*&error = [NSError errorWithDomain:@"AssetsLibrary" code:104 userInfo:errorDetail];
                *&error = [NSError errorWithDomain:@"AssetsLibrary" code:error.code userInfo:error.userInfo];
                
                if ([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]) {
                    [[self delegate] serviceDidFailWithError:ZMUserRegistrationService error:*&error];
                }
                
                NSLog(@"Error ocured: %@", *&error);
                
                onError([self getErrorWithCode:106 alternateMessage:@"Can't register user in the CMS. No Response from Server"]);
            }
            else {
                uriResponse = nil;
                uriResponse = [[[SBJsonParser new] autorelease] objectWithString:response];
                userId = [[uriResponse valueForKey:@"user"] intValue];
                complete(userId);
            }
        }
        else {
            
            NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
            [errorDetail setValue:[NSString stringWithFormat:@"Can't register user in the CMS. Connection Error"] forKey:NSLocalizedDescriptionKey];
            //*&error = [NSError errorWithDomain:@"AssetsLibrary" code:103 userInfo:errorDetail];
            *&error = [NSError errorWithDomain:@"AssetsLibrary" code:error.code userInfo:error.userInfo];
            
            if ([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]) {
                [[self delegate] serviceDidFailWithError:ZMUserRegistrationService error:*&error];
            }
            
            NSLog(@"> Error ocured: %@", *&error);
            onError([self getErrorWithCode:105 alternateMessage:@"There is a problem with the user load service."]);
        }
    }
    
}


- (BOOL) isConnectionAvailable{
    return TRUE;
}
-(NSString *) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int)requestType error:(NSError **)error{
    return [self contentByUri:uri parameters:params requestType:requestType headers:[NSDictionary dictionary] error:error];
}

-(NSString *) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int)requestType headers:(NSDictionary *)headers error:(NSError **)error{
    
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:uri]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    if([params count] > 0){
        NSMutableString *postData = [NSMutableString string];
        for(NSString *key in [params allKeys]){
            [postData appendFormat:@"%@=%@&",key,[params objectForKey:key]];
        }
        
        [request setHTTPMethod:ZMRequestTypeName(requestType)];
        NSString *requestDataLengthString = [[[NSString alloc] initWithFormat:@"%lu", (unsigned long)[postData length]] autorelease];
        [request setValue:requestDataLengthString forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSURLResponse *httpResponse = [NSURLResponse alloc];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&httpResponse error:error];
    NSLog(@"url %@ responded ",uri);
    return [[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] autorelease];
}

-(void) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int) requestType headers:(NSDictionary *)headers error:(NSError *)error complete:(void(^)(NSString *response, NSError *error))complete{
}

- (NSString *) requestUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int) requestType headers:(NSDictionary *)headers error:(NSError **)error {
    return @"";
}

- (void)downloadThumbnail:(NSString *)path complete:(void(^)(UIImage *thumbnail, NSString *p))complete fromCache:(BOOL)cache includeWaterMark:(NSString *)waterMark {
}

- (void)downloadThumbnail:(NSString *)path complete:(void(^)(UIImage *thumbnail))complete{
}


-(NSString *) getThumbnailPath:(NSString *)externalPath{
    return @"";
}

- (NSString *)MD5HashForString:(NSString *)input {
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    // Convert NSString into C-string and generate MD5 Hash
    CC_MD5([input UTF8String], [input length], result);
    // Convert C-string (the hash) into NSString
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [hash appendFormat:@"%02X", result[i]];
    }
    return [hash lowercaseString];
}

-(NSArray *) assets:(id)dummy withError:(NSError **)error{
    return [self assetsAfterDate:0 withError:&*error];
}

- (NSArray *) assetsAfterDate:(long)date withError:(NSError **)error
{
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&*error], [self checkParameter:@"assets.uri" error:&*error]];
    NSString *json = nil;
    uriResponse = nil;
    NSDictionary *headers = [NSDictionary dictionaryWithObjectsAndKeys:@"1.1",@"services_asset_getvocabulary_version", nil];
    if(!authenticated){
        [self authenticate:&*error];
    }
    
    if(*error == nil){
        if(date > 0){
            json = [self contentByUri:uri parameters:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",date],@"date", nil] requestType:ZMRequestGET  headers:headers error:&*error];
        }else{
            json = [self contentByUri:uri parameters:nil requestType:ZMRequestGET headers:headers error:&*error];
        }
    }
    
    if(*error){
        if([*error code] == 401){
            NSLog(@"Broken Session, Lets request again");
            [self authenticate:&*error];
        }
        if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){ 
            [[self delegate] serviceDidFailWithError:ZMListOfAssetsService error:*error];
        }
    }else{
        SBJsonParser *parser = [[SBJsonParser new] autorelease];
        uriResponse = nil;
        uriResponse = [parser objectWithString:json];
        NSMutableDictionary *assets = [NSMutableDictionary dictionaryWithDictionary:uriResponse];
        [assets removeObjectForKey:@"changed"];
        [assets removeObjectForKey:@"deleted"];
        [assets removeObjectForKey:@"unpublished"];
        [assets removeObjectForKey:@"total"];
        return [[[ZMParserArrayAssets alloc] initWithArray:[assets allValues] service:self] autorelease];
    }
    return nil;
}


- (void)getAssetsWithOptions:(NSDictionary *)options complete:(void(^)(NSDictionary *assets))complete error:(void(^)(NSError *error))onError {
    NSError *error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"assets.uri" error:&error]];
    NSString *json = nil;
    NSDictionary *headers = [NSDictionary dictionaryWithObjectsAndKeys:@"1.1",@"services_asset_getvocabulary_version", nil];

    if (error == nil) {
        json = [self requestUri:uri parameters:options requestType:ZMRequestGET headers:headers error:&error];
    }

    if (error) {
        if (!error.code) {
            onError([self getErrorWithCode:206 alternateMessage:@"The request was canceled before it could be fulfilled."]);
        }
        else if ([error code] == 401) {
            onError([self getErrorWithCode:107 alternateMessage:@"Broken Session, Authentication Error."]);
        }
        else {
            onError([self getErrorWithCode:201 alternateMessage:@"The assets service can't be reachable."]);
        }
        
        if ([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]) {
            [[self delegate] serviceDidFailWithError:ZMListOfAssetsService error:error];
        }
    }
    else {

        NSDictionary *assets = [[NSDictionary alloc] init];
        
        if (json) {
            assets = [[NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error] retain];
        }
        complete(assets);
    }
}

- (ZMAsset *) parseDataToAsset:(NSDictionary *)data{
    ZMAsset *asset=[[[ZMAsset alloc] init] autorelease];
    //asset internal data
    asset.uid = [[data objectForKey:@"nid"] intValue];
    asset.changed = [[data objectForKey:@"changed"] intValue];
    asset.url = [data objectForKey:@"uri_full"];
    asset.fileChanged = [[data objectForKey:@"file_timestamp"] intValue];
    
    asset.isPopular = ([[data objectForKey:@"popular"] intValue] == 1);
    
    asset.size = [[data objectForKey:@"file_size"] intValue];
    asset.tags = [data objectForKey:@"field_tags"];
    //asset visual names
    asset.name = [data objectForKey:@"title"];
    asset.description = [data objectForKey:@"field_description"];
    asset.state = ZMAssetStateRemote;    
    asset.mimetype = [data objectForKey:@"file_mime"];
    
    asset.publishDate = [[data objectForKey:@"publish_on"] intValue];
    
    NSMutableArray *groups = [NSMutableArray array];
    for(NSDictionary *brand in [data objectForKey:@"field_brand"]){
        ZMAssetsGroup *group = [[[ZMAssetsGroup alloc] init] autorelease];
        
        if ([brand objectForKey:@"brand_id"]) {
            group.uid = [[brand objectForKey:@"brand_id"] intValue];
        }
        
        if ([brand objectForKey:@"brand"]) {
            group.name = [brand objectForKey:@"brand"];
        }
        
        NSMutableArray *assetFranchises = [NSMutableArray array];
        
        for (NSString *franchiseID in [brand objectForKey:@"franchises"]){
            NSDictionary *franchise = [[brand objectForKey:@"franchises"] objectForKey:franchiseID];
            [assetFranchises addObject:[[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:[franchise objectForKey:@"name"] uid:(long)[[franchise objectForKey:@"id"] longLongValue] corporate_id:[[data objectForKey:@"field_franchise_id"] intValue] related:nil]];
        }

        group.related = assetFranchises;
        [groups addObject:group];
    }
    asset.groups = groups;
    asset.viewCount = [[data objectForKey:@"view_count"] intValue];
    NSDictionary *rating = [data objectForKey:@"rating"];
    asset.rating = [[[ZMRating alloc] initWithRating:[[rating objectForKey:@"votes"] intValue] average:[[rating objectForKey:@"average"] intValue] userVote:[[rating objectForKey:@"user_vote"] intValue]] autorelease];
    asset.unpublishDate = [[data objectForKey:@"unpublish_on"] intValue];
    asset.thumbnail =[data objectForKey:@"field_thumbnail"];
    NSMutableArray *categories = [NSMutableArray array];
    for(NSDictionary *category in [data objectForKey:@"field_categories"])
    {
        ZMCategory *cat = [[[ZMCategory alloc] init] autorelease];
        cat.categoryId = [[category objectForKey:@"id"] intValue];
        cat.categoryName = [category objectForKey:@"category"];
        [categories addObject:cat];
    }
    asset.categories = categories;
    return asset;
}

-(NSString *) downloadAsset:(ZMAsset *)asset error:(NSError **)error{
    return [self downloadAsset:asset error:error withDelegate:nil];
}

-(NSString *) downloadAsset:(ZMAsset *)asset error:(NSError **)error withDelegate:(id)progressDelegate{
    return nil;
}

- (NSString *) downloadAsset:(ZMAsset *)asset error:(NSError **)error withDelegate:(id)progressDelegate completionHandler:(void(^)(NSString *path, NSError *error)) completionHandler{
    return nil;
}


-(NSDictionary *)franchises:(long)date withError:(NSError **)error{
    
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&*error], [self checkParameter:@"brands.uri" error:&*error]];
    NSString *json = nil;
    NSDictionary *headers = [NSDictionary dictionaryWithObjectsAndKeys:@"1.1",@"services_asset_getvocabulary_version", nil];

    if (*error==nil) {
        NSDictionary *parameters = [NSDictionary dictionaryWithObject:[NSNumber numberWithLong:date] forKey:@"date"];
        
        json = [self requestUri:uri parameters:parameters requestType:ZMRequestPOST headers:headers error:&*error];
        
        if ([*error code]==401) {
            NSLog(@"Broken Session, Lets request again");
            [self authenticate:&*error];
        }
    }
    
    if (*error) {
        
        if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){
            [[self delegate] serviceDidFailWithError:ZMListOfFranchisesService error:*error];
        }
    }
    else{
        SBJsonParser *parser = [[SBJsonParser new] autorelease];
        NSDictionary *brands = [parser objectWithString:json];
        NSArray *newFranchises = [[[ZMParserArrayFranchises alloc] initWithArray:[NSArray array] service:self]autorelease];
        NSString *lastDate = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
        NSArray *newBrands = [NSArray array];
        NSArray *toDelete = [NSArray array];
        if([brands count]>0){
            if([[brands objectForKey:@"franchises"] count] > 0){
                newFranchises = [[[ZMParserArrayFranchises alloc] initWithArray:[[brands objectForKey:@"franchises"] allValues] service:self] autorelease];
            }
            newBrands = [brands objectForKey:@"brands"];
            toDelete = [brands objectForKey:@"deleted"];
            if([brands objectForKey:@"date"]){
                lastDate=[brands objectForKey:@"date"];
            }
        }
        else{
            *error = [NSError errorWithDomain:@"franchises" code:101 userInfo:[NSDictionary dictionaryWithObject:@"error parsing franchises" forKey:NSLocalizedDescriptionKey]];
            if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){
                [[self delegate] serviceDidFailWithError:ZMListOfFranchisesService error:*error];
            }
        }
        return [NSDictionary dictionaryWithObjectsAndKeys:newFranchises,@"franchises", newBrands, @"brands", toDelete, @"deleted",lastDate,@"lastDate", nil];
    }
    return nil;
}

-(NSDictionary *) syncronize:(NSArray *)folders folderDate:(NSString *)folderDate{
    
    NSMutableArray *JSONArray =[NSMutableArray array];
    
    if([folders count]>0){

        for(ZMFolder *folder in folders){
            
            NSMutableDictionary *folderAssets = [NSMutableDictionary dictionary];
            for(id key in folder.asset_list){
                ZMAsset *asset = (ZMAsset *) [folder.asset_list objectForKey:key];
                [folderAssets setObject:asset.name forKey:[NSString stringWithFormat:@"%ld",asset.uid]];
            }
            NSDictionary *folderToSend = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",folder.fldr_id],@"id",folder.fldr_name,@"name",[NSString stringWithFormat:@"%d",folder.fldr_qty],@"qty",[NSString stringWithFormat:@"%ld",folder.fldr_date_created],@"date",[NSString stringWithFormat:@"%d",folder.fldr_is_briefcase],@"briefcase",folderAssets,@"assets",folder.metadata,@"metadata", nil];
            [JSONArray addObject:folderToSend];
        }
    }
    NSString *JSONToPost = [[[SBJsonWriter alloc] init] stringWithObject:[NSDictionary dictionaryWithObjectsAndKeys:(folderDate?folderDate:@"-1"),@"briefcase_date", JSONArray,@"briefcase_data",nil]];
    //removing broken-sensible character ( i.e. &)
    JSONToPost = (NSString *) CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) JSONToPost,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8);
    //finally, send to cms
    NSError *error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"sync.uri" error:&error]];
    //if(!authenticated){
    //    [self authenticate:&error];
    //}
    NSMutableArray *remoteFolders = [NSMutableArray array];
    long remoteBriefDate =0;
    NSString *refreshTime = nil;
    
    if(error==nil){
        
        NSString *response = [self requestUri:uri
                                   parameters:[NSDictionary dictionaryWithObjectsAndKeys:JSONToPost,@"data",nil]
                                  requestType:ZMRequestPOST
                                      headers:nil
                                        error:&error];

        if ([error code]==401) {
            NSLog(@"Broken Session, Lets request again");
            [self authenticate:&error];
        }
        else if (error){
            
        }
        else{
            //check if sync needed
            SBJsonParser *parser = [[SBJsonParser new] autorelease];
            uriResponse = nil;
            uriResponse = [parser objectWithString:response];
            
            long localBriefDate = (long)[folderDate longLongValue];
            if([uriResponse objectForKey:@"briefcase_date"]){
                remoteBriefDate = (long)[[uriResponse objectForKey:@"briefcase_date"] longLongValue];
                if(remoteBriefDate>localBriefDate){
                    NSDictionary *folders = [uriResponse objectForKey:@"briefcase_data"];
                    for(NSDictionary *data in folders){
                        ZMFolder *folder = [[ZMFolder alloc]init];
                        folder.fldr_id = [[data objectForKey:@"id"] intValue];
                        folder.fldr_name = [data objectForKey:@"name"];
                        folder.fldr_qty = [[data objectForKey:@"qty"] intValue];
                        folder.fldr_date_created =[[data objectForKey:@"date"]intValue];
                        folder.fldr_is_briefcase = [[data objectForKey:@"briefcase"] intValue];
                        folder.metadata = [data objectForKey:@"metadata"];
                        
                        NSMutableDictionary *assets = [NSMutableDictionary dictionary];
                        
                        if([[data objectForKey:@"assets"] count]>0){
                            NSArray *folderAssets = [[data objectForKey:@"assets"] allKeys];
                            
                            for(NSString *key in folderAssets){
                                ZMAsset *asset = [[ZMAsset alloc]init];
                                asset.uid = [key intValue];
                                [assets setObject:asset forKey:[NSString stringWithFormat:@"%ld",asset.uid]];
                            }
                        }
                        folder.asset_list= assets;
                        [remoteFolders addObject:folder];
                    }
                }
                refreshTime = [uriResponse objectForKey:@"refreshTime"];
            }
            else{
                remoteBriefDate =-1;
            }
        }
        
    }
    if(error){
        if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){ 
            [[self delegate] serviceDidFailWithError:ZMEventsPostService error:error];
        }
        return nil;
    }
    NSDictionary *response = [[NSDictionary alloc] init];
    if(refreshTime){
        if(remoteFolders.count){
            response = [NSDictionary dictionaryWithObjectsAndKeys:remoteFolders,@"folders",[NSString stringWithFormat:@"%ld",remoteBriefDate],@"folders_date", refreshTime ,@"refreshTime", nil];
        }
    }else{
        response = [NSDictionary dictionaryWithObjectsAndKeys:remoteFolders,@"folders",[NSString stringWithFormat:@"%ld",remoteBriefDate],@"folders_date", nil];
    }
    return response;
}

-(BOOL) uploadEvents:(NSArray *)events{
    if([events count]>0){
        NSMutableArray *toJSON = [NSMutableArray array];
        for(ZMEvent * event in events){
            NSDictionary *eventToSend = [NSDictionary dictionaryWithObjectsAndKeys:[
                            NSString stringWithFormat:@"%ld",event.assetId],@"id",
                            [NSString stringWithFormat:@"%ld",event.eventType],@"type",
                            event.eventValue,@"value",[NSString stringWithFormat:@"%ld",
                            event.eventDate],@"date",event.sf,@"sf",nil];
            [toJSON addObject:eventToSend];
        }
        NSString *JSONToPost = [[[SBJsonWriter alloc] init] stringWithObject:toJSON];
        NSError *error = nil;
        NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&error], [self checkParameter:@"events.uri" error:&error]];
        /*if(!authenticated){
            [self authenticate:&error];
        }*/
        if(error==nil){
            [self requestUri:uri parameters:[NSDictionary dictionaryWithObjectsAndKeys:JSONToPost,@"events",nil] requestType:ZMRequestPOST headers:nil error:&error];
            //[self contentByUri:uri parameters:[NSDictionary dictionaryWithObjectsAndKeys:JSONToPost,@"events",nil] requestType:ZMRequestPOST error:&error];

            if ([error code] == 401){
                NSLog(@"Broken Session, Lets request again");
                [self authenticate:&error];
            }
        }
        if(error){
            if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){ 
                [[self delegate] serviceDidFailWithError:ZMEventsPostService error:error];
            }
            return FALSE;
        }
    }
    return TRUE;
}


-(ZMFranchise *) parseDataToFranchise:(NSDictionary *)data{
    ZMFranchise *franchise = [[[ZMFranchise alloc] init] autorelease];
    franchise.tid = [[data objectForKey:@"tid"] intValue];
    franchise.name = [data objectForKey:@"name"];
    franchise.field_private = [[data objectForKey:@"field_private"] boolValue ];
    franchise.corporate_id = (long)[[data objectForKey:@"field_franchise_id"] longLongValue];
    NSMutableArray *groups = [NSMutableArray array];
    for(NSMutableDictionary *brand in [data objectForKey:@"brands"]){
        NSMutableDictionary *group = [[[NSMutableDictionary alloc] init] autorelease];
        [group setValue:[brand objectForKey:@"tid"] forKey:@"tid"];
        [group setValue:[brand objectForKey:@"vid"] forKey:@"vid"];
        [group setValue:[brand objectForKey:@"name"] forKey:@"name"];
        [groups addObject:group];
    }
    franchise.brands = groups;
    return franchise;
}

-(int) lastChanged{
    if([uriResponse objectForKey:@"changed"]){
        return [[uriResponse objectForKey:@"changed"] intValue];
    }else{
        return 0;
    }
}

-(NSArray *) deleted{
    NSMutableArray *deleted = [[[NSMutableArray alloc] init ] autorelease];
    if([uriResponse objectForKey:@"deleted"]){
        for(NSString *nid in [uriResponse objectForKey:@"deleted"]){
            [deleted addObject:nid];
        }
    }
    return deleted;
}

-(NSArray *) unpublished{
    NSMutableArray *unpublished = [[[NSMutableArray alloc] init ] autorelease];
    if([uriResponse objectForKey:@"unpublished"]){
        for(NSString *nid in [uriResponse objectForKey:@"unpublished"]){
            [unpublished addObject:nid];
        }
    }
    return unpublished;
}     

-(int) total{
    int total = 0;
    if([uriResponse objectForKey:@"total"]){
        total = [[uriResponse objectForKey:@"total"] intValue];
    }
    return total;
}

-(NSDictionary *)statistics:(NSString *)date withError:(NSError **)error{
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&*error], [self checkParameter:@"statistics.uri" error:&*error]];
    NSString *json = nil;
    uriResponse = nil;
    //if(!authenticated){
    //    [self authenticate:&*error];
    //}
    if(*error == nil){
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        if([date longLongValue] > 0){
            [parameters setObject:date forKey:@"date"];
        }
        
        json = [self requestUri:uri parameters:parameters requestType:ZMRequestGET headers:nil error:&*error];
    }
    if(*error){
        if([*error code] == 401){
            NSLog(@"Broken Session, Lets request again");
            [self authenticate:&*error];
        }
        if([delegate respondsToSelector:@selector(serviceDidFailWithError:error:)]){ 
            [[self delegate] serviceDidFailWithError:ZMListOfAssetsService error:*error];
        }
    }else{
        SBJsonParser *parser = [[SBJsonParser new] autorelease];
        uriResponse = nil;
        uriResponse = [parser objectWithString:json];
        NSMutableDictionary *statistics;
        if([uriResponse count]>0){
            statistics = [NSMutableDictionary dictionaryWithDictionary:uriResponse];
        }else{
            statistics = [NSMutableDictionary dictionary]; 
        }
        return statistics;
    }
    return nil;
}


/**
 *
 **/
-(NSDictionary *)registerToken:(NSString *)deviceToken upi:(NSString *)upi withError:(NSError **)error{
    
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&*error],
                     [self checkParameter:@"apssave.uri" error:&*error]];
    NSString *json = nil;
    NSMutableDictionary *response;
    uriResponse = nil;
    
    if (!error) {
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        if (deviceToken && ![deviceToken isKindOfClass:[NSNull class]]) {
            [parameters setObject:deviceToken forKey:@"token"];
        }
        
        if (upi && ![deviceToken isKindOfClass:[NSNull class]]) {
            [parameters setObject:upi forKey:@"upi"];
        }

        [parameters setObject:@"ios" forKey:@"type"];
        json = [self requestUri:uri parameters:parameters requestType:ZMRequestPOST headers:nil error:&*error];
        
    }
    SBJsonParser *parser = [[SBJsonParser new] autorelease];
    uriResponse = nil;
    uriResponse = [parser objectWithString:json];
    response = ([uriResponse count] > 0) ? [NSMutableDictionary dictionaryWithDictionary:uriResponse]:[NSMutableDictionary dictionary];
    return response;
}

/**
 *
 **/
- (NSDictionary *)unRegisterToken:(NSString *)deviceToken withError:(NSError **)error{
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&*error], [self checkParameter:@"apsdelete.uri" error:&*error]];
    NSString *json = nil;
    NSMutableDictionary *response = NULL;
    uriResponse = nil;
    if(!error && deviceToken){
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:deviceToken forKey:@"token"];
        json = [self requestUri:uri parameters:parameters requestType:ZMRequestDELETE headers:nil error:&*error];
        //json = [self contentByUri:uri parameters:parameters requestType:ZMRequestDELETE error:&*error];
    }
    if(error){
        NSLog(@"Error deleting the token to apns.");
    }else if(json){
        SBJsonParser *parser = [[SBJsonParser new] autorelease];
        uriResponse = nil;
        uriResponse = [parser objectWithString:json];
        response = ([uriResponse count]>0)?[NSMutableDictionary dictionaryWithDictionary:uriResponse]:[NSMutableDictionary dictionary];
    }
    return response;
}

- (NSDictionary *) getMostPopularAsset:(NSError **)error{
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&*error], [self checkParameter:@"popular.asset.uri" error:&*error]];
    NSString *json = nil;
    NSMutableDictionary *response = NULL;
    uriResponse = nil;
    if(!error){
        json = [self requestUri:uri parameters:[NSMutableDictionary dictionary] requestType:ZMRequestGET headers:nil error:&*error];
    }
    if(error){
        NSLog(@"Error deleting the token to apns.");
    }
    else{
        uriResponse = nil;
        if(json){
            uriResponse = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: error];
            response = ([uriResponse count]>0)?[NSMutableDictionary dictionaryWithDictionary:uriResponse]:[NSMutableDictionary dictionary];
        }
    }
    return response;
}

/**
 *
 **/
- (void)checkVersion:(void(^)(NSDictionary *response))oncomplete onerror:(void (^) (NSError **response))onerror{
    NSError **error = nil;
    NSString *uri = [NSString stringWithFormat:@"%@%@", [self checkParameter:@"server" error:&*error], [self checkParameter:@"version.uri" error:&*error]];
    NSString *json = nil;
    NSMutableDictionary *response;
    uriResponse = nil;
    if(!error){
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"] forKey:@"v"];
        json = [self requestUri:uri parameters:parameters requestType:ZMRequestGET headers:nil error:&*error];
        //json = [self contentByUri:uri parameters:parameters requestType:ZMRequestGET error:&*error];
    }
    
    if (error) {
        onerror(error);
    }
    else {
        SBJsonParser *parser = [[SBJsonParser new] autorelease];
        uriResponse = nil;
        uriResponse = [parser objectWithString:json];
        response = ([uriResponse count]>0)?[NSMutableDictionary dictionaryWithDictionary:uriResponse]:[NSMutableDictionary dictionary];
        oncomplete(response);
    }
}

@end
