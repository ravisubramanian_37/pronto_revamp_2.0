//
//  ZMFranchise.h
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 12/01/12.
//  Copyright (c) 2012 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Represent a Franchise in software (ZMAssetGroups ase used as DAO methods).
 */
@interface ZMFranchise : NSObject

/**
 * id of the Franchise
 */
@property int tid;

/**
 * name of the franchise
 */
@property (retain)NSString *name;

/**
 * if franchise is private this BOOL will be on true
 */
@property bool field_private;

/**
 * corporate id
 */
@property long corporate_id;
/**
 * this will contain the brands asociated with this id
 */
@property (retain) NSArray* brands;
@end
