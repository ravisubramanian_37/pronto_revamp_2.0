//
//  ZMNotification.h
//  ZMAssetsLibrary
//
//  Created by Andres Ramirez on 12/30/13.
//
//

#import <Foundation/Foundation.h>

@interface ZMNotification : NSObject

@property long notificationId;
@property long assetId;
@property (nonatomic,retain) NSString *subject;
@property (nonatomic,retain) NSString *message;
@property (nonatomic,retain) NSString *action;
@property (nonatomic,retain) NSString *url;
@property (nonatomic,retain) NSString *date;
@property long viewed;
@end
