//
//  ZMNote.h
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * this is a model of a note, used on asset's notes.
 */

@interface ZMNote : NSObject
/**
 * the id of the note to be stored on database
 */
@property (nonatomic) long nte_id;
/**
 * the current text of the note
 */
@property (nonatomic, retain) NSString *nte_content;
/**
 * the date of creation/update of the asset
 */
@property (nonatomic) long nte_date;
/**
 * the asset wich was created the note
 */
@property (nonatomic) long asset_id;
/**
 * the order of appearance of the note, the less the number, the first to appear
 */
@property (nonatomic) int nte_weight;
/**
 * page of the asset were the note was created
 */
@property (nonatomic) int nte_page;
/**
 * X position where the note icon will be draw
 */
@property (nonatomic) int nte_xcord;
/**
 * Y position where the note icon will be draw
 */
@property (nonatomic) int nte_ycord;
/**
 * width of the note highlight
 */
@property (nonatomic) int nte_width;
/**
 * height of the note highlight
 */
@property (nonatomic) int nte_height;
@end
