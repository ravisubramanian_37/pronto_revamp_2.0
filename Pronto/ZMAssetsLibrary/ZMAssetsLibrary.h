//
//  ZMAssetsLibrary.h
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/1/11.
//  Copyright (c) 2011 Abbvie Inc. All rights reserved.

#import <Foundation/Foundation.h>
#import "ZMAssetsGroup.h"
#import "ZMAssetsDAO.h"
#import "Asset.h"

/**
 * protocol used to add observers to the software, used to notify when an asset/group are updated on database
 */

@protocol ZMAssetsLibraryObserver <NSObject>
@optional

/**
 * notifies when a libray (database) was loaded
 */
- (void)libraryDidLoad;

/**
 * notifies when a library start sync
 */
-(void) libraryStartSync;
/**
 * notifies when a library finish sync
 */
- (void) libraryDidFinish;
/**
 * notifies when a library has an error on sync
 */
- (void) libraryDidFinishWithError:(NSString *)error;
/**
 * notifies when a library has a sync in progress (if theres requested another one9
 */
- (void) librarySyncInProgress;

/**
 *  Notifies to the user that new assets were updated or inserted
 */
- (void) assetsChanged;

/**
 * notifies an advance of sync
 */
- (void) libraryProgressStep:(NSString *)progressText progress:(float)progress forVal:(NSString*)progressFor;
/**
 * notifies when a Asset was added/updated
 */
- (void)assetDidUpdate:(ZMAsset *)asset;

/**
 * notifies when a Franchise/Brand was added/updated
 */
- (void)groupDidUpdate:(ZMAssetsGroup *)group;

/**
 * notifies when a Franchise/Brand was deleted
 */
- (void) groupDidDelete:(ZMAssetsGroup *)group;


/**
 * notifies when library finished sync
 */

- (void)libraryFinishSync;

/**
 * notifies when a category has changed
 */
- (void) categoryChanged;

/**
 * notifies when thread is stoped (all duties finished
 */
- (void)threadStoped;
/**
 * notifies when asset downloaded successfully
 */ 
-(void) downloadAssetDidFinish:(Asset *) asset;

/**
 * notifies when asset failed on download
 */
-(void) downloadAssetDidNotFinish:(Asset *)asset error:(NSString *)error;


-(void) statsChanged:(NSDictionary *)assets;


@end

/**
 * Used as a layer between software and DAO, also handles observers to respond based on ZMAssetsLibraryObserver
 */
@interface ZMAssetsLibrary : NSObject{
    
@private
    /**
     * ZMAssetsDAO object wich will perform the requested database connections
     */
    ZMAssetsDAO *dao;
    /**
     * NSString with the filepath of the database file (sqlite3)
     */
    NSString *path;
    /**
     * Stores the observers that will be notified when a method happens according to ZMAssetsLibraryObserver
     */
    NSMutableArray *observers;
    
    BOOL completedStatus;
    
    BOOL finalStatus;
}

/**
* Verified if the database is present or not
**/
-(BOOL) isDatabaseCreated:(NSString *)dpath;

/**
 * Refreshes library path to register the change
 **/
-(void) refreshWithPath:(NSString *)dpath;

/**
 * Get category last Udpate
 */
-(NSString *) getCategoryDate;
/**
 * update category date
 */
-(BOOL) updateCategoryDate:(NSString *)date;

/**
 * update category
 */
-(BOOL)updateCategory:(ZMCategory *)category;

/**
 * Add an observer to see the changes into the library
 */
- (void) addObserver:(id)listener;

/**
 * Remove an observer
 **/
- (void) removeObserver:(id)listener;

/**
 * Gets the default library
 * @return ZMAssetsLibrary (Singleton)
 */    
+ (ZMAssetsLibrary *)defaultLibrary;

/**
 * Inicialize the library in a specific dirctory
 * @param path to the local directory
 */
-(void) initWithPath:(NSString *)path;

/**
 * get a property with key
 * @param key
 * @return NSString with the value of the requested key
 */
-(NSString *) property:(NSString *)key;

/**
 * get the list of properties into this library
 * @return NSDictionary with a key/values stored on database
 */
-(NSDictionary *) properties;

/**
 * set a property with a value
 * @param key
 v@param value
 */
-(void) setProperty:(NSString *)key value:(NSString *)value;

/**
 * search a group in the database with provided ID
 * @param uid of the group to search
 * @return ZMAssetGroup the group requested, null afterwards
 */
-(ZMAssetsGroup *) groupWithUID:(long)uid;

/**
 * search a group in the database based on its name
 * @param NSString with the name to search
 * @return ZMAssetGroup that matchs its name, nil afterwards
 */
-(ZMAssetsGroup *) groupByName:(NSString *)name;

/**search for all the groups on database
 */
-(NSArray *) groups;

/**
 * search groups in the database wich have related id provided
 * @param uid the group id to include in search
 * @return NSArray with the ZMAssetGroups that have this related search, empty array otherwise
 */
-(NSArray *) groupsWithRelatedUID:(long)uid;

/*
 *search group form the database that is not related (franchise) and has the name Provided
 */
-(NSArray *) franchiseWithName:(NSString *)franchiseName;

/**
 * searchs on database for an franchise with the Corporate ID Provided
 * @param NSString with the corportae id
 */
-(ZMAssetsGroup *) franchiseByCorporateID:(NSString *)franchiseID;

/**
 * search groups in the database that matches its additional properties with the provided params
 * @param a Dictionary with the parameters to search
 *@reutrn NSArray with the ZMAssetGroups found, empty arra otherwise
 */
-(NSArray *) groupsWithProps:(NSDictionary *)params;

/**
 * insert a group into the library
 * @param group to add
 @
 */
-(BOOL) addGroup:(ZMAssetsGroup *)group full:(BOOL)full;

/**
 * remove a groups, if assets has only this group assigned, will be deleted also
 * @param ZMAssetGroup to be deleted
 * @return BOOL if operation was completed
 */
-(BOOL) deleteGroup:(ZMAssetsGroup *)group;

/**
 * remove a franchise, if assets has only this franchise assigned, will be deleted also
 * @param ZMAssetGroup to be deleted
 * @return BOOL if operation was completed
 */
-(BOOL) deleteFranchise:(ZMAssetsGroup *)group;


/**
 * Select all categories
 * @return NSArray with the categories
 */
-(int)selectAllCategories;


/**
 *  Select all categories with filter for the menu
 *  @param filter asset filter
 *  @param searchText search criteria
 *  @param folder_id folder id
 *  @param asDictionary Boolean that determines if is an array of dictionaries or ZMCategories
 *  @return NSArray with the categories
 */
- (NSMutableArray *)selectMenuCategories:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id asDictionary:(BOOL)asDictionary;

/**
 * remove a franchise, if assets has only this franchise assigned, will be deleted also
 * @param assetId of the asset to delete
 * @return BOOL if operation was completed
 */
-(BOOL)deleteRelatedCategory:(long)assetId;



/**
 * returns the total number of assets stored on database without any filters
 */
-(int) totalStoredAssets;


/**
 * find asset with a specific ID
 * @param id of the asset to search
 * @return ZMAsset found on database, false otherwise
 */
-(ZMAsset *) assetWithUID:(long)uid;

/**
 * find POPULAR FIELD with a specific ID
 * @param id of the asset to search
 * @return PUPOLAR Field of ASSETS Table
 */

-(int)isPopularAsset:(long)uid;

/*
 * Alias og assetsByFilter but with a provided limit of results and sort directions
 */
-(NSArray *) assetsWithProperties:(ZMAsset *)filter withSearchText:(NSString *)searchText sort:(NSString *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit;


-(NSArray *) assetsWithProperties:(ZMAsset *)filter withSearchText:(NSString *)searchText sorts:(NSMutableArray *)sorts page:(int) page limit:(int)limit;


-(NSMutableArray *) folderAssetsByFilter:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long)folder_id sorts:(NSMutableArray *)sorts page:(int) page limit:(int)limit;

/**
 * return the number of asset acording to this query. the arguments are the same as assetsByFilter
 */
-(int) assetsWithPropertiesCount:(ZMAsset *)filter withSearchText:(NSString *)searchText sort:(NSString *)sort sortDir:(NSString *)sortDir;

//OMR

-(int) countFolderAssetsByCategory:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit ;
-(NSMutableArray *) aCountFolderAssetsByCategory:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit ;


/**
 * return the assets contained in the specified folder
 * @param ZMAsset with the filter data
 * @param long with the folder id to search
 * @param NSString with the sort column desired
 * @param NSString with the sort direction (ASC/DESC)
 */
-(NSMutableArray *) folderAssetsByFilter:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long)folder_id sort:(NSString *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit;


/**
 * return the assets IDS
 * @param ZMAsset with the filter data
 * @param long with the folder id to search
 * @param NSString with the sort column desired
 * @param NSString with the sort direction (ASC/DESC)
 */
-(NSMutableArray *) folderAssetsByFilterIds:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sort:(NSString *)sort sortDir:(NSString *)sortDir;

/**
 * return the assets from the Category menu selection
 * @param ZMAsset with the filter data
 * @param long with the folder id to search
 * @param NSString with the sort column desired
 * @param NSString with the sort direction (ASC/DESC)
 */
-(NSMutableArray *) folderAssetsByCategorySelection:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit;

/**
 * find assets that matches with the specified tag
 * @param NSString with the tag to search in the database
 * @param BOOL search from briefcase instead of library
 * @return NSMutableArray with the ZMAssets found on the database
 */
-(NSMutableArray *) assetsByTagOrTitle:(NSString *)filter withFilters:(ZMAsset *)assetFilter useBriefcase:(ZMFolder *)folder;


/**
 * saves an asset into especified folder
 * @param ZMAsset with the asset info to be saved
 * @param ZMFolder with the folder with the asset will be aded (can be nil in order to be added on the briefcase)
 * @param BOOL if you load only the folder header, please use TRUE to load the previously loaded assets
 * @return ZMFolder with the information updated
 */
-(ZMFolder *) addAssetToFolder:(Asset *)asset folder:(ZMFolder *)folder loadDetails:(BOOL)loadDetails;

/**
 * remove an asset from the especified folder
 * @param ZMAsset with the asset info to be removed
 * @param ZMFolder with the folder with the asset will be removed (can be nil in order to be added on the briefcase)
 * @return ZMFolder with the information updated
 */
-(ZMFolder *) removeAssetToFolder:(Asset *)asset folder:(ZMFolder *)folder;

- (void)deleteAssetFromFolderAssets:(long)assetId folderId:(long)folderid;

/**
 * retrieves all folders from the database (including folder0 (briefcase))
 * @param BOOL TRUE if Assets contained on folders will be loaded, FALSE otherwise
 * @return NSArray with ZMFolders Contained
 */
-(NSArray *) getAllFolders:(bool)withDetail;

/**
 * retrieves a folder from the database from the id loaded.
 * @param long with the id to look from dabase.
 * @folder ZMFolder with the information loaded from database
 */
-(ZMFolder *) folderWithId:(long)fldr_id;

/*
 * retrieves the assets that contains the folder id provided
 * @param long with the id to look on database
 * @return NSDictionary with the ZMAssets form the folder.
 */
-(NSDictionary *) folderAssets:(long)fldr_id;
-(NSArray *) folderAssetsIds:(long)fldr_id;

/*
 * searchs on database for folders that the requested asset has been added
 * @return NSArray with the folders found
 */
-(NSArray *) assetInFolders:(ZMAsset *)asset;

/**
 * adds a folder to the database
 * @param ZMFolder folder wich will be stored on database
 * @param BOOL TRUE if you want to save the assets on the Folder, FALSE if only save the header (name ie)
 * @return BOOL TRUE if the operation succeded, FALSE afterwards
 */
-(ZMFolder *)saveFolder:(ZMFolder *)folder withDetails:(BOOL)details;

/**
 * adds a folder to the database
 * @param ZMFolder folder wich will be stored on database
 * @param NSMutableDictionary with the assets List
 * @return ZMFolder with the folder created
 */
//- (ZMFolder *) saveSearchToFolder:(NSMutableArray *) assetsList folderName:(NSString *)folderName;

/**
 * adds a folder to the database
 * @param ZMFolder folder wich will be stored on database
 * @param NSMutableDictionary with the assets List
 * @return ZMFolder with the folder created
 */
- (ZMFolder *) saveSearchToFolder:(NSMutableArray *) assetsList newFolder:(ZMFolder *)newFolder;
/**
 * Verify if a folders has a duplicated name
 * @param NSString folder name
 * @return BOOL yes for duplicated, no for not duplicated
 */
-(BOOL)duplicatedFolderName:(NSString *)folderName;

/**
 * removes a folder from the database
 * @param ZMFolder folder to delete from a database
 * @return BOOL true if it was deleted from the dabase
 */
-(BOOL) removeFolder:(ZMFolder *)folder;

/**
 * add or replace an asset into the library
 * @param a ZmAsset to add
 * @return BOOL with the result of the operation
 */
-(BOOL) addAsset:(ZMAsset *)asset;

/**
 * update an asset into the library
 * @param a ZmAsset to update
 * @retun BOOL with the result of the operation
 */
-(BOOL) updateAsset:(ZMAsset *)asset;

/**
 * remove an asset from the library
 * @param id of the asset to delete
 * @return BOOL with the result of the operation
 */
-(BOOL) removeAssetWithUID:(long)uid;

/*
 * return the events saved on database
 */
-(NSArray *)events:(NSString *)format withSaleFranchiseId:(NSString *)saleFranchiseId;

/**
 * save an event on system 
 */

-(BOOL) saveEvent:(ZMEvent *)event;

/**
 * delete an event on the database
 */
-(BOOL) deleteEvent:(ZMEvent *)event;

/**
 * Save a new category
 */
-(BOOL)saveCategory:(ZMCategory *)category;

/**
 * Save a new category related
 */
-(BOOL)saveCategoryRelated:(ZMCategory *)category;


/**
 * Delete a category
 * @param id of the category
 */
-(BOOL)deleteCategory:(long)category;

/**
 * search all the notes on the database, used on sync with cms
 * @return NSArray of ZMNotes
 */
-(NSArray *)allNotes;


/**
 * search for a note in the database with the id suplied
 * @param long id of the note to search from database
 * @return ZMNote found, nil afterwards
 */
-(ZMNote *)noteWithId:(long)uid;

/**
 * saves a note to database
 * @param ZMNote to save  (if no nte_id defined, it will be created)
 * @return ZMNote with the note saved (and nte_id if there was sent with no one) nil afterwards
 */
-(ZMNote *) saveNote:(ZMNote *)note;
/**
 * deletes a note form database
 * @param ZMNote the note to be deleted
 * @return BOOL with TRUE if the operation was completed, FALSE afterwards
 */
-(BOOL) deleteNote:(ZMNote *)note;
/**
 * notifies that the library finish syncing basic data
 */






/*
 * replace the current briefcase with this one
 */
-(BOOL) replaceBriefcase:(NSArray *)folders;

/*
 * replace the current briefcase with this one
 */
-(BOOL) replaceNotes:(NSArray *)notes;

/** notifies to all the observers that the library starts its sync with cms
 */
-(void) notifyStart;
/**
 * notifies when a step is taken and send a progress percentage
 */
-(void) notifyStep:(NSString *)progressText progress:(float)progress progressFor:(NSString *)progressForString;
/**
 * notifies to all the obervers that the library finisht the cms syncs
 */

-(void) notifyFinish;

/**
 *  notifies if an asset has beed added or updated
 */
-(void)notifyAssetsChanged;



-(void)notifyStatsChanged:(NSDictionary *)assets;


/**
 * notifies to all the observers that the library has a sync error
 */
-(void) notifyError:(NSString *)withMessage;

/**
 * notifies to all the obserers that the requested sync is in progress
 */
-(void) notifyInProgress;
/**
 * notifies that the library finish all its duties and its stoping the thread (meaning realeasing and destroy)
 */
-(void) notifyThreadStop;
/**
 * notifies that a asset download succeeded
 * @param asset the asset that was downlaoded
 */
-(void) notifyDownloadCompleted:(Asset *) asset;

/**
 * notifies that the asset download failed
 * @param asset that failed to download
 * @error NSString with the error triggered
 */
-(void) notifyDownloadFailed:(Asset *)asset error:(NSString *)error;
/**
 * Removes the dabase from the filessytem, meaning deletion of all database data and assets files
 */
-(void) clean;
-(void) resetCoredataPath;

/**
 * prepares the application for new version
 **/
-(void) prepareForNewVersion;

/**
 * Save notification
 * @param ZMNotification
 * @return Bool if the value is saved
 **/
-(BOOL)saveNotification:(ZMNotification *)notification;

/**
 * Save notification
 * @param ZMNotification
 * @return Bool if the value is updated
 **/
-(BOOL)updateViewedNotification:(ZMNotification *)notification;

-(BOOL)deleteNotification:(long)notificationId;

-(NSArray *) getLastestNotifications;

-(NSArray *) getAllNotifications;

-(int) getTotalUnreadNotifications;

-(ZMNotification *)selectNotification:(long)notificationId;


// New Search Implementation

-(BOOL) searchCompleted:(BOOL) completion;
-(BOOL) completionStatus;

-(BOOL) checkFinalStatus:(BOOL) completion;


@end
