//
//  ZMRating.h
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 25/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZMRating : NSObject

@property long votes;

@property float average;

@property int userVote;

-(ZMRating *) initWithRating:(int)votes average:(float) average userVote:(int) userVote;

@end
