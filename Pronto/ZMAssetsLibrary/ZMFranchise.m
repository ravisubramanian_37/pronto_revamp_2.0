//
//  ZMFranchise.m
//  ZMAssetsLibrary
//
//  Created by Carlos Garcia on 12/01/12.
//  Copyright (c) 2012 Abbvie Inc. All rights reserved.
//

#import "ZMFranchise.h"

@implementation ZMFranchise
@synthesize tid,name,field_private,brands,corporate_id;
- (void)dealloc
{
    [name release];
    [brands release];
    [super dealloc];
}
@end
