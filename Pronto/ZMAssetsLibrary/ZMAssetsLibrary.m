//
//  ZMAssetsLibrary.m
//  ZMAssetsLibrary
//
//  Created by Carlos Beltran on 12/1/11.
//  Copyright (c) 2011 Abbvie Inc. All rights reserved.
//

#import "ZMAssetsLibrary.h"
#import "Pronto-Swift.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation ZMAssetsLibrary

ZMAssetsLibrary *defaultLibrary;

- (void)dealloc {
    [super dealloc];
}

- (void) addObserver:(id)observer{
    [observers addObject:observer];
}
- (void) removeObserver:(id)observer{
    [observers removeObject:observer];
}

- (id)init
{
    self = [[super init] retain];
    if (self) {
        observers = [[NSMutableArray array] retain];
    }
    
    return self;
}

+ (ZMAssetsLibrary *)defaultLibrary
{
    @synchronized(self)
    {
        static ZMAssetsLibrary *defaultLibrary = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            defaultLibrary = [[self alloc] init];
        });
        return defaultLibrary;
    }
}


- (BOOL)isDatabaseCreated:(NSString *)dpath {
    BOOL isDir = NO;
    return [[NSFileManager defaultManager] fileExistsAtPath:[dpath stringByAppendingPathComponent:@"assets.db"] isDirectory:&isDir];
}

- (void)refreshWithPath:(NSString *)dpath {
    
    if ([self isDatabaseCreated:dpath]) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error = nil;
        [fileManager removeItemAtPath:[dpath stringByAppendingPathComponent:@"assets.db"] error:&error];
    }
}


- (void)initWithPath:(NSString *)apath {

    path = apath;
    dao = [[ZMAssetsDAO alloc] initWithPath:[path stringByAppendingPathComponent:@"assets.db"]];
    
    for (id observer in observers) {

        if ([observer respondsToSelector:@selector(libraryDidLoad)]) {
            [observer libraryDidLoad];
        }
    }
}

-(NSString *) property:(NSString *)key{
    return [dao findProperty:key];
}

-(NSDictionary *) properties{
    return [dao listProperties];
}

-(void) setProperty:(NSString *)key value:(NSString *)value{
    [dao saveProperty:key value:value];
}

-(ZMAssetsGroup *) groupWithUID:(long)uid{
    return [dao findGroupById:uid];
}

-(ZMAssetsGroup *) groupByName:(NSString *)name{
    return [dao groupByName:name];
}
-(NSArray *) groups{
    return [dao findGroups];
}

-(NSArray *) groupsWithRelatedUID:(long)uid{
    return [dao findGroupByRelatedId:uid];
}

-(NSArray *) franchiseWithName:(NSString *)franchiseName{
    return [dao findGroupByFranchiseName:franchiseName];
}

-(ZMAssetsGroup *) franchiseByCorporateID:(NSString *)franchiseID{
    return [dao findFranchiseByCorporateID:franchiseID];
}

-(NSArray *) groupsWithProps:(NSDictionary*)params{
    return [dao findGroupsWithProps:params];
}

-(BOOL) addGroup:(ZMAssetsGroup *)group full:(BOOL)full{
    BOOL b=[dao addGroup:group full:full];
    if(b){
        for(id observer in observers){
            if([observer respondsToSelector:@selector(groupDidUpdate:)]){
                [observer groupDidUpdate:group];
            }
        }
    }
    
    return b;
}

-(BOOL) deleteGroup:(ZMAssetsGroup *)group{
    BOOL b=[dao deleteGroup:group];
    if(b){
        for(id observer in observers){
            if([observer respondsToSelector:@selector(groupDidDelete:)]){
                [observer groupDidDelete:group];
            }
        }
    }
    
    return b;
}

-(BOOL) deleteFranchise:(ZMAssetsGroup *)group{
    BOOL b=[dao deleteFranchise:group];
    if(b){
        for(id observer in observers){
            if([observer respondsToSelector:@selector(groupDidDelete:)]){
                [observer groupDidDelete:group];
            }
        }
    }
    
    return b;
}

-(int) totalStoredAssets{
    return [dao totalStoredAssets];
}

-(ZMAsset *) assetWithUID:(long)uid{
    return [dao findAssetById:uid];
}

-(int)isPopularAsset:(long)uid{
    return [dao isPopularAsset:uid];
}

/**
 Multiple sorts
 **/
-(NSArray *) assetsWithProperties:(ZMAsset *)filter withSearchText:(NSString *)searchText sorts:(NSMutableArray *)sorts page:(int) page limit:(int)limit{
    return [dao assetsByFilter:filter withSearchText:searchText sorts:sorts page:page limit:limit];
}

-(NSArray *) assetsWithProperties:(ZMAsset *)filter withSearchText:(NSString *)searchText sort:(NSString *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit{
    return [dao assetsByFilter:filter withSearchText:searchText sort:sort sortDir:sortDir page:page limit:limit];
}

-(int) assetsWithPropertiesCount:(ZMAsset *)filter withSearchText:(NSString *)searchText sort:(NSString *)sort sortDir:(NSString *)sortDir{
    return [dao assetsByFilterCount:filter withSearchText:searchText fromFolder:-1 sort:sort sortDir:sortDir];
}

//OMR

-(int) countFolderAssetsByCategory:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit {
    return [dao folderAssetsByCategory:filter withSearchText:searchText fromFolder:folder_id sorts:sort sortDir:sortDir page:page limit:limit];
}

-(NSMutableArray *) aCountFolderAssetsByCategory:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit {
    return [dao afolderAssetsByCategory:filter withSearchText:searchText fromFolder:folder_id sorts:sort sortDir:sortDir page:page limit:limit];
}

-(NSMutableArray *) folderAssetsByFilter:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long)folder_id sort:(NSString *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit{
    return [dao folderAssetsByFilter:filter withSearchText:searchText fromFolder:folder_id sort:sort sortDir:sortDir page:page limit:limit];
}

-(NSMutableArray *) folderAssetsByFilterIds:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sort:(NSString *)sort sortDir:(NSString *)sortDir{
    return [dao folderAssetsByFilterIds:filter withSearchText:searchText fromFolder:folder_id sort:sort sortDir:sortDir];
}

-(NSMutableArray *) folderAssetsByCategorySelection:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id sorts:(NSMutableArray *)sort sortDir:(NSString *)sortDir page:(int) page limit:(int)limit {
    return [dao folderAssetsByCategorySelection:filter withSearchText:searchText fromFolder:folder_id sorts:sort sortDir:sortDir page:page limit:limit];
}


/**
 * Multiple sort for briefcase
 **/
-(NSMutableArray *) folderAssetsByFilter:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long)folder_id sorts:(NSMutableArray *)sorts page:(int) page limit:(int)limit {
    return [dao folderAssetsByFilter:filter withSearchText:searchText fromFolder:folder_id sorts:sorts page:page limit:limit];
}

-(NSMutableArray *) assetsByTagOrTitle:(NSString *)filter withFilters:(ZMAsset *)assetFilter useBriefcase:(ZMFolder *)folder{
    return [dao assetsByTagOrTitle:filter withFilters:assetFilter useBriefCase:folder limit:1000];
}

-(ZMFolder *) addAssetToFolder:(Asset *)asset folder:(ZMFolder *)folder loadDetails:(BOOL)loadDetails{
    return [dao addAssetToFolder:asset folder:folder loadDetails:loadDetails];
}

-(ZMFolder *) removeAssetToFolder:(Asset *)asset folder:(ZMFolder *)folder{
    return [dao removeAssetToFolder:asset folder:folder];
}

-(void)deleteAssetFromFolderAssets:(long)assetId folderId:(long)folderid{
    return [dao deleteAssetFromFolderAssets:assetId folderId:folderid];
}

-(NSArray *) getAllFolders:(bool)withDetail{
    return [dao getAllFolders:withDetail];
}

-(ZMFolder *) folderWithId:(long)fldr_id{
    return [dao folderWithId:fldr_id];
}
//
-(NSDictionary *) folderAssets:(long)fldr_id{
    return [dao folderAssets:fldr_id];
}

-(NSArray *) folderAssetsIds:(long)fldr_id{
    return [dao folderAssetsIds:fldr_id];
}

-(NSArray *) assetInFolders:(ZMAsset *)asset{
    return [dao assetInFolders:asset];
}

-(ZMFolder *) saveFolder:(ZMFolder *)folder withDetails:(BOOL) details{
    return [dao saveFolder:folder withDetails:details];
}

- (ZMFolder *) saveSearchToFolder:(NSMutableArray *) assetsList newFolder:(ZMFolder *)newFolder
{
    return [dao saveSearchToFolder:assetsList newFolder:newFolder];
}

//- (ZMFolder *) saveSearchToFolder:(NSMutableArray *) assetsList folderName:(NSString *)folderName{
//    return [dao saveSearchToFolder:assetsList folderName:folderName];
//}


-(BOOL) removeFolder:(ZMFolder *)folder{
    return [dao removeFolder:folder];
}

-(BOOL) addAsset:(ZMAsset *)asset{
    BOOL b = [dao addAsset:asset];
    if(b){
        for(id observer in observers){
            if([observer respondsToSelector:@selector(assetDidUpdate:)]){
                [observer assetDidUpdate:asset];
            }
        }
    }
    return b;
}

-(BOOL) updateAsset:(ZMAsset *)asset{
    BOOL b = [dao updateAsset:asset];
    if(b){
        for(id observer in observers){
            if([observer respondsToSelector:@selector(assetDidUpdate:)]){
                [observer assetDidUpdate:asset];
            }
        }
    }
    return b;
}

-(BOOL) removeAssetWithUID:(long)uid{
    BOOL b = [dao removeAsset:uid];
    if(b){
        for(id observer in observers){
            if([observer respondsToSelector:@selector(assetDidUpdate:)]){
                [observer assetDidUpdate:nil];
            }
        }
    }
    return b;
}


-(NSArray *)events:(NSString *)format withSaleFranchiseId:(NSString *)saleFranchiseId{
    return [dao events:format withSaleFranchiseId:saleFranchiseId];
}

-(BOOL) saveEvent:(ZMEvent *)event{
    return [dao saveEvent:event];

}

-(BOOL)deleteEvent:(ZMEvent *)event{
    return [dao deleteEvent:event];
}

-(BOOL)duplicatedFolderName:(NSString *)folderName{
    return [dao duplicatedFolderName:folderName];
}

-(NSMutableArray *)selectMenuCategories:(ZMAsset *)filter withSearchText:(NSString *)searchText fromFolder:(long) folder_id asDictionary:(BOOL)asDictionary{
    return [dao selectCategoriesMenu:filter withSearchText:searchText fromFolder:folder_id asDictionary:asDictionary];
}

-(int)selectAllCategories{
    return [dao getAllCategoriesCount];
}

-(BOOL)saveCategory:(ZMCategory *)category{
    return [dao addCategory:category];
}

-(BOOL)saveCategoryRelated:(ZMCategory *)category{
    return [dao addCategoryRelated:category];
}

-(BOOL)updateCategory:(ZMCategory *)category{
    return [dao updateCategory:category];
}

-(BOOL) updateCategoryDate:(NSString *)date{
    return [dao updateCategoryDate:date];
}

-(NSString *) getCategoryDate{
    return [dao getCategoryDate];
}

-(BOOL)deleteCategory:(long)category{
    return [dao deleteCategory:category];
}

-(BOOL)deleteRelatedCategory:(long)assetId{
    return [dao deleteRelatedCategory:assetId];
}

-(BOOL)saveNotification:(ZMNotification *)notification{
    return [dao insertNotification:notification];
}

-(BOOL)deleteNotification:(long)notificationId{
    return [dao deleteNotification:notificationId];
}

-(BOOL)updateNotification:(ZMNotification *)notification{
    return [dao updateNotification:notification];
}

-(BOOL)updateViewedNotification:(ZMNotification *)notification{
    return [dao updateViewedNotification:notification];
}

-(ZMNotification *)selectNotification:(long)notificationId{
    return [dao findNotificationById:notificationId];
}

-(NSMutableArray *) getLastestNotifications{
    return [dao getLastNotifications];
}


-(NSArray *) getAllNotifications
{
    return [dao getAllNotifications];
}

-(int) getTotalUnreadNotifications
{
    return [dao getTotalUnreadNotifications];
}

-(NSArray *)allNotes{
    return [dao allNotes];
}

-(ZMNote *)noteWithId:(long)uid{
    return [dao noteWithId:uid];
}

-(ZMNote *) saveNote:(ZMNote *)note{
    return [dao saveNote:note];
}

-(BOOL) deleteNote:(ZMNote *)note{
    return [dao deleteNote:note];
}

-(BOOL) replaceBriefcase:(NSArray *)folders{
    return [dao replaceBriefcase:folders];
}

-(BOOL) replaceNotes:(NSArray *)notes{
    return [dao replaceNotes:notes];
}

-(void) notifyStart{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(libraryStartSync)]){
            [observer libraryStartSync];
        }
    }
}

-(void) notifyStep:(NSString *)progressText progress:(float)progress progressFor:(NSString *)progressForString{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(libraryProgressStep:progress:forVal:)]){
            [observer libraryProgressStep:progressText progress:progress forVal:progressForString];
        }
    }
}

-(void) notifyFinish{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(libraryFinishSync)]){
            [observer libraryFinishSync];
        }
    }
}


-(void)notifyStatsChanged:(NSDictionary *)assets
{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(statsChanged:)]){
            [observer statsChanged:assets];
        }
    }
}


-(void)notifyAssetsChanged{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(assetsChanged)]){
            [observer assetsChanged];
        }
    }
}

-(void) notifyError:(NSString *)withMessage{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(libraryDidFinishWithError:)]){
            [observer libraryDidFinishWithError:withMessage];
        }
    }
}

-(void) notifyInProgress{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(librarySyncInProgress)]){
            [observer librarySyncInProgress];
        }
    }
}

-(void) notifyThreadStop{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(threadStoped)]){
            [observer threadStoped];
        }
    }
}

-(void) notifyDownloadCompleted:(Asset *) asset{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(downloadAssetDidFinish:)]){
            [observer downloadAssetDidFinish:asset];
        }
    }
}

-(void) notifyDownloadFailed:(Asset *)asset error:(NSString *)error{
    for(id observer in observers){
        if([observer respondsToSelector:@selector(downloadAssetDidNotFinish:error:)]){
            [observer downloadAssetDidNotFinish:asset error:error];
        }
    }
}

-(void) prepareForNewVersion {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    [fileManager removeItemAtPath:[path stringByAppendingPathComponent:@"assets.db"] error:&error];
    dao = [[ZMAssetsDAO alloc] initWithPath:[path stringByAppendingPathComponent:@"assets.db"]];
}


- (void)clean {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

    if ([paths count] > 0) {
        
        NSError *error = nil;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Print out the path to verify we are in the right place
        NSString *directory = [paths objectAtIndex:0];
        // For each file in the directory, create full path and delete the file
        for (NSString *file in [fileManager contentsOfDirectoryAtPath:directory error:&error]) {
            
            NSString *filePath = [directory stringByAppendingPathComponent:file];
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"filePath %@", filePath]];
            if ([filePath rangeOfString:@"Pronto"].location == NSNotFound) {
                if(![[filePath lastPathComponent] isEqualToString:@"leaf.cer"]){
                    [fileManager removeItemAtPath:filePath error:&error];
                }
            }
        }
        
    }

    dao = [[ZMAssetsDAO alloc] initWithPath:[path stringByAppendingPathComponent:@"assets.db"]];
    
}

-(void)resetCoredataPath{
    [AbbvieLogging logInfo:@"Cleaning CoreData"];
     [[ZMProntoManager sharedInstance]cleanAndResetupDB];
}

-(BOOL) searchCompleted:(BOOL) completion
{
    completedStatus = completion;
    return completedStatus;
}

-(BOOL) completionStatus
{
    return completedStatus;
}

-(BOOL) checkFinalStatus:(BOOL)completion
{
    finalStatus = completion;
    return finalStatus;
}


@end
