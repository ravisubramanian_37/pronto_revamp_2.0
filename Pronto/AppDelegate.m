//
//  AppDelegate.m
//  Pronto
//
//  Created by Sebastian Romero on 11/27/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

// Development

#import "AppDelegate.h"
#import "ProntoInitialSetUpController.h"
#import "NotificationsServiceClass.h"


#import "ZMLogin.h"
#import "ZMAssetViewController.h"
#import "FastPDFViewController.h"
//#import "ZMLibrary.h"
#import "ZMAssetsLibrary.h"
#import "ZMTracking.h"
#import "ZMBrightcove.h"
#import "AFNetworkActivityLogger.h"
#import <HockeySDK/HockeySDK.h>
#import "AFNetworkActivityLogger.h"
#import <MagicalRecord/MagicalRecord.h>
#import <CoreData/CoreData.h>
#import <AVFoundation/AVFoundation.h>
#import "Pronto-Swift.h"
#import "Constant.h"
#import "ProntoUserDefaults.h"
#import "ZMUserActions.h"
//#import "GlobalSearchVC.h"
#import "BucketDetailDataControl.h"

//AOFKit Implementation
#import <AOFKit/AOFKit.h>

#import <sys/socket.h>
#import <netinet/in.h>

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;

//@class MyClass;

@interface AppDelegate() {
    
    NSURL *URLAction;
    int AssetAction;
    BOOL profileInActive;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //If switch to old implementation
    _openedDeeplinkAsset = NO;
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isRevampingEnabled"];
    if ([self isAppAlreadyLaunchedOnce]) {
      [[NSUserDefaults standardUserDefaults]setBool:NO forKey:RedesignConstants.isPreferenceRequired];
      [ProntoRedesignSingletonClass sharedInstance].callPreferencesApi = false;
    } else {
      [[NSUserDefaults standardUserDefaults]setBool:YES forKey:RedesignConstants.isPreferenceRequired];
    }
    self.featureSwitchkey = [[NSUserDefaults standardUserDefaults] valueForKey:@"isRevampingEnabled"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"changepreference"];
    [ProntoRedesignSingletonClass sharedInstance].savedCategoryIDs = [NSMutableArray new];
    if(self.featureSwitchkey){
        //New revamping features
//        [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"waitTime"];
        
        //Added on assumptions
         [ZMTracking configure];
       
        //Register for Push Notification
        [ProntoInitialSetUpController.sharedInstance registerAPNS];
        
         
//    }
//    else{
        
//    [ZMTracking configure];
    
//    [ZMUserActions sharedInstance].selectLeftMenu = @"";
    
    // root config
//    NSString* config1 = [self getPathResource:@"EnvironmentConfig"];
    //added by Prathap: Now this reads the data from Config file automatically based on the scheme it is being build.
    // endpointURL - stage,preprod,production
    //The 'CONFIG' variable defined in the Preprocessor's macro in the Build Settings.
//    NSDictionary* endPoint = [config1 valueForKey:CONFIG];
    //End of added by Prathap.
    
//    [self registerAPNS];
    
    //For AOFKit
    [self registerSecurityLibraryNotifications];
    
//    [[ProntoUserDefaults userDefaults] setAOFAccessToken:@""];
//    NSString* accessToken = [[ProntoUserDefaults userDefaults] getAOFAccessToken];
//    if(accessToken.length <= 0 || accessToken == nil)
//    {
//        [self displayLogin];
//    }
    
    //Instantiate the singlenton API manager
//    _apiManager = [ZMAbbvieAPI sharedAPIManager];
    
//    //Hockey App implementation
//    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:[endPoint valueForKey:@"BITHockeyIdentifier"]];
//    [[BITHockeyManager sharedHockeyManager] startManager];
//    [[BITHockeyManager sharedHockeyManager].crashManager setCrashManagerStatus:BITCrashManagerStatusAutoSend];
//    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    
    [self.window makeKeyAndVisible];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    //Abbvie Security framework implementation
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsDirectory = [[fileManager URLsForDirectory:NSDocumentDirectory
                                                     inDomains:NSUserDomainMask] firstObject];
    
    NSString *firstTimeCheck = [[NSUserDefaults standardUserDefaults] objectForKey:@"First_Time_Check"];
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"First_Time_Check"] || firstTimeCheck.length <= 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"First_Time_Check"];
        
        /**
         * @Tracking
         * User Error
         **/
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd, yyyy hh:mm a"];
        NSString *installDate = [dateFormat stringFromDate:[NSDate date]];
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        [trackingOptions setObject:@"installed" forKey:@"InstallEvent"];
        [trackingOptions setObject:installDate forKey:@"InstallDate"];
        [ZMTracking trackSection:@"login" withSubsection:FALSE withName:nil withOptions:trackingOptions];
    }
    
    // Set the settings application defaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    [[NSUserDefaults standardUserDefaults] setObject:version forKey:@"appBuild"];
    
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [[NSUserDefaults standardUserDefaults] setObject:build forKey:@"appVersion"];
    
    [[AOFLoginManager sharedInstance] setAOFKitVersion];
    
    [defaults synchronize];
    
    //New StatusBar implementation
    application.statusBarHidden = NO;
    application.statusBarStyle = UIStatusBarStyleLightContent;
    
    
    NSError *categoryError = nil;
    BOOL success = [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&categoryError];
    
    if (!success)
    {
        [AbbvieLogging logError:[NSString stringWithFormat:@"AppDelegate Debug - Error setting AVAudioSession category.  Because of this, there may be no sound. `%@`", categoryError]];
    }
    [[ZMProntoManager sharedInstance]setupDB];
//    [[[BucketDetailDataControl alloc] init] initializeBucketIDValues];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *applicationSupportDirectory = [paths lastObject];
    NSString *applicationName = [[[NSBundle mainBundle] infoDictionary] valueForKey:(NSString *)kCFBundleNameKey];
//    NSString *documentSamples = @"Samples";
      
    NSURL *urld = [documentsDirectory URLByAppendingPathComponent:applicationName];
    NSString *myString = [urld absoluteString];
    
    // check if asset is protected
    if ([UIApplication sharedApplication].protectedDataAvailable) {
        
        // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
        NSError *error;
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSDictionary *attrsAppDirectory = [fileManager attributesOfItemAtPath:applicationSupportDirectory error:&error];
        NSDictionary *attrsString = [fileManager attributesOfItemAtPath:myString error:&error];
        
        if(![[attrsAppDirectory objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
        {
            attrsAppDirectory = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            BOOL success = [fileManager setAttributes:attrsAppDirectory ofItemAtPath:applicationSupportDirectory error:&error];
            if (!success)
            [AbbvieLogging logInfo:@"Set ~/Documents attrsAppDirectory NOT successfull"];
        }
        
        if(![[attrsString objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
        {
            attrsString = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            BOOL success = [fileManager setAttributes:attrsString ofItemAtPath:applicationSupportDirectory error:&error];
            if (!success)
            [AbbvieLogging logInfo:@"Set ~/Documents attrsString NOT successfull"];
        }
    }
    
//    [ZMUserActions sharedInstance].shouldShowFranchiseListView = YES;
    
//    [ZMUserActions sharedInstance].shoudRefreshEventData = YES;
    
    if(getenv("NSZombieEnabled")) {
        [AbbvieLogging logInfo:@"*********** NSZombieEnabled enabled! *****************"];
    }
    
    [ProntoInitialSetUpController.sharedInstance checkForVersionUpgrade];
    [ProntoInitialSetUpController.sharedInstance performAssetMigration];
    [ProntoInitialSetUpController.sharedInstance initializeAbbvieLogging];
    [ProntoInitialSetUpController.sharedInstance checkForAOFLogoutInSettings];
//    [self loadHTTPCookies];
    
//    [ZMUserActions sharedInstance].shouldSwitchToOtherTab = YES;
    
    NSString *appCenterConstant = @"";
    if ([CONFIG isEqualToString:ProductionEnvironment] == YES)
    {
        appCenterConstant = appCenterConstProd;
    }
    else if ([CONFIG isEqualToString:PreProdEnvironment] == YES)
    {
        appCenterConstant = appCenterConstPreProd;
    }
    else
    {
        appCenterConstant = appCenterConstStage;
    }
    
        [MSACAppCenter start:appCenterConstant withServices:@[
            [MSACAnalytics class],
            [MSACCrashes class]
    ]];
    
    }
    return YES;
}

- (BOOL)isAppAlreadyLaunchedOnce {
    if ([[NSUserDefaults standardUserDefaults] boolForKey: RedesignConstants.isAppAlreadyLaunchedOnce])
    {
        return true;
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: RedesignConstants.isAppAlreadyLaunchedOnce];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: RedesignConstants.firstTimeloadingAssets];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return false;
    }
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    //For either external assets or URL schemas
    if (url) {
        //Check needed to display the correct message and perform the correct actions
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"First_Time_Check"] isEqualToString:@"YES"]) {
            _assetToOpenURL = nil;
        }
        else {
            _assetToOpenURL = url;
            
            //ABT-236 New implementation to open shema asset
            // Pronto-31 - From Schema Asset - Optimization of Deep Linking Feature
            NSString *urlString = [url absoluteString];
            
            // to avoid crash on removing the certificate
            if([urlString isEqualToString:@"abbviepronto://://"])
                _assetToOpenURL = nil;
            else
            {

            if([urlString containsString:@"abbviepronto://"])
              [ZMUserActions sharedInstance].checkURLSchema = YES;
              [AOFLoadingController sharedInstance].fromDeeplink = YES;
              _openedDeeplinkAsset = NO;
            
            [self openPendingAssetWitCompletion:^(NSString *idAsset) {
//                [ZMUserActions sharedInstance].checkURLSchema = YES;
                [self openSchemaAssetWithAppWithOutOpen:idAsset];
            } failure:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CMS_BRING_ALL_DATA" object:self userInfo:nil];
            }];
            }
            
        }
        
    }
    
    // Indicate that we have successfully opened the URL
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [AbbvieLogging logInfo:@"Background"];
//    [self saveHTTPCookies];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [AbbvieLogging logInfo:@"Will enter foreground"];
    
//    if(_featureSwitchkey){
//
//    }
//    else{
    
    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
    
//    ZMLibrary* library = [[ZMLibrary alloc] init];
//
//    if([topController.visibleViewController isKindOfClass:[ZMLibrary class]])
//    {
//        library = (ZMLibrary*) [topController visibleViewController];
//    }
//    else if([topController.visibleViewController isKindOfClass:[PreferencesListViewController class]] ||[topController.visibleViewController isKindOfClass:[ProntoTabbarViewController class]] )
//    {
//        library = [[ZMLibrary alloc] init];
//    }
    

//    [AbbvieLogging logInfo:[NSString stringWithFormat:@"top controllers:%@",library]];
    
    if([ProntoRedesignSingletonClass sharedInstance].lastSearchText.length > 0 && [ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
    {
        
        _topview = [[UIView alloc] initWithFrame:CGRectMake(0, 100, [UIApplication sharedApplication].statusBarFrame.size.width, 50)];
        _topview.backgroundColor = [UIColor lightGrayColor];
        _topview.tag = 5001;
        [topController.view addSubview:_topview];
        
        _lblView = [[UILabel alloc] initWithFrame:CGRectMake(20, 100, topController.view.frame.size.width, 50)];
        [_lblView setFont:[UIFont boldSystemFontOfSize:18.0]];
        _lblView.textColor = [UIColor whiteColor];
        _lblView.text = @"Want to check if any new assets and events available?";
        _lblView.tag = 5002;
        [topController.view addSubview:_lblView];
        
        _yesButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _yesButton.frame = CGRectMake(topController.view.frame.origin.x+480, 100, 50, 50);
        [_yesButton setImage:[UIImage imageNamed:@"selectedButton"] forState:UIControlStateNormal];
        [_yesButton addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        _yesButton.tag = 5003;
        [topController.view addSubview:_yesButton];
        
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeButton.frame = CGRectMake(topController.view.frame.origin.x+530, 100, 50, 50);
        [_closeButton setImage:[UIImage imageNamed:@"closeIcon"] forState:UIControlStateNormal];
        [_closeButton addTarget:self  action:@selector(dismissView:) forControlEvents:UIControlEventTouchUpInside];
        _closeButton.tag = 5004;
        [topController.view addSubview:_closeButton];
        
//        if(currentIndex == 0 && library.section == ProntoSectionTypeLibrary)
//        {
//            // display promoted content
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                if([_apiManager isConnectionAvailable])
//                    [library displayPromotedContent:@"foreground_call"];
//                else
//                    [library offlinePromotedChanges];
//            });
//        }
        
        
    }
//    else if(currentIndex == 0 && library.section == ProntoSectionTypeLibrary && [_apiManager isConnectionAvailable])
//    {
//        ZMLibrary* library = [[ZMLibrary alloc] init];
//        if(topController.visibleViewController.childViewControllers.count > 0)
//            library = (ZMLibrary*)[topController.visibleViewController.childViewControllers objectAtIndex:0];
//        else if([topController.visibleViewController isKindOfClass:[ZMLibrary class]])
//            library = (ZMLibrary*) [topController visibleViewController];
//        else if([topController.visibleViewController isKindOfClass:[PreferencesListViewController class]])
//            library = [[ZMLibrary alloc] init];
//
//        [self performSelector:@selector(reSyncLibrary) withObject:nil afterDelay:1];
//
//        // fix for resolving crash issue from Asset Detail screen
//        if([library isKindOfClass:[ZMLibrary class]])
//        {
//          [library changeTintColor];
//          // display promoted content
//          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            if([_apiManager isConnectionAvailable]) {
//                [library displayPromotedContent:@"foreground_call"];
//                [library showLoadingIndicatorForPromotedContent:YES];
//            }
//            else
//                [library offlinePromotedChanges];
//          });
//        }
//    }
    else
    {
//        ZMLibrary* library = [[ZMLibrary alloc] init];
        [self performSelector:@selector(reSyncLibrary) withObject:nil afterDelay:1];
//        [library changeTintColor];
        // display promoted content
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            if([_apiManager isConnectionAvailable])
//                [library displayPromotedContent:@""];
//            else
//                [library offlinePromotedChanges];
//        });
    }
    
    if ([topController.visibleViewController.childViewControllers isKindOfClass:NSClassFromString(@"MPInlineVideoFullscreenViewController")]) {
        
        if ([[topController.viewControllers objectAtIndex:1] isKindOfClass:[ZMBrightcove class]]) {
            ZMBrightcove *viewController = (ZMBrightcove *)[topController.viewControllers objectAtIndex:1];
            [viewController resetVideo];
        }
    }
    
    [self registerAPNS];
    [ProntoInitialSetUpController.sharedInstance checkForAOFLogoutInSettings];
    [self loadHTTPCookies];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [AbbvieLogging logInfo:@"applicationDidBecomeActive"];
    BOOL hasInternet = [self hasConnectivity];
//    NSString *networkType = [self checkNetwork];
    if (hasInternet && ![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
        [self showMessage:@"The internet connection is not available, please check your network connectivity" whithType:ZMProntoMessagesTypeWarning withTime:4];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [MagicalRecord cleanUp];
    [self unRegisterNotifications];
    [self saveHTTPCookies];
}

#pragma mark - Helpers



- (void)reSyncLibrary {
    
    if ([AOFLoginManager sharedInstance].checkForAOFLogin) {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"CMS_BRING_ALL_DATA" object:self userInfo:nil];
            ;
    }
}

/**
 * Handles the login display
 **/
//- (void)displayLogin
//{
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//    [AbbvieLogging logInfo:@"displayLogin"];
//    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
//    // Lets remove the device token if registered
//    if (self.deviceToken) {
//
//        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//    }
//
//    ZMLibrary *library;
//
//     // if child controllers are avail
//    if(topController.visibleViewController.childViewControllers.count > 0)
//    {
//      if ([[topController.visibleViewController.childViewControllers objectAtIndex:0] isKindOfClass:[ZMLibrary class]]) {
//        library = (ZMLibrary*) [topController.visibleViewController.childViewControllers objectAtIndex:0];
//        [library initLibraryAfterLogout];
//      }
//    }
//    else
//    {
//         // if no child controllers are avail
//         if ([topController.visibleViewController isKindOfClass:[ZMLibrary class]])
//         {
//            library = (ZMLibrary*) [topController visibleViewController];
//            [library initLibraryAfterLogout];
//         }
//    }
//
//    // if child controllers are avail
//    if(topController.visibleViewController.childViewControllers.count > 0)
//    {
//        if ([topController.visibleViewController.childViewControllers isKindOfClass:[ZMLibrary class]]) {
//            [library displayLogin];
//        }
//        else if(![topController.visibleViewController.childViewControllers isKindOfClass:[ZMLogin class]]) {
//            [topController popToRootViewControllerAnimated:NO];
//            [library displayLogin];
//        }
//    }
//    else
//    {
//        // if no child controllers are avail
//        if ([topController.visibleViewController isKindOfClass:[ZMLibrary class]]) {
//            [library displayLogin];
//        }
//        else if(![topController.visibleViewController isKindOfClass:[ZMLogin class]]) {
//            [topController popToRootViewControllerAnimated:NO];
//            [library displayLogin];
//        }
//
//    }
//    });
//}

/**
 *  Get a localizable Message
 *  @param code Message code from the file
 *  @param aMessage Alternate message if the message is not found on localizable file
 *  @return return NSString formatted message
 */
- (NSString *)getLocalizableMessage:(NSInteger)code withAlternateMessage: (NSString *) aMessage {
    
    NSString *messageString = [NSString stringWithFormat:@"MESSAGE_%ld", (long)code];
    NSString *desc = NSLocalizedString(messageString, aMessage);
    return desc;
}

/**
 *  Handles the url action from remote notification
 *  @param urlDictionary urlDictionary dictionary within the url action
 */
- (void)handlerNotificationAction:(id)action withType:(enum ZMProntoNotificationAlert)type {
    
    NSURL *url;
    NSString *urlTitle;
    NSInteger messageId = 0;
    NSDictionary *urlDictionary;
    NSString * message;
    NSString *label;
    
    switch (type) {
        case ZMProntoNotificationAlertTypeURL:
            urlTitle = @"External Link";
            
            if([action isKindOfClass:[NSArray class]]) {
                urlDictionary = (NSDictionary *)[action objectAtIndex:0];
            }
            messageId = 101;
            
            if([urlDictionary objectForKey:@"url"]) {
                url = [NSURL URLWithString:[urlDictionary objectForKey:@"url"]];
                URLAction = url;
                
            }
            if([urlDictionary objectForKey:@"title"])
            {
                urlTitle = [urlDictionary objectForKey:@"title"];
            }
            message = [NSString stringWithFormat:[self getLocalizableMessage:messageId withAlternateMessage:@"%@"], url];
            label = @"View Message";
            break;
        case ZMProntoNotificationAlertTypeAsset:
        case ZMProntoNotificationAlertTypeApp:
        case ZMProntoNotificationAlertTypeAssetNotFound:
            
            if (type == ZMProntoNotificationAlertTypeAsset) {
                
                if (action) {
                    
                    messageId = 102;
                    urlTitle = @"Open Asset";
                    AssetAction = [action intValue];
                    
                    NSString* assetName;
                    
                    NSFetchRequest * fetchrequest = [Asset MR_requestAll];
                    NSArray *importedAssets = [Asset MR_executeFetchRequest:fetchrequest];
                    
                    
                    for (NSDictionary * asset in importedAssets) {
                        
                        NSString * assetId = [[asset valueForKey:@"assetID"] stringValue];
                        
                        NSString* aID = [NSString stringWithFormat:@"%d",[action intValue]];
                        
                         if([assetId isEqualToString:aID])
                         {
                             assetName = [asset valueForKey:@"title"];
                         }
                        
                    }
                    
                    ZMAsset *asset = [[ZMAssetsLibrary defaultLibrary] assetWithUID:[action intValue]];
                    if(asset.name.length == 0 && assetName.length != 0)
                        [asset setName:assetName];
                    
                    //asset.name =
                    
                    if (assetName.length > 0) {
                        label = @"Open Asset";
                        message = [NSString stringWithFormat:[self getLocalizableMessage:messageId withAlternateMessage:@"%d"], assetName];
                    }
                    else {
                        messageId = 104;
                        urlTitle = @"Asset not found";
                        label = @"Synchronize";
                        message = [NSString stringWithFormat:[self getLocalizableMessage:messageId withAlternateMessage:@"%d"], asset.name];
                        type = ZMProntoNotificationAlertTypeAssetNotFound;
                    }
                }
                
            }
            else {
                messageId = 103;
                urlTitle = @"Open App";
                label = @"View App";
                URLAction = [NSURL URLWithString:action];
                message = [self getLocalizableMessage:messageId withAlternateMessage:@"%@"];
            }
            break;
    }
    
    
    // Alternative AlertView
    UIAlertController* confirm = [UIAlertController alertControllerWithTitle:urlTitle  message:message  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle OK button
                               }];
    
    UIAlertAction* otherButton = [UIAlertAction
                               actionWithTitle:label
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle Other button
                                   
                                   [self alertViewTag:type clickedButtonAtIndex:1];
                               }];
    
    [confirm addAction:okButton];
    [confirm addAction:otherButton];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    [viewController presentViewController:confirm animated:YES completion:nil];
}

/**
 *  Triggers the update notification on library
 */
//- (void)updateBadge {
//    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
//    UIViewController* viewController = [topController visibleViewController];
    
//        ZMLibrary *library = (ZMLibrary *)viewController;
//        [library getNotifications];
//}

/**
 *  Shows a messages with a given type
 *  @param message The message to display
 *  @param type the type of the message
 *  @param action the action for the retry button
 */
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withAction:(void(^)(void))mAction {
    
    if (!_currentMessage) {
        UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
        
        _currentMessage = [[ZMMessage alloc] initMessage:topController.view withMessage:message andType:type withAction:mAction];
        _currentMessage.delegate = self;
    }
}

/**
 *  Shows a messages with a given type
 *  @param message The message to display
 *  @param type the type of the message
 */
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type {
    
    BOOL alerDisplayed = NO;
    
    if (_assetToOpenURL) {
        
        if ([message isEqualToString:@"The asset was not found"]) {
            
            alerDisplayed = YES;
            
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //[alert show];
            
            // Alternative AlertView
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""  message:message  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle OK button
                                       }];
            
            [alert addAction:okButton];
            UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
                viewController = viewController.presentedViewController;
            }
            [viewController presentViewController:alert animated:YES completion:nil];
            
            
        }
    }
    
    if (!alerDisplayed) {
        
        if(!_currentMessage) {
            UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
            
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
                UIViewController* viewController = topController.visibleViewController;
                _currentMessage = [[ZMMessage alloc] initMessage:viewController.view withMessage:message andType:type];
            }
            else {
                _currentMessage = [[ZMMessage alloc] initMessage:topController.view withMessage:message andType:type];
            }
            _currentMessage.delegate = self;
        }
        else {
            _currentMessage.type = type;
            _currentMessage.message = message;
        }
    }
}

/**
 *  Shows a messages with a given type and hides after delay
 *  @param message The message to display
 *  @param type the type of the message
 *  @param time to close the message
 */
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withTime:(NSTimeInterval)time {
    [self showMessage:message whithType:type];
    
    if(_currentMessage) {
        [self performSelector:@selector(hideMessage) withObject:nil afterDelay:time];
    }
}


/**
 *  Hides the message
 */
- (void)hideMessage {
    if(_currentMessage) {
        
        [_currentMessage hideMessage:^(BOOL finished) {
            [_currentMessage removeFromSuperview];
            _currentMessage = nil;
        }];
    }
}

/**
 *  Shows the error
 *  @param error
 */
- (void)showError:(NSError *)error {
    
    if (error.code) {
        
        //Validate if there is internet connection
        NSString *message = @"The Internet connection is not available, please check your network connectivity.";
        if ([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
            message = [NSString stringWithFormat:@"%@", error.localizedDescription];
        }
        //Checking if the server is not under maintenance
        if (_apiManager.lastServerCodeResponse == 503) {
            message = @"The server is currently under maintenance mode.";
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self showMessage:message whithType:ZMProntoMessagesTypeWarning withAction:^{
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"session %@", [AOFLoginManager sharedInstance].upi]];
                [self hideMessage];
                [self performSelector:@selector(initPronto) withObject:nil afterDelay:.5];
            }];
        });
        
    }
}


- (void)redirectToLibrary
{
        dispatch_async(dispatch_get_main_queue(), ^{
        
        UINavigationController *topController = (UINavigationController *) self.window.rootViewController;
        
        if ([topController.visibleViewController.childViewControllers isKindOfClass:[ZMLogin class]])
        {
            ZMLogin *login = (ZMLogin *) [topController.visibleViewController.childViewControllers objectAtIndex:0];
            [login close];
        }
        else if ([topController isKindOfClass:[UINavigationController class]])
        {
                [topController popToRootViewControllerAnimated:YES];
                [topController viewWillAppear:NO];
        }
    });
}

/**
 *  Init the Pronto Application
 */
- (void)initPronto{
    
    //Initialize the asset libray. Create the database
    [self validateUserStatus];
    
    //Open the asset set in the openURL method
    //    [self openPendingAsset];
    
    if ([AOFLoginManager sharedInstance].checkForAOFLogin) {
        
        [[NSUserDefaults standardUserDefaults] setObject:[AOFLoginManager sharedInstance].upi forKey:@"User_UPI"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //New implementation of login module
        [_apiManager userLogin:^(NSString *token) {
            
            //Storing the UPI to know if a new user logs in. This is checked in the library viewDidAppear
            [[ProntoUserDefaults userDefaults] setCurrentUserID:[AOFLoginManager sharedInstance].upi];
            //ABT-236 New implementation to open shema asset
            
            [self openPendingAssetWitCompletion:^(NSString *idAsset) {
                   [ZMUserActions sharedInstance].checkURLSchema = YES;
                [self openSchemaAssetWithAppWithOutOpen:idAsset];
            } failure:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CMS_BRING_ALL_DATA" object:self userInfo:nil];
            }];
            
            
        } error:^(NSError *error) {
            
            if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
                [self showError:error];
            }
            else
            {
                [AbbvieLogging logError:@"> AppDelegate initPronto Error"];
                [self initPronto]; //Recursive call to atempt to login the user if there is an error
            }
            
            NSMutableDictionary *options = [[NSMutableDictionary alloc] initWithDictionary:@{@"scloginStatus":@"unsuccessful"}];
            [ZMTracking trackSection:@"login" withSubsection:FALSE withName:FALSE withOptions:options];
        }];
    }
    else
    {
        [AbbvieLogging logInfo:@"No session log back in"];
//        [self displayLogin];
    }
    
}

//TO-DO this method should be removed and the initialization must be made instead of this method
- (void)validateUserStatus {
    
    [[ZMAssetsLibrary defaultLibrary] initWithPath:[AOFLoginManager sharedInstance].documentsDir];
}

-(BOOL)hasConnectivity {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;

    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if (reachability != NULL) {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
            {
                // If target host is not reachable
                CFRelease(reachability);
                return NO;
            }

            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
            {
                // If target host is reachable and no connection is required
                //  then we'll assume (for now) that your on Wi-Fi
                CFRelease(reachability);
                return YES;
            }


            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                // ... and the connection is on-demand (or on-traffic) if the
                //     calling application is using the CFSocketStream or higher APIs.

                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    // ... and no [user] intervention is needed
                     CFRelease(reachability);
                    return YES;
                }
            }

            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                // ... but WWAN connections are OK if the calling application
                //     is using the CFNetwork (CFSocketStream?) APIs.
                 CFRelease(reachability);
                return YES;
            }
             CFRelease(reachability);
        }
        else{
            CFRelease(reachability);
        }
    }

    return NO;
}

/*!
 *Checks for the current type of network connection
 */
- (NSString *)checkNetwork{
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        // Fix for iOS 12 issue
        NSArray *subviews = nil;
        id statusBar = nil;
        if (@available(iOS 13.0, *)) {
           statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
        } else {
            // Fallback on earlier versions
        }
        
        if ([statusBar isKindOfClass:NSClassFromString(@"UIStatusBar_Modern")]) {
            subviews = [[[statusBar valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
        } else {
            subviews = [[statusBar valueForKey:@"foregroundView"] subviews];
        }
        NSNumber *dataNetworkItemView = nil;
        
        for (id subview in subviews) {
            if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
                dataNetworkItemView = subview;
                break;
            }
        }
        
        NSString *typeConnectionString = @"";
        
        if ([dataNetworkItemView valueForKey:@"dataNetworkType"]) {
            
            switch ([[dataNetworkItemView valueForKey:@"dataNetworkType"] integerValue]) {
                case 0:
                    typeConnectionString = @"Offline"; //No wifi or cellular
                    break;
                    
                case 1:
                    typeConnectionString = @"GPRS/EDGE"; //2G or ealrier
                    break;
                    
                case 2:
                    typeConnectionString = @"3G";
                    break;
                    
                    //3: 4G - 4: LTE
                case 3:
                case 4:
                    typeConnectionString = @"OTHERS";
                    break;
                    
                case 5:
                    typeConnectionString = @"Wifi";
                    break;
                    
                default:
                    break;
            }
        }
        else {
            typeConnectionString = @"Offline"; //No wifi or cellular
        }
        
        return typeConnectionString;
    }
    
    return @"Unknown";
}

- (void)registerAPNS{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
            else{
                NSLog( @"Push registration FAILED" );
                NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
            }
        }];
    }
  
    
}

- (void)openSchemaAsset:(NSArray *)assetArray {
    
    if (assetArray.count > 0) {
        Asset * asset = assetArray[0];
        NSDictionary *userDic = @{@"asset":asset};
        [ZMUserActions sharedInstance].countOfAssetDict = 0;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_SCHEMA_ASSET" object:self userInfo:userDic];        
    }else{
        [self showMessage:@"Asset not found" whithType:ZMProntoMessagesTypeWarning];
    }
}


- (void)openSchemaAssetWithAppWithOutOpen:(NSString *)asset {
   [ZMUserActions sharedInstance].checkURLSchema = YES;
    if (!asset) {
        [self showMessage:@"Asset not found" whithType:ZMProntoMessagesTypeWarning];
    }
    else {
        NSDictionary *userDic = @{@"assetString":asset};
      [HomeViewController sharedInstance].deeplinkAsset = asset;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_SCHEMA_ASSET_APP_CLOSED" object:self userInfo:userDic];
    }
}

-(void)openPendingAssetWitCompletion:(void(^)(NSString* idAsset))completion failure:(void(^)())failure{
    
    if (_assetToOpenURL) {
        
       [self resetLibraryView];
        
        if ([_assetToOpenURL isFileURL]) { //Local file
            NSDictionary *userDic = @{@"asset_link":_assetToOpenURL};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_TEMP_ASSET" object:self userInfo:userDic];
            failure();
        }
        else { //URL Schemas file
               [ZMUserActions sharedInstance].checkURLSchema = YES;
           
            
            
            NSString* idAsset = [[[_assetToOpenURL host] stringByRemovingPercentEncoding] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            if (idAsset) {
                completion(idAsset);
            }
        }
    }else{
        failure();
    }
}

- (void)openPendingAsset{
    
    
    if (_assetToOpenURL) {
        
        [self resetLibraryView];
        
        if ([_assetToOpenURL isFileURL]) { //Local file
            NSDictionary *userDic = @{@"asset_link":_assetToOpenURL};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_TEMP_ASSET" object:self userInfo:userDic];
        }
        else { //URL Schemas file
            
            
              NSString* idAsset = [[[_assetToOpenURL host] stringByRemovingPercentEncoding] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            if (idAsset) {
                NSNumber * assetId = [NSNumber numberWithInt:[idAsset intValue]];
                NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
                [self performSelector:@selector(openSchemaAsset:) withObject:assetArray afterDelay:1.0];
            }
        }
        
        _assetToOpenURL = nil;
    }
}

- (void)resetLibraryView{
    
    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
    [topController popToRootViewControllerAnimated:NO];
    
    //To avoid issues pop showning on View Controllers where they shouldn't be.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HIDE_ASSET_POPOVERS" object:self userInfo:nil];
    //This line is specifically inmportant because when a pop-over is shwon it becomes the topViewController and views can't be pushed to such controllers.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HIDE_LIBRARY_POPOVERS" object:self userInfo:nil];
}

-(void)loadHTTPCookies
{

}

-(void)saveHTTPCookies
{

}

# pragma mark - Alert view

- (void)alertViewTag:(int)alertViewTag clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertViewTag == 11)
    {
        if(buttonIndex == 1)
        {
            // sync
             [self performSelector:@selector(reSyncLibrary) withObject:nil afterDelay:1];
            
        }
        else
        {
            // Cancel
        
        }
    }
    else
    {
    switch (alertViewTag)
    {
            
        case ZMProntoNotificationAlertTypeURL:
        case ZMProntoNotificationAlertTypeApp:
            
            if (URLAction) {
                if(buttonIndex == 1){
                    
                    if ([[UIApplication sharedApplication] canOpenURL:URLAction]) {
                        UIApplication *application = [UIApplication sharedApplication];
                        [application openURL:URLAction options:@{} completionHandler:nil];

                    }
                    else {
                        [self showMessage:@"Your url cannot be open" whithType:ZMProntoMessagesTypeWarning];
                    }
                    
                }
                URLAction = nil;
            }
            break;
        case ZMProntoNotificationAlertTypeAsset:
            
            if (buttonIndex == 1) {
                
                if (AssetAction && (AssetAction != -1)) {
                    
                    [self resetLibraryView];
                    
                    NSNumber * assetId = [NSNumber numberWithInt:AssetAction];
                    NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
                    [self openSchemaAsset:assetArray];
                    AssetAction = -1;
                }
            }
            break;
        case ZMProntoNotificationAlertTypeAssetNotFound:
            
            if(buttonIndex == 1) {

                    //Tell the library to bring all the data
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"CMS_BRING_ALL_DATA" object:self userInfo:nil];
            }
            break;
    }
    }
}

#pragma mark - Abbvie Security Framework

//- (void)checkForAOFLogoutInSettings
//{
//    BOOL logoutVal = [[NSUserDefaults standardUserDefaults] boolForKey:@"AOFKitDoLogout"];
//    if(logoutVal == YES)
//    {
//        //Logout on launch
//        ZMLibrary* library = [[ZMLibrary alloc]init];
//        [library initLibraryAfterLogout];
//        
//        if([AOFLoginManager sharedInstance].checkForAOFLogin)
//        {
//            [[AOFLoginManager sharedInstance] AOFKitLogout];
//        }
//        
//        [ZMTracking trackSection:@"login"
//                  withSubsection:FALSE
//                        withName:FALSE
//                     withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"scloginStatus":@"unsuccessful"}]];
//        
//        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"AOFKitDoLogout"];
//    }
//    else
//    {
//        NSLog(@"No need to logout");
//    }
//}

- (void)registerSecurityLibraryNotifications {
    
    NSNotificationCenter *dc = [NSNotificationCenter defaultCenter];
    [dc addObserver:self selector:@selector(handleNotification:) name:kAOFLoginSuccessNotification object:nil];
    [dc addObserver:self selector:@selector(handleNotification:) name:kAOFLogoutSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CMS_BRING_ALL_DATA" object:nil];
}

- (void)registerDeeplinkNotification {
  NSNotificationCenter *dc = [NSNotificationCenter defaultCenter];
  [dc addObserver:self selector:@selector(handleNotification:) name:@"OPEN_SCHEMA_ASSET_APP_CLOSED" object:nil];
}


-(void) unRegisterNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAOFLoginSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAOFLogoutSuccessNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OPEN_SCHEMA_ASSET_APP_CLOSED" object:nil];
}

- (void)handleNotification:(NSNotification *)notification{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"****session name %@", notification.name]];
    
    if ([[notification name] isEqualToString:kAOFLoginSuccessNotification]) {
        
        [[ZMAssetsLibrary defaultLibrary] resetCoredataPath];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"First_Time_Check"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyUserDefaultGridFranchise];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Off_Start"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self registerAPNS];
        [self initPronto]; //It has to always be called when the session has started
        if([[[NSUserDefaults standardUserDefaults]valueForKey:@"isFirstTime"] isEqualToString:@"NO"]){
            [self redirectToLibrary]; //In any case redirect to the library
        }
        /**
         * @Tracking
         * User authentication
         **/
        NSMutableDictionary *options = [[NSMutableDictionary alloc] initWithDictionary:@{@"scloginStatus":@"successful", @"scusertype":[ZMTracking userType]}];
        [ZMTracking trackSection:@"login" withSubsection:FALSE withName:FALSE withOptions:options];
        
    }
    else if ([[notification name] isEqualToString:kAOFLogoutSuccessNotification])
    {
        [[ZMAssetsLibrary defaultLibrary] resetCoredataPath];
//        [[NSUserDefaults standardUserDefaults]setInteger:3 forKey:@"waitTime"];
        [[NSNotificationCenter defaultCenter] postNotificationName:kSignOutOnLaunchNotification object:nil];
        [ProntoInitialSetUpController.sharedInstance sessionEnd];
    } else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_APP_CLOSED"]) {
//      _resestFranchises = NO;
      //Once listen, Next time it should listen only after view will apper.
      NSString * idAsset = [notification.userInfo objectForKey:@"assetString"];
      
      //PRONTO-37 Deeplinking to a Page in a PDF
      // same format but caseinsensitive
      NSInteger pageNumber = 0;
      NSArray* deeplinkPage = [idAsset componentsSeparatedByString:@"&"];
      NSArray* deeplinkPageNo = [idAsset componentsSeparatedByString:@"="];
      NSString* stringToCheck = [NSString stringWithFormat:@"%@%@",@"pageNum=",[deeplinkPageNo lastObject]];
      if(deeplinkPage.count >1 )
      {
          if( isContainedIn(deeplinkPage,[deeplinkPage lastObject]))
          {
              idAsset = [idAsset stringByReplacingOccurrencesOfString:[deeplinkPage lastObject] withString:stringToCheck];
              NSArray* pageNums = [idAsset componentsSeparatedByString:@"&pageNum="];
              
              if (pageNums.count > 1) {
                  pageNumber = [[pageNums lastObject] integerValue];
                  idAsset = [pageNums firstObject];
              }
          }
      }
      
      NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:idAsset];
      if (assetArray.count > 0)
      {
          Asset * asset = assetArray[0];
          if ([asset.type isEqualToString:[EventOnlyAsset TypeAttributeValueAsString]])
          {
//              EventOnlyAsset* eoaAssetValue = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:[asset.assetID stringValue]];
//              [self openAssetDetailForDeeplinkEventOnlyAsset:eoaAssetValue];
          }
          else
          {
              //PRONTO-37 Deeplinking to a Page in a PDF
              [ZMUserActions sharedInstance].pageNo = pageNumber;
              if (_openedDeeplinkAsset == NO) {
                _openedDeeplinkAsset = YES;
                [self openAssetDetailForDeeplinkAsset:asset];
              }
              
          }
          // If asset found, No need to keep the reference of deepLinkUserInfo
//          self.actions.deepLinkUserInfo = nil;
      }
      else
      {
        [self showMessage:@"Asset not found" whithType:ZMProntoMessagesTypeWarning];
      }
  }
}

BOOL isContainedIn(NSArray* pageNums, NSString* stringToCheck)
{
    for (NSString* string in pageNums) {
        if ([string caseInsensitiveCompare:stringToCheck] == NSOrderedSame)
            return YES;
    }
    return NO;
}

- (void)openAssetDetailForDeeplinkAsset:(Asset*)asset
{
    [[FastPDFViewController sharedInstance] resetAssetValue];
    [[FastPDFViewController sharedInstance] validateAsset:asset];
}

//- (void)sessionEnd
//{
//    _assetToOpenURL = nil;
////    [self displayLogin];
//
//    //Unregister the token
//    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"Device_Push_Token"];
//
//    if([ZMAssetsLibrary defaultLibrary]){
//
//        if ([[ZMAssetsLibrary defaultLibrary] property:@"TOKEN"]) {
//            token = [[ZMAssetsLibrary defaultLibrary] property:@"TOKEN"];
//        }
//    }
//
//    NSString *cleanToken = [[token description] stringByReplacingOccurrencesOfString:@" " withString:@""];
//    cleanToken = [cleanToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
//    cleanToken = [cleanToken stringByReplacingOccurrencesOfString:@">" withString:@""];
//
//    [_apiManager unRegisterTokenNoSession:cleanToken complete:^{
//        [AbbvieLogging logInfo:@"Token unregisterd"];
//    } error:^(NSError * error) {
//        [AbbvieLogging logError:[NSString stringWithFormat:@"Token error %@",error.description]];
//    }];
//
//    //Since the session ended we need to clearn the database
////    _apiManager = [ZMAbbvieAPI sharedAPIManager];
//    [[ZMAssetsLibrary defaultLibrary] initWithPath:[AOFLoginManager sharedInstance].documentsDir]; //Necesary initialze the DAO object when cleaning it
//    [[ZMAssetsLibrary defaultLibrary] clean];
//
//    //Hide all popovers
////    [[NSNotificationCenter defaultCenter] postNotificationName:@"HIDE_LIBRARY_POPOVERS" object:self userInfo:nil];
//
//    //Check for correct URL Schema and external assets behaviours
////    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"First_Time_Check"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
////    [ZMUserActions sharedInstance].shouldShowFranchiseListView = YES;
////    [ZMUserActions sharedInstance].shoudRefreshEventData = YES;
//}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10
// notification in foreground
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    NSLog(@"selected User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}

# pragma mark - Notifications

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken:%@",deviceToken);
    self.deviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    self.deviceToken = [self.deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"self.deviceToken:%@",self.deviceToken);
    [[NSUserDefaults standardUserDefaults] setObject:self.deviceToken forKey:@"Device_Push_Token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //Persist the token
    [[ZMAssetsLibrary defaultLibrary] setProperty:@"TOKEN" value:self.deviceToken];
    
    /**
     * @Tracking
     * Push notification
     **/
    if (self.deviceToken) {
        [ZMTracking trackSection:@"push" withSubsection:FALSE withName:FALSE withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"scpushnotification":@"yes"}]];
    }
    else {
        [ZMTracking trackSection:@"push" withSubsection:FALSE withName:FALSE withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"scpushnotification":@"no"}]];
    }
    
    if ([AOFLoginManager sharedInstance].checkForAOFLogin) {
        
        if (self.deviceToken && ![self.deviceToken isKindOfClass:[NSNull class]]) {
            
            dispatch_async(dispatch_queue_create("Register Notifications", NULL), ^{
                
                [NotificationsServiceClass.sharedInstance registerToken:self.deviceToken complete:^() {
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@",ProntoRedesignAPIConstants.apnsRegistrationSuccessMsg]];
                } error:^(NSError *error) {
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ : %@",ProntoRedesignAPIConstants.apnsRegistrationError,error]];
                }];
            });
        }
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    [AbbvieLogging logError:[NSString stringWithFormat:@"APNS error: %@", error.description]];
    
    /**
     * @Tracking
     * Push notification
     **/
    [ZMTracking trackSection:@"push" withSubsection:FALSE withName:FALSE withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"scpushnotification":@"no"}]];
    
    if ([ZMAssetsLibrary defaultLibrary]) {
        
        if ([AOFLoginManager sharedInstance].checkForAOFLogin && [[ZMAssetsLibrary defaultLibrary] property:@"TOKEN"]) {
            
            [_apiManager unRegisterToken:[[ZMAssetsLibrary defaultLibrary] property:@"TOKEN"] complete:^() {
                [AbbvieLogging logInfo:@"APNS Token UNRegistered with the CMS"];
            } error:^(NSError *error)
            {
                [AbbvieLogging logError:[NSString stringWithFormat:@"APNS Token UNRegistration error: %@", error]];
            }];
        }
    }
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSDictionary *notification = [userInfo objectForKey:@"aps"];
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject:[notification objectForKey:@"alert"] forKey:@"scpushnotdetails"];
    
    NSInteger mid = [[[notification objectForKey:@"cdata"] objectForKey:@"mid"] integerValue];
    NSDateFormatter *dateFormat =[[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd, yyyy hh:mm a"];
    NSString *launched=[dateFormat stringFromDate:[NSDate date]];
    NSString *atr = [NSString stringWithFormat:@"%ld-%@-%@", (long)mid, [notification objectForKey:@"type"], launched];
    [trackingOptions setObject:atr forKey:@"scpushnotattr"];
    [trackingOptions setObject:[NSString stringWithFormat:@"%ld", (long)mid]   forKey:@"scpushnotid"];
    [trackingOptions setObject:[notification objectForKey:@"alert"] forKey:@"scpushnottitle"];
    [trackingOptions setObject:launched forKey:@"scpushnotlaunchdate"];
    /**
     * @Tracking
     * Messages
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];
    //TODO:Saranya Check below code
//    if ([notification objectForKey:@"badge"] != nil) {
//        [self updateBadge];
//    }
    
    if ([notification objectForKey:@"cdata"]) {
        
        for (NSString *cdata in [notification objectForKey:@"cdata"]) {
            
            NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
            [trackingOptions setObject:[NSString stringWithFormat:@"%ld", (long)mid] forKey:@"scpushnotid"];
            
            if([cdata isEqualToString:@"type"] && [[[notification objectForKey:@"cdata"] objectForKey:cdata] isEqualToString:@"Url"]) {
                /**
                 * @Tracking
                 * Messages
                 **/
                [trackingOptions setObject:@"url" forKey:@"scpushnottype"];
                [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];
                
                [self handlerNotificationAction:[[notification objectForKey:@"cdata"] objectForKey:@"url"] withType:ZMProntoNotificationAlertTypeURL];
                
                break;
            }
            else if ([cdata isEqualToString:@"type"] && [[[notification objectForKey:@"cdata"] objectForKey:cdata] isEqualToString:@"Asset"]) {
                
                
                [trackingOptions setObject:@"asset" forKey:@"scpushnottype"];
                /**
                 * @Tracking
                 * Messages
                 **/
                [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];
                
                [self handlerNotificationAction:[[notification objectForKey:@"cdata"] objectForKey:@"asset"] withType:ZMProntoNotificationAlertTypeAsset];
                
                break;
            }
            else if ([cdata isEqualToString:@"type"] && [[[notification objectForKey:@"cdata"] objectForKey:cdata] isEqualToString:@"App"]) {
                
                /**
                 * @Tracking
                 * Messages
                 **/
                [trackingOptions setObject:@"app" forKey:@"scpushnottype"];
                [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];
                
                [self handlerNotificationAction:[[notification objectForKey:@"cdata"] objectForKey:@"app"] withType:ZMProntoNotificationAlertTypeApp];
                
                break;
            }
        }
    }
}

# pragma mark - ZMProntoMessagesDelegate

- (void) messageRemoved {
    
    _currentMessage = nil;
}

- (NSString *)getPathResource:(NSString*)root {
    
    NSString *file = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Pronto-Config.plist"];
    NSMutableDictionary *config = [NSMutableDictionary dictionaryWithContentsOfFile:file];
    NSString *valueRoot = [config objectForKey:root];
    
    return valueRoot;
}



// Pronto - revamp changes - Overlay while loading assets
- (void) showHideOverlay:(BOOL)hide
{
    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
    
    UIView *grayView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [grayView setBackgroundColor:[UIColor clearColor]];
    grayView.tag = 999;
    
    if ([topController.visibleViewController isKindOfClass:NSClassFromString(@"ProntoTabbarViewController")])
    {
       [UIView animateWithDuration:.3 delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        if(hide){
            UIView *removeView;
            while((removeView = [topController.view viewWithTag:999]) != nil) {
                [removeView removeFromSuperview];
            }
            [topController.view setAlpha:1.0];
        } else {
            
            [topController.view addSubview:grayView];
            [topController.view setAlpha:0.5];
        }
    } completion:^(BOOL finished) {
        
    }];
    }
}

// Pronto - revamp changes - Overlay while loading assets
- (void) showHideOverlayForTabBar:(BOOL)hide
{
    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
    
    UIView *grayView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    CGRect frame = grayView.frame;
    frame.origin.y = frame.size.height - 54;
    frame.size.height = 54;
    grayView.frame = frame;
    [grayView setBackgroundColor:[UIColor clearColor]];
    grayView.tag = 888;
    
    if ([topController.visibleViewController isKindOfClass:NSClassFromString(@"ProntoTabbarViewController")])
    {
        [UIView animateWithDuration:.3 delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
            if(hide){
                UIView *removeView;
                while((removeView = [topController.view viewWithTag:888]) != nil) {
                    [removeView removeFromSuperview];
                }
            } else {
                
                [topController.view addSubview:grayView];
            }
        } completion:^(BOOL finished) {
            
        }];
    }
}

-(void) adjustTabBarOverlay {
    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
    UIView *overallView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIView *overlayView;
    if ([topController.view viewWithTag:888] != nil) {
        overlayView = [topController.view viewWithTag:888];
        CGRect frame = overlayView.frame;
        frame.origin.y = overallView.frame.size.height - 54;
        frame.size.height = 54;
        frame.size.width = overallView.frame.size.width;
        overlayView.frame = frame;
    }
}

-(NSInteger)  currentTabIndex :(NSInteger) tabIndex
{
    currentIndex = tabIndex;
    return currentIndex;
}

- (void)dismissView:(id)sender
{
    //removing overlay on button click
    
    UIView* removeView;
    
    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
    
    while((removeView = [topController.view viewWithTag:5001]) != nil)
    {
        
        for(UIView *v in [self.window.rootViewController.view subviews])
        {
            if([v viewWithTag:5002] || [v viewWithTag:5003] || [v viewWithTag:5004])
                [v removeFromSuperview];
        }
        
        [removeView removeFromSuperview];
    }
}

- (void)didOrientationChange
{
    //Setting up Banner UIView Frame.
    UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
    UIView* bannerView = [topController.view viewWithTag:5001];
    bannerView.frame = CGRectMake(0, 64, [UIApplication sharedApplication].statusBarFrame.size.width, 50);
}

- (void)btnClicked:(id)sender
{
    NSString *searchkey = [ProntoRedesignSingletonClass sharedInstance].lastSearchText;
    [self removeTopviews];
    [HomeViewModel sharedInstance].flowType = @"search";
  if (searchkey.length > 0) {
    [[SearchViewModel sharedInstance]performOnlineSearchWithSearchText:searchkey];
  }
}
-(void)removeTopviews
{
    [_topview removeFromSuperview];
    [_lblView removeFromSuperview];
    [_yesButton removeFromSuperview];
    [_closeButton removeFromSuperview];
}

@end
