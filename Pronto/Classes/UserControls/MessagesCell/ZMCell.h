//
//  ZMCell.h
//  Pronto
//
//  Created by Andres Ramirez on 2/7/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZMCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *close;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionlabel;



@end
