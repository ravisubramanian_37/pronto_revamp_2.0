//
//  ZMCell.m
//  Pronto
//
//  Created by Andres Ramirez on 2/7/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMCell.h"

@implementation ZMCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 0.0, 200.0, 21.0)];
        self.descriptionlabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 20.0, 212.0,  21.0)];
        self.statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(319.0, 35.0, 45.0,  21.0)];
        self.categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 35.0, 51.0,  21.0)];
        // Initialization code
        
        [self.titleLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        [self.descriptionlabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        [self.statusLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        [self.categoryLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        
        
         [self.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
         [self.descriptionlabel setFont:[UIFont boldSystemFontOfSize:12.0]];
         [self.statusLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
         [self.categoryLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.descriptionlabel];
        [self.contentView addSubview:self.statusLabel];
        [self.contentView addSubview:self.categoryLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
