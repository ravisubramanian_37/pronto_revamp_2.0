//
//  ZMBrowseTableViewCell.swift
//  Pronto
//
//  Created by Oscar Robayo on 11/5/15.
//  Copyright © 2015 Abbvie. All rights reserved.
//

import UIKit

@objc class ZMBrowseTableViewCell: UITableViewCell {

    @IBOutlet weak var topBorder: UIView!
    @IBOutlet weak var buttonBorder: UIView!
    @IBOutlet weak var franchiseName: UILabel!
    @IBOutlet weak var checked: UIImageView!
    @IBOutlet weak var checkedContratint: NSLayoutConstraint!
    
    @objc var isChecked = false {
        didSet{
            if isChecked{
                checkedContratint.constant = 30
                checked.isHidden = false
                ZMProntoTheme.shared().styleTransparentBrandPannelFilters(self.contentView)
            }else{
                checkedContratint.constant = 10
                checked.isHidden = true
                ZMProntoTheme.shared().clear(self.contentView)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        self.textLabel?.textColor = UIColor.white
        topBorder.backgroundColor = UIColor.white
        buttonBorder.backgroundColor = UIColor.white
        ZMProntoTheme.shared().styBrightLabel(franchiseName)
        ZMProntoTheme.shared().tintCloseCard(checked)
        ZMProntoTheme.shared().styleDivider(topBorder)
        ZMProntoTheme.shared().styleDivider(buttonBorder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @objc func setFranchiseDetail(_ franchiseTitle:String , totalBrandsToFranchise:Int){
        franchiseName.text = franchiseTitle
        self.isUserInteractionEnabled = totalBrandsToFranchise == 0 ? false : true
        if totalBrandsToFranchise == 0 {
            ZMProntoTheme.shared().brightLabelDisable!(franchiseName)
        }else{
            ZMProntoTheme.shared().styBrightLabel(franchiseName)
        }
    }

    
}
