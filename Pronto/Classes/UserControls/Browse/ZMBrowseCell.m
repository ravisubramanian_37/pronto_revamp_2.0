//
//  ZMBrowseCell.m
//  Pronto
//
//  Created by Sebastian Romero on 2/18/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMBrowseCell.h"
#import "ZMProntoTheme.h"


@interface ZMBrowseCell ()
{
    id <ZMProntoTheme> theme;
    __weak IBOutlet UIImageView *checkImage;
    __weak IBOutlet UILabel *label;
    __weak IBOutlet UIView *line;
}
@end

@implementation ZMBrowseCell


-(ZMBrowseCell *) initWithParent:(UIView *)parent
{
    self = (ZMBrowseCell *)[[[NSBundle mainBundle] loadNibNamed:@"BrowseCell" owner:self options:nil] objectAtIndex:0];
    if (!_parent)
    {
        _parent = parent;
        [self styleBrowseCell];
        checkImage.hidden = YES;
        [_parent addSubview:self];
    }
    return self;
}



-(void) styleBrowseCell
{
    if(!theme)
    {
        theme = [ZMProntoTheme sharedTheme];
    }
    [theme styleDivider:line];
    [theme styBrightLabel:label];
    [theme tintCloseCard:checkImage];
}



-(void) setChecked:(BOOL)checked
{
    _checked = checked;
    CGRect frame;
    frame = label.frame;
    if(checked)
    {
        frame.origin.x = 30;
    } else {
        frame.origin.x = 10;
    }
    [UIView animateWithDuration:0.2 animations:^{
        label.frame = frame;
        checkImage.hidden = !checked;
    } completion:nil];
    
}

-(void) unselectCell
{
    [self setSelectedCell:NO];
}


-(void) setTitle:(NSString *)title
{
    _title = title;
    label.text = title;
}


-(void) setSelectedCell:(BOOL)selectedCell
{
    _selectedCell = selectedCell;
    if(_selectedCell)
    {
        [theme styleTransparentBrandPannelFilters:self];
    } else {
        [theme clear:self];
    }
    
}


-(void) setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    [theme brightLabelDisable:label];
}


@end
