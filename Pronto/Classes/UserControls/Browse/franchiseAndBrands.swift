//
//  franchiseAndBrands.swift
//  Pronto
//
//  Created by Oscar Robayo on 9/11/15.
//  Copyright © 2015 Abbvie. All rights reserved.
//

import Foundation


//Struct to perform selections into Brwose Meny.

enum TypeObject: String {
    case brand = "brand"
    case franchise = "franchise"
}

struct FranchiseAndBrands {
    var uid: CLong
    var name: String
    var type: String

    init(uid:CLong, name:String, type: TypeObject) {
        self.uid = uid
        self.name = name
        self.type = type.rawValue
    }
}
