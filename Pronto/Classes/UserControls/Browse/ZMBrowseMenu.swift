//
//  ZMBrowseMenu.swift
//  Pronto
//
//  Created by Oscar Robayo on 11/5/15.
//  Copyright © 2015 Abbvie. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}



@objc protocol ZMBroseMenuDelegate {
    func closeBrowseMenu()
}

@objc class ZMBrowseMenu: UIViewController {

    @IBOutlet weak var tableViewFranchises: UITableView!
    @IBOutlet weak var tableViewBrands: UITableView!
    @IBOutlet weak var franchiseView: UIView!
    @IBOutlet weak var brandsView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectAllFranchisesLabel: UILabel!
    @IBOutlet weak var selectAllBrandsLabel: UILabel!
    @IBOutlet weak var titleLabelBrandsView: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var selectAllBrands: UIButton!
    @IBOutlet weak var selectAllFranchises: UIButton!
    @IBOutlet weak var brandsViewLeftConstraint: NSLayoutConstraint!
    
    @objc var franchises = [Franchise]()
    @objc var brands = [Brand]()
    var lastFranchiseSelected = [franchiseAndBrands]() /// variable to hold last franchise selected by user
    @objc var menuIsDisplayed = false
    @objc var brandMenuDisplayed = false
    @objc var franchiseName = String() ///Variable to hold franchise name by default
    @objc var touchOnView = UITapGestureRecognizer()
    @objc var modalForDismissingScreen = UIView()
    @objc var delegate:ZMBroseMenuDelegate?
    @objc var library = ZMLibrary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        configureAppereance()
        franchises = getFranchises()
        //Browse menu is currenthy not in use so commenting out the code
//        NotificationCenter.default.addObserver(self, selector: #selector(ZMBrowseMenu.hideMenuNotification(_:)), name: NSNotification.Name.ASFSessionWillEnd, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if ZMProntoManager.sharedInstance.selectedFranchiseBrands.count == 0 && library.resestFranchises{
            setDefaultFranchise()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc class func initMenu() -> ZMBrowseMenu {
        let nibFile = UINib(nibName: "ZMBrowseMenu", bundle: nil)
        let menu:ZMBrowseMenu = nibFile.instantiate(withOwner: self, options: nil).first as! ZMBrowseMenu
        return menu
    }

    @objc func configureAppereance() {
        ZMProntoTheme.shared().styleTransparentPannelFilters(tableViewFranchises.superview)
        ZMProntoTheme.shared().styleTransparentBrandPannelFilters(brandsView)
        ZMProntoTheme.shared().styBrightLabel(titleLabel)
        ZMProntoTheme.shared().styBrightLabel(titleLabelBrandsView)
        ZMProntoTheme.shared().tintButtonReset(resetButton)
        ZMProntoTheme.shared().tintButtonReset(selectAllFranchises)
        ZMProntoTheme.shared().tintButtonReset(selectAllBrands)
        selectAllFranchisesLabel.text = ZMUserActions().allFranchisesChecked ? "Uncheck all" : "Check all"
        updateLayoutView()
        tableViewFranchises.alpha = 0.95
        tableViewBrands.backgroundColor = UIColor.clear
    }
    
    /**
     Update frames to Browse menu when brand option is diplayed
     */
    @objc func updateLayoutView(){
        
        if !menuIsDisplayed {
            // New Search Implementation - Franchise menu height
            var frame = CGRect(x: 0, y: 160, width: franchiseView.frame.width, height: view.frame.height - 160)
            frame.origin.x = franchiseView.frame.size.width * -1
            self.view.frame = frame
            self.view.alpha = 0.95
            self.view.backgroundColor = UIColor.clear
        }
        
        if !brandMenuDisplayed {
            brandsView.frame.origin.x = brandsView.frame.width * -1
            brandsView.alpha = 0
            self.view.frame.size.width = self.franchiseView.frame.size.width 
        }else{
            self.view.frame.size.width = self.franchiseView.frame.size.width + self.brandsView.frame.size.width
        }
        
        if let frameParentView = self.view.superview?.frame {
            modalForDismissingScreen.frame = frameParentView
        }
        
    }
    
    @objc func displayMenu(_ success:@escaping ()->()){
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.frame.origin.x = 0
        }, completion: { (Bool) -> Void in
            self.menuIsDisplayed = true
            self.touchOnView = UITapGestureRecognizer(target: self, action: #selector(ZMBrowseMenu.hideBrowse(_:)))
            self.modalForDismissingScreen = UIView(frame: (self.view.superview?.frame)!)
            self.modalForDismissingScreen.addGestureRecognizer(self.touchOnView)
            self.view.superview?.insertSubview(self.modalForDismissingScreen, belowSubview: self.view)
            success()
        }) 
    }
    
    @objc func hideMenuNotification(_ notification: Notification){
        self.hideMenu()
    }
    
    @objc func hideMenu(){
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            let origin = self.brandMenuDisplayed ? (self.franchiseView.frame.size.width + self.brandsView.frame.size.width) * -1 : self.franchiseView.frame.size.width * -1
            self.view.frame.origin.x = /*self.franchiseView.frame.size.width * -1*/ origin
        }, completion: { (Bool) -> Void in
            self.menuIsDisplayed = false
            self.modalForDismissingScreen.removeGestureRecognizer(self.touchOnView)
            self.modalForDismissingScreen.removeFromSuperview()
            self.view.removeFromSuperview()
            self.delegate?.closeBrowseMenu()
        }) 
    }
    
    @objc func displayBrands(){
        if !brandMenuDisplayed{
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                self.view.frame.size.width = self.franchiseView.frame.size.width + self.brandsView.frame.size.width
                self.brandsView.alpha = 1
                self.brandsView.frame.origin.x += self.franchiseView.frame.width
            }, completion: { (Bool) -> Void in
                self.brandMenuDisplayed = true
                self.brandsView.frame.origin.x = self.franchiseView.frame.width
                self.brandsViewLeftConstraint.constant = 0
            })
        }
    }
    
    @objc func hideBrowse(_ sender: UISwipeGestureRecognizer){
        hideMenu()
    }
    
    @objc func registerCells(){
        tableViewFranchises.register(UINib(nibName: "ZMBrowseTableViewCell", bundle: nil), forCellReuseIdentifier: "ZMBrowseTableViewCell")
        tableViewBrands.register(UINib(nibName: "ZMBrowseTableViewCell", bundle: nil), forCellReuseIdentifier: "ZMBrowseTableViewCell")
    }
    
    @objc func setDefaultFranchise(){
        checkAllFranchisesWithDefault(franchises, nameDefault: franchiseName)
        updateTableView(tableViewFranchises)
    }
    
    @objc func getFranchises() -> [Franchise]{
        return ZMCDManager.sharedInstance.getFranchises() as! [Franchise]
    }
    
    
    
    /**
     Function to delete an object to FranchiseAndBrands array which contain the ids of Franchises or Brands
     
     - parameter franchiseOrBrand: Object of type FranchiseAndBrands
     */
    
    func removeItemToFranchiseAndBrands(_ franchiseOrBrand:franchiseAndBrands){
        for (index,value) in ZMProntoManager.sharedInstance.selectedFranchiseBrands.enumerated(){
            if value.uid == franchiseOrBrand.uid && value.name == franchiseOrBrand.name {
                ZMProntoManager.sharedInstance.selectedFranchiseBrands.remove(at: index)
            }
        }
    }
    
    /**
     This function allow know if a franchise or brand has been selected already, to do that, the function will iterate over selectedFranchiseBrands array to find the id object (Brand or Franchise), also, if a (Brand or Franchise) has been selected before, the function let us deleted it.
     
     - parameter franchiseToAdd: Franchise or Brands to check if exists on selected items
     - parameter removeIfFound:   Bool to indicate if the selected object will be removed.
     
     - returns: New selection of Franchise and Brands Ids, to check status on Franchise Menu
     */
    
    func addObjectToSelectedItems(_ franchiseToAdd:franchiseAndBrands, removeIfFound:Bool)->[franchiseAndBrands]{
        
        var franchisesAdded = [franchiseAndBrands]()
        
        let franchiseSelection = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter{
            $0.uid == franchiseToAdd.uid && $0.name == franchiseToAdd.name && $0.type == franchiseToAdd.type
        }
        
        if franchiseSelection.isEmpty {
            ZMProntoManager.sharedInstance.selectedFranchiseBrands.append(franchiseToAdd)
            franchisesAdded.append(franchiseToAdd)
        }else{
            if removeIfFound{
                removeItemToFranchiseAndBrands(franchiseSelection.first! as franchiseAndBrands)
            }
        }
        return franchisesAdded
    }

    /**
     Add brand to SelectedFranchiseBrand Array, if a brand not exists into selected Franchise and Brands, it will be add.
     
     - parameter brandsToAdd: brand to add of type Brand
     */
    
    @objc func addBrandsToSelectedFranchise(_ brandsToAdd:[Brand]){
        for brand in brandsToAdd  {
            let brands = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter{
                $0.uid == brand.brandID!.intValue && $0.name == brand.name && $0.type == "brand"
            }
            if brands.isEmpty{
                let newBrand = franchiseAndBrands(uid: brand.brandID!.intValue, name: brand.name!, type: typeObject.brand)
                ZMProntoManager.sharedInstance.selectedFranchiseBrands.append(newBrand)
            }
        }
    }

    /**
     Remove selected brand to Franchise and Brands selected
     
     - parameter brandsToDelete: Brand to deleted
     */
    @objc func removeBrandsToSelectedFranchise(_ brandsToDelete:[Brand]){
        for brand in brandsToDelete  {
            if let indexBrand = ZMProntoManager.sharedInstance.selectedFranchiseBrands.firstIndex(
                where: {$0.uid == brand.brandID!.intValue && $0.name == brand.name! && $0.type == typeObject.brand.rawValue
            }) {
                ZMProntoManager.sharedInstance.selectedFranchiseBrands.remove(at: indexBrand)
            }
        }
    }
    
    /**
     This function let know how many brands have been selected to one Franchise
     
     - parameter brandsToCheck: array of brands to check
     
     - returns: count of brands for franchise
     */
    @objc func countBrandsToSelectedFranchise(_ brandsToCheck:[Brand]) -> Int{
        var itemsFound = [franchiseAndBrands]()
        for (_,value) in brandsToCheck.enumerated()  {
            let brands = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter{ $0.uid == value.brandID!.intValue && $0.name == value.name && $0.type == "brand"}
            if !brands.isEmpty{
                itemsFound.append(brands.first!)
            }
        }
        return itemsFound.count
    }
    
    /**
     Get total of franchise selected in Franchise Menu
     
     - parameter totalItemSelected: Ids of Franchise And Brands.
     
     - returns: count selected Franchises.
     */
    func countFranchisesSelected(_ totalItemSelected:[franchiseAndBrands])->Int{
        let totalFranchiseSelected = totalItemSelected.filter({$0.type == typeObject.franchise.rawValue})
        return totalFranchiseSelected.count
    }
    
    /**
     Count active franchises, if a franchise doesn't have a brand, it will mark as not active
     
     - parameter franchises: Franchises to user
     
     - returns: Count active franchises.
     */
    @objc func countActiveFranchises(_ franchises:[AnyObject])->Int{
        
        var franchisesActive = 0
        
        for (_,value) in franchises.enumerated() {
            let franchise = value as! Franchise
            let brandsToFranchise =  franchise.brands
            if !brandsToFranchise!.isEmpty{
                franchisesActive += 1
            }
        }
        return franchisesActive
    }
    
    /**
     This function update the status of one Franchise on Menu Franchise checking each brand that belong to it, each time that a Brand has been selected, the fucntion performs the next validations to decide if the status of the selected Cell will be marked as selected or unselected, or the main buttons into Menu Franchise will be update as "Check All" or "UnCheckall"
     
     1. Get the total Brands to the last Franchise selected.
     2. If the Franchise doesn't have active Brands the "Option button" inside Brand Menu will be mark as "Check All", thus, if a Franchise doesn't have any brand as selected, the status of the Franchise must be changed, to do that, the parent Franchise will be remove to selectedFranchiseBrands array.
     3. If the Franchise has at least one brand, the Franchise and Brand will be marked as selected, and the "Option Button" will markes as "Check all" if the total of brands of the Franchise is equal to the total Brands selected, in other case will be marked as "Uncheck all"
     4. Finally, if the total of Franchises selected is equal to the total active Franchises the option button will be markes as "Uncheck all", in other case it will be marked as "Check all"
     */
    
    @objc func updateFranchiseStatus(){
        
        //1.
        
//        let totalFranchisesToLastBrandSelected = getBrandsToFranchise(lastFranchiseSelected.first!.uid)
//
//        //2.
//
//        if countBrandsToSelectedFranchise(brands) == 0 {
//            updateButtonState(selectAllBrandsLabel, title: "Check all")
//            removeBrandsToSelectedFranchise(brands)
//            if let indexFranchise = ZMProntoManager.sharedInstance.selectedFranchiseBrands.firstIndex(
//                where: {$0.uid == (lastFranchiseSelected.first!).uid && $0.name == (lastFranchiseSelected.first!).name && $0.type == typeObject.franchise.rawValue})
//            {
//                ZMProntoManager.sharedInstance.selectedFranchiseBrands.remove(at: indexFranchise)
//            }
//        //3.
//        }else if countBrandsToSelectedFranchise(brands) > 0 {
//            let franchiseSelection = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter{
//                $0.uid == (lastFranchiseSelected.first!).uid && $0.name == (lastFranchiseSelected.first!).name && $0.type == (lastFranchiseSelected.first!).type
//            }
//            if franchiseSelection.isEmpty {
//                ZMProntoManager.sharedInstance.selectedFranchiseBrands.append(lastFranchiseSelected.first!)
//            }
//
//            if totalFranchisesToLastBrandSelected.count == countBrandsToSelectedFranchise(brands){
//                updateButtonState(selectAllBrandsLabel, title: "Uncheck all")
//            }else{
//                updateButtonState(selectAllBrandsLabel, title: "Check all")
//            }
//        }
//        //4.
//        if countActiveFranchises(franchises) == countFranchisesSelected(ZMProntoManager.sharedInstance.selectedFranchiseBrands){
//            updateButtonState(selectAllFranchisesLabel, title: "Uncheck all")
//            ZMUserActions().allFranchisesChecked = true
//        }else{
//            updateButtonState(selectAllFranchisesLabel, title: "Check all")
//            ZMUserActions().allFranchisesChecked = false
//        }
//
//        updateTableView(tableViewFranchises)
    }
    
    /**
     Change de status of "option button" inside Brands Menu, if the total brands selected is zero the button status will be changed to "Check all", in other case will be changed to "Uncheck all"
     */
    @objc func checkFranchises(){
        
        library.resestFranchises = false
        
        if ZMProntoManager.sharedInstance.selectedFranchiseBrands.count == 0 {
            ZMUserActions().allFranchisesChecked = false
            updateButtonState(selectAllFranchisesLabel, title: "Check all")
        }

        if countActiveFranchises(franchises) == countFranchisesSelected(ZMProntoManager.sharedInstance.selectedFranchiseBrands){
            ZMUserActions().allFranchisesChecked = true
            selectAllFranchisesLabel.text = "Uncheck all"
            updateButtonState(selectAllFranchisesLabel, title: "Uncheck all")
        }else{
            ZMUserActions().allFranchisesChecked = false
            updateButtonState(selectAllFranchisesLabel, title: "Check all")
        }
    }
    
    /**
     This function allow selected all Franchise, and reset Franchises Menu as default, to perfomr the reset the nameDefault must containt data.
     
     - parameter franchisesToCheck: total Franchises to user
     - parameter nameDefault:       name of defualt Franchise.
     */
    
    @objc func checkAllFranchisesWithDefault(_ franchisesToCheck:[AnyObject], nameDefault:String?){
        
        for (_,value) in franchisesToCheck.enumerated(){
            let franchise : Franchise = value as! Franchise
            let franchiseToCheckStatus = franchiseAndBrands(uid: franchise.franchiseID!.intValue, name: franchise.name!, type:typeObject.franchise)
            let brandsToFranchise = franchise.brands
            
            if let _ = nameDefault {
                if franchise.franchiseID!.stringValue == ZMUserActions.sharedInstance().franchiseId {
                    ZMProntoManager.sharedInstance.selectedFranchiseBrands.removeAll()
                    if !brandsToFranchise!.isEmpty{
                        _ = addObjectToSelectedItems(franchiseToCheckStatus, removeIfFound: false)
                        
                        for (_,value) in brandsToFranchise!.enumerated(){
                            let brand : Brand = value
                            let brandToCheckStatus = franchiseAndBrands(uid: brand.brandID!.intValue, name: brand.name!, type:typeObject.brand)
                            _ = addObjectToSelectedItems(brandToCheckStatus, removeIfFound: false)
                        }
                    }
                }
            }else{
                if !brandsToFranchise!.isEmpty{
                    _ = addObjectToSelectedItems(franchiseToCheckStatus, removeIfFound: false)
                    
                    for (_,value) in brandsToFranchise!.enumerated(){
                        let brand : Brand = value
                        let brandToCheckStatus = franchiseAndBrands(uid: brand.brandID!.intValue, name: brand.name!, type:typeObject.brand)
                        _ = addObjectToSelectedItems(brandToCheckStatus, removeIfFound: false)
                    }
                }
            }
        }
    }
    
    /**
     This function return true if the franchise array has a defualt franchise, false in other case.
     
     - parameter franchiseToCheck: Franchise array to user
     - parameter nameDefault:      name of default franchise
     
     - returns: True or False.
     */
    
    @objc func franchiseArrayHasDefaultFranchise(_ franchiseToCheck:[AnyObject], nameDefault:String)->Bool{
        
        let franchiseDefault = franchiseToCheck.filter{ $0.name! == nameDefault }
        if franchiseDefault.isEmpty{
            return false
        }else{
            return true
        }
    }
    
    /**
     Update tables view of Frannchise Menu or Brand Menu, if the tableViewToUpdate parameter is nill, both table views will be updated.
     
     - parameter tableViewToUpdate: table view to update, optional could be nil
     */
    
    @objc func updateTableView(_ tableViewToUpdate:UITableView?){

        if let singleTableView = tableViewToUpdate{
            singleTableView.reloadData()
        }else{
            tableViewBrands.reloadData()
            tableViewFranchises.reloadData()
        }
        
    }
    
    /**
     This function take all franchise and brand selected into the Franchise and Brand Menu and build a new array with the ids, also build a array to update header that containt data on format ["id":xxx,"name":xxx"selected":xxx], then perform refresh library method to get new data.
     */
    
    @objc func updatelibrary(_ withDefault:Bool){
        
        var franchisesAndBrandsToUpdateLibrary = [AnyObject]()
        var franchisesToUpdateHeader = [AnyObject]()
        
       
        let franchisesSelected = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter({$0.type == typeObject.franchise.rawValue})
        
        for franchise in franchisesSelected {
            
            var brandsInsideFranchise = [AnyObject]()
            let brandsToFranchise = getBrandsToFranchise(franchise.uid)
            
            if !brandsToFranchise.isEmpty {
                for brand in brandsToFranchise{
                    let brandSelected = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter(
                      {$0.uid == brand.brandID!.intValue
                        && $0.name == brand.name!
                        && $0.type == typeObject.brand.rawValue})
                    if !brandSelected.isEmpty{
                        franchisesAndBrandsToUpdateLibrary.append(brand.brandID!)
                        let brandNode = ["id":String(describing: brand.brandID!), "name":brand.name!, "selected":"1"]
                        brandsInsideFranchise.append(brandNode as AnyObject)
                    }
                }
                let franchiseNode = ["id":String(franchise.uid), "name":franchise.name, "selected":"1", "brands":brandsInsideFranchise] as [String : Any]
                franchisesToUpdateHeader.append(franchiseNode as AnyObject)
                franchisesAndBrandsToUpdateLibrary.append(franchise.uid as AnyObject)
            }
        }
        
        ZMProntoManager.sharedInstance.franchisesAndBrands = franchisesAndBrandsToUpdateLibrary
        ZMProntoManager.sharedInstance.franchisesToUpdateHeader = franchisesToUpdateHeader
        ZMProntoManager.sharedInstance.selectedCategories = []
        ZMProntoManager.sharedInstance.selectedCategoriesToUpdateHeader = []

        ZMCDManager.sharedInstance.getActiveCategories { (data) -> () in
            ZMProntoManager.sharedInstance.categoriesToFilter = data
        }
        
        self.library.pageIndex = 0
        
        if withDefault {
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.library.backToDefault()
            }
        }else{
            library.refreshLibrary()
        }
    }
    
    @objc func updateButtonState(_ button:UILabel, title:String){
        button.text = title
    }
    
    /**
     Check all franchise inside Franchise Menu.
     
     - parameter sender: Sender.
     */
    @IBAction func checkAllFranchises(_ sender: AnyObject) {
        
        // PRONTO-8 Search Performance
             DispatchQueue.main.async {
        ZMUserActions.sharedInstance().checkUnCheckAllUserTracking(ZMUserActions().allFranchisesChecked)
        self.library.resestFranchises = false

        let isAllFranchise = ZMUserActions().allFranchisesChecked ? false : true
        
        if isAllFranchise {
            
            self.library.headerView.isResetFranchiseSelected = false
            self.updateButtonState(self.selectAllFranchisesLabel, title: "Uncheck all")
            ZMProntoManager.sharedInstance.selectedCategories.removeAll()
            self.checkAllFranchisesWithDefault(self.franchises, nameDefault: nil)
            ZMProntoManager.sharedInstance.resetFranchiseIsSelected = true
            ZMUserActions().allFranchisesChecked = true
            
            if((ZMProntoManager.sharedInstance.search.getTitle().count >= 3) && ((ZMAbbvieAPI.sharedAPIManager() as AnyObject).isConnectionAvailable()))
            {
                // Selecting the franchise should fetch the results

                self.library.headerView.textFieldDidChange(self.library.headerView.searchTextbox)
                self.updatelibrary(true)
                self.updateTableView(self.tableViewFranchises)

            }
            else if(ZMUserActions.sharedInstance().isKeywordSaved == true && (ZMProntoManager.sharedInstance.search.getTitle().count >= 3))
            {
                self.library.headerView.searchOffline(self.library.headerView.searchTextbox)
                self.updatelibrary(true)
                self.updateTableView(self.tableViewFranchises)
            
            }
            else
            {
                
                self.updatelibrary(false)
                self.updateTableView(nil)
                self.library.pageIndex = 0;
            }
        }else{
            
            
            self.library.headerView.isResetFranchiseSelected = false
            self.updateButtonState(self.selectAllFranchisesLabel, title: "Check all")
            ZMProntoManager.sharedInstance.selectedFranchiseBrands.removeAll()
            ZMUserActions().allFranchisesChecked = false
            
            ZMUserActions.sharedInstance().assetsCount = 0;
            self.updatelibrary(false)
            self.updateTableView(nil)
            self.library.headerView.isMakingService = false
            
            self.library.displayEmpty(false)
            
        }
        
            }
        
    }
    
    /**
     Perform reset franchise.
     
     - parameter sender: sender.
       // will commit this later
     */
    @IBAction func resetButton(_ sender: AnyObject) {
        ZMProntoManager.sharedInstance.resetFranchiseIsSelected = false
      
        // reset to reset all - will commit later
        
        ZMProntoManager.sharedInstance.categoriesToFilter = []
        ZMProntoManager.sharedInstance.selectedCategoriesToUpdateHeader = []
        ZMProntoManager.sharedInstance.resetFranchiseAndBrandsCollection()
        updateButtonState(selectAllFranchisesLabel, title: "Check all")
        library.backToDefault()
        
        if franchiseArrayHasDefaultFranchise(franchises, nameDefault: franchiseName){
            checkAllFranchisesWithDefault(franchises, nameDefault: franchiseName)
            updateButtonState(selectAllFranchisesLabel, title: "Check all")
            ZMUserActions().allFranchisesChecked = false
            library.resestFranchises = true
            
        }
        
        print("getTitle:",ZMProntoManager.sharedInstance.search.getTitle());
        // New Search Implementation - selecting franchise
        if((ZMProntoManager.sharedInstance.search.getTitle().count >= 3) && ((ZMAbbvieAPI.sharedAPIManager() as AnyObject).isConnectionAvailable()))
        {
            // Selecting the franchise should fetch the results
            library.headerView.textFieldDidChange(library.headerView.searchTextbox)
            updatelibrary(true)
            updateTableView(tableViewFranchises)
        }
        else{
           updatelibrary(true)
           updateTableView(nil)
        }
        
        
        self.library.headerView.isResetFranchiseSelected = true
        
        let selValue = ""
        ZMUserActions.sharedInstance().selectedSortByMenu = selValue
        self.library.headerView.setDefaultSorts()
        self.library.updateSortButton()
        self.library.headerView.hidingIndicator()
}
    
    /**
     Perform check all brands, if the last selected franchise dosen't has active Brands add each one to Selected items.
     
     - parameter sender: sender
     */
    @IBAction func checkAllBrands(_ sender: AnyObject?) {
        
        let totalBrandsToLastBrandSelected = getBrandsToFranchise(lastFranchiseSelected.first!.uid)
        
        if selectAllBrandsLabel.text == "Check all"{
            updateButtonState(selectAllBrandsLabel, title: "Uncheck all")
            let franchiseSelection = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter{
                $0.uid == (lastFranchiseSelected.first!).uid && $0.name == (lastFranchiseSelected.first!).name && $0.type == (lastFranchiseSelected.first!).type
            }
            if franchiseSelection.isEmpty {
                ZMProntoManager.sharedInstance.selectedFranchiseBrands.append(lastFranchiseSelected.first!)
            }
            for item in totalBrandsToLastBrandSelected{
                let brand = item
                let brandToCheckStatus = franchiseAndBrands(uid: brand.brandID!.intValue, name: brand.name!, type:typeObject.brand)
                _ = addObjectToSelectedItems(brandToCheckStatus, removeIfFound: false)
            }
        }else{
            updateButtonState(selectAllFranchisesLabel, title: "Check all")
            updateButtonState(selectAllBrandsLabel, title: "Check all")
            removeBrandsToSelectedFranchise(totalBrandsToLastBrandSelected)
            if let indexFranchise = ZMProntoManager.sharedInstance.selectedFranchiseBrands.firstIndex(where: {
                $0.uid == (lastFranchiseSelected.first!).uid && $0.name == (lastFranchiseSelected.first!).name && $0.type == typeObject.franchise.rawValue})
            {
                ZMProntoManager.sharedInstance.selectedFranchiseBrands.remove(at: indexFranchise)
            }
        }
        updateFranchiseStatus()
        updateTableView(nil)
        library.actions.isAllCategorySelected = true
        updatelibrary(false)
    }
    
    @objc func getArrayFromSet(_ set:NSSet)-> [Brand] {
        var brands = [Brand]()
        for brand in set {
            brands.append(brand as! Brand)
        }
        return brands
    }
    
    /**
     Tracking event to Omniture, receive paramenter to know if will track franchise or brand.
     
     - parameter isEventFranchise:          Bool to indicate if event is franchise
     - parameter isCellSelected:            Bool to know if cell is selcted
     - parameter franchiseAndBrandsToTrack: Array to franchises and Brands to track.
     */
    
    @objc func trackingEvents(_ isEventFranchise:Bool, isCellSelected:Bool, franchiseAndBrandsToTrack:[AnyObject]){
        
        if isEventFranchise {
            if franchiseAndBrandsToTrack.count > 0 {
                let action : String = "\(ZMUserActions.sharedInstance().assetsCount)"
                var trackingOptions = [String:AnyObject]()
                trackingOptions["filter|franchise:brand"] = "scfiltercategory" as AnyObject?
                trackingOptions["scfiltercategory"] = ZMUserActions.sharedInstance().getFiltersForTracking() as AnyObject?
                //trackingOptions["scfilteraction"] = isCellSelected ? "set" : "reset" as AnyObject?
                let s = "set" as AnyObject
                let r = "reset" as AnyObject
                trackingOptions["scfilteraction"] = isCellSelected ? s : r as AnyObject?
                
                trackingOptions["sctierlevel"] = "tier2" as AnyObject?
                trackingOptions["scfilteraction"] = action as AnyObject?
                let success = "successful" as AnyObject
                let unsuccess = "unsuccessful" as AnyObject
                
                trackingOptions["scsearchaction"] = ZMUserActions.sharedInstance().assetsCount > 0 ? success : unsuccess as AnyObject?
                //trackingOptions["scsearchaction"] = ZMUserActions.sharedInstance().assetsCount > 0 ? "successful" : "unsuccessful" as AnyObject?
                trackingOptions["scsearchterm"] = ZMProntoManager.sharedInstance.search.getTitle() as AnyObject?
                trackingOptions["scsortaction"] = library.getSelectedSort() as AnyObject?
                trackingOptions["scsortcategory"] = library.getCategoriesForTracking() as AnyObject?
                trackingOptions["single"] = "scsorttype" as AnyObject?
                let trackingDictionary = NSMutableDictionary(dictionary: trackingOptions)
                
                /**
                * @Tracking
                * Franchise Selection
                **/
                
                ZMTracking.trackSection(ZMUserActions.sharedInstance().section, withSubsection: nil, withName: franchiseName, withOptions: trackingDictionary)
            }
        }else{
            var trackingOptions = [String:AnyObject]()
            trackingOptions["scsortaction"] = "Brand" as AnyObject?
            trackingOptions["scsorttype"] = "single" as AnyObject?
            trackingOptions["scsortorder"] = "3:1:2:4:5" as AnyObject?
            let trackingDictionary = NSMutableDictionary(dictionary: trackingOptions)
            ZMTracking.trackSection(ZMUserActions.sharedInstance().section, withSubsection: nil, withName: nil, withOptions: trackingDictionary)
        }
    
    }
}

extension ZMBrowseMenu: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewFranchises{
            return franchises.count
        }else{
            return brands.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell : ZMBrowseTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ZMBrowseTableViewCell", for: indexPath) as! ZMBrowseTableViewCell
        cell.topBorder.isHidden = indexPath.row == 0 ? false : true
        
        switch tableView {
        case tableViewBrands:
            let brand : Brand = brands[indexPath.row]
            let brandToCheckStatus = franchiseAndBrands(
              uid: brand.brandID!.intValue,
              name: brand.name!,
              type: typeObject.brand
            )
            let isBrandSelected = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter
            { $0.uid == brandToCheckStatus.uid
              && $0.name == brandToCheckStatus.name
              && $0.type == typeObject.brand.rawValue
            }
            cell.isChecked = isBrandSelected.isEmpty ? false : true
            cell.setFranchiseDetail(brand.name!, totalBrandsToFranchise:1)
            return cell
        default:
            let franchise : Franchise = franchises[indexPath.row]
            let countBrands = franchise.brands?.count
            let franchiseToCheckStatus = franchiseAndBrands(uid: franchise.franchiseID!.intValue, name: franchise.name!, type:typeObject.franchise)
            let isFranchiseSelected = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter
            { $0.uid == franchiseToCheckStatus.uid
              && $0.name == franchiseToCheckStatus.name
              && $0.type == typeObject.franchise.rawValue
            }
            cell.isChecked = isFranchiseSelected.isEmpty ? false : true
            cell.setFranchiseDetail(franchise.name! , totalBrandsToFranchise:countBrands!)
            return cell
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let cell : ZMBrowseTableViewCell = tableView.cellForRow(at: indexPath) as! ZMBrowseTableViewCell
        
        switch tableView {
        case tableViewBrands:
            let brand : Brand = brands[indexPath.row]
            let brandToCheckStatus = franchiseAndBrands(uid: brand.brandID!.intValue, name: brand.name!, type: typeObject.brand)
            let brandAdded = addObjectToSelectedItems(brandToCheckStatus, removeIfFound: true)
            cell.isChecked = brandAdded.isEmpty ? false : true
            updateFranchiseStatus()
            updateTableView(tableViewBrands)
            updatelibrary(false)
            trackingEvents(false, isCellSelected: cell.isChecked, franchiseAndBrandsToTrack: ZMProntoManager.sharedInstance.franchisesAndBrands)
        default:
            let franchise : Franchise = franchises[indexPath.row]
            let franchiseToCheckStatus = franchiseAndBrands(uid: franchise.franchiseID!.intValue, name: franchise.name!, type:typeObject.franchise)
            let franchiseAdded = addObjectToSelectedItems(franchiseToCheckStatus, removeIfFound: true)
            let brandsToFranchise = self.getArrayFromSet(franchise.brands! as NSSet).sorted(by: {$0.name < $1.name})
            brands = brandsToFranchise
            lastFranchiseSelected = [franchiseToCheckStatus]
            
            cell.isChecked = franchiseAdded.isEmpty ? false : true
            if cell.isChecked {
                addBrandsToSelectedFranchise(brandsToFranchise)
                updateButtonState(selectAllBrandsLabel, title: "Uncheck all")
            }else{
                removeBrandsToSelectedFranchise(brandsToFranchise)
                updateButtonState(selectAllBrandsLabel, title: "Check all")
            }
            
            if !brandMenuDisplayed {
                displayBrands()
            }

            if franchise.name == franchiseName {
                ZMProntoManager.sharedInstance.resetFranchiseIsSelected = false
            }else{
                ZMProntoManager.sharedInstance.resetFranchiseIsSelected = true
            }
            
            self.library.headerView.isResetSelected = false
            checkFranchises()
            updateTableView(nil)
            
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
            backgroundQueue.async(execute: {
            // New Search Implementation - selecting franchise
            if(self.library.headerView.searchTextbox.text?.count >= 3)
            {
              // Selecting the franchise should fetch the results
                DispatchQueue.main.async
                {
                   self.library.headerView.editingChanged(self.library.headerView.searchTextbox.text)
                }
               self.updatelibrary(true)
         
            }
            else
            {
               self.updatelibrary(false)
            
            }
            });
            
           self.library.headerView.isResetFranchiseSelected = false
            self.library.actions.isDefault = false
            trackingEvents(true, isCellSelected: cell.isChecked, franchiseAndBrandsToTrack: ZMProntoManager.sharedInstance.franchisesAndBrands)
        }
      
    }

}

