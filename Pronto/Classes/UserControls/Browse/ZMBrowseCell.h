//
//  ZMBrowseCell.h
//  Pronto
//
//  Created by Sebastian Romero on 2/18/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZMBrowseCellDelegate <NSObject>
@optional
/**
 *  Triggered when the a cell is selected
 *  @param uid
 */
-(void) didSelectCell:(long)uid;
@end


@interface ZMBrowseCell : UIControl

-(ZMBrowseCell *) initWithParent:(UIView *)parent;
-(void) unselectCell;

@property (nonatomic) long uid;

/**
 *  Defines the parent or container of notification view
 */
@property (weak, nonatomic) UIView *parent;


@property (weak, nonatomic) ZMBrowseCell *reference;


@property (weak, nonatomic) NSString *title;


@property (nonatomic) BOOL checked;

@property (nonatomic) BOOL selectedCell;


/**
 *  Object who delegates its actions
 */
@property (weak, nonatomic) id<ZMBrowseCellDelegate> delegate;

@end
