//
//  FeedbackView.m
//  Pronto
//
//  Created by cccuser on 08/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import "FeedbackView.h"
#import "ZMLibrary.h"
#import "Pronto-Swift.h"
#import "SettingsServiceClass.h"

@interface FeedbackView()
{
    __strong UIView *modalForDismissingScreen;
    __weak IBOutlet UIButton *submitButton;
    __weak IBOutlet UIButton *cancelButton;
    __weak IBOutlet UILabel *headingLabel;
    __weak IBOutlet UIButton *assetButton;
    __weak IBOutlet UIButton *appButton;
    __weak IBOutlet UILabel *thankyouText;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    __weak IBOutlet UILabel *errorMessage;
    BOOL isInitial;
}
@end

@implementation FeedbackView

// Designated Initializer
- (void) initWithParent:(UIView*)parent background:(UIView*) bgView openedFrom:(NSString*) choice
{
    if(!_parent){
        self.parent = parent;
        self.bgView = bgView;
        isInitial = YES;
        thankyouText.hidden = YES;
        activityIndicator.hidden = YES;
        errorMessage.hidden = YES;
        submitButton.enabled = NO;
        submitButton.alpha = 0.5;
        _openedFrom = choice;
        
        self.textView.layer.borderWidth = 1.0f;
        self.textView.layer.borderColor = [[UIColor blackColor] CGColor];
        assetButton.selected = YES;
        [self adjustFrame:parent];
        
        [_parent addSubview:bgView];
        modalForDismissingScreen = [[UIView alloc]initWithFrame:parent.frame];
        modalForDismissingScreen.hidden = YES;
        [_parent addSubview:modalForDismissingScreen];
        [_parent addSubview:self];
        [self showFeedback];
       
    }
    
}

-(void) adjustFrame:(UIView *)parent {
    CGRect frame = self.frame;
    frame.size.width = 300;
    frame.size.height = 235;
    frame.origin.x = (parent.frame.size.width - frame.size.width)/2;
    frame.origin.y = (parent.frame.size.height - frame.size.height)/2;
    frame.origin.y += 4;
    
    int heightToCheck = 1000;
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        if([UIScreen mainScreen].bounds.size.height == 1024) {
            heightToCheck = 1100;
        }
    }
    else {
        if([UIScreen mainScreen].bounds.size.width == 1024) {
            heightToCheck = 1100;
        }
    }
    
    if(isInitial == NO ) {
        if( (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) && _bgView.frame.size.height > heightToCheck) || (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation) && _bgView.frame.size.height < heightToCheck)){
            CGRect frame1 = _bgView.frame;
            frame1.size.width = _bgView.frame.size.height;
            frame1.size.height = _bgView.frame.size.width;
            _bgView.frame = frame1;
        }
    }else {
        isInitial = NO;
    }
    
    if (_textView.isFirstResponder && UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        frame.origin.y = 80;
    }
    
    if([_openedFrom isEqualToString:@"settings"])
    {
        assetButton.hidden = YES;
        
        CGRect frame = appButton.frame;
        frame.origin.x = assetButton.frame.origin.x;
        appButton.frame = frame;
        
        [appButton setImage:[UIImage imageNamed:@"checkListSelect"] forState:UIControlStateNormal];
        appButton.selected = YES;
    }
    
    self.frame = frame;
}

-(void) showFeedback {
    modalForDismissingScreen.hidden = NO;
}

- (IBAction)cancelAction:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        modalForDismissingScreen.hidden = YES;
        [_bgView removeFromSuperview];
        [self removeFromSuperview];
    }completion:^(BOOL finished) {
        
    }];
}

- (IBAction)feedbackChoiceSelect:(UIButton*)sender {
    if([sender.titleLabel.text  isEqual: @"Asset"]) {
        [assetButton setImage:[UIImage imageNamed:@"checkListSelect"] forState:UIControlStateNormal];
        [appButton setImage:[UIImage imageNamed:@"checkList"] forState:UIControlStateNormal];
        assetButton.selected = YES;
        appButton.selected = NO;
    } else {
        [assetButton setImage:[UIImage imageNamed:@"checkList"] forState:UIControlStateNormal];
        [appButton setImage:[UIImage imageNamed:@"checkListSelect"] forState:UIControlStateNormal];
        appButton.selected = YES;
        assetButton.selected = NO;
    }
}

- (IBAction)submitAction:(UIButton*)sender {
    
    if([sender.titleLabel.text  isEqual: @"Close"]) {
        [UIView animateWithDuration:0.3 animations:^{
            modalForDismissingScreen.hidden = YES;
            [_bgView removeFromSuperview];
            [self removeFromSuperview];
        }completion:^(BOOL finished) {
        }];
    } else {
        if(![_textView.text isEqualToString:@""]) {
            errorMessage.hidden = YES;
            _textView.hidden = YES;
            cancelButton.hidden = YES;
            headingLabel.hidden = YES;
            assetButton.hidden = YES;
            appButton.hidden = YES;
            activityIndicator.hidden = NO;
            [activityIndicator startAnimating];
            [submitButton setTitle:@"Close" forState:UIControlStateNormal];
            self.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];
            
            // send feedback details to CMS
            //Instantiate the singlenton API manager
            ZMAbbvieAPI* apiManager = [ZMAbbvieAPI sharedAPIManager];
            NSString * type = @"app";
            if(assetButton.selected == YES) {
                type = @"asset";
            }
            else
                self.assetId = @"0";
    
            
            [SettingsServiceClass.sharedAPIManager sendFeedback:self.assetId type:type comments:_textView.text success:^(NSString *success){
                _textView.hidden = YES;
                thankyouText.hidden = NO;
                [activityIndicator stopAnimating];
                activityIndicator.hidden = YES;
                _successMessage = success;
                
            }error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.sendFeedbackError, error]];
                [activityIndicator stopAnimating];
                
            }];
            // Offline message
            if(![apiManager isConnectionAvailable] && _successMessage.length == 0)
            {
                _textView.hidden = NO;
                thankyouText.hidden = YES;
                activityIndicator.hidden = YES;
                _textView.text = ProntoRedesignAPIConstants.internetConnectionErrorMsg;
                [_textView setUserInteractionEnabled:NO];
                submitButton.hidden = YES;
                
            }
        }
        else {
            errorMessage.hidden = NO;
        }
    }
}

-(void)textViewDidChange:(UITextView *)textView {
    if(![_textView.text isEqualToString:@""]) {
        submitButton.enabled = YES;
        submitButton.alpha = 1;
    } else {
        submitButton.enabled = NO;
        submitButton.alpha = 0.5;
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        [UIView animateWithDuration:0.5 animations:^{
            CGRect frame = self.frame;
            // start x animation
            //Revamp - Make notification pannel appear to right
            frame.origin.y = 80;
            //End of Revamp
            self.frame =  frame;
        } completion:nil];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        [UIView animateWithDuration:0.5 animations:^{
        [self adjustFrame:self.parent];
        }];
    }
}

@end
