//
//  FeedbackView.h
//  Pronto
//
//  Created by cccuser on 08/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZMLibrary;

@interface FeedbackView : UIView<UITextViewDelegate>

@property (strong, nonatomic) ZMLibrary *library;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) UIView *parent;
@property (weak, nonatomic) UIView *bgView;
@property (strong, nonatomic)  NSString* assetId;
@property (strong, nonatomic)  NSString* successMessage;
@property (strong, nonatomic)  NSString* openedFrom;

- (void) initWithParent:(UIView*)parent background:(UIView*) bgView openedFrom:(NSString*) choice;
- (void) adjustFrame:(UIView *)parent;

@end
