//
//  ZMBadge.h
//  Pronto
//
//  Created by Sebastian Romero on 2/3/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZMHeader;

@interface ZMBadge : UIView
/**
 * Initialize the badge with options
 * @param label NString set the label of the badge
 * @param parent UIView set the parent view of the badge
 **/
-(ZMBadge *) initWithOptions:(NSString *)label parent:(UIView *)parent franchise:(NSString *)franchise;
@property (weak, nonatomic) ZMHeader *header;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) UIView *parent;
@end
