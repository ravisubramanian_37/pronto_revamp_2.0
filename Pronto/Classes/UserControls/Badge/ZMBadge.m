//
//  ZMBadge.m
//  Pronto
//
//  Created by Sebastian Romero on 2/3/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMBadge.h"
#import "ZMHeader.h"
#import "ZMProntoTheme.h"

@interface ZMBadge ()
{
    id <ZMProntoTheme> theme;
}
@end

@implementation ZMBadge


-(ZMBadge *) initWithOptions:(NSString *)label parent:(UIView *)parent franchise:(NSString *)franchise{
    self = [super init];
    if (!_parent) {
        self = (ZMBadge *)[[[NSBundle mainBundle] loadNibNamed:@"Badge" owner:self options:nil] objectAtIndex:0];
//        _parent = parent;
        [self setParent:parent];
        [self prepareSelf:label parent:parent franchise:franchise];
        
    }
    return self;
}
-(void)prepareSelf:(NSString *)label parent:(UIView *)parent franchise:(NSString *)franchise{
    _label.text = label;
    if(_parent.superview)
    {
        [_parent.superview addSubview:self];
    }
    self.layer.cornerRadius = 10;
    self.layer.masksToBounds = YES;
    if(!theme)
    {
        theme = [ZMProntoTheme sharedTheme];
    }
    [theme styleBadge:self franchise:franchise];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapOfView:)];
    [self addGestureRecognizer:tapGestureRecognizer];
}
-(void)handleTapOfView:(UITapGestureRecognizer *)gestureRecognizer {
    [_header displayNotificationMessage:nil];
}

-(void) setParent:(UIView *)parent
{
    _parent = parent;
    CGRect frame = self.frame;
    frame.origin.x = ((parent.frame.origin.x + parent.frame.size.width) - (self.frame.size.width/2))-14;
    frame.origin.y = (parent.frame.origin.y) + 6;
    self.frame = frame;
}
@end
