//
//  ZMWalkthroughItem.h
//  Pronto
//
//  Created by Andrés David Carreño on 2/18/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZMWalkthroughItem : UIView

@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UILabel *baseHiddenLabel;
@property (weak, nonatomic) UIView *mainLabel;

- (id)initWithParent:(UIView*)parent;

@end
