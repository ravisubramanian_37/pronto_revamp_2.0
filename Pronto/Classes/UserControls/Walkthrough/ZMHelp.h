//
//  ZMHelp.h
//  Pronto
//
//  Created by Sebastian Romero on 5/16/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZMHelp : UIView <UIScrollViewDelegate>{
    
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIPageControl *pager;
    __weak IBOutlet UIImageView *_imgFirstPage;
    __weak IBOutlet UIImageView *_imgSecondPage;
    __weak IBOutlet UIImageView *_imgThirdPage;
    
    BOOL _isDocument;
}


// PRONTO-6,PRONTO-7 - Help Screen Bug
@property (nonatomic) unsigned _currentPage;
@property (nonatomic) BOOL _isRotating;
@property (nonatomic) BOOL _isDocument;

- (void)initScrollView:(NSInteger)withOptions;
- (void)updatePageForPortrait;
- (void)updatePageForLandscape;

@end
