//
//  ZMWalkthrough.h
//  Pronto
//
//  Created by Andrés David Carreño on 2/18/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMWalkthroughItem.h"

@interface ZMWalkthrough : UIView <UIScrollViewDelegate>

@property (weak, nonatomic) UIView *parent;
@property (nonatomic) unsigned _currentPage;
@property (nonatomic) BOOL _isRotating;

- (id)initWithParent:(UIView*)parent andData:(NSArray*)data;
- (void)goToPage:(int)pageNumber;
- (void)updateLayout;

@end
