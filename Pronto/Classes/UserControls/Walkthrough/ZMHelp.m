//
//  ZMHelp.m
//  Pronto
//
//  Created by Sebastian Romero on 5/16/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMHelp.h"

@implementation ZMHelp

// PRONTO-6,PRONTO-7 - Help Screen Bug
@synthesize _currentPage, _isRotating,_isDocument;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)initScrollView:(NSInteger)withOptions
{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(orientationChanged:)
                                                name:UIDeviceOrientationDidChangeNotification
                                               object:[UIDevice currentDevice]];
    
    int scroll = self.frame.size.width;
    
    //Set the second page in the right position
    if (scroll == 768) {
        [self updatePageForPortrait];
    }
    
    [pager setHidden:YES];
    
    if (withOptions == 1) {
        
        scroll = scroll * 2; //Twice the screen width size to have 2 pages scrolling
        scrollView.delegate = self;
        [pager setHidden:NO];
    }
    else if (withOptions == 2) {
        _imgFirstPage.hidden = YES;
        _imgThirdPage.hidden = NO;
    }
    
    _isDocument = withOptions;

    scrollView.contentSize = CGSizeMake(scroll, 0);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeView)];
    [self addGestureRecognizer:tap];
}

- (void)orientationChanged:(NSNotification *)note{
    
    UIDevice *device = note.object;
    NSInteger ctPage;
            
    switch (device.orientation) {
                    
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            
            if (_isDocument) {
                scrollView.frame = CGRectMake(0, 0, 768, 1024);
                scrollView.contentSize = CGSizeMake(1536, 0);
                
                
            }
            else{
                scrollView.frame = CGRectMake(0, 0, 768, 1024);
                scrollView.contentSize = CGSizeMake(768, 0);
                 _isDocument = NO;
            }
            
            [self updatePageForPortrait];
            break;
                    
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
           
            
            if (_isDocument) {
                
                scrollView.frame = CGRectMake(0, 0, 1024, 768);
                scrollView.contentSize = CGSizeMake(2048, 0);
                
                // PRONTO-7 - Help Screen Bug
                ctPage = pager.currentPage;
                
                if(ctPage > 0 )
                {
                    
                    if (scrollView.contentOffset.x > 0) {
                        scrollView.frame = CGRectMake(0, 0, 1024, 768);
                        scrollView.contentOffset = CGPointMake(1024, 0);
                    }
                }
                else
                {
                    
                    scrollView.frame = CGRectMake(0, 0, 1024, 768);
                    scrollView.contentOffset = CGPointMake(0, 0);
                }
                
            }
            else{
                scrollView.frame = CGRectMake(0, 0, 1024, 768);
                scrollView.contentSize = CGSizeMake(1024, 0);
                _isDocument = NO;
            }
            
            [self updatePageForLandscape];
            break;
                    
        default:
            break;
    };
}

- (void)closeView
{
    [[NSNotificationCenter defaultCenter] removeObserver:self]; //Remove "self" as an observer to avoid crashes when checking when the orientation changes
    [self removeFromSuperview];
}

// PRONTO-6 - Help Screen Bug

- (void)updateLayout {
    
    //For the width and x values, the self.frame.size.HEIGHT is used because this method is called before rotation happens so that height will be the future width
    //_isRotating = YES; needs to be placed twitce because reasons (iOS bug when the scroll reaches the end and the screen rotates it goes back 2 pages because it calls scrollViewDidScroll twice)
    
    _isRotating = YES;
    scrollView.contentSize = CGSizeMake(self.frame.size.height * [scrollView.subviews count],  self.frame.size.width);
    _isRotating = YES;
    scrollView.contentOffset = CGPointMake(self.frame.size.height * _currentPage, 0);
    
}


- (void)scrollViewDidScroll:(UIScrollView *)sv
{
    // PRONTO-6 - Help Screen Bug
    
    if (_isRotating) {
        
        _isRotating = NO;
        
    }
    else {
        
        _currentPage = round(scrollView.contentOffset.x / self.frame.size.width);
        pager.currentPage = _currentPage;
    }
}

- (void)updatePageForPortrait{
    
    _imgSecondPage.frame = CGRectMake(512, 0, 1024, 768);
}

- (void)updatePageForLandscape{
    
    _imgSecondPage.frame = CGRectMake(1024, 0, 1024, 768);
}

@end
