//
//  ZMWalkthrough.m
//  Pronto
//
//  Created by Andrés David Carreño on 2/18/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMWalkthrough.h"
#import "Pronto-Swift.h"
#import "SMPageControl.h"
#import "UIImage+ImageDirectory.h"
#import "Constant.h"

@interface ZMWalkthrough()

@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet SMPageControl *pageControl;


@end

@implementation ZMWalkthrough
@synthesize _currentPage, _isRotating;

- (id)initWithFrame:(CGRect)frame
{
    return nil;
}

// Designated Initializer
- (id)initWithParent:(UIView*)parent andData:(NSArray*)data
{
    self = (ZMWalkthrough *)[[[NSBundle mainBundle] loadNibNamed:@"ZMWalkthrough" owner:self options:nil] objectAtIndex:0];
    
    if(self && !self.parent){
        
        self.parent = parent;
        self.frame = parent.frame;
        self.scrollView.contentSize = CGSizeMake(parent.frame.size.width * [data count], parent.frame.size.height);
        self.scrollView.delegate = self;
        self.pageControl.userInteractionEnabled = NO;
        self.pageControl.numberOfPages = [data count];
        
        self.pageControl.indicatorMargin = 10.0f;
        self.pageControl.indicatorDiameter = 5.0f;
        [self.pageControl setPageIndicatorImage:[UIImage GetPageControlDotIcon]];
        [self.pageControl setCurrentPageIndicatorImage:[UIImage GetPageControlDotIconSelected]];
        
        for(int i = 0; i < [data count]; i++){
            [self.scrollView addSubview:[self createPageWithData:data[i] inPage:i]];
        }
        
    }
    
    return self;
}


- (ZMWalkthroughItem *)createPageWithData:(NSDictionary*)data inPage:(int)pageNumber{
    
    ZMWalkthroughItem *page = [[ZMWalkthroughItem alloc]initWithParent:self];
    page.frame = CGRectMake(self.frame.size.width * pageNumber, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    page.mainImage.image = [UIImage imageNamed:[data objectForKey:@"image"]];
    
    page.mainLabel = (UIView *)[[[NSBundle mainBundle] loadNibNamed:[data objectForKey:@"nibName"] owner:self options:nil] objectAtIndex:0];
    
    page.mainLabel.frame = page.baseHiddenLabel.frame;
    [page addSubview:page.mainLabel];
    
    page.tag = pageNumber;
    
    return page;
}

- (void)updateLayout {
    
    //For the width and x values, the self.frame.size.HEIGHT is used because this method is called before rotation happens so that height will be the future width
    //_isRotating = YES; needs to be placed twitce because reasons (iOS bug when the scroll reaches the end and the screen rotates it goes back 2 pages because it calls scrollViewDidScroll twice)
    
    _isRotating = YES;
    self.scrollView.contentSize = CGSizeMake(self.frame.size.height * [self.scrollView.subviews count],  self.frame.size.width);
    _isRotating = YES;
    self.scrollView.contentOffset = CGPointMake(self.frame.size.height * _currentPage, 0);
    
    for (ZMWalkthroughItem *page in self.scrollView.subviews) {
        page.frame = CGRectMake(self.frame.size.height * page.tag, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    }
}

- (void)goToPage:(int)pageNumber{
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * pageNumber;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:NO];
}

# pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"> x:%f - width:%f", self.scrollView.contentOffset.x, self.frame.size.width]];
    
    if (_isRotating) {
        
        _isRotating = NO;
    }
    else {
        
        self._currentPage = round(self.scrollView.contentOffset.x / self.frame.size.width);
        self.pageControl.currentPage = self._currentPage;
    }
}

# pragma mark - IBActions
- (IBAction)closeButtonTouch:(UIButton *)sender {

    [ZMWalkthrough transitionWithView:self duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
    [self setHidden:YES];
}

@end
