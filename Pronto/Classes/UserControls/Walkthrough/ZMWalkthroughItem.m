//
//  ZMWalkthroughItem.m
//  Pronto
//
//  Created by Andrés David Carreño on 2/18/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMWalkthroughItem.h"

@interface ZMWalkthroughItem()

@end

@implementation ZMWalkthroughItem

- (id)initWithFrame:(CGRect)frame
{
    return nil;
}

- (id)initWithParent:(UIView*)parent
{
    self = (ZMWalkthroughItem *)[[[NSBundle mainBundle] loadNibNamed:@"ZMWalkthroughItem" owner:self options:nil] objectAtIndex:0];
    
    if (self) {
        self.frame = parent.frame;
    }
    
    return self;
}

@end
