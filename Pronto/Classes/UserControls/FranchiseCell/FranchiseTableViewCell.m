//
//  FranchiseTableViewCell.m
//  Pronto
//
//  Created by cccuser on 28/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import "FranchiseTableViewCell.h"
#import "DALabeledCircularProgressView.h"
#import "Pronto-Swift.h"
#import "FavFranchiseAsset.h"
#import "UIImage+ImageDirectory.h"
#import "UIColor+ColorDirectory.h"
#import "FavFranchise+CoreDataClass.h"
#import "FavFranchise+CoreDataProperties.h"
#import "Constant.h"
#import "ZMUserActions.h"
#import "GlobalSearchVC.h"


@interface FranchiseTableViewCell() {
    DALabeledCircularProgressView *progressOverlayView;
}
@end

@implementation FranchiseTableViewCell

+ (NSString*)GetFranchiseTableViewCellIdentifier
{
    return @"FranchiseTableViewCell";
}

+ (NSString*)GetFranchiseTableViewCellNibName
{
    return @"FranchiseTableViewCell";
}

+ (CGFloat)DefaultHeightOfCell
{
    return 96.0;
}

- (void)createRactangleAroundView
{
    self.layer.cornerRadius = 2.0;
    self.layer.borderColor  = [UIColor GetColorFromHexValue8E8E8E].CGColor;
    self.layer.borderWidth  = 0.5;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.currentAsset = nil;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.currentAsset = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// Pronto new revamp changes: cheching wether new or not
-(void) setIsNew:(BOOL) isItNew {
    if(isItNew) {
        _statusButton.hidden = false;
        _openButton.hidden = false;
        _statusButton.backgroundColor = [UIColor colorWithRed:0.00 green:0.51 blue:0.73 alpha:1.0];
    } else {
        _statusButton.hidden = true;
        _openButton.hidden = true;
    }
}

// Pronto new revamp changes: if the Update is Available
-(void) setIsUpdateAvailable:(BOOL) isAvailable {
    if(isAvailable) {
        _updateAvailableButton.hidden = false;
        _updateButton.hidden = false;
        _updateView.hidden = false;
        _openButton.hidden = true;
        //_updateAvailableButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:12.0];

    } else {
        _updateAvailableButton.hidden = true;
        _updateButton.hidden = true;
        _updateView.hidden = true;
    }
}

-(void) setProgress:(float)progress
{
    _progress = progress;
    
    if(!progressOverlayView){
        progressOverlayView = [[DALabeledCircularProgressView alloc] initWithFrame:_loaderView.bounds];
        progressOverlayView.roundedCorners = YES;
        [_loaderView addSubview:progressOverlayView];
        //        [progressOverlayView displayOperationWillTriggerAnimation];
        [progressOverlayView setProgress:progress/100 animated:YES];
    }
    
    if(_progress >= 1)
    {
        //        [progressOverlayView displayOperationDidFinishAnimation];
        [progressOverlayView setProgress:progress animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [progressOverlayView removeFromSuperview];
            progressOverlayView = nil;
        });
        
    } else {
        progressOverlayView.progress = progress;
        progressOverlayView.progressLabel.text = [NSString stringWithFormat:@"%.0f%%", progressOverlayView.progress*100];
        [progressOverlayView.progressLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12.0f]];
    }
    
    
}


- (void)prepareCellWithAsset:(Asset*)asset atIndexPath:(NSIndexPath*)indexPath
{
    if ([asset isKindOfClass:[Asset class]] == NO) {  return; }
    
    self.currentAsset = asset;
    
    //Hide loader background
    [self.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
    [self.loaderBackgroundView setAlpha:1];
    self.loaderBackgroundView.hidden = YES;
    self.loaderView.hidden = YES;
    
    
        self.labelAssetName.text = asset.title;
        
        //Adjust and increase label height
        self.labelAssetName.frame = [self adjustLabelHeight:self.labelAssetName];
        
        CGRect frame = self.labelAssetType.frame;
        frame.origin.y = self.labelAssetName.frame.origin.y + self.labelAssetName.frame.size.height + 5;
        self.labelAssetType.frame = frame;
        self.labelAssetType.text = [asset.field_description stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        self.labelAssetType.frame = [self adjustLabelHeight:self.labelAssetType];
        
        self.isNew = asset.is_new;
        //self.isUpdateAvailable = asset.isUpdateAvailable;
        self.uid = asset.assetID.longValue;
        [self.shareButton  addTarget:self  action:@selector(exportIconButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.favButton  addTarget:self  action:@selector(favIconButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.infoButton  addTarget:self  action:@selector(infoIconButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        self.shareButton.tag = indexPath.row;
        self.favButton.tag = indexPath.row;
        self.infoButton.tag = indexPath.row;
        // If indexpath = 1 it means it is favorite cell. else othere's cell
        // don't need to call it again n again.
        [self.favButton setImage:[UIImage imageNamed:RedesignConstants.favoUnSelected] forState:UIControlStateSelected]; // rename this to Method name -> favIcon
        [self.favButton setSelected:NO];
    
    NSString *currentBucketName = @"";
    
    if([ZMUserActions sharedInstance].globalSearchViewController)
    {
        currentBucketName = [[ZMUserActions sharedInstance].globalSearchViewController getBuckeNameFromSelectedTab];
    }
    else
    {
        currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
    }

}

- (CGFloat)getHeightOfCell:(Asset*)asset atIndexPath:(NSIndexPath*)indexPath
{
    //Getting default height
    CGFloat totalHeight = [FranchiseTableViewCell DefaultHeightOfCell];
    CGFloat heightToIncrease = 0.0;
    
    //Adjust and increase label height
    CGFloat labelAssetNameHeight = [self adjustLabelHeight:self.labelAssetName withText:asset.title];
    if(labelAssetNameHeight > 21) {
        heightToIncrease = heightToIncrease + labelAssetNameHeight - 21;
    }
    else
    {
        CGRect newFrame = self.labelAssetName.frame;
        newFrame.size.height = 21;
        self.labelAssetName.frame = newFrame;
    }
    
    NSString* typeText = [asset.field_description stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    CGFloat labelAssetTypeHeight = [self adjustLabelHeight:self.labelAssetType withText:typeText];
    if(labelAssetTypeHeight > 21) {
        heightToIncrease = heightToIncrease + labelAssetNameHeight - 21;
    }
    
    totalHeight = totalHeight + heightToIncrease;
    
    return totalHeight;
}


//For adjusting label height according to content
-(CGRect) adjustLabelHeight: (UILabel *)label{
    NSInteger widthToCalculate = 490;
    if([ZMUserActions sharedInstance].globalSearchViewController)
    {
        widthToCalculate = widthToCalculate - 210;
    }
    CGSize constraint = CGSizeMake([[UIScreen mainScreen] bounds].size.width - widthToCalculate, CGFLOAT_MAX);

    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    CGRect newFrame = label.frame;
    newFrame.size.height = size.height;
    return newFrame;
}

-(CGFloat) adjustLabelHeight: (UILabel *)label withText:(NSString*)content {
    NSInteger widthToCalculate = 490;
    if([ZMUserActions sharedInstance].globalSearchViewController)
    {
        widthToCalculate = widthToCalculate - 210;
    }
    CGSize constraint = CGSizeMake([[UIScreen mainScreen] bounds].size.width - widthToCalculate, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [content boundingRectWithSize:constraint
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:label.font}
                                               context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    CGRect newFrame = label.frame;
    newFrame.size.height = size.height;
    return size.height;
}


- (IBAction)exportIconButtonAction:(UIButton*)sender
{
    if (sender.isSelected == NO)
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(cell:configureExportPopOver:)])
        {
            [self.delegate cell:self configureExportPopOver:sender];
        }
    }
    [self makeShareButtonSelected:!sender.isSelected];
}

- (IBAction)favIconButtonAction:(UIButton*)sender
{

    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(cell:configureFavButton:)])
    {
        [self.delegate cell:self configureFavButton:sender];
    }
    
}

- (IBAction)infoIconButtonAction:(UIButton*)sender
{
    // Pronto new revamp changes: setting selected/deselected info button's image
    if (sender.isSelected == NO)
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(cell:configureInfoPopOver:)])
        {
            [self.delegate cell:self configureInfoPopOver:sender];
        }
    }
    [self makeInfoButtonSelected:!sender.isSelected];
}

- (void)makeShareButtonSelected:(BOOL)yesOrNo
{
    if (yesOrNo == YES)
    {
        [self.shareButton setImage:[UIImage GetExportIconSelectedImage] forState:UIControlStateSelected];
        [self.shareButton setSelected:YES];
    }
    else
    {
        [self.shareButton setImage:[UIImage GetExportIconImage] forState: UIControlStateNormal];
        [self.shareButton setSelected:NO];
    }
}

- (void)makeInfoButtonSelected:(BOOL)yesOrNo
{
    if (yesOrNo == YES)
    {
        [self.infoButton setImage:[UIImage GetInfoIconSelectedImage] forState:UIControlStateSelected];
        [self.infoButton setSelected:YES];
    }
    else
    {
        [self.infoButton setImage:[UIImage GetInfoIconImage] forState: UIControlStateNormal];
        [self.infoButton setSelected:NO];
    }
}
@end
