//
//  FranchiseTableViewCell.h
//  Pronto
//
//  Created by cccuser on 28/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Asset.h"
#import "ZMGridItem.h"

@class FranchiseTableViewCell;
@protocol ZMListItemDelegate <NSObject>
@optional
-(void) cellClosedOnEvent;
-(void) deleteComplete:(NSString *)folderName listItem:(FranchiseTableViewCell *)listItem;
-(void) deleteComplete:(NSString *)folderName;
-(void) deleteAssetFromFolder:(Asset *)asset;
-(void) updateFolderName:(NSString *)folderName folderId:(long)folderId;
- (void)cell:(FranchiseTableViewCell*)cell  configureExportPopOver:(UIButton*) sender;
- (void)cell:(FranchiseTableViewCell*)cell  configureInfoPopOver:(UIButton*) sender;
- (void)cell:(FranchiseTableViewCell*)cell  configureFavButton:(UIButton*) sender;

@end

@interface FranchiseTableViewCell : UITableViewCell

@property (weak, nonatomic) id<ZMListItemDelegate> delegate;
@property (nonatomic) enum ZMGridItemType type;
@property (weak, nonatomic) IBOutlet UILabel *labelAssetName;
@property (weak, nonatomic) IBOutlet UILabel *labelAssetType;
@property (strong, nonatomic) IBOutlet UIView *updateView;
@property (strong, nonatomic) IBOutlet UIButton *openButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIButton *favButton;
@property (strong, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (strong, nonatomic) IBOutlet UIButton *updateAvailableButton;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (strong, nonatomic) IBOutlet UIView *loaderBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIView *audioPlayerView;
@property (weak, nonatomic) IBOutlet UIImageView *assetImageType;
@property (weak, nonatomic) IBOutlet UIView *parentFolderView;
@property (weak, nonatomic) IBOutlet UILabel *parentFolderLabel;
@property (weak, nonatomic) IBOutlet UIButton *parentFolderMoreButton;

@property (nonatomic, strong) Asset *currentAsset;
@property (nonatomic) BOOL isUpdateAvailable;
@property (nonatomic) BOOL isNew;
@property (nonatomic) long uid;
@property (nonatomic) float progress;


+ (NSString*)GetFranchiseTableViewCellIdentifier;
+ (NSString*)GetFranchiseTableViewCellNibName;

- (void)prepareCellWithAsset:(Asset*)asset atIndexPath:(NSIndexPath*)indexPath;
- (void)makeShareButtonSelected:(BOOL)yesOrNo;
- (void)makeInfoButtonSelected:(BOOL)yesOrNo;
- (void)createRactangleAroundView;
- (CGFloat)getHeightOfCell:(Asset*)asset atIndexPath:(NSIndexPath*)indexPath;

@end
