//
//  ZMBriefcaseMenu.h
//  Pronto
//
//  Created by Sebastian Romero on 2/26/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Asset.h"

@class ZMAsset;
@class ZMHeader;

/**
 *  Managed Action into Briefcase
 */
@protocol ZMPopOverDelegate <NSObject>
@optional
-(void) didSelectOption;
-(void) folderCreated;
@end

@interface ZMBriefcaseMenu : UIViewController <UITableViewDelegate , UITableViewDataSource, UITextFieldDelegate> {
    UIAlertController *saveFolderAlertView;
    NSMutableDictionary* allFolders;
}


- (void)hideAlert;

@property (weak, nonatomic) Asset *asset;
@property (weak, nonatomic) ZMHeader *parent;
@property (weak, nonatomic) id<ZMPopOverDelegate> delegate;


@end
