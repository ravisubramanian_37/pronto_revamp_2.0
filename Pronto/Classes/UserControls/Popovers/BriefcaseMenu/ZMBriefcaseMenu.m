//
//  ZMBriefcaseMenu.m
//  Pronto
//
//  Created by Sebastian Romero on 2/26/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMBriefcaseMenu.h"
#import "CategoryMenuCell.h"
#import "ZMAssetsLibrary.h"
#import "ZMFolder.h"
#import "ZMAsset.h"
#import "ZMHeader.h"
#import "AppDelegate.h"
#import "ZMUserActions.h"
#import "ZMTracking.h"
#import "Pronto-Swift.h"
#import "ZMAbbvieAPI.h"

@interface ZMBriefcaseMenu ()
{
    __weak IBOutlet UITableView *foldersTable;
    NSMutableArray *folders;
    NSMutableArray *selectedAssets;
    __weak IBOutlet UIButton *addToBriefcase;
    __weak IBOutlet UIButton *createFolder;
    UITextField* alertTextField;
}

@property (retain,nonatomic) NSMutableArray *selectedAssets;

@end

@implementation ZMBriefcaseMenu

@synthesize selectedAssets;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.view.superview.layer.cornerRadius = 0;
    foldersTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    foldersTable.separatorColor = [UIColor clearColor];
    folders = [[NSMutableArray alloc] initWithArray:[[ZMAssetsLibrary defaultLibrary] getAllFolders:FALSE]];
    
    //For ios6
    [foldersTable setOpaque:NO];
    [foldersTable setBackgroundColor:[UIColor clearColor]];

    [foldersTable registerNib:[UINib nibWithNibName:@"CategoryMenuCell" bundle:nil] forCellReuseIdentifier:@"CategoryMenuCell"];

    
    selectedAssets = [[NSMutableArray alloc]init];

    for (int i = 0; i < folders.count; i ++) {
        
        [selectedAssets addObject:@"0"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    
    

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [folders count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CategoryMenuCell *cell = (CategoryMenuCell *)[tableView dequeueReusableCellWithIdentifier:@"CategoryMenuCell"];
    ZMFolder *folder = (ZMFolder *)[folders objectAtIndex:indexPath.row];
    cell.mainLabel.text = folder.fldr_name;
    
    
     // PRONTO-88 - Backlog issue - Asset selection not highlighting the Folder added
        if(![[ZMUserActions sharedInstance].existingFolders isKindOfClass:[NSNull class]])
           allFolders = [[ZMUserActions sharedInstance].existingFolders objectForKey:@"briefcase_data"];
    
    
    if([folders count] > 0)
    {
        NSDictionary* assetsDict = nil;
        
        if(![allFolders isKindOfClass:[NSNull class]])
        {
            
            if(![[allFolders valueForKey:@"assets"] isKindOfClass:[NSNull class]] && [[allFolders valueForKey:@"assets"] count]> indexPath.row && indexPath.row >= 0)
               assetsDict = [[allFolders valueForKey:@"assets"] objectAtIndex:indexPath.row];
        }

        folder.asset_list = assetsDict;
        
        if([assetsDict count]>0)
        {
            for (id key in assetsDict)
            {
                if (assetsDict.count > (int)[indexPath row] && indexPath.row >= 0)
                {
                    if([[assetsDict valueForKey:key] isEqualToString:self.asset.title])
                    {
                        [selectedAssets replaceObjectAtIndex:indexPath.row withObject:@"1"];
                    }
                }
                else if([assetsDict count]>0)
                {
                    if([[assetsDict valueForKey:key] isEqualToString:self.asset.title])
                    {
                        [selectedAssets replaceObjectAtIndex:indexPath.row withObject:@"1"];
                    }
                }
            }
        }
        
    }

    NSString *cellIsSelected = [selectedAssets objectAtIndex:indexPath.row] ;
    cell.backgroundColor = (BOOL)[cellIsSelected intValue] ? [UIColor orangeColor] : [UIColor clearColor];
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZMFolder *folder = (ZMFolder *)[folders objectAtIndex:indexPath.row];
    [selectedAssets replaceObjectAtIndex:indexPath.row withObject:@"1"];
    
    UITableViewCell *cell =(UITableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor orangeColor];
        
     // PRONTO-88 - Backlog issue - Asset selection not highlighting the Folder added
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // PRONTO-55 & PRONTO-63 - Adding an Asset to Briefcase Functionality
        [[ZMAssetsLibrary defaultLibrary] addAssetToFolder:_asset folder:folder loadDetails:TRUE];
    
        
        ZMAbbvieAPI* apiManager = [ZMAbbvieAPI sharedAPIManager];
        
        dispatch_group_t syncGroup = dispatch_group_create();
        
        dispatch_group_enter(syncGroup);
        [apiManager synchronizeFolders:^{
            [AbbvieLogging logInfo:@">>>> Finished sycronizing user data "];
            dispatch_group_leave(syncGroup);
        } error:^(NSError *error) {
            apiManager._syncingInProgress = NO;
            [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with synchronizeFolders: %@", error]];
            apiManager.lastServerCodeResponse = (int)error.code;
            [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
            dispatch_group_leave(syncGroup);
        }];

        
    });
    
    /**
     * @Tracking
     * Add Asset to Folder
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"folder" withName:[NSString stringWithFormat:@"addtofolder-%@",folder.fldr_name] withOptions:
     [[NSMutableDictionary alloc] initWithDictionary:@{@"scbriefcaseaction": @"add", @"sccollateralname" : folder.fldr_name }]];
    
    if(_delegate)
    {
        if([_delegate respondsToSelector:@selector(didSelectOption)])
        {
            [_delegate didSelectOption];
        }
        
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addToMyBriefcase:(UIButton *)sender
{
    [[ZMAssetsLibrary defaultLibrary] addAssetToFolder:_asset folder:0 loadDetails:TRUE];
    
    
    /**
     * @Tracking
     * Add Asset to Folder
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section
              withSubsection:@"assetview"
                    withName:[NSString stringWithFormat:@"addtobriefcase-%@",@""]
                 withOptions: [[NSMutableDictionary alloc] initWithDictionary:@{@"scbriefcaseaction": @"add", @"sctierlevel" :  @"tier2" }]];
   
    if (_parent) {
         [[_parent.briefcasePopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
}

// Revamping changes for deprecated methods
- (IBAction)createFolderAction:(UIButton *)sender {
    
    
    // Alternative AlertView
    saveFolderAlertView = [UIAlertController alertControllerWithTitle:@"Add a new Folder"  message:@"Please enter a folder name."  preferredStyle:UIAlertControllerStyleAlert];
    
    [saveFolderAlertView addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        alertTextField = textField;
    }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle Cancel button
                                   }];
    UIAlertAction* saveButton = [UIAlertAction
                                 actionWithTitle:@"Save"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action) {
                                     //Handle Save button
                                     
                                     [self performSelector:@selector(addFolder) withObject:nil afterDelay:1.0];
                                     
                                 }];
    
    [saveFolderAlertView addAction:cancelButton];
    [saveFolderAlertView addAction:saveButton];
    
    [self presentViewController:saveFolderAlertView animated:YES completion:nil];

}

-(void) addFolder
{
    [self alertViewTag:1 clickedButtonAtIndex:1];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return ( range.location < 15 );
}


# pragma mark - Alert view

- (void)alertViewTag:(int)alertViewTag clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertViewTag)
    {
        case 1:
            if(buttonIndex == 1)
            {
                UITextField *folder = alertTextField;
                NSString *folderName = folder.text;
                BOOL isDuplicated = [[ZMAssetsLibrary defaultLibrary] duplicatedFolderName:folderName];
                if (isDuplicated == NO && ![folderName isEqualToString:@""])
                {
                    ZMFolder *newFolder = [[ZMFolder alloc] init];
                    newFolder.fldr_name = folderName;
                    [[ZMAssetsLibrary defaultLibrary] saveFolder:newFolder withDetails:FALSE];
                    folders = [[NSMutableArray alloc] initWithArray:[[ZMAssetsLibrary defaultLibrary] getAllFolders:FALSE]];
                    [foldersTable reloadData];
                    
                    
                    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [mainDelegate showMessage:@"Your folder has been successfully created." whithType:ZMProntoMessagesTypeSuccess withTime:2];
                    
                    
                    
                    if(_delegate)
                    {
                        if([_delegate respondsToSelector:@selector(folderCreated)])
                        {
                            [_delegate folderCreated];
                            [selectedAssets addObject:@"0"];
                        }
                    }
                    
                    /**
                     * @Tracking
                     * Create Folder
                     **/
                    [ZMTracking trackSection:@"assetview" withSubsection:@"folder" withName:[NSString stringWithFormat:@"addfolder-%@",folderName] withOptions:
                        [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                        @"scbriefcaseaction": @"create",
                                                                        @"sccollateralname" : folderName,
                                                                        @"sctierlevel" : @"tier2" }]];
                    
                    
                    //_parent.folderCreated = YES;
                    
                } else {
                    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    NSString *message = @"Error, duplicated folder name, please change it.";
                    if([folderName isEqualToString:@""])
                    {
                        message = @"Error, invalid folder name, please change it.";
                    }
                    [mainDelegate showMessage:message whithType:ZMProntoMessagesTypeWarning withTime:2];
                }
            }
            break;
    }
}

- (void)hideAlert {
    
    [saveFolderAlertView dismissViewControllerAnimated:YES completion:nil];
}

-(void)handleEnteredBackground:(id)sender{

     [saveFolderAlertView dismissViewControllerAnimated:YES completion:nil];
}

@end
