//
//  ZMTablesDataLayer.h
//  Pronto
//
//  Created by Andres Ramirez on 2/12/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMUserActions.h"

@interface ZMTablesDataLayer : NSObject

@property (strong, nonatomic)NSArray *franchisesArray;

- (NSArray *)franchises;
- (NSArray *)brands:(NSArray *)franchises franchiseId:(NSString *)franchiseId;
- (NSArray *)categories;
- (NSArray *)sort;
- (NSArray *)messages;

-(NSString *)updateArraySelection:(NSString *)id type:(NSString *)type selected:(NSString *)selected;


@end
