//
//  ZMTablesDataLayer.m
//  Pronto
//
//  Created by Andres Ramirez on 2/12/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMTablesDataLayer.h"
#import "Pronto-Swift.h"

@implementation ZMTablesDataLayer

@synthesize franchisesArray = _franchisesArray;

-(NSArray *)serviceArray:(NSString *)serviceName{
    //Read service, in this case read a dummy plist
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:serviceName ofType:@"plist"];
    NSArray *contentArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    return contentArray;
}

-(void)SetFranchises{
    //Read service, in this case read a dummy plist

    //return [self serviceArray:@"dummyData"];
}

-(void) addFranchises:(NSArray *)selection withTotalOfItems:(int)total
{
    self.franchisesArray = selection;
    
}

-(NSArray *)franchises{
    [self SetFranchises];
    return self.franchisesArray;
}

-(NSString *)updateArraySelection:(NSString *)id type:(NSString *)type selected:(NSString *)selected{
    
    for (NSString *array in self.franchisesArray) {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@", array]];
        
    }
    return @"";
}

-(NSArray *)brands:(NSArray *)franchises franchiseId:(NSString *)franchiseId{
    //Read service, in this case read a dummy plist
    NSArray *brandsArray;
    for (id object in franchises) {
        if ([[object valueForKey:@"id"] isEqualToString:franchiseId]) {
            brandsArray = [NSArray arrayWithArray:[object objectForKey:@"brands"]];
        }
    }
    return brandsArray;
}

-(NSArray *)categories{
    //Read service, in this case read a dummy plist
    return [self serviceArray:@"categories"];
}

-(NSArray *)messages{
    //Read service, in this case read a dummy plist
    return [self serviceArray:@"messages"];
}

-(NSArray *)sort{
    //Read service, in this case read a dummy plist
    return [self serviceArray:@"filters"];
    
}

@end
