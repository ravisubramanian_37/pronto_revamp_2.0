//
//  InfoMenu.m
//  Pronto
//
//  Created by Buvana on 17/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import "InfoMenu.h"
#import "ZMTablesDataLayer.h"
#import "ZMAsset.h"
#import "Pronto-Swift.h"


@interface InfoMenu (){
    id <ZMProntoTheme> theme;
}

@property(strong, nonatomic) ZMTablesDataLayer *sort;

@end

@implementation InfoMenu

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedValues = [[NSMutableArray alloc]init];
    theme = [ZMProntoTheme sharedTheme];
    
    //For ios6
    [self.infoMenuTableView setOpaque:NO];
    [self.infoMenuTableView setBackgroundColor:[UIColor clearColor]];
}


- (void)setSelectedValues:(NSMutableArray *)selectedValues{
    
    _selectedValues = selectedValues;
    for (NSDictionary* currentDic in selectedValues) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[currentDic objectForKey:@"id"] intValue] inSection:0];
        [self.infoMenuTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self.infoMenuTableView.delegate tableView:self.infoMenuTableView didSelectRowAtIndexPath:indexPath];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _infoArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    InfoMenuCell *cell = (InfoMenuCell *)[tableView dequeueReusableCellWithIdentifier:RedesignConstants.infoID];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:RedesignConstants.infoID owner:self options:nil];
        
        [tableView registerNib:[UINib nibWithNibName:RedesignConstants.infoID bundle:nil] forCellReuseIdentifier:RedesignConstants.infoID];
        
        cell = (InfoMenuCell*)[nib objectAtIndex:0];
    }
            // Configure the cell...
            cell.textLabel.textColor = [UIColor grayColor];
            cell.textLabel.text = [_infoArray objectAtIndex:indexPath.row];
            //Pronto new revamp changes: set font as per spec
    [cell.textLabel setFont:[UIFont fontWithName:RedesignConstants.infoFontName size:11.0f]];

    switch (indexPath.row) {
        case 0:
            // Display views count
            cell.detailTextLabel.text = _totalViewsCount;
            [cell.detailTextLabel setFont:[UIFont fontWithName:RedesignConstants.infoFontName size:14.0f]];
            break;
        case 1:
        {
            // Display Med/Reg No.
            NSString *string = @"NA";
            NSString *trimmed = [_medRegNo stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"(\n)"]];
            if(trimmed.length > 0)
                string = _medRegNo;
            else
                string = @"NA";


            UITextView *txtView =[[UITextView alloc] init];
            CGSize textSize = [txtView sizeThatFits:CGSizeMake(txtView.frame.size.width, FLT_MAX)];
            CGFloat textHeight = textSize.height;
            txtView.frame = CGRectMake(12, cell.textLabel.frame.size.height +5, tableView.frame.size.width-20, textHeight-8);
            txtView.contentInset = UIEdgeInsetsMake(-7.0,-3.0,0,0.0);
            txtView.font = [UIFont fontWithName:RedesignConstants.infoFontName size:14.0f];
            txtView.textColor=[UIColor blackColor];
            txtView.editable=NO;
            txtView.text = string;
            [cell.contentView addSubview:txtView];

        }
            
            break;
        case 2:
            // Display last updated date.
            cell.detailTextLabel.text = _lastUpdatedDate;
            [cell.detailTextLabel setFont:[UIFont fontWithName:RedesignConstants.infoFontName size:14.0f]];
            break;
        case 3:
             // Display expired date.
            cell.detailTextLabel.text = _expiredDate;
            [cell.detailTextLabel setFont:[UIFont fontWithName:RedesignConstants.infoFontName size:14.0f]];
            break;
        default:
            break;
    }

    
            cell.mainLabel.frame = CGRectMake(15, 5, 177, 40);
            cell.detailTextLabel.frame = CGRectMake(25, 5, 177, 40);
    cell.mainLabel.font = [UIFont fontWithName:RedesignConstants.infoFontName size:11.0f];

            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            cell.accessibilityValue = [_infoArray objectAtIndex:indexPath.row];
    
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
