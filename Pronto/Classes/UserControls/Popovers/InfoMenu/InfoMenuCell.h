//
//  CategoryMenuCell.h
//  Pronto
//
//  Created by Andrés David Carreño on 2/20/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *detailedLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;

@end
