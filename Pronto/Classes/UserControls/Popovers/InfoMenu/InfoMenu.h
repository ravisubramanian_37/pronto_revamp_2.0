//
//  InfoMenu.h
//  Pronto
//
//  Created by Buvana on 17/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoMenuCell.h"
#import "ZMProntoTheme.h"



@class ZMLibrary;

@interface InfoMenu : UIViewController <UITableViewDelegate , UITableViewDataSource>

/**
 *  Class to show export paramenters
 */

@property (weak, nonatomic) IBOutlet UITableView *infoMenuTableView;
@property(strong, nonatomic) NSMutableArray *selectedValues;
@property (nonatomic,strong) NSMutableArray *infoArray;
@property (strong, nonatomic) NSString* totalViewsCount;
@property (strong, nonatomic) NSString* expiredDate;
@property (strong, nonatomic) NSString* lastUpdatedDate;
@property (strong, nonatomic) NSString* medRegNo;

@end

