//
//  ExportMenu.h
//  Pronto
//
//  Created by Buvana on 17/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryMenuCell.h"
#import "ZMProntoTheme.h"
#import <MessageUI/MessageUI.h>


@class ExportMenu;

@protocol ExportMenuDelegate<NSObject>

- (void)exportMenu:(ExportMenu*)exportMenu didDeselectRowAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface ExportMenu : UIViewController <UITableViewDelegate , UITableViewDataSource,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate>

/**
 *  Class to show export paramenters
 */
@property (strong , nonatomic) ZMHeader *parent;
@property (weak, nonatomic) IBOutlet UITableView *exportMenuTableView;
@property(strong, nonatomic) NSMutableArray *selectedValues;
@property (nonatomic,strong) NSMutableArray *exportArray;
@property (nonatomic,strong) NSString* assetID;
@property (nonatomic,strong) NSString* urlPath;
@property (nonatomic,strong) NSString* assetTitle;
@property (nonatomic,strong) Asset* currentSelectedItem;
@property (nonatomic,strong) NSString* assetType;
// check the asset fields
@property (nonatomic) BOOL isPrint;
@property (nonatomic) BOOL isEmailToSelf;
@property (nonatomic) enum exportAction selection;
@property (nonatomic) BOOL isPresentedFromGlobalSearch;

@property (nonatomic, assign) id<ExportMenuDelegate>exportMenuDelegate;

- (void)selectingAssetWithDetails:(Asset *)asset withSender:(id) sender;
-(void) sendEmail:(NSString*)emailId;
- (void)printPDF:(NSData*)pdfData assetTitle: (NSString*) title;

/**
 *  Enumerates the sections types
 */
enum exportAction
{
    Print = 0,
    Share = 1,
    Email_to_self = 2,
    Feedback = 3
};



@end

