//
//  ExportMenu.m
//  Pronto
//
//  Created by Buvana on 17/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import "ExportMenu.h"
#import "ZMTablesDataLayer.h"
#import "ZMAsset.h"
#import "Pronto-Swift.h"
#import <MessageUI/MFMailComposeViewController.h>
// check the simulator/device
#include <sys/types.h>
#include <sys/sysctl.h>
#import "ZMProntoPhase3.h"
#import "Constant.h"
#import "Pronto-Swift.h"
#import "ProntoAssetHandlingClass.h"
#import "RedesignErrorHandling.h"
#import "AssetsServiceClass.h"

@interface ExportMenu (){
    id <ZMProntoTheme> theme;
    NSInteger selectedRow;
}

@property(strong, nonatomic) ZMTablesDataLayer *sort;

@property (nonatomic) BOOL valueToReturn;

@end

@implementation ExportMenu

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedValues = [[NSMutableArray alloc]init];
    theme = [ZMProntoTheme sharedTheme];
    _exportMenuTableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popupBody"]];
    //For ios6
    [self.exportMenuTableView setOpaque:NO];
    [self.exportMenuTableView setBackgroundColor:[UIColor clearColor]];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CGRect frame = _exportMenuTableView.frame;
    frame.size.height = [_exportArray count] * 44;
    _exportMenuTableView.frame = frame;
    selectedRow = _exportArray.count;
    _exportMenuTableView.delegate = self;
    _exportMenuTableView.dataSource = self;
    [_exportMenuTableView reloadData];
}

- (void)setSelectedValues:(NSMutableArray *)selectedValues{
    
    _selectedValues = selectedValues;
    for (NSDictionary* currentDic in selectedValues) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[currentDic objectForKey:@"id"] intValue] inSection:0];
        [self.exportMenuTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self.exportMenuTableView.delegate tableView:self.exportMenuTableView didSelectRowAtIndexPath:indexPath];
    }
    
}

//Check for download needed during print
- (void) downloadCheck:(Asset*) asset completion:(void (^)(BOOL continuePrint))completion
               failure:(void (^)(NSString *messageFailure))failure
{
    if(asset.path.length == 0 && [self.assetType isEqualToString:@"Document"])
    {
        
        NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:asset.assetID];
        if([offlineDownloadArray count] > 0) {
            OfflineDownload * data = [offlineDownloadArray objectAtIndex:0];
            
            if([data.status isEqualToString:@"in_progress"]||[data.status isEqualToString:@"completed_partialy"]) {
                __weak typeof (self) weakSelf = self;
                dispatch_async(dispatch_get_main_queue(), ^{
//                   start download
                    BOOL completionValue = [weakSelf validateForTabs:asset];
                    completion(completionValue);
                });
                
            }
            else {

//                Start Download
                BOOL completionValue = [self validateForTabs:asset];
                completion(completionValue);
            }
        }
    }
    else
    {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"print path:%@",asset.path]];
        
        BOOL completionValue = [self validateForTabs:asset];
        completion(completionValue);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

-(void) selectDeselectCell:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    
    if([[_exportArray objectAtIndex:indexPath.row] isEqualToString:@"Print"])
    {
        _selection = Print;
    }
    else if([[_exportArray objectAtIndex:indexPath.row] isEqualToString:@"Share"])
    {
        _selection = Share;
    }
    else if([[_exportArray objectAtIndex:indexPath.row] isEqualToString:@"Email to self"])
    {
        _selection = Email_to_self;
    }
    else if ([[_exportArray objectAtIndex:indexPath.row] isEqualToString:@"Feedback"])
    {
        _selection = Feedback;
    }
    
    switch (_selection)
    {
        case Print:
        {
            // check the device and display the message
            NSString *hardware = [self hardwareString];
            if ([hardware isEqualToString:@"i386"] || [hardware isEqualToString:@"x86_64"])
            {
                //device is simulator
                NSString* msg = @"Your device cannot print using AirPrint";
                
                UIAlertController* confirm = [UIAlertController alertControllerWithTitle:@""  message:msg  preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               //To make popup dissappear and make cell selection icon to deselect
                                               [self exportMenuHide];
                                           }];
                
                [confirm addAction:okButton];
                [self presentViewController:confirm animated:YES completion:nil];
                return;
            }
            else {
                //Download if asset not present
                [self downloadCheck:_currentSelectedItem completion:^(BOOL continuePrint) {
                    if(continuePrint) {
                        // Print
                        if(self.urlPath.length > 0)
                        {
                            NSData* pdfData = [NSData dataWithContentsOfFile:self.urlPath];
                            [self printPDF:pdfData assetTitle:self.assetTitle];
                        }
                    }
                    else {
                        
                    }
                     [self exportMenuHide];
                } failure:^(NSString *messageFailure) {
                     [self exportMenuHide];
                }];
            }
        }
            break;
        case Share:
        {
            // Share - displaying mail client
            [self sharingThroughMail];
        }
            break;
        case Email_to_self:
        {
            // Share - without displaying mail client
            [self sendMailService];
        }
            break;
        case Feedback:
        {
                [[FeedbackViewModel sharedInstance]loadFeedbackForAssetWithAssetId:self.assetID];
            
            [self exportMenuHide];
            
        }
            break;
            
        default:
            [self exportMenuHide];
            break;
    }
    
    if (self.exportMenuDelegate != nil && [self.exportMenuDelegate respondsToSelector:@selector(exportMenu:didDeselectRowAtIndexPath:)])
    {
        [self.exportMenuDelegate exportMenu:self didDeselectRowAtIndexPath:indexPath];
    }
}

-(void)exportMenuHide {

        [[CategoryViewModel sharedInstance]setExportToNormal:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _exportArray.count;
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"CategoryMenuCell";
    CategoryMenuCell *cell = (CategoryMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryMenuCell" owner:self options:nil];
        
        [tableView registerNib:[UINib nibWithNibName:@"CategoryMenuCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
        
        cell = (CategoryMenuCell*)[nib objectAtIndex:0];
    }
    
    switch([indexPath section]){
        case 0:
        {
            // Configure the cell...
            cell.mainLabel.textColor = [UIColor grayColor];
   
            cell.mainLabel.text = [_exportArray objectAtIndex:indexPath.row];
            
            cell.checkedImage.hidden = NO;
            [cell.checkedImage sizeToFit];
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            if([[_exportArray objectAtIndex:indexPath.row] isEqualToString:@"Print"])
                _selection = Print;
            
            if([[_exportArray objectAtIndex:indexPath.row] isEqualToString:@"Share"])
                _selection = Share;
            
            if([[_exportArray objectAtIndex:indexPath.row] isEqualToString:@"Email to self"])
                _selection = Email_to_self;
            
            if([[_exportArray objectAtIndex:indexPath.row] isEqualToString:@"Feedback"])
                _selection = Feedback;
            
            switch (_selection) {
                case Print:
                    if (selectedRow == indexPath.row) {
                        cell.checkedImage.image = [UIImage imageNamed:@"printIconSelect"];
                        cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
                    } else {
                        cell.checkedImage.image = [UIImage imageNamed:@"printIcon"];
                    }
                    break;
                case Share:
                    if (selectedRow == indexPath.row) {
                        cell.checkedImage.image = [UIImage imageNamed:@"shareIconSelect"];
                        cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
                    } else {
                        cell.checkedImage.image = [UIImage imageNamed:@"sharingIcon"];
                    }
                    break;
                case Email_to_self:
                    if (selectedRow == indexPath.row) {
                        cell.checkedImage.image = [UIImage imageNamed:@"emailIconSelect"];
                        cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
                    } else {
                        cell.checkedImage.image = [UIImage imageNamed:@"mailbox"];
                    }
                    break;
                case Feedback:
                    if (selectedRow == indexPath.row) {
                        cell.checkedImage.image = [UIImage imageNamed:@"feedbackIconSelect"];
                        cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
                    } else {
                        cell.checkedImage.image = [UIImage imageNamed:@"feedbackIcon"];
                    }
                    break;
                default:
                    break;
            }
            cell.accessibilityValue = [_exportArray objectAtIndex:indexPath.row];
            break;
        }
    }
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // To Do
}
    
// Share/emailToSelf
-(void) sharingThroughMail
{
    // Email to self button is pressed - showing mail client
    NSString* emailId = [AOFLoginManager sharedInstance].mail ;
    
//    if ([MFMailComposeViewController canSendMail])
//    {
//        if(emailId == nil || [emailId isEqual: [NSNull null]])
//        {
//            [self sendEmail:@""];
//        }
//        else{
            [self sendEmail:@""];
//            [self sendEmail:@""];
//        }
//    }
//    else
//    {
//        NSString* msg = @"Your device cannot send email";
//
//        UIAlertController* confirm = [UIAlertController alertControllerWithTitle:@""  message:msg  preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* okButton = [UIAlertAction
//                                   actionWithTitle:@"OK"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
//
//                                       //To make popup dissappear and make cell selection icon to deselect
//                                       [self exportMenuHide];
//                                   }];
//
//        [confirm addAction:okButton];
//        [self presentViewController:confirm animated:YES completion:nil];
//
//    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedRow = indexPath.row;
    [tableView reloadData];
    [self selectDeselectCell:indexPath tableView:tableView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// get the selected asset details
- (void)selectingAssetWithDetails:(Asset *)asset withSender:(id) sender
{
    NSString *extension = [asset.uri_full pathExtension];
    BOOL isVideo = ([asset.uri_full rangeOfString:@"brightcove://"].location == 0);
    BOOL isImage = [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"jpg|png|gif|jpeg|bmp"] evaluateWithObject:extension];
    // PRONTO-25 - Web View - Ability to provide a Web link as an asset type.
    BOOL isWeblink = [asset.file_mime isEqualToString:@"weblink"];
    
   self.assetType = nil;
    
    if(isVideo){
        self.assetType = RedesignConstants.videoText;
    }
    else if(isImage){
        self.assetType = @"Image";
    }
    else if(isWeblink)
        self.assetType = @"weblink";
    else
        self.assetType = @"Document";
    
    self.assetID = [NSString stringWithFormat:@"%@",asset.assetID];
  
    self.assetTitle = asset.title;

    // download the document if it has to be printed
    _currentSelectedItem = asset;
    
    self.urlPath = asset.path;
}

// send mail on selecting "Share" by showing mail client
-(void) sendEmail:(NSString*)emailId
{
  NSString* mailSubject = [NSString stringWithFormat:@"%@ is Sharing a Pronto Asset",[AOFLoginManager sharedInstance].givenName];
  // format the mail body content
  //NSString* wcUser = [NSString stringWithFormat:@"<div>Hello %@,</div>\n",emailId];
  NSString* wcUser = [NSString stringWithFormat:@"<div>Hello ,</div>\n"];
  NSString* deeplink = [NSString stringWithFormat:@"abbviepronto://%@",self.assetID];
  NSString* strHtmlBody = [NSString stringWithFormat:
                           @"\nPlease click <b><u><a href=\"%@\">here</a></u></b> to open the asset '%@' in Pronto.You will not be able to open the asset unless you have the Pronto app on your iPad.",
                           deeplink,self.assetTitle];
  NSMutableString *body = [NSMutableString string];
  [body appendString:wcUser];
  [body appendString:@"<div><br/></div>"];
  [body appendString:strHtmlBody];
  [body appendString:@"<div><br/></div>"];
  [body appendString:@"<div>Thanks</div>"];
  
  if ([MFMailComposeViewController canSendMail]) {
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    [mail setSubject:mailSubject];
    //[mail setSubject:@"Pronto - Sharing Deeplink URL"];
    [mail setToRecipients:@[emailId]];
    [mail setMessageBody:body isHTML:YES];
    mail.delegate = self;
    [self presentViewController:mail animated:YES completion:NULL];
  } else {
    [RedesignConstants sendByURLTo:emailId subject:mailSubject body:body isHtml:YES];
  }
}

// --------------------------------
//
// Function: didFinishWithResult
// Description: Used to return the result of the mail sent
//
// —————————————————------------------

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString* msg = nil;
    
    switch (result)
    {
        case MFMailComposeResultSent:
            if([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
                msg = RedesignConstants.mailSentSuccess;
            else
                msg = RedesignConstants.mailConnectionError;
            break;
        case MFMailComposeResultFailed:
            msg = RedesignConstants.mailSentError;
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];

    // show the message at the bottom of the screen
    AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(msg != nil)
    {
        [mainDelegate showMessage:msg whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    }
    [self exportMenuHide];
}


// Print PDF - AirPrint

- (void)printPDF:(NSData*)pdfData assetTitle: (NSString*) title {
    UIPrintInteractionController *printer=[UIPrintInteractionController sharedPrintController];
    UIPrintInfo *info = [UIPrintInfo printInfo];
    info.orientation = UIPrintInfoOrientationPortrait;
    info.outputType = UIPrintInfoOutputGeneral;
    info.jobName= [NSString stringWithFormat:@"%@",title];
    info.duplex=UIPrintInfoDuplexLongEdge;
    printer.printInfo = info;
    printer.printingItem=pdfData;
    
    UIPrintInteractionCompletionHandler completionHandler =
    ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        if (!completed && error)
            [AbbvieLogging logError:[NSString stringWithFormat:@"FAILED! error = %@",[error localizedDescription]]];
    };
    [printer presentAnimated:YES completionHandler:completionHandler];
}

// check whether the app is in simulator or device
- (NSString*)hardwareString {
    size_t size = 100;
    char *hw_machine = malloc(size);
    int name[] = {CTL_HW,HW_MACHINE};
    sysctl(name, 2, hw_machine, &size, NULL, 0);
    NSString *hardware = [NSString stringWithUTF8String:hw_machine];
    free(hw_machine);
    return hardware;
}

// New Pronto Revamp changes - trigger mail from webservice on selecting "Email to self"
-(void) sendMailService
{
     NSString* emailId = [AOFLoginManager sharedInstance].mail ;
    if ([MFMailComposeViewController canSendMail])
    {
        NSString* msg = [NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.mailSentSuccessTxt,emailId];
        
        [[AssetsServiceClass sharedAPIManager] sendEmailToself:self.assetID success:^(NSDictionary *success){
            // show the message at the bottom of the screen
            [[RedesignErrorHandling sharedInstance] showMessage:msg whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
             [self dismissViewControllerAnimated:YES completion:NULL];

        }error:^(NSError *error) {
//            [AbbvieLogging logError:[NSString stringWithFormat:@">>>> Error sending the mail %@", error]];
            
        }];
         [self exportMenuHide];
    }
    else
    {
        NSString* msg = @"Your device cannot send email";
        
        UIAlertController* confirm = [UIAlertController alertControllerWithTitle:@""  message:msg  preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                    
                                       //To make popup dissappear and make cell selection icon to deselect
                                       [self exportMenuHide];
                                   }];
        
        [confirm addAction:okButton];
        [self presentViewController:confirm animated:YES completion:nil];
        
    }
       

}


-(BOOL) validateForTabs: (Asset*) asset
{
    self.valueToReturn = NO;

            [[ProntoAssetHandlingClass sharedInstance] validateAssetBeforeOpen:asset completion:^(BOOL assetNeedsDownload) {
                if (assetNeedsDownload) {
                    [[RedesignErrorHandling sharedInstance] showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
                    [AbbvieLogging logInfo:@"------Asset needs download ------"];
                    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
                    if(![asset.file_mime isEqualToString:@"weblink"])
                        dispatch_async(dispatch_get_main_queue(), ^{

                            // for printing
                            [[ProntoAssetHandlingClass sharedInstance] downloadOneAsset:asset completion:^(BOOL assetDownloaded){
                                if(assetDownloaded) {
                                    [self printPageShow:asset];
                                }
                            }failure:^(NSString *assetDownloadFail) {
                            }];
                        });
                }
                else {
                    self.valueToReturn = YES;
                }
            }failure:^(NSString *messageFailure) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"failure:%@",messageFailure]];
                [[RedesignErrorHandling sharedInstance] showMessage:messageFailure whithType:ZMProntoMessagesTypeWarning];
                self.valueToReturn = NO;
            }];

//    }
    return self.valueToReturn;
}

// for printing
-(void) printPageShow: (Asset*) asset
{
    [[RedesignErrorHandling sharedInstance] hideMessage];
    NSData* pdfData = [NSData dataWithContentsOfFile:asset.path];
    [self printPDF:pdfData assetTitle:asset.title];
}

@end
