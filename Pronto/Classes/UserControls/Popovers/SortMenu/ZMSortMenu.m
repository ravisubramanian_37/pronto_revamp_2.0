//
//  ZMSortMenu.m
//  Pronto
//
//  Created by Andres Ramirez on 2/7/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMSortMenu.h"
#import "ZMTablesDataLayer.h"
#import "ZMAsset.h"
#import "ZMAssetsLibrary.h"
#import "ZMLibrary.h"
#import "ZMHeader.h"
#import "Pronto-Swift.h"
#import "ZMProntoPhase3.h"
#import "Constant.h"


@interface ZMSortMenu (){
    id <ZMProntoTheme> theme;
}

@property(strong, nonatomic) ZMTablesDataLayer *sort;

@end

@implementation ZMSortMenu

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedValues = [[NSMutableArray alloc]init];
    theme = [ZMProntoTheme sharedTheme];
    
    //For ios6
    [self.sortMenuTableView setOpaque:NO];
    [self.sortMenuTableView setBackgroundColor:[UIColor clearColor]];
    
    
    [self.sortMenuTableView registerNib:[UINib nibWithNibName:@"CategoryMenuCell" bundle:nil] forCellReuseIdentifier:@"CategoryMenuCell"];
}


- (void)setSortArray:(NSMutableArray *)sortArray{
    _sortArray = [[NSMutableArray alloc] initWithArray:sortArray];
    [_sortMenuTableView reloadData];
}

- (void)setSelectedValues:(NSMutableArray *)selectedValues{
    
    _selectedValues = selectedValues;
    for (NSDictionary* currentDic in selectedValues) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[currentDic objectForKey:@"id"] intValue] inSection:0];
        [self.sortMenuTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self.sortMenuTableView.delegate tableView:self.sortMenuTableView didSelectRowAtIndexPath:indexPath];
    }
     
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

-(void) selectDeselectCell:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    
    //Iterate over all the fields unmarking them
    
    for (int i = 0; i < _sortArray.count; i++) {
        NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:[_sortArray objectAtIndex:i]];
        [initElement setObject:@"0" forKey:@"selected"];
        [_sortArray replaceObjectAtIndex:i withObject:initElement];
    }
    
    NSMutableDictionary *sort = [[NSMutableDictionary alloc] initWithDictionary:[_sortArray objectAtIndex:indexPath.row]];
    
    if([[sort objectForKey:@"selected"] intValue] == 0)
    {
        [self unmarkCells:tableView];
        
        [sort setObject:@"1" forKey:@"selected"];
        
        CategoryMenuCell *cell = (CategoryMenuCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.mainLabel.frame = CGRectMake(11, 11, 200, 21);
    }
    else {

        for (id obj in _sortArray) {
            
            if ([[obj objectForKey:@"id"] isEqualToString:@"5"]) {
                [obj setValue:@"0" forKey:@"selected"];
            }
        }
        
        [sort setObject:@"0" forKey:@"selected"];
        CategoryMenuCell *cell = (CategoryMenuCell*)[tableView cellForRowAtIndexPath:indexPath];
        //cell.checkedImage.hidden = NO;
        cell.mainLabel.frame = CGRectMake(11, 11, 200, 21);
    }
    
    if(_parent){
        // removing deprecated warnings
            [[_parent.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    
    [_sortArray replaceObjectAtIndex:indexPath.row withObject:sort];
    _library.actions.sorts = [NSMutableArray arrayWithArray:_sortArray];
    _library.pageIndex = 0;
    [ZMProntoManager sharedInstance].sortBy = _sortArray;
    [_library refreshLibrary];
    
    NSDictionary* userInfo = @{ZMSortMenuSelectedCellLabelValue: [sort valueForKey:@"label"]};
    [[NSNotificationCenter defaultCenter] postNotificationName:kZMSortMenuDidSelectRowNotification object:nil userInfo:userInfo];
}

- (void)unmarkCells:(UITableView *)tableView {
    
    int totalCells = 3;
    
    for (int i = 0; i<= totalCells; i++) {
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:i inSection:0];
        CategoryMenuCell *cell = (CategoryMenuCell*)[tableView cellForRowAtIndexPath:myIP];
        //cell.checkedImage.hidden = NO;
        cell.mainLabel.frame = CGRectMake(11, 11, 200, 21);
    }
}


- (bool) isMarquedAToZ
{
    BOOL marked = NO;
    for (id obj in _sortArray){
        if ([[obj objectForKey:@"id"] isEqualToString:@"2"]) {
            if ([[obj objectForKey:@"selected"] intValue] == 1 ){
                marked = YES;
            }
        }
    }
    return marked;
}

- (bool) isMarquedZToA
{
    BOOL marked = NO;
    for (id obj in _sortArray){
        if ([[obj objectForKey:@"id"] isEqualToString:@"3"]) {
            if ([[obj objectForKey:@"selected"] intValue] == 1 ){
                marked = YES;
            }
        }
    }
    return marked;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _sortArray.count;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46;
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CategoryMenuCell";
    CategoryMenuCell *cell = (CategoryMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryMenuCell" owner:self options:nil];

        [tableView registerNib:[UINib nibWithNibName:@"CategoryMenuCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];

        cell = (CategoryMenuCell*)[nib objectAtIndex:0];
    }

    // Configure the cell...
    cell.mainLabel.textColor = [UIColor blackColor];
    cell.mainLabel.text = [[_sortArray objectAtIndex:indexPath.row] objectForKey:@"label"];
    cell.separatorView.hidden = YES;
    
    [cell.checkedImage removeFromSuperview];

    if ([[[_sortArray objectAtIndex:indexPath.row] objectForKey:@"selected"] integerValue] == 1 )
    {
        // Pronto new revamp changes: Extended label width to make visible the whole value
        cell.mainLabel.frame = CGRectMake(11, 11, 200, 21);
        // End
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    } else {
        
        // Pronto new revamp changes: Extended label width to make visible the whole value
        cell.mainLabel.frame = CGRectMake(11, 11, 200, 21);
        // End
    }


    // Pronto new revamp changes: To check selected sorted items
    if ([[_library getSelectedSort] length] > 1) {
        NSString* sortItem = [[_sortArray objectAtIndex:indexPath.row] objectForKey:@"label"];
        if ([sortItem.lowercaseString isEqualToString: [_library getSelectedSort].lowercaseString]) {
            cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
            // change background color of selected cell
            cell.contentView.backgroundColor = [UIColor colorWithRed:233.0/255.0 green: 237.0/255.0 blue: 248.0/255.0 alpha: 1];
        } else {
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
    } else {
        if (indexPath.row == 0 && [[_library getSelectedSort] length] == 0) {
            cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
            // change background color of selected cell
            cell.contentView.backgroundColor = [UIColor colorWithRed:233.0/255.0 green: 237.0/255.0 blue: 248.0/255.0 alpha: 1];
        } else {
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
    }
    //End
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectDeselectCell:indexPath tableView:tableView];
    
    [ZMUserActions sharedInstance].selectedSortByMenu = @"";
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    
    NSString *selectedSort = [[_sortArray objectAtIndex:indexPath.row] objectForKey:@"label"];
 
    [[ZMUserActions sharedInstance] setSelectedSortByMenu:selectedSort];
    
    NSString *sortOrder = @"6:1:2:4";
    
    if ([selectedSort isEqualToString:@"View Count"]) {
        sortOrder = @"4:1:2:3:5";
    }
    
    if ([selectedSort isEqualToString:@"Rating"]) {
        sortOrder = @"5:1:2:3:4";
    }
    
    [trackingOptions setObject:@"filter|franchise:brand" forKey:@"scfiltercategory"];
    [trackingOptions setObject:[[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
    [trackingOptions setObject:@"reset" forKey:@"scfilteraction"];
    [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    [trackingOptions setObject:[NSString stringWithFormat: @"%d", [ZMUserActions sharedInstance].assetsCount] forKey:@"scfilterresultcount"];
    [trackingOptions setObject:([ZMUserActions sharedInstance].assetsCount > 0) ? @"successful":@"unsuccessful" forKey:@"scsearchaction"];
    [trackingOptions setObject:/*_library.actions.search*/[[ZMProntoManager sharedInstance].search getTitle] forKey:@"scsearchterm"];
    [trackingOptions setObject:[_library getSelectedSort] forKey:@"scsortaction"];
    [trackingOptions setObject:[_library getCategoriesForTracking] forKey:@"scsortcategory"];
    [trackingOptions setObject:@"single" forKey:@"scsorttype"];
    [trackingOptions setObject:sortOrder forKey:@"scsortorder"];
    /**
    * @Tracking
    * Sorting
    **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectDeselectCell:indexPath tableView:tableView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0;
    
    // Pronto new revamp changes: To remove corner radius and to set selected sorted items
    self.view.superview.clipsToBounds = NO;
    [_sortMenuTableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
