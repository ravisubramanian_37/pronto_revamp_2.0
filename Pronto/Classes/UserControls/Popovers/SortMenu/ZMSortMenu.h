//
//  ZMSortMenu.h
//  Pronto
//
//  Created by Andres Ramirez on 2/7/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryMenuCell.h"
#import "ZMProntoTheme.h"

@class ZMLibrary;
@interface ZMSortMenu : UIViewController <UITableViewDelegate , UITableViewDataSource>

/**
 *  Class to show sort paramenters
 */

@property (weak, nonatomic) ZMLibrary *library;
@property (weak, nonatomic) ZMHeader *parent;
@property (weak, nonatomic) IBOutlet UITableView *sortMenuTableView;
@property(strong, nonatomic) NSMutableArray *selectedValues;

@property (nonatomic,strong) NSMutableArray *sortArray;

@end
