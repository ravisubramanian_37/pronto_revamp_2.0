//
//  Settings.h
//  Pronto
//
//  Created by Buvana on 17/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryMenuCell.h"
#import "ZMProntoTheme.h"
#import <MessageUI/MessageUI.h>
#import "PreferencesListViewController.h"

@class ZMLibrary;
@class Competitors;
@class ToolsTraining;
@class EventsViewController;
@class Settings;

@protocol settingsDelegate <NSObject>

-(void)settings:(Settings*)settingsView displayFeedbackFor:(NSString*)selectionOption openedFrom:(NSString*)openOption;

@end

@interface Settings : UIViewController <UITableViewDelegate , UITableViewDataSource,UINavigationControllerDelegate>

/**
 *  Class to show export paramenters
 */

@property (strong, nonatomic) ZMLibrary *library;
@property (strong, nonatomic) Competitors *competitors;
@property (strong, nonatomic) ToolsTraining *toolsTraining;
@property (strong, nonatomic) EventsViewController *eventsViewController;

@property (weak, nonatomic) ZMHeader *parent;

//@property (strong , nonatomic) ZMHeader *parent;
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
//@property (nonatomic, strong) NSArray *arrItems;
@property(strong, nonatomic) NSMutableArray *selectedValues;
@property (nonatomic,strong) NSMutableArray *settingsArray;
@property (nonatomic,strong) NSString* assetID;
@property (nonatomic,strong) NSString* urlPath;
@property (nonatomic,strong) NSString* assetTitle;
@property (nonatomic,strong) Asset* currentSelectedItem;
@property (nonatomic,strong) NSString* assetType;
@property (nonatomic) NSInteger heightOfTableView;

@property (nonatomic) BOOL isOpenedFromEventsDetail;

@property (nonatomic, weak) id<settingsDelegate> delegate;
// check the asset fields
@property (nonatomic) BOOL isPrint;
@property (nonatomic) BOOL isEmailToSelf;
@property (nonatomic) enum settingsAction selection;
@property (nonatomic, strong) PreferencesListViewController *preferencesListViewController;







@end

