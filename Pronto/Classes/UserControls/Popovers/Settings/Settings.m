//
//  Settings.m
//  Pronto
//
//  Created by Buvana on 17/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import "Settings.h"
#import "ZMTablesDataLayer.h"
#import "ZMAsset.h"
#import "Pronto-Swift.h"
#import <MagicalRecord/MagicalRecord.h>
#import "AppDelegate.h"
#import "ZMProntoPhase3.h"
#import "Constant.h"
#import "HelpViewController.h"
#import "EventsViewController.h"
#import "UIColor+ColorDirectory.h"
#import "UIImage+ImageDirectory.h"

/**
 *  Enumerates the sections types
 */
enum settingsAction
{
    UserProfile = 0,
    Preferences = 1,
    AppFeedback = 2,
    Logout = 3
    
};

@interface Settings (){
    id <ZMProntoTheme> theme;
    NSInteger selectedRow;
}

@property(strong, nonatomic) ZMTablesDataLayer *sort;


@end

@implementation Settings

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedValues = [[NSMutableArray alloc]init];
    theme = [ZMProntoTheme sharedTheme];
    _settingsTableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"popupBody"]];
    //For ios6
    [self.settingsTableView setOpaque:NO];
    [self.settingsTableView setBackgroundColor:[UIColor clearColor]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0;
    [self setTableHeight];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    selectedRow = _settingsArray.count;
    _settingsTableView.delegate = self;
    _settingsTableView.dataSource = self;
    [_settingsTableView reloadData];
}

- (void)setSelectedValues:(NSMutableArray *)selectedValues{
    
    _selectedValues = selectedValues;
    for (NSDictionary* currentDic in selectedValues) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[currentDic objectForKey:@"id"] intValue] inSection:0];
        [self.settingsTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self.settingsTableView.delegate tableView:self.settingsTableView didSelectRowAtIndexPath:indexPath];
    }
    
}

- (NSString*)getCategoryMenuCellIdentifier
{
    return @"CategoryMenuCell";
}

//Set Table height according to cell height
- (void) setTableHeight
{
    CategoryMenuCell *categoryCell = nil;
    if (categoryCell == nil)
    {
        categoryCell = (CategoryMenuCell*)[_settingsTableView dequeueReusableCellWithIdentifier:[self getCategoryMenuCellIdentifier]];
        if (categoryCell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:[self getCategoryMenuCellIdentifier] owner:self options:nil];
            [_settingsTableView registerNib:[UINib nibWithNibName:[self getCategoryMenuCellIdentifier] bundle:nil] forCellReuseIdentifier:[self getCategoryMenuCellIdentifier]];
            categoryCell = (CategoryMenuCell*)[nib objectAtIndex:0];
        }
        categoryCell.mainLabel.text = [[_settingsArray objectAtIndex:0] objectForKey:@"main"];
        categoryCell.subLabel.text = [[_settingsArray objectAtIndex:0] objectForKey:@"sub"];
        CGRect heightMain = [self adjustLabelHeight: categoryCell.mainLabel];
        if(heightMain.size.height <= 18) {
            heightMain.size.height = 0;
        }
        CGRect height = [self adjustLabelHeight: categoryCell.subLabel];
        if(height.size.height > 11)
        {
            CGRect frame = _settingsTableView.frame;
            frame.size.height = ([_settingsArray count] * 44) + heightMain.size.height + height.size.height - 10;
            _settingsTableView.frame = frame;
            self.preferredContentSize = CGSizeMake(185, frame.size.height);
            self.heightOfTableView = frame.size.height;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(self.settingsTableView.frame.size.height == 0)
    {
        CGRect frame = self.settingsTableView.frame;
        frame.size.height = self.heightOfTableView;
        self.settingsTableView.frame = frame;
    }
    return _settingsArray.count;
   
}

//For Dynamic Height of settings
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *simpleTableIdentifier = @"CategoryMenuCell";
    CategoryMenuCell *cell = (CategoryMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    CGRect height;
    CGRect heightMain = CGRectZero;
    switch (_selection) {
        case UserProfile:
            if(indexPath.row == 0) {
                cell.mainLabel.text = [[_settingsArray objectAtIndex:indexPath.row] objectForKey:@"main"];
                cell.subLabel.text = [[_settingsArray objectAtIndex:indexPath.row] objectForKey:@"sub"];
            }
            //Display other options
            else {
                cell.mainLabel.text = [_settingsArray objectAtIndex:indexPath.row];
            }
            
            if(cell.mainLabel != nil && ![cell.mainLabel.text isEqualToString:@""] && cell.mainLabel.text != nil &&
               [[_settingsArray objectAtIndex:0] isKindOfClass:[NSDictionary class]])
            {
                cell.mainLabel.text = [[_settingsArray objectAtIndex:0] objectForKey:@"main"];
                heightMain = [self adjustLabelHeight: cell.mainLabel];
                if(heightMain.size.height <= 18) {
                    heightMain.size.height = 0;
                }
            }
            
            if(cell.subLabel != nil && ![cell.subLabel.text isEqualToString:@""] && cell.subLabel.text != nil &&
               [[_settingsArray objectAtIndex:0] isKindOfClass:[NSDictionary class]])
            {
                cell.subLabel.text = [[_settingsArray objectAtIndex:0] objectForKey:@"sub"];
                height = [self adjustLabelHeight: cell.subLabel];
                if(height.size.height > 11)
                {
                    return 46 + height.size.height + heightMain.size.height - 10;
                }
                else
                {
                    return 46;
                }
            }
            else
            {
                return 44;
            }
            break;
        default:
            //Logout button hide fix
            _settingsTableView.frame = self.view.frame;
            return 44;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"CategoryMenuCell";
    CategoryMenuCell *cell = (CategoryMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryMenuCell" owner:self options:nil];
        [tableView registerNib:[UINib nibWithNibName:@"CategoryMenuCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
        cell = (CategoryMenuCell*)[nib objectAtIndex:0];
    }
    //Nil exception handling
    if(_settingsArray.count > 0){
    switch([indexPath section]){
        case 0:
        {
            // Configure the cell...
            if(indexPath.row == 0) {
                cell.mainLabel.text = [[_settingsArray objectAtIndex:indexPath.row] objectForKey:@"main"];
                cell.subLabel.text = [[_settingsArray objectAtIndex:indexPath.row] objectForKey:@"sub"];
            }
            else {
                cell.mainLabel.text = [_settingsArray objectAtIndex:indexPath.row];
            }
            
            cell.checkedImage.hidden = NO;
            cell.checkedImage.frame = CGRectMake(0, 0 , 40, 40);
            cell.mainLabel.frame = CGRectMake(40, 0, 177, 40);
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            NSString * stringToCheck = @"";
             if(indexPath.row == 0) {
                 stringToCheck = [[_settingsArray objectAtIndex:indexPath.row] objectForKey:@"main"];
             }
             else {
                 stringToCheck = [_settingsArray objectAtIndex:indexPath.row];
             }
            
            if(![stringToCheck isEqualToString:@"Change Preferences"] && ![stringToCheck isEqualToString:@"Logout"])
                _selection = UserProfile;
            if([stringToCheck isEqualToString:@"Change Preferences"])
                _selection = Preferences;
            if([stringToCheck isEqualToString:@"Feedback"])
                _selection = AppFeedback;
            if([stringToCheck isEqualToString:@"Logout"])
                _selection = Logout;
            
            cell.mainLabel.textColor = [UIColor colorWithRed:0.00 green:0.51 blue:0.73 alpha:1.0];

            CGRect height1 = [self adjustLabelHeight: cell.mainLabel];
            switch (_selection) {
                case UserProfile:
                    //Adjust height and value
                    
                    cell.mainLabel.frame = CGRectMake(10, 5, 200, height1.size.height);
                    cell.mainLabel.textColor = [UIColor grayColor];
                    cell.mainLabel.font = [UIFont fontWithName:@"Lato-Regular" size:16];
                    
                    CGRect height = [self adjustLabelHeight: cell.subLabel];
                    height.origin.x = 10;
                    height.origin.y = cell.mainLabel.frame.origin.y + height1.size.height + (height1.size.height/2);
                    cell.subLabel.frame = height;
                    cell.subLabel.textColor = [UIColor grayColor];
                    cell.subLabel.font = [UIFont fontWithName:@"Lato-Regular" size:12];
                    
                    [cell.checkedImage removeFromSuperview];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.userInteractionEnabled = NO;
                    
                    break;
                case Preferences:
                    if(self.isOpenedFromEventsDetail == YES)
                    {
                        cell.contentView.alpha = 0.3;
                        cell.checkedImage.image = [UIImage GetPreferencesIcon];
                        cell.userInteractionEnabled = NO;
                    }
                    else
                    {
                        if (selectedRow == indexPath.row) {
                            cell.checkedImage.image = [UIImage GetPreferencesIcon];
                            cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
                        } else {
                            cell.checkedImage.image = [UIImage GetPreferencesIcon];
                        }
                    }
                    break;
                case AppFeedback:
                    if (selectedRow == indexPath.row) {
                        cell.checkedImage.image = [UIImage imageNamed:@"feedbackIconSelect"];
                        cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
                    } else {
                        cell.checkedImage.image = [UIImage imageNamed:@"feedbackIcon"];
                    }
                    break;
                case Logout:
                    if (selectedRow == indexPath.row) {
                        cell.checkedImage.frame = CGRectMake(0, 0 , 30, 30);
                        cell.checkedImage.image = [UIImage imageNamed:@"logoutIcon"];
                        cell.mainLabel.textColor = [ZMProntoPhase3 popOverSelectedItemBlueColor];
                    } else {
                        cell.checkedImage.image = [UIImage imageNamed:@"logoutIcon"];
                    }
                    break;

                default:
                    break;
            }
            if(indexPath.row == 0) {
                cell.accessibilityValue = [[_settingsArray objectAtIndex:indexPath.row] objectForKey:@"main"];
            }
            else {
                cell.accessibilityValue = [_settingsArray objectAtIndex:indexPath.row];
            }
            break;
        }
    }
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 // to do
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedRow = indexPath.row;
    [tableView reloadData];
        [self selectDeselectCell:indexPath tableView:tableView];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

-(void) selectDeselectCell:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    _parent.settingsMenuIsPresented = NO;
    NSString * stringToCheck = @"";
    if(indexPath.row == 0) {
        stringToCheck = [[_settingsArray objectAtIndex:indexPath.row] objectForKey:@"main"];
    }
    else {
        stringToCheck = [_settingsArray objectAtIndex:indexPath.row];
    }
    if(![stringToCheck isEqualToString:@"Change Preferences"] && !([stringToCheck isEqualToString:@"Logout"]))
        _selection = UserProfile;
    
    if([stringToCheck isEqualToString:@"Change Preferences"])
        _selection = Preferences;
    if([stringToCheck isEqualToString:@"Feedback"])
        _selection = AppFeedback;
    if([stringToCheck isEqualToString:@"Logout"])
        _selection = Logout;

    
    switch (_selection) {
        case UserProfile:
        {
            _parent.settingsMenuIsPresented = YES;
            // UserProfile
        }
            break;
        case Preferences:
        {
            [self dismissViewControllerAnimated:YES completion:NULL];
            [self loadPrefencesListViewController];
        }
            break;
        case AppFeedback:
        {
            [self dismissViewControllerAnimated:YES completion:NULL];
            // initialize if it is not already initialized
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(settings:displayFeedbackFor:openedFrom:)])
            {
                [self.delegate settings:self displayFeedbackFor:@"0" openedFrom:@"settings"];
                break;
            }
            
            if([ZMUserActions sharedInstance].currentSelectedTabIndex == 0) {
                if(_library == nil)
                {
                    _library = [[ZMLibrary alloc]init];
                }
                [_library initiateFeedback:@"0" openedFrom:@"settings"];
            }
            else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 1) {
                if(_competitors == nil)
                {
                    _competitors = [[Competitors alloc]init];
                }
                [_competitors initiateFeedback:@"0" openedFrom:@"settings"];
            }
            else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 2) {
                if(_toolsTraining == nil)
                {
                    _toolsTraining = [[ToolsTraining alloc]init];
                }
                [_toolsTraining initiateFeedback:@"0" openedFrom:@"settings"];
            }
            else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 3) {
                if(self.eventsViewController == nil)
                {
                    self.eventsViewController = [[EventsViewController alloc]init];
                }
            }
        }
            break;
        case Logout:
        {
            [self dismissViewControllerAnimated:YES completion:NULL];

            AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSArray *vcs = ((UINavigationController*)(mainDelegate.window.rootViewController)).viewControllers;
            ProntoTabbarViewController *tabVC = vcs.firstObject;
            if ([tabVC isKindOfClass:[ProntoTabbarViewController class]])
            {
                if (vcs != nil && vcs.count > 1 &&
                    [vcs.lastObject isKindOfClass:[UIViewController class]])
                {
                    //If user has logged out from asset detail page or Help screen.
                    // First poping the asset detail page or Help screen and coming back to
                    //Root view controller.
                    if([vcs.lastObject isKindOfClass:[HelpViewController class]])
                    {
                        [ZMUserActions sharedInstance].isLoggedoutFromHelp = YES;
                        HelpViewController *helpVC = (HelpViewController*)vcs.lastObject;
                        [helpVC pauseVideoPlayerResetTheValue];
                    }
                    [((UINavigationController*)(mainDelegate.window.rootViewController)) popViewControllerAnimated:NO];
                }
                [self removeGlobalSearchViewControllerFromTabBarHierarchy:tabVC];
                if (tabVC.library == tabVC.currentController)
                {
                    //Resetting the datasource to show empty screen.
                    [self.library.assetsPage addObjectsFromArray:@[]];
                    [ZMProntoManager sharedInstance].categoriesToFilter = @[];
                    self.library.actions.categories = @[];
                    [self performSelector:@selector(doCleanUpAfterLogout:) withObject:tabVC.library afterDelay:0.2];
                }
                else
                {
                    [tabVC makeLibraryViewControllerVisible];
                    [self performSelector:@selector(doCleanUpAfterLogout:) withObject:tabVC.library afterDelay:0.2];
                    if([vcs.lastObject isKindOfClass:[HelpViewController class]])
                    {
                        [ZMUserActions sharedInstance].isLoggedoutFromHelpOtherTab = YES;
                    }
                }
            }
        }
            break;
        default:
            break;
    }
}

- (void)removeGlobalSearchViewControllerFromTabBarHierarchy:(ProntoTabbarViewController*)tabBar
{
    NSArray *childControllers = tabBar.currentController.childViewControllers;
    BOOL isGlobalSearchPresent = NO;
    for (UIViewController *vc in [childControllers reverseObjectEnumerator])
    {
        if ([vc isKindOfClass:[GlobalSearchVC class]])
        {
            isGlobalSearchPresent = YES;
            break;
        }
    }
    
    if (isGlobalSearchPresent == YES)
    {
        if ([tabBar.currentController isKindOfClass:[ZMLibrary class]])
        {
            [((ZMLibrary*)tabBar.currentController) resetGlobalSearchFromLibrary];
        }
        else if ([tabBar.currentController isKindOfClass:[Competitors class]])
        {
            [((Competitors*)tabBar.currentController) resetGlobalSearchFromCompetitors];
        }
        else if ([tabBar.currentController isKindOfClass:[ToolsTraining class]])
        {
            [((ToolsTraining*)tabBar.currentController) resetGlobalSearchFromToolsTraining];
        }
        else if ([tabBar.currentController isKindOfClass:[EventsViewController class]])
        {
            [((EventsViewController*)tabBar.currentController) resetGlobalSearchFromEvents];
        }
    }
}

- (void)doCleanUpAfterLogout:(ZMLibrary*)library
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([AOFLoginManager sharedInstance].checkForAOFLogin)
        {
            [[AOFLoginManager sharedInstance] AOFKitLogout];
        }
    });
    [ZMUserActions sharedInstance].shouldShowFranchiseListView    = YES;
    [ZMUserActions sharedInstance].shoudRefreshEventData = YES;
    [ZMUserActions sharedInstance].currentSelectedTabIndex = 0;
    [ZMProntoManager sharedInstance].categoriesToFilter = @[]; // left menu will have no data
    library.actions.categories = @[];
    [self dismissViewControllerAnimated:YES completion:NULL];
    [library clearUserData];
}

- (void)loadPrefencesListViewController
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kPreferenceListVCWillShowNotification object:nil];
}

//For adjusting label height according to content
-(CGRect) adjustLabelHeight: (UILabel *)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width - 10, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    CGRect newFrame = label.frame;
    newFrame.size.height = size.height;
    return newFrame;
}



@end
