//
//  CategoryMenuCell.m
//  Pronto
//
//  Created by Andrés David Carreño on 2/20/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "CategoryMenuCell.h"



@implementation CategoryMenuCell

+ (NSString*)GetCategoryMenuCellIdentifier
{
    return @"CategoryMenuCell";
}

+ (NSString*)GetCategoryMenuCellNibName
{
    return @"CategoryMenuCell";
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.cntLabel.hidden = YES;
    self.contentView.alpha = 1.0;
    self.userInteractionEnabled = YES;
}

@end
