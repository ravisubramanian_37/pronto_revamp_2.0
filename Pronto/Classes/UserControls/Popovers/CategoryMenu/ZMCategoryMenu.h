//
//  ZMCategoryMenu.h
//  Pronto
//
//  Created by Andres Ramirez on 2/7/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryMenuCell.h"
#import "ZMProntoTheme.h"

extern NSString* const kAllFranchiseAssets;
extern NSString* const kFranchiseFavorites;

@class ZMLibrary;

/**
 *  Class to display categories tableView
 */

@interface ZMCategoryMenu : UIViewController <UITableViewDelegate , UITableViewDataSource>

@property (weak, nonatomic) ZMLibrary *library;
//@property (nonatomic,strong) NSArray *arrItems;
@property (weak, nonatomic) IBOutlet UITableView *categoryMenuTableView;
@property (nonatomic,strong) NSMutableArray *categoriesArray;
@property (nonatomic) enum ProntoSectionType section;
@property (nonatomic) BOOL isNeeded;
@property (nonatomic,strong) NSMutableArray *allBrandsArray;


-(void)checkStatusCell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath;
- (void)setAnalyticsCategory;
-(void)savedCategories:(NSArray*)selectedCategories;

@end
