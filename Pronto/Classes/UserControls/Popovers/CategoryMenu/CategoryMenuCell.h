//
//  CategoryMenuCell.h
//  Pronto
//
//  Created by Andrés David Carreño on 2/20/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIImageView *checkedImage;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *cntLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;


@end
