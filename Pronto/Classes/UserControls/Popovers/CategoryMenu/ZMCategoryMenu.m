//
//  ZMCategoryMenu.m
//  Pronto
//
//  Created by Andres Ramirez on 2/7/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMCategoryMenu.h"
#import "ZMTablesDataLayer.h"
#import "ZMCategory.h"
#import "ZMAsset.h"
#import "ZMAssetsLibrary.h"
#import "ZMLibrary.h"
#import "Pronto-Swift.h"
#import "Categories.h"


NSString* const kFranchiseFavorites = @"Franchise Favorites";
NSString* const kAllFranchiseAssets = @"All Franchise Assets";

@interface ZMCategoryMenu (){
    id <ZMProntoTheme> theme;
}

@property(strong, nonatomic) ZMTablesDataLayer *categories;

@end

@implementation ZMCategoryMenu

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    theme = [ZMProntoTheme sharedTheme];
    _section = ProntoSectionTypeLibrary;

    //For ios6
    [self.categoryMenuTableView setOpaque:NO];
    [self.categoryMenuTableView setBackgroundColor:[UIColor clearColor]];
    
    [ZMUserActions sharedInstance].isCategoriesSelected = NO;
}

-(void) setCategoriesArray:(NSMutableArray *)categoriesArray{

    //Changes for Removing Warnings
        NSArray * categoriesToFilter = [ZMProntoManager sharedInstance].categoriesToFilter;
        _categoriesArray = [[NSMutableArray alloc]initWithArray:categoriesToFilter];
        [_categoryMenuTableView reloadData];
}

-(void)savedCategoriesForFieldUsers:(NSArray*)selectedCategories
{
        [ZMProntoManager sharedInstance].categoriesToFilter = selectedCategories;
    
        __block NSMutableArray * categoriesToSave = [[NSMutableArray alloc]init];
        __block NSMutableArray * categoriesToUpdateHeader = [[NSMutableArray alloc]init];
        [selectedCategories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    
            NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:obj];
    
            NSMutableDictionary * categoryNode = [[NSMutableDictionary alloc]init];
            [categoryNode setObject:[initElement valueForKey:@"selected"] forKey:@"selected"];
            [categoryNode setObject:[initElement valueForKey:@"name"] forKey:@"name"];
            [categoriesToUpdateHeader addObject:categoryNode];
    
            if (idx == 0){
                if ([[initElement valueForKey:@"selected"] isEqualToString:@"1"]){
                    *stop = YES;
                }else{
                    [categoriesToSave addObject:[initElement valueForKey:@"id"]];
                }
                if([ZMUserActions sharedInstance].isFieldUser)
                    ZMProntoManager.sharedInstance.franchisesAndBrands = [_library defaultFranchise:[_library getFranchiseName]];
                else
                    ZMProntoManager.sharedInstance.franchisesAndBrands = _library.franchiseList;
                [ZMProntoManager sharedInstance].selectedCategories = categoriesToSave;
    
            } else {
                if ([[initElement valueForKey:@"selected"] isEqualToString:@"1"]){
    
    
                    [categoriesToSave addObject:[initElement valueForKey:@"id"]];
                    // on selecting the Brands
                    if([[initElement objectForKey:@"id"] intValue] > 1 && [[initElement objectForKey:@"id"] intValue] <100)
                    {
                        // selecting the brands
                        NSMutableArray* franchiseBrands = [[_library franchiseToUpdateHeader:[ZMUserActions sharedInstance].franchiseName] copy];
    
                        NSDictionary* allFranchiseBrands = [franchiseBrands objectAtIndex:0];
                        NSDictionary* allBrands = [allFranchiseBrands objectForKey:@"brands"];
                        NSMutableArray* selectedBrand = [[NSMutableArray alloc] init];
                        for(id key in allBrands)
                        {
                            NSString* brandSelection = [key objectForKey:@"name"];
                            NSString* catSelection= [initElement valueForKey:@"name"];
                            if([brandSelection isEqualToString: catSelection])
                            {
                                [selectedBrand addObject:[key objectForKey:@"id"]];
                            }
                        }
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Previous brand selected %@", [ZMUserActions sharedInstance].brandsSelected]];
                        [selectedBrand addObject:[allFranchiseBrands objectForKey:@"id"]];
                        [[ZMUserActions sharedInstance].brandsSelected addObject:selectedBrand];
                        ZMProntoManager.sharedInstance.franchisesAndBrands = selectedBrand;
                        _isNeeded = YES;
                    }
                    else
                    {
                        _isNeeded = NO;
                    }
                }
            }
        }];
        // PRONTO - 29 - Fix Crashes - [UIKeyboardTaskQueue waitUntilAllTasksAreFinished]
        dispatch_async(dispatch_get_main_queue(),^{
    
            [ZMProntoManager sharedInstance].selectedCategoriesToUpdateHeader = categoriesToUpdateHeader;
    
            if(_isNeeded)
            {
                [ZMProntoManager sharedInstance].selectedCategories = @[];
    
                if(self.allBrandsArray.count == [ZMUserActions sharedInstance].brandsArray.count)
                {
                    if([ZMUserActions sharedInstance].isFieldUser)
                      ZMProntoManager.sharedInstance.franchisesAndBrands = [_library defaultFranchise:[_library getFranchiseName]];
                    else
                        ZMProntoManager.sharedInstance.franchisesAndBrands = _library.franchiseList;
                }
            }
            else
            {
                for (NSString *brand in [ZMUserActions sharedInstance].brandsArray) {
                    if ([[self.allBrandsArray valueForKey:@"name"] containsObject:brand])
                    {
                        _isNeeded = YES;
                    }
                }
                // check the brands selection
                if(!_isNeeded || ([ZMUserActions sharedInstance].brandsArray.count == [ZMUserActions sharedInstance].brandsSelected.count))
                {
                    if([ZMUserActions sharedInstance].isFieldUser)
                        ZMProntoManager.sharedInstance.franchisesAndBrands = [_library defaultFranchise:[_library getFranchiseName]];
                    else
                        ZMProntoManager.sharedInstance.franchisesAndBrands = _library.franchiseList;
    
                }
                [ZMProntoManager sharedInstance].selectedCategories = categoriesToSave;
            }
            _library.pageIndex = 0;
            [_library refreshLibrary];
        });
}

-(void)savedCategoriesForHomeOffice:(NSArray*)selectedCategories
{
    [ZMProntoManager sharedInstance].categoriesToFilter = selectedCategories;
    
    __block NSMutableArray * categoriesToSave = [[NSMutableArray alloc]init];
    __block NSMutableArray * categoriesToUpdateHeader = [[NSMutableArray alloc]init];
    __block NSMutableArray *tempArrayCollection   = [NSMutableArray array];
    
    [selectedCategories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:obj];
        
        NSMutableDictionary * categoryNode = [[NSMutableDictionary alloc]init];
        [categoryNode setObject:[initElement valueForKey:@"selected"] forKey:@"selected"];
        [categoryNode setObject:[initElement valueForKey:@"name"] forKey:@"name"];
        [categoriesToUpdateHeader addObject:categoryNode];
        
        if (idx == 0){
            if ([[initElement valueForKey:@"selected"] isEqualToString:@"1"]){
                *stop = YES;
            }else{
                [categoriesToSave addObject:[initElement valueForKey:@"id"]];
            }
            if([ZMUserActions sharedInstance].isFieldUser)
                ZMProntoManager.sharedInstance.franchisesAndBrands = [_library defaultFranchise:[_library getFranchiseName]];
            else
                ZMProntoManager.sharedInstance.franchisesAndBrands = _library.franchiseList;
            [ZMProntoManager sharedInstance].selectedCategories = categoriesToSave;
            
        }
        else
        {
            if ([[initElement valueForKey:@"selected"] isEqualToString:@"1"])
            {
                [categoriesToSave addObject:[initElement valueForKey:@"id"]];
                // on selecting the Brands
                if([[initElement objectForKey:@"id"] intValue] > 1 && [[initElement objectForKey:@"id"] intValue] <100)
                {
                    NSArray *allFranchise = [_library getAllFranchieseName];
                    NSMutableArray *allBrandsDictArray = [NSMutableArray array];
                    
                    for (NSString *franchise in allFranchise)
                    {
                        NSArray *tempArray = [[_library franchiseToUpdateHeader:franchise] copy];
                        //if there is no Brands/Franchise to add, look for other franchise.
                        if (tempArray == nil || tempArray.count <= 0) {
                            continue;
                        }
                        
                        NSMutableDictionary *tempAllBrandsDict = [NSMutableDictionary dictionary];
                        [tempAllBrandsDict addEntriesFromDictionary:[tempArray objectAtIndex:0]];
                        // it is  array of dictionary  of all brand and franchise  dictionary
                        [allBrandsDictArray addObject:tempAllBrandsDict];
                    }
                    
                    for (NSDictionary *franchiseAndBrandsCollection in allBrandsDictArray)
                    {
                        NSString *franchiseName = franchiseAndBrandsCollection[@"name"];
                        NSMutableArray* franchiseBrandslcl = [[_library franchiseToUpdateHeader:franchiseName] copy];
                        //if there is no Brands/Franchise to add, look for other franchise.
                        if (franchiseBrandslcl == nil || franchiseBrandslcl.count <= 0) {
                            continue;
                        }
                        NSDictionary* allFranchiseBrandslcl = [franchiseBrandslcl objectAtIndex:0];
                        NSDictionary* allBrandslcl = [allFranchiseBrandslcl objectForKey:@"brands"];
                        NSMutableArray* selectedBrandlcl = [[NSMutableArray alloc] init];
                        
                        BOOL shouldIncludeFranchiseName = NO;
                        
                        for(id key in allBrandslcl)
                        {
                            NSString* brandSelection = [key objectForKey:@"name"];
                            NSString* catSelection= [initElement valueForKey:@"name"];
                            if([brandSelection isEqualToString: catSelection])
                            {
                                [selectedBrandlcl addObject:[key objectForKey:@"id"]];
                                shouldIncludeFranchiseName = YES;
                            }
                        }
                        
                        if (shouldIncludeFranchiseName == YES)
                        {
//                            shouldIncludeFranchiseName = NO;
                            [selectedBrandlcl addObject:[allFranchiseBrandslcl objectForKey:@"id"]];
                        }
                        [tempArrayCollection addObjectsFromArray:selectedBrandlcl];
                    }
                    _isNeeded = YES;
                }
                else
                {
                    _isNeeded = NO;
                }
            }
        }
    }];
    
    ZMProntoManager.sharedInstance.franchisesAndBrands = [self arrayByEliminatingDuplicatesWithoutOrder:tempArrayCollection];
    //As on now brandsSelected, I have kept = franchisesAndBrands, But needs to clearify from buvana
    [ZMUserActions sharedInstance].brandsSelected = (NSMutableArray*)ZMProntoManager.sharedInstance.franchisesAndBrands;
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Franchise And brands %@", ZMProntoManager.sharedInstance.franchisesAndBrands]];
    dispatch_async(dispatch_get_main_queue(),^{
        
        [ZMProntoManager sharedInstance].selectedCategoriesToUpdateHeader = categoriesToUpdateHeader;
        
        if(_isNeeded)
        {
            [ZMProntoManager sharedInstance].selectedCategories = @[];
            
            if([ZMUserActions sharedInstance].isFieldUser == YES)
            {
                if(self.allBrandsArray.count == [ZMUserActions sharedInstance].brandsArray.count)
                {
                    if([ZMUserActions sharedInstance].isFieldUser)
                        ZMProntoManager.sharedInstance.franchisesAndBrands = [_library defaultFranchise:[_library getFranchiseName]];
                    else
                        ZMProntoManager.sharedInstance.franchisesAndBrands = _library.franchiseList;
                }
            }
        }
        else
        {
            for (NSString *brand in [ZMUserActions sharedInstance].brandsArray) {
                if ([[self.allBrandsArray valueForKey:@"name"] containsObject:brand])
                {
                    _isNeeded = YES;
                }
            }
            // check the brands selection
            if(!_isNeeded || ([ZMUserActions sharedInstance].brandsArray.count == [ZMUserActions sharedInstance].brandsSelected.count))
            {
                if([ZMUserActions sharedInstance].isFieldUser)
                    ZMProntoManager.sharedInstance.franchisesAndBrands = [_library defaultFranchise:[_library getFranchiseName]];
                else
                    ZMProntoManager.sharedInstance.franchisesAndBrands = _library.franchiseList;
                
            }
            [ZMProntoManager sharedInstance].selectedCategories = categoriesToSave;
        }
        _library.pageIndex = 0;
        [_library refreshLibrary];
    });
}

-(void)savedCategories:(NSArray*)selectedCategories
{
    if([ZMUserActions sharedInstance].isFieldUser)
    {
        [self savedCategoriesForFieldUsers:selectedCategories];
    }
    else
    {
        [self savedCategoriesForHomeOffice:selectedCategories];
    }
}

- (NSMutableArray *)arrayByEliminatingDuplicatesWithoutOrder:(NSMutableArray *)array{
    
    NSArray *uniqueObjects = [[NSSet setWithArray:[array copy]] allObjects];
    
    NSMutableArray* newArray  = [uniqueObjects mutableCopy];
    return newArray;
    
}
/// New Revamping changes - Left menu selection

-(void)checkStatusCell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath{
    
    NSMutableDictionary *category = [[NSMutableDictionary alloc] initWithDictionary:[_categoriesArray objectAtIndex:indexPath.row]];
    NSMutableArray *finalArray = [[NSMutableArray alloc]init];
    //NSMutableArray *allBrandsArray = [[NSMutableArray alloc]init];
    
    __block BOOL isAllCategorySelected = NO;
//    BOOL needsUpdateAllItems = NO;
    int index = (int)indexPath.row;
    
    if(_section == ProntoSectionTypeLibrary)
    {
    //New implementation taking data from CoreData only for Library Section

        switch (index) {
            case 0:
                if ([[category objectForKey:@"selected"] intValue] == 0) {
                    [_categoriesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:obj];
                       // New Revamping - All Franchise Assets Filter Selection
                        if(finalArray.count < 1 && ([[initElement objectForKey:@"name"] isEqualToString:kAllFranchiseAssets]))
                           [initElement setObject: @"1" forKey:@"selected"];
                        else
                            [initElement setObject:@"0" forKey:@"selected"];
                       [finalArray addObject: initElement];
                     
                    }];
                }else{
                    [_categoriesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:obj];
                        [initElement setObject: @"0" forKey:@"selected"];
                        [finalArray addObject: initElement];
                    }];
                }
                _categoriesArray = finalArray;
//                needsUpdateAllItems = YES;
    
                [ZMUserActions sharedInstance].selectLeftMenu = kAllFranchiseAssets;
                break;
            case 1:
                if ([[category objectForKey:@"selected"] intValue] == 0) {
                    
                    [_categoriesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:obj];
                        // New Revamping - Franchise Favorites Filter Selection
                        if([[initElement objectForKey:@"name"] isEqualToString:kFranchiseFavorites])
                             [initElement setObject: @"1" forKey:@"selected"];
                        else
                            [initElement setObject:@"0" forKey:@"selected"];
                            
                        [finalArray addObject: initElement];
                        
                    }];
                    [ZMUserActions sharedInstance].selectLeftMenu = kFranchiseFavorites;
                }else{
                    [_categoriesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:obj];
                        [initElement setObject: @"0" forKey:@"selected"];
                        [finalArray addObject: initElement];
                    }];
                    
                    [ZMUserActions sharedInstance].selectLeftMenu = kAllFranchiseAssets;
                }
                _categoriesArray = finalArray;
//                needsUpdateAllItems = YES;
                break;
            default:
                // display message when there are no assets
                [ZMUserActions sharedInstance].selectLeftMenu = kAllFranchiseAssets;
                if ([[category objectForKey:@"selected"] intValue] == 0){
                NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:category];
                // New Revamping - Franchise Favorites Filter Selection
                if([[initElement objectForKey:@"name"] isEqualToString:kAllFranchiseAssets] || [[initElement objectForKey:@"name"] isEqualToString:kFranchiseFavorites])
                    [initElement setObject: @"0" forKey:@"selected"];
                else
                    [initElement setObject: @"1" forKey:@"selected"];
                
                if([[[_categoriesArray objectAtIndex:1] objectForKey:@"selected"] integerValue] == 1)
                {
                    NSMutableDictionary *otherElement = [[NSMutableDictionary alloc] initWithDictionary:[_categoriesArray objectAtIndex:1]];
                    [otherElement setObject: @"0" forKey:@"selected"];
                    [_categoriesArray replaceObjectAtIndex:1 withObject:otherElement];
                }
                
                [_categoriesArray replaceObjectAtIndex:index withObject:initElement];
                [ZMUserActions sharedInstance].isCategoriesSelected = YES;
                
            }
            else
            {
                
                NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:category];
                [initElement setObject: @"0" forKey:@"selected"];
                [_categoriesArray replaceObjectAtIndex:index withObject:initElement];
                [ZMUserActions sharedInstance].isCategoriesSelected = NO;
            }
                [_categoriesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    isAllCategorySelected = NO;
                    if (idx != 0){
                        NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:obj];
                        if ([[initElement valueForKey:@"selected"] isEqualToString:@"0"]){
                            isAllCategorySelected = NO;
                            *stop = YES;
                        }
                    }
                }];
                
                // first row will not get selected with any of the rows
                NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:[_categoriesArray objectAtIndex:0]];
                isAllCategorySelected = NO;
                if (isAllCategorySelected) {
                    _library.actions.isAllCategorySelected = NO;
                    [initElement setObject: @"1" forKey:@"selected"];
                    [_categoriesArray replaceObjectAtIndex:0 withObject:initElement];
                }else{
                    _library.actions.isAllCategorySelected = NO;
                    [initElement setObject: @"0" forKey:@"selected"];
                    [_categoriesArray replaceObjectAtIndex:0 withObject:initElement];
                }
                //Control asssets change when moved from fav
//                ZMProntoManager.sharedInstance.favArrayIds = @[];
                break;
        }
       
        // check if all the categories unselected - in that case select the first row
        NSMutableArray *finalCatArray = [[NSMutableArray alloc]init];
        [_categoriesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if (idx != 0){
                NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:obj];
                if ([[initElement valueForKey:@"selected"] isEqualToString:@"1"]){
                    [finalCatArray addObject:initElement];
                    
                }
            }
        }];
        
        if(finalCatArray.count == 0)
        {
            NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:[_categoriesArray objectAtIndex:0]];
            [initElement setObject: @"1" forKey:@"selected"];
            [_categoriesArray replaceObjectAtIndex:0 withObject:initElement];
        }
        // end of checking unselect rows
        
        NSArray *filtered = [_categoriesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(selected == %@)", @"1"]];
        self.allBrandsArray = [NSMutableArray arrayWithArray:filtered];
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"allBrandsArray:%@",self.allBrandsArray]];
        [self savedCategories:_categoriesArray];
        
    }
    
    [_categoryMenuTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _categoriesArray.count+1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CategoryMenuCell";
    CategoryMenuCell *cell = (CategoryMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryMenuCell" owner:self options:nil];
        
        [tableView registerNib:[UINib nibWithNibName:@"CategoryMenuCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
        
        cell = (CategoryMenuCell*)[nib objectAtIndex:0];
        
    }
    
    NSDictionary *category  = nil;
    if (_categoriesArray.count >= indexPath.row)
    {
        category = [_categoriesArray objectAtIndex:indexPath.row];
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"categoriesArray:%@",[_categoriesArray objectAtIndex:indexPath.row]]];
    }

    [ZMUserActions sharedInstance].categories = _categoriesArray;
    
    // Configure the cell...
    // cell.mainLabel.textColor = [UIColor whiteColor];
    cell.mainLabel.text = [category objectForKey:@"name"];
    
    if([[ZMProntoManager sharedInstance].categoriesToFilter count] > 1 && [[ZMProntoManager sharedInstance].categoriesToFilter count] > indexPath.row)
    {
        if(indexPath.row <= [_categoriesArray count])
        {
            cell.mainLabel.text = [category objectForKey:@"name"];
            
        }
    }
    else
    {
        if(indexPath.row == 0)
           cell.mainLabel.text = kAllFranchiseAssets;
        else
           cell.mainLabel.text = kFranchiseFavorites;
    }
    
    /// New Revamping changes
    if ([[category objectForKey:@"selected"] integerValue] == 1)
    {
        cell.checkedImage.hidden = NO;
        [theme tintArrow:cell.checkedImage];
        cell.mainLabel.frame = CGRectMake(45, 11, 177, 21);
        cell.checkedImage.image = [UIImage imageNamed:@"checkListSelect"];
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        
       
    } else {
       
        cell.checkedImage.hidden = NO;
        cell.checkedImage.image = [UIImage imageNamed:@"checkList"];
        [theme tintArrow:cell.checkedImage];
        cell.mainLabel.frame = CGRectMake(45, 11, 177, 21);
        
    }
        
   cell.backgroundColor = [UIColor clearColor];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [ZMUserActions sharedInstance].isCategoriesSelected = YES;
    
    [_categoryMenuTableView deselectRowAtIndexPath:indexPath animated:YES];
    CategoryMenuCell *cell = (CategoryMenuCell*)[tableView cellForRowAtIndexPath:indexPath];
    [self checkStatusCell:cell indexPath:indexPath];
    [self setAnalyticsCategory];

}

- (void)setAnalyticsCategory{
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject:@"filter|franchise:brand" forKey:@"scfiltercategory"];
    [trackingOptions setObject:[[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
    [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    [trackingOptions setObject:[NSString stringWithFormat: @"%d", [ZMUserActions sharedInstance].assetsCount] forKey:@"scfilterresultcount"];
    [trackingOptions setObject:([ZMUserActions sharedInstance].assetsCount>0)?@"successful":@"unsuccessful" forKey:@"scsearchaction"];
    [trackingOptions setObject:/*[ZMUserActions sharedInstance].search*/[[ZMProntoManager sharedInstance].search getTitle] forKey:@"scsearchterm"];
    [trackingOptions setObject:[_library getSelectedSort] forKey:@"scsortaction"];
    [trackingOptions setObject:[_library getCategoriesForTracking] forKey:@"scsortcategory"];
    [trackingOptions setObject:@"single" forKey:@"scsorttype"];
    
    /**
     * @Tracking
     * Franchise Selection
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section
              withSubsection:nil
                    withName:[ZMUserActions sharedInstance].franchiseName
                 withOptions:trackingOptions];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

