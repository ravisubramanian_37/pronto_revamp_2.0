//
//  ZMMessages.m
//  Pronto
//
//  Created by Andres Ramirez on 2/7/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMMessages.h"
#import "ZMCell.h"
#import "ZMTablesDataLayer.h"

@interface ZMMessages ()

@property(strong, nonatomic) ZMTablesDataLayer *messages;

@end

@implementation ZMMessages

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    self.messages = [[ZMTablesDataLayer alloc] init];
    self.arrItems = [self.messages messages];
    static NSString *CellIdentifier = @"Cell";
    [self.messagesTableView registerClass:[ZMCell class] forCellReuseIdentifier:CellIdentifier];
    [super viewDidLoad];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return self.arrItems.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    ZMCell *cell = (ZMCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil) {
        cell = [[ZMCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    // Configure the cell...
    cell.accessibilityValue = [[self.arrItems objectAtIndex:indexPath.row] objectForKey:@"id"];
    [cell.titleLabel setText:[[self.arrItems objectAtIndex:indexPath.row] objectForKey:@"title"]];
    [cell.descriptionlabel setText:[[self.arrItems objectAtIndex:indexPath.row] objectForKey:@"description"]];
    [cell.statusLabel setText:[[self.arrItems objectAtIndex:indexPath.row] objectForKey:@"state"]];
    [cell.categoryLabel setText:[[self.arrItems objectAtIndex:indexPath.row] objectForKey:@"category"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.text = @"SELECTED";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
