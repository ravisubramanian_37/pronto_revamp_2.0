//
//  ZMMessages.h
//  Pronto
//
//  Created by Andres Ramirez on 2/7/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZMMessages : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *messagesTableView;

@property (nonatomic,strong) NSArray *arrItems;
@property (nonatomic,strong) NSString* selectedItems;

@end
