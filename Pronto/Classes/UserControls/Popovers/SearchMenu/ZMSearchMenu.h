//
//  ZMSearchMenu.h
//  Pronto
//
//  Created by Buvana on 12/06/17.
// 
//

#import <UIKit/UIKit.h>
#import "CategoryMenuCell.h"
#import "ZMProntoTheme.h"

@class ZMLibrary;

@protocol searchMenuDelegate <NSObject>
@end

@interface ZMSearchMenu : UIViewController <UITableViewDelegate , UITableViewDataSource>

/**
 *  Class to show sort paramenters
 */

@property (weak, nonatomic) ZMLibrary *library;
@property (weak, nonatomic) Competitors* competitors;
@property (weak, nonatomic) ToolsTraining* tools;
@property (weak, nonatomic) EventsViewController* events;
@property (strong , nonatomic) ZMHeader *parent;
@property (weak, nonatomic) IBOutlet UITableView *searchMenuTableView;

//@property (nonatomic, strong) NSArray *arrItems;
@property(strong, nonatomic) NSMutableArray *selectedValues;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (nonatomic,strong) NSMutableArray *sortArray;
@property (nonatomic, weak) id<searchMenuDelegate> delegate;

//@property BOOL isFranchise;

@end
