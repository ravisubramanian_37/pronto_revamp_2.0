//
//  ZMSearchMenu.m
//  Pronto
//
//  Created by Buvana on 12/06/17.
//
//

#import "ZMSearchMenu.h"
#import "ZMTablesDataLayer.h"
#import "ZMAsset.h"
#import "ZMAssetsLibrary.h"
#import "ZMLibrary.h"
#import "ZMHeader.h"
#import "Pronto-Swift.h"
#import "ZMUserActions.h"
#import "Constant.h"

@interface ZMSearchMenu (){
    id <ZMProntoTheme> theme;
}

@property(strong, nonatomic) ZMTablesDataLayer *sort;

@end

@implementation ZMSearchMenu

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedValues = [[NSMutableArray alloc]init];
    theme = [ZMProntoTheme sharedTheme];
    
    //For ios6
    [self.searchMenuTableView setOpaque:NO];
    [self.searchMenuTableView setBackgroundColor:[UIColor clearColor]];
}


- (void)setSortArray:(NSMutableArray *)sortArray{
    _sortArray = [[NSMutableArray alloc] initWithArray:sortArray];
    [_searchMenuTableView reloadData];
}

- (void)setSelectedValues:(NSMutableArray *)selectedValues{
    
    _selectedValues = selectedValues;
    for (NSDictionary* currentDic in selectedValues) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[[currentDic objectForKey:@"id"] intValue] inSection:0];
        [self.searchMenuTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self.searchMenuTableView.delegate tableView:self.searchMenuTableView didSelectRowAtIndexPath:indexPath];
    }
     
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

-(void) selectDeselectCell:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    
    if (_sortArray.count > 0 && _sortArray.count >= indexPath.row)
    {
        _parent.searchTextbox.text = [_sortArray objectAtIndex:indexPath.row];
    }
    
    [[_parent.searchMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    [self.parent doRequiredSetupOnDidSelectOfSearchMenu];
}

- (void)unmarkCells:(UITableView *)tableView {
    
    int totalCells = 5;
    
    for (int i = 0; i<= totalCells; i++) {
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:i inSection:0];
        CategoryMenuCell *cell = (CategoryMenuCell*)[tableView cellForRowAtIndexPath:myIP];
        cell.checkedImage.hidden = YES;
        cell.mainLabel.frame = CGRectMake(11, 11, 177, 21);
    }
}


- (bool) isMarquedAToZ
{
    BOOL marked = NO;
    for (id obj in _sortArray){
        if ([[obj objectForKey:@"id"] isEqualToString:@"2"]) {
            if ([[obj objectForKey:@"selected"] intValue] == 1 ){
                marked = YES;
            }
        }
    }
    return marked;
}

- (bool) isMarquedZToA
{
    BOOL marked = NO;
    for (id obj in _sortArray){
        if ([[obj objectForKey:@"id"] isEqualToString:@"3"]) {
            if ([[obj objectForKey:@"selected"] intValue] == 1 ){
                marked = YES;
            }
        }
    }
    return marked;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return _sortArray.count;
            break;
    }
    return 0;

    // Return the number of rows in the section.
    //return _sortArray.count;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    int height = 44;
    if([indexPath section] == 0) {
        height = 25;
    }
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"CategoryMenuCell";
    CategoryMenuCell *cell = (CategoryMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryMenuCell" owner:self options:nil];
        
        [tableView registerNib:[UINib nibWithNibName:@"CategoryMenuCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
        
        cell = (CategoryMenuCell*)[nib objectAtIndex:0];
    }
    
    switch([indexPath section]){
        case 0:
        {
            // Configure the cell...
            cell.mainLabel.textColor = [UIColor blackColor];
            cell.mainLabel.text = @"   Recent Searches";
            // New Revamping changes - Search bar text
            cell.mainLabel.font = [theme normalLato];
            //Chnages for Warnings
            cell.mainLabel.frame = cell.contentView.frame;
            cell.mainLabel.backgroundColor = [UIColor colorWithRed:218.0/255.0 green:223.0/255.0 blue:232.0/255.0 alpha:1];
            
            break;
        }
        case 1:
        {
            // Configure the cell...
            // New Revamping changes - font colour
            cell.mainLabel.font = [theme bigNormalLato];
            cell.mainLabel.textColor = [UIColor blackColor];
            cell.mainLabel.text = [_sortArray objectAtIndex:indexPath.row];
            cell.accessibilityValue = [_sortArray objectAtIndex:indexPath.row];
            break;
        }
    }
    
    CGRect frame1 = cell.separatorView.frame;
    frame1.size.height = 2;
    cell.separatorView.frame = frame1;
    cell.separatorView.backgroundColor =[UIColor colorWithRed:214.0/255.0 green:219.0/255.0 blue:232.0/255.0 alpha:1];
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch([indexPath section]){
        case 0:
            return;
            break;
        case 1:
        {            
            [ZMUserActions sharedInstance].searchingText = [_sortArray objectAtIndex:indexPath.row];
             [self selectDeselectCell:indexPath tableView:tableView];
            [_parent textFieldDidChange:_parent.searchTextbox];
            [_parent setDeleteBackward:NO];
           

          break;
        }
            
    }
    

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectDeselectCell:indexPath tableView:tableView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // self.view.superview.layer.cornerRadius = 0;
    self.view.superview.layer.cornerRadius = 0;
    
    // Pronto new revamp changes: To remove corner radius and to set selected sorted items
    self.view.superview.clipsToBounds = NO;
    
    //For making the search history aligned
    
    CGRect frame = self.backgroundView.frame;
    frame.origin.x = self.view.superview.frame.origin.x;
    frame.size.width = _parent.searchTextbox.frame.size.width - 7;
    self.backgroundView.frame = frame;
    
    CGRect frame1 = self.searchMenuTableView.frame;
    frame1.origin.x = self.view.superview.frame.origin.x;
    frame1.size.width = _parent.searchTextbox.frame.size.width - 7;
    self.searchMenuTableView.frame = frame;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
