//
//  ProntoTabbarViewController.m
//  Pronto
//
//  Created by CCC on 28/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//
#import "ProntoTabbarViewController.h"
#import "ZMLibrary.h"
#import "Pronto-Swift.h"
#import "ZMUserActions.h"
#import "UIColor+ColorDirectory.h"

const CGFloat tabBarItemWidth = 104;
const CGFloat tabBarItemHeight = 54;
const CGFloat numberOfTabBarItems = 4;


@interface ProntoTabbarViewController () {
    UIView *transparentView;
}

@property (weak, nonatomic) IBOutlet UIView *tabBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;


@end


@implementation ProntoTabbarViewController


- (void)makeAllTabBarItemSelectable:(BOOL)selectable
{
    self.franchiseTabbarItem.userInteractionEnabled     = selectable;
    self.competitorTabbarItem.userInteractionEnabled    = selectable;
    self.toolsTrainingTabbarItem.userInteractionEnabled = selectable;
    self.eventsTabbarItem.userInteractionEnabled        = selectable;
    
    if(!selectable){
         transparentView = [[UIView alloc]initWithFrame:self.tabBarView.frame];
        transparentView.backgroundColor = [UIColor lightGrayColor];
        transparentView.alpha = 0.50f;
        [self.tabBarView.superview addSubview:transparentView];
        [self.tabBarView.superview bringSubviewToFront:transparentView];
    }
    else{
        if(transparentView != nil){
            [transparentView removeFromSuperview];
            transparentView = nil;
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setController:@"ZMLibrary"];
    [self setTabBarItemFrame];

}

-(void)viewDidLayoutSubviews {
//    [self hideContentController: self.currentController];
    [self displayContentController: self.currentController];
    [self setTabBarItemFrame];
}

- (void)setController:(NSString*)nameOfController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.currentController = [sb instantiateViewControllerWithIdentifier:nameOfController];
    if ([self.currentController isKindOfClass:[ZMLibrary class]])
    {
        self.library = (ZMLibrary*)self.currentController;
        self.library.shouldLoadAllData = NO;
    }
    [self displayContentController: self.currentController];
}

- (void)makeLibraryViewControllerVisible
{
    [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseSelect"] forState: UIControlStateNormal];
    [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compUnselect"] forState: UIControlStateNormal];
    [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsUnselect"] forState: UIControlStateNormal];
    [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventUnselect"] forState: UIControlStateNormal];
    _franchiseTabbarItem.backgroundColor = [UIColor GetColorFromHexValue071D49];
    _competitorTabbarItem.backgroundColor = [UIColor clearColor];
    _toolsTrainingTabbarItem.backgroundColor = [UIColor clearColor];
    _eventsTabbarItem.backgroundColor = [UIColor clearColor];
    [self hideContentController: self.currentController];
    //Resetting the datasource to show empty screen.
    [self.library.assetsPage addObjectsFromArray:@[]];
    [ZMProntoManager sharedInstance].categoriesToFilter = @[];
    self.library.actions.categories = @[];
    [self displayContentController: self.library];
    self.currentController = self.library;
}

- (IBAction)tabBarItemTapped:(UIButton*)sender
{
    //If already selected tab Bar Item Tapped again. Don't do anything and return;
    if ([ZMUserActions sharedInstance].currentSelectedTabIndex == sender.tag){ return; }
    
    switch (sender.tag)
    {
        case 0:
        {
            [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseSelect"] forState: UIControlStateNormal];
            [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compUnselect"] forState: UIControlStateNormal];
            [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsUnselect"] forState: UIControlStateNormal];
            [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventUnselect"] forState: UIControlStateNormal];
            _franchiseTabbarItem.backgroundColor = [UIColor GetColorFromHexValue071D49];
            _competitorTabbarItem.backgroundColor = [UIColor clearColor];
            _toolsTrainingTabbarItem.backgroundColor = [UIColor clearColor];
            _eventsTabbarItem.backgroundColor = [UIColor clearColor];
            [self hideContentController: self.currentController];
            [self setController:@"ZMLibrary"];
        }
            break;
        case 1:
        {
            [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseUnselect"] forState: UIControlStateNormal];
            [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compSelect"] forState: UIControlStateNormal];
            [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsUnselect"] forState: UIControlStateNormal];
            [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventUnselect"] forState: UIControlStateNormal];
            _franchiseTabbarItem.backgroundColor = [UIColor clearColor];
            _competitorTabbarItem.backgroundColor = [UIColor GetColorFromHexValue071D49];
            _toolsTrainingTabbarItem.backgroundColor = [UIColor clearColor];
            _eventsTabbarItem.backgroundColor = [UIColor clearColor];
            [self hideContentController: self.currentController];
            [self setController:@"Competitors"];
        }
            break;
        case 2:
        {
            [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseUnselect"] forState: UIControlStateNormal];
            [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compUnselect"] forState: UIControlStateNormal];
            [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsSelect"] forState: UIControlStateNormal];
            [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventUnselect"] forState: UIControlStateNormal];
            _franchiseTabbarItem.backgroundColor = [UIColor clearColor];
            _competitorTabbarItem.backgroundColor = [UIColor clearColor];
            _toolsTrainingTabbarItem.backgroundColor = [UIColor GetColorFromHexValue071D49];
            _eventsTabbarItem.backgroundColor = [UIColor clearColor];
            [self hideContentController: self.currentController];
            [self setController:@"ToolsTraining"];
        }
            break;
        case 3:
        {
            [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseUnselect"] forState: UIControlStateNormal];
            [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compUnselect"] forState: UIControlStateNormal];
            [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsUnselect"] forState: UIControlStateNormal];
            [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventSelect"] forState: UIControlStateNormal];
            _franchiseTabbarItem.backgroundColor = [UIColor clearColor];
            _competitorTabbarItem.backgroundColor = [UIColor clearColor];
            _toolsTrainingTabbarItem.backgroundColor = [UIColor clearColor];
            _eventsTabbarItem.backgroundColor = [UIColor GetColorFromHexValue071D49];
            [self hideContentController: self.currentController];
            [self setController:@"EventsViewController"];

        }
            break;
        default:
            break;
    }
}

- (void) displayContentController: (UIViewController*) content {
    [self addChildViewController:content];
    content.view.frame = CGRectMake(0, _contentView.frame.origin.y, _contentView.frame.size.width, self.view.frame.size.height - ( 54) );
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
}

- (void) hideContentController: (UIViewController*) content {
    [content willMoveToParentViewController:nil];
    [content.view removeFromSuperview];
    [content removeFromParentViewController];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
}
    
- (void) setTabBarItemFrame {
    CGFloat xOrigin =  ([UIScreen mainScreen].bounds.size.width - (tabBarItemWidth * numberOfTabBarItems)) / 2;
    self.franchiseTabbarItem.frame = CGRectMake(xOrigin, 0, tabBarItemWidth, tabBarItemHeight);
    self.competitorTabbarItem.frame = CGRectMake(xOrigin + tabBarItemWidth, 0, tabBarItemWidth, tabBarItemHeight);
    self.toolsTrainingTabbarItem.frame = CGRectMake(xOrigin + (tabBarItemWidth * 2), 0, tabBarItemWidth, tabBarItemHeight);
    self.eventsTabbarItem.frame = CGRectMake(xOrigin + (tabBarItemWidth * 3), 0, tabBarItemWidth, tabBarItemHeight);
}

- (void)cycleFromViewController: (UIViewController*) oldVC
               toViewController: (UIViewController*) newVC {
    // Prepare the two view controllers for the change.
    [oldVC willMoveToParentViewController:nil];
    [self addChildViewController:newVC];
    
    // Get the start frame of the new view controller and the end frame
    // for the old view controller. Both rectangles are offscreen.
    //    newVC.view.frame = [self newViewStartFrame];
    //    CGRect endFrame = [self oldViewEndFrame];
    
    // Queue up the transition animation.
    [self transitionFromViewController: oldVC toViewController: newVC
                              duration: 0.25 options:0
                            animations:^{
                                // Animate the views to their final positions.
                                //                                newVC.view.frame = oldVC.view.frame;
                                //                                oldVC.view.frame = endFrame;
                            }
                            completion:^(BOOL finished) {
                                // Remove the old view controller and send the final
                                // notification to the new view controller.
                                [oldVC removeFromParentViewController];
                                [newVC didMoveToParentViewController:self];
                            }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateTabSelection:(int)index
{
    self.tabBarController.tabBar.selectedItem.tag = index;
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Tab index = %d", index]];
    
    AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [mainDelegate currentTabIndex:index];
    // [self tabBar:self.tabBarController.tabBar didSelectItem:self.tabBarController.tabBar.selectedItem];
    
    switch (index) {
        case 0:
        {
            [AbbvieLogging logInfo:@"Landing screen"];
            [ZMUserActions sharedInstance].isLoginSuccess = YES;
            
        }
            break;
        case 1:
        {
            [AbbvieLogging logInfo:@"Comp screen"];
            
        }
            break;
        case 2:
        {
            [AbbvieLogging logInfo:@"Tools screen"];
        }
            break;
        case 3:
        {
            [AbbvieLogging logInfo:@"Events screen"];
        }
            break;
        default:
            break;
    }
}


@end
