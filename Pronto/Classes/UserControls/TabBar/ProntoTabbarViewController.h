//
//  ProntoTabbarViewController.h
//  Pronto
//
//  Created by CCC on 28/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProntoTabbarViewController.h"
#import "ZMProntoViewController.h"
@class ZMLibrary;

@interface ProntoTabbarViewController : UIViewController
{
    
}

@property (weak, nonatomic) IBOutlet UIButton *franchiseTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *competitorTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *toolsTrainingTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *eventsTabbarItem;

@property (nonatomic, strong) UIViewController* currentController;
@property (nonatomic, strong) ZMLibrary *library;

-(void) updateTabSelection:(int)index;
-(void)makeLibraryViewControllerVisible;
- (void)makeAllTabBarItemSelectable:(BOOL)selectable;

- (IBAction)tabBarItemTapped:(UIButton*)sender;

@end
