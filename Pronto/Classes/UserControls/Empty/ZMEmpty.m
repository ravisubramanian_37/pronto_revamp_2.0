//
//  ZMEmpty.m
//  Pronto
//
//  Created by Andres Ramirez on 3/3/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//
#import "ZMProntoTheme.h"
#import "ZMLibrary.h"
#import "ZMEmpty.h"

@interface ZMEmpty ()
{
    id <ZMProntoTheme> theme;
}

@end

@implementation ZMEmpty

- (id)initWithFrame:(CGRect)frame
{
    return nil;
}

- (id) initWithEventScreenAsParent:(UIView*)parent
{
    self = (ZMEmpty *)[[[NSBundle mainBundle] loadNibNamed:@"Empty"
                                                     owner:self options:nil] objectAtIndex:0];
    theme = [ZMProntoTheme sharedTheme];
    [self styleHelp];
    if(self && !self.parent)
    {
        self.parent = parent;
        
        CGRect frame = self.frame;
        frame.origin.x = (self.parent.frame.size.width - frame.size.width)/2;
        frame.origin.y = (self.parent.frame.size.height - (frame.size.height + 64))/2;
        self.frame = frame;
    }
    self.userInteractionEnabled = NO;
    return self;
}

- (id) initWithGlobalSearchAsParent:(UIView*)parent
{
    self = (ZMEmpty *)[[[NSBundle mainBundle] loadNibNamed:@"Empty"
                                                     owner:self options:nil] objectAtIndex:0];
    theme = [ZMProntoTheme sharedTheme];
    [self styleHelp];
    if(self && !self.parent)
    {
        self.parent = parent;
        
        CGRect frame = self.frame;
        frame.origin.x = (self.parent.superview.superview.frame.size.width - frame.size.width)/2;
        frame.origin.y = (self.parent.superview.superview.frame.size.height - (frame.size.height + 64))/2;
        self.frame = frame;
    }
    self.userInteractionEnabled = NO;
    return self;
}

- (id) initWithParent:(UIView*)parent
{
    self = (ZMEmpty *)[[[NSBundle mainBundle] loadNibNamed:@"Empty"
                                                             owner:self options:nil] objectAtIndex:0];
    theme = [ZMProntoTheme sharedTheme];
    [self styleHelp];
    if(self && !self.parent){
        self.parent = parent;
        [self centerMessage];
    }
    self.userInteractionEnabled = NO;
    
    return self;
}


-(void) centerMessage
{
    if(self.parent){
        CGRect frame = self.frame;
        // reduce the leftMenuTable width
        frame.origin.x = (self.parent.superview.superview.frame.size.width - frame.size.width + 232)/2;
        frame.origin.y = (self.parent.superview.superview.frame.size.height - frame.size.height)/2;
        self.frame = frame;
    }
}

-(void) updateMessage
{
    CGRect frame = self.frame;
    frame.origin.x = self.parent.frame.size.width/2-210;
    frame.origin.y = self.parent.frame.size.height/2-110;
    self.frame = CGRectMake(frame.origin.x ,frame.origin.y, [[UIScreen mainScreen]bounds].size.width/2, [[UIScreen mainScreen]bounds].size.height/2);
    self.center =  CGPointMake([[UIScreen mainScreen]bounds].size.width/2, [[UIScreen mainScreen]bounds].size.height/2);
    self.frame = frame;
}
-(void) styleHelp
{
    [theme styleHeaderThird:self];
    
}

@end
