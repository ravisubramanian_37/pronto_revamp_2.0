//
//  ZMEmpty.h
//  Pronto
//
//  Created by Andres Ramirez on 3/3/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Show a message when the user does't selected any Asset or Fracnhise
 */

@interface ZMEmpty : UIView

/**
 *  Message to show
 */

@property (weak, nonatomic) IBOutlet UILabel *message;

@property (weak, nonatomic) IBOutlet UILabel *subtitle;

/**
 *  Parent View
 */
@property (weak, nonatomic) UIView *parent;

- (id) initWithParent:(UIView*)parent;
- (id) initWithGlobalSearchAsParent:(UIView*)parent;
- (id) initWithEventScreenAsParent:(UIView*)parent;

-(void) centerMessage;
-(void) updateMessage;

@end
