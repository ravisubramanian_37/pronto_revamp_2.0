//
//  ZMHeader.m
//  Pronto
//
//  Created by Andres Ramirez on 2/3/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMHeader.h"
#import "ZMBadge.h"
#import "ZMProntoTheme.h"
#import "ZMMessages.h"
#import "ZMCategoryMenu.h"
#import "ZMSortMenu.h"
#import "ZMLibrary.h"
//Revamp(1/12) - Import tabbar classes
#import "Competitors.h"
#import "ToolsTraining.h"
#import "EventsViewController.h"
//End of Revamp
#import "ZMNotifications.h"
#import "AppDelegate.h"
#import "ZMAssetViewController.h"
#import "ZMInfoView.h"
#import "ZMTracking.h"
#import "ZMBrightcove.h"
#import "ZMHelp.h"
#import "Pronto-Swift.h"
// New Search Implementation
#import "ZMSearchMenu.h"
#import "Franchise+CoreDataProperties.h"
#import "UIImage+ImageDirectory.h"
#import "ZMAbbvieAPI.h"
#import "FavFranchiseAsset.h"
#import "Settings.h"
#import "Constant.h"
#import "GlobalSearchVC.h"
//#import "ZMPDFDocument.h"
#import "EventCoreDataManager.h"

@class ZMProntoManager;

@interface ZMHeader ()
{
    id <ZMProntoTheme> theme;
    ZMBadge *badge;
    long initDate;
    __weak IBOutlet UIImageView *browseIcon;
    __weak IBOutlet UIView *lineDividerPannel;
    __weak IBOutlet UIImageView *prontoLogo;
    __weak IBOutlet UIButton *whatIsButton;
    __weak IBOutlet UIView *buttonWrapper;
    __weak IBOutlet UIButton *infoIcon;
    __weak IBOutlet UILabel *messageLabel;
    __weak IBOutlet UILabel *helpLabel;
    
    __weak IBOutlet UIView *lineDividerIcons;
    // New Search Implementation
    __weak IBOutlet UIView *lineDividerUser;
    int counter;
    UITextField* alertTextField;
}

@property (nonatomic, weak) ZMCategoryMenu *categoryMenu;
@property (nonatomic, weak) ZMSortMenu *sortMenu;
// New Revamping
@property (nonatomic, strong) UILabel* franchiseLabel;

// New Seatch Implementation
@property (nonatomic, strong) ZMSearchMenu *searchMenu;
@property BOOL clearSearch;
//Definition for global saving of screen size
@property CGFloat screenWidth;
@property CGFloat screenHeight;

//@property (nonatomic) id senderForSearch;
@property (nonatomic) BOOL isSearchMenuPresented;
@property (nonatomic) id senderForSearchMenu;

@property (nonatomic, strong) UIView* statusBar;

@end


@implementation ZMHeader
@synthesize briefcaseMenu, detail,counter;


/**
 *
 **/
-(void) initWithOptions:(UIView *)container{
    self.container = container;
    
    _arrow.hidden = YES;
    
    if (self.container){
        
        CGRect frame = self.frame;
        frame.size.width = self.container.frame.size.width;
        frame.size.height = self.container.frame.size.height;
        self.frame = frame;
        
        theme = [ZMProntoTheme sharedTheme];
        [self styleHeader];
        [self.container addSubview:self];
        
        // floating icon issue fix
        //[self.searchTextbox setRightViewMode:UITextFieldViewModeAlways];
        self.searchTextbox.rightView = _lensButton;
        [self.searchTextbox setRightViewMode:UITextFieldViewModeUnlessEditing];
        [self.searchTextbox setClearButtonMode:UITextFieldViewModeWhileEditing];
        // New Search Implementation
         _isMakingService = NO;
        
        
        [self changeOrientation];
        
    }
    [self prepareHeaderLibrary];
    
}


-(void) prepareHeaderLibrary
{
    badge = [[ZMBadge alloc] initWithOptions:@"0" parent:_messagesButton franchise:self.userFranchise.text];
    badge.header = self;
    
    [self showHideBadge:YES];
    self.searchTextbox.delegate = self;
    [self.searchTextbox addTarget:self action:@selector(searchText:) forControlEvents: UIControlEventEditingDidEndOnExit];
    
    // New Search Implementation
    _keyWordArray = [[NSMutableArray alloc] initWithCapacity:5];
    counter = 0;
    
  
}


/**
 *
 **/
- (void)initDetailHeaderWithOptions:(UIView *)container{
    self.container = container;
    
    if (self.container) {
        CGRect frame = self.frame;
        frame.size.width = self.container.frame.size.width;
        frame.size.height = self.container.frame.size.height;
        self.frame = frame;
        
        [self.searchTextbox becomeFirstResponder];
        
        theme = [ZMProntoTheme sharedTheme];
        [self styleHeaderDetail];
        
        [self.container addSubview:self];
    }
    
    [self initTracking];
}



-(void) initTracking
{
    //Engament tracking
    initDate = (long)[[NSDate date] timeIntervalSince1970];
}


/**
 *
 **/
-(void) initDetailHeaderWithOptions:(UIView *)container withFrame:(CGRect)frame
{
    self.container = container;
    if (self.container){
        self.frame = frame;
        theme = [ZMProntoTheme sharedTheme];
        [self styleHeaderDetail];
        
        [self.container addSubview:self];
    }
}



-(void) styleHeader
{
    //Top header
    [theme styleHeader:self.UserInformation];
    [theme tintPrimaryButton:_messagesButton];
    [theme tintPrimaryButton:_helpButton];
    //Revamp - Lens button tint change
    [theme tintPrimaryButton:_lensButton];
    //End of Revamp
    [theme toggleButtonSelected:_briefcaseButton];
    [theme toggleButton:_libraryButton];
    //[theme notificationsArrow:arrow];
    [theme tintProntoLogoCard:browseIcon];
    [theme stylePrimaryView:lineDividerPannel];
    [theme tintPrimaryButton:infoIcon];
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"logo frame:%@", NSStringFromCGRect(prontoLogo.frame)]];
    [self updateProntoLogo];
   // [theme logoImage:prontoLogo];

    [theme searchBoxContainer:_searchBoxContainer];
    
    
    //Browse
    [theme styleHeaderSecond:self.Browse];
    [theme stylePrimaryButton:self.resetButton];
    [theme styleHeaderThird:self.Search];
    [theme styleSecondaryButton:self.folderButton];
}

//Revamp(7/12) - Adjust logo
-(void) updateProntoLogo {
    // New Revamping changes
    if([self.userFranchise.text  isEqual: @"In House"] || [self.userFranchise.text  isEqual: @"Home Office"]) {
        //Revamp(1/12) - If HO
        CGRect frame = prontoLogo.frame;
        frame.size.width = 90;
        frame.size.height = 35;
        prontoLogo.frame = frame;
        self.userFranchise.hidden = YES;
        //End of Revamp
        [theme logoImageHO:prontoLogo];
    }
    else {
        //Revamp(1/12) - If FG
        CGRect frame = prontoLogo.frame;
        frame.size.width = 45;
        frame.size.height = 15;
        prontoLogo.frame = frame;
        self.userFranchise.hidden = NO;
        //End of Revamp
        [theme logoImageFG:prontoLogo];
    }
    //Revamp(13/12) - Update badge colour
    [theme styleBadge:badge franchise:self.userFranchise.text];
    //End of Revamp
}

-(void) styleHeaderDetail
{
    //Top header
    [theme styleHeader:self.UserInformation];
    [theme tintButton:_messagesButton];
    [theme tintButton:_helpButton];
    //Revamp - Lens button tint change
    [theme tintButton:_lensButton];
    //End of Revamp
    [theme toggleButtonSelected:_briefcaseButton];
    [theme toggleButton:_libraryButton];
    //[theme tintArrow:arrow];
    [theme tintProntoLogoCard:browseIcon];
    [theme toggleButton:_addToBriefcase];
    [theme logoImage:prontoLogo];
    [theme tintPrimaryButton:whatIsButton];
}


- (void)changeTitleSize {

    CGRect frame;
    _documentName.text = @"";
    frame = _documentName.frame;
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2) {
        frame.size.width = 320;
    }
    else {
        frame.size.width = 570;
    }
    _documentName.frame = frame;
    _documentName.text = _asset.title;
    [_documentName sizeThatFits:frame.size];
    
    // floating icon issue fix
    self.searchTextbox.rightView = _lensButton;
    
    [self.searchTextbox setRightViewMode:UITextFieldViewModeUnlessEditing];
    [self.searchTextbox setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    
}


- (void)setDisplayName:(NSString *)displayName {
    _displayName = displayName;
    int charWidthDisplayName = 9; //Width of the char for helvetica for the display name label
    int charWidthFranchiseName = 9; //Width of the char for helvetica for the franchise label
    int padding = 5;
    CGRect frame = _userName.frame;
    CGRect frameUserFranchise = _userFranchise.frame;
    CGRect frameButtonWrapper = buttonWrapper.frame;
    frame.size.width = _displayName.length * charWidthDisplayName;
    frameUserFranchise.size.width = _userFranchise.text.length * charWidthFranchiseName;
    _userName.frame = frame;
    _userFranchise.frame = frameUserFranchise;
    frameButtonWrapper.origin.x = MAX(frameUserFranchise.size.width, frame.size.width) + frameUserFranchise.origin.x + padding;
    buttonWrapper.frame = frameButtonWrapper;

    
}

/*!
 *Rules for displaying folderbutton
 */
- (void) setHeaderState {
    
    // PRONTO-10 - Button Visibility not Consistent
    [[ZMUserActions sharedInstance] setFranchisesAndBrands:[ZMProntoManager sharedInstance].franchisesAndBrands];
    
    //If is the default view the button should be hidden
    _resetButton.hidden = YES;
    _folderButton.hidden = YES;

    if (_library.section == ProntoSectionTypeLibrary) {
        
        if ([[ZMProntoManager sharedInstance].search getTitle].length > 0) {
            //Changed/Commented:Added by Buvana: 30-Sep-2016
            //Comments:
            // PRONTO-11 - Button Visibility not Consistent
            if (_library.actions.isAllCategorySelected || ![_categoryButton.titleLabel.text isEqualToString:@"Category:  None Selected"]) {
            // if (_library.actions.isAllCategorySelected) {
            //End of fix
                if (_library.actions.assetsCount > 0) {
                    _folderButton.hidden = NO;
                    _resetButton.hidden = NO;
                    [_folderButton setTitle:@"Save results as Folder" forState:UIControlStateNormal];
                }
            }
        }else{
            
            // PRONTO-10 - Button Visibility not Consistent
            if ([ZMProntoManager sharedInstance].resetFranchiseIsSelected && ![ZMUserActions sharedInstance].isDefault){
                if (_library.actions.assetsCount > 0) {
                    _folderButton.hidden = NO;
                    _resetButton.hidden = NO;
                    [_folderButton setTitle:@"Save results as Folder" forState:UIControlStateNormal];
                }
            }
         
        
        }
        
        
    }
    
    if (!_library.actions.isDefaultSort) {
        _resetButton.hidden = NO;
    }
    // PRONTO-10 - Button Visibility not Consistent
    else if (_resetButton.hidden && _library.actions.isDefaultSort && [ZMUserActions sharedInstance].isDefault)
    {
        _resetButton.hidden = YES;
        
    }
    else if(![ZMUserActions sharedInstance].isDefault && _library.actions.assetsCount > 0)
    {
        _resetButton.hidden = NO;
        _folderButton.hidden = NO;
    }
    
    if(_library.section == ProntoSectionTypeBriefcase || _library.section == ProntoSectionTypeBriefcaseFolderDetail) {
        _folderButton.hidden = NO;
        // New Search Implementation - hiding search bar in MyBriefcase
        _searchTextbox.hidden = YES;
        _searchBoxContainer.hidden = YES;
    }
    else
    {
        _searchTextbox.hidden = NO;
        _searchBoxContainer.hidden = NO;
    }
    // New Search Implementation
    [self changeOrientation];
    
    ZMAbbvieAPI* apiManager = [ZMAbbvieAPI sharedAPIManager];
    if (theme == nil) {
        theme = [ZMProntoTheme sharedTheme];
    }
    UIColor *color = [theme whiteFontColor];
    self.searchTextbox.backgroundColor = [UIColor clearColor];
    self.searchTextbox.font = [theme normalLato];
    if(apiManager.isConnectionAvailable && !apiManager._syncingCompletion)
    {
        self.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Loading Assets..." attributes:@{NSForegroundColorAttributeName: color}];
        self.searchTextbox.textColor = color;
        //Revamp(15/12) - Search white
        [_lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
        self.searchTextbox.text = @"";
        [ZMUserActions sharedInstance].enableSearch = NO;
        _searchTextbox.enabled = NO;
        [self hidingIndicator];
    }
    else
    {
        _searchTextbox.enabled = YES;
        self.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color}];
        //Revamp(15/12) - Search white
        [_lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
        self.searchTextbox.textColor = color;
        [ZMUserActions sharedInstance].enableSearch = YES;
    }
}

- (void)enableSearchIconWithDefaultValues
{
    UIColor *color = [theme whiteFontColor];
    _searchTextbox.enabled = YES;
    self.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color}];
    [_lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
    self.searchTextbox.textColor = color;
    [ZMUserActions sharedInstance].enableSearch = YES;
}

- (void)setAsset:(Asset *)asset{
    
    _asset = asset;

    // PRONTO-1 - Likes not Working
    
    //New Like implementation
    [self._likeButton setImage:[UIImage imageNamed:@"likePressed"] forState:UIControlStateNormal];
    
    if ([_asset.user_vote isEqualToString:@"100"]) {
        self._likeButton.tag = 101;
        
    }
    else  {
        self._likeButton.tag = 100;
        [self._likeButton setImage:[UIImage imageNamed:@"likeNormal"] forState:UIControlStateNormal];
        
    }
    
    [self._likeButton setImage:[UIImage imageNamed:@"likePressed"] forState:UIControlStateNormal];
    if (detail.asset.votes.intValue > 0) {
        self._likeButton.tag = 101;
    }else if (detail.asset.votes.intValue == 0) {
        self._likeButton.tag = 100;
        [self._likeButton setImage:[UIImage imageNamed:@"likeNormal"] forState:UIControlStateNormal];
    }
    
    
    //Pronto new revamp chnages: Handle like action based on getFavFranchise but previously it was based on asset.votes
    [self._likeButton setImage:[UIImage imageNamed:@"likeNormal"] forState:UIControlStateNormal];
    self._likeButton.tag = 100;
    NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchise];
    for(FavFranchise *fav in favfranchise) {
        if(detail.asset.assetID.longValue == [fav.asset_ID longValue]) {
            self._likeButton.tag = 101;
            [self._likeButton setImage:[UIImage imageNamed:@"likePressed"] forState:UIControlStateNormal];
        }
    }
    // End:
}


-(void) addBreadCumb:(NSArray *)selection total:(int)total
{
    
    int i = 0;
    NSMutableString *breadCumb = [[NSMutableString alloc] init];
    NSString *separator;
    NSArray *brands;
    NSMutableArray *brandsRanges = [[NSMutableArray alloc] init];
    NSMutableDictionary *brandRage;
    NSRange totalRage;
    int brandCount;
    
    NSString *itemString = (total == 1)?@"Asset":@"Assets";
    
    //Start creating the bread cumb
    for (; i<[selection count]; i++)
    {
        if(_showResumeSelection)
        {
            if(i > 0) {
                breadCumb = [[NSMutableString alloc] initWithString:@"Multiple Selection "];
                //Changed:Added by Buvana: 30-Sep-2016
                // PRONTO-12 - Folder Creation and Search
                if (_library.section == ProntoSectionTypeLibrary) {
                    _folderButton.hidden = NO;
                    [_folderButton setTitle:@"Save results as Folder" forState:UIControlStateNormal];
                }
                //End of fix/
                break;
            }
        }
        brandCount = 0;
        separator = (i == [selection count]-1) ? @"":@"| ";
        [breadCumb appendString:[NSString stringWithFormat:@"%@ ", [[selection objectAtIndex:i] objectForKey:@"name"]]];
        brands = [[selection objectAtIndex:i] objectForKey:@"brands"];
        brandRage = [[NSMutableDictionary alloc] init];
        brandRage[@"from"] = [NSNumber numberWithInt:(int)[breadCumb length] - 1];
        for(int u = 0; u<[brands count]; u++)
        {
            [breadCumb appendString:[NSString stringWithFormat:@"%@ | ", [[brands objectAtIndex:u] objectForKey:@"name"]]];
            brandCount += [[[brands objectAtIndex:u] objectForKey:@"name"] length] + 3;
        }
        brandRage[@"to"] = [NSNumber numberWithInt:brandCount];
        [brandsRanges addObject:brandRage];
        [breadCumb appendString:separator];
    }
   
    /// Revamp - Franchise Count
    NSString *totalAsset = [NSString stringWithFormat:@"%@ (%d %@)", @"Franchise ", total, itemString];
    _library.franchiseCount.text = totalAsset;
    // New Revamping Changes - Font change
    if (_library.franchiseCount.text.length > 0) {
        _library.franchiseCount.font = [theme bigNormalLato];
        _library.franchiseCount.textColor = [theme franchiseCountColor];
         NSRange range = [_library.franchiseCount.text rangeOfString:@"Franchise "];
        NSMutableAttributedString *attText = [[NSMutableAttributedString alloc] initWithString:_library.franchiseCount.text];
        [attText setAttributes:@{NSFontAttributeName:[theme bigBoldLato]}
                                range:range];
         _library.franchiseCount.attributedText = attText;
    }
    NSMutableAttributedString *attributedText;
    NSRegularExpression *expression;
    
    if([selection count] == 0){
        breadCumb = [[NSMutableString alloc] initWithString:@"No Selection "];
    }
    
    if(![breadCumb isEqualToString:@"Multiple Selection "] && ![breadCumb isEqualToString:@"No Selection "])
    {
        
        totalRage = NSMakeRange([breadCumb length], [totalAsset length]);
        [breadCumb appendString:totalAsset];
        
        attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithString:breadCumb]
                                               attributes:[theme breadCumbGeneralStyle]];
        [attributedText setAttributes:[theme breadCumbTotalStyle] range:totalRage];
        
        //Loop into the brand ranges to add the corresponding styling
        for (i = 0; i<[brandsRanges count]; i++)
        {
            [attributedText setAttributes:[theme breadCumbBrandStyle] range:NSMakeRange([[[brandsRanges objectAtIndex:i] objectForKey:@"from"] integerValue],
                                                                                        [[[brandsRanges objectAtIndex:i] objectForKey:@"to"] integerValue])];
        }
        expression = [NSRegularExpression regularExpressionWithPattern:@"(\\|)" options:0 error:nil];
        [expression enumerateMatchesInString:attributedText.string options:0 range:NSMakeRange(0,[attributedText.string length])
                                  usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                      NSRange pipeRange = [result rangeAtIndex:1];
                                      
                                      [attributedText setAttributes:[theme breadCumbPipeStyle] range:pipeRange];
                                  }];
    }
    else {
        totalRage = NSMakeRange([breadCumb length], [totalAsset length]);
        [breadCumb appendString:totalAsset];
        attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithString:breadCumb]
                                                                attributes:[theme breadCumbGeneralStyle]];
        [attributedText setAttributes:[theme breadCumbTotalStyle] range:totalRage];
    }
    
    [_franchise setAttributedText:attributedText];
}



-(void) formatButton:(NSString *)stringPattern withText:(NSString *)labelText andButton:(UIButton *)button
{
    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:stringPattern options:0 error:nil];
    [expression enumerateMatchesInString:labelText options:0 range:NSMakeRange(0,[labelText length])
        usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        NSRange hashRange = [result rangeAtIndex:1];
            
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithString:labelText] attributes:[theme sortButtonTitleStyle]];
            [attributedText setAttributes:[theme buttonTitleMainStyle] range:hashRange];
            [button setAttributedTitle:attributedText forState:UIControlStateNormal];
    }];
}


- (void)adjustButton:(UIButton *)button {
    if([button isEqual:_categoryButton]){
        
    }
}

-(void) hideView:(UIView *)view{
    view.hidden = true;
}

-(void) hideButton:(UIButton *)button{
    button.hidden = true;
}

-(void)displayNotificationMessage:(UIButton *)sender {
    [[self.searchMenuPopOver presentingViewController] dismissViewControllerAnimated:NO completion:NULL];
    if([ZMUserActions sharedInstance].globalSearchViewController)
    {
        [_searchTextbox endEditing:YES];
    }
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:didTapNotificationButton:)])
    {
        [self.delegate header:self didTapNotificationButton:sender];
        return;
    }
    
    //Revamp(1/12) - Notifications for all tabs
//    UIView *view = _library.view;
//    if (_tabBarTag == SelectedTabFranchise) {
//        [_library displayNotifications:view];
//    } else if (_tabBarTag == SelectedTabCompetitors) {
//        [_competitors displayNotifications];
//    } else if (_tabBarTag == SelectedTabToolsTraining) {
//        [_tools displayNotifications];
//    } else if (_tabBarTag == SelectedTabEvents) {
////        [self.events displayNotifications];
//    }
    //End of Revamp
}

- (IBAction)displayMessages:(UIButton *)sender {
    
    [self displayNotificationMessage:sender];
    
}

- (IBAction)help:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:didTapHelpButton:)])
    {
        [self.delegate header:self didTapHelpButton:sender];
        return;
    }
    //Revamp(1/12) - Help for all tabs
    UIView *view = _library.view;
    if (_tabBarTag == SelectedTabFranchise) {
        [_library displayWalkthrough:view];
    } else if (_tabBarTag == SelectedTabCompetitors) {
        [_competitors displayWalkthrough];
    } else if (_tabBarTag == SelectedTabToolsTraining) {
        [_tools displayWalkthrough];
    } else if (_tabBarTag == SelectedTabEvents) {
//        [_events displayWalkthrough];
    }
    //End of Revamp
}


-(void) prepareHeaderForBriefcase
{
    // New Search Implementation Frame setting
    [UIView animateWithDuration:0.25 animations:^{
        _library.section = ProntoSectionTypeBriefcase;
        CGRect frame = _Browse.frame;
        CGRect searchFrame = _Search.frame;
        CGRect mainFrame = self.frame;
        frame.origin.y = 0;
        searchFrame.origin.y = 88;
        
        //Used to be mainFrame.size.height = mainFrame.size.height-80 but the -80 is not used for anything and it affect the pressing of the sort button
        mainFrame.size.height = mainFrame.size.height;
        _Browse.frame =  frame;
        _Search.frame = searchFrame;
        self.frame = mainFrame;
    }];
}

-(void) prepareHeaderForLibrary
{
    // New Search Implementation Frame setting
    [UIView animateWithDuration:0.25 animations:^{
        _library.section = ProntoSectionTypeLibrary;
        CGRect frame = _Browse.frame;
        CGRect searchFrame = _Search.frame;
        CGRect mainFrame = self.frame;
        searchFrame.origin.y = 140;
        frame.origin.y = 88;
        mainFrame.size.height = mainFrame.size.height + 88;
        _Browse.frame =  frame;
        _Search.frame = searchFrame;
        self.frame = mainFrame;
    }];
}


-(void) setBadgeNumber:(int)badgeNumber
{
    _badgeNumber = badgeNumber;
    NSString *badgeText = [NSString stringWithFormat:@"%d",_badgeNumber];
    if(_badgeNumber==0)
    {
        badgeText = @"";
        [self showHideBadge:YES];
    } else {
        [self showHideBadge:NO];
    }
    badge.label.text = badgeText;
}

//Revamp(14/12) - global function for hide and unhide button
-(void) showHideBadge:(BOOL)hide {
    if(hide) {
        [badge setHidden:YES];
    }
    else {
        if(_badgeNumber != 0)
            [badge setHidden:NO];
    }
}

#pragma mark - Actions

// New Search Implementation

- (IBAction)setTextFieldAsFirstResponder:(id)sender{
    [self.searchTextbox becomeFirstResponder];
}

- (IBAction)briefcase:(UIButton *)sender {
    
    [ZMProntoManager sharedInstance].currentLibraryView = MainViewMyBriefcase;
    
    if (_library.section != ProntoSectionTypeBriefcase && _library.section != ProntoSectionTypeBriefcaseFolderDetail) {
        
        [theme toggleButtonSelected:_libraryButton];
        [theme toggleButton:_briefcaseButton];
        [_folderButton setTitle:@"Create a folder" forState:UIControlStateNormal];
        
        [UIView animateWithDuration:0.25 animations:^{
            CGRect frame = _arrow.frame;
            frame.origin.x = _briefcaseButton.frame.origin.x + (_briefcaseButton.frame.size.width/2);
            _arrow.frame =  frame;
            [self prepareHeaderForBriefcase];
        }];
        
        _library.pageIndex = 0;
        [_library loadBriefcase];
    }
}

- (IBAction)library:(UIButton *)sender
{
    
    [ZMProntoManager sharedInstance].currentLibraryView = MainViewLibrary;
    NSString * searchText = [ZMUserActions sharedInstance].searchingText;
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"searchText:%@",searchText]];
    self.searchTextbox.text = searchText;
    
    if (searchText.length > 0) {
        [_folderButton setTitle:@"Save results as Folder" forState:UIControlStateNormal];
    }
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"_library.section:%u",_library.section]];
    
    if(_library.section != ProntoSectionTypeLibrary)
    {
//        if(![_library.headerView.searchTextbox.text isEqualToString:@""] && _library.section == ProntoSectionTypeBriefcaseFolderDetail){
//                [[ZMProntoManager sharedInstance].search getTitle];
//        }
        
        _folderButton.hidden = YES;
        [theme toggleButtonSelected:_briefcaseButton];
        [theme toggleButton:_libraryButton];

        [UIView animateWithDuration:0.25 animations:^{
         
            CGRect frame = _arrow.frame;
            frame.origin.x = _libraryButton.frame.origin.x + (_libraryButton.frame.size.width/2);
            _arrow.frame =  frame;
            [self prepareHeaderForLibrary];

            if (_library.headerDetailFolder) {
                [_library.headerDetailFolder removeFromSuperview];
                _library.headerDetailFolder = nil;
                _library.libraryBackToFolderDetail = YES;
            }
            
            self.searchTextbox.text = searchText;
           // [_library loadSeaLibrary];
            // New Search Implementation
            if (searchText.length >=3 )
                [_library.headerView textFieldDidChange:self.library.headerView.searchTextbox];
               // self.library.headerView.textFieldDidChange(self.library.headerView.searchTextbox)
                //[_library loadSearchedAssets];
            else
                 [_library loadLibrary];
        }];
    }
}

/**
 *  Returns the header to library state
 */
-(void) backToLibraryState
{
    if(_library.section != ProntoSectionTypeLibrary)
    {
        [theme toggleButtonSelected:_briefcaseButton];
        [theme toggleButton:_libraryButton];
        [_folderButton setTitle:@"Save results as Folder" forState:UIControlStateNormal];
        CGRect frame = _arrow.frame;
        frame.origin.x = _libraryButton.frame.origin.x + (_libraryButton.frame.size.width/2);
        _arrow.frame =  frame;
        
        _library.section = ProntoSectionTypeLibrary;
        frame = _Browse.frame;
        CGRect searchFrame = _Search.frame;
        CGRect mainFrame = self.frame;
        searchFrame.origin.y = 132;
        frame.origin.y = 80;
        mainFrame.size.height = mainFrame.size.height + 100;
        _Browse.frame =  frame;
        _Search.frame = searchFrame;
        self.frame = mainFrame;
    }
}


- (IBAction)reset:(UIButton *)sender {
    
//    [self resetSelectedSort];
    [self resetTrackingOptions];
    
    /*UIAlertView *confirm = [[UIAlertView alloc] initWithTitle:@"Please Confirm"
                                                      message:@"The franchises, brands and sort you have previously selected will be reset. Are you ok with this?"
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"OK", nil];
    confirm.tag = 2;
    [confirm show];*/
    // Alternative AlertView
    UIAlertController* confirm = [UIAlertController alertControllerWithTitle:@"Please Confirm"  message:@"The franchises, brands and sort you have previously selected will be reset. Are you ok with this?"  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle Cancel button
                                   }];
    UIAlertAction* saveButton = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action) {
                                     //Handle Save button
                                     
                                     [self performSelector:@selector(entireReset) withObject:nil afterDelay:1.0];
                                     
                                 }];
    
    [confirm addAction:cancelButton];
    [confirm addAction:saveButton];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    [viewController presentViewController:confirm animated:YES completion:nil];


}

-(void) entireReset
{
    [self alertViewTag:2 clickedButtonAtIndex:1];
}

- (void)resetTrackingOptions{
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject:@"filter|franchise:brand" forKey:@"scfiltercategory"];
    [trackingOptions setObject:[[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
    [trackingOptions setObject:@"reset" forKey:@"scfilteraction"];
    [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    [trackingOptions setObject:[NSString stringWithFormat: @"%d", [ZMUserActions sharedInstance].assetsCount] forKey:@"scfilterresultcount"];
    [trackingOptions setObject:([ZMUserActions sharedInstance].assetsCount>0)?@"successful":@"unsuccessful" forKey:@"scsearchaction"];
    [trackingOptions setObject:[[ZMProntoManager sharedInstance].search getTitle] forKey:@"scsearchterm"];
    [trackingOptions setObject:[_library getSelectedSort] forKey:@"scsortaction"];
    [trackingOptions setObject:[_library getCategoriesForTracking] forKey:@"scsortcategory"];
    [trackingOptions setObject:@"single" forKey:@"scsorttype"];
    
    /**
     * @Tracking
     * Reset Action
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section
              withSubsection:@"filter"
                    withName:[NSString stringWithFormat:@"brand-%@-reset", [ZMUserActions sharedInstance].franchiseName]
                 withOptions:trackingOptions];
}

- (void)resetSelectedSort{
    
    for (int i = 0; i < self.sortMenu.sortArray.count; i++) {
        NSMutableDictionary *initElement = [[NSMutableDictionary alloc] initWithDictionary:[self.sortMenu.sortArray objectAtIndex:i]];
        [initElement setObject:@"0" forKey:@"selected"];
        
        if (i == 0) {
            [initElement setObject:@"1" forKey:@"selected"];
        }
        
        [self.sortMenu.sortArray replaceObjectAtIndex:i withObject:initElement];
    }
    [self.sortMenu.sortMenuTableView reloadData];
}

- (void)resetHeader{
    
    [self resetSelectedSort];
    [self resetTrackingOptions];
    [_library backToDefault];
}


- (IBAction)searchText:(id *)sender
{
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    
    if (_library.section == ProntoSectionTypeBriefcase || _library.section == ProntoSectionTypeBriefcaseFolderDetail) {
        [trackingOptions setObject:@"my briefcase" forKey:@"scsearchtype"];
    }
    else {
        [trackingOptions setObject:@"library" forKey:@"scsearchtype"];
    }
    
    [_library.view endEditing:YES];
    _library.pageIndex = 0;
   
    /**
     * @Tracking
     * Search Action
     **/
    
    [trackingOptions setObject:@"filter|franchise:brand" forKey:@"scfiltercategory"];
    [trackingOptions setObject:[[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
    [trackingOptions setObject:@"set" forKey:@"scfilteraction"];
    [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    [trackingOptions setObject:[NSString stringWithFormat: @"%d", [ZMUserActions sharedInstance].assetsCount] forKey:@"scfilterresultcount"];
    [trackingOptions setObject:([ZMUserActions sharedInstance].assetsCount>0)?@"successful":@"unsuccessful" forKey:@"scsearchaction"];
    [trackingOptions setObject:[[ZMProntoManager sharedInstance].search getTitle] forKey:@"scsearchterm"];
    // empty string check for resolving crash
    NSString* selectedSort = [_library getSelectedSort] ;
    if(selectedSort.length)
        [trackingOptions setObject:[_library getSelectedSort] forKey:@"scsortaction"];
    else
        [trackingOptions setObject:@"" forKey:@"scsortaction"];
    NSString* selectedCategories = [_library getCategoriesForTracking];
    if(selectedCategories.length > 0)
        [trackingOptions setObject:selectedCategories forKey:@"scsortcategory"];
    else
        [trackingOptions setObject:@"" forKey:@"scsortcategory"];
    
    if (([ZMUserActions sharedInstance].assetsCount==0)) {
        
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                  withSubsection:@"search"
                        withName:[NSString stringWithFormat:@"%@search-unsuccessful", [ZMUserActions sharedInstance].section]
                     withOptions:trackingOptions];
    }
    else {
        
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                  withSubsection:@"search"
                        withName:[NSString stringWithFormat:@"%@search-successful", [ZMUserActions sharedInstance].section]
                     withOptions:trackingOptions];
    }
}

- (IBAction)showHelpDetail:(UIButton *)sender{
    
    if(detail){
        
        ZMHelp *help = (ZMHelp *)[[[NSBundle mainBundle] loadNibNamed:@"ZMHelpDetail" owner:self options:nil] objectAtIndex:0];
        help.frame = detail.view.frame;
        
        if (_asset) {
            if ([[_asset.uri_full pathExtension] isEqualToString:@"pdf"] || [[_asset.uri_full pathExtension] isEqualToString:@"PDF"]) {
                [help initScrollView:1];
                // PRONTO-6 Help Screen Bug during Orientation
                [help set_isDocument:YES];
            }
            else {
                [help initScrollView:2];
                // PRONTO-6 Help Screen Bug during Orientation
                [help set_isDocument:NO];
            }
        }
        else {
            [help initScrollView:3];
            [help set_isDocument:NO];
        }
        
        [detail.view addSubview:help];
    }
}

- (IBAction)category:(UIButton *)sender {
    
    _categoryMenu = (ZMCategoryMenu *)[[[NSBundle mainBundle] loadNibNamed:@"CategoryMenu" owner:self options:nil] objectAtIndex:0];

    _categoryMenu.categoriesArray = [[NSMutableArray alloc] initWithArray:_library.actions.categories];
    _categoryMenu.library = _library;
    
    // removing deprecated warnings
    _categoryMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    _categoryMenu.popoverPresentationController.sourceRect = sender.frame;
    _categoryMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomPopoverBackgroundView class];
    [self.library presentViewController:_categoryMenu animated:YES completion:nil];
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"categoriesToFilter:%@",[ZMProntoManager sharedInstance].categoriesToFilter]];
    
    if([ZMProntoManager sharedInstance].categoriesToFilter.count > 0){
        
        BOOL showCategories = NO;
        
        if ([ZMProntoManager sharedInstance].categoriesToFilter.count == 1){
            NSPredicate * predicateCategories = [NSPredicate predicateWithFormat:@"name == %@", @"All Franchise Assets"];
            NSArray * selectedAllCategories = [[ZMProntoManager sharedInstance].categoriesToFilter filteredArrayUsingPredicate:predicateCategories];
            
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"selectedAllCategories:%@",selectedAllCategories]];
            
            if (selectedAllCategories.count == 0) {
                showCategories = YES;
            }        
        }else{
            showCategories = YES;
        }
        
        if (showCategories) {
            
            // removing deprecated warnings
            // configure the Popover presentation controller
            _categoryPopOver = [_categoryMenu popoverPresentationController];
            _categoryPopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
            _categoryPopOver.delegate = self;
            _categoryPopOver.sourceView = sender.superview;
            _categoryPopOver.sourceRect = CGRectMake(sender.frame.origin.x+15.0,_sortButton.frame.origin.y-150.0,200.0, 175.0);
            
        }
        
    }
    else
    {
        // New Revamping Implementation
        NSMutableArray* categoriesSet = [[NSMutableArray alloc] init];
        [categoriesSet addObject:@{@"id":@"0", @"name":@"All Franchise Assets", @"selected":@"1"}];
        [categoriesSet addObject:@{@"id":@"1", @"name":@"Franchise Favorites", @"selected":@"0"}];
        if(_library.brandsArray.count > 1)
        {
            int k = 2;
            for(int g = 0; g<_library.brandsArray.count;g++)
            {
                [categoriesSet addObject:@{@"id":[NSString stringWithFormat:@"%d",k], @"name":[NSString stringWithFormat:@"%@",[_library.brandsArray objectAtIndex:g]], @"selected":@"0"}];
                
                k = k+1;
            }
        }
                 
        
        [ZMProntoManager sharedInstance].categoriesToFilter = categoriesSet;
        [self category:nil];
        
    }
}
    
 
- (NSArray*)categoryList {
    
    _categoryMenu = (ZMCategoryMenu *)[[[NSBundle mainBundle] loadNibNamed:@"CategoryMenu" owner:self options:nil] objectAtIndex:0];
    
    _categoryMenu.categoriesArray = [[NSMutableArray alloc] initWithArray:_library.actions.categories];
    _categoryMenu.library = _library;
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"categoriesToFilter:%@",[ZMProntoManager sharedInstance].categoriesToFilter]];
    
    if([ZMProntoManager sharedInstance].categoriesToFilter.count > 0){
        
//        BOOL showCategories = NO;
        
        if ([ZMProntoManager sharedInstance].categoriesToFilter.count == 1){
            NSPredicate * predicateCategories = [NSPredicate predicateWithFormat:@"name == %@", @"All Franchise Assets"];
            NSArray * selectedAllCategories = [[ZMProntoManager sharedInstance].categoriesToFilter filteredArrayUsingPredicate:predicateCategories];
            
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"selectedAllCategories:%@",selectedAllCategories]];
            
            if (selectedAllCategories.count == 0) {
//                showCategories = YES;
            }
        }
        
    }
    else
    {
        // New Revamping Implementation
        NSMutableArray* categoriesSet = [[NSMutableArray alloc] init];
        [categoriesSet addObject:@{@"id":@"0", @"name":@"All Franchise Assets", @"selected":@"1"}];
         [categoriesSet addObject:@{@"id":@"1", @"name":@"Franchise Favorites", @"selected":@"0"}];
        if(_library.brandsArray.count > 1)
        {
            int k = 2;
            
            for(int g = 0; g<_library.brandsArray.count;g++)
            {
                [categoriesSet addObject:@{@"id":[NSString stringWithFormat:@"%d",k], @"name":[NSString stringWithFormat:@"%@",[_library.brandsArray objectAtIndex:g]], @"selected":@"0"}];
                k = k+1;
                
            }
        }
        [ZMProntoManager sharedInstance].categoriesToFilter = categoriesSet;
       // [self category:nil];
        
    }
    
    return [ZMProntoManager sharedInstance].categoriesToFilter;
}

- (void)adjustHeader
{
    // removing deprecated APIs
    if(_sortMenuPopOver != nil && _sortMenuPopOver.presentationStyle == UIModalPresentationPopover)
    {
        // configure the Popover presentation controller
        _sortMenuPopOver = [_sortMenu popoverPresentationController];
        _sortMenuPopOver.permittedArrowDirections = 0;
        _sortMenuPopOver.delegate = self;
        //sourceView -> nil, It will cause the crash
//        _sortButton = nil;
        if (_sortButton.superview != nil)
        {
            _sortMenuPopOver.sourceView = _sortButton.superview;
        }
        self.sortMenu.preferredContentSize = CGSizeMake(_sortButton.frame.size.width - 2, [self.sortMenu.sortArray count]*44);
        _sortMenuPopOver.sourceRect = CGRectMake(_sortButton.frame.origin.x+(_sortButton.frame.size.width/2),_sortButton.frame.origin.y+104,1, 1);

    }
    //Detect the orientation of the device and change title size
    [self changeTitleSize];
    [self changeOrientation];
}


// New Search Implementation - UI Changes
-(void) changeOrientation
{
    //Revamp - Hide button line, arrow, library and briefcase button from header
    _screenWidth = [UIScreen mainScreen].bounds.size.width;
    _screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    _briefcaseButton.hidden = YES;
    _libraryButton.hidden = YES;
    lineDividerPannel.hidden = YES;
    //arrow.hidden = YES;
    _userName.hidden = YES;
    //_userFranchise.hidden = YES;
    
    CGRect logoFrame = prontoLogo.frame;
    logoFrame.origin.y = 0;
    prontoLogo.frame = logoFrame;
    
    //End of Revamp
        
    CGRect frame = _userName.frame;
    CGRect frameUserFranchise = _userFranchise.frame;
   
    _userName.font = [UIFont systemFontOfSize:13];
    _userFranchise.font = [UIFont boldSystemFontOfSize:20];
   
    
    float userWidthIs = 0.0;
    
    // Fix for [__NSPlaceholderDictionary initWithObjects:forKeys:count:]: attempt to insert nil object from objects[0]
    if (![_userName.text  isEqual: @""] && _userName.text != nil)
    {
        userWidthIs =   [_userName.text
                         boundingRectWithSize:_userName.frame.size
                         options:NSStringDrawingUsesLineFragmentOrigin
                         attributes:@{ NSFontAttributeName:_userName.font }
                         context:nil].size.width;
    }
    
    float widthIs = 0.0;
    if (![_userFranchise.text  isEqual: @""] && _userFranchise.text != nil)
    {
        widthIs = [_userFranchise.text
                   boundingRectWithSize:_userFranchise.frame.size
                   options:NSStringDrawingUsesLineFragmentOrigin
                   attributes:@{ NSFontAttributeName:_userFranchise.font }
                   context:nil].size.width;
    }
    
    
    // user name
    frame.origin.x =  [UIApplication sharedApplication].statusBarFrame.size.width - (userWidthIs + widthIs) - 45;
    frame.size.width = userWidthIs;
    _userName.frame = frame;
    
    
    //Revamp - Align header
    
    // Search Container Frame
    CGRect searchFrame = _searchBoxContainer.frame;
    CGRect searchTextFrame = _searchTextbox.frame;
    if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2)
    {
        searchFrame.size.width = 200;
        searchFrame.origin.x = 400;
        //Revamp(7/12) - For iPad Pro sizes
        if(_screenWidth == iPadPro10_width) {
            searchFrame.size.width = 250;
            searchFrame.origin.x = 415;
        } else if(_screenWidth == iPadPro12_width) {
            searchFrame.size.width = 340;
            searchFrame.origin.x = 520;
        }
        //End of Revamp
        searchFrame.size.height = 40;
        searchFrame.origin.y = 10;
        
        [_messagesButton setTitle:@" " forState:UIControlStateNormal];
        [_helpButton setTitle:@" " forState:UIControlStateNormal];
        [_profileButton setTitle:@" " forState:UIControlStateNormal];
    }
    else {
        searchFrame.size.width = 200;
        searchFrame.origin.x = 515;
        //Revamp(7/12) - For iPad Pro sizes
        if(_screenHeight == iPadPro10_width) {
            searchFrame.size.width = 250;
            searchFrame.origin.x = 550;
        } else if(_screenHeight == iPadPro12_width) {
            searchFrame.size.width = 370;
            searchFrame.origin.x = 680;
        }
        //End of Revamp
        searchFrame.origin.y = 10;
        searchFrame.size.height = 40;
        
        [_messagesButton setTitle:@"Messages" forState:UIControlStateNormal];
        [_helpButton setTitle:@"Help" forState:UIControlStateNormal];
        [_profileButton setTitle:@"Profile" forState:UIControlStateNormal];
    }
    searchTextFrame.size.width = searchFrame.size.width;
    self.searchBoxContainer.frame = CGRectMake(searchFrame.origin.x, searchFrame.origin.y,
                                               searchFrame.size.width, searchFrame.size.height);
    self.searchTextbox.frame = CGRectMake(searchTextFrame.origin.x, searchTextFrame.origin.y,
                                          searchTextFrame.size.width, searchTextFrame.size.height);
    
    // franchise name
    frameUserFranchise.origin.x = prontoLogo.frame.origin.x; //+frame.size.width+20;
    frameUserFranchise.origin.y = prontoLogo.frame.origin.y+prontoLogo.frame.size.height-3;
    frameUserFranchise.size.width = 360;
    frameUserFranchise.size.height = prontoLogo.frame.size.height*2;
    _userFranchise.frame = frameUserFranchise;
    
    // message badge frame
    CGRect badgeFrame = badge.frame;
    [theme notificationsArrow:_arrow];
    CGRect arrowFrame = _arrow.frame;
    if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2)
    {
        // lineDividerUser for username and franchise name
        [lineDividerUser setFrame:CGRectMake(_searchBoxContainer.frame.origin.x - 10,3,1,41)];
        
        // help button
        [_helpButton setFrame:CGRectMake(_searchBoxContainer.frame.origin.x + _searchBoxContainer.frame.size.width + 20, 0, 44, 44)];
        // help button divider
        [lineDividerIcons setFrame:CGRectMake(_searchBoxContainer.frame.origin.x + _searchBoxContainer.frame.size.width +10,3,1,41)];
        // message button
        [_messagesButton setFrame:CGRectMake(_helpButton.frame.origin.x + _helpButton.frame.size.width + 2, 0, 44, 44)];
        // profile button
        [_profileButton setFrame:CGRectMake(_messagesButton.frame.origin.x + _messagesButton.frame.size.width + 5, 0, 44, 44)];
        badgeFrame.origin.x = _messagesButton.frame.origin.x+22;
        arrowFrame.origin.x = _messagesButton.frame.origin.x;
    } else {
        
        // lineDividerUser for username and franchise name
        [lineDividerUser setFrame:CGRectMake(_searchBoxContainer.frame.origin.x - 10,3,1,50)];
        
        // help button
        [_helpButton setFrame:CGRectMake(_searchBoxContainer.frame.origin.x + _searchBoxContainer.frame.size.width + 15, 0, 80, 44)];
        // help button divider
        [lineDividerIcons setFrame:CGRectMake(_searchBoxContainer.frame.origin.x + _searchBoxContainer.frame.size.width + 10,3,1,50)];
        // message button
        [_messagesButton setFrame:CGRectMake(_helpButton.frame.origin.x + _helpButton.frame.size.width + 3, 0, 110, 44)];
        // profile button
        [_profileButton setFrame:CGRectMake(_messagesButton.frame.origin.x + _messagesButton.frame.size.width + 3, 0, 92, 44)];
        badgeFrame.origin.x = _messagesButton.frame.origin.x+24;
        arrowFrame.origin.x = _messagesButton.frame.origin.x;
    }
    
    badgeFrame.origin.y = 4;
    arrowFrame.origin.y = _UserInformation.frame.origin.y + _UserInformation.frame.size.height - _arrow.frame.size.height;
    badge.frame = badgeFrame;
    _arrow.frame = arrowFrame;
    
    //End of Revamp
}

-(void) adjustGlobalSearchIndicator
{
    self.indicatorView.center = CGPointMake([[UIScreen mainScreen]bounds].size.width/2, ([[UIScreen mainScreen]bounds].size.height - 100)/2);
}

-(void) showIndicatorFromGlobalSearch
{
    
    [AbbvieLogging logInfo:@"Indicator -> showIndicatorFromGlobalSearch"];
    if(_indicatorView != nil)
    {
        [_indicatorView removeFromSuperview];
    }
    
    // view
    _indicatorView = [[UIView alloc] initWithFrame:CGRectMake(75, 155, 100, 100)];
    _indicatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [self adjustGlobalSearchIndicator];
    _indicatorView.clipsToBounds = YES;
    _indicatorView.layer.cornerRadius = 10.0;
    
    // indicator
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    CGRect frame = CGRectMake(0, 0, _indicatorView.bounds.size.width, _indicatorView.bounds.size.height);
    _activityIndicatorView.frame = frame;
    if([ZMUserActions sharedInstance].enableSearch == YES)
    {
        [_indicatorView addSubview:_activityIndicatorView];
        //        [_library.view addSubview:_indicatorView];
        [_activityIndicatorView startAnimating];
        
        if (self.sourceViewController != nil)
        {
            [self.sourceViewController.view addSubview:_indicatorView];
        }
        else
        {
            [_library.view addSubview:_indicatorView];
        }
    }
    else
    {
        if(_indicatorView != nil)
        {
            [AbbvieLogging logInfo:@"Indicator -> showIndicatorFromGlobalSearch from else"];
            [_indicatorView removeFromSuperview];
        }
    }
    
//    _isShowingIndicator = YES;
}

-(void) showingIndicator
{
        if(_indicatorView != nil)
            [_indicatorView removeFromSuperview];
        
        // view
         _indicatorView = [[UIView alloc] initWithFrame:CGRectMake(75, 155, 100, 100)];
        _indicatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        // updated center
        _indicatorView.center = CGPointMake([[UIScreen mainScreen]bounds].size.width/2+248/2, [[UIScreen mainScreen]bounds].size.height/2);
        _indicatorView.clipsToBounds = YES;
        _indicatorView.layer.cornerRadius = 10.0;
        
        // indicator
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        CGRect frame = CGRectMake(0, 0, _indicatorView.bounds.size.width, _indicatorView.bounds.size.height);
        _activityIndicatorView.frame = frame;
     if([ZMUserActions sharedInstance].enableSearch == YES)
     {
        [_indicatorView addSubview:_activityIndicatorView];
//        [_library.view addSubview:_indicatorView];
        [_activityIndicatorView startAnimating];
         
         if (self.sourceViewController != nil)
         {
             [self.sourceViewController.view addSubview:_indicatorView];
         }
         else
         {
             [_library.view addSubview:_indicatorView];
         }
     }
    
         _isShowingIndicator = YES;
}

-(void) hidingIndicator
{

     dispatch_async(dispatch_get_main_queue(), ^(void){
         [AbbvieLogging logInfo:@"Indicator -> hidingIndicator"];
    [self.activityIndicatorView stopAnimating];
    [self.indicatorView removeFromSuperview];
    [self.activityIndicatorView removeFromSuperview];
    self.activityIndicatorView  = nil;
    self.indicatorView = nil;
    
    self.activityIndicatorView.hidden = YES;
    self.indicatorView.hidden = YES;
    
    NSArray *viewsToRemove = [self.indicatorView subviews];
    for (_indicatorView in viewsToRemove) {
        
        [self.activityIndicatorView removeFromSuperview];
        [self.indicatorView removeFromSuperview];
    }
     });
 
    
}

// New Search Implementation - Recent Search
- (IBAction)searchMenu:(UIButton *)sender {
    
    if (!self.searchMenu) {
        self.searchMenu = (ZMSearchMenu *)[[[NSBundle mainBundle] loadNibNamed:@"SearchMenu" owner:self options:nil] objectAtIndex:0];
        
        if (_tabBarTag == SelectedTabFranchise) {
            self.searchMenu.library = _library;
        } else if (_tabBarTag == SelectedTabCompetitors) {
            self.searchMenu.competitors = _competitors;
        } else if (_tabBarTag == SelectedTabToolsTraining) {
            self.searchMenu.tools = _tools;
        } else if (_tabBarTag == SelectedTabEvents) {
            self.searchMenu.events = self.events;
        }
        
        self.searchMenu.parent = self;
    }

    _keyWordArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Keywords"]];

    // display the entries in menu
    _searchMenu.sortArray =_keyWordArray;
    
    // 'UIPopoverController' is deprecated so using Popover presentation controller
    self.searchMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.searchMenu.popoverPresentationController.sourceRect = [sender frame];

    // configure the Popover presentation controller
    self.searchMenuPopOver = [_searchMenu popoverPresentationController];
    //Revamp(15/12) - Recent search alignnment
    self.searchMenuPopOver.permittedArrowDirections = 0;
    self.searchMenuPopOver.delegate = self;
    self.searchMenuPopOver.sourceView = [sender superview];
    self.searchMenuPopOver.sourceRect = CGRectMake(sender.frame.origin.x + 4,sender.frame.origin.y - 40,_searchTextbox.frame.size.width, 175.0);
    self.searchMenu.preferredContentSize = CGSizeMake(_searchTextbox.frame.size.width - 7, ([self.searchMenu.sortArray count]*44) + 25);
    
    self.isSearchMenuPresented = YES;
    self.senderForSearchMenu = sender;
    
    if (_tabBarTag == SelectedTabFranchise) {
        [self.library presentViewController:_searchMenu animated:YES completion:nil];
    } else if (_tabBarTag == SelectedTabCompetitors) {
        [self.competitors presentViewController:_searchMenu animated:YES completion:nil];
    } else if (_tabBarTag == SelectedTabToolsTraining) {
        [self.tools presentViewController:_searchMenu animated:YES completion:nil];
    } else if (_tabBarTag == SelectedTabEvents) {
        [self.events presentViewController:self.searchMenu animated:YES completion:nil];
    }
    else
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:didInitiateSearch:)])
        {
            [self.delegate header:self didInitiateSearch:self.searchMenu];
        }
    }

}



- (IBAction)sort:(UIButton *)sender {
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"buttonname:%@",sender.titleLabel.text]];
    _sortButton = sender;

    if (!self.sortMenu) {
        
        self.sortMenu = (ZMSortMenu *)[[[NSBundle mainBundle] loadNibNamed:@"SortMenu" owner:self options:nil] objectAtIndex:0];
        self.sortMenu.library = _library;
        self.sortMenu.parent = self;
        self.sortMenu.sortArray = _library.actions.sorts;
        
    }
    
    // 'UIPopoverController' is deprecated so using Popover presentation controller
    self.sortMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.sortMenu.popoverPresentationController.sourceRect = sender.frame;
    //self.sortMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomPopoverBackgroundView class];
    [self.sortMenu.library presentViewController:_sortMenu animated:YES completion:nil];
    // configure the Popover presentation controller
    _sortMenuPopOver = [self.sortMenu popoverPresentationController];
    _sortMenuPopOver.permittedArrowDirections = 0;
    _sortMenuPopOver.delegate = self;
    _sortMenuPopOver.sourceView = sender.superview;
    self.sortMenu.preferredContentSize = CGSizeMake(_sortButton.frame.size.width - 2, [self.sortMenu.sortArray count]*44);
    _sortMenuPopOver.sourceRect = CGRectMake(_sortButton.frame.origin.x+(_sortButton.frame.size.width/2),_sortButton.frame.origin.y+104,1, 1);
    
}

- (void)hidePopovers{
   
    // removing deprecated warnings
    [[self.settingsMenu presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    [[_sortMenu presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    [[self.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    
    
}


- (IBAction)addToBriefcaseAction:(UIButton *)sender
{
    if(!briefcaseMenu){
    
        briefcaseMenu = (ZMBriefcaseMenu *)[[[NSBundle mainBundle] loadNibNamed:@"FoldersMenu" owner:self options:nil] objectAtIndex:0];
        briefcaseMenu.asset = _asset;
        briefcaseMenu.delegate = self;
        briefcaseMenu.parent = self;
    }
    
   
    // 'UIPopoverController' is deprecated so using Popover presentation controller
    briefcaseMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    briefcaseMenu.popoverPresentationController.sourceRect = sender.frame;
    briefcaseMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomPopoverBackgroundView class];
    [briefcaseMenu.parent.library presentViewController:briefcaseMenu animated:YES completion:nil];
    
    // configure the Popover presentation controller
    _briefcasePopOver = [briefcaseMenu popoverPresentationController];
    _briefcasePopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
    _briefcasePopOver.delegate = self;
    _briefcasePopOver.sourceView = sender.superview;
    _briefcasePopOver.sourceRect = CGRectMake(sender.frame.origin.x-75,sender.frame.origin.y-130.0,200.0, 175.0);
}

-(void) folderCreated {
    _library.needRefresh = YES;

}


-(void) didSelectOption
{
     [[_briefcasePopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    //[_briefcasePopOver dismissPopoverAnimated:YES];
}


-(NSString *) rateToText:(int)rate
{
    NSString *response;
    switch (rate) {
        case 25:
            response = @"Poor";
            break;
        case 50:
            response = @"Average";
            break;
        case 75:
            response = @"Good";
            break;
        case 100:
            response = @"Excellent";
            break;
    }
    return response;
}


-(NSString *)getAssetType
{
    NSString *extension = [_asset.uri_full pathExtension];
    BOOL isVideo = [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"mp4|mov|avi|m4v"] evaluateWithObject:extension];
    BOOL isImage = [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"jpg|png|gif|jpeg|bmp"] evaluateWithObject:extension];
    // PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    BOOL isMimeType = ([_asset.file_mime isEqualToString:@"weblink"]);
    
    if(isVideo)
    {
        return @"Video";
    }
    if(isImage)
    {
        return @"Image";
    }
    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    if(isMimeType)
        return @"weblink";
    
    return @"Document";
}


- (IBAction)folderAction:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    //Changed:Added by Buvana: 30-Sep-2016
    //Comments:
    // PRONTO-12 - Folder Creation and Search
    _showFolderAlert = YES;
    //End of fix

    
   // [self.searchMenuPopOver dismissPopoverAnimated:NO];
    
    [[self.searchMenuPopOver presentingViewController] dismissViewControllerAnimated:NO completion:NULL];
    
    if ([button.titleLabel.text isEqualToString:@"Save results as Folder"]) {
        
        // Alternative AlertView
        UIAlertController* saveFolderAlertView = [UIAlertController alertControllerWithTitle:@"Save results as Folder"  message:@"Please enter a folder name."  preferredStyle:UIAlertControllerStyleAlert];
        
        [saveFolderAlertView addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            
            alertTextField = textField;
            alertTextField.tag = 1001;
            //alertTextField.text = _name.text;
        }];
        
        UIAlertAction* cancelButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction * action) {
                                           //Handle Cancel button
                                       }];
        UIAlertAction* saveButton = [UIAlertAction
                                     actionWithTitle:@"Save"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                         //Handle Save button
                                         
                                         [self performSelector:@selector(saveAsFolder) withObject:nil afterDelay:1.0];
                                         
                                     }];
        
        [saveFolderAlertView addAction:cancelButton];
        [saveFolderAlertView addAction:saveButton];
        
        UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
            viewController = viewController.presentedViewController;
        }
        [viewController presentViewController:saveFolderAlertView animated:YES completion:nil];


    }
    else if ([button.titleLabel.text isEqualToString:@"Create a folder"]) {
        
        
        // Alternative AlertView
        UIAlertController* saveFolderAlertView = [UIAlertController alertControllerWithTitle:@"Add a new Folder"  message:@"Please enter a folder name."  preferredStyle:UIAlertControllerStyleAlert];
        
        [saveFolderAlertView addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            
            alertTextField = textField;
            alertTextField.tag = 1000;
            //alertTextField.text = _name.text;
        }];
        
        UIAlertAction* cancelButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction * action) {
                                           //Handle Cancel button
                                       }];
        UIAlertAction* saveButton = [UIAlertAction
                                     actionWithTitle:@"Save"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                         //Handle Save button
                                         
                                         [self performSelector:@selector(addNewFolder) withObject:nil afterDelay:1.0];
                                         
                                     }];
        
        [saveFolderAlertView addAction:cancelButton];
        [saveFolderAlertView addAction:saveButton];
        
        UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
            viewController = viewController.presentedViewController;
        }
        [viewController presentViewController:saveFolderAlertView animated:YES completion:nil];

    }
}

-(void) saveAsFolder
{
    [self alertViewTag:1 clickedButtonAtIndex:1];
    
}

-(void) addNewFolder
{
    [self alertViewTag:3 clickedButtonAtIndex:1];
}


// PRONTO -12 Folder Creation and Search
// there was no blinking "I" icon in the search field.before populating
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIButton *btnClear = [textField valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage GetCloseIconImage] forState:UIControlStateNormal];
    
    // keytype
    textField.returnKeyType = UIReturnKeyDone;
    //Revamp(15/12) - Search white
    _searchTextbox.backgroundColor = [UIColor whiteColor];
    _searchTextbox.textColor = [UIColor blackColor];
    [_lensButton setImage:[UIImage imageNamed:@"searchIconSelect"] forState:UIControlStateNormal];
  
    _clearSearch = NO;
    
//    BOOL isAlert = NO;
    
//    isAlert = _showFolderAlert;
    
    
    if([textField isFirstResponder])
    {
        if(_library.section == ProntoSectionTypeLibrary && [ZMUserActions sharedInstance].enableSearch && textField.text.length == 0)
        {
            // Fix for crashing - Application tried to present modally an active controller
            if (!_searchMenuPopOver)
            {
                [self searchMenu:(UIButton*) _searchBoxContainer];
            }
            else
            {
                [[self.searchMenuPopOver presentingViewController] dismissViewControllerAnimated:NO completion:NULL];
                [self searchMenu:(UIButton*) _searchBoxContainer];
            }
        }
        
//        if(isAlert)
//        {
//            isAlert = NO;
//           
//        }
      
    }
    _showFolderAlert = NO;
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:textFieldDidBeginEditing:)])
    {
        [self.delegate header:self textFieldDidBeginEditing:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    UIButton *btnClear = [textField valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage GetCloseIconImage] forState:UIControlStateNormal];
    
    if([alertTextField tag] != 1001 && [alertTextField tag] !=1000) {
     //New Search implementation - Indexing
    self.searchTextbox.textColor = [UIColor blackColor];
    _searchTextbox.backgroundColor = [UIColor whiteColor];
    [_lensButton setImage:[UIImage imageNamed:@"searchIconSelect"] forState:UIControlStateNormal];
   
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        if (range.location == 0 && [string isEqualToString:@" "]) {
            return NO;
        }
    
    if (string.length == 0 && range.length == 1)
    {
        _deleteBackward = YES;
        if(textField.text.length == 1)
        {
            [textField becomeFirstResponder];
            // Fix for crashing - Application tried to present modally an active controller
            if(!_searchMenuPopOver)
                [self searchMenu:(UIButton*) _searchBoxContainer];
        }
        else
        {
           // [_searchMenuPopOver dismissPopoverAnimated:NO];
                [[self.searchMenuPopOver presentingViewController] dismissViewControllerAnimated:NO completion:NULL];
            
        }

    }
    else
        _deleteBackward = NO;
        
    }
    

    // PRONTO-33 - Increase the limit on the search characters
    if(([ZMUserActions sharedInstance].enableSearch)|| [alertTextField tag]== 1001 ||[alertTextField tag] == 1000 )
    {
        
       return (range.location < 30);
    }
    else if(textField.placeholder.length > 6)
    {
        return 0;
    }
    else
    {
        return (range.location < 30);
    }
    
}

-(void)MykeyBoardUp{
    [_searchTextbox becomeFirstResponder];
}

//New Search implementation - Indexing

-(void)textFieldDidChange :(UITextField *) textField
{
    // Use done button

    UIButton *btnClear = [textField valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage GetCloseIconImage] forState:UIControlStateNormal];
    
    textField.returnKeyType = UIReturnKeyDone;

   [AbbvieLogging logInfo:[NSString stringWithFormat:@"CHANGING:%@",textField.text]];
    
    [ZMUserActions sharedInstance].searchingText = textField.text;
     _isMakingService = NO;
    if(_library.section == ProntoSectionTypeLibrary)
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:editingChanged:)])
        {
            [self.delegate header:self editingChanged:textField.text];
        }
        
     if(textField.text.length == 0 && [ZMUserActions sharedInstance].enableSearch)
     {
         // Fix for crashing - Application tried to present modally an active controller
         if(!_searchMenuPopOver)
         {
             [self searchMenu:(UIButton*) _searchBoxContainer];
         }
         
         [self textFieldShouldClear:textField];
         
         return;

     }else{
             [[self.searchMenuPopOver presentingViewController] dismissViewControllerAnimated:NO completion:NULL];
     }
        
    }
  
}

- (void) searchForKeyword:(NSString *)keyword
{
    [self showIndicatorFromGlobalSearch];
    
    // perform your search (stubbed here using NSLo)
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Searching for keyword %@", keyword]];

    
    ZMAbbvieAPI* apiManager = [ZMAbbvieAPI sharedAPIManager];
    
    // if textlenght is greater than equal to 3,not deleting backward make the service call
    
    if(_searchTextbox.text.length >= 3 && apiManager.isConnectionAvailable)
    {
        _isMakingService = YES;
        
        if(_clearSearch==NO)
            [self keywordSearchHistory:_searchTextbox];
        
        [AbbvieLogging logInfo:@"New Search Webservice.."];
        
        _isSearchAssets = YES;
        
        [ZMUserActions sharedInstance].updatedAssets = YES;
        
        [ZMProntoManager.sharedInstance.search setTitle:keyword];
        
        _library.headerView.searchTextbox.text = keyword;
        
        [self performSelector:@selector(editingChanged:) withObject:keyword afterDelay:0.5];
        
        
    }
    else
    {
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            counter = 0;
            [self searchOffline:_searchTextbox];
        });
    }
    
}


 - (void)editingChanged:(NSString *)text
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"textedit:%@",text]];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:editingChanged:)])
    {
        [self.delegate header:self editingChanged:_searchTextbox.text];
    }
    
    if ([text isEqualToString:_searchTextbox.text])
    {
        [ZMUserActions sharedInstance].updatedAssets = YES;
        
        [ZMProntoManager.sharedInstance.search setTitle:_searchTextbox.text];
        
        _library.headerView.searchTextbox.text = _searchTextbox.text;
        
          ZMAbbvieAPI* apiManager = [ZMAbbvieAPI sharedAPIManager];
        
        [self showIndicatorFromGlobalSearch];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                   //dispatch_async(dispatch_get_main_queue(), ^{
            [apiManager searchAssetsFromCMS:^{
            }
            successList:^(NSArray *success)
             {
             }
            error:^(NSError *error)
            {
                
            }];
        });
        
    }
    
    
}

// New Search Implementation - Offline Search
-(void) searchOffline : (UITextField*) textField
{
    [ZMProntoManager.sharedInstance.search setTitle:textField.text];
    _library.headerView.searchTextbox.text = textField.text;
    NSString* txtString = textField.text;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ZMUserActions sharedInstance].isKeywordSaved = NO;
        // check with the saved keywords
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0];
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        
        NSString *plistPath = @"";
        NSMutableDictionary* allFiles = [[NSMutableDictionary alloc] init];
//        BOOL result = NO;
        
        if([paths count]>0 && txtString.length >= 3 )
        {
            
            NSMutableArray* finalArry = [[NSMutableArray alloc] init];
            NSError *error = nil;
            NSArray *documentArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
            NSEnumerator *e = [documentArray objectEnumerator];
            NSString *extension = @"plist";
            NSString *filename;
            
            while ((filename = [e nextObject]))
            {
                if ([[filename pathExtension] isEqualToString:extension])
                {
                    filename = [filename stringByDeletingPathExtension];
                    [finalArry addObject:filename];
                }
            }
            for(int k=0; k <[finalArry count]; k++)
            {
                NSString* findString = nil;
                if ([txtString length] >= 3)
                {
                    findString = [txtString substringToIndex:3];
                }
                if([[finalArry objectAtIndex:k] containsString:findString] || [[finalArry objectAtIndex:k] containsString:[NSString stringWithFormat:@"%@",txtString]])
                {
//                    result = YES;
                    plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",[finalArry objectAtIndex:k]]];
                    
                    [allFiles setValue:plistPath forKey:[NSString stringWithFormat:@"%@",[finalArry objectAtIndex:k]]];
                }
                else
                {
//                    result = NO;
                }
            }
        }
        NSMutableDictionary* searchDict = [[NSMutableDictionary alloc] init];
        if(txtString.length >=3 && [allFiles count]==0)
        {
            plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%d.plist",txtString,counter]];
            
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"plistPath-off:%@",plistPath]];
        }

        if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath] && txtString.length >=3)
        {
            NSString* existPath = plistPath;
            // Merge all plist files
            if(allFiles.count >= 1)
            {
                for(int k=0; k <=[allFiles count]; k++)
                {
                    plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%d.plist",txtString,k]];
                    
                    if(![plistPath containsString:existPath])
                    {
                        existPath = [existPath componentsSeparatedByString:@"_"].firstObject;
                        plistPath = [NSString stringWithFormat:@"%@_%d.plist",existPath,k];
                        
                    }
                    NSDictionary* searchingDict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
                    
                    //Append the value of dictionary for events
                    NSDictionary* searchingDictOfEvents = searchingDict[@"events"];
                    NSMutableDictionary *eventsDictionary  = searchDict[@"events"];
                    if(eventsDictionary != nil)
                    {
                        if(searchingDictOfEvents.count > 0)
                        {
                            [eventsDictionary addEntriesFromDictionary:searchingDictOfEvents];
                        }
                        [searchingDict setValue:eventsDictionary forKey:@"events"];
                    }
                    
                    [searchDict addEntriesFromDictionary:searchingDict];
                }
            }
            
            [ZMUserActions sharedInstance].isKeywordSaved = YES;
            // if keyword is available
            if([searchDict count] != 0 && txtString.length >= 3)
            {
                NSDictionary *responseObjectWithEventsResponse = searchDict;
                
                NSDictionary *eventsResponse = responseObjectWithEventsResponse[@"events"];
                if (eventsResponse != nil &&
                    [eventsResponse isKindOfClass:[NSDictionary class]] &&
                    eventsResponse.count > 0)
                {
                    [[EventCoreDataManager sharedInstance] prepareGlobalSearchResultsForEvent:eventsResponse];
                }
                
                [searchDict removeObjectForKey:@"events"];
                
                // keys from response
                NSArray* assetsIdArray = [searchDict allKeys];
                NSArray* assetValues = [searchDict allValues];
                dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [ZMUserActions sharedInstance].updatedAssets = YES;
                    ZMAbbvieAPI* apiManager = [ZMAbbvieAPI sharedAPIManager];
                    NSMutableDictionary *dictKeys = [[[NSMutableDictionary alloc] initWithObjects:assetValues forKeys:assetsIdArray]mutableCopy]; //create dict
                    
                    NSString* total = [dictKeys objectForKey:@"total"];
                    NSString* pages = [dictKeys objectForKey:@"pages"];
                    
                    // check if the response is only having this count or both count is "0"
                    if([dictKeys count] == 2 && [total intValue] == 0 && [pages intValue] == 0)
                    {
                        [ZMUserActions sharedInstance].assetsCount = 0;
                        [AbbvieLogging logInfo:@"showing alert offline.."];
                        [ZMUserActions sharedInstance].searchCompletion = YES;
                        return;
                    }
                    // check if the response is only having this count
                    else if([dictKeys count] == 2 && [total intValue] != 0 && [pages intValue] != 0)
                    {
                        return;
                    }
                    else
                    {
                        counter = counter + 1;
                        
                        if([pages intValue] > 0 &&  counter == 1)
                        {
                            [ZMUserActions sharedInstance].totalSearchedAssets = [[NSMutableArray alloc] initWithCapacity:[total intValue]];
                        }
                        else if([pages intValue] == 0 ||  counter == 0)
                        {
                            [ZMUserActions sharedInstance].totalSearchedAssets = nil;
                        }
                        
                        [ZMUserActions sharedInstance].searchCompletion = NO;
                        
                        [dictKeys removeObjectForKey:@"total"]; //remove object
                        [dictKeys removeObjectForKey:@"pages"]; //remove object
                    
                        self.totalSearchedAssets = [[dictKeys allKeys] copy];
                        
                        [[ZMUserActions sharedInstance].totalSearchedAssets addObjectsFromArray:self.totalSearchedAssets];
                        
                        [ZMUserActions sharedInstance].searchedKeys = [self arrayByEliminatingDuplicatesWithoutOrder:[ZMUserActions sharedInstance].totalSearchedAssets];
                        
                        [apiManager getSearchedAssets:[ZMUserActions sharedInstance].searchedKeys success:^(NSArray * assetsLoaded) {
                            
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"successed imported searched assets %lu", (unsigned long)assetsLoaded.count]];
                                // display the count
                                [ZMUserActions sharedInstance].assetsCount  = (int)[[ZMUserActions sharedInstance] resultantAssets].count;
                        
                            ZMProntoManager.sharedInstance.assetsIdToFilter = assetsLoaded;
                            [ZMUserActions sharedInstance].searchedCount = (int)assetsLoaded.count;
                            [[ZMUserActions sharedInstance] setResultantAssets:assetsLoaded];
                            [ZMUserActions sharedInstance].isFinalSearch = YES;
                            [ZMUserActions sharedInstance].searchCompletion = YES;
                            
                            dispatch_async(dispatch_get_main_queue(),^{
                                [[ZMUserActions sharedInstance].globalSearchViewController showSearchedAssetFromOfflineMode];
                            });
                        }error:^(NSError *error) {
                            [AbbvieLogging logError:[NSString stringWithFormat:@"error at importing assets %@", error.localizedDescription]];
                        }];
                    }
                });
            }
            else
            {
                // no keyword available
                [ZMProntoManager sharedInstance].assetsIdToFilter = @[];
                [ZMProntoManager sharedInstance].selectedCategories = @[];
                [ZMUserActions sharedInstance].searchedCount = 0;
                [ZMUserActions sharedInstance].isFinalSearch = YES;
                [ZMUserActions sharedInstance].searchCompletion = YES;
                dispatch_async(dispatch_get_main_queue(),^{
                    // If keyword not available, show search found
                    [[ZMUserActions sharedInstance].globalSearchViewController showSearchedAssetFromOfflineMode];
                });
            }
        }
        else
        {
            // no keyword available
            [ZMProntoManager sharedInstance].assetsIdToFilter = @[];
            [ZMUserActions sharedInstance].searchedCount = 0;
            [ZMUserActions sharedInstance].isFinalSearch = YES;
            [ZMUserActions sharedInstance].searchCompletion = YES;
            
            //Avoid display of all franchise assets when delete of search by backspace
//                [ZMProntoManager sharedInstance].selectedCategories = @[];
            dispatch_async(dispatch_get_main_queue(),^{
                // If keyword not available, show search found
                [[ZMUserActions sharedInstance].globalSearchViewController showSearchedAssetFromOfflineMode];
            });
        }
    });
}

// New Search Implementation - Complete Edit
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    UIButton *btnClear = [textField valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage GetCloseIconImage] forState:UIControlStateNormal];
    
     // save the keywords in searchmenu
    if((_clearSearch==NO && (_library.section == ProntoSectionTypeLibrary) && textField.text.length >=3 && alertTextField.tag !=111 && alertTextField.tag !=222) && _library.section == ProntoSectionTypeLibrary)
    {
            
         [self keywordSearchHistory:textField];
         [ZMUserActions sharedInstance].searchingText = textField.text;
    }
    
    //[self.searchMenuPopOver dismissPopoverAnimated:NO];
        [[self.searchMenuPopOver presentingViewController] dismissViewControllerAnimated:NO completion:NULL];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"End editing:%@",textField.text]];
    
    if(textField.text.length < 1) {
        _resetButton.hidden = YES;
        //Revamp(15/12) - Search white
        _searchTextbox.backgroundColor = [UIColor clearColor];
        [_lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
        self.searchTextbox.textColor = [UIColor whiteColor];
    }
    
    
    if(self.searchTextbox.text.length >=3 && [ZMUserActions sharedInstance].isServiceHitted)
    {
        [ZMUserActions sharedInstance].searchCompletion = NO;
    }
}

-(void) favSelected: (BOOL) selected andAsset:(Asset *) assetVal {
        ZMEvent *ratingEvent = [[ZMEvent alloc] init];
        ratingEvent.assetId = assetVal.assetID.longValue;
        ratingEvent.eventType = ZMEventLike;
    
//        NSString *rating = @"liked";
        NSInteger rate = 100;
    
        // PRONTO-1 - Likes not Working
        NSNumber *k = [NSNumber numberWithInt:1];
    
        NSNumber *totalVotes = [NSNumber numberWithInt:([k intValue] + [assetVal.votes intValue])];
    
        if (selected) {
            
            if(assetVal.votes.intValue >0)
                assetVal.votes = totalVotes;
            else
                assetVal.votes = [NSNumber numberWithInt:0];
            
            assetVal.user_vote = @"100";
        }
        else {
            
            if(assetVal.votes.intValue > 0)
                assetVal.votes = [NSNumber numberWithInt:([assetVal.votes intValue]-[k intValue])];
            else
                assetVal.votes = [NSNumber numberWithInt:0];
            
            assetVal.user_vote = @"0";
            ratingEvent.eventType = ZMEventUnlike;
//            rating = @"disliked";
            rate = 0;
        }
    
    
        ratingEvent.eventValue = [NSString stringWithFormat:@"%ld", (long)rate];
    
        //Event created from bucket id
        ratingEvent.createdFromBucketId = [self getBucketIdForCurrentSelection];
        //Saves the rating to the local DB
        [[ZMAssetsLibrary defaultLibrary] saveEvent:ratingEvent];
    
        __weak typeof (self) weakSelf = self;
        [[ZMCDManager sharedInstance] updateAsset:assetVal success:^{
            
            //Send the events to the server
            [weakSelf assetSendEvent];
            
            if([ZMUserActions sharedInstance].globalSearchViewController)
            {
                [[ZMUserActions sharedInstance].globalSearchViewController markLiked:assetVal type:ratingEvent.eventType];
            }
            else
            {
                if([ZMUserActions sharedInstance].currentSelectedTabIndex == 0)
                {
                    [weakSelf.library markLiked:assetVal type:ratingEvent.eventType];
                }
                else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 1)
                {
                    [weakSelf.competitors markLiked:assetVal type:ratingEvent.eventType];
                }
                else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 2)
                {
                    [weakSelf.tools markLiked:assetVal type:ratingEvent.eventType];
                }
            }
            
        } error:^(NSError * error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on ZMHeader %@",error.localizedDescription]];
        }];
    
}

- (NSInteger)getBucketIdForCurrentSelection
{
    NSInteger currentBucketID = 0;
    
    if([ZMUserActions sharedInstance].globalSearchViewController)
    {
        currentBucketID = [[ZMUserActions sharedInstance].globalSearchViewController getBuckeIdFromSelectedTab];
    }
    else
    {
        currentBucketID = [[Constant sharedConstant] getCurrentActiveTabOptionIDAsString].integerValue;
    }
    return currentBucketID;
}
- (IBAction)likeAction:(UIButton *)sender{
    
    ZMEvent *ratingEvent = [[ZMEvent alloc] init];
    ratingEvent.assetId = self.asset.assetID.longValue;
    ratingEvent.eventType = ZMEventLike;
    
    NSString *rating = @"liked";
    NSInteger rate = 100;
    
    // PRONTO-1 - Likes not Working
    NSNumber *k = [NSNumber numberWithInt:1];
    
    NSNumber *totalVotes = [NSNumber numberWithInt:([k intValue] + [self.asset.votes intValue])];
    
    if (sender.tag == 100) {
       
        if(self.asset.votes.intValue >0)
            self.asset.votes = totalVotes;
        else
            self.asset.votes = [NSNumber numberWithInt:0];
        
        self.asset.user_vote = @"100";
        sender.tag = 101;
        [sender setImage:[UIImage imageNamed:@"likePressed"] forState:UIControlStateNormal];
    }
    else if (sender.tag == 101) {
        
        if(self.asset.votes.intValue > 0)
            self.asset.votes = [NSNumber numberWithInt:([self.asset.votes intValue]-[k intValue])];
        else
            self.asset.votes = [NSNumber numberWithInt:0];
        
        self.asset.user_vote = @"0";
        ratingEvent.eventType = ZMEventUnlike;
        rating = @"disliked";
        rate = 0;
        sender.tag = 100;
        [sender setImage:[UIImage imageNamed:@"likeNormal"] forState:UIControlStateNormal];
    }
    
    
    ratingEvent.eventValue = [NSString stringWithFormat:@"%ld", (long)rate];
    
    //Saves the rating to the local DB
    [[ZMAssetsLibrary defaultLibrary] saveEvent:ratingEvent];
    
    [[ZMCDManager sharedInstance] updateAsset:self.asset success:^{
        //Send the events to the server
       
        [self assetSendEvent];
        
        if([ZMUserActions sharedInstance].currentSelectedTabIndex == 0)
        {
            [_library markLiked:self.asset type:ratingEvent.eventType];
        }
        else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 1)
        {
            [_competitors markLiked:self.asset type:ratingEvent.eventType];
        }
        else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 2)
        {
            [_tools markLiked:self.asset type:ratingEvent.eventType];
        }
        
        /**
         * @Tracking
         * Rating
         **/
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                  withSubsection:@"assetview" withName:[NSString stringWithFormat:@"%@-rateit",[ZMAssetViewController getAssetType:_asset]]
                     withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"sctierlevel":@"tier3", @"scrating":rating}]];

    } error:^(NSError * error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on ZMHeader %@",error.localizedDescription]];
    }];
    
    }

- (void)assetSendEvent {
    
    if (_asset) {
        
        dispatch_async(dispatch_queue_create("Sending Events", NULL), ^{            
            [[ZMAbbvieAPI sharedAPIManager] uploadEvents:^{
                [AbbvieLogging logInfo:@">>> Events Synced"];
            } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with uploadEvents: %@", error]];
            }];
        });
    }
}

-(void)setDefaultSorts{
    
    NSMutableArray *sort = [[NSMutableArray alloc] init];
    
    [sort addObject:@{@"id":@"1", @"sort":@"dateAdded", @"label":@"Recently Added", @"selected":@"1", @"sortDir":@"DESC"}];
    [sort addObject:@{@"id":@"5", @"sort":@"viewcount", @"label" : @"Most Viewed", @"selected" : @"0", @"sortDir" : @"DESC"}];
    [sort addObject:@{@"id":@"2", @"sort":@"dateUpdated", @"label" : @"Recently Updated", @"selected" : @"0", @"sortDir":@"DESC"}];
    
    self.isDefaultSort = YES;
    [ZMProntoManager sharedInstance].sortBy = sort;
    self.sortMenu.sortArray = sort;
}

-(void)hideForAssetDetail {
    // hiding keyboard in weblink/video
    [_searchTextbox endEditing:YES];
    _searchBoxContainer.hidden = YES;
    lineDividerUser.hidden = YES;
    lineDividerIcons.hidden = YES;
    lineDividerPannel.hidden = YES;
    _helpButton.hidden = YES;
    _messagesButton.hidden = YES;
    _arrow.hidden = YES;
    badge.hidden = YES;
}

# pragma mark - Alert view

- (void)alertViewTag:(int)alertViewTag clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertViewTag)
    {
        //Save results as folder
        case 1:
            if (buttonIndex == 1) {
                
                UITextField *folder = alertTextField;
                folder.tag = 111;
                alertTextField.tag = 111;
                NSString *folderName = folder.text;
                                        
                BOOL isDuplicated = [[ZMAssetsLibrary defaultLibrary] duplicatedFolderName:folderName];
                
                if (isDuplicated == NO && ![folderName isEqualToString:@""]){
                    
                    /**
                     * @Tracking
                     * Search Saved
                     **/
                    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
                    [trackingOptions setObject:[NSString stringWithFormat:@"saved"] forKey:@"scsearchaction"];
                    
                    [ZMTracking trackSection:@"search" withSubsection:@"NO" withName:@"NO" withOptions:trackingOptions];
                    
                    ZMFolder *newFolder = [[ZMFolder alloc] init];
                    newFolder.fldr_name = folderName;
                    [[ZMAssetsLibrary defaultLibrary] saveFolder:newFolder withDetails:FALSE];
                    
                    
                    /**
                     * @Tracking
                     * Create Folder
                     **/
                    [ZMTracking trackSection:_library.actions.section
                              withSubsection:@"folder"
                                    withName:[NSString stringWithFormat:@"addfolder-%@",folderName]
                                 withOptions: [[NSMutableDictionary alloc] initWithDictionary:@{@"scbriefcaseaction":@"create",
                                                                                                @"sccollateralname":folderName }]];
                    
                    [[ZMCDManager sharedInstance]getAssets:0 limit:-1 success:^(NSArray * totalAssets) {
                        
                        NSMutableArray * assetList = [[NSMutableArray alloc]initWithArray:totalAssets];
                        
                        // New Search Implementation
                        if(_searchTextbox.text.length >= 3)
                        {
                            assetList = [[NSMutableArray alloc]initWithArray:[[ZMUserActions sharedInstance] resultantAssets]];
                        }
                       // PRONTO-63&PRONTO-55 - Briefcase Functionality to behave like regular Folder and Files - adding only to folder
                        [[ZMAssetsLibrary defaultLibrary] saveSearchToFolder:assetList newFolder:newFolder];
                       

                        dispatch_async(dispatch_get_main_queue(), ^{
                            AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                            [mainDelegate showMessage:@"Your folder has been successfully created." whithType:ZMProntoMessagesTypeSuccessWithoutIndicator withTime:2];
                        });
                    }];
                    
                }
                else {
                    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    NSString *message = @"Error, duplicated folder name, please change it.";

                    if ([folderName isEqualToString:@""]) {
                        message = @"Error, invalid folder name, please change it.";
                    }
                    [mainDelegate showMessage:message whithType:ZMProntoMessagesTypeWarning];
                }

            }
            break;
        case 2:
            // Selecting "Reset" button
            if(buttonIndex == 1) {
                [self setDefaultSorts];
                [_library updateSortButton];
                [ZMProntoManager sharedInstance].categoriesToFilter = @[];
                [ZMProntoManager sharedInstance].selectedCategoriesToUpdateHeader = @[];
                [ZMProntoManager sharedInstance].selectedCategories = @[];
                [ZMProntoManager sharedInstance].resetFranchiseIsSelected = NO;
                [[ZMProntoManager sharedInstance] resetFranchiseAndBrandsCollection];
                _library.resestFranchises = YES;
                [ZMUserActions sharedInstance].allFranchisesChecked = NO;
                [ZMUserActions sharedInstance].checkURLSchema = NO;
                [_library backToDefault];
                // New Search Implementation
                [[ZMProntoManager sharedInstance].search setTitle:@""];
                _searchTextbox.text = @"";
                
                [ZMUserActions sharedInstance].enableSearch = YES;
                _isResetSelected = YES;
                [ZMUserActions sharedInstance].selectedSortByMenu = @"";
                [self hidingIndicator];
                
                
            }
            break;
        case 3:

            if (buttonIndex == 1) {
                
                UITextField *folder =alertTextField;
                alertTextField.tag = 222;
                folder.tag = 222;
                NSString *folderName = folder.text;
                BOOL isDuplicated = [[ZMAssetsLibrary defaultLibrary] duplicatedFolderName:folderName];
                if (isDuplicated == NO && ![folderName isEqualToString:@""])
                {
                    // PRONTO-12 - Folder Creation and Search
                    [[ZMProntoManager sharedInstance].search setTitle:_searchTextbox.text];
                    ZMFolder *newFolder = [[ZMFolder alloc] init];
                    newFolder.fldr_name = folderName;
                    [[ZMAssetsLibrary defaultLibrary] saveFolder:newFolder withDetails:FALSE];
                    _library.pageIndex = 0;
                    [_library loadBriefcase];
                    
                    /**
                     * @Tracking
                     * Create Folder
                     **/
                    [ZMTracking trackSection:_library.actions.section
                              withSubsection:@"folder"
                                    withName:[NSString stringWithFormat:@"addfolder-%@",folderName]
                                 withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"scbriefcaseaction":@"create", @"sccollateralname":folderName}]];
                    
                    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [mainDelegate showMessage:@"Your folder has been successfully created." whithType:ZMProntoMessagesTypeSuccessWithoutIndicator withTime:2];
                
                } else {
                    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    NSString *message = @"Error, duplicated folder name, please change it.";
                    if([folderName isEqualToString:@""])
                    {
                        message = @"Error, invalid folder name, please change it.";
                    }
                    [mainDelegate showMessage:message whithType:ZMProntoMessagesTypeWarning];
                }
            }
            break;
    }
}


- (IBAction)categoryWhite:(UIButton *)sender
{
    sender.enabled = NO;
   [_library displayBrowse];
}

- (IBAction)closeAction:(id)sender
{
    [ZMUserActions sharedInstance].section = [[ZMUserActions sharedInstance].section stringByReplacingOccurrencesOfString:@"-assetview" withString:@""];
    
    long interval;
    interval = (long)[[NSDate date] timeIntervalSince1970] - initDate;
    //Engagement Tracking
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc]init];
    [trackingOptions setObject:[NSString stringWithFormat:@"tier%d" ,detail.tierTrackingValue] forKey:@"sctierlevel"];

    if ([detail isKindOfClass:[ZMBrightcove class]]) {
        
        ZMBrightcove *brightcoveController = (ZMBrightcove *)detail;
        [trackingOptions setObject:[NSString stringWithFormat:@"<%ld>:<%ld>:<%@>:<%ld>:<%@>:<%ld>:<%lld>:<%@>:<%@>", _asset.view_count.longValue, _asset.view_count.longValue, @"NA", _asset.votes.longValue, @"NA", interval, brightcoveController.duration, @"NA" ,@"NA" ] forKey:@"scactivitylog"];
    }
    else {
        [trackingOptions setObject:[NSString stringWithFormat:@"<%ld>:<%ld>:<%@>:<%ld>:<%@>:<%ld>:<%@>:<%@>:<%@>", _asset.view_count.longValue, _asset.view_count.longValue, @"NA", _asset.votes.longValue, @"NA", interval, @"NA", @"NA" ,@"NA" ] forKey:@"scactivitylog"];
    }
    
    /**
     * @Tracking
     * Engagement
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"assetview" withName:[NSString stringWithFormat:@"%@-engagement",[ZMAssetViewController getAssetType:_asset]] withOptions:trackingOptions];
    if([ZMUserActions sharedInstance].currentSelectedTabIndex == 0)
    {
        if (_library != nil)
        {
            if(_library.navigationController == nil)
            {
                AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                UINavigationController* navigationController = (UINavigationController*)(mainDelegate.window.rootViewController);
                [navigationController popToRootViewControllerAnimated:YES];
            }
            else
            {
                [_library.navigationController popToRootViewControllerAnimated:YES];
            }
        }
    }
    else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 1)
    {
        [_competitors.navigationController popToRootViewControllerAnimated:YES];
    }
    else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 2)
    {
        [_tools.navigationController popToRootViewControllerAnimated:YES];
    }
    else if([ZMUserActions sharedInstance].currentSelectedTabIndex == 3)
    {
        [_events.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kZMAssetViewControllerGoBackNotification object:nil];
    }
    
    /**
     * @Tracking
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section
              withSubsection:@"assetview"
                    withName:[NSString stringWithFormat:@"%@|%@",[ZMAssetViewController getAssetType:_asset], _asset.title]
                 withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{ @"sccollateralname": [NSString stringWithFormat:@"%@|%@", [ZMAssetViewController getAssetType:_asset], _asset.title],@"scpdfaction" :@"complete"}]];

                 [self.delegate closeButtonDidPress];
    
           
}


-(void) setContainer:(UIView *)container
{
    _container = container;
    CGRect frame = self.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    self.frame = frame;
    
}


- (IBAction)applicationInfoAction:(UIButton *)sender
{
    ZMInfoView *info = (ZMInfoView *)[[[NSBundle mainBundle] loadNibNamed:@"InfoView" owner:self options:nil] objectAtIndex:0];
    info.modalPresentationStyle = UIModalPresentationFormSheet;
    [_library presentViewController:info animated:YES completion:nil];
}

// TextField Delegate Methods

- (BOOL) textFieldShouldClear:(UITextField *)textField
{
    UIButton *btnClear = [textField valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage GetCloseIconImage] forState:UIControlStateNormal];
    
    [self hidingIndicator];
    [[ZMProntoManager sharedInstance].search resetTitleToCurrentView];
    _clearSearch = YES;
    _searchTextbox.text = @"";
    [ZMUserActions sharedInstance].searchingText = @"";
    [_lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
    [_library loadLibrary];
    
    _library.actions.isAllCategorySelected = YES;
    _library.actions.isCategoriesSelected = NO;
    
    [_library updateCategoryButton];
    
    if(_library.actions.assetsCount > 0)
    {
        //Avoid assets display all franchise when search cleared
        [ZMProntoManager sharedInstance].selectedCategoriesToUpdateHeader  = @[];
        
        for(int k=0; k< [self.categoryMenu.categoriesArray count]; k++)
        {
             NSMutableDictionary *category = [[NSMutableDictionary alloc] initWithDictionary:[self.categoryMenu.categoriesArray objectAtIndex:k]];
            if([[category objectForKey:@"selected"] intValue] == 0)
               [[self.categoryMenu.categoriesArray objectAtIndex:k] setObject:@"1" forKey:@"selected"];
        }
        
        [self.categoryMenu.categoryMenuTableView reloadData];
        
        [_library loadMyLibrary];
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
   
    UIButton *btnClear = [textField valueForKey:@"clearButton"];
    [btnClear setImage:[UIImage GetCloseIconImage] forState:UIControlStateNormal];
    //Changed/Commented:Added by Buvana: 30-Sep-2016
    //Comments:
    // PRONTO-12 - Folder Creation and Search
    if(_showFolderAlert)
    {
        self.searchTextbox.text = [[ZMProntoManager sharedInstance].search getTitle];
        _library.headerView.searchTextbox.text = [[ZMProntoManager sharedInstance].search getTitle];
        _showFolderAlert = NO;
    }
    else
    {
        
        [[ZMProntoManager sharedInstance].search setTitle:textField.text];
        self.searchTextbox.text = textField.text;
        _library.headerView.searchTextbox.text =textField.text;
    }
    
    // PRONTO-11 - Button Visibility not Consistent
    if (_library.section == ProntoSectionTypeLibrary && ([ZMUserActions sharedInstance].assetsCount > 0)) {
        
        _folderButton.hidden = NO;
        [_folderButton setTitle:@"Save results as Folder" forState:UIControlStateNormal];
    }
    
    //Hide the Search Menu if presented
    if(self.isSearchMenuPresented)
    {
        [[self.searchMenu presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    if (self.settingsMenuIsPresented)
    {
        [[self.settingsMenu presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(header:didTapShouldTextFieldReturn:)])
    {
        [self.delegate header:self didTapShouldTextFieldReturn:textField];
    }
    
    if(textField.text.length >= 3 )
    {
        [self searchForKeyword:textField.text];
    }
    else
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:editingChanged:)])
        {
            [self.delegate header:self editingChanged:_searchTextbox.text];
        }
    }
    
    return YES;
}

// New Search Implementation - Search History
-(void) keywordSearchHistory : (UITextField*) textField
{
    // retrieve the data
    
    // max. 5 recent searches
    if([_keyWordArray count] < 5)
    {
        // check for empty values
        if(![textField.text isEqualToString:@""] || textField.text.length != 0)
            [_keyWordArray replaceObjectsInRange:NSMakeRange(0,0)
                            withObjectsFromArray:[NSArray arrayWithObject:textField.text]];
        
        _keyWordArray = [self removeDuplicates:_keyWordArray];
        _keyWordArray =[_keyWordArray mutableCopy];
        
        counter = 0;
        
    }
    else
    {
        if(![textField.text isEqualToString:@""] || textField.text.length != 0 )
        {
            
            if(![textField.text isEqualToString:_keyWordArray[0]]) {
                
                [_keyWordArray replaceObjectsInRange:NSMakeRange(0,0)
                 
                                withObjectsFromArray:[NSArray arrayWithObject:textField.text]];
                
                _keyWordArray = [self removeDuplicates:_keyWordArray];
                
                _keyWordArray =[_keyWordArray mutableCopy];
                
            
                if(_keyWordArray.count > 5){
                    
                    [_keyWordArray removeLastObject];
                    
                }
                
            }
            
        }
        
    }
    
    // add the array in NSUserDefault
    [[NSUserDefaults standardUserDefaults] setObject:_keyWordArray forKey:@"Keywords"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
}

// New Search Implementation  - Remove Duplicates
- (NSMutableArray *)removeDuplicates:(NSMutableArray *)array{

    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:[array copy]];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    NSMutableArray * newArray = [[[NSOrderedSet orderedSetWithArray:arrayWithoutDuplicates] array] copy];
    return newArray;
    
}

- (NSMutableArray *)arrayByEliminatingDuplicatesWithoutOrder:(NSMutableArray *)array{

    NSMutableArray* newArray  = [[[NSSet setWithArray:[array copy]] allObjects] mutableCopy];
    return newArray;

}

- (void)doRequiredSetupOnDidSelectOfSearchMenu
{
    _senderForSettings = nil;
    _settingsMenuIsPresented = NO;
    
    self.isSearchMenuPresented = NO;
    self.senderForSearchMenu = nil;
}

-(void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    BOOL tempIsSettingsMenuPresented = self.settingsMenuIsPresented;
    _senderForSettings = nil;
    _settingsMenuIsPresented = NO;

    self.isSearchMenuPresented = NO;
    self.senderForSearchMenu = nil;

    if (tempIsSettingsMenuPresented == NO)
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:isDismissingSearchPopover:)])
        {
            [self.delegate header:self isDismissingSearchPopover:popoverPresentationController];
        }
    }
}

- (IBAction)profileSelected:(id)sender
{
    [self settingsShow:sender];
}

-(void) settingsShow: (id)sender
{
    if (!self.settingsMenu)
    {
        self.settingsMenu = (Settings *)[[[NSBundle mainBundle] loadNibNamed:@"Settings" owner:self options:nil] objectAtIndex:0];
        self.settingsMenu.library = _library;
        self.settingsMenu.competitors = _competitors;
        self.settingsMenu.toolsTraining = _tools;
        self.settingsMenu.eventsViewController = self.events;
        self.settingsMenu.parent = self;
    }
    
    NSMutableArray * settingsMutableArray =[[NSMutableArray alloc]init];
    
    NSString * name = [NSString stringWithFormat:@"%@", [AOFLoginManager sharedInstance].displayName];
    //For large and small text
    NSMutableDictionary * profileDict = [[NSMutableDictionary alloc]init];
    [profileDict setObject:name forKey:@"main"];
    [profileDict setObject:_userFranchise.text forKey:@"sub"];
    [settingsMutableArray addObject:profileDict];
    
    // check the user
    if([self.userFranchise.text  isEqual: @"In House"] || [self.userFranchise.text  isEqual: @"Home Office"])
        [settingsMutableArray addObject:@"Change Preferences"];
    [settingsMutableArray addObject:@"Feedback"];
    [settingsMutableArray addObject:@"Logout"];
    self.settingsMenu.settingsArray = settingsMutableArray;
    // 'UIPopoverController' is deprecated so using Popover presentation controller
    self.settingsMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.settingsMenu.popoverPresentationController.sourceRect = [sender frame];
    self.settingsMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomInfoPopoverBackgroundView class];
    
    
    // configure the Popover presentation controller
    self.settingsMenuPopOver = [self.settingsMenu popoverPresentationController];
    self.settingsMenuPopOver.permittedArrowDirections = 1;
    self.settingsMenuPopOver.delegate = self;
    self.settingsMenuPopOver.sourceView = [sender superview];
    if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2)
        self.settingsMenu.preferredContentSize = CGSizeMake(185, [self.settingsMenu.settingsArray count]*44);
    else
        self.settingsMenuPopOver.sourceRect =
        CGRectMake([sender frame].origin.x-20,[sender frame].origin.y-80,_profileButton.frame.size.width-10, 125.0);
    _senderForSettings = sender;
    _settingsMenuIsPresented = YES;
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(header:presentSettings:)])
    {
        [self.delegate header:self presentSettings:self.settingsMenu];
        return;
    }
    if([ZMUserActions sharedInstance].currentSelectedTabIndex == 0)
    {
        [self.library presentViewController:self.settingsMenu animated:YES completion:nil];
    }
    else if ([ZMUserActions sharedInstance].currentSelectedTabIndex == 1)
    {
        [self.competitors presentViewController:self.settingsMenu animated:YES completion:nil];
    }
    else if ([ZMUserActions sharedInstance].currentSelectedTabIndex == 2)
    {
        [self.tools presentViewController:self.settingsMenu animated:YES completion:nil];
    }
    else if ([ZMUserActions sharedInstance].currentSelectedTabIndex == 3)
    {
        [self.events presentViewController:self.settingsMenu animated:YES completion:nil];
    }
    
}


-(void) settingsHideOnRotate {
    if(_senderForSettings) {
        if(_settingsMenuIsPresented) {
            [[_settingsMenu presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        }
    }
}

-(void) settingsShowOnRotate {
    if(_senderForSettings) {
        if(_settingsMenuIsPresented) {
            [self settingsShow:_senderForSettings];
        }
    }
}

-(void) searchHideOnRotate
{
    if(self.senderForSearchMenu)
    {
        if(self.isSearchMenuPresented)
        {
            [[self.searchMenu presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        }
    }
}

-(void) searchShowOnRotate
{
    if(self.senderForSearchMenu)
    {
        if(self.isSearchMenuPresented && self.searchTextbox.text.length <= 0)
        {
            self.searchTextbox.returnKeyType    = UIReturnKeyDone;
            self.searchTextbox.backgroundColor  = [UIColor whiteColor];
            self.searchTextbox.textColor = [UIColor blackColor];
            [self.lensButton setImage:[UIImage imageNamed:@"searchIconSelect"] forState:UIControlStateNormal];
            self.clearSearch = NO;
            [self searchMenu:self.senderForSearchMenu];
        }
    }
}

- (void)resignFromFirstResponder
{
    //Do other setup here only, if required.
    [self.searchTextbox resignFirstResponder];
}

-(IBAction)searchLenseButtonAction:(id)sender
{
    //It will make the textFieldDidBeginEditing.
    [self.searchTextbox becomeFirstResponder];
}

- (void)prepareHeaderView
{
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        [self.messagesButton setTitle:@" " forState:UIControlStateNormal];
        [self.helpButton setTitle:@" " forState:UIControlStateNormal];
        [self.profileButton setTitle:@" " forState:UIControlStateNormal];
    }
    else
    {
        [self.messagesButton setTitle:@"Messages" forState:UIControlStateNormal];
        [self.helpButton setTitle:@"Help" forState:UIControlStateNormal];
        [self.profileButton setTitle:@"Profile" forState:UIControlStateNormal];
    }
    [self adjustHeader];
    [self initializeHeaderValues];
}

- (void)initializeHeaderValues
{
    //AOFKit implementation
    self.userName.text = [NSString stringWithFormat:@"%@", [AOFLoginManager sharedInstance].displayName];
    
    self.userFranchise.text = [NSString stringWithFormat:@"%@", [AOFLoginManager sharedInstance].salesFranchise];
    
    if([self.userFranchise.text isEqualToString:@""])
        self.userFranchise.text = @"In House";
    
    //    UIApplication.sharedApplication.statusBarHidden = NO;
    //Revamp(1/12) - Change header color
    if (@available(iOS 13.0, *)) {
        self.statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
    }
    else
    {
        self.statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    }
    //Revamp(14/12)- check for login page
    NSString *firstTimeCheck = [[NSUserDefaults standardUserDefaults] objectForKey:@"First_Time_Check"];
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"First_Time_Check"] || firstTimeCheck.length <= 0) {
        if([self.userFranchise.text  isEqual: @"In House"] || [self.userFranchise.text  isEqual: @"Home Office"] ) {
            // if([franchiseName isEqual: @"In House"] || [franchiseName isEqual: @"Home Office"])
            //{
            [AbbvieLogging logInfo:@"is in house"];
            [ZMUserActions sharedInstance].isFieldUser = NO;
            [theme styleHOUser:self.statusBar];
            [theme styleHOUser:self.UserInformation];
            
            //Revamp(7/12) - Update logo
            [self updateProntoLogo];
            //End of Revamp
        } else {
            
            [AbbvieLogging logInfo:@"field user"];
            [ZMUserActions sharedInstance].isFieldUser = YES;
            if(![self.userFranchise.text  isEqual: @""]) {
                [theme styleFGUser:self.statusBar franchise:self.userFranchise.text colorCode:[ZMUserActions sharedInstance].franchiseColor];
                [theme styleFGUser:self.UserInformation franchise:self.userFranchise.text colorCode:[ZMUserActions sharedInstance].franchiseColor];
            }
            else {
                [theme styleHeader:self.statusBar];
                [theme styleHeader:self.UserInformation];
            }
            //Revamp(7/12) - Update logo
            [self updateProntoLogo];
            //End of Revamp
        }
    }
    else {
        [theme styleHeader:self.statusBar];
        [theme styleHeader:self.UserInformation];
        [self updateProntoLogo];
    }
    [[UIApplication sharedApplication].keyWindow addSubview:self.statusBar];
    //End of Revamp
    //AOFKit implementation
    self.displayName = [AOFLoginManager sharedInstance].displayName;
    
    if (self.tabBarTag == 0) {
        [self addBreadCumb:[ZMProntoManager sharedInstance].franchisesToUpdateHeader total: [ZMUserActions sharedInstance].assetsCount];
    }
    
   
    
    if(![ZMUserActions sharedInstance].searchCompletion && self.searchTextbox.text.length >=3 && [ZMUserActions sharedInstance].isServiceHitted)
    {
        
        [ZMUserActions sharedInstance].searchCompletion = NO;
        [self showingIndicator];
        
    }
    
    // if the search has been completed and no results found
    if([ZMUserActions sharedInstance].assetsCount == 0 && self.searchTextbox.text.length >= 3)
    {
        [AbbvieLogging logInfo:@"no record alert!"];
    }
    
    
    // PRONTO-11 - Button Visibility not Consistent
    [self updateSortButton];
    [self setHeaderState];
}

-(void)adjustStatusBar
{
    if(self.statusBar == nil)
    {
        [self initializeHeaderValues];
    }
    else
    {
        CGRect frame = self.statusBar.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width;
        self.statusBar.frame = frame;
    }
}

- (void)updateSortButton
{
    // New Search Implementation
    NSString *sortLabel = @"View Count";
    
    NSString* selectedValue = [ZMUserActions sharedInstance].selectedSortByMenu;
    NSString* gettingValue = @"";
    
    if([ZMProntoManager sharedInstance].sortBy.count > 0)
    {
        for (NSDictionary *sort in [ZMProntoManager sharedInstance].sortBy) {
            if ([[sort objectForKey:@"selected"] intValue] == 1) {
                sortLabel = [NSString stringWithFormat:@"Sort by: %@ ", [sort objectForKey:@"label"]];
                
                gettingValue = [sort objectForKey:@"label"];
            }
        }
        
    }
    else
    {
        for (NSDictionary *sort in [ZMUserActions sharedInstance].sorts) {
            
            if ([[sort objectForKey:@"selected"] intValue] == 1) {
                sortLabel = [NSString stringWithFormat:@"%@ ", [sort objectForKey:@"label"]];
                
                gettingValue = [sort objectForKey:@"label"];
            }
        }
    }
    
    
    if(![selectedValue isEqualToString:gettingValue])
    {
        if(self.isResetSelected)
        {
            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
            self.isResetSelected = NO;
            
        }
        else if(self.isResetFranchiseSelected)
        {
            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
            self.isResetFranchiseSelected = NO;
            
        }
        else
        {
            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
            self.isResetSelected = NO;
            
        }
    }
    else if([selectedValue isEqualToString:gettingValue])
    {
        sortLabel = [NSString stringWithFormat:@"%@ ", selectedValue];
        
    }
    else
    {
        sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
        
    }
    
    [self formatButton:@"()" withText:[NSString stringWithString:sortLabel] andButton:self.sortButton];
}

@end
