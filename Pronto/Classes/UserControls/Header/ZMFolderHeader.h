//
//  ZMFolderHeader.h
//  Pronto
//
//  Created by Sebastian Romero on 2/24/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZMLibrary;

@interface ZMFolderHeader : UIView

/**
 *  Use this class to changed Header into folder view
 */
@property (nonatomic) int folderId;
@property (weak, nonatomic) IBOutlet UILabel *folderName;
@property (weak, nonatomic) UIView *container;
@property (weak, nonatomic) ZMLibrary *library;


-(ZMFolderHeader *) initWithContainer:(UIView *)container folderId:(int)folderId folderName:(NSString *)name;
@end
