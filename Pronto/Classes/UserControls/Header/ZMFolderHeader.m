//
//  ZMFolderHeader.m
//  Pronto
//
//  Created by Sebastian Romero on 2/24/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMFolderHeader.h"
#import "ZMLibrary.h"


@implementation ZMFolderHeader

/**
 *
 **/
-(ZMFolderHeader *) initWithContainer:(UIView *)container folderId:(int)folderId folderName:(NSString *)name
{
    self = (ZMFolderHeader *)[[[NSBundle mainBundle] loadNibNamed:@"FolderHeader" owner:self options:nil] objectAtIndex:0];
    _container = container;
    _folderName.text = name;
    _folderId = folderId;
    if (_container){
        
        
        CGRect frame = self.frame;
        frame.size.width = _container.frame.size.width;
        frame.origin.x = _container.frame.size.width;
        self.frame = frame;
        
        
        [_container addSubview:self];
    }
    return self;
}

- (IBAction)backAction:(id)sender
{
    if(![_library.headerView.searchTextbox.text isEqualToString:@""]){
//        _library.actions.searchBriefcase = @"";
    }
    [self removeFromSuperview];
    [_library backToBriefcase];
}

@end
