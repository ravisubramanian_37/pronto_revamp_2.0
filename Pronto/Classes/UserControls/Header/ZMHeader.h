//
//  ZMHeader.h
//  Pronto
//
//  Created by Andres Ramirez on 2/3/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMAsset.h"
#import "ZMCustomPopoverBackgroundView.h"
#import "ZMBriefcaseMenu.h"
#import "Asset.h"
#import "Settings.h"
//Revamp(1/12) - Import Tab bar
#import "TabBarController.h"
//End of Revamp

@class ZMLibrary;
//Revamp(1/12) - Import tabbar classes
@class ZMProntoViewController;
@class Competitors;
@class ToolsTraining;
@class EventsViewController;
//End of Revamp
@class ZMAssetViewController;
@class Settings;
@class CalendarDetailVC;
@class ZMSearchMenu;


@protocol headerActions <NSObject>
@optional
-(void)closeButtonDidPress;
-(void)header:(ZMHeader*)headerView editingChanged:(NSString*)searchtext;
-(void)header:(ZMHeader*)headerView textFieldDidBeginEditing:(UITextField*)textField;
-(void)header:(ZMHeader*)headerView didTapShouldTextFieldReturn:(UITextField *)textField;
-(void)header:(ZMHeader*)headerView isDismissingSearchPopover:(UIPopoverPresentationController *)popoverPresentationController;


-(void)header:(ZMHeader*)headerView didTapNotificationButton:(UIButton*)sender;
-(void)header:(ZMHeader*)headerView presentSettings:(Settings*)settings;
-(void)header:(ZMHeader*)headerView didTapHelpButton:(UIButton*)sender;
-(void)header:(ZMHeader*)headerView didInitiateSearch:(ZMSearchMenu*)searchMenu;
@end
@interface ZMHeader : UIView <UITextFieldDelegate, ZMPopOverDelegate, UIPopoverPresentationControllerDelegate>


/**
 *  Enumerates the sections types
 */
enum ZMProntoHeaderType
{
    ZMProntoHeaderTypeLibrary = 0,
    ZMProntoHeaderTypeDetail = 1
};


@property (nonatomic) enum ZMProntoHeaderType type;

@property (weak, nonatomic) UIView *container;
@property (weak, nonatomic) ZMLibrary *library;
@property (weak, nonatomic) UIViewController *sourceViewController;
//Revamp(1/12) - object for tabbar classes
@property (weak, nonatomic) Competitors *competitors;
@property (weak, nonatomic) EventsViewController *events;
@property (weak, nonatomic) ToolsTraining *tools;
@property (weak, nonatomic) CalendarDetailVC *calendarDetailVC;
//End of Revamp
@property (weak, nonatomic) ZMAssetViewController *detail;
//Revamp(1/12) - Object for Tabbar
@property (nonatomic, strong) TabBarController *tabBar;
//End of Revamp
@property (nonatomic) int badgeNumber;
//Revamp(1/12) - propertu for tag
@property (nonatomic) int tabBarTag;
//End of Revamp
@property (nonatomic) BOOL showResumeSelection;
@property (weak, nonatomic) Asset *asset;
@property (weak, nonatomic) NSString *displayName;
@property (nonatomic, weak) id<headerActions> delegate;
//Changed:Added by Buvana: 30-Sep-2016
//Comments:
// PRONTO-12 - Folder Creation and Search
@property (nonatomic) BOOL showFolderAlert;
//End of fix
// New Search Implementation
@property (nonatomic) BOOL deleteBackward;
@property (nonatomic) BOOL isSearchAssets;
@property (nonatomic) BOOL isMakingService;
@property (nonatomic) BOOL isDefaultSort;
@property (nonatomic) BOOL isResetSelected;
@property (nonatomic) BOOL isResetFranchiseSelected;
@property (nonatomic) int counter;
@property (nonatomic) BOOL alreadySaved;
@property (nonatomic) BOOL isShowingIndicator;



/**
 * Initialize the Header with options
 * @param container UIView set the container view of the Header
 **/
- (void)initWithOptions:(UIView *)container;

/**
 *  Init header inside view
 *
 *  @param container Parent View to instance header
 */
- (void)initDetailHeaderWithOptions:(UIView *)container;

/**
 *   Init header inside view with custom frame
 *
 *  @param container Parent View to instance header
 *  @param frame     Frame to header
 */
- (void)initDetailHeaderWithOptions:(UIView *)container withFrame:(CGRect)frame;

/**
 *  Hide Header with view
 *
 *  @param view Parent view
 */
- (void)hideView:(UIView *)view;

/**
 *  Hide custom button on Header
 *
 *  @param button Button tho hide
 */
- (void)hideButton:(UIButton *)button;

/**
 *  Add custom title to header with the franchises selected
 *
 *  @param selection Array with the franchises selected
 *  @param total     Total franchises selected
 */
- (void)addBreadCumb:(NSArray *)selection total:(int)total;

/**
 *  Show custom title to define Button
 *
 *  @param stringPattern Title of button
 *  @param labelText     Title of button
 *  @param button        Button to change title
 */
- (void)formatButton:(NSString *)stringPattern withText:(NSString *)labelText andButton:(UIButton *)button;

/**
 *  Function to reset view and load defaul franchises
 */
- (void)backToLibraryState;

/**
 *  Change status header to default
 */
- (void)setHeaderState;

/**
 *  Change layout of header when rotation has changed
 */
- (void)adjustHeader;

/**
 *  Hide subviews of header
 */
- (void)hidePopovers;

/**
 *  back to default view on header
 */
- (void)resetHeader;

-(void)setDefaultSorts;

//Update Logo in Header
-(void)updateProntoLogo;

-(void)displayNotificationMessage:(UIButton *)sender;

/**
 *
 **/
-(void) changeTitleSize;
// New Search Implementation - show/hide indicator
-(void) showingIndicator;
-(void) hidingIndicator;
-(void) showIndicatorFromGlobalSearch;

- (IBAction)sort:(UIButton *)sender;


- (IBAction)likeAction:(UIButton *)sender;
// New Search Implementation
-(void)textFieldDidChange :(UITextField *) textField;
-(void) searchOffline : (UITextField*) textField;
- (IBAction)searchText:(id *)sender;
- (void)resetSelectedSort;
- (void)editingChanged:(NSString *)text;
- (IBAction)category:(UIButton *)sender;
- (NSArray*)categoryList;
//Revamp(13/12) - Badge show and hide
-(void) showHideBadge:(BOOL)hide;
//Hide for detail header
-(void)hideForAssetDetail;
//For Like action
-(void) favSelected: (BOOL) selected andAsset:(Asset *) assetVal;
//For settings popup movement
-(void) settingsHideOnRotate;
-(void) settingsShowOnRotate;
- (IBAction)closeAction:(id)sender;
- (IBAction)profileSelected:(id)sender;
- (void) searchHideOnRotate;
-(void) searchShowOnRotate;
-(void) adjustGlobalSearchIndicator;
-(void) adjustStatusBar;


//Views
@property (weak, nonatomic) IBOutlet UIView *UserInformation;
@property (weak, nonatomic) IBOutlet UIView *Browse;
@property (weak, nonatomic) IBOutlet UIView *Search;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

//Labels
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userFranchise;
@property (weak, nonatomic) IBOutlet UILabel *franchise;
@property (weak, nonatomic) IBOutlet UILabel *totalAssets;
@property (weak, nonatomic) IBOutlet UILabel *documentName;

//Popover
@property (nonatomic, strong) UIPopoverPresentationController *sortMenuPopOver;
@property (nonatomic, strong) UIPopoverPresentationController *categoryPopOver;
@property (nonatomic, strong) UIPopoverPresentationController *briefcasePopOver;
// New Search Implementation
@property (nonatomic, strong) UIPopoverPresentationController *searchMenuPopOver;
@property (nonatomic, strong) NSMutableArray * totalSearchedAssets;
@property (nonatomic, strong) NSMutableArray* keyWordArray;

//Buttons
@property (weak, nonatomic) IBOutlet UIButton *messagesButton;
@property (weak, nonatomic) IBOutlet UIButton *helpButton;
@property (weak, nonatomic) IBOutlet UIButton *briefcaseButton;
@property (weak, nonatomic) IBOutlet UIButton *libraryButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UIButton *folderButton;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (weak, nonatomic) IBOutlet UIButton *lensButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *categoryWhiteButton;
@property (weak, nonatomic) IBOutlet UIButton *addToBriefcase;
@property (weak, nonatomic) IBOutlet UITextField *searchTextbox;
@property (weak, nonatomic) IBOutlet UIView *searchBoxContainer;
@property (weak, nonatomic) IBOutlet UIButton *_likeButton;
//Revamp - Profile button outlet
@property (weak, nonatomic) IBOutlet UIButton *profileButton;
//End of Revamp

@property (weak, nonatomic) IBOutlet UIButton *searchListButton;
@property (nonatomic, weak) ZMBriefcaseMenu *briefcaseMenu;
@property (weak, nonatomic) NSTimer* searchTimer;
@property (strong, nonatomic) UIView *indicatorView;
@property (strong, nonatomic) UIActivityIndicatorView * activityIndicatorView;

// Settings Menu
@property (nonatomic, strong) Settings* settingsMenu;
@property (nonatomic) BOOL settingsMenuIsPresented;
@property (nonatomic) id senderForSettings;
@property(nonatomic, strong) UIPopoverPresentationController* settingsMenuPopOver;

- (void)resignFromFirstResponder;
- (void)doRequiredSetupOnDidSelectOfSearchMenu;

//Changes from events flow.. Later remove this comment

- (void)prepareHeaderView;

//Prepare search text field with default behaviour
- (void)enableSearchIconWithDefaultValues;

@end
