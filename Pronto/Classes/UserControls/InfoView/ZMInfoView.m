//
//  ZMInfoView.m
//  Pronto
//
//  Created by Sebastian Romero on 2/23/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMProntoTheme.h"
#import "ZMInfoView.h"
//#import <PSPDFKit/PSPDFKit.h>
#import "Pronto-Swift.h"

@interface ZMInfoView ()
{
//    __weak IBOutlet UILabel *pspdfkitVersion;
    __weak IBOutlet UIButton *closeButton;
    __weak IBOutlet UILabel *emailLabel;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *upiLabel;
    __weak IBOutlet UILabel *spaceStructureLabel;
    __weak IBOutlet UILabel *serverLabel;
    IBOutletCollection(UIImageView) NSArray *checks;
    IBOutletCollection(UIView) NSArray *lines;
    id <ZMProntoTheme> theme;
}

@end

@implementation ZMInfoView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	theme = [ZMProntoTheme sharedTheme];
    [theme toggleButton:closeButton];
    
    [theme styleDivider:((UIView *)[lines objectAtIndex:0])];
    [theme styleDivider:((UIView *)[lines objectAtIndex:1])];
    [theme styleDivider:((UIView *)[lines objectAtIndex:2])];
    [theme styleDivider:((UIView *)[lines objectAtIndex:3])];
    [theme styleDivider:((UIView *)[lines objectAtIndex:4])];
    
    [theme tintCloseCard:((UIImageView *)[checks objectAtIndex:0])];
    [theme tintCloseCard:((UIImageView *)[checks objectAtIndex:1])];
    [theme tintCloseCard:((UIImageView *)[checks objectAtIndex:2])];
    [theme tintCloseCard:((UIImageView *)[checks objectAtIndex:3])];
    [theme tintCloseCard:((UIImageView *)[checks objectAtIndex:4])];
    [theme tintCloseCard:((UIImageView *)[checks objectAtIndex:5])];
    
    
    [theme styBrightLabel:emailLabel];
    [theme styBrightLabel:nameLabel];
    [theme styBrightLabel:upiLabel];
    [theme styBrightLabel:spaceStructureLabel];
    [theme styBrightLabel:serverLabel];

    
    
    NSMutableDictionary * config = [NSMutableDictionary dictionaryWithContentsOfFile:[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:@"Pronto-Config.plist"]];
    
    
    emailLabel.text = [NSString stringWithFormat:@"Email: %@", [AOFLoginManager sharedInstance].mail];
    nameLabel.text = [NSString stringWithFormat:@"Name: %@", [AOFLoginManager sharedInstance].displayName];
    nameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    upiLabel.text = [NSString stringWithFormat:@"Upi: %@", [AOFLoginManager sharedInstance].upi];
    spaceStructureLabel.text = [NSString stringWithFormat:@"Space Structure ID: %@", [AOFLoginManager sharedInstance].spaceStructureID];
    //added by Prathap: Now this reads the data from Config file automatically based on the scheme it is being build.
    NSDictionary* serverCOnfig = config[@"ServerConfig"];
    //End of added by Prathap
    serverLabel.text = [NSString stringWithFormat:@"Server: %@", [serverCOnfig objectForKey:@"server"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
