//
//  ZMNotifications.h
//  Pronto
//
//  Created by Sebastian Romero on 2/15/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMNotificationCell.h"
//#import "ZMHeader.h"
//#import "ProntoTabbarViewController.h"
//@class ZMLibrary;
//Revamp(1/12) - Import tabbar classes
//@class Competitors;
@class EventsViewController;
//@class ToolsTraining;
@class ZMNotifications;
//End of Revamp

@protocol notificationDelegate <NSObject>

-(void)notification:(ZMNotifications*)notificationView didCloseLayout:(BOOL)closeLayout;

//-(void)notification:(ZMNotifications*)notificationView actionForGetNotifications:(UIButton*)sender;
//-(void)notification:(ZMNotifications*)notificationView actionForGetTotalNotifications:(UIButton*)sender;
-(void)notification:(ZMNotifications*)notificationView actionForChangeBadgeNumber:(int)badgeNumber;

@end

@interface ZMNotifications : UIView <UIScrollViewDelegate, ZMNotificationCellDelegate>

/**
 *  Defines the parent or container of notification view
 */
@property (weak, nonatomic) UIView *parent;
@property (weak, nonatomic) UIView *bgView;
@property (weak, nonatomic) NSArray *notificationsObject;
//@property (weak, nonatomic) ZMLibrary *library;
@property (weak, nonatomic) ZMHeader *header;
@property (weak, nonatomic) ZMNotificationCell *currentSelectedCell;
@property (weak, nonatomic) ZMNotificationCell *cellForContinuousSelection;
@property (strong, nonatomic) UIView *modalForDismissingScreen;
//Revamp(1/12) - object for tabbar classes
//@property (weak, nonatomic) Competitors *competitors;
//@property (weak, nonatomic) ToolsTraining *tools;
@property (weak, nonatomic) EventsViewController *events;
//@property (strong, nonatomic) ProntoTabbarViewController *tabBar;
@property (nonatomic) int tabBarTag;
@property (nonatomic, weak) id<notificationDelegate> delegate;
//End of Revamp
@property (nonatomic) BOOL allSelected;
@property (weak, nonatomic) IBOutlet UIScrollView *notificationsScrollView;


/**
 *  Initializes the notification panel
 *  @param parent UIVIew reference to its parent or container to be added
 */
-(void) initWithParent:(UIView *)parent background:(UIView*) bgView;

/**
 *  Populates the notifications with the given array
 *  @param notifications Array of object with the notifications
 */
-(void) populateNotifications:(NSArray *)notifications;

/**
 *  Hides notification pannel
 */
-(void) hideNotificationPannel;

//Revamp - Function definition
/**
 *  Adjusts notification according to screen turn
 */
-(void) adjustNotifications;
//End of Revamp

/**
 *  Closes the pannel without any animation
 */
-(void) closePannel;

/**
 *  Closes the detail pannel
 */
-(void) closeDetail;

/**
 *  Shows dismiss button
 */
-(void) showDismissMessagesButton;

//Revamp - Long press edit color
/**
 *  Edit on long press
 */
-(void) editOnLongPress;
//End of Revamp

-(void) changeOfOrientation;

@end
