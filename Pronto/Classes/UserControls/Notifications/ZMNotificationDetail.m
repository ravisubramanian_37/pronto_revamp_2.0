//
//  ZMNotificationDetail.m
//  Pronto
//
//  Created by Sebastian Romero on 6/30/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMNotificationDetail.h"
#import "ZMProntoTheme.h"
#import "ZMNotificationCell.h"


@interface ZMNotificationDetail ()
{
    id <ZMProntoTheme> theme;
    __weak IBOutlet UIButton *titleButton;
    __weak IBOutlet UITextView *bodyMessage;
    __weak IBOutlet UIButton *openButton;
    __weak IBOutlet UILabel *dateString;
    __weak IBOutlet UIImageView *actionIcon;
    __weak IBOutlet UIView *seperatorView;
}
@end

@implementation ZMNotificationDetail


-(ZMNotificationDetail *) initWithParent:(UIView *)parent
{
    self = (ZMNotificationDetail *)[[[NSBundle mainBundle] loadNibNamed:@"NotificationDetail" owner:self options:nil] objectAtIndex:0];
    if (!_parent){
        _parent = parent;
        [_parent addSubview:self];
    }
    return self;
}


-(void) setTitle:(NSString *)title
{
    _title = title;
    [titleButton setTitle:_title forState:UIControlStateDisabled];
    int amoutOfChars = 24;
    int totalLines = floor(_title.length / amoutOfChars);
    CGRect frame = titleButton.frame;
    frame.size.height = totalLines*26;
    titleButton.frame = frame;
    
    frame = bodyMessage.frame;
    frame.origin.y = titleButton.frame.size.height + titleButton.frame.origin.y + 18;
    bodyMessage.frame = frame;
    
    frame = openButton.frame;
    frame.origin.y = titleButton.frame.size.height + titleButton.frame.origin.y + 20;
    openButton.frame = frame;

    
}

// revamp - condition to check the notification having valid asset to display the "Open Asset" link
-(void) setAssetId:(NSString*) aId
{
    _assetId = aId;
}

-(void) setBody:(NSString *)body
{
    _body = body;
    bodyMessage.text = body;
}


-(void) setAction:(NSString *)action
{
    _action = action;
    if([_action isEqualToString:@""])
    {
        [openButton setHidden:YES];
        [actionIcon setHidden:YES];
    }
    // revamp - condition to check the notification having valid asset to display the "Open Asset" link
    else if([_action isEqualToString:@"Asset"] && [_assetId isEqualToString:@"0"])
    {
        [openButton setHidden:YES];
        [actionIcon setHidden:YES];
    }
    else {
        [openButton setHidden:NO];
        [actionIcon setHidden:NO];
        [theme tintLinkIcon:actionIcon];
        [openButton setTitle:[NSString stringWithFormat:@"Open %@", action] forState:UIControlStateNormal];
        
        CGRect frame = bodyMessage.frame;
        frame.origin.y = bodyMessage.frame.origin.y + 30;
        bodyMessage.frame = frame;
    }
}



-(void) setDate:(NSString *)date
{
    _date = date;
    dateString.text = _date;
}


- (IBAction)openAction:(UIButton *)sender
{
    if(_cell){
        [_cell getAction];
    }
}


@end
