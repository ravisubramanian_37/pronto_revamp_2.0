//
//  ZMNotificationDetail.h
//  Pronto
//
//  Created by Sebastian Romero on 6/30/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZMNotificationCell;

@interface ZMNotificationDetail : UIView
@property (weak, nonatomic) UIView *parent;

@property (weak, nonatomic) NSString *title;
@property (weak, nonatomic) NSString *body;
@property (weak, nonatomic) NSString *action;
@property (weak, nonatomic) NSString *date;
@property (weak, nonatomic) NSString *assetId;
@property (weak, nonatomic) ZMNotificationCell *cell;

-(ZMNotificationDetail *) initWithParent:(UIView *)parent;
@end
