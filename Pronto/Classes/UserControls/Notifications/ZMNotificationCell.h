//
//  ZMNotificationCell.h
//  Pronto
//
//  Created by Sebastian Romero on 2/15/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMNotification.h"
#import "AppDelegate.h"

@protocol ZMNotificationCellDelegate <NSObject>
    @optional
/**
 *  Triggered when the a cell is selected
 *  @param threadId
 */
    -(void) didSelectCell:(long)threadId;
    -(void) openAssetFromNotification:(long)assetId;
    -(void) openURLFromNotification:(NSString *)url notifID:(long)assetId;
    -(void) openPopoverNotification:(id)cell;
    -(void) openCellChecked:(id) cell;
@end

@interface ZMNotificationCell : UIView <UIGestureRecognizerDelegate>
/**
 *  Defines the parent or container of notification view
 */
@property (weak, nonatomic) UIView *parent;

/**
 *  Object who delegates its actions
 */
@property (weak, nonatomic) id<ZMNotificationCellDelegate> delegate;
@property (weak, nonatomic) NSString *notificationTitle;
@property (weak, nonatomic) NSString *notificationDescription;
/**
 *  Notification Id
 */
@property (nonatomic) long threadId;
@property (nonatomic) BOOL editable;
@property (nonatomic) BOOL selected;
@property (nonatomic) BOOL checked;
@property (weak, nonatomic) IBOutlet UIView *isNewIcon;


/**
 *  Referece to notification object
 */
@property (strong, nonatomic) ZMNotification* notification;

/**
 *  Initializes the notification panel
 *  @param parent UIVIew reference to its parent or container to be added
 *  @return instance of ZMNotificationCell
 */
-(ZMNotificationCell *) initWithParent:(UIView *)parent;

/**
 *  Hides and removes the cell after the animations completes
 */
-(void) removeCell;

-(void) markAsRead;

-(void) getAction;

-(void)dismissNotification;

-(void) markAsUnRead;

-(void) adjustFinalCell;

@end
