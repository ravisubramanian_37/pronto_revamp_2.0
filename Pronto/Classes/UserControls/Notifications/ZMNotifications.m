//
//  ZMNotifications.m
//  Pronto
//
//  Created by Sebastian Romero on 2/15/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMNotifications.h"
#import "ZMNotification.h"
#import "ZMProntoTheme.h"
//#import "ZMLibrary.h"
//#import "Competitors.h"
//#import "ToolsTraining.h"
#import "ZMBadge.h"
//Revamp(1/12) - Import tabbar classes
//#import "Competitors.h"
//#import "ToolsTraining.h"
#import "EventsViewController.h"
//End of Revamp
#import "AppDelegate.h"
#import "ZMAssetsLibrary.h"
#import "ZMNotificationDetail.h"
#import "AppDelegate.h"
#import "Pronto-Swift.h"
#import "EventsViewController.h"
#import "Constant.h"


@interface ZMNotifications ()
{
    id <ZMProntoTheme> theme;
    ZMBadge *badge;
    UIRefreshControl *refreshControl;
    UIColor *selectedLabelColor;
    UIColor *unselectedLabelColor;
    AppDelegate *mainDelegate;
    //For second Alert
    //    UIAlertController *sampleAlertMessage;
    //    UIViewController *sampleAlertViewController;
    __weak IBOutlet UIButton *dismissButton;
    __weak IBOutlet UIButton *closeButton;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UIImageView *arrow;
    
    __weak IBOutlet UIButton *dismissMessagesButton;
    __weak IBOutlet UIButton *editButton;
    //Revamp(7/12) - Cancel button definition
    __weak IBOutlet UIButton *cancelButton;
    //End of Revamp
    ZMNotificationDetail *detailView;
    __weak IBOutlet UIButton *markAsUnReadButton;
    __weak IBOutlet UIButton *markAsReadButton;
    __weak IBOutlet UILabel *noMessagesPannel;
    __weak IBOutlet UIButton *selectAllButton;
    __weak IBOutlet  UILabel* selectedItemsCount;
    __weak IBOutlet UILabel *selectedNumber;
    __weak IBOutlet UILabel *unreadLabel;
    __weak IBOutlet UILabel *readLabel;
    __weak IBOutlet UILabel *trashLabel;
    BOOL isInitial;
}
@end

@implementation ZMNotifications
@synthesize header;

-(void) initWithParent:(UIView *)parent background:(UIView*) bgView
{
    if (!_parent){
        //Revamp(1/12) - Hide mark as read btn
        mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        selectedLabelColor = [UIColor colorWithRed:57.0/255.0 green:126.0/255.0 blue:188.0/255.0 alpha:1];
        unselectedLabelColor = [UIColor colorWithRed:132.0/255.0 green:194.0/255.0 blue:222.0/255.0 alpha:1];
        //End of Revamp
        _parent = parent;
        _bgView = bgView;
        
        CGRect bgFrame = _bgView.frame;
        bgFrame.origin.y = 63;
        _bgView.frame = bgFrame;
        
        [mainDelegate showHideOverlayForTabBar:NO];
        
        isInitial = YES;
        // New Search Implementation - Notification panel height
        //Revamp(1/12) - Make notification pannel appear to right
        int padding = 63;
        //End of Revamp
        
        
        if([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] == NSOrderedAscending)
        {
            padding = 58;
        }
        CGRect frame = self.frame;
        frame.size.height = (_parent.frame.size.height) - padding;
        //Revamp - Make notification pannel appear to right
        frame.origin.x = frame.size.width*3;
        //End of Revamp
        frame.origin.y = padding;
        self.frame = frame;
        refreshControl = [[UIRefreshControl alloc] init];
        [self styleNotificationPannel];
        [_parent addSubview:_bgView];
        _modalForDismissingScreen = [[UIView alloc]initWithFrame:parent.frame];
        _modalForDismissingScreen.hidden = YES;
        [_parent addSubview:_modalForDismissingScreen];
        [_parent addSubview:self];
        [self showNotificationPannel];
    }
}


/**
 *  Styles the notification pannel
 */
-(void) styleNotificationPannel
{
    if(!theme)
    {
        theme = [ZMProntoTheme sharedTheme];
    }
    //Revamp - Change background for notification pannel
    [theme styleBlueNotificationPannel:self];
    //End of Revamp
    [theme brightRefresh:refreshControl];
}


/**
 *  Shows the notification pannel with an animation
 */
-(void) showNotificationPannel
{
    // New Revamping
    selectedItemsCount.hidden = YES;
    selectedNumber.hidden = YES;
    selectAllButton.hidden = YES;

    header.arrow.hidden = NO;
    [badge setHidden:YES];
    //End if Revamp
    _allSelected = YES;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.frame;
        // start x animation
        //Revamp - Make notification pannel appear to right
        frame.size.width = frame.size.width*2;
        frame.origin.x = _parent.frame.size.width - frame.size.width;
        //End of Revamp
        self.frame =  frame;
    } completion:nil];
    
    _modalForDismissingScreen.hidden = NO;
    UITapGestureRecognizer *touchOnView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideNotificationPannel)];
    [_modalForDismissingScreen addGestureRecognizer:touchOnView];
}


/**
 *  hides the notification pannel with an animation
 */
-(void) hideNotificationPannel
{
 
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(notification:didCloseLayout:)])
    {
        [self.delegate notification:self didCloseLayout:YES];
    }
    
    [badge setHidden:NO];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.frame;
        //Revamp - Make notification pannel appear to right
        frame.origin.x = _parent.frame.size.width;
        //End of Revamp
        self.frame =  frame;
    } completion:^(BOOL finished) {
        _parent = nil;
        _modalForDismissingScreen.hidden = YES;
        [self removeFromSuperview];
        [_bgView removeFromSuperview];
        [mainDelegate showHideOverlayForTabBar:YES];
    }];
    [ZMUserActions sharedInstance].currentSelectedCell = nil;
}

/**
 *  Closes the pannel without any animation
 */
-(void) closePannel
{
    _parent = nil;
    _modalForDismissingScreen.hidden = YES;
    [self removeFromSuperview];
}

/**
 *  Rearrages the cells of the notifications scroll view
 */
- (void)reArrangeNotifications {
    int count = 0;
    int yPosition = 0;
    for (UIView* view in _notificationsScrollView.subviews) {
        
        if ([view isKindOfClass:[ZMNotificationCell class]]) {
        
            [UIView animateWithDuration:0.5 animations:^{
                CGRect frame = view.frame;
                frame.origin.y = yPosition; //count*frame.size.height;
                view.frame =  frame;
            } completion:nil];
            count++;
            yPosition += view.frame.size.height;
        }
    }
}


/**
 *  Event triggered when the view is added to a superview
 *  @param newSuperview
 */
-(void) willMoveToSuperview:(UIView *)newSuperview
{
}

/**
 *  Populates the notifications with the given array
 *  @param notifications Array of object with the notifications
 */
-(void) populateNotifications:(NSArray *)notifications
{
    if(refreshControl)
    {
        [refreshControl endRefreshing];
    }
    //Clears the scroll view
    [_notificationsScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    ZMNotificationCell *cell;
    CGRect frame;
    int count = 0;
    int yPosition = 0;
    [noMessagesPannel setHidden:NO];
    if(notifications>0){
        [noMessagesPannel setHidden:YES];
    }
    for(ZMNotification *notification in notifications)
    {
        cell = [[ZMNotificationCell alloc] initWithParent:_notificationsScrollView];
        frame = cell.frame;
        frame.size.width = _notificationsScrollView.frame.size.width + 100;
        frame.origin.y = yPosition;
        cell.frame = frame;
        cell.delegate = self;
        cell.threadId = notification.notificationId;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"notification.subject:%@",notification.subject]];
        cell.notificationTitle = notification.subject;
        cell.notificationDescription = notification.message;
        cell.notification = notification;
        yPosition += cell.frame.size.height;
        

        count++;
    
    }
    

    
    float sizeHeight = 100;

    if (cell) {
        sizeHeight = cell.frame.origin.y + cell.frame.size.height;
    }
    _notificationsScrollView.contentSize=CGSizeMake(_notificationsScrollView.frame.size.width, sizeHeight);
    
    [self addRefreshControl];
    
    if (count == 0) {
        editButton.alpha = 0.5;
        editButton.enabled = NO;
    }
    else {
        editButton.alpha = 1.0;
        editButton.enabled = YES;
    }
    
    [self keepCellSelected];
}

//Revamp - Adjust Notifications
/**
 *  Adjusts notification according to screen turn
 */
-(void) adjustNotifications {
    if(_modalForDismissingScreen.hidden == NO) {
        CGRect frame = self.frame;
        if(frame.size.width > 650) {
            frame.size.width = 650;
        }
        frame.origin.x = _parent.frame.size.width - (frame.size.width);
        self.frame =  frame;
        
        CGRect frame1 = detailView.frame;
        frame1.origin.x = self.frame.size.width - detailView.frame.size.width;
        detailView.frame = frame1;
        if(isInitial == NO) {
            if( (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) && _bgView.frame.size.height > 1000) || (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation) && _bgView.frame.size.height < 1000)){
                CGRect frame1 = _bgView.frame;
                frame1.size.width = _bgView.frame.size.height;
                frame1.size.height = _bgView.frame.size.width;
                _bgView.frame = frame1;
                [mainDelegate adjustTabBarOverlay];
            }
        }else {
            isInitial = NO;
        }
        
    }
    
}
//End of Revamp

/**
 *  Adds the refresh control to the scroll view
 */
-(void) addRefreshControl
{
    [refreshControl addTarget:self action:@selector(controlEventValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_notificationsScrollView addSubview:refreshControl];
    _notificationsScrollView.alwaysBounceVertical = YES;
}

/**
 *  Handles the UIControlEventValueChanged event
 *  @param refreshControl
 */
- (void)controlEventValueChanged:(UIRefreshControl *)refresh{
    
    BOOL edit = NO;
    
    markAsReadButton.enabled = NO;
    readLabel.textColor = unselectedLabelColor;
    markAsUnReadButton.enabled = NO;
    unreadLabel.textColor = unselectedLabelColor;
    dismissMessagesButton.enabled = NO;
    trashLabel.textColor = unselectedLabelColor;
    selectAllButton.enabled = NO;
    selectAllButton.hidden = YES;
    //        cancelButton.hidden = YES;
    titleLabel.hidden = NO;
    closeButton.hidden = NO;
    selectedItemsCount.hidden = YES;
    selectedNumber.hidden = YES;
        
    [cancelButton setTitle:@"Edit" forState:UIControlStateNormal];

    for(UIView* view in _notificationsScrollView.subviews)
    {
        if ([view isKindOfClass:[ZMNotificationCell class]])
        {
            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            cell.editable = edit;
        }
    }
    
   
    [NotificationsRedesign.sharedInstance getNotifications];


}

/**
 *  Handles the close notification button
 *  @param sender
 */
- (IBAction)closeNotificationsPannel:(UIButton *)sender
{
    [self hideNotificationPannel];
}


- (IBAction)selectAll:(UIButton *)sender {
    for(UIView* view in _notificationsScrollView.subviews)
    {
        if ([view isKindOfClass:[ZMNotificationCell class]])
        {
            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            cell.selected = _allSelected;
        }
    }
    if(_allSelected){
       _allSelected = NO;
        [selectAllButton setTitle:@"Unselect all" forState:UIControlStateNormal];
    } else {
        _allSelected = YES;
        [selectAllButton setTitle:@"Select all" forState:UIControlStateNormal];
    }
    [self showDismissMessagesButton];
}

/**
 *  Handles dismiss all action
 *  @param sender
 */
- (IBAction)dismissAllAction:(UIButton *)sender
{
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject:@"deleteall" forKey:@"scmessageaction"];
    /**
     * @Tracking
     * Sorting
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];
    

    
    // Alternative AlertView
    UIAlertController *confirm = [UIAlertController alertControllerWithTitle:@"Please Confirm"  message:@"Do you want to dismiss all messages?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle Cancel button
                                       }];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle OK button
                                   [self performSelector:@selector(dismissMessages) withObject:nil afterDelay:1.0];
                                   
                               }];
    [confirm addAction:okButton];
    [confirm addAction:cancelButton];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
     if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
     viewController = viewController.presentedViewController;
     }
    
    [viewController presentViewController:confirm animated:YES completion:nil];

}

-(void) dismissMessages
{
    [self alertViewTag:1 clickedButtonAtIndex:1];
}

- (IBAction)dismissMessagesAction:(UIButton *)sender
{
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject:@"delete" forKey:@"scmessageaction"];
    /**
     * @Tracking
     * Sorting
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];

    // Alternative AlertView
    UIAlertController *confirm = [UIAlertController alertControllerWithTitle:@"Please Confirm"  message:@"Do you want to dismiss the current selection of messages?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle Cancel button

                                   }];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle OK button
                                   [self performSelector:@selector(dismissCurrentSelection) withObject:nil afterDelay:1.0];
                                   
                               }];
    [confirm addAction:okButton];
    [confirm addAction:cancelButton];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }

    [viewController presentViewController:confirm animated:YES completion:nil];
    //[sampleAlertViewController dismissViewControllerAnimated:NO completion:nil]
}

-(void) cancelTapped {
}

-(void) dismissCurrentSelection
{
    //    [sampleAlertViewController dismissViewControllerAnimated:NO completion:nil];
    if (selectAllButton.hidden == NO) {
        [self alertViewTag:2 clickedButtonAtIndex:1];
    }
    else {
        [_currentSelectedCell dismissNotification];
    }
}

//Revert back to old style
- (IBAction)markAsUnReadAction:(UIButton *)sender
{
    //Check if edit is tapped
    if (selectAllButton.hidden == NO) {
   
        for(UIView* view in _notificationsScrollView.subviews)
        {
            if ([view isKindOfClass:[ZMNotificationCell class]])
            {
                ZMNotificationCell *cell = (ZMNotificationCell *)view;
                if(cell.selected){
                    [cell markAsUnRead];
                    
                    markAsReadButton.enabled = YES;
                    readLabel.textColor = selectedLabelColor;
                    markAsUnReadButton.enabled = NO;
                    unreadLabel.textColor = unselectedLabelColor;
                    //                    //[markAsUnReadButton setTitle:@"Mark as read" forState:UIControlStateNormal];
                    //                    readUnreadLabel.text = @"Mark as read";
                    [self markNotificationkAsUNRead:cell];
                }
            }
        }

        
        //End of Revamp
    }
    else {
        if(_currentSelectedCell != nil) {
            [_currentSelectedCell markAsUnRead];

            markAsUnReadButton.enabled = NO;
            unreadLabel.textColor = unselectedLabelColor;
            dismissMessagesButton.enabled = NO;
            trashLabel.textColor = unselectedLabelColor;
            [self markNotificationkAsUNRead:_currentSelectedCell];
            [self makeEverythingCleared];
        }
    }
    

        [NotificationsRedesign.sharedInstance geTotalOfNotifications];

}


- (IBAction)markAsReadAction:(UIButton *)sender
{
    //Check if edit is tapped
    if (selectAllButton.hidden == NO) {
        for(UIView* view in _notificationsScrollView.subviews)
        {
            if ([view isKindOfClass:[ZMNotificationCell class]])
            {
                ZMNotificationCell *cell = (ZMNotificationCell *)view;
                if (cell.selected) {
                    
                    [cell markAsRead];
                    cell.notification.viewed = 1;
                    [[ZMAssetsLibrary defaultLibrary] updateViewedNotification:cell.notification];
                    
                    markAsReadButton.enabled = NO;
                    readLabel.textColor = unselectedLabelColor;
                    markAsUnReadButton.enabled = YES;
                    unreadLabel.textColor = selectedLabelColor;
                    
                    [self markNotificationkAsRead:cell];
                }
            }
        }
        
    }
    else {
        if(_currentSelectedCell != nil) {
            [_currentSelectedCell markAsRead];
            _currentSelectedCell.notification.viewed = 1;
            [[ZMAssetsLibrary defaultLibrary] updateViewedNotification:_currentSelectedCell.notification];
            
            markAsReadButton.enabled = NO;
            readLabel.textColor = unselectedLabelColor;
            markAsUnReadButton.enabled = YES;
            unreadLabel.textColor = selectedLabelColor;
            
            [self markNotificationkAsRead:_currentSelectedCell];
        }
    }
   
        [NotificationsRedesign.sharedInstance geTotalOfNotifications];

}

- (void)markNotificationkAsRead:(ZMNotificationCell *)cell{
    
    dispatch_async(dispatch_queue_create("Mark the notification as read", NULL), ^{
        
        [[SettingsServiceClass sharedAPIManager] markNotificationsAsRead:(int)cell.notification.notificationId complete:^(NSDictionary *notifications) {
            
            [AbbvieLogging logInfo:@"> Notification mark as read"];
            
        } error:^(NSError *error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"Error removing %@", [error localizedDescription]]];
        }];
        
    });
}

- (void)markNotificationkAsUNRead:(ZMNotificationCell *)cell{
    
    dispatch_async(dispatch_queue_create("Mark the notification as UNread", NULL), ^{
        
        [[SettingsServiceClass sharedAPIManager] markNotificationsAsUNRead:(int)cell.notification.notificationId complete:^(NSDictionary *notifications) {
            
            [AbbvieLogging logInfo:@"* Notification mark as UNREAD"];
            
        } error:^(NSError *error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"Error removing %@", [error localizedDescription]]];
        }];
        
    });
}

/**
 *  Handles the edit messages action
 *  @param sender
 */
- (IBAction)editMessages:(UIButton *)sender
{
    BOOL edit = NO;
    
    if ([[sender titleLabel].text isEqualToString:@"Edit"]) {
        
        [sender setTitle:@"Cancel" forState:UIControlStateNormal];
        edit = YES;
        [self showDismissMessagesButton];
        selectAllButton.enabled = YES;
        
    }
    else {
        markAsReadButton.enabled = NO;
        readLabel.textColor = unselectedLabelColor;
        markAsUnReadButton.enabled = NO;
        unreadLabel.textColor = unselectedLabelColor;
        dismissMessagesButton.enabled = NO;
        trashLabel.textColor = unselectedLabelColor;
        selectAllButton.enabled = NO;
        
        [sender setTitle:@"Edit" forState:UIControlStateNormal];
        
        [self checkForNotificationsNumber];
    }
    
    for(UIView* view in _notificationsScrollView.subviews)
    {
        if ([view isKindOfClass:[ZMNotificationCell class]])
        {
            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            cell.editable = edit;
        }
    }
}

//Revamp - Long press function call
-(void) editOnLongPress {
    [self showDismissMessagesButton];
    
    // New Revamping
    [self editMessages:nil];
    selectAllButton.enabled = YES;
    selectedItemsCount.hidden = NO;
    selectedNumber.hidden = NO;
    closeButton.hidden = YES;
    selectAllButton.hidden = NO;
    //Revamp(7/12) - On long Press
    titleLabel.hidden = YES;
    //End of Revamp
    
    for(UIView* view in _notificationsScrollView.subviews)
    {
        if ([view isKindOfClass:[ZMNotificationCell class]])
        {
            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            cell.editable = YES;
        }
    }
}
//End of Revamp
//Revamp(7/12) - Cancel action function definition
- (IBAction)cancelAction:(id)sender {
    if ([[sender titleLabel].text isEqualToString:@"Edit"]) {
        [sender setTitle:@"Cancel" forState:UIControlStateNormal];
        [selectAllButton setTitle:@"Select all" forState:UIControlStateNormal];
        _allSelected = YES;
        [self editOnLongPress];
    }
    else {
        [sender setTitle:@"Edit" forState:UIControlStateNormal];
        [self editMessages:nil];
        markAsReadButton.enabled = NO;
        readLabel.textColor = unselectedLabelColor;
        markAsUnReadButton.enabled = NO;
        unreadLabel.textColor = unselectedLabelColor;
        dismissMessagesButton.enabled = NO;
        trashLabel.textColor = unselectedLabelColor;
        selectAllButton.hidden = YES;
//        cancelButton.hidden = YES;
        titleLabel.hidden = NO;
        closeButton.hidden = NO;
        selectedItemsCount.hidden = YES;
        selectedNumber.hidden = YES;
        
        //Make all cells white background and unselected
        [self makeEverythingCleared];
    }
}
//End of Revamp

//Set al cell background to white and return back to normal
-(void) makeEverythingCleared {
    [detailView setHidden:YES];
    
    for(UIView* view in _notificationsScrollView.subviews)
    {
        if ([view isKindOfClass:[ZMNotificationCell class]])
        {
            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            cell.editable = NO;
            cell.selected = NO;
            cell.backgroundColor = [UIColor whiteColor];
        }
    }
    [ZMUserActions sharedInstance].currentSelectedCell = nil;
}

-(void) showReadUnread {
    markAsReadButton.enabled = NO;
    readLabel.textColor = unselectedLabelColor;
    
    markAsUnReadButton.enabled = YES;
    unreadLabel.textColor = selectedLabelColor;
    
    dismissMessagesButton.enabled = YES;
    trashLabel.textColor = selectedLabelColor;
}

-(void) showDismissMessagesButton
{
    BOOL showButton = NO;
    BOOL thereAreUnRead = NO;
    BOOL thereAreReadMessages = NO;
    int notificationsCount = 0;
    for(UIView* view in _notificationsScrollView.subviews)
    {
        if ([view isKindOfClass:[ZMNotificationCell class]])
        {
            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            if(cell.selected){
                showButton = YES;
                notificationsCount += 1;
                if(cell.notification.viewed == 1){
                    thereAreReadMessages = YES;
                } else {
                    thereAreUnRead = YES;
                }
            }
        }
    }
    selectedNumber.text = [NSString stringWithFormat:@"%d",notificationsCount];
    if(showButton){
        if(thereAreReadMessages){
            markAsUnReadButton.enabled = YES;
            unreadLabel.textColor = selectedLabelColor;
        } else {
            markAsUnReadButton.enabled = NO;
            unreadLabel.textColor = unselectedLabelColor;
        }
        if(thereAreUnRead){
            markAsReadButton.enabled = YES;
            readLabel.textColor = selectedLabelColor;

        } else {
            markAsReadButton.enabled = NO;
            readLabel.textColor = unselectedLabelColor;
        }
        if(thereAreUnRead && thereAreReadMessages){
            markAsReadButton.enabled = YES;
            readLabel.textColor = selectedLabelColor;
            markAsUnReadButton.enabled = NO;
            unreadLabel.textColor = unselectedLabelColor;

        }
        dismissMessagesButton.enabled = YES;
        trashLabel.textColor = selectedLabelColor;
    } else {
        markAsReadButton.enabled = NO;
        readLabel.textColor = unselectedLabelColor;
        markAsUnReadButton.enabled = NO;
        unreadLabel.textColor = unselectedLabelColor;
        dismissMessagesButton.enabled = NO;
        trashLabel.textColor = unselectedLabelColor;
    }
}

# pragma mark - UIScrollView Delegate

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    for(UIView* view in _notificationsScrollView.subviews)
    {
        if ([view isKindOfClass:[ZMNotificationCell class]])
        {
            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            [cell adjustFinalCell];
        }
    }
}

# pragma mark - UIAlertView Delegate

- (void)alertViewTag:(int)alertViewTag clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (alertViewTag)
    {
        case 1:
            if(buttonIndex == 1)
            {
                NSMutableArray *notifications = [[NSMutableArray alloc] init];
                for(UIView* view in _notificationsScrollView.subviews)
                {
                    if ([view isKindOfClass:[ZMNotificationCell class]])
                    {
                        ZMNotificationCell *cell = (ZMNotificationCell *)view;
                        [notifications addObject:cell.notification];
                        [cell removeCell];
                    }
                }
                
                    [NotificationsRedesign.sharedInstance dismissAllNotifications:notifications];
                    [NotificationsRedesign.sharedInstance notification:self actionForChangeBadgeNumber:0];

            }
            break;
            
        case 2:
            if(buttonIndex == 1)
            {

                NSMutableArray *notifications = [[NSMutableArray alloc] init];
                for(UIView* view in _notificationsScrollView.subviews)
                {
                    if ([view isKindOfClass:[ZMNotificationCell class]])
                    {
                        ZMNotificationCell *cell = (ZMNotificationCell *)view;
                        
                        if (cell.selected) {
                            [notifications addObject:cell.notification];
                            [cell dismissNotification];

                            [NotificationsRedesign.sharedInstance notification:self actionForChangeBadgeNumber:(int)[UIApplication sharedApplication].applicationIconBadgeNumber - 1];

                        }
                    }
                }
                [self closeDetail];
            }
            break;
    }
}

- (void)checkForNotificationsNumber{
    
    if ([_notificationsScrollView.subviews count] == 0) {
        editButton.alpha = 0.5;
        editButton.enabled = NO;
    }
    else {
        editButton.alpha = 1.0;
        editButton.enabled = YES;
    }
}


# pragma mark - ZMNotificationCellDelegate

- (void)didSelectCell:(long)threadId {
    
    [detailView setHidden:YES];
    dispatch_async(dispatch_queue_create("Mark the notification as read", NULL), ^{
    
        [[ZMAbbvieAPI sharedAPIManager] markNotificationsAsRead:(int)threadId complete:^(NSDictionary *notifications) {
            
            [AbbvieLogging logInfo:@"Notification deleted for user"];
            [self populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
            
        } error:^(NSError *error) {
            [AbbvieLogging logError:@"Couldn't mark the cell as read"];
        }];
        
        [[ZMAssetsLibrary defaultLibrary] deleteNotification:threadId];
    });
 
    [self reArrangeNotifications];
    
    /**
     * @Tracking
     * Messages
     **/
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject:@"app" forKey:@"scmsgtype"];
    [trackingOptions setObject:[NSString stringWithFormat:@"%ld", threadId] forKey:@"scmsgid"];
    [trackingOptions setObject:@"app" forKey:@"scpushnottype"];
    
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];
}

-(void) openAssetFromNotification:(long)assetId
{
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject:@"asset" forKey:@"scmsgtype"];
    /**
     * @Tracking
     * Messages
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];
    
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:@"actioncomplete" withOptions:nil];
    
    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [mainDelegate handlerNotificationAction:[NSString stringWithFormat:@"%ld", assetId] withType:ZMProntoNotificationAlertTypeAsset];

}

-(void) openURLFromNotification:(NSString *)url notifID:(long)assetId {
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]){
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];

        [trackingOptions setObject:@"url" forKey:@"scmsgtype"];
        [trackingOptions setObject:[NSString stringWithFormat:@"%ld", assetId] forKey:@"scmsgid"];
        /**
         * @Tracking
         * Messages
         **/
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];
                
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:url];
        [application openURL:URL options:@{} completionHandler:nil];
        
    } else {
        AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        [mainDelegate showMessage:@"Your url cannot be open" whithType:ZMProntoMessagesTypeWarning];
    }
}


-(void) closeDetail
{
    if(detailView){
        [UIView animateWithDuration:0.5 animations:^{
            CGRect frame = detailView.frame;
            frame.origin.x = detailView.frame.size.width * -1;
            detailView.frame = frame;
            detailView.alpha = 0;
        } completion:^(BOOL finished) {
            CGRect frame = self.frame;
            frame.size.width = frame.size.width - detailView.frame.size.width;
            self.frame = frame;
            [detailView removeFromSuperview];
            detailView = nil;
        }];
    }
}


- (void)openCellChecked:(id)cell {
    [detailView setHidden:NO];
    
    ZMNotificationCell *currentCell = (ZMNotificationCell *)cell;
    
    [NotificationsRedesign.sharedInstance geTotalOfNotifications];
  

    for (UIView* view in _notificationsScrollView.subviews) {
        
        if ([view isKindOfClass:[ZMNotificationCell class]]) {
        
            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            cell.checked = NO;
        }
    }
    currentCell.checked = YES;
    
    [self showReadUnread];
    
    if (!detailView) {
        
        detailView = [[ZMNotificationDetail alloc] initWithParent:self];
        detailView.alpha = 0;
        CGRect frame = detailView.frame;
        frame.origin.x = 200;
        frame.size.height = self.frame.size.height;
        detailView.frame = frame;
        [self addSubview:detailView];
            CGRect frame1 = detailView.frame;
            //Revamp - Make space for detail view in right
            frame1.origin.x = detailView.frame.size.width;
            //End of Revamp
            detailView.frame = frame1;
            detailView.alpha = 1;
            CGRect frame2 = self.frame;
            frame2.size.width = frame2.size.width + detailView.frame.size.width;
            self.frame = frame2;
        
    }
    
    /**
     * @Tracking
     * Messages
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:@"scmessagedetails" withOptions:nil];
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    NSString *attr = [NSString stringWithFormat:@"%ld-%@-%@", currentCell.notification.notificationId, currentCell.notification.action, currentCell.notification.date];
    [trackingOptions setObject:attr forKey:@"scmessageattr"];
    [trackingOptions setObject:[NSString stringWithFormat:@"%ld", currentCell.notification.notificationId] forKey:@"scmsgid"];
    [trackingOptions setObject:currentCell.notification.subject forKey:@"scmsgtitle"];
    NSDateFormatter *dateFormat =[[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd, yyyy hh:mm a"];
    NSString *launched=[dateFormat stringFromDate:[NSDate date]];
    [trackingOptions setObject:launched forKey:@"scmsglaunchdate"];
    [trackingOptions setObject:@"opened" forKey:@"scpushnotaction"];
    /**
     * @Tracking
     * Messages
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:nil withName:nil withOptions:trackingOptions];

    
    detailView.title = currentCell.notification.subject;
    detailView.body = currentCell.notification.message;
    detailView.cell = currentCell;
    // revamp - condition to check the notification having valid asset to display the "Open Asset" link
    detailView.assetId = [NSString stringWithFormat:@"%ld",currentCell.notification.assetId];
    detailView.date = [self dateDiff:currentCell.notification.date];
    detailView.action = currentCell.notification.action;
    _currentSelectedCell = currentCell;
    [ZMUserActions sharedInstance].currentSelectedCell = currentCell;
}

-(void) keepCellSelected
{
    for (UIView* view in _notificationsScrollView.subviews) {

        if ([view isKindOfClass:[ZMNotificationCell class]]) {

            ZMNotificationCell *cell = (ZMNotificationCell *)view;
            if(cell.threadId == [ZMUserActions sharedInstance].currentSelectedCell.threadId)
            {
                cell.checked = YES;
            }
            else
            {
                cell.checked = NO;
            }
        }
    }
    if([ZMUserActions sharedInstance].currentSelectedCell)
    {
        detailView.title = [ZMUserActions sharedInstance].currentSelectedCell.notification.subject;
        detailView.body = [ZMUserActions sharedInstance].currentSelectedCell.notification.message;
        detailView.cell = [ZMUserActions sharedInstance].currentSelectedCell;
        // revamp - condition to check the notification having valid asset to display the "Open Asset" link
        detailView.assetId = [NSString stringWithFormat:@"%ld",[ZMUserActions sharedInstance].currentSelectedCell.notification.assetId];
        detailView.date = [self dateDiff:[ZMUserActions sharedInstance].currentSelectedCell.notification.date];
        detailView.action = [ZMUserActions sharedInstance].currentSelectedCell.notification.action;
    }
}

- (NSString *)dateDiff:(NSString *)origDate {
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setFormatterBehavior:NSDateFormatterBehavior10_4];
    [df setDateFormat:@"EEE, dd MMM yy HH:mm:ss VVVV"];
    
    NSDate *convertedDate = [NSDate dateWithTimeIntervalSince1970:[origDate doubleValue]];
    NSDate *todayDate = [NSDate date];
    double ti = [convertedDate timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    if(ti < 60) {
    	return @"Just Now";
    }
    else if (ti < 3600) {
    	int diff = round(ti / 60);
    	return [NSString stringWithFormat:@"%d minutes ago", diff];
    }
    else if (ti < 86400) {
    	int diff = round(ti / 60 / 60);
    	return[NSString stringWithFormat:@"%d hours ago", diff];
    }
    else if (ti < 2629743) {
    	int diff = round(ti / 60 / 60 / 24);
    	return[NSString stringWithFormat:@"%d days ago", diff];
    }
    else {
        [df setDateFormat:@"EEE, dd MMM"];
        return [df stringFromDate:convertedDate];
    	return @"Months ago";
    }
}

-(void) changeOfOrientation
{
    if(selectAllButton.isHidden == NO)
    {
        [cancelButton setTitle:@"Edit" forState:UIControlStateNormal];
        [self editMessages:nil];
        markAsReadButton.enabled = NO;
        readLabel.textColor = unselectedLabelColor;
        markAsUnReadButton.enabled = NO;
        unreadLabel.textColor = unselectedLabelColor;
        dismissMessagesButton.enabled = NO;
        trashLabel.textColor = unselectedLabelColor;
        selectAllButton.hidden = YES;
        //        cancelButton.hidden = YES;
        titleLabel.hidden = NO;
        closeButton.hidden = NO;
        selectedItemsCount.hidden = YES;
        selectedNumber.hidden = YES;
    }
}

@end
