//
//  ZMNotificationCell.m
//  Pronto
//
//  Created by Sebastian Romero on 2/15/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMNotificationCell.h"
#import "ZMAssetsLibrary.h"
#import "ZMProntoTheme.h"
#import "ZMNotificationDetail.h"
#import "ZMNotifications.h"
#import "Pronto-Swift.h"

@interface ZMNotificationCell ()
{
    id <ZMProntoTheme> theme;
    __weak IBOutlet UILabel *title;
    __weak IBOutlet UILabel *body;
    //Revamp - Outlet for divider
    __weak IBOutlet UIView *divider;
    //End of Revamp
    __weak IBOutlet UIButton *deleteButton;
    IBOutletCollection(UIView) NSArray *diveders;
    __weak IBOutlet UIImageView *closeIcon;
    __weak IBOutlet UIImageView *arrowDocument;
    
    UIViewController *detail;
    //Revamp - Outlet for cell celection button
    __weak IBOutlet UIButton *cellSelectButton;
    //End of Revamp
    __weak IBOutlet UIButton *selectButton;
    __weak IBOutlet UIImageView *linkIcon;
    ZMNotificationDetail *detailView;
    __weak IBOutlet UIView *longSwipeView;
    double swipeStart;
    double swipeMoved;
    double swipeEnded;
}
@end


@implementation ZMNotificationCell

-(ZMNotificationCell *) initWithParent:(UIView *)parent
{
    self = (ZMNotificationCell *)[[[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil] objectAtIndex:0];
    if (!_parent){
        _parent = parent;
        [self styleNotificationCell];
        [_parent addSubview:self];
    }
    
    [self customizeMore];
    
    return self;
}
/**
*  More Customisation of Cell
*/
-(void) customizeMore{

        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapOfView:)];
        [self addGestureRecognizer:tapGestureRecognizer];
        

        
        [_isNewIcon setHidden:YES];
        //End of Revamp
}
/**
 *  Adds the styles to the cell
 */
-(void) styleNotificationCell
{
    if(!theme)
    {
        theme = [ZMProntoTheme sharedTheme];
    }
    //Revamp - Change label colour
    [theme styDarkLabel:title];
    [theme styGrayLabel:body];
    //End of Revamp
    [theme tintCloseCard:closeIcon];
    
    [theme tintLinkIcon:linkIcon];
    
    [theme tintProntoLogoCard:arrowDocument];
    [theme readUnreadIcon:self.isNewIcon];
    //Revamp - Adding color for divider
    //[theme styleDivider:((UIView *)[diveders objectAtIndex:0])];
    [theme styleDivider:divider];
    //End of Revamp
}


-(void) setNotification:(ZMNotification *)notification
{
    _notification = notification;
    //Currently the cell behaves as mail client not facebook message system
    if(_notification.viewed == 1)
    {
        [self markAsRead];

    }
    else {
        
    }
    NSString* assID = [NSString stringWithFormat:@"%ld",_notification.assetId];
    //Adding the link icon if the notification contains a message
    if([_notification.action isEqualToString:@""]){
        linkIcon.hidden = YES;
    }
    // revamp - condition to check the notification having valid asset to display the "Open Asset" link
    else if([_notification.action isEqualToString:@"Asset"] && [assID isEqualToString:@"0"])
    {
        linkIcon.hidden = YES;
    }
    else
    {
        CGRect frame = title.frame;
        frame.origin.x = frame.origin.x + 7;
        title.frame = frame;
    }
    
}


-(void) markAsRead
{
    if(theme){
        //Revamp - Change label colour
        [theme styReadLabelDark:title];
        [theme tintLinkIcon:linkIcon];
        //End of Revamp
    }
    
    [self.isNewIcon setHidden:YES];
}


-(void) markAsUnRead
{
    if(theme){
        //Revamp - Change label colour
        [theme styDarkLabel:title];
        [theme styGrayLabel:body];
        [theme tintLinkIcon:linkIcon];
        //End of Revamp
    }
    _notification.viewed = 0;
    [[ZMAssetsLibrary defaultLibrary] updateViewedNotification:_notification];
    //Hiding dot icon
    [self.isNewIcon setHidden:YES];
    self.isNewIcon.alpha = 0;
}

//Revamp - Long press function definition
-(void)handleLongPressNotification:(UILongPressGestureRecognizer *)gestureRecognizer {
    ZMNotifications *notifications = (ZMNotifications *)_parent.superview;
    [notifications editOnLongPress];
    [AbbvieLogging logInfo:@"LONG PRESS"];
}
//End of Revamp

-(void)handleTapOfView:(UITapGestureRecognizer *)gestureRecognizer {
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        if (frame.origin.x < 0) {
            frame.origin.x = 0;
            self.frame = frame;
        }
    } completion:nil];
    
    if (self.isNewIcon.hidden == NO) {
        
        [[SettingsServiceClass sharedAPIManager] markNotificationsAsRead:(int)self.notification.notificationId complete:^(NSDictionary *notifications) {
            [AbbvieLogging logInfo:@"Notification mark as read"];
            
        } error:^(NSError *error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"Error removing %@", [error localizedDescription]]];
        }];
    }
    
    [self markAsRead];
    
    _notification.viewed = 1;
    
    [[ZMAssetsLibrary defaultLibrary] updateViewedNotification:_notification];
    
    if ([_delegate respondsToSelector:@selector(openCellChecked:)]) {
        [_delegate openCellChecked:self];
    }
}

//Revamp (02/01) - Swipe Left & Right handling
-(void) handleMovement {
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        frame.origin.x = frame.origin.x - (swipeStart - swipeMoved);
        self.frame = frame;
    } completion:nil];
}

//Revamp (02/01) - Final Position swipe
-(void) adjustFinalCell {
    if(swipeStart - (swipeMoved + self.frame.origin.x) > 0) {
        if(swipeStart - (swipeMoved + self.frame.origin.x) <= 150) {
            [UIView animateWithDuration:0.2 animations:^{
                CGRect frame = self.frame;
                frame.origin.x = -100;
                self.frame = frame;
            } completion:nil];
        }
        else {

            if(swipeStart-swipeMoved > 0) {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Dismissed %f",swipeStart - (swipeMoved + self.frame.origin.x)]];
                [self dismissNotification];
            }

        }
    }
    if(swipeStart-swipeMoved <= 0) {
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = self.frame;
            frame.origin.x = 0;
            self.frame = frame;
        } completion:nil];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint gestureStartPoint = [touch locationInView:self];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Start : %f",gestureStartPoint.x]];
    swipeStart = gestureStartPoint.x;
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint gestureEndPoint = [touch locationInView:self];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"End : %f",gestureEndPoint.x]];
    
    swipeEnded = gestureEndPoint.x;
    
    [self adjustFinalCell];
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint gestureMovePoint = [touch locationInView:self];

    swipeMoved = gestureMovePoint.x;

    [self handleMovement];

}

-(void) setNotificationTitle:(NSString *)notificationTitle
{
    _notificationTitle = notificationTitle;
    title.text = _notificationTitle;
}


-(void) setNotificationDescription:(NSString *)notificationDescription
{
    _notificationDescription = notificationDescription;
    body.text = _notificationDescription;
    [body sizeToFit];
    CGRect frame = self.frame;
    frame.size.height = title.frame.size.height + body.frame.size.height + 30;
    self.frame = frame;
    CGRect frame1 = divider.frame;
    frame1.origin.y = frame.size.height - frame1.size.height;
    divider.frame = frame1;
    
    CGRect frame2 = deleteButton.frame;
    frame2.size.height = frame.size.height;
    deleteButton.frame = frame2;
    
    CGRect frame3 = longSwipeView.frame;
    frame3.size.height = frame.size.height;
    longSwipeView.frame = frame3;
}


-(void) setEditable:(BOOL)editable
{
    if(_editable == editable){
        return;
    }
    _editable = editable;
    //Revamp - UI Edit for edit pressed
    if(_editable){
        //To display selection icons at the right
        CGRect frame = selectButton.frame;
        frame.origin.y = (self.frame.size.height/2) - (frame.size.height/2);
        selectButton.frame = frame;
        //To display selection icons at the left
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = body.frame;
            CGRect titleFrame = title.frame;
            CGRect linkIconFrame = linkIcon.frame;
            linkIconFrame.origin.x = linkIconFrame.origin.x + 30;
            linkIcon.frame = linkIconFrame;
            frame.origin.x = frame.origin.x + 30;
            titleFrame.origin.x = titleFrame.origin.x + 30;
            body.frame =  frame;
            title.frame = titleFrame;
            if(self.isNewIcon.isHidden == NO){
                self.isNewIcon.alpha = 0;
            }
            selectButton.alpha = 1;
            
        } completion:nil];
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = body.frame;
            CGRect titleFrame = title.frame;
            frame.origin.x = frame.origin.x - 30;
            titleFrame.origin.x = titleFrame.origin.x - 30;
            body.frame =  frame;
            title.frame = titleFrame;
            CGRect linkIconFrame = linkIcon.frame;
            linkIconFrame.origin.x = linkIconFrame.origin.x - 30;
            linkIcon.frame = linkIconFrame;
            if(self.isNewIcon.isHidden == NO){
                self.isNewIcon.alpha = 1;
            }
            selectButton.alpha = 0;
        } completion:nil];
    }
    //End of Revamp
}


/**
 *  Hides and removes the cell after the animations completes
 */
-(void) removeCell
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.frame;
        frame.origin.x = frame.origin.x + frame.size.width;
        self.frame =  frame;
        self.alpha = 0;
    } completion:nil];
}

/**
 *  Event triggered by notification cell
 *  @param sender
 */
- (void)dismissNotification
{
    [UIView animateWithDuration:0.4 animations:^{
        CGRect frame = self.frame;
        frame.origin.x = frame.origin.x - frame.size.width;
        self.frame =  frame;
    }
    completion:^(BOOL finished) {
        [self removeFromSuperview];
        if(_delegate)
        {
            if([_delegate respondsToSelector:@selector(didSelectCell:)])
            {
                [_delegate didSelectCell:_threadId];
            }
        }
    }];
}


- (IBAction)selectAction:(UIButton *)sender
{
    ZMNotifications *pannel = (ZMNotifications *)_parent.superview;
    if(!_selected){
        _selected = YES;
        [sender setImage:[UIImage imageNamed:@"selectedButton"] forState:UIControlStateNormal];
    } else {
        _selected = NO;
        [sender setImage:[UIImage imageNamed:@"roundButton"] forState:UIControlStateNormal];
    }
    [pannel showDismissMessagesButton];
}


-(void) setSelected:(BOOL)selected
{
    _selected = selected;
    if(_selected){
        [selectButton setImage:[UIImage imageNamed:@"selectedButton"] forState:UIControlStateNormal];
    } else {
        [selectButton setImage:[UIImage imageNamed:@"roundButton"] forState:UIControlStateNormal];
    }
}

/**
 *  Monitor
 *  @param sender
 */
- (IBAction)actionMessage:(UIButton *)sender {
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.frame;
        if (frame.origin.x < 0) {
            frame.origin.x += 100;
            self.frame = frame;
        }
    } completion:nil];
    
    if (self.isNewIcon.hidden == NO) {
        
        [[SettingsServiceClass sharedAPIManager] markNotificationsAsRead:(int)self.notification.notificationId complete:^(NSDictionary *notifications) {
            [AbbvieLogging logInfo:@"Notification mark as read"];
            
        } error:^(NSError *error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"Error removing %@", [error localizedDescription]]];
        }];
    }
    
    [self markAsRead];
    
    _notification.viewed = 1;
    
    [[ZMAssetsLibrary defaultLibrary] updateViewedNotification:_notification];
    
    if ([_delegate respondsToSelector:@selector(openCellChecked:)]) {
        [_delegate openCellChecked:self];
    }
}

- (IBAction)swipeDeleteAction:(UIButton *)sender {
    UIAlertController *confirm = [UIAlertController alertControllerWithTitle:@"Please Confirm"  message:@"Do you want to dismiss the current selection of messages?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                   }];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle OK button
                                   [self performSelector:@selector(dismissCurrentSelection) withObject:nil afterDelay:1.0];
                                   
                               }];
    [confirm addAction:okButton];
    [confirm addAction:cancelButton];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    
    [viewController presentViewController:confirm animated:YES completion:nil];
}

-(void) dismissCurrentSelection
{
    [self dismissNotification];
}

-(void) getAction {
    
    if (![_notification.action isEqualToString:@""]) {

        // revamp - condition to check the notification having valid asset to display the "Open Asset" link
        if ([_notification.action isEqualToString:@"Asset"] && _notification.assetId != 0 ) {
            
            if ([_delegate respondsToSelector:@selector(openAssetFromNotification:)]) {
                [_delegate openAssetFromNotification:_notification.assetId];
            }
        }
        else if ([_notification.action isEqualToString:@"Url"] || [_notification.action isEqualToString:@"App"]) {
            
                [_delegate openURLFromNotification:_notification.url notifID:_notification.notificationId];
        }
    }
}


-(void) setChecked:(BOOL)checked
{
    _checked = checked;
    if(_checked){
        //Revamp - Change label colour
        self.backgroundColor = [UIColor colorWithRed:218.0/255.0 green:223.0/255.0 blue:232.0/255.0 alpha:1];
    } else {
        self.backgroundColor = [UIColor whiteColor];
        //End of Revamp
    }
}

@end
