//
//  ZMMessage.h
//  Pronto
//
//  Created by Sebastian Romero on 2/16/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^MessageAction)(void);

@protocol ZMProntoMessagesDelegate <NSObject>
    @optional
    -(void) messageRemoved;
@end

@class ZMLibrary;

@interface ZMMessage : UIView

/**
 *  Enumerates the message type
 */
enum ZMProntoMessages
{
    ZMProntoMessagesTypeWarning = 1,
    ZMProntoMessagesTypeAlert = 2,
    ZMProntoMessagesTypeSuccess = 3,
    ZMProntoMessagesTypeReload = 4,
    ZMProntoMessagesTypeSuccessWithoutIndicator = 5,
    ZMProntoMessagesTypeSync = 6
};

/**
 *  Defines the parent or container of notification view
 */
@property (weak, nonatomic) UIView *parent;

/**
 *  Notification message
 */
@property (strong, nonatomic) NSString *message;
@property (nonatomic) enum ZMProntoMessages type;
@property (nonatomic) ZMLibrary *library;
@property (weak, nonatomic) id<ZMProntoMessagesDelegate> delegate;


@property (nonatomic, copy) MessageAction action;

/**
 *  initializes the message
 *  @param parent theview parent
 *  @param message the message to display
 *  @param type the type of the message
 *  @return instance of the object
 */
-(ZMMessage *) initMessage:(UIView *)parent withMessage:(NSString *) message andType:(enum ZMProntoMessages)type;

/**
 *  Hides the message
 *  @param completion block that determines the complition of the hide
 */
-(void) hideMessage:(void (^)(BOOL finished))completion;

/**
 *  initializes the message with an action
 *  @param parent theview parent
 *  @param message the message to display
 *  @param type the type of the message
 *  @param block action for the retry button
 *  @return instance of the object
 */
-(ZMMessage *) initMessage:(UIView *)parent withMessage:(NSString *) message andType:(enum ZMProntoMessages)type withAction:(void(^)(void))mAction;

@end
