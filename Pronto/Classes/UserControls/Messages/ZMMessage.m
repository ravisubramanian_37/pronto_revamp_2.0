//
//  ZMMessage.m
//  Pronto
//
//  Created by Sebastian Romero on 2/16/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMProntoTheme.h"
#import "ZMMessage.h"
//#import "ZMLibrary.h"
#import "ViewUtils.h"

@interface ZMMessage ()
{
    id <ZMProntoTheme> theme;
    __weak IBOutlet UILabel *labelMessage;
    __weak IBOutlet UIImageView *closeImage;
    __weak IBOutlet UIButton *retryButton;
    __weak IBOutlet UIButton *closeButton;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    __weak IBOutlet UIImageView *errorImage;
    __weak IBOutlet UILabel *labelTitle;
    
}

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation ZMMessage

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.topView.hidden = YES;
    self.bottomView.hidden = YES;
}

- (ZMMessage *) initMessage:(UIView *)parent withMessage:(NSString *) message andType:(enum ZMProntoMessages)type {

    self = (ZMMessage *)[[[NSBundle mainBundle] loadNibNamed:@"Message" owner:self options:nil] objectAtIndex:0];
    if (!_parent){
        _parent = parent;
        _type = type;
        CGRect frame = self.frame;
        frame.size.width = _parent.frame.size.width;
        frame.origin.y = _parent.frame.size.height;
        self.frame = frame;
        _message = message;
        labelMessage.text = _message;
        if(type == ZMProntoMessagesTypeAlert){
            labelTitle.text = @"Info !";
            errorImage.image = [UIImage imageNamed:@"Error"];
        }else{
            [labelTitle setHidden:YES];
            [labelMessage setFrame:CGRectMake(labelMessage.frame.origin.x, labelMessage.frame.origin.y - 12, labelMessage.frame.size.width, labelMessage.frame.size.height)];
        }
        [self styleMessage];
        [_parent addSubview:self];
        [self showMessage];
        [retryButton setHidden:YES];
    }
    return self;
}

- (ZMMessage *) initMessage:(UIView *)parent withMessage:(NSString *) message andType:(enum ZMProntoMessages)type withAction:(void(^)(void))mAction {
    self = [self initMessage:parent withMessage:message andType:type];
    if (type == ZMProntoMessagesTypeAlert) {
        
        [closeImage setHidden:NO];
        [closeButton setHidden:NO];
        self.backgroundColor = [UIColor whiteColor];
        [_topView setBackgroundColor:[UIColor redColor]];
    }else {
        [labelTitle setHidden:YES];

    }
    _action = mAction;
    return self;
}

/**
 *  Styles the notification pannel
 */
-(void) styleMessage
{
    if(!theme)
    {
        theme = [ZMProntoTheme sharedTheme];
    }
    switch (_type) {
        case ZMProntoMessagesTypeWarning:
            [theme styleWarningMessage:self label:labelMessage];
            
            activityIndicator.hidden = YES;
            errorImage.hidden = YES;
            
            [self buttonHideControl:YES];
            
            break;
        case ZMProntoMessagesTypeAlert:
            [theme styleAlertMessage:self label:labelMessage];
            activityIndicator.hidden = YES;
            errorImage.hidden = NO;
            
            [retryButton setHidden:NO];
            [closeImage setHidden:NO];
            [closeButton setHidden:NO];
            [_topView setBackgroundColor:[UIColor redColor]];
            break;
        case ZMProntoMessagesTypeReload:
            [theme styleReloadMessage:self label:labelMessage];
            
            activityIndicator.hidden = YES;
            errorImage.hidden = YES;
            
            [self buttonHideControl:YES];
            break;
        case ZMProntoMessagesTypeSuccessWithoutIndicator:
            [theme styleSuccessMessage:self label:labelMessage];
            activityIndicator.hidden = YES;
            errorImage.hidden = YES;
            
            [self buttonHideControl:YES];
            break;
        case ZMProntoMessagesTypeSync:
            [theme styleSyncMessage:self label:labelMessage];
            activityIndicator.hidden = NO;
            [activityIndicator startAnimating];
            
            errorImage.hidden = YES;
            [theme styleActivityIndicatorColor:activityIndicator];
            [self buttonHideControl:YES];
            break;
        default:
            [theme styleSuccessMessage:self label:labelMessage];
            activityIndicator.hidden = NO;
            [activityIndicator startAnimating];
            errorImage.hidden = YES;
            
            [self buttonHideControl:YES];
            break;
    }
    
    if((_type == ZMProntoMessagesTypeWarning) || (_type == ZMProntoMessagesTypeReload)) {
        [theme tintProntoLogoCardDark:closeImage];
        [theme blackButton:closeButton];
        [theme styleActivityIndicatorColorWhite:activityIndicator];
        CGRect frame = labelMessage.frame;
        frame.origin.x = 20;
        labelMessage.frame = frame;
        self.topView.hidden = YES;
        self.bottomView.hidden = YES;
    }
    else if (_type == ZMProntoMessagesTypeSync)
    {
        closeImage.hidden = NO;
        closeButton.hidden = NO;
        
        self.topView.hidden = NO;
        self.bottomView.hidden = NO;
        
        [theme tintProntoLogoCardGold:closeImage];
        [theme goldButton:closeButton];
        
        CGRect frame = labelMessage.frame;
        frame.origin.x = 45;
        labelMessage.frame = frame;
    }
    else if (_type == ZMProntoMessagesTypeAlert)
    {
        [theme tintProntoLogoCard:closeImage];
        [theme whiteButton:closeButton];
        self.topView.hidden = NO;
        self.bottomView.hidden = NO;
    }
    else
    {
        [theme tintProntoLogoCard:closeImage];
        [theme whiteButton:closeButton];
        [theme styleActivityIndicatorColorWhite:activityIndicator];
        CGRect frame = labelMessage.frame;
        if (self.type == ZMProntoMessagesTypeSuccessWithoutIndicator)
        {
            frame.origin.x = 20;
        }
        else
        {
            frame.origin.x = 45;
        }
        labelMessage.frame = frame;
        self.topView.hidden = YES;
        self.bottomView.hidden = YES;
    }
    
}

-(void) buttonHideControl: (BOOL) retryHidden {
    if(retryHidden) {
        [retryButton setHidden:YES];
        [closeImage setHidden:NO];
        [closeButton setHidden:NO];
    }
    else {
        [retryButton setHidden:NO];
        [closeImage setHidden:YES];
        [closeButton setHidden:YES];
    }
}

/**
 *  Shows the notification pannel with an animation
 */
-(void) showMessage
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.frame;
        frame.origin.y = _parent.frame.size.height - frame.size.height;
        self.frame =  frame;
    } completion:nil];
}



/**
 *  Hides the notification pannel with an animation
 */
-(void) hideMessage:(void (^)(BOOL finished))completion
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.frame;
        frame.origin.y = _parent.frame.size.height + frame.size.height;
        self.frame =  frame;
    } completion:completion];
}




-(void) setMessage:(NSString *)message
{
    _message = message;
    [self hideMessage:^(BOOL finished) {
        labelMessage.text = _message;
        [self showMessage];
    }];
}


-(void) setType:(enum ZMProntoMessages)type
{
    _type = type;
    [self styleMessage];
}

- (IBAction)closeMessage:(UIButton *)sender
{
    [self hideMessage:^(BOOL finished) {
        if(_delegate)
        {
            if([_delegate respondsToSelector:@selector(messageRemoved)])
            {
                [_delegate messageRemoved];
            }
        }
        [self removeFromSuperview];
    }];
}


- (IBAction)retryAction:(UIButton *)sender
{
    if([labelMessage.text containsString:@"Promoted Content"])
    {

    }
    else
    {
        if (_action != nil)
        {
            _action();
        }
    }
}

@end
