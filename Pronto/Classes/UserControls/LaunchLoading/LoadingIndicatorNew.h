//
//  LoadingIndicatorNew.h
//  Pronto
//
//  Created by cccuser on 09/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DALabeledCircularProgressView.h"

@interface LoadingIndicatorNew : UIView

@property (weak, nonatomic) UIView *parent;
@property (nonatomic) float progress;
@property (weak, nonatomic) IBOutlet UIView *loadingIndicatorView;
@property (weak, nonatomic) IBOutlet UILabel *loadingIndicatorLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

- (id) initWithParent:(UIView*)parent;
-(void) setProgress:(float)progress;
-(void)adjustFrame;

@end
