//
//  ZMLaunchLoading.h
//  Pronto
//
//  Created by Andrés David Carreño on 2/21/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZMLaunchLoading : UIView

/**
 *  Class to show launch message while asset are sincrhonizing
 */

@property (weak, nonatomic) UIView *parent;
@property (weak, nonatomic) IBOutlet UILabel *loadingMessage;

- (id)initWithParent:(UIView*)parent;
- (void)startAnimation;

@end
