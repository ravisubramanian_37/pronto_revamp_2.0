//
//  LoadingIndicatorNew.m
//  Pronto
//
//  Created by cccuser on 09/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "LoadingIndicatorNew.h"

@interface LoadingIndicatorNew() {
    DALabeledCircularProgressView *progressOverlayView;
}
@end

@implementation LoadingIndicatorNew

- (id)initWithFrame:(CGRect)frame
{
    return nil;
}

// Designated Initializer
- (id) initWithParent:(UIView*)parent
{
    self = (LoadingIndicatorNew *)[[[NSBundle mainBundle] loadNibNamed:@"LoadingIndicatorNew" owner:self options:nil] objectAtIndex:0];
    
    
    if(self && !self.parent){
        self.parent = parent;
        
        self.frame = parent.frame;
        //Hiding tabbar when displaying overlay
        CGRect frame = self.frame;
        frame.size.height = frame.size.height + 54;
        self.frame = frame;
        
        CGRect frame1 = self.loadingIndicatorView.frame;
        frame1.origin.x = (parent.frame.size.width - frame1.size.width)/2;
        frame1.origin.y = (parent.frame.size.height - frame1.size.height)/2;
        frame1.origin.y += 4;
        self.loadingIndicatorView.frame = frame1;
        
        self.loadingIndicatorLabel.center = self.loadingIndicatorView.center;
        CGRect frame2 = self.loadingIndicatorLabel.frame;

        frame2.origin.y = frame1.origin.y + frame1.size.height + 8;
        self.loadingIndicatorLabel.frame = frame2;
        
        [self.activityIndicator startAnimating];
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:.5];
    }
    
    return self;
}

-(void)adjustFrame {
    //Handle orientation
    self.frame = self.parent.frame;
    CGRect frame = self.frame;
    frame.size.height = frame.size.height + 54;
    self.frame = frame;
    
    CGRect frame1 = self.loadingIndicatorView.frame;
    frame1.origin.x = (_parent.frame.size.width - frame1.size.width)/2;
    frame1.origin.y = (_parent.frame.size.height - frame1.size.height)/2;
    frame1.origin.y += 4;
    self.loadingIndicatorView.frame = frame1;
    
    self.loadingIndicatorLabel.center = self.loadingIndicatorView.center;
    CGRect frame2 = self.loadingIndicatorLabel.frame;

    frame2.origin.y = frame1.origin.y + frame1.size.height + 8;
    self.loadingIndicatorLabel.frame = frame2;
}

-(void) setProgress:(float)progress
{
    _progress = progress;
    if(!progressOverlayView){
        progressOverlayView = [[DALabeledCircularProgressView alloc] initWithFrame:_loadingIndicatorView.bounds];
        progressOverlayView.roundedCorners = YES;
        [_loadingIndicatorView addSubview:progressOverlayView];
        //        [progressOverlayView displayOperationWillTriggerAnimation];
        [progressOverlayView setProgress:progress/100 animated:YES];
    }
    
    if(_progress >= 1)
    {
        [progressOverlayView setProgress:progress animated:YES];
        progressOverlayView.progressLabel.text = [NSString stringWithFormat:@"%.0f%%", progress*100];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [progressOverlayView removeFromSuperview];
            progressOverlayView = nil;
            _loadingIndicatorLabel.text = @"";
        });
        
    } else {
        progressOverlayView.progress = _progress;
        progressOverlayView.progressLabel.text = [NSString stringWithFormat:@"%.0f%%", _progress*100];
        [progressOverlayView.progressLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:25.0f]];
    }
    
    
}

@end
