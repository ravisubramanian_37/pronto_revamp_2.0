//
//  ZMLaunchLoading.m
//  Pronto
//
//  Created by Andrés David Carreño on 2/21/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMLaunchLoading.h"
#import "ZMProntoTheme.h"



@interface ZMLaunchLoading()
{
    id <ZMProntoTheme> theme;
}

@property (weak, nonatomic) IBOutlet UIImageView *loadingImage;

@end

@implementation ZMLaunchLoading

- (id)initWithFrame:(CGRect)frame
{ 
    return nil;
}

// Designated Initializer
- (id) initWithParent:(UIView*)parent
{
    self = (ZMLaunchLoading *)[[[NSBundle mainBundle] loadNibNamed:@"ZMLaunchLoading" owner:self options:nil] objectAtIndex:0];
    
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleDidBecomeActive:)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(mainHandleEnteredBackground:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    
    if(self && !self.parent){
        self.parent = parent;
        
        theme = [ZMProntoTheme sharedTheme];
        
        [theme tintArrow:self.loadingImage];
        
        CGRect frame = self.frame;
        frame.origin.x = (parent.frame.size.width - frame.size.width)/2;
        frame.origin.y = (parent.frame.size.height - frame.size.height)/2;
        frame.origin.y += 4;
        
        self.frame = frame;
    
        
        self.backgroundColor = [UIColor colorWithRed:36.0/255.0 green:36.0/255.0 blue:36.0/255.0 alpha:.3];
        [self startAnimation];
    }
    
    return self;
}


- (void)startAnimation
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * 1 * 1 ];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    
    [self.loadingImage.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

-(void)mainHandleEnteredBackground:(id)sender{
    [self.layer removeAnimationForKey:@"rotationAnimation"];

}

-(void)handleDidBecomeActive:(id)sender{
    [self startAnimation];
//    [self runSpinAnimationOnView:self.loadingImage duration:1 rotations:1 repeat:500];
}

@end
