//
//  ZMCustomPopoverBackgroundView.h
//  Pronto
//
//  Created by Andrés David Carreño on 2/20/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZMCustomPopoverBackgroundView : UIPopoverBackgroundView

@end

@interface ZMCustomInfoPopoverBackgroundView : UIPopoverBackgroundView

@end
