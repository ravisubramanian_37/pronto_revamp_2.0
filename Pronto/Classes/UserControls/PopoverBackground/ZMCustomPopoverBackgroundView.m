//
//  ZMCustomPopoverBackgroundView.m
//  Pronto
//
//  Created by Andrés David Carreño on 2/20/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMCustomPopoverBackgroundView.h"

@interface ZMCustomPopoverBackgroundView(){
    CGFloat _arrowOffset;
    UIPopoverArrowDirection _arrowDirection;
}

@property (nonatomic, strong) UIImageView *borderImageView;
@property (nonatomic, strong) UIImageView *arrowView;

@end

#define CONTENT_INSET 0.0
//#define CAP_INSET 25.0
#define ARROW_BASE 25.0
//Pronto new revamp changes: changes arrow height from 12.5 to 10.0
#define ARROW_HEIGHT 10.0

@implementation ZMCustomPopoverBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.borderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popupBody"]];
        self.arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popupArrow"]];
        
        [self addSubview:self.borderImageView];
        [self addSubview:self.arrowView];
    }
    return self;
}

+ (BOOL)wantsDefaultContentAppearance
{
    return NO;
}

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    CGFloat _height = self.bounds.size.height;
    CGFloat _width = self.bounds.size.width;
        
    CGFloat _left = 0.0;
    CGFloat _top = 0.0;
    CGFloat _coordinate = 0.0;
    CGAffineTransform _rotation = CGAffineTransformIdentity;
    
    switch (self.arrowDirection) {
        case UIPopoverArrowDirectionUp:
            _top += ARROW_HEIGHT;
            _height -= ARROW_HEIGHT;
            _coordinate = ((_width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, 0, ARROW_BASE, ARROW_HEIGHT);
            _arrowOffset = -70.0;
            break;
            
            
        case UIPopoverArrowDirectionDown:
            _height -= ARROW_HEIGHT;
            _coordinate = ((_width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, _height, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI );
            _arrowOffset = -70.0;
            break;
            
        case UIPopoverArrowDirectionLeft:
            _left += ARROW_BASE;
            _width -= ARROW_BASE;
            _coordinate = ((self.frame.size.height / 2) + self.arrowOffset) - (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake(8, _coordinate, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( -M_PI_2 );
            break;
            
        case UIPopoverArrowDirectionRight:
            _width -= ARROW_BASE;
            _coordinate = ((self.frame.size.height / 2) + self.arrowOffset)- (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake(_width, _coordinate, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI_2 );
            
            break;
        case UIPopoverArrowDirectionAny:
        case UIPopoverArrowDirectionUnknown:
            break;
    }
    _borderImageView.frame =  CGRectMake(_left, _top, _width, _height);
    
    [_arrowView setTransform:_rotation];
    
}

- (CGFloat)arrowOffset {
    return _arrowOffset;
}

- (void)setArrowOffset:(CGFloat)arrowOffset {
    _arrowOffset = arrowOffset;
    [self setNeedsLayout];
}

- (UIPopoverArrowDirection)arrowDirection {
    return _arrowDirection;
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection {
    _arrowDirection = arrowDirection;
    [self setNeedsLayout];
}

- (void) setArrowView:(UIImageView *)arrowView {
    _arrowView = arrowView;
    [self setNeedsLayout];
}

+ (UIEdgeInsets)contentViewInsets{
    return UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
}

+ (CGFloat)arrowHeight{
    return ARROW_HEIGHT;
}

+ (CGFloat)arrowBase{
    return ARROW_BASE;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

// Pronto new revamp changes: To add background image to popover, implemented this class.
@interface ZMCustomInfoPopoverBackgroundView(){
    CGFloat _arrowOffset;
    UIPopoverArrowDirection _arrowDirection;
}

@property (nonatomic, strong) UIImageView *borderImageView;
@property (nonatomic, strong) UIImageView *arrowView;

@end

//Pronto new revamp changes: added this class to show background image as per spec

@implementation ZMCustomInfoPopoverBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.borderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popupBody"]];
        self.arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popupArrow"]];
        
        [self addSubview:self.borderImageView];
        [self addSubview:self.arrowView];
        
    }
    return self;
}

+ (BOOL)wantsDefaultContentAppearance
{
    return NO;
}

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    CGFloat _height = self.bounds.size.height;
    CGFloat _width = self.bounds.size.width;
    
    CGFloat _left = 0.0;
    CGFloat _top = 0.0;
    CGFloat _coordinate = 0.0;
    CGAffineTransform _rotation = CGAffineTransformIdentity;
    
    switch (self.arrowDirection) {
        case UIPopoverArrowDirectionUp:
            _top += ARROW_HEIGHT;
            _height -= ARROW_HEIGHT;
            _coordinate = ((_width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, 0, ARROW_BASE, ARROW_HEIGHT);
            break;
            
            
        case UIPopoverArrowDirectionDown:
            _height -= ARROW_HEIGHT;
            _coordinate = ((_width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, _height, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI );
            break;
            
        case UIPopoverArrowDirectionLeft:
            _left += ARROW_BASE;
            _width -= ARROW_BASE;
            _coordinate = ((self.frame.size.height / 2) + self.arrowOffset) - (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake(8, _coordinate, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( -M_PI_2 );
            break;
            
        case UIPopoverArrowDirectionRight:
            _width -= ARROW_BASE;
            _coordinate = ((self.frame.size.height / 2) + self.arrowOffset)- (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake(_width, _coordinate, ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI_2 );
            
            break;
        case UIPopoverArrowDirectionAny:
        case UIPopoverArrowDirectionUnknown:
            break;
    }
    _borderImageView.frame =  CGRectMake(_left, _top, _width, _height);
    
    [_arrowView setTransform:_rotation];
    
}

- (CGFloat)arrowOffset {
    return _arrowOffset;
}

- (void)setArrowOffset:(CGFloat)arrowOffset {
    _arrowOffset = arrowOffset;
    [self setNeedsLayout];
}

- (UIPopoverArrowDirection)arrowDirection {
    return _arrowDirection;
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection {
    _arrowDirection = arrowDirection;
    [self setNeedsLayout];
}

- (void) setArrowView:(UIImageView *)arrowView {
    _arrowView = arrowView;
    [self setNeedsLayout];
}

+ (UIEdgeInsets)contentViewInsets{
    return UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
}

+ (CGFloat)arrowHeight{
    return ARROW_HEIGHT;
}

+ (CGFloat)arrowBase{
    return ARROW_BASE;
}

@end
