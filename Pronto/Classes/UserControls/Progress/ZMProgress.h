//
//  ZMProgress.h
//  Pronto
//
//  Created by Sebastian Romero on 2/17/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZMProgressDelegate <NSObject>
    @optional
    -(void) progressFinished;
@end


@interface ZMProgress : UIView


/**
 *  Defines the parent or container of notification view
 */
@property (weak, nonatomic) UIView *parent;


@property (nonatomic) float progress;


@property (weak, nonatomic) id<ZMProgressDelegate> delegate;


-(ZMProgress *) initWithParent:(UIView *)parent;

/**
 *  Hides the progress
 */
-(void) hideProgress:(void (^)(BOOL finished))completion;

@end
