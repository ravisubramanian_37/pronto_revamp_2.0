//
//  ZMProgress.m
//  Pronto
//
//  Created by Sebastian Romero on 2/17/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMProgress.h"
#import "ZMProntoTheme.h"

@interface ZMProgress ()
{
    id <ZMProntoTheme> theme;
    __weak IBOutlet UIProgressView *progressBar;
}
@end

@implementation ZMProgress



-(ZMProgress *) initWithParent:(UIView *)parent
{
    self = (ZMProgress *)[[[NSBundle mainBundle] loadNibNamed:@"Progress" owner:self options:nil] objectAtIndex:0];
    if (!_parent){
        _parent = parent;
        CGRect frame = self.frame;
        frame.size.width = _parent.frame.size.width;
        frame.origin.y = _parent.frame.size.height - frame.size.height;
        self.frame = frame;
        [self styleProgress];
        [_parent addSubview:self];
    }
    return self;
}




-(void) styleProgress
{
    if(! theme)
    {
        theme = [ZMProntoTheme sharedTheme];
        
    }
}



/**
 *  Hides the progress
 */
-(void) hideProgress:(void (^)(BOOL finished))completion
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.frame;
        frame.origin.y = _parent.frame.size.height + frame.size.height;
        self.frame =  frame;
    } completion:completion];
}




/**
 *  Sets the progress
 *  @param progress
 */
-(void) setProgress:(float)progress
{
    _progress = progress;
    [progressBar setProgress:_progress animated:YES];
    if(_progress >= 1)
    {
        [self performSelector:@selector(hideProgress:) withObject:^(BOOL finished) {
            if(_delegate)
            {
                if([_delegate respondsToSelector:@selector(progressFinished)])
                {
                    [_delegate progressFinished];
                }
            }
            [self removeFromSuperview];
        } afterDelay:.5];
    }
}


@end
