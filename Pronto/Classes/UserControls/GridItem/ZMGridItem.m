//
//  ZMGridItem.m
//  Pronto
//
//  Created by Sebastian Romero on 2/4/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMGridItem.h"
#import "AppDelegate.h"
#import "ZMProntoTheme.h"
#import "ZMFolder.h"
#import "DAProgressOverlayView.h"
#import "DALabeledCircularProgressView.h"
#import "ZMAssetsLibrary.h"
#import "ZMUserActions.h"
#import "ZMTracking.h"
#import "Pronto-Swift.h"
#import "Asset.h"
#import "UIImage+ImageDirectory.h"
#import "FavFranchiseAsset.h"
#import "Constant.h"
#import "ZMUserActions.h"
#import "GlobalSearchVC.h"

@interface ZMGridItem ()
{
    id <ZMProntoTheme> theme;
    BOOL isAnimating;
    BOOL thumbLoading;
    NSTimer *thumbTimer;
    //__weak IBOutlet UIView *popularView;
    __weak IBOutlet UIView *ratingView;
    __weak IBOutlet UIButton *settingsFolderButton;
    __weak IBOutlet UIImageView *prontoLogo;
    __weak IBOutlet UIView *unreadIcon;
    __weak IBOutlet UIView *baseBackCardView;
    __weak IBOutlet UIView *backCardCanvas;
    __weak IBOutlet UIView *frontCard;
    __weak IBOutlet UIView *backCard;
    __weak IBOutlet UIImageView *closeIcon;
    __weak IBOutlet UILabel *reviewLabel;
    __weak IBOutlet UILabel *totalVotesLabel;
    __weak IBOutlet UIView *downloadFrame;
    __weak IBOutlet UIView *downloadProgress;
    __weak IBOutlet UIView *editFolderView;
    __weak IBOutlet UIImageView *editIcon;
    __weak IBOutlet UIImageView *deleteIcon;
    __weak IBOutlet UIImageView *prontoIcon;
    __weak IBOutlet UIImageView *playIcon;
    __weak IBOutlet UIView *loaderView;
    __weak IBOutlet UIImageView *popularImage;
    DALabeledCircularProgressView *progressOverlayView;
    
    __weak IBOutlet UIImageView *unreadImage;
    __weak IBOutlet UIImageView *ivLike;

    //ABT-182 Get video duration from Asset
    __weak IBOutlet UIView *videoDurationView;
    __weak IBOutlet UILabel *videoDurationLabel;
    UITextField* alertTextField;
    
  
}
@end

@implementation ZMGridItem

+ (NSString*)GetGridItemCellIdentifier
{
    return @"AssetItem";
}
+ (NSString*)GetGridItemCellNibName
{
    return @"AssetItem";
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.currentAsset = nil;
    [frontCard bringSubviewToFront:_redesignCenterImgView];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    if (progressOverlayView)
    {
        [progressOverlayView removeFromSuperview];
        progressOverlayView = nil;
    }
    self.currentAsset = nil;
}

-(void) willMoveToSuperview:(UIView *)newSuperview
{
    [self initializeTheme];
    loaderView.backgroundColor = [UIColor colorWithRed:0. green:0. blue:0. alpha:0.0];
    CGRect frame = downloadProgress.frame;
    frame.size.width = 0;
    downloadProgress.frame = frame;
}

/**
 * Initialize the theme and start the cell styling
 **/
-(void) initializeTheme
{
    if(!theme)
    {
        theme = [ZMProntoTheme sharedTheme];
        [self styleGridItem];
        [editFolderView setHidden:YES];
    }
}

-(void) showEditMode {
    editFolderView.hidden = NO;
    settingsFolderButton.hidden = YES;
    _name.hidden = YES;
    _assetCount.hidden = YES;
}

-(void) hideEditMode {
    editFolderView.hidden = YES;
    settingsFolderButton.hidden = NO;
    _name.hidden = NO;
    _assetCount.hidden = NO;
}

/**
 *
 **/
-(ZMGridItem *) initWithType:(enum ZMGridItemType)type
{
    if(!_type)
    {
        _type = type;
        switch (type)
        {
            case ZMGridItemTypeAsset:
            default:
                self = (ZMGridItem *)[[[NSBundle mainBundle]
                                       loadNibNamed:@"AssetItem" owner:self options:nil]
                                      objectAtIndex:0];
                _popularView.hidden = YES;
                break;
        }
        [self initializeTheme];
    }
    return self;
}

/**
 *
 **/
-(void) setIsPopularDocument:(BOOL)isPopularDocument
{
    _isPopularDocument = isPopularDocument;
    if(_type)
    {
        if(_type == ZMGridItemTypeAsset)
        {
           _popularView.hidden = !isPopularDocument;
            if(isPopularDocument){
                popularImage.hidden = NO;
                popularImage.image = [UIImage imageNamed:@"popular-badge"];
                
            } else{
                popularImage.hidden = YES;
            }
        }
    } else {
        _popularView.hidden = YES;
        popularImage.image = nil;
    }
}

// Check if the Update is Available
-(void) setIsUpdateAvailable:(BOOL) isAvailable
{
    if(isAvailable)
    {
        _isAnySelected = YES;
        _popularView.hidden = NO;
        _downloadButton.hidden = NO;
        _updateLabel.font = [UIFont fontWithName:@"Lato-Regular" size:14.0];
        _updateLabel.text = @"Update Available";
        _updateLabel.hidden = NO;
    }
    else
    {
        _popularView.hidden = YES;
        _downloadButton.hidden = YES;
        _statusLabel.hidden = YES;
        _updateLabel.hidden = YES;
        _isAnySelected = NO;
        
    }
        
}


// Check if the Update is Available
-(void) setIsNew:(BOOL) isItNew
{
    if(isItNew)
    {
         _popularView.hidden = NO;
        _statusLabel.hidden = NO;
        _statusLabel.backgroundColor =[UIColor colorWithRed:0.00 green:0.51 blue:0.73 alpha:1.0];
        _statusLabel.font = [UIFont fontWithName:@"Lato-Regular" size:14.0];
        //self.isNew = YES;
        if(_isAnySelected)
        {
            _updateLabel.hidden = NO;
            _updateLabel.frame = CGRectMake(232-_updateLabel.frame.size.width,_updateLabel.frame.origin.y,_updateLabel.frame.size.width,_updateLabel.frame.size.height);
            
        }

        _downloadButton.hidden = NO;
    }
    else
    {
        _isAnySelected = NO;
        _statusLabel.hidden = YES;
    }
}

/**
 *
 **/
-(void) setType:(enum ZMGridItemType)type
{
    _type = type;
    [self hideEditMode];
}

/**
 * ABT-80 Implementation
 **/
- (void)setThumbnailPath:(NSString *)thumbnailPath {
    
    _thumbnailPath = thumbnailPath;

    self.thumbnail.image = nil;
    self.thumbnail.alpha = 1.0;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[[EventsServiceClass sharedAPIManager] getThumbnailPath:_thumbnailPath]]) {
        self.thumbnail.image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:[[EventsServiceClass sharedAPIManager] getThumbnailPath:self.thumbnailPath]]];
    }
    else {

        if (!thumbLoading) {
        
            thumbLoading = YES;
            
            //Downloads the thumbnail and caches it
            [[EventsServiceClass sharedAPIManager] downloadThumbnail:_thumbnailPath complete:^(UIImage *thumbnail, NSString *p) {
                thumbLoading = NO;
            
                if (![p isEqualToString:self.thumbnailPath]) {
                    thumbnail = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:[[EventsServiceClass sharedAPIManager]getThumbnailPath:self.thumbnailPath]]];
                }

                self.thumbnail.image = thumbnail;
            
            } fromCache:YES includeWaterMark:@""];
        }
    }
    
    
    if(self.thumbnail.image == nil){
        self.assetFrame.backgroundColor = UIColor.lightGrayColor;
        self.thumbnail.image = [UIImage imageNamed:@""];
    }

}

/**
 *
 **/
-(void) styleGridItem
{
    if(_type)
    {
        switch (_type)
        {
            case ZMGridItemTypeFolder:
                [theme tintSettingsButton:settingsFolderButton];
                [theme tintProntoLogoCard:editIcon];
                [theme tintProntoLogoCard:deleteIcon];
                break;
            case ZMGridItemTypeAsset:
            default:
                [theme styleItemFrame:_assetFrame];
                [theme tintCloseCard:closeIcon];
                [theme tintProntoLogoCard:prontoLogo];
                [theme readUnreadIcon:unreadIcon];
                [theme tintProntoLogoCard:prontoIcon];
                //[theme tintProntoLogoCard:playIcon];
                break;
        }
    }
}


-(void) flip
{
    if(!self.flipped)
    {
        [UIView transitionFromView:frontCard toView:backCard
                      duration:.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft|UIViewAnimationOptionShowHideTransitionViews
                        completion:^(BOOL finished) {
                            self.flipped = YES;
                        }];
    } else {
        [UIView transitionFromView:backCard toView:frontCard
                          duration:.5
                           options:UIViewAnimationOptionTransitionFlipFromRight|UIViewAnimationOptionShowHideTransitionViews
                        completion:^(BOOL finished) {
                            self.flipped = NO;
                        }];
    }
}


- (IBAction)cancelDeleteAction:(UIButton *)sender
{
    [self flip];
    if(_delegate)
    {
        if([_delegate respondsToSelector:@selector(cellClosedOnEvent)])
        {
            [_delegate cellClosedOnEvent];
        }
    }
}


-(void) setIsVideo:(BOOL)isVideo
{
    _isVideo = isVideo;
    playIcon.hidden = YES;
    videoDurationView.hidden = !_isVideo; //ABT-182 Get video duration from Asset

}


-(void) setReviews:(int)reviews
{
    _reviews = reviews;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@","];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    reviewLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithInt:_reviews]];
}


-(void) setTotalViews:(int)totalViews
{
    _totalViews = totalViews;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@","];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    totalVotesLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithInt:_totalViews]];
    
    if(self.flipped)
    {
        [frontCard setHidden:NO];
        [backCard setHidden:YES];
        self.flipped = NO;
    }
}

//ABT-182 Get video duration from Asset

-(void)setTotalDuration:(NSString *)totalDuration{
    
    _totalDuration = totalDuration;
    videoDurationLabel.text = _totalDuration;
    
}

-(void) setProgress:(float)progress
{
    _progress = progress;
    if(!progressOverlayView){
        progressOverlayView = [[DALabeledCircularProgressView alloc] initWithFrame:loaderView.bounds];
        progressOverlayView.roundedCorners = YES;
        [loaderView addSubview:progressOverlayView];
        [progressOverlayView setProgress:progress/100 animated:YES];
    }
    
    if(_progress >= 1)
    {
        [progressOverlayView setProgress:progress animated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [progressOverlayView removeFromSuperview];
            progressOverlayView = nil;
        });
        
    } else {
        progressOverlayView.progress = progress;
        progressOverlayView.progressLabel.text = [NSString stringWithFormat:@"%.0f%%", progressOverlayView.progress*100];
        [progressOverlayView.progressLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12.0f]];
    }
    
    
}

-(void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
}

-(void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
}


-(void) highlight
{
    if(!isAnimating)
    {
        [UIView animateWithDuration:0.5 animations:^{
            isAnimating = YES;
            loaderView.backgroundColor = [UIColor colorWithRed:0. green:0. blue:0. alpha:0.5];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                loaderView.backgroundColor = [UIColor colorWithRed:0. green:0. blue:0. alpha:0.0];
            } completion:^(BOOL finished) {
                isAnimating = NO;
            }];
        }];
    }
}


-(void) setRate:(int)rate
{
    _rate = rate;
    //[_name sizeToFit];
    if (progressOverlayView) {
        [progressOverlayView removeFromSuperview];
        progressOverlayView = nil;
    }
    
    //New like logic
    if (_rate > 0) {
        [ivLike setImage:[UIImage imageNamed:@"likeYES.png"]];
    }
    else
        [ivLike setImage:[UIImage imageNamed:@"likeNO.png"]];
}


-(void) setRead:(BOOL)read
{
    _read = read;
    [unreadIcon setHidden:_read];
    //unreadImage.hidden = _read;
    if (read) {
        [unreadImage setImage:[UIImage imageNamed:@"eyeYES.png"]];
    }
    else
        [unreadImage setImage:[UIImage imageNamed:@"eyeNO.png"]];
    
}

- (IBAction)showEdit:(UIButton *)sender{
    [self showEditMode];
}

// Revamping changes for deprecated methods
- (IBAction)editFolder:(UIButton *)sender
{
    // Alternative AlertView
    UIAlertController* editFolderAlertView = [UIAlertController alertControllerWithTitle:@"Edit Folder"  message:@"Enter the new folder name."  preferredStyle:UIAlertControllerStyleAlert];
    
    [editFolderAlertView addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        alertTextField = textField;
        alertTextField.text = _name.text;
    }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle Cancel button
                                   }];
    UIAlertAction* saveButton = [UIAlertAction
                               actionWithTitle:@"Save"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle Yes button
                                   
                                   [self performSelector:@selector(editingFolder) withObject:nil afterDelay:1.0];
                                   
                               }];
    
    [editFolderAlertView addAction:cancelButton];
    [editFolderAlertView addAction:saveButton];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    [viewController presentViewController:editFolderAlertView animated:YES completion:nil];

    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return ( range.location < 15 );
}


- (IBAction)closeEditMode:(UIButton *)sender
{
    [self hideEditMode];
}

// Revamping changes for deprecated methods
- (IBAction)removeAssetFromFolder:(UIButton *)sender
{
    
    // Alternative AlertView
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Delete?"  message:@"Are you sure you want to delete this Asset from Folder?"  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle Cancel button
                                   }];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Yes"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle Yes button
                                   
                                   [self performSelector:@selector(removeAssetFromFolder) withObject:nil afterDelay:1.0];
                                   
                               }];
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    [viewController presentViewController:alert animated:YES completion:nil];

}

// Revamping changes for deprecated methods
- (IBAction)removeFolderFromBriefcase:(UIButton *)sender
{
    
    // Alternative AlertView
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Delete?"  message:@"Are you sure you want to delete this Folder?"  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle Cancel button
                               }];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Yes"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle Yes button
                                   
                                   [self performSelector:@selector(removeOnlyFolderFromBriefcase) withObject:nil afterDelay:1.0];

                               }];
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    [viewController presentViewController:alert animated:YES completion:nil];

    
}

// Revamping changes for deprecated methods
-(void) removeOnlyFolderFromBriefcase
{
    [self alertViewTag:1 clickedButtonAtIndex:1];
}

-(void) removeAssetFromFolder
{
    [self alertViewTag:2 clickedButtonAtIndex:1];
}

-(void) editingFolder
{
    [self alertViewTag:3 clickedButtonAtIndex:1];
}

// Revamping changes for deprecated methods
- (void)alertViewTag:(int)alertViewTag  clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertViewTag) {
        case 1:
            if (buttonIndex == 1) {
                
                ZMFolder *currentFolder = [[ZMFolder alloc] init];
                currentFolder.fldr_id = self.uid;
                
                currentFolder.fldr_name = self.name.text;
                [[ZMAssetsLibrary defaultLibrary] removeFolder:currentFolder];
                
                /**
                 * @Tracking
                 * Delete Folder
                 **/
                
                [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                          withSubsection:@"folder"
                                withName:[NSString stringWithFormat:@"deletefolder-%@",self.name.text]
                             withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"scbriefcaseaction":@"remove", @"sccollateralname" :self.name.text}]];
                
                
                
                if([_delegate respondsToSelector:@selector(deleteComplete:gridItem:)])
                {
                     [_delegate deleteComplete:self.name.text gridItem:self];
                }
                else
                     [_delegate deleteComplete:self.name.text];
                
                
                
            }
            break;
        case 2:
            if (buttonIndex == 1) {
                
                if([_delegate respondsToSelector:@selector(deleteAssetFromFolder:)])
                {
                    NSNumber * assetId = [NSNumber numberWithLong:self.uid];
                    NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
                    if (assetArray.count > 0) {
                        Asset *asset = assetArray[0];
                        [_delegate deleteAssetFromFolder:asset];
                    }
                }
                
            }
            break;
        case 3:
            if(buttonIndex == 1)
            {
                if([_delegate respondsToSelector:@selector(updateFolderName:folderId:)])
                {
                    
        
                    UITextField *folder = alertTextField;
                    
                    //UITextField *textFiel = [alert.textFields firstObject];
                    NSString *folderName = folder.text;
                    BOOL isDuplicated = [[ZMAssetsLibrary defaultLibrary] duplicatedFolderName:folderName];
                    if(!isDuplicated && ![folderName isEqualToString:@""]){
                        self.name.text = folder.text;
                        [self hideEditMode];
                    }
                    [_delegate updateFolderName:folderName folderId:_uid];
                }
            }
            break;
    }
}


- (void)prepareCellWithAsset:(Asset*)asset atIndexPath:(NSIndexPath*)indexPath
{
    if ([asset isKindOfClass:[Asset class]] == NO) {  return; }
    
    [AbbvieLogging logInfo:@"Preparing Assets"];
    
    self.currentAsset = asset;
    
    //Hide loader background
    [self.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
    [self.loaderBackgroundView setAlpha:1];
    self.loaderBackgroundView.hidden = YES;
    
    self.uid = asset.assetID.longValue;
    self.type = ZMGridItemTypeAsset;
    
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    self.nameLabel.text = asset.title;
    
    self.countLabel.hidden = YES;
    self.isVideo = [[asset getAssetType] isEqualToString:RedesignConstants.videoText];
    self.assetType = [asset getAssetType];
    // removing thumbnail path
    self.thumbnailPath = asset.field_thumbnail;
    
    // PRONTO-1 - Likes not Working
    self.reviews = asset.votes.intValue;
    self.totalViews = asset.view_count.intValue;
    self.totalDuration = asset.field_length;
    self.isPopularDocument = NO;
    
    // New revamping changes - icons in a single asset
    self.shareButton.tag = indexPath.item;
    self.favButton.tag = indexPath.item;
    self.infoButton.tag = indexPath.item;
    
    [self.shareButton  addTarget:self  action:@selector(exportIconButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.favButton  addTarget:self  action:@selector(favIconButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.infoButton  addTarget:self  action:@selector(infoIconButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.isNew = asset.is_new;
    
    if (![asset.path isEqualToString:@""] || [asset.path isEqualToString:RedesignConstants.brightCoveText])
    {
        self.read = YES;
    }
    else
    {
        self.read = NO;
    }
    long mostPopularAsset = [[[ZMAssetsLibrary defaultLibrary] property:@"POPULAR_ASSET"] integerValue];
    if (mostPopularAsset == asset.assetID.longValue)
    {
        self.isPopularDocument = YES;
    }
    //Revamp(4/12) - Keep fav icon pressed for the assets tapped before
    [self.favButton setImage:[UIImage GetFavIconImage] forState:UIControlStateSelected];
    [self.favButton setSelected:NO];
    NSString *currentBucketName = @"";
    
    if([ZMUserActions sharedInstance].globalSearchViewController)
    {
        currentBucketName = [[ZMUserActions sharedInstance].globalSearchViewController getBuckeNameFromSelectedTab];
    }
    else
    {
        currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
    }

}

- (IBAction)exportIconButtonAction:(UIButton*)sender
{
    if (sender.isSelected == NO)
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(gridCell:configureExportPopOver:)])
        {
            [self.delegate gridCell:self configureExportPopOver:sender];
        }
    }
    [self makeShareButtonSelected:!sender.isSelected];
}

- (void)makeShareButtonSelected:(BOOL)yesOrNo
{
    if (yesOrNo == YES)
    {
        [self.shareButton setImage:[UIImage GetExportIconSelectedImage] forState:UIControlStateSelected];
        [self.shareButton setSelected:YES];
    }
    else
    {
        [self.shareButton setImage:[UIImage GetExportIconImage] forState: UIControlStateNormal];
        [self.shareButton setSelected:NO];
    }
}

- (IBAction)infoIconButtonAction:(UIButton*)sender
{
    // Pronto new revamp changes: setting selected/deselected info button's image
    if (sender.isSelected == NO)
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(gridCell:configureInfoPopOver:)])
        {
            [self.delegate gridCell:self configureInfoPopOver:sender];
        }
    }
    [self makeInfoButtonSelected:!sender.isSelected];
}

- (void)makeInfoButtonSelected:(BOOL)yesOrNo
{
    if (yesOrNo == YES)
    {
        [self.infoButton setImage:[UIImage GetInfoIconSelectedImage] forState:UIControlStateSelected];
        [self.infoButton setSelected:YES];
    }
    else
    {
        [self.infoButton setImage:[UIImage GetInfoIconImage] forState: UIControlStateNormal];
        [self.infoButton setSelected:NO];
    }
}

- (IBAction)favIconButtonAction:(UIButton*)sender
{
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(gridCell:configureFavButton:)])
    {
        [self.delegate gridCell:self configureFavButton:sender];
    }
}


@end
