//
//  ZMGridItem.h
//  Pronto
//
//  Created by Sebastian Romero on 2/4/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Asset.h"

@class ZMAsset;
@class ZMGridItem;
@class Asset;

@protocol ZMGridItemDelegate <NSObject>
    @optional
    -(void) cellClosedOnEvent;
    -(void) deleteComplete:(NSString *)folderName gridItem:(ZMGridItem *)gridItem;
    -(void) deleteComplete:(NSString *)folderName;
    -(void) deleteAssetFromFolder:(Asset *)asset;
    -(void) updateFolderName:(NSString *)folderName folderId:(long)folderId;

    - (void)gridCell:(ZMGridItem*)cell  configureExportPopOver:(UIButton*) sender;
    - (void)gridCell:(ZMGridItem*)cell  configureInfoPopOver:(UIButton*) sender;
    - (void)gridCell:(ZMGridItem*)cell  configureFavButton:(UIButton*) sender;

@end

@interface ZMGridItem : UICollectionViewCell <UITextFieldDelegate>

enum ZMGridItemType
{
    ZMGridItemTypeDefault,
    ZMGridItemTypeAsset,
    ZMGridItemTypeFolder
};


enum ZMGridAssetType
{
    ZMGridAssetTypeVideo = 0,
    ZMGridAssetTypeDocument = 1,
    ZMGridAssetTypeEpub = 2,
    ZMGridAssetTypeWeblink = 3 // PRONTO-25 - Web View - Ability to provide a Web link as an asset type.
};



/**
 * Outlets
 **/
@property (weak, nonatomic) IBOutlet UIImageView *centerTopAssetImage;
@property (weak, nonatomic) IBOutlet UILabel *redesignDotLabel;
@property (weak, nonatomic) IBOutlet UIImageView *redesignCenterImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *views;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UIImageView *viewIcon;
@property (weak, nonatomic) IBOutlet UIView *assetFrame;

@property (weak, nonatomic) IBOutlet UIButton *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *assetCount;
// New Revamping Changes
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *favButton;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
// Only for New/Update Available need to show below fields
@property (weak, nonatomic) IBOutlet UIButton * downloadButton;
@property (weak, nonatomic) IBOutlet UILabel * statusLabel;
@property (weak, nonatomic) IBOutlet UILabel * updateLabel;
@property (weak, nonatomic) IBOutlet UIView *popularView;
@property (strong, nonatomic) IBOutlet UIView *loaderBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *audioPlayerView;
@property (weak, nonatomic) IBOutlet UIView *assetBackGroundView;

/**
 *
 **/
@property (weak, nonatomic) NSString *thumbnailPath;
@property (nonatomic) enum ZMGridItemType type;
@property (nonatomic) BOOL isPopularDocument;
@property (nonatomic) BOOL isUpdateAvailable;
@property (nonatomic) BOOL isNew;
@property (nonatomic) BOOL isAnySelected;
@property (nonatomic) int rate;
@property (weak, nonatomic) id<ZMGridItemDelegate> delegate;
@property (weak, nonatomic) NSString *assetType;
@property (nonatomic) int reviews;
@property (nonatomic) int totalViews;
@property (nonatomic) long uid;
@property (nonatomic) float progress;
@property (nonatomic) BOOL read;
@property (nonatomic) BOOL isVideo;
@property (nonatomic) BOOL flipped;
@property (nonatomic) NSString * totalDuration; //ABT-182 Get video duration from Asset

@property (nonatomic, strong) Asset *currentAsset;

+ (NSString*)GetGridItemCellIdentifier;
+ (NSString*)GetGridItemCellNibName;
- (void)prepareCellWithAsset:(Asset*)asset atIndexPath:(NSIndexPath*)indexPath;
- (void)makeShareButtonSelected:(BOOL)yesOrNo;
- (void)makeInfoButtonSelected:(BOOL)yesOrNo;



-(ZMGridItem *) initWithType:(enum ZMGridItemType)type;
-(void) flip;
-(void) highlight;
- (void)alertViewTag:(int)alertViewTag  clickedButtonAtIndex:(NSInteger)buttonIndex;
@end
