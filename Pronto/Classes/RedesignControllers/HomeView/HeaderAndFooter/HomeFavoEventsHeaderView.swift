//
//  HomeFavoEventsHeaderView.swift
//  Pronto
//
//  Created by Saranya on 23/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class HomeFavoEventsHeaderView:UIView
{
    @IBOutlet weak var headerTotalCountLabel: UILabel!
    @IBOutlet weak var headerTitleLabel
    : UILabel!
    
    override func awakeFromNib() {
    super.awakeFromNib()
    
    }
    func setThemes(){
        self.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        doInitIfRequired()
    }
    func doInitIfRequired(){        
        let count:NSNumber = NSNumber(integerLiteral: ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.count ?? 0)
        if (count.intValue <= 1)
        {
            headerTotalCountLabel.text = String(count.intValue) + RedesignConstants.assetTxt
        }
        else
        {
            headerTotalCountLabel.text = String(count.intValue) + RedesignConstants.assetsTxt
        }
    }
}
