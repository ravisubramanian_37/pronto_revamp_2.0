//
//  HomeMainHeaderView.swift
//  Pronto
//
//  Created by Saranya on 17/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import UIKit

protocol FavSearchHistoryProtocol {
  func addSearchListView(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int)
}

protocol CategorySearchHistoryProtocol {
  func categorySearchListView(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int)
}

protocol SearchResultsHistoryProtocol {
  func addSearchListView(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int)
}

@objc public protocol EventsResultsHistoryProtocol {
  @objc func addSearchListView(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int)
}

@objc class HomeMainHeaderView: UIView,UIPopoverPresentationControllerDelegate, UISearchBarDelegate {
  @IBOutlet weak var userTypeLabel: UILabel!
  @IBOutlet weak var searchBarHomeScreen: UISearchBar!
  @IBOutlet weak var magnifierBtn: UIButton!
  @IBOutlet weak var helpButton: UIButton!
  @IBOutlet weak var notificationButton: UIButton!
  @IBOutlet weak var settingsIcon: UIButton!
  @IBOutlet weak var userNameLabel: UILabel!
  @IBOutlet weak var franchiseTypeLabel: UILabel!
  @IBOutlet weak var notificationArrow: UIImageView!
  var badge:ZMBadge?
  
  @IBOutlet weak var refreshButton: UIButton!
  @objc var helpOptions:Bool = false
  @objc var helpViewController:HelpViewController?
  @objc static let sharedInstance:HomeMainHeaderView = createSharedInstance()
  var delegate: FavSearchHistoryProtocol?
  var categoryDelegate: CategorySearchHistoryProtocol?
  var searchResultsDelegate: SearchResultsHistoryProtocol?
  @objc var eventsDelegate: EventsResultsHistoryProtocol?
  var searchText = ""
  @objc var flowType: String = ""
  
  override func awakeFromNib() {
    super.awakeFromNib()
    NotificationCenter.default.addObserver(self, selector: #selector(clearSearchFieldText),
                                           name: NSNotification.Name(rawValue: RedesignConstants.clearSearchTextNotification),
                                           object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(dismissSearchListView),
                                           name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewSearchListNotification),
                                           object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(dismissSearchListView),
                                           name: NSNotification.Name(rawValue: RedesignConstants.removeCategoryViewSearchListNotification),
                                           object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(dismissSearchListView),
                                           name: NSNotification.Name(rawValue: RedesignConstants.removeSearchResultsListNotification), object: nil)
    
    let dismissSearchList = UITapGestureRecognizer(target: self,
                                                   action: #selector(dismissSearchListView))
    self.addGestureRecognizer(dismissSearchList)
  }
  /* Method to setup themes for Header */
  @objc func setThemes(){
    self.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
    self.refreshButton.isHidden = true
    doInitIfRequired()
  }
  /* Method to enable Refresh */
  func enableRefreshButton(_ :Bool){
    self.refreshButton.isHidden = false
  }
  @objc func clearSearchFieldText() {
    searchBarHomeScreen.text = ""
  }
  /* Method to create singleton Object */
  class func createSharedInstance()-> HomeMainHeaderView{
    Bundle.main.loadNibNamed(RedesignConstants.mainViewHeaderNib, owner: self, options: nil)?.first as! HomeMainHeaderView
  }
  /* Method to configure header for open Asset Option */
  @objc func configureHeaderForAssetPage(){
    searchBarHomeScreen.isHidden = true
    notificationButton.isHidden = true
    helpOptions = true
    magnifierBtn.isHidden = true
    UserDefaults.standard.set(true, forKey: RedesignConstants.settingsOptionForAsset)
  }
  /* Method to set up header view */
  func doInitIfRequired(){
    searchBarHomeScreen.layer.cornerRadius = 20
    searchBarHomeScreen.barTintColor = UIColor.white
    
    if AOFLoginManager.sharedInstance.salesFranchiseID.count > 0{
      franchiseTypeLabel.text = AOFLoginManager.sharedInstance.salesFranchise
      userTypeLabel.isHidden = true
    }
    else{
      userTypeLabel.isHidden = false
      userNameLabel.isHidden = true
      franchiseTypeLabel.isHidden = true
    }
    setUpBadgeForNotifications()
    configureSearch()
  }
  /* Configure Search Bar */
  func configureSearch(){
    if #available(iOS 13.0, *) {
      searchBarHomeScreen.searchTextField.text = searchText
      searchBarHomeScreen.searchTextField.leftViewMode = UITextField.ViewMode.never
      searchBarHomeScreen.delegate = self
    } else {
      let textfield:UITextField = searchBarHomeScreen.subviews[0].subviews[2] as! UITextField
      textfield.text = searchText
      textfield.leftView = nil
      searchBarHomeScreen.delegate = self
    }
    magnifierBtn.isHidden = searchText != "" ? true : false
  }
  
  /* Method to setup Badgenumber */
  func setUpBadgeForNotifications(){
    badge = ZMBadge.init(options: "0", parent: notificationButton, franchise: franchiseTypeLabel.text)
    showHideBadge(value: true)
  }
  @objc func setBadgeNumber(badgeNumber: Int) {
    var badgeText = "\(badgeNumber)"
    if badgeNumber == 0 {
      badgeText = ""
      showHideBadge(value: true)
    } else {
      showHideBadge(value: false)
    }
    badge?.label.text = badgeText
  }
  /* Method to show/hide badge */
  @objc func showHideBadge(value:Bool){
    if (value){
      badge?.isHidden = true
    }else{
      if((badge?.label.text == "")||(badge?.label.text == "0")){
        badge?.isHidden = true;
      }
      else{
        badge?.isHidden = false
      }
    }
  }
  /* Method invoked on tapping Settings */
  @IBAction func settingsTapped(_ sender: Any) {
    let settingsViewController:SettingsViewController = SettingsViewController()
    settingsViewController.helpOption = helpOptions
    settingsViewController.flowType = self.flowType
    let popoverContent = settingsViewController as UIViewController
    popoverContent.modalPresentationStyle = .popover
    
    if let popover = popoverContent.popoverPresentationController {
      popover.backgroundColor = .white
      let viewForSource = sender as! UIView
      popover.sourceView = viewForSource
      popover.permittedArrowDirections = .up
      popover.sourceRect = CGRect(x: 50, y: viewForSource.bounds.maxY - viewForSource.bounds.minY - 10, width: 0, height: 0)
      // the size you want to display
      if(ZMUserActions.sharedInstance()!.isFieldUser){
        popoverContent.preferredContentSize = CGSize(width: RedesignConstants.settingsWidth,height: RedesignConstants.settingsHeight - 50)
      }else{
        popoverContent.preferredContentSize = CGSize(width: RedesignConstants.settingsWidth,height: RedesignConstants.settingsHeight)
      }
      popover.delegate = self
      if(helpOptions){
        if let helpController = helpViewController{
          helpController.navigationController?.present(popoverContent, animated: true, completion: nil)
        }else{
          //Asset open Case
          RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: popoverContent, animated: true)
        }
      }
      else{
        RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: popoverContent, animated: true)
      }
    }
    dismissSearchListView()
  }
  
  func searchPopOver(_ sender: Any) {
    let searchHistoryViewController: SearchHistoryViewController = SearchHistoryViewController()
    searchHistoryViewController.width = searchBarHomeScreen.frame.size.width - 36
    searchHistoryViewController.height = CGFloat((UserDefaults.standard.array(forKey: "keywordTxt") ?? [""]).count * 36) + 48
    let popoverContent = searchHistoryViewController as UIViewController
    popoverContent.modalPresentationStyle = .popover
    if let popover = popoverContent.popoverPresentationController {
      popover.backgroundColor = .white
      popover.sourceView = self
      popoverContent.preferredContentSize = CGSize(width: searchBarHomeScreen.frame.size.width - 36,
                                                   height: CGFloat((UserDefaults.standard.array(forKey: "keywordTxt") ?? [""]).count * 36) + 26)
      popover.delegate = self
      RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: popoverContent, animated: true)
    }
  }
  
  //Method to define the presentation style for popover presentation controller
  func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
    return .none
  }
  /* Method invoked on tapping notifications in header */
  @IBAction func notificationsTapped(_ sender: Any) {
    NotificationsRedesign.sharedInstance().didTapNotificationButton(self.superview?.superview ?? HomeViewController.sharedInstance.view )
    dismissSearchListView()
  }
  /* Method toconfigure option for Help View */
  @objc func configureHeaderForHelp(headerView:HomeMainHeaderView){
    if(helpOptions){
      headerView.notificationButton.isHidden = true
      headerView.searchBarHomeScreen.isHidden = true
    }
    else{
      headerView.notificationButton.isHidden = false
      headerView.searchBarHomeScreen.isHidden = false
    }
  }
  
  @objc func dismissSearchListView() {
    searchBarHomeScreen.endEditing(true)
    magnifierBtn.isHidden = true
    HomeViewController.sharedInstance.dismissView()
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewControllerSearchListNotification), object: self, userInfo: nil)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeCategoryViewControllerSearchListNotification), object: self, userInfo: nil)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeSearchResultsVCListNotification), object: self, userInfo: nil)
    EventsViewController.sharedInstance().dismissView()
  }
  /* Search Bar Delegates */
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    magnifierBtn.isHidden = true
    HomeViewController.sharedInstance.setUpSearchList(xAxis: searchBarHomeScreen.frame.origin.x,
                                                      yAxis: searchBarHomeScreen.frame.origin.y,
                                                      width: (searchBarHomeScreen.frame.size.width - 36),
                                                      height: (UserDefaults.standard.array(forKey: "keywordTxt") ?? [""]).count * 36)
    self.delegate?.addSearchListView(xAxis: searchBarHomeScreen.frame.origin.x,
                                     yAxis: searchBarHomeScreen.frame.origin.y,
                                     width: (searchBarHomeScreen.frame.size.width - 48),
                                     height: (UserDefaults.standard.array(forKey: "keywordTxt") ?? [""]).count * 36)
    self.categoryDelegate?.categorySearchListView(xAxis: searchBarHomeScreen.frame.origin.x,
                                                  yAxis: searchBarHomeScreen.frame.origin.y,
                                                  width: (searchBarHomeScreen.frame.size.width - 48),
                                                  height: (UserDefaults.standard.array(forKey: "keywordTxt") ?? [""]).count * 36)
    self.searchResultsDelegate?.addSearchListView(xAxis: searchBarHomeScreen.frame.origin.x,
                                                  yAxis: searchBarHomeScreen.frame.origin.y,
                                                  width: (searchBarHomeScreen.frame.size.width - 36),
                                                  height: (UserDefaults.standard.array(forKey: "keywordTxt") ?? [""]).count * 36)
    self.eventsDelegate?.addSearchListView(xAxis: searchBarHomeScreen.frame.origin.x,
                                           yAxis: searchBarHomeScreen.frame.origin.y,
                                           width: (searchBarHomeScreen.frame.size.width - 36),
                                           height: (UserDefaults.standard.array(forKey: "keywordTxt") ?? [""]).count * 36)
  }
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    //make Search Service Call
    let searchModel:SearchViewModel = SearchViewModel()
    
    if (searchBar.text?.count ?? 0) > 2 {
      searchModel.doSearch(searchText: searchBar.text ?? "")
    }
    dismissSearchListView()
  }
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    magnifierBtn.isHidden = false
    dismissSearchListView()
  }
  @IBAction func refreshTapped(_ sender: Any) {
    ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs = []
    HomeViewModel.sharedInstance.fetchFranchiseListInBackground()
    HomeViewController.sharedInstance.loadActualView()
    dismissSearchListView()
  }
}
