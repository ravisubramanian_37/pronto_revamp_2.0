//
//  HomeViewModel.swift
//  Pronto
//
//  Created by Saranya on 12/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class HomeViewModel: NSObject {
    
    @objc static let sharedInstance = HomeViewModel()
    /* While invoking parallel service calls remember not to use Singleton Class objects. Since it may cause some issues */
    var isFolderDetailsFetched = false
    var isPromotedContentService = false
    var isFavourtiesService = false
    var isEventsService = false
    var isHomeCategoriesService = false
    @objc var flowType: String = ""
    var progressText = ""
    
    /* Method to load Promoted Contents */
    func loadPromotedContents() {
      if AOFLoginManager.sharedInstance.salesFranchise.count == 0 {
        let valueofbfid = (UserDefaults.standard.value(forKey: RedesignConstants.baseFranchiseID) as? String)
        AOFLoginManager.sharedInstance.baseFranchiseID = valueofbfid ?? ""
        ZMUserActions.sharedInstance().franchiseId = valueofbfid ?? ""
      }
        var assetArray: [Asset] = []
        self.progressText = "Getting Promoted Contents"
        if AOFLoginManager.sharedInstance.baseFranchiseID.isEmpty {
            ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray? = assetArray  // pass empty array
          HomeViewModel.sharedInstance.isPromotedContentService = true
          if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow && HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
            self.updateProgressView()
          }
        } else {
            PromotedContentServiceClass.sharedAPIManager().fetchPromotedHomeContent(assetArray as [Any], success: { [self] (response) in
                print(response)
                if let promoteArray = response as? [PromotedContents] {
                    print("succcesssfully %@", promoteArray)

                    for item in promoteArray {
                        if (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getPromoAsset(item.id!) != nil{
                            assetArray.append((ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getPromoAsset(item.id!))
                        }
                    }
                }
                ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray? = assetArray
              HomeViewModel.sharedInstance.isPromotedContentService = true
              if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow
                  && HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
                self.updateProgressView()
              }
            }) { (error) in
                print("show the error desc promoted", error.localizedDescription)
              HomeViewModel.sharedInstance.isPromotedContentService = true
                ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray = []
                self.updateProgressView()
            }
        }
       
        
        
    }
    /* Method to load Events */
    @objc func loadEvents() {
        // Replace with getEventsService in EventsServiceClass - Call 1 for metadata
      let utilityVar:AppUtilities = AppUtilities.sharedUtilities() as! AppUtilities
      if HomeViewModel.sharedInstance.flowType == RedesignConstants.eventsFlow {
        utilityVar.showBottomIndicatorView(true, withMessage: ProntoMessageGettingEvents)
      }
        var eventidArray:[String] = []
        EventsServiceClass.sharedAPIManager().invokeEventsAllServiceCall({ [self] (eventsDict: [AnyHashable : Any]) in
            print("Events %@", eventsDict)
            if let dict = eventsDict as NSDictionary? as! [String:Any]? {
                eventidArray = Array(dict.keys)
            }

            ProntoRedesignSingletonClass.sharedInstance.eventsAllArray = []
            for item in eventidArray
            {
                ProntoRedesignSingletonClass.sharedInstance.eventsAllArray?.append(EventsServiceClass.sharedAPIManager().getEventsbyId(item))
            }
            
          if HomeViewModel.sharedInstance.flowType == RedesignConstants.eventsFlow {
            utilityVar.showBottomIndicatorView(false, withMessage: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NSNotification.Name.preferenceListVCDidSavePreferences.rawValue),
                                            object: self, userInfo: nil)
          }
          HomeViewModel.sharedInstance.isEventsService = true
          if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow
              && HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
            self.progressText = "Getting Events"
            self.updateProgressView()
          }
        })
        { (error) in
            print("&&&&& Nandana Behera******", error.localizedDescription)
          HomeViewModel.sharedInstance.isEventsService = true
            ProntoRedesignSingletonClass.sharedInstance.eventsAllArray = []
            self.updateProgressView()
          if HomeViewModel.sharedInstance.flowType == RedesignConstants.eventsFlow {
            utilityVar.showBottomIndicatorView(false, withMessage: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NSNotification.Name.preferenceListVCDidSavePreferences.rawValue), object: self, userInfo: nil)
          }
        }
    }
    /* Method to load Favourites */
    @objc func loadFavourites() {
        var assetArray: [Asset] = []
        ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray = []
        MyFavouritesServiceClass.sharedAPIManager().fetchFavouritesHomeContent(assetArray as [Any], success: { [self] (response: [Any]) in
            print(response)
            if let res = response as? [FavFranchise]{
                print("succcesssfully %@",res)
              let filteredRes = res.compactMap { data in
                if data.asset_ID != 0 {
                  ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.append(data)
                }
              }
            }
          HomeViewModel.sharedInstance.isFavourtiesService = true
          if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow
              && HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
            self.progressText = "Getting Favourites"
            self.updateProgressView()
          }
            
        }) { (error) in
          HomeViewModel.sharedInstance.isFavourtiesService = true
          ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray = []
          self.updateProgressView()
        }
        
    }
    func displayHomeView() {
      RedesignUtilityClass.sharedInstance.invokeHomeScreen(withNav: LoginViewModel.aofNavigationController)
    }
    func removeProgressBar() {
      print("isEventsService", HomeViewModel.sharedInstance.isEventsService)
      print("isPromotedContentService", HomeViewModel.sharedInstance.isPromotedContentService)
      print("isFavourtiesService", HomeViewModel.sharedInstance.isFavourtiesService)
      print("isHomeCategoriesService", HomeViewModel.sharedInstance.isHomeCategoriesService)
      print("isFolderDetailsFetched", HomeViewModel.sharedInstance.isFolderDetailsFetched)

      if(HomeViewModel.sharedInstance.isEventsService && HomeViewModel.sharedInstance.isPromotedContentService && HomeViewModel.sharedInstance.isFavourtiesService && HomeViewModel.sharedInstance.isHomeCategoriesService && HomeViewModel.sharedInstance.isFolderDetailsFetched){
        setupHomePage()
      } else if (HomeViewModel.sharedInstance.isEventsService && (HomeViewModel.sharedInstance.isPromotedContentService == false) && HomeViewModel.sharedInstance.isFavourtiesService && HomeViewModel.sharedInstance.isHomeCategoriesService && HomeViewModel.sharedInstance.isFolderDetailsFetched) {
        setupHomePage()
      }
    }
  func setupHomePage() {
    HomeViewController.sharedInstance.removeProgressView()
    HomeViewController.sharedInstance.loadMyFavourites()
    HomeViewController.sharedInstance.loadData()
    HomeViewController.sharedInstance.loadPromotedContent()
    HomeViewController.sharedInstance.loadEventsSection()
    HomeViewController.sharedInstance.collectionView.reloadData()
    HomeViewController.sharedInstance.eventsView.setNeedsDisplay()
    HomeViewController.sharedInstance.promotedContentView.setNeedsDisplay()
    HomeViewModel.sharedInstance.isEventsService = false
    HomeViewModel.sharedInstance.isPromotedContentService = false
    HomeViewModel.sharedInstance.isFavourtiesService = false
    HomeViewModel.sharedInstance.isHomeCategoriesService = false
    HomeViewModel.sharedInstance.isFolderDetailsFetched = false
  }
    /* Method to make all required services in Home View */
    @objc func loadHomeServiceCalls() {
      if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow
          && HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
        displayHomeView()
      } else {
        self.updateProgressView()
      }
      DispatchQueue.main.async {
        if HomeViewController.sharedInstance.collectionView != nil
            && HomeViewController.sharedInstance.myFavouritesView != nil
            && HomeViewController.sharedInstance.promotedContentView != nil
            && HomeViewController.sharedInstance.eventsView != nil {
          HomeViewController.sharedInstance.collectionView.isHidden = true
          HomeViewController.sharedInstance.myFavouritesView.isHidden = true
          HomeViewController.sharedInstance.promotedContentView.isHidden = true
          HomeViewController.sharedInstance.eventsView.isHidden = true
        }
        ProntoRedesignSingletonClass.sharedInstance.callPreferencesApi = false
        if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow {
          self.loadEvents()
        }
        self.loadHomeCategories()
        self.loadFavourites()
        self.loadPromotedContents()
      }
    }
    /* Method to Remove the progress View in Home View */
    func removeProgressView() {
        HomeViewController.sharedInstance.removeProgressView()
    }
    /* Method to load the progress View in Home View */
    func updateProgressView(){
      if HomeViewModel.sharedInstance.flowType == RedesignConstants.eventsFlow {
        self.loadEvents()
      } else if HomeViewModel.sharedInstance.flowType == RedesignConstants.searchFlow {
        SearchViewModel.sharedInstance.doSearch(searchText: ProntoRedesignSingletonClass.sharedInstance.lastSearchText)
      } else {
        HomeViewController.sharedInstance.updateProgressBar(progressText: self.progressText)
        self.removeProgressBar()
      }
    }
    /*Method to load Home after all Service Calls */
    func loadHomeView() {
        if ((UserDefaults.standard.bool(forKey: RedesignConstants.isLoadingFirstTime)) && isEventsService && isPromotedContentService && isFavourtiesService && isHomeCategoriesService) {
            isEventsService = false
            isPromotedContentService = false
            isHomeCategoriesService = false
            isFavourtiesService = false
            
            // TODO:Remove this delay after integrating with homeview Services
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                 RedesignUtilityClass.sharedInstance.invokeHomeScreen(withNav: LoginViewModel.aofNavigationController)
            })
        }
        else if (!UserDefaults.standard.bool(forKey: RedesignConstants.isLoadingFirstTime)
                  && isEventsService && isPromotedContentService && isFavourtiesService && isHomeCategoriesService) {
            isEventsService = false
            isPromotedContentService = false
            isHomeCategoriesService = false
            isFavourtiesService = false
            //TODO:Remove this delay after integrating with homeview Services
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.removeProgressView()
                 RedesignUtilityClass.sharedInstance.invokeHomeScreen(withNav: LoginViewModel.aofNavigationController)
            })
        }
    }
    /* Method to load category detail in home */
  @objc func loadHomeCategories(flowType: String? = ""){
        HomeViewServiceClass.sharedAPIManager().fetchCategoryDetails({ [self] (response) in
            
            (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).printResponse(ProntoRedesignAPIConstants.categoryAssetCountResponseMsg, responseObj: response)
          HomeViewModel.sharedInstance.isHomeCategoriesService = true
          if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow && HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
            self.progressText = "Getting Assets"
            self.updateProgressView()
          }
        }) { (error) in
             (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).printResponse(ProntoRedesignAPIConstants.categoryAssetCountResponseMsg, responseObj: error)
          HomeViewModel.sharedInstance.isHomeCategoriesService = true
            self.updateProgressView()
        }
    }
    /* Method to handle tapping of load More categories Option */
    func loadMoreCategories(index: Int, nav: UINavigationController?) {
        let categoryViewController = CategoriesViewController()
        categoryViewController.isMoreCategoryView = true
      
        if let foldersArray = HomeViewController.sharedInstance.foldersArray {
          let halfArray: [Categories] = (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) == 0 ? Array(foldersArray[19...]) : Array(foldersArray[9...])
        categoryViewController.foldersArray = halfArray
        ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
        nav?.pushViewController(categoryViewController, animated: true)
        }
    }
    /* Method to handle tapping of any category in HomeView */
  func tapCategory(index: Int, nav: UINavigationController?) {
    let categoryObj:Categories = HomeViewController.sharedInstance.foldersArray![index]
    
    let categoriesObject = getSelectedCategoryData(categoryObj: categoryObj)
    if !(ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.contains(categoriesObject.categoryID ?? 0) ?? false) ||
        (categoriesObject.assets?.count == 0 && categoriesObject.subfolders?.count == 0) {
      HomeViewServiceClass.sharedAPIManager().fetchCategorySubFolderDetails({ (response) in
        ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.append(categoriesObject.categoryID ?? 0)
        let catObject:Categories = response as! Categories
        let categoryViewController = CategoriesViewController()
        categoryViewController.selectedCategory = catObject
        self.setSortingAssetsArray(selectedCategory: catObject)
        ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
        nav?.pushViewController(categoryViewController, animated: true)
      }, error: { (error) in
        
      }, selectedCategory: categoryObj)
    } else {
      let categoryViewController = CategoriesViewController()
      categoryViewController.selectedCategory = categoriesObject
      self.setSortingAssetsArray(selectedCategory: categoriesObject)
      ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
      nav?.pushViewController(categoryViewController, animated: true)
    }
  }
  func clickCategory(index: Int, nav: UINavigationController?) {
    let categoryObj: Categories = HomeViewController.sharedInstance.foldersArray![index]
    let categoriesObject = getSelectedCategoryData(categoryObj: categoryObj)
    
    if !(ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.contains(categoriesObject.categoryID ?? 0) ?? false) ||
        (categoriesObject.assets?.count == 0 && categoriesObject.subfolders?.count == 0) {
      HomeViewServiceClass.sharedAPIManager().fetchDetailCategory({ (response) in
        ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.append(categoriesObject.categoryID ?? 0)
        let catObject:Categories = response as! Categories
        let categoryViewController = CategoriesViewController()
        categoryViewController.selectedCategory = catObject
        self.setSortingAssetsArray(selectedCategory: catObject)
        ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
        nav?.pushViewController(categoryViewController, animated: true)
      }, error: { (error) in
        
      }, selectedCategory: categoryObj)
    } else {
      let categoryViewController = CategoriesViewController()
      categoryViewController.selectedCategory = categoriesObject
      self.setSortingAssetsArray(selectedCategory: categoriesObject)
      ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
      nav?.pushViewController(categoryViewController, animated: true)
    }
  }
  @objc func checkAssetIsAvailable(assetID: NSNumber) -> Bool {
    let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(assetID)
    for data in offlineAssets {
      if let offData = data as? Asset {
        if offData.assetID == assetID {
          return true
        }
      }
    }
    return false
  }
  @objc func getAsset(assetID: NSNumber) -> Asset {
      let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(assetID)
      for data in offlineAssets {
        if let offData = data as? Asset {
          if offData.assetID == assetID {
            return offData
          }
        }
      }
      return Asset()
    }
  public func checkAssetIDAvailableLocally(assetID: NSNumber) -> Bool {
      let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(assetID)
      for data in offlineAssets {
        if let offData = data as? Asset {
          if offData.assetID == assetID && offData.isDownloaded == true {
            return true
          }
        }
      }
      return false
    }
    /* Set data for Sorting Options */
    func setSortingAssetsArray(selectedCategory:Categories) {
        var assetsArray: [Asset]? = []
        // Converting Assets set to array
        if let assetsArrayObject = selectedCategory.assets {
            for item in assetsArrayObject{
                assetsArray?.append(item)
            }
        }
        ProntoRedesignSingletonClass.sharedInstance.sortingsasetsArray = assetsArray
        
        // Set sorting Flag
        if ProntoRedesignSingletonClass.sharedInstance.sortingsasetsArray?.count ?? 0 > 1{
            ProntoRedesignSingletonClass.sharedInstance.sortingEnabled = true
        }
    }
    /* Method to load category details in Categories View */
    func tapCategory(index: IndexPath, backBtnView: UIButton?,
                     previousHeaderStringName: String, nav: UINavigationController) {
      let categoryObj: Categories = HomeViewController.sharedInstance.foldersArray![index.row]
      let categoriesObject = getSelectedCategoryData(categoryObj: categoryObj)
      
      if !(ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.contains(categoriesObject.categoryID ?? 0) ?? false) ||
          (categoriesObject.assets?.count == 0 && categoriesObject.subfolders?.count == 0) {
          let categoryObj: Categories = HomeViewController.sharedInstance.foldersArray![index.row]
          HomeViewServiceClass.sharedAPIManager().fetchCategorySubFolderDetails({ (response) in
            ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.append(categoriesObject.categoryID ?? 0)
              let catObject: Categories = response as! Categories
              let categoryViewController = CategoriesViewController()
              categoryViewController.previousHeaderName = previousHeaderStringName
              categoryViewController.selectedCategory = catObject
              ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
              nav.pushViewController(categoryViewController, animated: true)
          }, error: { (error) in
              
          }, selectedCategory: categoryObj)
      } else {
        let categoryViewController = CategoriesViewController()
        categoryViewController.previousHeaderName = previousHeaderStringName
        categoryViewController.selectedCategory = categoriesObject
        ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
        nav.pushViewController(categoryViewController, animated: true)
      }
    }
  
  func getSelectedCategoryData(categoryObj: Categories) -> Categories {
    let importedCategory = ZMCDManager.sharedInstance.getCategoriesByID(categoryId: categoryObj.categoryID ?? 0)
    var categoriesObject = Categories()
    if let categories: [Categories] = importedCategory as? [Categories] {
      for data in categories {
        categoriesObject = data
      }
    }
    return categoriesObject
  }
  
  
  func setUpUrl(categoryID: Int) -> String {
      if categoryID != -1 && categoryID > 0 {
        return (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getBaseUrl() +
        (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getServiceResourceUrl(ProntoRedesignAPIConstants.categoryAssetCountURL) +
        "\(categoryID)"
      } else if categoryID == -1 {
        return (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getBaseUrl() +
        (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getServiceResourceUrl(ProntoRedesignAPIConstants.getAssetsURL) + "new"
      } else {
        return (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getBaseUrl() +
        (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getServiceResourceUrl(ProntoRedesignAPIConstants.getAssetsURL) + "updated"
      }
    }
  
  @objc func performBackgroundDownload() {
     DispatchQueue.global(qos: .userInteractive).async {
      self.fetchAssets()
    }
   }
  
  //MARK: api calls to fetch folders & assets
    func fetchAssets() {
      let getFolderDetailsURL = (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getBaseUrl() +
      (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getServiceResourceUrl(ProntoRedesignAPIConstants.categoryAssetCountURL)
      let url = URL(string: getFolderDetailsURL)
      let queryItems = [URLQueryItem(name: kTokenKey, value: AOFLoginManager.sharedInstance.getAccessToken())]
      let newUrl = url?.appending(queryItems)!
      print("folders api----->", newUrl)
      let task = URLSession.shared.dataTask(with: newUrl!) { (data, response, error) in
        do {
          if let fetchedData = data {
            guard let json = try JSONSerialization.jsonObject(with: fetchedData, options: []) as? NSDictionary else {
              throw error!
            }
            print("json", json)
            HomeViewModel.sharedInstance.isFolderDetailsFetched = true
            DispatchQueue.main.async {
              self.updateProgressView()
            }
            var convertedJsonArray = [Any]()
            convertedJsonArray.append(json.value(forKey: "0") as Any)
            convertedJsonArray.append(json.value(forKey: "1") as Any)
            convertedJsonArray.append(json.value(forKey: "sub-folders") as Any)
            self.saveData(jsonData: (convertedJsonArray as? NSArray ?? []))
          }
        } catch let error as NSError {
          print(error.debugDescription)
          HomeViewModel.sharedInstance.isFolderDetailsFetched = true
          DispatchQueue.main.async {
            self.updateProgressView()
          }
        }
    }
    task.resume()
  }
  
  func saveData(jsonData: NSArray) {
    ProntoRedesignSingletonClass.sharedInstance.folderDetails = jsonData
  }
  
  func getAssetDetails(categoryObj: Categories, index: Int? = 0,
                       previousHeaderStringName: String? = "",
                       parentView: UIView? = nil, nav: UINavigationController?) {
    let categoriesObject = getSelectedCategoryData(categoryObj: categoryObj)
    if let foldersJson: NSArray = ProntoRedesignSingletonClass.sharedInstance.folderDetails {
      for data in foldersJson {
        if let assetValues = data as? NSDictionary {
          let id = assetValues.value(forKey: "tid") as! String
          if categoryObj.categoryID?.stringValue == id {
            if !(ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.contains(categoryObj.categoryID ?? 0) ?? false) ||
                      (categoriesObject.assets?.count == 0 && categoriesObject.subfolders?.count == 0) {
              (HomeViewServiceClass.sharedInstance() as AnyObject).edit(toCoreData: nil,
                                                                        assetValues as! [AnyHashable : Any],
                                                                        selectedCategory: categoryObj,
                                                                        success: { assetsData in
                ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.append(categoryObj.categoryID ?? 0)
                if let totalData = (assetsData[0] as? Categories) {
                  self.showDetailPage(response: totalData,
                                                  previousHeaderStringName: previousHeaderStringName,
                                                  parentView: parentView, nav: nav)
                }
              })
            } else {
              self.showDetailPage(response: categoriesObject,
                                  previousHeaderStringName: previousHeaderStringName,
                                  parentView: parentView, nav: nav)
            }
            break
          }
        } else if let newData = data as? NSArray {
          for value in newData {
            if let assetValues = value as? NSDictionary {
              let id = assetValues.value(forKey: "tid") as! String
              if categoryObj.categoryID?.stringValue == id {
                if !(ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.contains(categoryObj.categoryID ?? 0) ?? false) ||
                          (categoriesObject.assets?.count == 0 && categoriesObject.subfolders?.count == 0) {
                  (HomeViewServiceClass.sharedInstance() as AnyObject).edit(toCoreData: nil,
                                                                            assetValues as! [AnyHashable : Any],
                                                                            selectedCategory: categoryObj,
                                                                            success: { assetsData in
                    ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs?.append(categoryObj.categoryID ?? 0)
                    if let totalData = (assetsData[0] as? Categories) {
                      self.showDetailPage(response: totalData,
                                                      previousHeaderStringName: previousHeaderStringName,
                                                      parentView: parentView, nav: nav)
                    }
                  })
                } else {
                  self.showDetailPage(response: categoriesObject,
                                      previousHeaderStringName: previousHeaderStringName,
                                      parentView: parentView, nav: nav)
                }
                break
              }
            }
          }
        }
      }
    }
  }
  
  func showDetailPage(response: Categories,
                      previousHeaderStringName: String? = nil,
                      parentView: UIView? = nil, nav: UINavigationController?) {
    let catObject: Categories = response
    let categoryViewController = CategoriesViewController()
    categoryViewController.selectedCategory = catObject
    ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
    if previousHeaderStringName != "" {
      categoryViewController.previousHeaderName = previousHeaderStringName
    }
    if parentView == nil {
      HomeViewModel.sharedInstance.setSortingAssetsArray(selectedCategory: catObject)
    }
    nav?.pushViewController(categoryViewController, animated: true)
  }
  
  @objc func fetchFranchiseListInBackground() {
    DispatchQueue.main.async {
      (PreferencesListViewController.sharedInstance() as AnyObject).fetchPreferencesInBackground()
    }
  }
  
  @objc func convertPriority(values: NSDictionary) {
    print("PRIORITIES", values)
  }
}
