//
//  HomeViewController.swift
//  Pronto
//
//  Created by Saranya on 06/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
@objc class HomeViewController: SearchHistoryViewController,
                                UICollectionViewDataSource,
                                UICollectionViewDelegate,
                                UICollectionViewDelegateFlowLayout,
                                UINavigationControllerDelegate {
    
    @objc static let sharedInstance = HomeViewController(nibName: RedesignConstants.mainHomeNibName, bundle: nil)
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    @IBOutlet var homeView: UIView!
  @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var eventsStack: UIStackView!
    @IBOutlet weak var mainHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var promotedContentHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var eventsStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainHomeHeader: UIView!
  @IBOutlet weak var promotedContentView: UIView!
    @IBOutlet weak var myFavouritesView: UIView!
    @IBOutlet weak var eventsView: UIView!
    var myFavouritesHomeView: MyFavouritesHomeView!
    var eventsSection: HomeViewEventsMain!
    
    var statusBar: UIView!
    var screenWidth = UIScreen.main.bounds.size.width
    var homeViewClassObj: HomeViewClass?
    var foldersArray: [Categories]?
    var folderDetailsArray: [Categories]?
   @objc var assets: [Asset]?
    @objc var favouritesAssetArray: [FavFranchise]?
    var isreloadRequired: Bool = false
    var progressOverlayView: DALabeledCircularProgressView?
    var progressValue: Float = 0.0
    var preferenceSwitchkey: Bool = false
    var grayview: UIView!
    var progressLabel = UILabel()
  private var observation: NSKeyValueObservation?
  var selectedCategory: Categories?
  @objc var deeplinkAsset: String = ""
  
  deinit {
      observation?.invalidate()
      currentCell = nil
    }
  
  var currentCell: HomeScreenCollectionCell?
  

    override func viewDidLoad() {
        super.viewDidLoad()
        self.statusBar = UIView()
        self.grayview = UIView()
        self.homeViewClassObj = HomeViewClass()
        
      progressOverlayView = DALabeledCircularProgressView.init(frame: CGRect(x: (UIScreen.main.bounds.size.width - 50)/2,
                                                                               y: (UIScreen.main.bounds.size.height - 50)/2, width: 100, height: 100))

        let collectioncell = UINib.init(nibName: RedesignConstants.collectionCellNibName, bundle:nil)
        collectionView.register(collectioncell, forCellWithReuseIdentifier: RedesignConstants.collectionCellId)
        collectionView.isScrollEnabled = true
        // Set Automatic size for CollectionViewCell
        collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        //Register for Notification
        registerForNotification()
        //loadData()
      if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow && HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
        self.setUpOverLayView()
      }
        commonInit()
    }
  
  @objc func dismissSearchHeaderView() {
    HomeMainHeaderView.sharedInstance.dismissSearchListView()
    dismissView()
  }
  
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
         updateStackViewForTraitChange()
    }
    override func viewWillAppear(_ animated: Bool) {
        print("inside view will appear..***")
        super.viewWillAppear(animated)
        self.navigationController?.delegate = self
        HomeViewModel.sharedInstance.flowType = ""
        HomeMainHeaderView.sharedInstance.searchBarHomeScreen.text = ""
        HomeMainHeaderView.sharedInstance.dismissSearchListView()
        ProntoAssetHandlingClass.sharedInstance().resetAudioPlayer()
        if myFavouritesHomeView != nil {
          myFavouritesHomeView.addHeader()
          myFavouritesHomeView.reloadFavouritesHomeView()
        }
        //Register Collection View Cell
        ProntoRedesignSingletonClass.sharedInstance.backHeaderButtons = []
        showViews()
        if (isreloadRequired) {
            myFavouritesHomeView.removeFromSuperview()
            loadMyFavourites()
        } else {
            loadData()
            loadPromotedContent()
            loadEventsSection()
            self.collectionView.reloadData()
            self.eventsView.setNeedsDisplay()
            self.promotedContentView.reloadInputViews()
        }
      isreloadRequired = true
    }
  
  
  func openDeeplink() {
    if HomeViewController.sharedInstance.deeplinkAsset != "" {
      appDelegate?.registerDeeplinkNotification()
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OPEN_SCHEMA_ASSET_APP_CLOSED"), object: nil, userInfo: ["assetString": HomeViewController.sharedInstance.deeplinkAsset])
      AOFLoadingController.sharedInstance.fromDeeplink = false
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    dismissView()
    HomeMainHeaderView.sharedInstance.searchBarHomeScreen.resignFirstResponder()
    HomeMainHeaderView.sharedInstance.searchBarHomeScreen.text = ""
    HomeMainHeaderView.sharedInstance.dismissSearchListView()
    RedesignErrorHandling.sharedInstance().hideMessage()
  }
  
  func setUpSearchList(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int) {
    addSearchListView(xAxis: xAxis, yAxis: -24, width: width, height: height, mainStackView: self.mainView)
  }
  
  override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.isUserInteractionEnabled = true
        currentCell = nil
        isreloadRequired = true
    }
  
    /* Return navigation Controller */
    func getHomeNavController() -> UINavigationController {
        return self.navigationController!
    }
    /* Method for common Initialisation */
    func commonInit() {
        self.edgesForExtendedLayout = []
        self.navigationController?.navigationBar.isHidden = true
        
        UserDefaults.standard.set(false, forKey: RedesignConstants.isLoadingFirstTime)
      
        // setThemes
        RedesignThemesClass.sharedInstance.setThemes(color: RedesignThemesClass.sharedInstance.royalBlueCode)
        
        // load Main Header
        loadHeader()
        
        // load category data
        loadData()
        
        // load Promoted Content
        loadPromotedContent()
        
        // load My Favourites
        loadMyFavourites()
        
        // load Events Section
        loadEventsSection()
        
        // loading status bar
        if #available(iOS 13.0, *) {
            statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        } else {
            statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as? NSObject)?.value(forKey: "statusBar") as? UIView
        }
        self.statusBar.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        
        self.view.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        
    }
    /* SetUp data*/
    func loadData() {
      self.foldersArray = HomeViewServiceClass.sharedAPIManager().categoriesCountResponse as? [Categories]
      self.foldersArray = self.foldersArray?.sorted(by: { $1.bucketId!.intValue > $0.bucketId!.intValue })
    }
    /* Register for Notification */
    func registerForNotification() {
        NotificationCenter.default.addObserver(self, selector:#selector(dismissHomeViewAndLogout),
                                               name: NSNotification.Name(rawValue: RedesignConstants.logOutNotification),
                                               object: nil)
    }
  
  /* Method to sort data based on the priority */
  func sortFavAsset() {
     
  }

    /* Method to load Header */
    func loadHeader() {
        var headerView = HomeMainHeaderView.sharedInstance
        mainHomeHeader.addSubview(headerView)
        headerView.setThemes()
        headerView.enableRefreshButton(true)
        addConstraintsForHeaderView(toView: &headerView)
    }
    /* Method to add constraints for Header Content View */
    func addConstraintsForHeaderView(toView: inout HomeMainHeaderView) {
        toView.translatesAutoresizingMaskIntoConstraints = false
        toView.pinEdges(to: mainHomeHeader)
    }
    /* Method to load Promoted Content */
    func loadPromotedContent() {
      var promotedContent = Bundle.main.loadNibNamed(RedesignConstants.promotedContentNib,
                                                     owner: self, options: nil)?.first as! PromotedContentView
      promotedContent.weblinkDelegate = self
      promotedContentView.addSubview(promotedContent)
      addConstraintsForPromotedContent(toView: &promotedContent)
      
      if ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count == 0 {
        self.promotedContentHeightConstraint.constant = 0
        mainHeightConstraint.constant = 204
        let savedFranchiese: [String] = (UserDefaults.standard.value(forKey: "selectedprefList")) as? [String] ?? []
        if HomeViewModel.sharedInstance.isPromotedContentService {
          if savedFranchiese.count > 1 {
            RedesignErrorHandling.sharedInstance().showMessage(kMultipleFranchiseForHomeOfficeMessage,whithType: ZMProntoMessagesTypeWarning)
          } else {
            RedesignErrorHandling.sharedInstance().showMessage(RedesignConstants.noPromotedContentText,whithType: ZMProntoMessagesTypeWarning)
          }
        } else if (HomeViewModel.sharedInstance.isEventsService &&
                    (HomeViewModel.sharedInstance.isPromotedContentService == false) &&
                    HomeViewModel.sharedInstance.isFavourtiesService &&
                    HomeViewModel.sharedInstance.isHomeCategoriesService &&
                   HomeViewModel.sharedInstance.isFolderDetailsFetched) {
          RedesignErrorHandling.sharedInstance().showMessage(RedesignConstants.promotedContentServerError,whithType: ZMProntoMessagesTypeAlert)
        } else {
          if savedFranchiese.count > 1 {
            RedesignErrorHandling.sharedInstance().showMessage(kMultipleFranchiseForHomeOfficeMessage,whithType: ZMProntoMessagesTypeWarning)
          } else {
            RedesignErrorHandling.sharedInstance().showMessage(RedesignConstants.noPromotedContentText,whithType: ZMProntoMessagesTypeWarning)
          }
        }
      } else {
        self.promotedContentHeightConstraint.constant = 350
        mainHeightConstraint.constant = 554
        RedesignErrorHandling.sharedInstance().hideMessage()
      }
      promotedContent.doAdditionalSetUp()
    }
    
    /* Method to add constraints for Promoted Content View */
    func addConstraintsForPromotedContent(toView:inout PromotedContentView){
        toView.translatesAutoresizingMaskIntoConstraints = false
        toView.pinEdges(to: promotedContentView)
    }
    
    /* Method to load My Favourites */
    func loadMyFavourites() {
        myFavouritesHomeView = Bundle.main.loadNibNamed(RedesignConstants.myFavouritesHomeViewNib,
                                                        owner: self, options: nil)?.first as? MyFavouritesHomeView
        myFavouritesHomeView.weblinkDelegate = self
        myFavouritesView.addSubview(myFavouritesHomeView)
        myFavouritesHomeView.doInitialSetUp()
        addConstraintsForMyFavo(toView: &myFavouritesHomeView)
    }
    
    /* Method to add constraints for My Favourites View */
    func addConstraintsForMyFavo(toView:inout MyFavouritesHomeView) {
        toView.translatesAutoresizingMaskIntoConstraints = false
        toView.pinEdges(to: myFavouritesView)
    }
    
    /* Method to load My Favourites */
    func loadEventsSection() {
        eventsSection = Bundle.main.loadNibNamed(RedesignConstants.eventsMainViewNib,
                                                 owner: self, options: nil)?.first as? HomeViewEventsMain
        eventsView.addSubview(eventsSection)
        eventsSection.doInitialSetUp()
        addConstraintsToEventsSection(toView: &eventsSection)
    }
       
    /* Method to add constraints for My Favourites View */
    func addConstraintsToEventsSection(toView:inout HomeViewEventsMain) {
        toView.translatesAutoresizingMaskIntoConstraints = false
        toView.pinEdges(to: eventsView)
    }
    
    /* Delegate Methods when orientations Change */
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateStackViewForTraitChange()
        postNotificationForOrientationChange()
        self.promotedContentView.layoutIfNeeded()
        progressOverlayView?.frame = CGRect(x: (size.width - 50)/2, y: (size.height - 50)/2, width: 100, height: 100)
        progressLabel.frame = CGRect(x: (progressOverlayView?.frame.origin.x ?? 0) - 14,
                                               y: ((progressOverlayView?.frame.origin.y ?? 0) + 120), width: 250, height: 36)
    }

    /* Method to handle the alignment of stackviews(Promoted content View, Events and Favourties) when changing the orientations */
     func updateStackViewForTraitChange() {
        screenWidth = UIScreen.main.bounds.size.width
        homeView.updateConstraints()
        self.promotedContentView.layoutIfNeeded()
        homeView.setNeedsLayout()
        
    }
    /* Method to Post Notification on orientation Change */
    func postNotificationForOrientationChange() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange),
                                        object: self, userInfo: nil)
    }
    
    /* UICollectionView DataSource & Delegate Methods */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //5 columns
        if let folders = foldersArray {
//          if folders.count >= 20 && ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count == 0 {
//            return 20
//          } else if folders.count >= 10 && (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) > 0 {
//            return 10
//          } else if (folders.count >= 10 && folders.count < 20) && (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) == 0 {
//            return folders.count
//          } else {
//            return folders.count
//          }
          return folders.count
        }
        return 0
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // rows
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // create folders
        let cell:HomeScreenCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:RedesignConstants.collectionCellId, for: indexPath) as! HomeScreenCollectionCell
        cell.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser

        if let categoryObj: Categories = (foldersArray?[indexPath.row]) {
//          if ((foldersArray?.count ?? 0) > 20 && indexPath.row == 19) && (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) == 0 {
//              cell.imageView.image = UIImage(named: RedesignConstants.ImageType.moreCategory.rawValue)!
//              cell.categoryTitle.text = RedesignConstants.FolderTitle.moreCategory.rawValue
//              cell.assetCount.text = ""
//          } else if ((foldersArray?.count ?? 0) > 10 && indexPath.row == 9) && (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) > 0 {
//            cell.imageView.image = UIImage(named: RedesignConstants.ImageType.moreCategory.rawValue)!
//            cell.categoryTitle.text = RedesignConstants.FolderTitle.moreCategory.rawValue
//            cell.assetCount.text = ""
//          } else {
              cell.imageView.image = homeViewClassObj?.checkfolderImage(index: indexPath)
              
              if (homeViewClassObj?.checkFolderTitle(index: indexPath) == RedesignConstants.titleString) {
                  cell.categoryTitle.text = categoryObj.name
              } else {
                  cell.categoryTitle.text = homeViewClassObj?.checkFolderTitle(index: indexPath)
              }
              if (categoryObj.asset_count != nil) {
                if ((categoryObj.asset_count?.intValue ?? 0) <= 1) {
                  cell.assetCount.text = String(categoryObj.asset_count?.intValue ?? 0) + RedesignConstants.assetTxt
                } else {
                  cell.assetCount.text = String(categoryObj.asset_count?.intValue ?? 0) + RedesignConstants.assetsTxt
                }
              }
//          }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let collectionViewWidth: Int = (4 * RedesignConstants.widthBetweenHomeCollectionCellsLandscape)
        let screenW = Int(collectionView.frame.size.width) - collectionViewWidth
        var width: Int = 0
        var height: Int = 0
      
      if UIDevice.current.orientation.isValidInterfaceOrientation {
        if ((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft) || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)) {
          if (self.foldersArray?.count ?? 0) <= 5 {
            height = 312
          } else {
            height = 168
          }
          width = screenW / 5
        } else {
          height = 224
          width = screenW / 5
        }
      } else {
        print("UIScreen.main.bounds.width", UIScreen.main.bounds.width)
        print("UIScreen.main.bounds.width", collectionView.frame.size.height)
        let size: CGSize = UIScreen.main.bounds.size
        if size.width > 1024 {
          if (self.foldersArray?.count ?? 0) <= 5 {
            height = 312
          } else {
            height = 168
          }
          width = screenW / 5
        } else {
          height = 224
          width = screenW / 5
        }
      }
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)
    }
    
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let categoryObj: Categories = foldersArray![indexPath.row]
    self.selectedCategory = categoryObj
//    if (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) == 0 {
//      if ((foldersArray?.count ?? 0) > 20 && indexPath.row == 19) {
//        HomeViewModel.sharedInstance.loadMoreCategories(index: indexPath.row, nav: self.navigationController)
//      } else {
//        HomeViewModel.sharedInstance.getAssetDetails(categoryObj: categoryObj, nav: self.navigationController)
//      }
//    } else if (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) > 0 {
//      if ((foldersArray?.count ?? 0) > 10 && indexPath.row == 9) {
//        HomeViewModel.sharedInstance.loadMoreCategories(index: indexPath.row, nav: self.navigationController)
//      } else {
//        HomeViewModel.sharedInstance.getAssetDetails(categoryObj: categoryObj, nav: self.navigationController)
//      }
//    }
    HomeViewModel.sharedInstance.getAssetDetails(categoryObj: categoryObj, nav: self.navigationController)
  }
    /* Delegate for LogOut - to dismiss HomeView */
    @objc func dismissHomeViewAndLogout() {
        if self.navigationController?.viewControllers.first is AOFLoadingController {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else {
            self.navigationController?.dismiss(animated: true, completion: {
                let navController = UINavigationController.init(rootViewController: AOFLoadingController())
                RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: navController, animated: true)
            })
        }
    }
    /* Method to load view after checking preferences */
  @objc func loadActualView() {
    HomeViewModel.sharedInstance.performBackgroundDownload()
    isreloadRequired = false
    if HomeViewModel.sharedInstance.flowType != RedesignConstants.eventsFlow && HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
      self.setUpOverLayView()
    } 
    HomeViewModel().loadHomeServiceCalls()
  }
    func setUpOverLayView()
    {
        let presentviewController = UIApplication.shared.keyWindow?.rootViewController
        if presentviewController is HomeViewController {
             print(" in home screen")
        }
        else
        {
            print("navigation  home screen")
            if(grayview != nil)
            {
            
//                grayview.frame = UIScreen.main.bounds
//                grayview.backgroundColor = UIColor.clear
//                grayview.tag = 999
          
            
            progressValue = 0.0
          progressOverlayView = DALabeledCircularProgressView.init(frame: CGRect(x: (UIScreen.main.bounds.size.width - 50)/2,
                                                                                   y: (UIScreen.main.bounds.size.height - 50)/2, width: 100, height: 100))
//            progressOverlayView?.backgroundColor = UIColor.gray
            progressOverlayView?.roundedCorners = 1
            progressOverlayView?.setProgress(CGFloat(progressValue), animated: true)
            progressLabel.frame = CGRect(x: (progressOverlayView?.frame.origin.x ?? 0) - 10,
                                         y: ((progressOverlayView?.frame.origin.y ?? 0) + 112), width: 250, height: 36)
            presentviewController?.view.addSubview(progressOverlayView!)
            presentviewController?.view.addSubview(progressLabel)
//            presentviewController?.view.addSubview(grayview)
            if(progressValue < 1)
            {
                progressOverlayView?.progress = CGFloat(progressValue)
                progressOverlayView?.progressLabel.text = String(format: "%.0f%%", progressOverlayView?.progress ?? 0 * 100)
                progressOverlayView?.progressLabel.font = UIFont(name: "Lato-Regular", size: 16.0)
                progressLabel.font = UIFont(name: "Lato-Bold", size: 16.0)
                progressLabel.textColor = .black
//                progressLabel.textAlignment = .left
                progressLabel.frame.origin.x = (progressOverlayView?.frame.origin.x ?? 0)
                progressLabel.text = "Loading Data..."
            }
            }
        }

    }
  /* Function to update progree bar */
func updateProgressBar(progressText: String) {
  if((progressValue * 100) >= 100){
    progressValue = 100/100
  } else {
    progressValue = ((progressValue * 100) + 20)/100
  }
  progressOverlayView?.progress = CGFloat(progressValue)
  progressOverlayView?.setProgress(CGFloat(progressValue), animated: true)
  if progressText == "Getting Promoted Contents" {
    progressLabel.frame.origin.x = (progressOverlayView?.frame.origin.x ?? 0) - 48
  } else {
    progressLabel.frame.origin.x = (progressOverlayView?.frame.origin.x ?? 0) - 10
  }
  progressLabel.text = progressText
  progressOverlayView?.progressLabel.text = String(format: "%.0f%%", (progressValue) * 100)
  progressOverlayView?.progressLabel.font = UIFont(name: "Lato-Regular", size: 16.0)
  
}
  /* Method to Remove the progress View in Home View */
func removeProgressView() {
  showViews()
  progressValue = 0
  DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
    HomeViewController.sharedInstance.progressOverlayView?.removeFromSuperview()
    HomeViewController.sharedInstance.progressOverlayView = nil
    HomeViewController.sharedInstance.progressLabel.removeFromSuperview()
    HomeViewController.sharedInstance.progressLabel.text = ""
  })
}
func showViews() {
  if self.collectionView != nil && self.myFavouritesView != nil && self.promotedContentView != nil && self.eventsView != nil {
    self.collectionView.isHidden = false
    self.myFavouritesView.isHidden = false
    self.promotedContentView.isHidden = false
    self.eventsView.isHidden = false
  }
}
  @objc func showSafari(uri: String) {
      if let url = URL(string: uri) {
        if #available(iOS 11.0, *) {
          let config = SFSafariViewController.Configuration()
          config.entersReaderIfAvailable = false
          let vc = SFSafariViewController(url: url, configuration: config)
          vc.dismissButtonStyle = .close
          vc.toolbarItems = []
          present(vc, animated: true)
        } else {
          // Fallback on earlier versions
        }
      }
  }
}

extension HomeViewController: HomeWeblinkViewProtocol {
  func openWebLink(uri: String) {
    showSafari(uri: uri)
  }
}

extension HomeViewController: FavHomeWeblinkViewProtocol {
  func openHomeFavWebLink(uri: String) {
    showSafari(uri: uri)
  }
}

extension URL {
  func appending(_ queryItems: [URLQueryItem]) -> URL? {
          guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else {
              // URL is not conforming to RFC 3986 (maybe it is only conforming to RFC 1808, RFC 1738, and RFC 2732)
              return nil
          }
          // append the query items to the existing ones
          urlComponents.queryItems = (urlComponents.queryItems ?? []) + queryItems

          // return the url from new url components
          return urlComponents.url
      }
}
