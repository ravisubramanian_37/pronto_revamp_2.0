//
//  HomeViewClass.swift
//  Pronto
//
//  Created by Saranya on 09/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import UIKit

class HomeViewClass: UIView {
    /*
    Method to return the title for folder
    */
    func checkFolderTitle(index: IndexPath) -> String
    {
        var title: String
        switch (index.section, index.row) {
        case (0, 0):
            title = RedesignConstants.FolderTitle.newAssetCategory.rawValue
        case (0, 1):
            title = RedesignConstants.FolderTitle.updatedAssetCategory.rawValue
        default:
            title = RedesignConstants.titleString
        }
        return title
    }
    /*
    Method to check the folder image
    */
    func checkfolderImage(index: IndexPath) -> UIImage {
        var imageName: String
        switch (index.section, index.row) {
        case (0, 0):
            imageName = RedesignConstants.ImageType.newAssetCategory.rawValue
        case (0, 1):
            imageName = RedesignConstants.ImageType.updatedAssetCategory.rawValue
        default:
            imageName = RedesignConstants.ImageType.normalCategory.rawValue
        }
        return UIImage(named: imageName)!
    }
    /*Method to load category*/
    func loadCategory() {
        
    }
    
}
