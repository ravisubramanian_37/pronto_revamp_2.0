//
//  MyFavouritesHomeView.swift
//  Pronto
//
//  Created by Saranya on 23/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
protocol FavHomeWeblinkViewProtocol {
  func openHomeFavWebLink(uri: String)
}
class MyFavouritesHomeView: UIView,
                            UICollectionViewDataSource,
                            UICollectionViewDelegate,
                            UICollectionViewDelegateFlowLayout,
                            UIPopoverPresentationControllerDelegate,
                            closeAudioDelegate {
  @IBOutlet weak var myHeading: UIView!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
  @IBOutlet weak var moreButton: UIButton!
  @IBOutlet weak var bottomView: UIView!
  var audioViewController: UIViewController = UIViewController()
  var audioView: MyFavouritesHomeAudioView?
  var copiedAudioView: MyFavouritesHomeAudioView?
  var YConstraintForLandscape: NSLayoutConstraint?
  var YConstraintForPortrait: NSLayoutConstraint?
  var XConstraintForLandscape: NSLayoutConstraint?
  var XConstraintForPortrait: NSLayoutConstraint?
  var assetNameArray: [FavFranchise]?
  
  var selectedCell: MyFavouriteHomeViewTableCell?
  var selectedIndex: IndexPath?
  
  let cellId = "collectionCell"
  let tableViewCellNibName = "MyFavoHomeViewTableCell"
  let headerNibName = "HomeFavoEventsHeader"
  
  var count: Int = 0
  var weblinkDelegate: FavHomeWeblinkViewProtocol?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  /* Method to add additional setup */
  func doInitialSetUp() {
    // Add Header
    addHeader()
    
    assetNameArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray
    sortFavAsset()
    loadFavouritesAssetArray()
    
    if assetNameArray?.count == 0
    {
      bottomView.isHidden = true
    }
    // Register for Notification
    NotificationCenter.default.addObserver(self, selector: #selector(handleOrientationChange),
                                           name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(handleOrientationChange),
                                           name: Notification.Name(rawValue: RedesignConstants.favouritesAssetUpdationNotification), object: nil)
  }
  func sortFavAsset() {
    MyFavouritesViewModel.sharedInstance.checkFavoSingletonAssetArray()
    self.assetNameArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.sorted(by: { $1.priority!.intValue > $0.priority!.intValue })
  }
  /* Method to load My Favourites array */
  func loadFavouritesAssetArray(){
    bottomView.isHidden = false
    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.backgroundColor = .white
    
    // Register Collection View Cell
    let collectioncell = UINib.init(nibName: tableViewCellNibName, bundle:nil)
    collectionView.register(collectioncell, forCellWithReuseIdentifier: cellId)
  }
  /* Method to reload self view */
  func reloadFavouritesHomeView() {
    assetNameArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray
    if assetNameArray?.count ?? 0 > 0 {
      CategoryViewModel.sharedInstance.cleanUpCategories()
      loadFavouritesAssetArray()
      collectionView.reloadData()
      collectionView.layoutIfNeeded()
      checkAudioFrame()
    }
    count = 0
  }
  /* Method to align audioView when rotated */
  func checkAudioFrame() {
    if (audioView != nil) {
      if ((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft)
          || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)) {
        YConstraintForPortrait?.isActive = false
        YConstraintForLandscape?.isActive = true
      } else {
        YConstraintForPortrait?.isActive = true
        YConstraintForLandscape?.isActive = false
      }
      audioView?.updateConstraints()
      audioView?.setNeedsLayout()
    }
  }
  /* Method called to handle Notifiaction */
  @objc func handleOrientationChange() {
    if (self.window != nil) {
      reloadFavouritesHomeView()
    }
  }
  /* Method to add header */
  func addHeader() {
    let myFavourites = Bundle.main.loadNibNamed(headerNibName, owner: self, options: nil)?.first as! HomeFavoEventsHeaderView
    myHeading.addSubview(myFavourites)
    
    // Add Constraints
    myFavourites.translatesAutoresizingMaskIntoConstraints = false
    myHeading.pinEdges(to: myFavourites)
    
    // Themes
    myFavourites.setThemes()
  }
  
  /* UICollectionView DataSource & Delegate Methods */
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    // columns
    let count = assetNameArray?.count ?? 0
    if count == 0
    {
      self.collectionView.setMessage(RedesignConstants.noAssetFavouritesText)
    }
    if count > 4 {
        self.collectionView.setMessage("")
      return 4
    }
    else{
      return count
    }
    
  }
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    // rows
    return 1
  }
  func getImageName(selectedAsset:Asset)-> String{
    let assetType = Constant.getFileType(for: selectedAsset)
    var imageName: String = RedesignConstants.centerImagePDF
    
    if (assetType == ZMProntoFileTypesPDF) {
      imageName = RedesignConstants.centerImagePDF
    } else if (assetType == ZMProntoFileTypesVideo) {
      imageName = RedesignConstants.centerImageVideo
    } else if (assetType == ZMProntoFileTypesBrightcove) {
      imageName = RedesignConstants.centerImageVideo
    } else if (assetType == ZMProntoFileTypesDocument) {
      imageName = RedesignConstants.centerImagePDF
    } else if(assetType == ZMProntoFileTypesWeblink) {
      imageName = RedesignConstants.centerImageWeblink
    } else if(assetType == ZMProntoFileTypesAudio) {
      imageName = RedesignConstants.audioLinkName
    }
    return imageName
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    var cell: MyFavouriteHomeViewTableCell = collectionView.dequeueReusableCell(withReuseIdentifier:cellId, for: indexPath) as! MyFavouriteHomeViewTableCell
    
    //if let selectedAsset = self.assetNameArray?[indexPath.row].asset { //changed as it is not giving correct answer - Nandana Change
    if let selectedAsset = getAssetForIndexPath(indexpath: indexPath.row){
      cell.assetLabel.text = selectedAsset.title
      cell.assetLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: "#0D1C49")
      if (RedesignConstants.isBigDevice()) {
        cell.assetLabel.font = UIFont.systemFont(ofSize: 14)
      } else {
        cell.assetLabel.font = UIFont.systemFont(ofSize: 13)
      }
      
      cell.imageview.image = UIImage(named: getImageName(selectedAsset: selectedAsset))
      
      // Set Shadow for cell
      RedesignUtilityClass.sharedInstance.setShadow(view: &cell)
      
      count += 1
      if (count >= assetNameArray?.count ?? 0) {
        count = 0
      }
    }
    if (audioView != nil && selectedIndex == indexPath) {
      if ((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft)
          || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)) {
        if (YConstraintForLandscape == nil) {
          if (indexPath.row == 3) {
            YConstraintForLandscape = audioView?.topAnchor .constraint(equalTo: cell.topAnchor, constant: -(cell.frame.size.height + 10))
            
          } else {
            YConstraintForLandscape = audioView?.topAnchor .constraint(equalTo: cell.topAnchor, constant:0)
          }
        }
        YConstraintForPortrait?.isActive = false
        YConstraintForLandscape?.isActive = true
        
        if (XConstraintForLandscape == nil) {
          XConstraintForLandscape = audioView?.leadingAnchor .constraint(equalTo: cell.leadingAnchor, constant: 0)
        }
        XConstraintForPortrait?.isActive = false
        XConstraintForLandscape?.isActive = true
        
      } else {
        if (YConstraintForPortrait == nil) {
          YConstraintForPortrait = audioView?.topAnchor .constraint(equalTo: collectionView.topAnchor, constant: 0)
        }
        YConstraintForLandscape?.isActive = false
        YConstraintForPortrait?.isActive = true
        if (XConstraintForPortrait == nil) {
          if ((indexPath.row == 0)||(indexPath.row == 2)) {
            XConstraintForPortrait = audioView?.leadingAnchor .constraint(equalTo: cell.leadingAnchor, constant: 0)
          } else {
            XConstraintForPortrait = audioView?.leadingAnchor .constraint(equalTo: collectionView.leadingAnchor, constant: cell.frame.size.width + 5)
          }
        }
        XConstraintForLandscape?.isActive = false
        XConstraintForPortrait?.isActive = true
      }
    }
    return cell
  }
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    if UIDevice.current.orientation.isValidInterfaceOrientation {
      if ((UIDevice.current.orientation == UIDeviceOrientation.portrait)
            || (UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown)) {
        let height:Int = (Int(collectionView.frame.size.height) / 2) - 4
        if (RedesignConstants.isBigDevice()) {
          return CGSize(width: 245, height: height - 8)
        } else if RedesignConstants.isiPad9_7() {
          return CGSize(width: 180, height: height - 2)
        }
        else {
          return CGSize(width: 190, height: height - 5)
        }
      } else{
        let height: Int = (Int(collectionView.frame.size.height) / 2) - 4
        if (RedesignConstants.isBigDevice()) {
          return CGSize(width: 325, height: height - 8)
        } else{
          return CGSize(width: 240, height: height - 10)
        }
      }
    } else {
      // if isValidInterfaceOrientation == false
      print("UIScreen.main.bounds.width", UIScreen.main.bounds.width)
      let size:CGSize = UIScreen.main.bounds.size
      let height:Int = (Int(collectionView.frame.size.height) / 2) - 4
      if size.width == 1366 {
        return CGSize(width: 325, height: height - 8)
      } else if size.width >= 1024 {
        return CGSize(width: 245, height: height - 10)
      } else if size.height == 1366 {
        return CGSize(width: 245, height: height - 8)
      } else {
        return CGSize(width: 190, height: height - 10)
      }
    }
  }
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 2
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
  {
    return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
  }
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    let cell = collectionView .cellForItem(at: indexPath) as? MyFavouriteHomeViewTableCell
    if (cell != nil){
      if let asset = getAssetForIndexPath(indexpath: indexPath.row){
        let assetType = Constant.getFileType(for: asset)
        if (assetType == ZMProntoFileTypesAudio) {
          loadAudioAssetPopOver(asset: asset, sender: cell!,index: indexPath)
          ProntoAssetHandlingClass.sharedInstance().isFromHomeView = true
        } else {
          ProntoAssetHandlingClass.sharedInstance().isFromHomeView = false
        }
        if assetType == ZMProntoFileTypesWeblink {
          self.weblinkDelegate?.openHomeFavWebLink(uri: asset.uri_full ?? "")
        } else {
          MyFavouritesViewModel.sharedInstance.openAsset(selectedAsset: asset)
        }
      }
    }
  }
  /* Get asset for index path */
  func getAssetForIndexPath(indexpath: Int) -> Asset? {
    if let selectedAsset = self.assetNameArray, selectedAsset.count > 0 {
      var selAsset = self.assetNameArray?[indexpath]
      if (selAsset?.asset_ID != nil)
        {
            let selectedasset = (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getAllAsset(selAsset?.asset_ID!)
            return selectedasset
        }
    }
    return nil
  }
  /* Method called on tapping more button */
  @IBAction func moreButtonTapped(_ sender: UIButton) {
    //TODO: load this after making service call to fetch the details of Favourties assets
    let myFavouritesDetailsViewController = MyFavouritesDetailsViewController()
    //TODO: Update below object with favourites asset fetched in service call here
    myFavouritesDetailsViewController.favouritesAssetsArray = self.assetNameArray
    HomeViewController.sharedInstance.navigationController!.pushViewController(myFavouritesDetailsViewController, animated: true)
  }
  /* Method to load Audio view */
  func loadAudioAssetPopOver(asset:Asset, sender:MyFavouriteHomeViewTableCell, index:IndexPath){
    selectedCell = sender
    selectedIndex = index
    audioView = (Bundle.main.loadNibNamed("MyFavouritesHomeAudioView", owner: self, options: nil)?[0] as! MyFavouritesHomeAudioView)
    audioView?.delegate = self
    audioView?.imageView = sender.imageview
    audioView?.titleLabel.text = sender.assetLabel.text
    audioView?.titleLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: "#0D1C49")
    if (RedesignConstants.isBigDevice()) {
      audioView?.titleLabel.font = UIFont.systemFont(ofSize: 14)
    } else {
      audioView?.titleLabel.font = UIFont.systemFont(ofSize: 13)
    }
    self.addSubview((audioView)!)
    ProntoAssetHandlingClass.sharedInstance().homeFavouritesAudioView = audioView!
    // Set Shadow for cell
    RedesignUtilityClass.sharedInstance.setShadow(view: &audioView!)
    
    audioView?.translatesAutoresizingMaskIntoConstraints = false
    
    //Portrait
    audioView?.frame = CGRect(x: sender.frame.origin.x, y: 61, width: sender.frame.size.width, height: collectionView.frame.size.height - 10)
    audioView?.heightAnchor.constraint(equalTo: sender.heightAnchor, multiplier: 2, constant: 10).isActive = true
    audioView?.widthAnchor.constraint(equalTo: sender.widthAnchor).isActive = true
    
    if ((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft)
          || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)) {
      //Landscape
      if (index.row == 3) {
        YConstraintForLandscape = audioView?.topAnchor .constraint(equalTo: sender.topAnchor, constant: -(sender.frame.size.height + 10))
      } else {
        YConstraintForLandscape = audioView?.topAnchor .constraint(equalTo: sender.topAnchor, constant:0)
      }
      YConstraintForLandscape?.isActive = true
      XConstraintForLandscape = audioView?.leadingAnchor .constraint(equalTo: sender.leadingAnchor, constant: 0)
      XConstraintForLandscape?.isActive = true
    } else {
      YConstraintForPortrait = audioView?.topAnchor .constraint(equalTo: collectionView.topAnchor, constant: 0)
      YConstraintForPortrait?.isActive = true
      if ((index.row == 0)||(index.row == 2)) {
        XConstraintForPortrait = audioView?.leadingAnchor .constraint(equalTo: sender.leadingAnchor, constant: 0)
      } else {
        XConstraintForPortrait = audioView?.leadingAnchor .constraint(equalTo: collectionView.leadingAnchor, constant: collectionView.frame.size.width/2)
      }
      XConstraintForPortrait?.isActive = true
    }
  }
  /* Delegate for closing the AudioView*/
  func closeAudioView() {
    RedesignErrorHandling.sharedInstance().hideMessage()
    ProntoAssetHandlingClass.sharedInstance().resetHomeAudioView()
    selectedCell = nil
    selectedIndex = nil
    audioView = nil
    YConstraintForPortrait = nil
    YConstraintForLandscape = nil
    XConstraintForPortrait = nil
    XConstraintForLandscape = nil
  }
}
extension UICollectionView {
  func setMessage(_ message: String) {
      let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
      messageLabel.text = message
      messageLabel.textColor = .black
      messageLabel.numberOfLines = 0
      messageLabel.textAlignment = .center
      messageLabel.font = UIFont(name: "Lato-Regular", size: 14.0)
      messageLabel.sizeToFit()

      self.backgroundView = messageLabel
  }
  func removeMessage() {
      self.backgroundView = nil
  }
  }
