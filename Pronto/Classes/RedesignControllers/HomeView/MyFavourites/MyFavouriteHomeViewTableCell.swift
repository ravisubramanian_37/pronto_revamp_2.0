//
//  MyFavouriteHomeViewTableCell.swift
//  Pronto
//
//  Created by Saranya on 23/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class MyFavouriteHomeViewTableCell: UICollectionViewCell{
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var assetLabel: UILabel!
    @IBOutlet weak var labelWidthAnchor: NSLayoutConstraint!
    var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
         super.awakeFromNib()
         // Initialization code
         contentView.translatesAutoresizingMaskIntoConstraints = false
         widthConstraint = NSLayoutConstraint(item: contentView,
                                              attribute: NSLayoutConstraint.Attribute.width,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: nil,
                                              attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                                              multiplier: 1, constant: 100)

         NSLayoutConstraint.activate([
             contentView.leftAnchor.constraint(equalTo: leftAnchor),
             contentView.rightAnchor.constraint(equalTo: rightAnchor),
             contentView.topAnchor.constraint(equalTo: topAnchor),
             contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
         ])
        contentView.addConstraint(widthConstraint)
        widthConstraint.isActive = true
     }
}
