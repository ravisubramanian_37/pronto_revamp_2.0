//
//  ProntoInitialLoadController.m
//  Pronto
//
//  Created by Saranya on 13/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoInitialLoadController.h"
#import "NSMutableDictionary+Utilities.h"
#import "Preferences+CoreDataClass.h"
#import "Preferences+CoreDataProperties.h"
#import "InitialLoadServiceClass.h"
#import "Pronto-Swift.h"

@implementation ProntoInitialLoadController
+ (id)sharedInstance {
    
    @synchronized(self)
    {
        static ProntoInitialLoadController *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
-(void)postPreferencesFinishNotification{
  [[HomeViewModel sharedInstance]fetchFranchiseListInBackground];
    [[HomeViewController sharedInstance]loadActualView];
//  [[HomeViewModel sharedInstance] loadHomeServiceCalls];
}
-(void)checkIfPreferencesViewIsRequired{
  // Preferences shd be shown only for first time login and not aftr that
  if ([ZMUserActions sharedInstance].isFieldUser == NO)
  {
    
    self.isPreferenceRequired = [[NSUserDefaults standardUserDefaults] boolForKey: RedesignConstants.isPreferenceRequired];
    self.isLoadingFirstTime = [[NSUserDefaults standardUserDefaults] boolForKey: RedesignConstants.isLoadingFirstTime];
    if (_isPreferenceRequired == YES)
    {
      [[NSUserDefaults standardUserDefaults]setBool:NO forKey:RedesignConstants.isPreferenceRequired];
      [[NSUserDefaults standardUserDefaults]setBool:NO forKey: RedesignConstants.isLoadingFirstTime];
      
      [SettingsView.sharedInstance loadPreferences];
    }
    else
    {
      [self postPreferencesFinishNotification];
    }
  }
  else
  {
    [self postPreferencesFinishNotification];
  }
}

@end
