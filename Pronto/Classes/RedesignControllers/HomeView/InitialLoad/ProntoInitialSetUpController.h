//
//  ProntoInitialSetUpController.h
//  Pronto
//
//  Created by Saranya on 15/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProntoInitialSetUpController : NSObject
+ (id)sharedInstance;
- (void)registerAPNS;
- (void)checkForVersionUpgrade;
- (void)performAssetMigration;
- (void)initializeAbbvieLogging;
- (void)checkForAOFLogoutInSettings;
- (void)performInitialSetUp;
- (void)loadAssets;
- (void)sessionEnd;
- (BOOL)checkForHomeUser;
@end

NS_ASSUME_NONNULL_END
