//
//  ProntoInitialLoadController.h
//  Pronto
//
//  Created by Saranya on 13/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMUserActions.h"
#import "ZMNotifications.h"


NS_ASSUME_NONNULL_BEGIN

@interface ProntoInitialLoadController : NSObject
+ (id)sharedInstance;
@property (nonatomic)BOOL isPreferenceRequired;
@property (nonatomic)BOOL isLoadingFirstTime;
- (void)checkIfPreferencesViewIsRequired;
- (void)postPreferencesFinishNotification;
@end


NS_ASSUME_NONNULL_END
