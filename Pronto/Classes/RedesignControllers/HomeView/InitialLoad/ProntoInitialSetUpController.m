//
//  ProntoInitialSetUpController.m
//  Pronto
//
//  Created by Saranya on 15/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoInitialSetUpController.h"
#import "ProntoUserDefaults.h"
#import "ZMUserActions.h"
#import "Pronto-Swift.h"
#import "NotificationsServiceClass.h"
//#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@implementation ProntoInitialSetUpController
/* Method to create Singleton Object for this class */
+ (id)sharedInstance {
    
    @synchronized(self)
    {
        static ProntoInitialSetUpController *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
/* Method to check for Field / Home User */
-(BOOL)checkForHomeUser{
    if([AOFLoginManager sharedInstance].salesFranchise.length > 0){
        [ZMUserActions sharedInstance].isFieldUser = YES;
        return NO;
    }
    [ZMUserActions sharedInstance].isFieldUser = NO;
    return YES;
}
/* Method to register for Push Notifications */
- (void)registerAPNS{
    
//    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
//        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
//        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
//            if( !error ){
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[UIApplication sharedApplication] registerForRemoteNotifications];
//                });
//            }
//            else{
//                NSLog( @"Push registration FAILED" );
//                NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
//                NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
//            }
//        }];
//    }
  
    
}
- (void)checkForVersionUpgrade
{
    NSString *oldAppVersion = [[ProntoUserDefaults userDefaults] getAppVersion];
    NSString *currentAppVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    if (oldAppVersion == nil || oldAppVersion.length <= 0  ||
        ([[currentAppVersion lowercaseString] isEqualToString:[oldAppVersion lowercaseString]] == NO))
    {
        [ZMUserActions sharedInstance].isVersionUpgraded = YES;
    }
    else
    {
        [ZMUserActions sharedInstance].isVersionUpgraded = NO;
    }
}
- (void)performAssetMigration
{
    NSString *oldAppVersion = [[ProntoUserDefaults userDefaults] getAppVersion];
    NSString *currentAppVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@" current version %@  and older version %@", currentAppVersion, oldAppVersion]];
    if (oldAppVersion == nil || oldAppVersion.length <= 0  ||
        ([[currentAppVersion lowercaseString] isEqualToString:[oldAppVersion lowercaseString]] == NO))
    {
        //forcing initial launch screen to be shown on franchise (library) screen
        [[ProntoUserDefaults userDefaults] setCMSLastSynchronization:@""];
        
        // Do Migration
        [[ZMProntoManager sharedInstance] cleanupEntitiesOnAppMigration];
    
        //Save App Version
        [[ProntoUserDefaults userDefaults] saveAppVersion];
    }
}
- (void)initializeAbbvieLogging
{
    if ([CONFIG isEqualToString:ProductionEnvironment] == NO)
    {
        //Initializing the logger file, heavy task,
         [AbbvieLogging  loadLogger];
    }
}
#pragma mark - Abbvie Security Framework

- (void)checkForAOFLogoutInSettings
{
    BOOL logoutVal = [[NSUserDefaults standardUserDefaults] boolForKey:@"AOFKitDoLogout"];
    if(logoutVal == YES)
    {
        //Logout on launch
        if([AOFLoginManager sharedInstance].checkForAOFLogin)
        {
            [[AOFLoginManager sharedInstance] AOFKitLogout];
        }
        [[SettingsView sharedInstance] postLogOutNotification];
        [ZMTracking trackSection:@"login"
                  withSubsection:FALSE
                        withName:FALSE
                     withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"scloginStatus":@"unsuccessful"}]];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"AOFKitDoLogout"];
    }
    else
    {
        NSLog(@"No need to logout");
    }
}
-(void)performInitialSetUp{
    //Initialize the asset libray. Create the database
       [self validateUserStatus];
}
 //Method used to Initialize the asset libray and Create the database
- (void)validateUserStatus {
    
    [[ZMAssetsLibrary defaultLibrary] initWithPath:[AOFLoginManager sharedInstance].documentsDir];
}
/* Method to load assets */
-(void)loadAssets{
     [ZMAbbvieAPI.sharedAPIManager syncAssets:^{

         [AbbvieLogging logInfo:@">>>> SyncCMS   sync of Asset completes"];
         [ZMUserActions sharedInstance].enableSearch = YES;
         [[ZMAssetsLibrary defaultLibrary] notifyFinish];
         
         NSFetchRequest * fetchrequest = [Asset MR_requestAll];
         NSArray *importedCategories = [Asset MR_executeFetchRequest:fetchrequest];
          NSLog(@"Asset list:%@",importedCategories);
         NSMutableArray *arr = [importedCategories mutableCopy];
         [arr removeLastObject];
         
         
         [HomeViewController sharedInstance].assets = [NSArray arrayWithArray:arr];
         [HomeViewController sharedInstance].favouritesAssetArray = [NSArray arrayWithArray:arr];
         
         [[HomeViewModel sharedInstance] loadHomeServiceCalls];
     } progress:^(float progressValue) {
         //Adding parameter for type of progress
         [[ZMAssetsLibrary defaultLibrary] notifyStep:[NSString stringWithFormat:@"Getting Assets %d%%", (int)progressValue] progress:progressValue progressFor:@"mainSync"];
         [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download asset progress %f", progressValue]];
     } error:^(NSError *error) {
         //Adding parameter for type of progress
         [[ZMAssetsLibrary defaultLibrary] notifyStep:@"Refreshing Events" progress:0 progressFor:@"mainSync"];
         [ZMUserActions sharedInstance].selectLeftMenu = @"";
         [ZMUserActions sharedInstance].enableSearch = NO;
         [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> Error getting the assets %@", error]];
         [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
     }];
}

#pragma mark - Session Handling
- (void)sessionEnd
{
    //Unregister the token
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"Device_Push_Token"];
    
    if([ZMAssetsLibrary defaultLibrary]){
        
        if ([[ZMAssetsLibrary defaultLibrary] property:@"TOKEN"]) {
            token = [[ZMAssetsLibrary defaultLibrary] property:@"TOKEN"];
        }
    }
    
    NSString *cleanToken = [[token description] stringByReplacingOccurrencesOfString:@" " withString:@""];
    cleanToken = [cleanToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    cleanToken = [cleanToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    [NotificationsServiceClass.sharedAPIManager unRegisterTokenNoSession:cleanToken complete:^{
        [AbbvieLogging logInfo:@"Token unregisterd"];
    } error:^(NSError * error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Token error %@",error.description]];
    }];
    
    //Since the session ended we need to clearn the database
    [[ZMAssetsLibrary defaultLibrary] initWithPath:[AOFLoginManager sharedInstance].documentsDir]; //Necesary initialze the DAO object when cleaning it
    [[ZMAssetsLibrary defaultLibrary] clean];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
