//
//  InitialLoadServiceClass.m
//  Pronto
//
//  Created by Saranya on 13/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "InitialLoadServiceClass.h"
#import "ProntoRedesignServerUtilityClass.h"
#import "Pronto-Swift.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Preferences+CoreDataClass.h"
//#import "Preferences+CoreDataProperties.h"
@interface InitialLoadServiceClass(){
    NSMutableArray *savedPreferences ;
}
@end

@implementation InitialLoadServiceClass
/* Singleton Object Creation */

+ (InitialLoadServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static InitialLoadServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
/**
 *  Sync Franchises and Brands fro WS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */

- (void)synchronizeBrands:(void(^)(void))complete error:(void(^)(NSError *error))onError{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> SyncCMS calling: %s", __PRETTY_FUNCTION__]];
}
#pragma Mark Core Data Updations
/**
 *  Mapping franchises into CoreData Database
 *
 *  @param franchises Dictionary with data franchise-brands
 *  @param success    Block success returned franchises array and brands array
 *  @param onError    Block error
 */

-(void)getFranchise:(NSDictionary *)franchises success:(void (^)(NSArray *franchiseArray, NSArray* brandsArray))success error:(void (^)(NSError *))onError{
       
}
-(NSDictionary*)formatFranchises:(NSDictionary*)franchises{
    
    NSMutableDictionary * newFranchises = [[NSMutableDictionary alloc]init];
    
    NSDictionary * franchiseObject = [franchises valueForKey:@"franchises"];
    
    NSMutableArray * newFranchiseObject = [self formatFranchisesObject:franchiseObject];
    
    [newFranchises setObject:newFranchiseObject forKey:@"franchises"];
    [newFranchises setObject:[franchises valueForKey:@"deleted"] forKey:@"deleted"];
    [newFranchises setObject:[franchises valueForKey:@"brands"] forKey:@"brands"];
    
    return newFranchises;
}
-(NSMutableArray*)formatFranchisesObject:(NSDictionary*)object{
    
    NSMutableArray * newObject = [[NSMutableArray alloc]init];
    NSMutableArray * endObject = [[NSMutableArray alloc]init];
    
    for (id key in object) {
        NSMutableDictionary *franchise = [object objectForKey:key];
        [newObject addObject:franchise];
    }
    
    for (id key in newObject) {
        NSMutableDictionary * franchise = [[NSMutableDictionary alloc]initWithDictionary:key];
        if ([franchise objectForKey:@"brands"]) {
            NSMutableDictionary * brands = [franchise objectForKey:@"brands"];
            NSMutableArray * newBrands = [[NSMutableArray alloc]init];
            for (id key in brands) {
                NSMutableDictionary * brand = [[NSMutableDictionary alloc]initWithDictionary:key];
                [brand removeObjectForKey:@"franchises"];
                [newBrands addObject:brand];
            }
            [franchise removeObjectForKey:@"brands"];
            [franchise setObject:newBrands forKey:@"brands"];
            [endObject addObject:franchise];
        }else{
            [endObject addObject:franchise];
        }
    }
    
    return endObject;
}
/**
 *  Get Array to franchises from Franchise entity
 *
 *  @param object  franchiseId to find
 *  @param context Context to performed query
 *
 *  @return Array of Franchies
 */

-(NSArray*)getFranchise:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *franchisesFilter = [NSPredicate predicateWithFormat:@"franchiseID == %@", object];
    NSArray * franchises = [Franchise MR_findAllWithPredicate:franchisesFilter inContext:context];
    return franchises;
}
/**
 *  Delete Branchise or Brand to Franchise or Brand entity when exists a key in WS
 *
 *  @param key     FranchiseID or BrandID
 *  @param context Context to performed query
 */

-(void)deletedFranchiseOrBrandWithKey:(NSString*)key withContext:(NSManagedObjectContext*)context{
    
    NSArray * franchiseArray = [self getFranchise:key withContext:context];
    
    if (franchiseArray.count == 0) {
        NSArray * brandsArray = [self getBrands:key withContext:context];
        if (brandsArray.count > 0) {
            Brand * brand = brandsArray[0];
            [brand MR_deleteEntityInContext:context];
        }
    }else{
        Franchise * franchise = franchiseArray[0];
        [franchise MR_deleteEntityInContext:context];
    }
    
    [context MR_saveToPersistentStoreAndWait];
    
}
/**
 *  Get Array to Brands from Brand entity
 *
 *  @param object  brandID to find
 *  @param context Context to performed query
 *
 *  @return Array of Brands
 */

-(NSArray*)getBrands:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *brandsFilter = [NSPredicate predicateWithFormat:@"brandID == %@", object];
    NSArray * brands = [Brand MR_findAllWithPredicate:brandsFilter inContext:context];
    return brands;
}
/**
 *  Synch categories from WS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */

- (void)getCategories:(void(^)(void))complete error:(void(^)(NSError *error))onError{
  
  [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> SyncCMS calling: %s", __PRETTY_FUNCTION__]];
  if ([AOFLoginManager sharedInstance].upi) {
    
    //AOFKit Implementation
    NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
    NSString* spaceStructureId = [AOFLoginManager sharedInstance].spaceStructureID;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"upi":[AOFLoginManager sharedInstance].upi,
                                                                                  @"sf":franchiseId,
                                                                                  @"ssid":spaceStructureId}];
    
    
    NSString *categoryLastUpdate = [[ZMAssetsLibrary defaultLibrary] property:@"CATEGORIES_LASTUPDATE"];
    NSArray * currentCategories = [[ZMCDManager sharedInstance]getCategories];
    
    if (categoryLastUpdate && !currentCategories) {
      [params setObject:categoryLastUpdate forKey:@"date"];
    }
    
    NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.categoriesURL];
    
    
    [super performBlock:^(CallbackBlock callback) {
      
      [[super requestManagerWithToken:YES headers:[NSDictionary new]] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
          if (callback) {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"resp of category.uri:%@",responseObject]];
            callback(nil, responseObject);
            
          }
        });
        
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (callback) {
          NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
          callback(errorCode, nil);
        }
        
      }];
      
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
      
      if (error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS category.uri", (long)error.code, error.localizedDescription]];
        if (error.code == serverAlert107 ) {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
        }else if (error.code == serverAlert401 || [ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code]) {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
        }
        else {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
        }
        
      }else{
        if (response) {
          NSDictionary *categories = (NSDictionary *)response;
          //                    [[[BucketDetailDataControl alloc] init] saveBucketDetailFromCategories:categories];
          [self getCategories:categories success:^(NSArray *categories) {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"successed imported categories %lu", (unsigned long)categories.count]];
            [ZMProntoManager sharedInstance].categoriesArray = categories;
            complete();
          } error:^(NSError *error) {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"error at importing categories %@", error.localizedDescription]];
            onError(error);
          }];
        }
      }
    }];
    
  }
}
/**
 *  Mapping categories into CoreData Model
 *
 *  @param categories Dictionary with data to mapping
 *  @param success    Array on Categories format
 *  @param onError    Error
 */

-(void)getCategories:(NSDictionary *)categories success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{
  
  NSDictionary * categoriesToImport = [categories valueForKey:@"categories"];
  NSDictionary * categoriesToDeleted = [categories valueForKey:@"deleted"];
  
  [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
    
    
    for (id key in categoriesToImport) {
      
      id category = [categoriesToImport objectForKey:key];
      NSString * categoryId = [category valueForKey:@"tid"];
      NSArray * categoriesArray = [self getCategory:categoryId withContext:localContext];
      
      if (categoriesArray.count == 0) {
        Categories * categoryToCheck = [Categories MR_createEntityInContext:localContext];
        [categoryToCheck setValue:[category valueForKey:@"bucketname"] forKey:@"bucketName"];
        NSNumber * bId = [NSNumber numberWithInt:[[category valueForKey:@"bucketid"] intValue]];
        [categoryToCheck setValue:bId forKey:@"bucketId"];
        [categoryToCheck MR_importValuesForKeysWithObject:category];
      }
    }
    
    if (categoriesToDeleted) {
      for (id key in categoriesToDeleted) {
        
        NSString * categoryId = key;
        NSArray * categoriesArray = [self getCategory:categoryId withContext:localContext];
        
        if (categoriesArray.count > 0) {
          Categories * categoryToCheck = categoriesArray[0];
          [categoryToCheck MR_deleteEntityInContext:localContext];
          [localContext MR_saveToPersistentStoreAndWait];
        }
      }
    }
    
  } completion:^(BOOL contextDidSave, NSError *error) {
    if (error) {
      onError(error);
    }else{
      NSFetchRequest * fetchrequest = [Categories MR_requestAll];
      NSArray *importedCategories = [Categories MR_executeFetchRequest:fetchrequest];
      success(importedCategories);
    }
  }];
}
/**
 *  Get Array to categories from Categories entity
 *
 *  @param object  CategoryId to find
 *  @param context Context to performed query
 *
 *  @return Array of categories
 */

-(NSArray*)getCategory:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *categoryFilter = [NSPredicate predicateWithFormat:@"categoryID == %@", object];
    NSArray * catetory = [Categories MR_findAllWithPredicate:categoryFilter inContext:context];
    return catetory;
}
/**
 *  Get latets Assets updated from WS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */

- (void)refreshStats:(void(^)(void))complete error:(void(^)(NSError *error))onError{
  
  [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> SyncCMS calling: %s", __PRETTY_FUNCTION__]];
  
  if ([AOFLoginManager sharedInstance].upi) {
    
    NSString *date = [[ZMAssetsLibrary defaultLibrary] property:@"STATS_LASTUPDATE"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    //New Implementation for Token
    [params setObject:[AOFLoginManager sharedInstance].getAccessToken forKey:kTokenKey];
    
    if ([date longLongValue] > 0) {
      [params setObject:date forKey:@"date"];
    }
    
    NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.statisticsURL];
    
    [super performBlock:^(CallbackBlock callback) {
      
      [[super requestManagerWithToken:YES headers:[NSDictionary new]] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
          if (callback) {
            callback(nil, responseObject);
          }
        });
        
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (callback) {
          NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
          callback(errorCode, nil);
        }
        
      }];
      
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
      
      if (error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS statistics.uri", (long)error.code, error.localizedDescription]];
        if (error.code == serverAlert107 ) {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
        }else if (error.code == serverAlert401 || [ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code]) {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
        }
        else {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
        }
        
      }else{
        if (response) {
          
          NSDictionary *responseObject = (NSDictionary *)response;
          NSDictionary *assets = [responseObject objectForKey:@"assets"];
          if ([assets count]) {
            
            for (id key in assets) {
              id value = [assets objectForKey:key];
              
              if ([value isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *stats = (NSDictionary*)value;
                
                if (stats) {
                  
                  if ([stats objectForKey:@"assetID"]) {
                    
                    NSArray * assetArray = [[ZMCDManager sharedInstance] getAssetArrayById:[[stats objectForKey:@"assetID"] stringValue]];
                    if (assetArray.count > 0) {
                      
                      Asset * assetCache = assetArray[0];
                      
                      if ([stats objectForKey:@"views"]) {
                        NSNumber *viewCount = [NSNumber numberWithInt:[[stats objectForKey:@"views"] intValue]];
                        assetCache.view_count = viewCount;
                      }
                      
                      if ([stats objectForKey:@"votes"]) {
                        NSNumber *votes = [NSNumber numberWithInt:[[stats objectForKey:@"votes"] intValue]];
                        assetCache.votes = votes;
                      }
                      
                      [[ZMCDManager sharedInstance]updateAsset:assetCache success:^{
                        [[ZMAssetsLibrary defaultLibrary] notifyStatsChanged:assets];
                      } error:^(NSError * error) {
                        [AbbvieLogging logError:[NSString stringWithFormat:@"Error at updating staticts asset %@", error.localizedDescription]];
                      }];
                      
                    }
                    
                  }
                }
              }
            }
          }
          
          [[ZMAssetsLibrary defaultLibrary] setProperty:@"STATS_LASTUPDATE"
                                                  value:[NSString stringWithFormat:@"%ld",[[responseObject objectForKey:@"date"] longValue]]];
          
          complete();
          
        }
      }
    }];
  }
}
/*
 * Fetch the Preferences
 * Fetch the Preferences from CMS
 *
 */

- (void)fetchPrefernces:(void(^)(NSArray *preferencesFetched))complete error:(void(^)(NSError *error))onError
{
  if ([AOFLoginManager sharedInstance].upi) {
    
    [self performBlock:^(CallbackBlock callback) {
      
      //Logout crash fix
      if([AOFLoginManager sharedInstance].upi != nil) {
        
        //New Change for Token
        NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
        
        NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.getPreferencesURL];
        
        [[super requestManagerWithToken:YES headers:[NSDictionary new]] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
          
          dispatch_async(dispatch_get_main_queue(), ^(void){
            if (callback)
            {
              [self saveToCoreDataFranchise:responseObject success:^(NSArray *franchiseAvailable) {
                [self createPreferences:savedPreferences];
                callback(nil, [franchiseAvailable mutableCopy]);
              } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"error saving categories %@", error.localizedDescription]];
                callback(error, responseObject);
              }];
            }
          });
          
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          
          if (callback)
          {
            NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
            callback(errorCode, nil);
          }
        }];  }
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
      
      if (error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS fetchPreferences uri", (long)error.code, error.localizedDescription]];
        if (error.code == serverAlert107 ) {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
        }else if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code]) {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
        }
        else {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:999 alternateMessage:@"Preferences Failed"]);
        }
        
        [ZMUserActions sharedInstance].isPromotedLoaded = NO;
        
      }
      else
      {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"fetchPreferences WS Response: %@", response]];
        complete((NSArray *)response);
      }
    }];
  }
}
/*
 * Save the Preferences
 * Save the selected Preferences sending to CMS
 *
 */
// savePreferences to send and receive the updated preference details
- (void)savePreferences:(NSArray*)franchiseNode complete:(void(^)(NSDictionary *preferencesSavedStatus))complete error:(void(^)(NSError *error))onError{
  if ([AOFLoginManager sharedInstance].upi) {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSMutableDictionary *finalDict = [[NSMutableDictionary alloc] init];
    [finalDict setObject:[AOFLoginManager sharedInstance].getAccessToken forKey:@"token"];
    for (int i=0; i<franchiseNode.count; i++) {
      NSDictionary *dictionary = [[NSMutableDictionary alloc] init];
      [dictionary setValue:franchiseNode[i] forKey:@"tid"];
      [array insertObject:dictionary atIndex:i];
    }
    
    [finalDict setObject:array forKey:@"preferences"];
    
    //New Changes for Token
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"sync params:%@",finalDict]];
    
    NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.savePreferencesURL];
    
    [super performBlock:^(CallbackBlock callback) {
      
      [[super requestManagerWithToken:YES headers:[NSDictionary new]] POST:url parameters:finalDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
          if (callback) {
            callback(nil, responseObject);
          }
        });
        
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (callback) {
          NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
          callback(errorCode, nil);
        }
        
      }];
      
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
      
      if (error) {
        
        [AbbvieLogging logInfo:@"Save Preferences Request Not Successfull"];
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"params:%@",finalDict]];
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS saved Preferences.uri", (long)error.code, error.localizedDescription]];
        
        if (error.code == serverAlert107 ) {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
        }else if (error.code == serverAlert401 || [ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code]) {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
        }
        else {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
        }
      }else{
        if (response)
        {
          [AbbvieLogging logInfo:@"Save Preferences Request Successfull"];
          complete((NSDictionary *)response);
        }
      }
    }];
  }
}

-(void)saveToCoreDataFranchise:(id) response success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{
    NSDictionary * franchisesToImport = response;
    NSMutableArray *initialCatArray = [NSMutableArray new];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        for (NSDictionary * franchise in franchisesToImport) {
           
            NSString * franchiseId = [franchise valueForKey:@"tid"];
            NSArray * franchisesArray = [self getFranchiseNew:franchiseId withContext:localContext];
             [initialCatArray addObject:franchiseId];
            if (franchisesArray.count == 0) {
                Franchise * newFranchise = [Franchise MR_createEntityInContext:localContext];
                [newFranchise MR_importValuesForKeysWithObject:franchise];
            }else{
                NSPredicate *predicate      = [NSPredicate predicateWithFormat:@"tid == %@", franchiseId];
                NSFetchRequest *fetchRequest= [Franchise MR_requestAllWithPredicate:predicate];
                Franchise *franchiseObj         = [[Franchise MR_executeFetchRequest:fetchRequest] objectAtIndex:0];
                franchiseObj.name = [franchise valueForKey:@"name"];
                franchiseObj.field_active = [NSNumber numberWithBool:[franchise valueForKey:@"selected"]];
                franchiseObj.field_franchise_color = [franchise valueForKey:@"color"];
                franchiseObj.franchiseID = [NSNumber numberWithInt:[[franchise valueForKey:@"sales_franchise_id"] intValue]];
                
                
            }
        }
            
        
        [[NSUserDefaults standardUserDefaults] setValue:initialCatArray forKey:@"Franchise"];
    } completion:^(BOOL contextDidSave, NSError *error) {
        if (error) {
            
        }else{
            
            NSFetchRequest * fetchrequestFranchise = [Franchise MR_requestAll];
            NSArray *importedCategories = [Franchise MR_executeFetchRequest:fetchrequestFranchise];
            NSMutableArray * arr = [NSMutableArray new];
            savedPreferences = [NSMutableArray new];
            NSArray *preferencesArray = [[NSUserDefaults standardUserDefaults] valueForKey:@"Franchise"];
            for(Franchise *cat in importedCategories){
                for(int i=0;i<preferencesArray.count;i++){
                    if([[preferencesArray objectAtIndex:i]isEqualToString:[[cat valueForKey:@"tid"]stringValue]]){
                        [arr addObject:cat];
                        BOOL selected = [[cat valueForKey:@"field_active"] boolValue];
                        if(selected == YES){
                            [savedPreferences addObject:cat];
                            break;
                        }
                    }
                }
            }
            
            NSLog(@"%@",arr);
            success(arr);
        }
    }];
    
}
-(NSArray*)getFranchiseNew:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *franchiseFilter = [NSPredicate predicateWithFormat:@"tid == %@", object];
    NSArray * franchise = [Franchise MR_findAllWithPredicate:franchiseFilter inContext:context];
    return franchise;
}
-(void)createPreferences:(NSArray *)savedPreferences{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {

        NSString *userName = [AOFLoginManager sharedInstance].displayName;
        
        Preferences *preference = [Preferences MR_findFirstByAttribute:[Preferences GetUserNameAsString]
                                                             withValue:userName inContext:localContext];
        if (preference == nil)
        {
            //There is no Entry of preference for this user
            preference = [Preferences MR_createEntityInContext:localContext];
        }
        else
        {
            preference.franchieses = [NSArray arrayWithArray:savedPreferences];
        }
    } completion:^(BOOL success, NSError *error) {
      
    }];
}
@end
