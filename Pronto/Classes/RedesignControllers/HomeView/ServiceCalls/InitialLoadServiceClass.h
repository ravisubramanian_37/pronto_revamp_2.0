//
//  InitialLoadServiceClass.h
//  Pronto
//
//  Created by Saranya on 13/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProntoRedesignAPIManager.h"
#import "NSMutableDictionary+Utilities.h"

NS_ASSUME_NONNULL_BEGIN

@interface InitialLoadServiceClass : ProntoRedesignAPIManager
@property (nonatomic) BOOL _syncingInProgress;
@property (nonatomic) int lastServerCodeResponse;

+ (InitialLoadServiceClass *)sharedAPIManager;
- (void)synchronizeBrands:(void(^)(void))complete error:(void(^)(NSError *error))onError;
- (void)syncWithCMS;
- (void)fetchPrefernces:(void(^)(NSArray *preferencesFetched))complete error:(void(^)(NSError *error))onError;
- (void)savePreferences:(NSArray*)franchiseNode complete:(void(^)(NSDictionary *preferencesSavedStatus))complete error:(void(^)(NSError *error))onError;
@end

NS_ASSUME_NONNULL_END
