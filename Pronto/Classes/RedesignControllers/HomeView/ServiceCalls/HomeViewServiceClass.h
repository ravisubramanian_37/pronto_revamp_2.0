//
//  HomeViewServiceClass.h
//  Pronto
//
//  Created by Saranya on 01/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProntoRedesignAPIManager.h"
#import "Categories.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeViewServiceClass : ProntoRedesignAPIManager
@property(nonatomic, strong) NSMutableArray *categoriesCountResponse;
@property (nonatomic, strong)Categories *currentCategoryObjectResponse;
+ (HomeViewServiceClass *)sharedAPIManager;
- (void)userLogin:(void(^)(NSString *token))complete error:(void(^)(NSError *error))onError;
- (void)fetchCategoryDetails:(void(^)(id response))complete error:(void(^)(NSError *error))onError;
- (void)fetchCategorySubFolderDetails:(void(^)(id response))complete error:(void(^)(NSError *error))onError selectedCategory:(Categories *)selCategory;
- (void)fetchDetailCategory:(void(^)(id response))complete error:(void(^)(NSError *error))onError selectedCategory:(Categories *)selCategory;
-(void)EditToCoreData: response:(NSDictionary *)responseObj selectedCategory:(Categories *)selCategory success:(void (^)(NSArray *))success;

@end

NS_ASSUME_NONNULL_END
