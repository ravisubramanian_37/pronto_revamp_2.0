//
//  HomeViewServiceClass.m
//  Pronto
//
//  Created by Saranya on 01/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "HomeViewServiceClass.h"
#import "ProntoRedesignServerUtilityClass.h"
#import "Pronto-Swift.h"
#import "AssetsServiceClass.h"

typedef void (^AssetCompletionBlock) (void);
typedef void (^CallbackBlock)(NSError* error, NSObject* response);

@implementation HomeViewServiceClass
/* Singleton Object Creation */
+ (HomeViewServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static HomeViewServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
/* Method to invoke User Login services */
- (void)userLogin:(void(^)(NSString *token))complete error:(void(^)(NSError *error))onError{
  
  if ([AOFLoginManager sharedInstance].upi) {
    
    NSError *err;
    
    NSDictionary *UsernamePasswordDict = @{ProntoRedesignAPIConstants.userNameText:kUsername, ProntoRedesignAPIConstants.passwordText:kPassword};
    
    //Forming Json array for username and password
    NSData *jsonUserNameAndPasswordData = [NSJSONSerialization dataWithJSONObject:UsernamePasswordDict options:0 error:&err];
    
    //Encrypting User Login data
    NSData *encryptedUsernamePasswordDictData = [RNEncryptor encryptData:jsonUserNameAndPasswordData
                                                            withSettings:kRNCryptorAES256Settings
                                                                password:kRNEncryptionKeyForStage
                                                                   error:&err];
    
    //Forming Params dict for login Service call
    NSString* accessToken = [AOFLoginManager sharedInstance].getAccessToken;
    
    //For Dev & Stage Environment - With encryption & new params list
    NSDictionary *params = @{kTokenKey:accessToken,ProntoRedesignAPIConstants.infoKey:[encryptedUsernamePasswordDictData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]};
    
    NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.userLoginUrl];
    
    
    [super performBlock:^(CallbackBlock  _Nonnull callback) {
      
      AFHTTPRequestOperationManager *manager = [super requestManagerWithToken:NO headers:[NSDictionary new]];
      
      [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
          if (callback) {
            
            [[NSUserDefaults standardUserDefaults]setValue:[responseObject valueForKey:ProntoRedesignAPIConstants.responseSessId] forKey:ProntoRedesignAPIConstants.sessionIdKey];
            [[NSUserDefaults standardUserDefaults]setValue:[responseObject valueForKey:ProntoRedesignAPIConstants.responseSess_Name] forKey:ProntoRedesignAPIConstants.sessionNameKey];
            
            NSString *path = [[NSBundle mainBundle] pathForResource:ProntoRedesignAPIConstants.prontoPlistName ofType:ProntoRedesignAPIConstants.plistType];
            NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
            
            NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:[dict valueForKey:ProntoRedesignAPIConstants.serverTxt]]];
            for (NSHTTPCookie *cookie in cookies)
            {
              NSLog(@"COOKIE :%@\n",cookie);
              if([cookie.name isEqualToString:ProntoRedesignAPIConstants.simpleSAMLSessionIDKey])
                [[NSUserDefaults standardUserDefaults]setValue:cookie.value forKey:ProntoRedesignAPIConstants.simpleSAMLSessionIDKey];
            }
            NSString *salesFran = [responseObject valueForKey:@"salesFranchise"];
            if(salesFran.length > 0 && ![salesFran containsString:@"null"]){
              [AOFLoginManager sharedInstance].salesFranchise = [responseObject valueForKey:@"salesFranchise"];
            }
            NSString *salesFranID = [responseObject valueForKey:@"salesFranchiseID"];
            
            if(salesFranID.length > 0 && ![salesFranID containsString:@"null"]){
              [AOFLoginManager sharedInstance].salesFranchiseID = [responseObject valueForKey:@"salesFranchiseID"];
            }
            /* Setting Base Franchise of User */
            NSString *baseFranID = [responseObject valueForKey:@"BFID"];
            
            if(baseFranID.length > 0 && ![baseFranID containsString:@"null"]){
              [AOFLoginManager sharedInstance].baseFranchiseID = [responseObject valueForKey:@"BFID"];
            }
            /* Setting Theme to User */
            NSString *color = [responseObject valueForKey:@"COLOR"];
            
            if(baseFranID.length > 0 && ![baseFranID containsString:@"null"]){
              [[RedesignThemesClass sharedInstance] setDefaultThemeforUser:[[RedesignThemesClass sharedInstance]hexStringToUIColorWithHex:color]];
            }
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.loginResponseMsg ,responseObject]];
            [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.loginResponseMsg responseObj:responseObject];
            
            callback(nil, responseObject);
          }
        });
        
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (callback) {
          NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
          [AbbvieLogging logInfo:[NSString stringWithFormat:@"%ld %@",(long)error.code,error.localizedDescription]];
          [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.loginResponseMsg responseObj:error];
          callback(errorCode, nil);
        }
      }];
      
    } retryingNumberOfTimes:3 onCompletion:^(NSError * _Nonnull error, NSObject * _Nonnull response) {
      if (error) {
        [ZMUserActions sharedInstance].isLoginSuccess = NO;
        [AbbvieLogging logError:[ProntoRedesignAPIConstants.sharedInstance returnLoginStringWithCode:error.code description:error.localizedDescription]];
        if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
        {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
        }
        else
        {
          onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
        }
      }else
      {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.loginResponseMsg ,response]];
        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.loginResponseMsg responseObj:response];
        [ZMUserActions sharedInstance].isLoginSuccess = YES;
        complete([response valueForKey:kTokenKey]);
      }
    }];
  }
}
/* Method to fetch Categories Home Details */
- (void)fetchCategoryDetails:(void(^)(id response))complete error:(void(^)(NSError *error))onError{
  [super performBlock:^(CallbackBlock  _Nonnull callback) {
    
    NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:@"categoryAssetCount.uri"] ;
    url = [url stringByAppendingString:@"0/"];
    if ([AOFLoginManager.sharedInstance.baseFranchiseID  isEqual: @""]) {
      url = [url stringByAppendingString:@"1"];
    } else {
      url = [url stringByAppendingString:AOFLoginManager.sharedInstance.baseFranchiseID];
    }
    AFHTTPRequestOperationManager *manager = [super requestManagerWithToken:NO headers:[NSDictionary new]];
    
    NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
      
      dispatch_async(dispatch_get_main_queue(), ^(void){
        if (callback) {
          [self saveToCoreData:responseObject success:^(NSArray *assetsLoaded) {
            if (assetsLoaded.count > 0) {
              NSMutableArray *tempArray = [[NSMutableArray alloc] init];
              self.categoriesCountResponse = [assetsLoaded mutableCopy];
              for(Categories *cat in assetsLoaded){
              if ([cat.name isEqualToString:@"Updated Assets"] || [cat.name isEqualToString:@"New Assets"])
              {
                      [tempArray addObject:cat];
                      [self.categoriesCountResponse removeObject:cat];
              }
                  if ([cat.asset_count intValue] == 0)
                  {
                    [self.categoriesCountResponse removeObject:cat];
                  }
              }
              NSSortDescriptor *sortDescriptor;
              sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"bucketId"  ascending:NO];
              NSArray *sortedArray = [self.categoriesCountResponse sortedArrayUsingDescriptors:@[sortDescriptor]];
              self.categoriesCountResponse = [sortedArray mutableCopy];
              
              NSSortDescriptor *newsortDescriptor;
              newsortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryID"  ascending:NO];
              NSArray *newsortArray = [tempArray sortedArrayUsingDescriptors:@[newsortDescriptor]];
              tempArray =  [newsortArray mutableCopy];
                
              [self.categoriesCountResponse insertObject:tempArray[0] atIndex:0];  //New Asset
              [self.categoriesCountResponse insertObject:tempArray[1] atIndex:1]; // Updated Asset
  //            [self.categoriesCountResponse exchangeObjectAtIndex:1 withObjectAtIndex:0];
              callback(nil, assetsLoaded);
            } else {
              callback(nil, assetsLoaded);
            }
          } error:^(NSError *error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"error saving categories %@", error.localizedDescription]];
            callback(error, responseObject);
          }];
        }
      });
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      
      if (callback) {
        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%ld %@",(long)error.code,error.localizedDescription]];
        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.categoryAssetCountResponseMsg responseObj:error];
        callback(errorCode, nil);
      }
    }];
    
  } retryingNumberOfTimes:3 onCompletion:^(NSError * _Nonnull error, NSObject * _Nonnull response) {
    
    
    if (error) {
      [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.categoryAssetCountErrorMsg, error.localizedDescription]];
      if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
      {
        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
      }
      else
      {
        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
      }
      
    }else
    {
      [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.categoryAssetCountResponseMsg ,response]];
      [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.categoryAssetCountResponseMsg responseObj:response];
      complete(response);
    }
  }];
}

/**
 *  Get Array to categories from Categories entity
 *
 *  @param object  CategoryId to find
 *  @param context Context to performed query
 *
 *  @return Array of categories
 */

-(NSArray*)getCategory:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *categoryFilter = [NSPredicate predicateWithFormat:@"categoryID == %@", object];
    NSArray * catetory = [Categories MR_findAllWithPredicate:categoryFilter inContext:context];
    return catetory;
}
-(void)saveToCoreData:(id) response success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{
    NSDictionary * franchisesToImport = response;
    NSMutableArray *initialCatArray = [NSMutableArray new];
    NSMutableDictionary *priorityArray = [NSMutableDictionary new];
    if ([franchisesToImport isKindOfClass:[NSDictionary class]]) {
      NSMutableDictionary *priorityValue = [franchisesToImport objectForKey:@"priority"];
      BOOL items = (priorityValue != [NSNull null]);
      if(items == YES) {
        priorityArray = [franchisesToImport objectForKey:@"priority"];
        NSLog(@"priorityArray %@", [priorityArray mutableCopy]);
      } else {
          NSLog(@"Key not Exists");
      }
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
      for (NSDictionary * franchiseList in franchisesToImport) {
        if([franchiseList isEqual:@"priority"]){
          continue;
        }
        NSDictionary * franchise = [franchisesToImport valueForKey:franchiseList];
        NSString * franchiseId = [franchise valueForKey:@"tid"];

        NSArray * franchisesArray = [self getCategory:franchiseId withContext:localContext];
        [initialCatArray addObject:franchiseId];

        if (franchisesArray.count == 0) {
          Categories * newFranchise = [Categories MR_createEntityInContext:localContext];
          [newFranchise MR_importValuesForKeysWithObject:franchise];

          // MARK: Default priority values for New & Updated Assets
          if ([newFranchise.name isEqualToString:@"New Assets"]) {
            NSString* newPidVal = @"-100001";
            newFranchise.bucketId = [NSNumber numberWithInt:[newPidVal intValue]];
          } else if ([newFranchise.name isEqualToString:@"Updated Assets"]) {
            NSString* updatedPidVal = @"-100000";
            newFranchise.bucketId = [NSNumber numberWithInt:[updatedPidVal intValue]];
          }

          for (NSDictionary * pids in priorityArray) {
            NSString* pidVal = [pids valueForKey:@"pid"];
            NSString* fidVal = [pids valueForKey:@"fid"];
            if ([fidVal isEqual:[NSNumber numberWithInt:[franchiseId intValue]]]) {
              newFranchise.bucketId = [NSNumber numberWithInt:[pidVal intValue]];
            }
          }
        }
        else  // Already category detail exist, update the value.
        {
          Categories * oldFranchise = franchisesArray[0];
          oldFranchise.asset_count = [franchise valueForKey:@"asset_count"];
          oldFranchise.name = [franchise valueForKey:@"name"];
          NSString* val = [franchise valueForKey:@"parent"];
          oldFranchise.parent = [NSNumber numberWithInt:[val intValue]];

          // MARK: Default priority values for New & Updated Assets
          if ([oldFranchise.name isEqualToString:@"New Assets"]) {
            NSString* newPidVal = @"-100001";
            oldFranchise.bucketId = [NSNumber numberWithInt:[newPidVal intValue]];
          } else if ([oldFranchise.name isEqualToString:@"Updated Assets"]) {
            NSString* updatedPidVal = @"-100000";
            oldFranchise.bucketId = [NSNumber numberWithInt:[updatedPidVal intValue]];
          }

          for (NSDictionary * pids in priorityArray) {
            NSString* pidVal = [pids valueForKey:@"pid"];
            NSString* fidVal = [pids valueForKey:@"fid"];
            if ([fidVal isEqual:[NSNumber numberWithInt:[franchiseId intValue]]]) {
              oldFranchise.bucketId = [NSNumber numberWithInt:[pidVal intValue]];
            }
          }
        }
      }
      [[NSUserDefaults standardUserDefaults] setValue:initialCatArray forKey:@"HomeCatArray"];
    } completion:^(BOOL contextDidSave, NSError *error) {
          if (error) {

          }else{
              NSFetchRequest * fetchrequestFranchise = [Categories MR_requestAll];
              NSArray *importedCategories = [Categories MR_executeFetchRequest:fetchrequestFranchise];
              NSMutableArray * arr = [NSMutableArray new];
              NSArray *homeViewArray = [[NSUserDefaults standardUserDefaults] valueForKey:@"HomeCatArray"];
              for(Categories *cat in importedCategories){
                  for(int i=0;i<homeViewArray.count;i++){
                      if([[homeViewArray objectAtIndex:i]isEqualToString:[[cat valueForKey:@"categoryID"]stringValue]]){
                      [arr addObject:cat];
                      continue;
                      }
                  }
              }
              NSLog(@"%@",arr);
              success(arr);
          }
      }];
    } else {
      NSMutableArray * arr = [NSMutableArray new];
      NSLog(@"Empty array %@",arr);
      success(arr);
    }
}
/* Method to update core data */
-(void)EditToCoreData:(id) response selectedCategory:(Categories *)selectedCategory success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{
    NSDictionary * responseObject = response;
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        NSString * franchiseId = selectedCategory.categoryID.stringValue;
        NSArray * franchisesArray = [self getCategory:franchiseId withContext:localContext];
        if (franchisesArray.count == 0) {
            
        }else{
             Categories * currentCategoryObject = franchisesArray[0];
//            NSMutableArray *selAssetID = [NSMutableArray new];
        if([responseObject valueForKey:@"assets"] != nil) {
            int i= 0;
            NSMutableSet *assetsSet = [NSMutableSet new];
 
            /* Saving asset in core data removing the duplicates and editing the existing values in asset object*/
            for (NSDictionary * asset in [responseObject valueForKey:@"assets"])
            
            {
                            
//                            NSString * assetId = [asset valueForKey:@"nid"];
                              NSString * assetId = [asset valueForKey:@"assetID"];
//                            [selAssetID addObject:assetId];
                            NSArray * assetArray = [AssetsServiceClass.sharedAPIManager getAsset:assetId withContext:localContext];
                            if (assetArray.count == 0)
                            {
                                Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                                [newAsset MR_importValuesForKeysWithObject:asset];
                                newAsset.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                                [assetsSet addObject:newAsset];
                            }
                            else if ([[HomeViewModel sharedInstance] checkAssetIsAvailableWithAssetID: [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]]] == NO)
                            {
                              Asset * assetToCheck = assetArray[0];
                              // NSArray * brandsInAssetToImport = [asset valueForKey:@"brands"];
                              //NSArray * franchisesInAssetToImport = [asset valueForKey:@"franchise"];
                              // NSArray * franchisesInAssetToImport = [asset valueForKey:@"field_franchises"];
                              NSArray * categoiresInAssetToImport = [asset valueForKey:@"categories"];
                              NSString * valueChanged = [asset valueForKey:@"changed"];
                              
                              // PRONTO-1 - Likes not Working
                              NSString * user_vote = [asset valueForKey:@"user_vote"];
                              NSNumber * valueCount = [NSNumber numberWithInt:[[asset valueForKey:@"view_count"] intValue]];
                              NSNumber * valueVotes = [NSNumber numberWithInt:[[asset valueForKey:@"votes"] intValue]];
                                if (
                                    assetToCheck.categories.count != categoiresInAssetToImport.count
                                    || !([assetToCheck.changed.stringValue isEqualToString: valueChanged])
                                    || assetToCheck.view_count.intValue != valueCount.intValue
                                    || assetToCheck.votes.intValue != valueVotes.intValue
                                    || assetToCheck.user_vote.intValue != user_vote.intValue) {
                                    
                                    [assetToCheck MR_deleteEntityInContext:localContext];
                                    [localContext MR_saveToPersistentStoreAndWait];
                                    assetToCheck = [Asset MR_createEntityInContext:localContext];
                                    [assetToCheck MR_importValuesForKeysWithObject:asset];
                                    assetToCheck.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                                    
                                }
                                [assetsSet addObject:assetToCheck];
                            } else {
                              Asset * assetToCheck = assetArray[0];
                              Asset * localAsset = [[HomeViewModel sharedInstance] getAssetWithAssetID:[NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]]];
                              assetToCheck.isDownloaded = localAsset.isDownloaded;
//                              assetToCheck.path = localAsset.path;
                              [assetsSet addObject:assetToCheck];
                            }
                            i++;
                        }
            [currentCategoryObject setAssets:assetsSet];
            
        }
            if([responseObject valueForKey:@"sub-folders"] != nil) {
            
            NSMutableSet *set= [NSMutableSet new];
            for (int i=0;i<[[responseObject valueForKey:@"sub-folders"] count];i++){
                Categories * newCategories = [Categories MR_createEntityInContext:localContext];
                [newCategories MR_importValuesForKeysWithObject:[[responseObject valueForKey:@"sub-folders"]objectAtIndex:i]];
                [set addObject:newCategories];
                //New Code
                NSDictionary* dict = [[responseObject valueForKey:@"sub-folders"]objectAtIndex:i];
                for (id key in dict) {
                    NSLog(@"show me the key %@", key);
                    if ([key isEqualToString:@"sub-folders"])
                    {
                        for (NSArray * assetarray in [dict[key] valueForKey:@"assets"])
                        {
                            if([assetarray count] == 0){ NSLog(@"Empty"); }
                            else
                            {
                                NSLog(@"not Empty");
                                for (NSDictionary * asset in assetarray)
                                {
                                  NSString * assetId = [asset valueForKey:@"assetID"];
                                  
//                                  if ([[HomeViewModel sharedInstance] checkAssetIsAvailableWithAssetID: [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]]] == YES) {
//                                    [set addObject:newCategories];
//                                  } else {
                                    //                                    NSArray * assetArray = [AssetsServiceClass.sharedAPIManager getAsset:assetId withContext:localContext];
                                    //
                                    //                                    if (assetArray.count == 0)
                                    //                                    {
                                    Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                                    [newAsset MR_importValuesForKeysWithObject:asset];
                                    newAsset.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
//                                    [set addObject:newAsset];
                                    //}
//                                    [set addObject:newCategories];
                                    NSLog(@"show assetID %@",assetId);
//                                  }
                                }
                            }
 
                        }
                    }
                    if ([key isEqualToString:@"assets"] && dict[key] != nil)
                    {
                     for (NSDictionary * asset in dict[key])
                      {
                            NSString * assetId = [asset valueForKey:@"assetID"];
                          Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                          [newAsset MR_importValuesForKeysWithObject:asset];
                          newAsset.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                         // [subfolderset addObject:newAsset];
                            NSLog(@"show assetID *****%@",assetId);
                      }
                    }
                }
                //End of new code
              }
                [currentCategoryObject setSubfolders:set];
            }
        }
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        if (error) {
            
//            onError(error);
        }else{
            NSFetchRequest * fetchrequestFranchise = [Categories MR_requestAll];
            NSArray *importedCategories = [Categories MR_executeFetchRequest:fetchrequestFranchise];
            NSPredicate *categoryFilter = [NSPredicate predicateWithFormat:@"categoryID == %@", selectedCategory.categoryID];
             NSArray *arr = [importedCategories filteredArrayUsingPredicate:categoryFilter];
//            NSArray *arr = [NSArray arrayWithObject:HomeViewServiceClass.sharedAPIManager.currentCategoryObjectResponse];
            
            NSLog(@"%@",arr);
            success(arr);
        }
    }];    
}

-(NSArray *)convertStringArrayToNumberArray:(NSArray *)strings {
    return [strings valueForKeyPath:@"priority"];
}

/* Method to update core data */
-(void)EditToCoreData: response:(NSDictionary *)responseObj selectedCategory:(Categories *)selectedCategory success:(void (^)(NSArray *))success {
    NSDictionary * responseObject = responseObj;
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        NSString * franchiseId = selectedCategory.categoryID.stringValue;
        NSArray * franchisesArray = [self getCategory:franchiseId withContext:localContext];
        if (franchisesArray.count == 0) {
            
        }else{
             Categories * currentCategoryObject = franchisesArray[0];
//            NSMutableArray *selAssetID = [NSMutableArray new];
        if([responseObject valueForKey:@"assets"] != nil) {
            int i= 0;
            NSMutableSet *assetsSet = [NSMutableSet new];
 
            /* Saving asset in core data removing the duplicates and editing the existing values in asset object*/
            for (NSDictionary * asset in [responseObject valueForKey:@"assets"])
            
            {
                            
//                            NSString * assetId = [asset valueForKey:@"nid"];
                              NSString * assetId = [asset valueForKey:@"assetID"];
//                            [selAssetID addObject:assetId];
                            NSArray * assetArray = [AssetsServiceClass.sharedAPIManager getAsset:assetId withContext:localContext];
                            if (assetArray.count == 0)
                            {
                                Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                                [newAsset MR_importValuesForKeysWithObject:asset];
                                newAsset.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                                [assetsSet addObject:newAsset];
                            }
                            else if ([[HomeViewModel sharedInstance] checkAssetIsAvailableWithAssetID: [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]]] == NO)
                            {
                              Asset * assetToCheck = assetArray[0];
                              // NSArray * brandsInAssetToImport = [asset valueForKey:@"brands"];
                              //NSArray * franchisesInAssetToImport = [asset valueForKey:@"franchise"];
                              // NSArray * franchisesInAssetToImport = [asset valueForKey:@"field_franchises"];
                              NSArray * categoiresInAssetToImport = [asset valueForKey:@"categories"];
                              NSString * valueChanged = [asset valueForKey:@"changed"];
                              
                              // PRONTO-1 - Likes not Working
                              NSString * user_vote = [asset valueForKey:@"user_vote"];
                              NSNumber * valueCount = [NSNumber numberWithInt:[[asset valueForKey:@"view_count"] intValue]];
                              NSNumber * valueVotes = [NSNumber numberWithInt:[[asset valueForKey:@"votes"] intValue]];
                                if (
                                    assetToCheck.categories.count != categoiresInAssetToImport.count
                                    || !([assetToCheck.changed.stringValue isEqualToString: valueChanged])
                                    || assetToCheck.view_count.intValue != valueCount.intValue
                                    || assetToCheck.votes.intValue != valueVotes.intValue
                                    || assetToCheck.user_vote.intValue != user_vote.intValue) {
                                    
                                    [assetToCheck MR_deleteEntityInContext:localContext];
                                    [localContext MR_saveToPersistentStoreAndWait];
                                    assetToCheck = [Asset MR_createEntityInContext:localContext];
                                    [assetToCheck MR_importValuesForKeysWithObject:asset];
                                    assetToCheck.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                                    
                                }
                                [assetsSet addObject:assetToCheck];
                            } else {
                              Asset * assetToCheck = assetArray[0];
                              Asset * localAsset = [[HomeViewModel sharedInstance] getAssetWithAssetID:[NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]]];
                              assetToCheck.isDownloaded = localAsset.isDownloaded;
//                              assetToCheck.path = localAsset.path;
                              [assetsSet addObject:assetToCheck];
                            }
                            i++;
                        }
            [currentCategoryObject setAssets:assetsSet];
            
        }
            if([responseObject valueForKey:@"sub-folders"] != nil) {
            
            NSMutableSet *set= [NSMutableSet new];
            for (int i=0;i<[[responseObject valueForKey:@"sub-folders"] count];i++){
                Categories * newCategories = [Categories MR_createEntityInContext:localContext];
                [newCategories MR_importValuesForKeysWithObject:[[responseObject valueForKey:@"sub-folders"]objectAtIndex:i]];
                [set addObject:newCategories];
                //New Code
                NSDictionary* dict = [[responseObject valueForKey:@"sub-folders"]objectAtIndex:i];
                for (id key in dict) {
                    NSLog(@"show me the key %@", key);
                    if ([key isEqualToString:@"sub-folders"])
                    {
                        for (NSArray * assetarray in [dict[key] valueForKey:@"assets"])
                        {
                            if([assetarray count] == 0){ NSLog(@"Empty"); }
                            else
                            {
                                NSLog(@"not Empty");
                                for (NSDictionary * asset in assetarray)
                                {
                                  NSString * assetId = [asset valueForKey:@"assetID"];
                                  
//                                  if ([[HomeViewModel sharedInstance] checkAssetIsAvailableWithAssetID: [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]]] == YES) {
//                                    [set addObject:newCategories];
//                                  } else {
                                    //                                    NSArray * assetArray = [AssetsServiceClass.sharedAPIManager getAsset:assetId withContext:localContext];
                                    //
                                    //                                    if (assetArray.count == 0)
                                    //                                    {
                                    Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                                    [newAsset MR_importValuesForKeysWithObject:asset];
                                    newAsset.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
//                                    [set addObject:newAsset];
                                    //}
//                                    [set addObject:newCategories];
                                    NSLog(@"show assetID %@",assetId);
//                                  }
                                }
                            }
 
                        }
                    }
                    if ([key isEqualToString:@"assets"] && dict[key] != nil)
                    {
                     for (NSDictionary * asset in dict[key])
                      {
                            NSString * assetId = [asset valueForKey:@"assetID"];
                          Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                          [newAsset MR_importValuesForKeysWithObject:asset];
                          newAsset.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                         // [subfolderset addObject:newAsset];
                            NSLog(@"show assetID *****%@",assetId);
                      }
                    }
                }
                //End of new code
              }
                [currentCategoryObject setSubfolders:set];
            }
        }
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        if (error) {
            
//            onError(error);
        }else{
            NSFetchRequest * fetchrequestFranchise = [Categories MR_requestAll];
            NSArray *importedCategories = [Categories MR_executeFetchRequest:fetchrequestFranchise];
            NSPredicate *categoryFilter = [NSPredicate predicateWithFormat:@"categoryID == %@", selectedCategory.categoryID];
             NSArray *arr = [importedCategories filteredArrayUsingPredicate:categoryFilter];
//            NSArray *arr = [NSArray arrayWithObject:HomeViewServiceClass.sharedAPIManager.currentCategoryObjectResponse];
            
            NSLog(@"%@",arr);
            success(arr);
        }
    }];
}

/* Method to fetch Categories Sub folders Home Details */
- (void)fetchCategorySubFolderDetails:(void(^)(id response))complete error:(void(^)(NSError *error))onError selectedCategory:(Categories*)selCategory{
  [super performBlock:^(CallbackBlock  _Nonnull callback) {
    
    NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:@"categoryAssetCount.uri"] ;
    
    url = [url stringByAppendingString:selCategory.categoryID.stringValue];
    AFHTTPRequestOperationManager *manager = [super requestManagerWithToken:NO headers:[NSDictionary new]];
    NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
      
      dispatch_async(dispatch_get_main_queue(), ^(void){
        if (callback) {
          [self EditToCoreData:responseObject selectedCategory:selCategory success:^(NSArray *assetsLoaded) {
            callback(nil, [assetsLoaded objectAtIndex:0]);
          } error:^(NSError *error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"error saving categories %@", error.localizedDescription]];
            callback(error, responseObject);
          }];
          
        }
      });
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      
      if (callback) {
        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%ld %@",(long)error.code,error.localizedDescription]];
        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.categoryAssetCountResponseMsg responseObj:error];
        callback(errorCode, nil);
      }
    }];
    
  } retryingNumberOfTimes:3 onCompletion:^(NSError * _Nonnull error, NSObject * _Nonnull response) {
    
    if (error) {
      [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.categorySubfolderErrorMsg, error.localizedDescription]];
      if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
      {
        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
      }
      else
      {
        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
      }
      
    }else
    {
      [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.categorySubfolderResponseMsg ,response]];
      [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.categorySubfolderResponseMsg responseObj:response];
      complete(response);
    }
  }];
}

//New API call for new and updated Asset 
- (void)fetchDetailCategory:(void(^)(id response))complete error:(void(^)(NSError *error))onError selectedCategory:(Categories*)selCategory{
  [super performBlock:^(CallbackBlock  _Nonnull callback) {
    
    NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:@"getAssets.uri"] ;
    
    if ([selCategory.categoryID.stringValue  isEqual: @"-1"])
    {url = [url stringByAppendingString:@"new"]; }
    else {
      url = [url stringByAppendingString:@"updated"];  //upadted
    }
    
    AFHTTPRequestOperationManager *manager = [super requestManagerWithToken:NO headers:[NSDictionary new]];
    NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
      
      dispatch_async(dispatch_get_main_queue(), ^(void){
        if (callback) {
          [self EditToCoreData:responseObject selectedCategory:selCategory success:^(NSArray *assetsLoaded) {
            callback(nil, [assetsLoaded objectAtIndex:0]);
          } error:^(NSError *error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"error saving categories %@", error.localizedDescription]];
            callback(error, responseObject);
          }];
          
        }
      });
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      
      if (callback) {
        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%ld %@",(long)error.code,error.localizedDescription]];
        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.categoryAssetCountResponseMsg responseObj:error];
        callback(errorCode, nil);
      }
    }];
    
  } retryingNumberOfTimes:3 onCompletion:^(NSError * _Nonnull error, NSObject * _Nonnull response) {
    
    if (error) {
      [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.categorySubfolderErrorMsg, error.localizedDescription]];
      if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
      {
        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
      }
      else
      {
        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
      }
      
    }else
    {
      [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.categorySubfolderResponseMsg ,response]];
      [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.categorySubfolderResponseMsg responseObj:response];
      complete(response);
    }
  }];
}
/* Method to load dictionary keys */
- (void)logDictionaryKeys:(NSDictionary*)dict {
    for (id key in dict) {
        if ([dict[key] isKindOfClass:[NSDictionary class]]) {
            [self logDictionaryKeys:dict[key]];
        }else {

        }
    }

    return;
}
@end
