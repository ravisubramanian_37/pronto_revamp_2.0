//
//  EventsHomeView.swift
//  Pronto
//
//  Created by Ravi Subramanian on 18/12/21.
//  Copyright © 2021 Abbvie. All rights reserved.
//

import Foundation
class EventsHomeView: UIView {
  
  @IBOutlet weak var eventsTitle: UILabel!
  @IBOutlet weak var location: UILabel!
  @IBOutlet weak var monthsView: UILabel!
  @IBOutlet weak var headerView: UIView!
  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var stackView: UIStackView!
  @IBOutlet weak var moreButton: UIButton!
  @IBOutlet weak var footerView: UIView!
  @IBOutlet weak var mainView: UIView!
  var eventsArray : [Event]?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  /* Method to add additional setup */
  func doInitialSetUp(){
    
    eventsArray?.removeAll()
    self.eventsArray = ProntoRedesignSingletonClass.sharedInstance.eventsAllArray
//      ?.sorted(by: { $1.startDate ?? "" > $0.startDate ?? "" })
        
    headerLabel.text = RedesignConstants.eventsTitle
    
    //Set themes
    headerView.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
    //Disable more button if events is equal to or less than 1
    if(eventsArray?.count ?? 0 == 0){
      moreButton.isEnabled = false
      moreButton.isHidden = true
      monthsView.isHidden = true
      self.backgroundColor = UIColor.red
      self.setMessage(RedesignConstants.noEventsContentText)
    } else{
      if(eventsArray?.count ?? 0 == 1 ){
        moreButton.isEnabled = true
        moreButton.isHidden = false
      } else{
        moreButton.isHidden = false
      }
      //Add constraint for StackView
//      stackView.distribution = .fill
//      updateCosntraintsForContents()
//
      if((eventsArray?.count ?? 0) > 0 ) {
        //load contents in CalendarView
          populateCalendarView()
          
          //load contents in MonthView
          populateMonthView()
      }
      
  
      //Register for Notification
//      NotificationCenter.default.addObserver(self, selector:#selector(handleOrientationChange) , name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange), object: nil)

//      handleOrientationChange()
//
//      //Add gesture recognizer to handle tap for event loaded in Home Screen
      let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
      stackView.addGestureRecognizer(tap)
    }
  }
  
  /* Method to populate Calendar View */
  func populateCalendarView(){
    
    let eventObj = eventsArray?[0]
    
    location.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: "#0D1C49")
    eventsTitle.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: "#0D1C49")
    
    if(eventObj != nil){
      eventsTitle.text = eventObj?.title
      location.text = eventObj?.location
    }
  }
  
  /* Method to populate Calendar View */
  func populateMonthView(){
    
    let eventObj = eventsArray?[0]
    
//    mainView.backgroundColor = RedesignThemesClass.sharedInstance.blueBackgroundColorForEvents
//    self.mainView.layer.cornerRadius = 4.0
//    self.mainView.alpha = 0.3
    
    if(eventObj != nil){
      let startDate = EventsServiceClass.sharedAPIManager().getEventsDateString(eventObj?.startDate ?? "")
      let startMonth = EventsServiceClass.sharedAPIManager().getMonthOfStartDate(eventObj?.startDate ?? "")
      let endDate = EventsServiceClass.sharedAPIManager().getEventsDateString(eventObj?.endDate ?? "")
      let endMonth = EventsServiceClass.sharedAPIManager().getMonthOfStartDate(eventObj?.endDate ?? "")
      monthsView.text = "\(startMonth) \(startDate)" + " - " + "\(endMonth) \(endDate)"
    }
  }
    
    /* Method called on tap of More Button */
    @IBAction func tapMoreButton(_ sender: Any) {
      let eventsHome:EventsViewModel = EventsViewModel()
      eventsHome.loadEventsLandingController()
    }
    /* Method to load event details on tap of event in Home Screen */
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
      // handling code
      let eventsHome:EventsViewModel = EventsViewModel()
      eventsHome.loadEventSelected(selectedEvet: (eventsArray?[0])!)
    }
    /* Method to set message */
    func setMessage(_ message: String) {
      let messageLabel = UILabel(frame: CGRect(x: 100, y: 50, width: self.bounds.size.width, height: self.bounds.size.height))
      messageLabel.text = message
      messageLabel.textColor = .black
      messageLabel.numberOfLines = 0
      messageLabel.textAlignment = .center
      messageLabel.font = UIFont(name: "Lato-Regular", size: 14.0)
      messageLabel.sizeToFit()
      
      self.stackView.addSubview(messageLabel)
      
    }
}
