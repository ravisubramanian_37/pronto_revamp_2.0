//
//  HomeViewEventsMain.swift
//  Pronto
//
//  Created by Saranya on 25/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class HomeViewEventsMain: UIView {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var monthView: UIView!
    
    var equalWidthConstraint: NSLayoutConstraint!
    var equalHeightConstraint: NSLayoutConstraint!
    var calendarHeightConstraint: NSLayoutConstraint!
    var calendarWidthConstraint: NSLayoutConstraint!

  @IBOutlet weak var leadingConstraintForCalendarInternalView: NSLayoutConstraint!
  @IBOutlet weak var trailingConstraintForCalendarInternalView: NSLayoutConstraint!
    var topConstraintForCalendarInternalView: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintForCalendarInternalView: NSLayoutConstraint!
    @IBOutlet weak var meetingLabel: UILabel!
    @IBOutlet weak var venueLabel: UILabel!
    @IBOutlet weak var calendarInternalView: UIView!
    
    @IBOutlet weak var monthInternalView: UIView!
    @IBOutlet weak var monthInternalViewLeadingAnchor: NSLayoutConstraint!
    @IBOutlet weak var monthInternalViewTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var monthInternalViewTrailingAnchor: NSLayoutConstraint!
    @IBOutlet weak var monthInternalViewBottomAnchor: NSLayoutConstraint!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var monthLabelTopAnchor: NSLayoutConstraint!
    
    var eventsArray: [Event]?
    // ["Video Type Asset"]
    
    override func awakeFromNib() {
      super.awakeFromNib()
    }
    
    /* Method to add additional setup */
    func doInitialSetUp() {
        
        eventsArray?.removeAll()
        self.eventsArray = ProntoRedesignSingletonClass.sharedInstance.eventsAllArray?.sorted(by: { $0.getPriority() < $1.getPriority() })
        ProntoRedesignSingletonClass.sharedInstance.eventsAllArray = self.eventsArray
        
        headerLabel.text = RedesignConstants.eventsTitle
        
        // Set themes
        headerView.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        RedesignUtilityClass.sharedInstance.setShadow(view: &stackView)
        
        // Disable more button if events is equal to or less than 1
        if (eventsArray?.count ?? 0 == 0) {
            moreButton.isEnabled = false
            moreButton.isHidden = true
            calendarView.isHidden = true
            monthView.isHidden = true
            self.backgroundColor = UIColor.red
            self.setMessage(RedesignConstants.noEventsContentText)
        } else {
//            if(eventsArray?.count ?? 0 == 1 ){
//                moreButton.isEnabled = false
//                moreButton.isHidden = true
//            }else{
                moreButton.isHidden = false
//            }
            // Add constraint for StackView
            stackView.distribution = .fill
            updateCosntraintsForContents()
        
            // load contents in CalendarView
            populateCalendarView()
        
            // load contents in MonthView
            populateMonthView()
        
            // Register for Notification
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(handleOrientationChange),
                                                   name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange),
                                                   object: nil)
        
            handleOrientationChange()
            
            // Add gesture recognizer to handle tap for event loaded in Home Screen
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            stackView.addGestureRecognizer(tap)
        }
    }

    /* Method to populate Calendar View */
    func populateCalendarView() {
        
        let eventObj = eventsArray?[0]
        
        venueLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: "#0D1C49")
        meetingLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: "#0D1C49")
        
        calendarInternalView.backgroundColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: "#F6F6F5")
        RedesignUtilityClass.sharedInstance.setShadow(view: &calendarInternalView)
        
        if (eventObj != nil) {
            meetingLabel.text = eventObj?.title
            venueLabel.text = eventObj?.location
        }
        
    }
    
    /* Method to populate Calendar View */
    func populateMonthView() {
        
        let eventObj = eventsArray?[0]
        
        monthInternalView.backgroundColor = RedesignThemesClass.sharedInstance.blueBackgroundColorForEvents
//        monthInternalView.roundCornersWithLayerMask(cornerRadii: 4.0, corners: [.topRight, .bottomRight])
        
        if (eventObj != nil) {
            monthLabel.text = EventsServiceClass.sharedAPIManager().getEventsHomeMonthString(eventObj?.startDate ?? "", endDate: eventObj?.endDate ?? "")
            dateLabel.text = EventsServiceClass.sharedAPIManager().getEventsHomeDateString(eventObj?.startDate ?? "", endDate: eventObj?.endDate ?? "")
        }
    }
    
    /* Method to update constraints for Contents */
    func updateCosntraintsForContents() {
        
        equalWidthConstraint = calendarView.widthAnchor .constraint(equalTo: monthView.widthAnchor, multiplier: 2.5)
        equalHeightConstraint = calendarView.heightAnchor .constraint(equalTo: monthView.heightAnchor, multiplier: 1.0)
        
        equalWidthConstraint.isActive = true
        equalHeightConstraint.isActive = true
        
        calendarWidthConstraint = calendarView.widthAnchor .constraint(equalTo: monthView.widthAnchor, multiplier: 1.0)
        calendarHeightConstraint = calendarView.heightAnchor .constraint(equalTo: monthView.heightAnchor, multiplier: 0.4)
        
//        topConstraintForCalendarInternalView = calendarView.topAnchor .constraint(equalTo: calendarView.topAnchor, constant: 0)
//
//        topConstraintForCalendarInternalView.isActive = true
//

    }
    /* Method called to handle Notification */
    @objc func handleOrientationChange() {
      stackView.axis = .horizontal
      if let myView = stackView.subviews.first {
        
        stackView.removeArrangedSubview(myView)
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()
        stackView.insertArrangedSubview(myView, at: 0)
        
        // Updating constraints for Events Main Content
        equalWidthConstraint.isActive = true
        equalHeightConstraint.isActive = true
        calendarWidthConstraint.isActive = false
        calendarHeightConstraint.isActive = false
        
        // Updating constraints for Calendar Content
        trailingConstraintForCalendarInternalView.constant = 0
//        topConstraintForCalendarInternalView.constant = 0
        bottomConstraintForCalendarInternalView.constant = 0
        
        // Updating constraints for Month Content
        monthInternalViewTopAnchor.constant = 6
        monthInternalViewLeadingAnchor.constant = 0
        monthInternalViewBottomAnchor.constant = 0
        monthInternalViewTrailingAnchor.constant = 0
//        monthLabelTopAnchor.constant = 0
        
        stackView.setNeedsLayout()
      }
    }
    /* Method called on tap of More Button */
    @IBAction func tapMoreButton(_ sender: Any) {
        let eventsHome: EventsViewModel = EventsViewModel()
        eventsHome.loadEventsLandingController()
    }
    /* Method to load event details on tap of event in Home Screen */
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        let eventsHome: EventsViewModel = EventsViewModel()
        eventsHome.loadEventSelected(selectedEvet: (eventsArray?[0])!)
    }
  /* Method to set message */
    func setMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 100, y: 50, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Lato-Regular", size: 14.0)
        messageLabel.sizeToFit()
        
        self.stackView.addSubview(messageLabel)
        
    }
}

extension UIView {
func roundCornersWithLayerMask(cornerRadii: CGFloat, corners: UIRectCorner) {
    let path = UIBezierPath(roundedRect: bounds,
                            byRoundingCorners: corners,
                            cornerRadii: CGSize(width: cornerRadii, height: cornerRadii))
    let maskLayer = CAShapeLayer()
    maskLayer.path = path.cgPath
    layer.mask = maskLayer
  }
}
