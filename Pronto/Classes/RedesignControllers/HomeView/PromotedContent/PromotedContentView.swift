//
//  PromotedContentView.swift
//  Pronto
//
//  Created by Saranya on 19/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
protocol HomeWeblinkViewProtocol {
  func openWebLink(uri: String)
}
class PromotedContentView: UIView, iCarouselDelegate, iCarouselDataSource {
    
    @IBOutlet weak var previousAssetButton: UIButton!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var nextAssetButton: UIButton!
    
    var promotedContentArray: [Asset]?
    let carousel = iCarousel()
    var selectedIndex: Int?
    var globalPromotedaudioView: UIView?
    var weblinkDelegate: HomeWeblinkViewProtocol?
    
    override func awakeFromNib() {
    super.awakeFromNib()
    
    }
    
    /* Method to add carousel View */
    func doAdditionalSetUp() {
        
        promotedContentArray = ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray
        
        if (promotedContentArray?.count ?? 0 > 0) {
            loadCarouselView()
        } else {
          self.isHidden = true
        }
        registerForNotification()
    }
    
    /* Register for Notification */
    func registerForNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrientationChange),
                                               name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange),
                                               object: nil)
    }
    /* Method called to handle Orientation Change Notification */
    @objc func handleOrientationChange() {
        carousel.type = .linear
        carousel.reloadData()
    }

    /* Load carousel view */
    func loadCarouselView() {
        // add carousel
        carousel.backgroundColor = .white
        carousel.dataSource = self
        carousel.delegate = self
        carousel.type = .linear
        carousel.clipsToBounds = true
        mainContentView.addSubview(carousel)
        
        // addConstraints
        setUpConstraints()
        
        // disable previous button initially
        previousAssetButton.isEnabled = false
        
        // setting image for next button
        nextAssetButton.setImage((UIImage.init(named: RedesignConstants.nextItemImageNormal)), for: .normal)
        nextAssetButton.setImage((UIImage.init(named: RedesignConstants.nextItemImageDisabled)), for: .disabled)
        
        //Enabling next button
        if (promotedContentArray?.count ?? 0 > 1) {
            nextAssetButton.isEnabled = true
        } else {
            nextAssetButton.isEnabled = false
        }
    }
    /* Method to set Constraints for Carousel */
    func setUpConstraints(){
        carousel.translatesAutoresizingMaskIntoConstraints = false
        carousel.pinEdges(to: mainContentView)
    }
    
    /* Carousel Datasource Methods */
    func numberOfItems(in carousel: iCarousel) -> Int {
        return promotedContentArray?.count ?? 0
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let promotedContentHomeview: PromotedContentHomeView = Bundle.main.loadNibNamed("PromotedContentHomeView", owner: self,
                                                                                        options: nil)?.first as! PromotedContentHomeView
        if((UIDevice.current.orientation == UIDeviceOrientation.landscapeRight) || (UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft)){
            promotedContentHomeview.frame = CGRect(x: 0, y: 0, width: 1366, height: carousel.frame.size.height)
        }else{
            promotedContentHomeview.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: carousel.frame.size.height)
        }
      print("Carousel Size", promotedContentHomeview.frame)

        let selectedAsset = promotedContentArray?[index]
        promotedContentHomeview.thumbnailImageview.image = (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).setThumbnailPath(selectedAsset?.field_thumbnail)
        
        promotedContentHomeview.titleLabel.text = selectedAsset?.title
        promotedContentHomeview.titleLabel.textColor = .white
        promotedContentHomeview.titleLabel.backgroundColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode).withAlphaComponent(0.75)
        let assetType = Constant.getFileType(for: selectedAsset)
        var imageName: String = ""

        if (promotedContentHomeview.thumbnailImageview.image == nil) {
            promotedContentHomeview.thumbnailImageview.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
            promotedContentHomeview.thumbnailImageview.isOpaque = true
            if (assetType == ZMProntoFileTypesPDF) {
                imageName = RedesignConstants.centerImagePDF
            } else if (assetType == ZMProntoFileTypesVideo) {
                imageName = RedesignConstants.centerImageVideo
            } else if (assetType == ZMProntoFileTypesBrightcove) {
                imageName = RedesignConstants.centerImageVideo
            } else if (assetType == ZMProntoFileTypesDocument) {
                imageName = RedesignConstants.centerImagePDF
            } else if (assetType == ZMProntoFileTypesWeblink){
                imageName = RedesignConstants.centerImageWeblink
            } else if (assetType == ZMProntoFileTypesAudio){
                imageName = RedesignConstants.audioLinkName
            }
            promotedContentHomeview.bringSubviewToFront(promotedContentHomeview.centerImageView)
            promotedContentHomeview.centerImageView.image = UIImage(named: imageName)
            promotedContentHomeview.centerImageView.backgroundColor = .white
            
        }
        return promotedContentHomeview
    }
    
    /* carousel Delegate Methods */
    func carousel(_ carousel: iCarousel, shouldSelectItemAt index: Int) -> Bool {
        return true
    }
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        selectedIndex = index
        if let asset = promotedContentArray?[index] {
            let assetType = Constant.getFileType(for: asset)
            ProntoAssetHandlingClass.sharedInstance().isFromHomeView = false
            if (assetType == ZMProntoFileTypesAudio) {
                let promotedView: PromotedContentHomeView = carousel.itemView(at:index) as! PromotedContentHomeView
                loadAudioAssetPopOver(asset: asset, sender: promotedView)
                return
            }
            if assetType == ZMProntoFileTypesWeblink {
              self.weblinkDelegate?.openWebLink(uri: asset.uri_full ?? "")
            } else {
              PromotedContentsViewModel.sharedInstance.openAsset(selectedAsset: asset, audioView: nil)
            }
        }
    }
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        if (carousel.currentItemIndex == 0) {
            nextAssetButton.isEnabled = true
            previousAssetButton.isEnabled = false
        }
        else if (carousel.currentItemIndex < (promotedContentArray!.count-1)) {
            nextAssetButton.isEnabled = true
            previousAssetButton.isEnabled = true
        }
        else {
            nextAssetButton.isEnabled = false
            previousAssetButton.isEnabled = true
        }
    }
   
    /* Previous Button Action */
    @IBAction func tapPreviousAssetButton(_ sender: Any) {
        if globalPromotedaudioView != nil{
            PromotedContentsViewModel.sharedInstance.audioView?.stopAudio()
            globalPromotedaudioView?.removeFromSuperview()
            PromotedContentsViewModel.sharedInstance.prontoAssetHandlingClassObject?.homePromotedContentAudioView = UIView()
        }
        carousel.scroll(byNumberOfItems: -1, duration: 1.0)
    }
    
    /* Next Button Action */
    @IBAction func tapNextButtonAction(_ sender: Any) {
        if (globalPromotedaudioView != nil) {
            if (self.subviews .contains(globalPromotedaudioView!)) {
                PromotedContentsViewModel.sharedInstance.audioView?.stopAudio()
                globalPromotedaudioView?.removeFromSuperview()
                PromotedContentsViewModel.sharedInstance.prontoAssetHandlingClassObject?.homePromotedContentAudioView = UIView()
            }
        }
        carousel.scroll(byNumberOfItems: 1, duration: 1.0)
    }
    /* Method to load Audio view */
    func loadAudioAssetPopOver(asset: Asset, sender: PromotedContentHomeView) {

        globalPromotedaudioView = UIView()
        self.addSubview(globalPromotedaudioView!)
        PromotedContentsViewModel.sharedInstance.openAsset(selectedAsset: asset, audioView: globalPromotedaudioView)
        globalPromotedaudioView?.translatesAutoresizingMaskIntoConstraints = false

        globalPromotedaudioView?.widthAnchor .constraint(equalToConstant: self.frame.size.width/2).isActive = true
        globalPromotedaudioView?.heightAnchor .constraint(equalToConstant: self.frame.size.height/2).isActive = true
        globalPromotedaudioView?.centerXAnchor .constraint(equalTo: self.centerXAnchor).isActive = true
        globalPromotedaudioView?.centerYAnchor .constraint(equalTo: self.centerYAnchor).isActive = true
    }
}

