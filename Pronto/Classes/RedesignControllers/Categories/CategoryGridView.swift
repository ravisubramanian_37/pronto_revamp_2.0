//
//  CategoryGridView.swift
//  Pronto
//
//  Created by Saranya on 18/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
protocol WeblinkGridViewProtocol {
  func openWeblink(uri: String)
}
class CategoryGridView: UIView,
                        UICollectionViewDelegate,
                        UICollectionViewDataSource {
    var collectionView: UICollectionView!
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    var selectedCategory: Categories!
    var parentController: CategoriesViewController?
    var isObjectAvailable = false
    var screenWidth = UIScreen.main.bounds.size
    var audioPlayerView: AudioPlayer?
    var assetForAudio: Asset?
    var subfolderArray: [Categories]? = []
    var assetsArray: [Asset]? = []
    
    var isMoreCategoryView: Bool = false
    var foldersArray: [Categories]?
    var utilityVar: AppUtilities = AppUtilities.sharedUtilities() as! AppUtilities
    var weblinkDelegate: WeblinkGridViewProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame) // calls designated initializer
        configureMainView()
    }
    
    func setDataForMoreCategories(foldersArray: [Categories],
                                  parentContlr: CategoriesViewController,
                                  objAvailable: Bool){
        self.parentController = parentContlr
        self.isObjectAvailable = objAvailable
        self.foldersArray = foldersArray
        
        // Converting subfolder set to array
        if self.foldersArray?.count ?? 0 > 0 {
            for item in foldersArray {
                self.subfolderArray?.append(item)
            }
        }
    }
    
    func setData(selectedCat: Categories, parentContlr: CategoriesViewController, objAvailable: Bool) {
        self.selectedCategory = selectedCat
        self.parentController = parentContlr
        self.isObjectAvailable = objAvailable
        
        //Converting subfolder set to array
        if let subFoldersArray = self.selectedCategory.subfolders {
            for item in subFoldersArray{
                if item.asset_count?.intValue != 0 {
                    self.subfolderArray?.append(item)
                }
            }
        }
        
      HomeViewModel.sharedInstance.setSortingAssetsArray(selectedCategory: self.selectedCategory)
        if let assetArray = ProntoRedesignSingletonClass.sharedInstance.sortingsasetsArray {
            self.assetsArray = assetArray
            //Bydefault sort by Recently Added
            self.assetsArray?.sort {return $0.publish_on!.compare($1.publish_on!) == .orderedDescending}
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError(RedesignConstants.commonInitError)
    }
    func configureMainView()
    {
        collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: self.frame, collectionViewLayout: collectionViewFlowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        let collectioncell = UINib.init(nibName: RedesignConstants.collectionCellNibName, bundle:nil)
        collectionView.register(collectioncell, forCellWithReuseIdentifier: RedesignConstants.collectionCellId)
        collectionView.register(UINib.init(nibName: RedesignConstants.collectionGridAssetNibName, bundle:nil), forCellWithReuseIdentifier: RedesignConstants.collectionGridAssetNibName)
        collectionView.backgroundColor = .white
        collectionView.isScrollEnabled = true
        self.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.pinEdges(to: collectionView)
        decideViewSpacing(isReload: false)
            
        //Register for Notification
        registerForNotification()
    }
    func registerForNotification() {
        //Register for Notification
        NotificationCenter.default.addObserver(self, selector:#selector(handleOrientationChange) , name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadSortingResults(_:)), name: NSNotification.Name(rawValue: RedesignConstants.sortingReloadNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadForFavourites), name: Notification.Name(rawValue: RedesignConstants.favouritesAssetUpdationNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(hideBottomView) , name: NSNotification.Name(rawValue: RedesignConstants.hideBottomViewNotification), object: nil)
    }
    /* Method to reload cells when Favourites is updated in Asset Tool bar */
    @objc func reloadForFavourites() {
        if(isObjectAvailable){
            collectionView.reloadData()
        }
    }
  /* Method to hide bottom view */
      @objc func hideBottomView() {
        utilityVar.showBottomIndicatorView(false, withMessage: nil)
      }
    /* Method to reload view on sorting option */
    @objc func reloadSortingResults(_ notification: NSNotification){
        let notificationDict = notification.userInfo
        self.assetsArray = notificationDict?[RedesignConstants.sortingTermKey] as? [Asset]
        ProntoRedesignSingletonClass.sharedInstance.sortingsasetsArray = self.assetsArray
        collectionView.reloadData()
    }
    /* Method called to handle Orientation Change Notification */
    @objc func handleOrientationChange() {
       if(isObjectAvailable){
           decideViewSpacing(isReload: true)
           collectionView.setNeedsLayout()
           collectionView.layoutIfNeeded()
       }
    }
    func decideViewSpacing(isReload:Bool) {
        var numberOfItemsPerRow:CGFloat = 4
        if((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft) || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)){
            numberOfItemsPerRow = 5
        }
        let spacingBetweenCells:CGFloat = 5
              
        let totalSpacing = (2 * 5) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        var screenSize:CGFloat = screenWidth.width
        if((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft) || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)){
            screenSize = max(screenWidth.width, screenWidth.height)
        }
        else{
            screenSize = min(screenWidth.width, screenWidth.height)
        }
        var size :CGSize = CGSize(width: 0, height: 0)
        let width = (screenSize - totalSpacing)/numberOfItemsPerRow
        size =  CGSize(width: width, height: width)
            
        collectionViewFlowLayout.itemSize = CGSize(width: size.width , height: 200)
        collectionViewFlowLayout.minimumInteritemSpacing = 0
    }
    /* UICollectionView DataSource & Delegate Methods */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return calculateNoOfItems()
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //create folders
        if let selectedSubfolder = subfolderArray {
            if (indexPath.row >= selectedSubfolder.count) {
                return createAssetCell(index: indexPath)
            }
            else{
                let cell: HomeScreenCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:RedesignConstants.collectionCellId,
                                                                                       for: indexPath) as! HomeScreenCollectionCell
                let category:Categories = (subfolderArray?[indexPath.row])!
                if ((category.asset_count?.intValue ?? 0) <= 1)
                {
                    cell.assetCount.text = String(category.asset_count!.intValue) + RedesignConstants.assetTxt
                }
                else{
//                    cell.assetCount.text = String(category.assets!.count) + RedesignConstants.assetsTxt
                    cell.assetCount.text = String(category.asset_count!.intValue) + RedesignConstants.assetsTxt
                }
                cell.categoryTitle.text = category.name
                cell.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
                cell.imageView.image = UIImage(named:RedesignConstants.ImageType.normalCategory.rawValue)!
                return cell
            }
        }else{
            return createAssetCell(index: indexPath)
        }

    }
    
    func createAssetCell(index: IndexPath) -> UICollectionViewCell {
        var indexVar = index.row
        if let selectedSubfolder = selectedCategory?.subfolders {
            if(selectedSubfolder.count > 0) {
                indexVar -= selectedSubfolder.count
            }
          
          if let selectedAssets = selectedCategory?.assets {
            if(selectedAssets.count > 0 && selectedSubfolder.count > 0) {
                if(indexVar < 0){
                    indexVar = 0
                }
                if(indexVar == selectedAssets.count) {
                    indexVar = selectedAssets.count - 1
                }
              print("indexVar", indexVar)
              }
          }
        }
        
        var asset: Asset! = (self.assetsArray?[indexVar])!
        var cell: ZMGridItem = collectionView.dequeueReusableCell(withReuseIdentifier: RedesignConstants.collectionGridAssetNibName,
                                                                 for: index) as! ZMGridItem

        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1.0
        cell.redesignDotLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)
        cell.nameLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)
        cell.assetFrame.layer.borderColor = UIColor.gray.cgColor
        cell.assetFrame.layer.borderWidth = 0.25
        cell.nameLabel.text = asset.title
        
        //Hide loader background
      if ProntoAssetHandlingClass.sharedInstance().selectedGridCell != nil && ProntoAssetHandlingClass.sharedInstance().currentAssetDownload != nil {
        if ProntoAssetHandlingClass.sharedInstance().selectedGridCell.nameLabel.text == cell.nameLabel.text {
          cell = ProntoAssetHandlingClass.sharedInstance().selectedGridCell
        } else {
          utilityVar.showBottomIndicatorView(false, withMessage: nil)
          cell.loaderBackgroundView.backgroundColor = UIColor.clear
          cell.loaderBackgroundView.alpha = 1
          cell.loaderBackgroundView.isHidden = true
          cell.audioPlayerView.isHidden = true
        }
      } else {
        utilityVar.showBottomIndicatorView(false, withMessage: nil)
        cell.loaderBackgroundView.backgroundColor = UIColor.clear
        cell.loaderBackgroundView.alpha = 1
        cell.loaderBackgroundView.isHidden = true
        cell.audioPlayerView.isHidden = true
      }

        cell.uid = asset.assetID?.intValue ?? 0
        cell.type = ZMGridItemTypeAsset

        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        cell.isVideo = asset.getType() == RedesignConstants.videoText
        cell.assetType = (asset.getType()) as NSString
        // removing thumbnail path
        cell.thumbnailPath = asset.field_thumbnail as NSString?

        cell.reviews = Int32(asset.votes?.intValue ?? 0)
        cell.totalViews = Int32(asset.view_count?.intValue ?? 0)
        cell.totalDuration = asset.field_length
        cell.isPopularDocument = false
        
        cell.shareButton.isSelected = false
        cell.infoButton.isSelected = false
        cell.favButton.isSelected = false

        cell.shareButton.tag = index.row
        cell.favButton.tag = index.row
        cell.infoButton.tag = index.row

        cell.shareButton.addTarget(self, action: #selector(exportIconClicked(_:)), for: .touchUpInside)
        cell.favButton.addTarget(self, action: #selector(favIconClicked(_:)), for: .touchUpInside)
        cell.infoButton.addTarget(self, action: #selector(infoIconClicked(_:)), for: .touchUpInside)
                // New revamping changes - Status of asset
        if let changetime = asset.changed, let createtime = asset.created
        {
            let myTimeInterval = TimeInterval(changetime.doubleValue)
            let mychgdate = Date(timeIntervalSince1970: myTimeInterval)
            let mycreatInterval = TimeInterval(createtime.doubleValue)
            let mycreatedate = Date(timeIntervalSince1970: mycreatInterval)
            let calendar = Calendar.current
            let numberOfDays = calendar.dateComponents([.day], from: mychgdate, to: Date())
            let order = Calendar.current.compare(mychgdate, to: mycreatedate, toGranularity: .day)
            if (order == .orderedSame && numberOfDays.day! + 1 <= 30)
            {
              if asset.is_new {
                cell.updateLabel.isHidden = true
              } else {
                cell.updateLabel.isHidden = false
              }
            }
            else if (order == .orderedDescending && numberOfDays.day! + 1 <= 30)
            {
//                cell.isUpdateAvailable = true
                cell.updateLabel.isHidden = false
            }
            else{
//                cell.isUpdateAvailable = false
                cell.updateLabel.isHidden = true
            }
        }
    
       cell.isNew = asset!.is_new

      let assetType = Constant.getFileType(for: asset!)
      print("isDownloaded", asset.isDownloaded)
      if !UserDefaults.standard.bool(forKey: RedesignConstants.firstTimeloadingAssets) {
        let checkLocally = CategoryViewModel.sharedInstance.checkAssetIDAvailableLocally(assetID: asset.assetID ?? 0, title: asset.title ?? "")
        if assetType == ZMProntoFileTypesPDF {
          if checkLocally {
            cell.downloadButton.isHidden = true
            asset.savedPath = CategoryViewModel.sharedInstance.retrieveAsset(assetID: asset.assetID ?? 0)
            self.assetsArray?[indexVar].isDownloaded = checkLocally
            self.assetsArray?[indexVar].savedPath = asset.savedPath
          } else {
            cell.downloadButton.isHidden = false
          }
        } else {
          cell.downloadButton.isHidden = true
        }
      } else {
        cell.downloadButton.isHidden = assetType == ZMProntoFileTypesPDF ? false : true
      }
    
    if !(asset.path == "") || (asset.path == RedesignConstants.brightCoveText) {
      cell.read = true
    } else {
      cell.read = false
    }
    let favfranchise = ZMCDManager.sharedInstance.getFavFranchise()
    cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
    cell.favButton.isSelected = false
    
    for fav in favfranchise {
      guard let fav = fav as? FavFranchise else {
        continue
      }
      
      if cell.uid == fav.asset_ID?.intValue {
        cell.favButton.setImage(UIImage(named: RedesignConstants.favoSelected), for: .selected)
        cell.favButton.isSelected = true
        break
      } else {
        cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
        cell.favButton.isSelected = false
      }
    }
    if let audioAsset = ProntoRedesignSingletonClass.sharedInstance.audioAsset{
      assetForAudio = audioAsset
    }
    if asset.assetID == assetForAudio?.assetID && assetForAudio?.assetID != 0 {
      cell.audioPlayerView.isHidden = false
      audioPlayerView?.removeFromSuperview()
      if let audioView = ProntoRedesignSingletonClass.sharedInstance.audioAssetView {
        audioPlayerView = audioView
      }
      audioPlayerView?.frame = CGRect(x: 0, y: 0, width: cell.audioPlayerView.frame.size.width, height: cell.audioPlayerView.frame.size.height)
      if let audioView = audioPlayerView{
        cell.audioPlayerView.addSubview(audioView)
      }
    }
    centerImage(cell: cell, asset: asset)
    return cell
  }
  /* Set center Image */
  func centerImage(cell: ZMGridItem,asset: Asset) {
    //Redesign center view
    var imageName:String = ""
    let assetType = Constant.getFileType(for: asset)
    
    var centerImageName:String = ""
    
    if (assetType == ZMProntoFileTypesPDF) {
      imageName = RedesignConstants.pdfName
      if(cell.thumbnail.image == nil){
        centerImageName = RedesignConstants.centerImagePDF
      }
    } else if (assetType == ZMProntoFileTypesVideo) {
      imageName = RedesignConstants.videoName
      if(cell.thumbnail.image == nil){
        centerImageName = RedesignConstants.centerImageVideo
      }
    } else if (assetType == ZMProntoFileTypesBrightcove) {
      imageName = RedesignConstants.videoName
      if(cell.thumbnail.image == nil){
        centerImageName = RedesignConstants.centerImageVideo
      }
    } else if (assetType == ZMProntoFileTypesDocument) {
      imageName = RedesignConstants.pdfName
      if(cell.thumbnail.image == nil){
        centerImageName = RedesignConstants.centerImagePDF
      }
    } else if (assetType == ZMProntoFileTypesWeblink)
    {
      imageName = RedesignConstants.weblinkName
      if(cell.thumbnail.image == nil){
        centerImageName = RedesignConstants.centerImageWeblink
      }
    } else if (assetType == ZMProntoFileTypesAudio)
    {
      imageName = RedesignConstants.centerImageAudio
      if(cell.thumbnail.image == nil){
        centerImageName = RedesignConstants.audioLinkName
      }
    }
    
    
    cell.redesignCenterImgView.image = UIImage(named: imageName);
    if (centerImageName.count > 0) {
      cell.assetBackGroundView.backgroundColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.lightGrayForAssetBackground)
      cell.centerTopAssetImage.image = UIImage(named: centerImageName)
      
    } else {
      //            cell.centerTopAssetImage.image = UIImage(named: "")
      cell.centerTopAssetImage.image = nil
      cell.assetBackGroundView.backgroundColor = .white
    }
  }
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewSearchListNotification), object: self, userInfo: nil)
    let cell = collectionView .cellForItem(at: indexPath) as? HomeScreenCollectionCell
    if (cell != nil){
      let categoryViewController = CategoriesViewController()
      let category:Categories? = subfolderArray?[indexPath.row]
      if let categoryObj = category {
        if (isMoreCategoryView) {
          HomeViewModel.sharedInstance.getAssetDetails(categoryObj: categoryObj,
                                                       index: (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) > 0 ?  9 +                                                  indexPath.row : 19 + indexPath.row,
                                                       previousHeaderStringName: parentController?.previousHeaderName ?? "",
                                                       parentView: self,
                                                       nav: (parentController?.navigationController)!)
        } else {
          if (categoryObj.assets?.count ?? 0 > 0 || categoryObj.subfolders?.count ?? 0 > 0) {
            categoryViewController.previousHeaderName = parentController?.previousHeaderName
            categoryViewController.selectedCategory = categoryObj
            ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
            parentController?.navigationController?.pushViewController(categoryViewController, animated: true)
          }
        }
      }
      
    } else if let cell = collectionView .cellForItem(at: indexPath) as? ZMGridItem {
      var indexVar = indexPath.row
      if let selectedSubfolder = selectedCategory?.subfolders {
          if(selectedSubfolder.count > 0) {
              indexVar -= selectedSubfolder.count
          }
        
        if let selectedAssets = selectedCategory?.assets {
          if(selectedAssets.count > 0 && selectedSubfolder.count > 0) {
              if(indexVar < 0){
                  indexVar = 0
              }
              if(indexVar == selectedAssets.count) {
                  indexVar = selectedAssets.count - 1
              }
            print("indexVar", indexVar)
          }
        }
      }
      
      let asset: Asset! = (self.assetsArray?[indexVar])!
      let assetType = Constant.getFileType(for: asset!)
      let progressRange = ProntoAssetHandlingClass.sharedInstance().selectedGridCell.progress > 0 && ProntoAssetHandlingClass.sharedInstance().selectedGridCell.progress < 1
      if ProntoAssetHandlingClass.sharedInstance().selectedGridCell != cell
          && progressRange {
        utilityVar.showBottomIndicatorView(true, withMessage: "Please wait, Download is in progress")
      } else {
        if assetType == ZMProntoFileTypesPDF {
          if !UserDefaults.standard.bool(forKey: RedesignConstants.firstTimeloadingAssets) {
            let checkLocally = CategoryViewModel.sharedInstance.checkAssetIDAvailableLocally(assetID: asset.assetID ?? 0, title: asset.title ?? "")
            if checkLocally {
              asset.savedPath = CategoryViewModel.sharedInstance.retrieveAsset(assetID: asset.assetID ?? 0)
              self.assetsArray?[indexVar].isDownloaded = checkLocally
              self.assetsArray?[indexVar].savedPath = asset.savedPath
              print("did select savedPath", self.assetsArray?[indexVar])
            }
          }
          
          if asset.savedPath != nil && asset.savedPath != "" {
            let fileSizePdf = fileSize(path: asset.savedPath ?? "")
            if fileSizePdf == asset.file_size?.intValue ?? 0 {
              ProntoAssetHandlingClass.sharedInstance().open(asset)
            } else {
              openAsset(selectedAsset: asset, selectedGridCell: cell, selectedlistItem: nil)
            }
          } else if asset.path != nil && asset.path != "" {
            let fileSizePdf = fileSize(path: asset.path ?? "")
            if fileSizePdf == asset.file_size?.intValue ?? 0 {
              ProntoAssetHandlingClass.sharedInstance().open(asset)
            } else {
              openAsset(selectedAsset: asset, selectedGridCell: cell, selectedlistItem: nil)
            }
          } else {
            openAsset(selectedAsset: asset, selectedGridCell: cell, selectedlistItem: nil)
          }
          UserDefaults.standard.set(false, forKey: RedesignConstants.firstTimeloadingAssets)
        } else if assetType == ZMProntoFileTypesWeblink {
          ProntoAssetHandlingClass.sharedInstance().open(asset)
          self.weblinkDelegate?.openWeblink(uri: asset.uri_full ?? "")
        } else {
          openAsset(selectedAsset: asset, selectedGridCell: cell, selectedlistItem: nil)
        }
      }
    }
  }
  func fileSize(path: String) -> UInt64 {
    let fileManager = FileManager.default
    do {
      let attributes = try fileManager.attributesOfItem(atPath: path)
      var fileSize = attributes[FileAttributeKey.size] as! UInt64
      let dict = attributes as NSDictionary
      fileSize = dict.fileSize()
      return fileSize
    }
    catch let error as NSError {
      print("Something went wrong: \(error)")
      return 0
    }
  }
  /* Method to calculate the number of items to be displayed in collection View */
  func calculateNoOfItems()->Int{
    var count = 0
    if let subFolder = self.assetsArray {
      count += subFolder.count
    }
    if let subFolder = subfolderArray {
      count += subFolder.count
    }
    return count
  }
  /* Handling asset options - Share */
  @objc func exportIconClicked(_ sender: UIButton) {
    if sender.isSelected {
      sender.setImage(UIImage(named: RedesignConstants.export_redesign), for: .normal)
      sender.isSelected = false
      DispatchQueue.main.async {
        self.collectionView.reloadItems(at: [IndexPath(row: sender.tag, section: 0)])
      }
    } else {
      sender.setImage(UIImage(named: RedesignConstants.export_redesign_selected), for: .selected)
      sender.isSelected = true
      
      let assetArrayIndex = currentCellIndex(index: sender.tag)
      CategoryViewModel.sharedInstance.handleExportPopOver(sender: sender,
                                                           parentCategoryGrid: self,
                                                           parentCategoryList: nil,
                                                           parentFavouritesGrid: nil,
                                                           parentFavouritesList: nil,
                                                           selectedAsset: self.assetsArray![assetArrayIndex])
    }
  }
  /* Handling asset options - Info */
  @IBAction func infoIconClicked(_ sender: UIButton) {
    // Pronto new revamp changes: setting selected/deselected info button's image
    if sender.isSelected {
      sender.setImage(UIImage(named: RedesignConstants.info_redesign), for: .normal)
      sender.isSelected = false
      DispatchQueue.main.async {
        self.collectionView.reloadItems(at: [IndexPath(row: sender.tag, section: 0)])
      }
    } else {
      sender.setImage(UIImage(named:  RedesignConstants.info_redesign_selected), for: .selected)
      sender.isSelected = true
      
      let assetArrayIndex = currentCellIndex(index: sender.tag)
      CategoryViewModel.sharedInstance.configureInfoPopOver(sender,
                                                            parentCategoryGrid: self,
                                                            parentCategoryList: nil,
                                                            parentFavouritesGrid: nil,
                                                            parentFavouritesList: nil,
                                                            selectedAsset: self.assetsArray![assetArrayIndex])
    }
  }
  /* Handle Opening of Asset */
  func openAsset(selectedAsset: Asset,
                 selectedGridCell: ZMGridItem?,
                 selectedlistItem: FranchiseTableViewCell?) {
    CategoryViewModel.sharedInstance.openAsset(selectedAsset: selectedAsset,
                                               selectedGridCell: selectedGridCell,
                                               selectedlistItem: nil,
                                               selectedCategoryListView: nil,
                                               selectedCategoryGridView: self,
                                               selectedFavouritesListView: nil,
                                               selectedFavouritesGridView: nil)
  }
  /* To find current cell index */
  func currentCellIndex(index:Int) -> Int {
    var indexVar = index
    if let selectedSubfolder = selectedCategory?.subfolders{
      if(selectedSubfolder.count > 0){
        indexVar -= selectedSubfolder.count
      }
    }
    if(indexVar <= 0){
      indexVar = 0
    }
    return indexVar
  }
  /* Handle favo icon tapped Action */
  @IBAction func favIconClicked(_ sender: UIButton) {
    let assetArrayIndex = currentCellIndex(index: sender.tag)
    
    if sender.isSelected {
      sender.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .normal)
      sender.isSelected = false
      
      MyFavouritesViewModel.sharedInstance.deleteAssetFromFavourites(asset: self.assetsArray![assetArrayIndex])
    } else {
      sender.setImage(UIImage(named: RedesignConstants.favoSelected), for: .selected)
      sender.isSelected = true
      
      MyFavouritesViewModel.sharedInstance.createFavObject(selectedAsset: self.assetsArray![assetArrayIndex])
    }
    sortFavAsset()
  }
  
  /// Sort favourite assets
  func sortFavAsset(){
    MyFavouritesViewModel.sharedInstance.checkFavoSingletonAssetArray()
  }
}
