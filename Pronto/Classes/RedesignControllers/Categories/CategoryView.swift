//
//  CategoryView.swift
//  Pronto
//
//  Created by Saranya on 12/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit

protocol CategoryProtocol {
  func addCategorySearchHistory(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int, mainStackView: UIView)
}
protocol WeblinkViewProtocol {
  func openWeblink(uri: String)
}

class CategoryView: UIView {
  @IBOutlet weak var subHeaderView: UIView!
  @IBOutlet weak var mainHeaderView: UIView!
  @IBOutlet weak var collectionContainerView: UIView!
  var backButtonViewFromParent: UIButton?
  var previousHeaderName: String?
  var listView: UIView!
  var collectionView: UIView!
  var selectedCategory: Categories?
  var parentController: CategoriesViewController?
  var isObjectAvailable = false
  var screenWidth = UIScreen.main.bounds.size
  var isMoreCategoryView: Bool = false
  var foldersArray: [Categories]?
  var subHeader: CategoryHeaderView!
  var delegate: CategoryProtocol?
  var webLinkDelegate: WeblinkViewProtocol?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    isObjectAvailable = true
  }
  // Setup views
  func doAdditionalSetUp() {
    let headerView:HomeMainHeaderView = Bundle.main.loadNibNamed(RedesignConstants.mainViewHeaderNib, owner: self, options: nil)?.first as! HomeMainHeaderView
    mainHeaderView.addSubview(headerView)
    headerView.categoryDelegate = self
    headerView.setThemes()
    /* Add constraints for Categories View */
    headerView.translatesAutoresizingMaskIntoConstraints = false
    headerView.pinEdges(to: mainHeaderView)
    
    //Configure Sub Header for Categories
    configureSubHeader()
    
    if (ProntoRedesignSingletonClass.sharedInstance.isListChosen) {
      //Configure list View
      configureList()
    } else {
      //Configure Grid
      configureGrid()
    }
    
    //Set Themes
    self.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
    
    //Register for Notification
    registerForNotification()
    
    //Set Category Model Obj for Favourites
    ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen = false
  }
  //Register for Notification
  func registerForNotification(){
    NotificationCenter.default.addObserver(self, selector: #selector(handleGridOrListChange), name: Notification.Name(rawValue: RedesignConstants.notificationforListAndGridChange), object: nil)
  }
  
  /* Method called to handle List or Grid Notification */
  @objc func handleGridOrListChange() {
    if (ProntoRedesignSingletonClass.sharedInstance.isListChosen) {
      configureList()
    } else {
      configureGrid()
    }
  }
  /* Method to configure List View */
  func configureList() {
    if collectionView != nil{
      collectionView.removeFromSuperview()
    }
    let list = CategoryListView()
    list.weblinkListDelegate = self
    if(isMoreCategoryView) {
      list.isMoreCategoryView = true
      list.setDataForMoreCategories(foldersArray: self.foldersArray!, parentContlr: parentController!, objAvailable: isObjectAvailable)
    } else {
      list.setData(selectedCat: selectedCategory!, parentContlr: parentController!, objAvailable: isObjectAvailable)
    }
    listView = list
    collectionContainerView.addSubview(listView)
    list.prepareView()
    listView.translatesAutoresizingMaskIntoConstraints = false
    collectionContainerView.pinEdges(to: listView)
  }
  /* Method to configure Grid View */
  func configureGrid(){
    if listView != nil{
      listView.removeFromSuperview()
    }
    let grid = CategoryGridView()
    grid.weblinkDelegate = self
    if(isMoreCategoryView){
      grid.isMoreCategoryView = true
      grid.setDataForMoreCategories(foldersArray: self.foldersArray!, parentContlr: parentController!, objAvailable: isObjectAvailable)
    }else{
      grid.setData(selectedCat: selectedCategory!, parentContlr: parentController!, objAvailable: isObjectAvailable)
    }
    collectionView =  grid
    collectionContainerView.addSubview(collectionView)
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionContainerView.pinEdges(to: collectionView)
  }
  /* Method to configure header View */
  func configureSubHeader(){
    subHeader = Bundle.main.loadNibNamed(RedesignConstants.categoryHeaderNib, owner: self, options: nil)?.first as? CategoryHeaderView
    if(isMoreCategoryView){
      subHeader.isMoreCategoryView = true
    }
    subHeaderView.addSubview(subHeader)
    /* Add constraints for Categories header View */
    subHeader.translatesAutoresizingMaskIntoConstraints = false
    subHeader.pinEdges(to: subHeaderView)
    subHeader.parentController = parentController
    subHeader.parentViewTitle = previousHeaderName
    subHeader.selectedCategory = selectedCategory
    DispatchQueue.main.async {
      self.subHeader.doAdditionalSetUp()
    }
  }
}

/* Delegate to setup search history view */
extension CategoryView: CategorySearchHistoryProtocol {
  /// add search history view
    /// - Parameters:
    ///   - xAxis: xaxis for search history view
    ///   - yAxis: yaxis for search history view
    ///   - width: width for search history view
    ///   - height: height for search history view
    ///   - mainStackView: superview for search history view
  func categorySearchListView(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int) {
    self.delegate?.addCategorySearchHistory(xAxis: xAxis, yAxis: yAxis, width: width, height: height, mainStackView: self)
  }
}

extension CategoryView: WeblinkGridViewProtocol {
  func openWeblink(uri: String) {
    self.webLinkDelegate?.openWeblink(uri: uri)
  }
}

extension CategoryView: WeblinkListViewProtocol {
  func openWeblinkList(uri: String) {
    self.webLinkDelegate?.openWeblink(uri: uri)
  }
}

