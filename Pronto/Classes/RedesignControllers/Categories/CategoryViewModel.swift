//
//  CategoryViewModel.swift
//  Pronto
//
//  Created by Saranya on 12/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class CategoryViewModel: NSObject {
  @objc static let sharedInstance = CategoryViewModel()
  //For My Favourites
  var parentFavouritesGridView: MyFavouritesGridView?
  var parentFavouritesListView: MyFavouritesListView?
  //Export Options
  var selectedBtn: UIButton!
  @objc var parentCategoryGridView: CategoryGridView?
  @objc var parentCategoryListView: CategoryListView?
  @objc var selectedGridCell: ZMGridItem?
  @objc var updateProgress: Float = 0.0
  @objc var selectedListCell: FranchiseTableViewCell?
  var index: IndexPath!
  
  /* Clean up shared Instance */
  func cleanUpCategories() {
    CategoryViewModel.sharedInstance.selectedGridCell = nil
    CategoryViewModel.sharedInstance.selectedListCell = nil
    CategoryViewModel.sharedInstance.parentCategoryGridView = nil
    CategoryViewModel.sharedInstance.parentCategoryListView = nil
  }
  /* Set basic things needed to perform activities in this ViewModel*/
  func performBasicSettings(sender: UIButton,
                            parentCategoryGrid: CategoryGridView?, parentCategoryList: CategoryListView?,
                            parentFavouritesGrid: MyFavouritesGridView?,
                            parentFavouritesList: MyFavouritesListView?,
                            selectedAsset: Asset) {
    if (ProntoRedesignSingletonClass.sharedInstance.isListChosen) {
      if(ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen) {
        parentFavouritesGridView = nil
        selectedGridCell = nil
        parentCategoryGridView = nil
        parentCategoryListView = nil
        parentFavouritesListView = parentFavouritesList
      } else {
        parentFavouritesGridView = nil
        parentFavouritesListView = nil
        parentCategoryGridView = nil
        selectedGridCell = nil
        parentCategoryListView = parentCategoryList
        selectedListCell = parentCategoryListView?.tableView(parentCategoryListView!.tableView,
                                                             cellForRowAt:IndexPath(row: selectedBtn.tag, section: 0)) as? FranchiseTableViewCell
      }
    } else {
      if (ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen) {
        parentCategoryListView = nil
        parentCategoryGridView = nil
        parentFavouritesListView = nil
        selectedListCell = nil
        parentFavouritesGridView = parentFavouritesGrid
        selectedGridCell = parentFavouritesGridView?.collectionView((parentFavouritesGridView!.collectionView)!, cellForItemAt: IndexPath(row: selectedBtn.tag, section: 0) )as? ZMGridItem
      } else {
        parentFavouritesListView = nil
        parentFavouritesGridView = nil
        parentCategoryListView = nil
        selectedListCell = nil
        parentCategoryGridView = parentCategoryGrid
        selectedGridCell = parentCategoryGrid?.collectionView((parentCategoryGrid!.collectionView)!,
                                                              cellForItemAt: IndexPath(row: selectedBtn.tag, section: 0)) as? ZMGridItem
      }
    }
  }
  
  /* Handling Export PopOver */
  func handleExportPopOver(sender: UIButton, parentCategoryGrid: CategoryGridView?, parentCategoryList: CategoryListView?,
                           parentFavouritesGrid: MyFavouritesGridView?,
                           parentFavouritesList: MyFavouritesListView?, selectedAsset: Asset){
    selectedBtn = sender
    index = IndexPath(row: selectedBtn.tag, section: 0)
    
    performBasicSettings(sender: sender,
                         parentCategoryGrid: parentCategoryGrid,
                         parentCategoryList: parentCategoryList,
                         parentFavouritesGrid: parentFavouritesGrid,
                         parentFavouritesList: parentFavouritesList,
                         selectedAsset: selectedAsset)
    
    let assetModel = ProntoAssetHandlingViewModel()
    assetModel.categoryParent = self
    assetModel.configureExportPopOver(selectedAsset: selectedAsset, sender: sender)
    
    
  }
  
  /* Method to set back the Share Icon to Normal and close export popover */
  @objc func setExportToNormal(_ dismissExport: Bool) {
    if(ProntoRedesignSingletonClass.sharedInstance.isListChosen){
      selectedListCell?.shareButton.setImage(UIImage(named: RedesignConstants.export_redesign), for: .normal)
      selectedListCell?.shareButton.isSelected = false
      if(ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen){
        parentFavouritesListView?.tableView.reloadRows(at: [index], with: .none)
      }else{
        parentCategoryListView?.tableView.reloadRows(at: [index], with: .none)
      }
      
    }else{
      selectedGridCell?.shareButton.setImage(UIImage(named: RedesignConstants.export_redesign), for: .normal)
      selectedGridCell?.shareButton.isSelected = false
      if(ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen){
        parentFavouritesGridView?.collectionView.reloadItems(at: [index])
      }else{
        parentCategoryGridView?.collectionView.reloadItems(at: [index])
      }
    }
  }
  /* Method to handle Info Option */
  func configureInfoPopOver(_ sender: UIButton,
                            parentCategoryGrid: CategoryGridView?,
                            parentCategoryList: CategoryListView?,
                            parentFavouritesGrid: MyFavouritesGridView?,
                            parentFavouritesList: MyFavouritesListView?,
                            selectedAsset:Asset) {
    
    selectedBtn = sender
    index = IndexPath(row: selectedBtn.tag, section: 0)
    
    performBasicSettings(sender: sender,
                         parentCategoryGrid: parentCategoryGrid,
                         parentCategoryList: parentCategoryList,
                         parentFavouritesGrid: parentFavouritesGrid,
                         parentFavouritesList: parentFavouritesList,
                         selectedAsset: selectedAsset)
    
    let assetModel = ProntoAssetHandlingViewModel()
    assetModel.categoryParent = self
    assetModel.configureInfoPopOver(selectedAsset: selectedAsset, sender: sender)
  }
  
  /* Handle Opening of Asset */
  func openAsset(selectedAsset: Asset,
                 selectedGridCell: ZMGridItem?,
                 selectedlistItem: FranchiseTableViewCell?,
                 selectedCategoryListView: CategoryListView?, selectedCategoryGridView: CategoryGridView?,
                 selectedFavouritesListView: MyFavouritesListView?,
                 selectedFavouritesGridView: MyFavouritesGridView?) {
    
    ProntoAssetHandlingClass.sharedInstance().resetAssetValue()
    ProntoAssetHandlingClass.sharedInstance().isFromHomeView = false
    
    if let selList = selectedlistItem{
      if(ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen){
        self.selectedGridCell = nil
        self.parentCategoryGridView = nil
        self.parentCategoryListView = nil
        self.parentFavouritesListView = selectedFavouritesListView
        self.parentFavouritesGridView = nil
      }else{
        self.selectedListCell = selList
        self.parentCategoryGridView = nil
        self.selectedGridCell = nil
        self.parentCategoryListView = selectedCategoryListView
      }
      
      ProntoAssetHandlingClass.sharedInstance().selectedListCell = selList
      
    }else if let selGrid = selectedGridCell{
      if(ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen){
        
      }else{
        self.selectedGridCell = selGrid
        self.parentCategoryGridView = selectedCategoryGridView
        self.selectedListCell = nil
        self.parentCategoryListView = nil
        self.parentFavouritesListView = nil
        self.parentFavouritesGridView = nil
      }
      ProntoAssetHandlingClass.sharedInstance().selectedGridCell = selGrid
    }
    
    let assetHandlingViewModel = ProntoAssetHandlingViewModel()
    assetHandlingViewModel.openAsset(selectedAsset: selectedAsset)
    
  }
  
  /*  Mark the asset as read */
  @objc func markAssetAsRead(selectedAsset:Asset) {
    
    if self.selectedListCell != nil{
      
    }else if self.selectedGridCell != nil{
      self.selectedGridCell?.totalViews = selectedAsset.view_count?.int32Value ?? 0
      self.selectedGridCell?.read = true
    }
  }
  
  /* Set required values to selected List or Grid View */
  @objc func setAudioParamsToParent(selectedAsset: Asset,
                                    audioPlayerView: AudioPlayer) {
    ProntoRedesignSingletonClass.sharedInstance.audioAssetView = audioPlayerView
    ProntoRedesignSingletonClass.sharedInstance.audioAsset = selectedAsset
    if (ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen) {
      if (parentFavouritesGridView != nil) {
        parentFavouritesGridView?.assetForAudio = selectedAsset
        parentFavouritesGridView?.audioPlayerView = audioPlayerView
      } else {
        parentFavouritesListView?.assetForAudio = selectedAsset
        parentFavouritesListView?.audioPlayerView = audioPlayerView
      }
    } else {
      if (parentCategoryGridView != nil) {
        parentCategoryGridView?.assetForAudio = selectedAsset
        parentCategoryGridView?.audioPlayerView = audioPlayerView
      } else {
        parentCategoryListView?.assetForAudio = selectedAsset
        parentCategoryListView?.audioPlayerView = audioPlayerView
      }
    }
  }
  
  /// check selected asset is available locally
  /// - Parameters:
  ///   - assetID: selected assetID
  ///   - title: selected asset title
  /// - Returns: returns a boolean if asset is available or not
  func checkAssetIDAvailableLocally(assetID: NSNumber, title: String) -> Bool {
       let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(assetID)
       for data in offlineAssets {
         if let offData = data as? Asset {
           if offData.assetID == assetID && offData.title == title && offData.isDownloaded == true {
             print("ASSETS AVAILABILITY", offData)
             return true
           }
         }
       }
       return false
     }
  
  /// retrieve an asset from db
  /// - Parameter assetID: assetID to be retrieved from db
  /// - Returns: saved path of the selected asset to be retrieved
   func retrieveAsset(assetID: NSNumber) -> String {
     let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(assetID)
     for data in offlineAssets {
       if let offData = data as? Asset {
         if offData.assetID == assetID && offData.isDownloaded == true {
           return offData.savedPath ?? ""
         }
       }
     }
     return ""
   }
}
