//
//  CategoriesViewController.swift
//  Pronto
//
//  Created by Saranya on 12/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
class CategoriesViewController: SearchHistoryViewController {
  var backButtonViewFromParent: UIButton?
  var selectedCategory: Categories? // For Normal Category/Folder Tap
  var previousHeaderName: String?
  var isMoreCategoryView: Bool = false // For MoreCategory View
  var foldersArray: [Categories]?
  var assetsFromDB: [Asset] = [Asset]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if (isMoreCategoryView) {
      loadMoreView()
    } else {
      initialLoad()
    }
    NotificationCenter.default.addObserver(self,
                                           selector:#selector(dismissView),
                                           name: NSNotification.Name(rawValue: RedesignConstants.removeCategoryViewControllerSearchListNotification),
                                           object: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.clearSearchTextNotification), object: self, userInfo: nil)
    if(ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories){
      if(isMoreCategoryView) {
        loadMoreView()
      } else {
        initialLoad()
      }
      
    }
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if(ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories){
      self.viewWillLayoutSubviews()
    }
  }
  /* Setup Category View */
  func initialLoad() {
    //Set Sorting Array
    if let selCategory = selectedCategory {
      HomeViewModel.sharedInstance.setSortingAssetsArray(selectedCategory: selCategory)
    }
    // swiftlint:disable:next force_cast
    let categoryView = Bundle.main.loadNibNamed(RedesignConstants.categoriesNib, owner: self, options: nil)?.first as! CategoryView
    categoryView.parentController = self
    
    categoryView.backButtonViewFromParent = self.backButtonViewFromParent
    categoryView.previousHeaderName = self.previousHeaderName
    categoryView.delegate = self
    categoryView.webLinkDelegate = self
    self.view.addSubview(categoryView)
    if(isMoreCategoryView) { // More Category View
      categoryView.isMoreCategoryView = true
      categoryView.foldersArray = self.foldersArray
    } else { // Normal View on select of any categories
      DispatchQueue.global(qos: .background).async {
        self.initiateOfflineDownload()
      }
      categoryView.selectedCategory = selectedCategory
    }
    categoryView.doAdditionalSetUp()
    /* Add constraints for Categories View */
    categoryView.translatesAutoresizingMaskIntoConstraints = false
    categoryView.pinEdges(to: self.view)
  }
  func loadMoreView() {
    initialLoad()
  }
  /* Fetching offline data */
  func initiateOfflineDownload() {
    let assetsMutableArray:NSMutableArray = []
    
    //Converting Assets set to array
    if let assetsArray = self.selectedCategory?.assets{
      //TODO:Comment below line and add above line after integrating with real data
      for item in assetsArray{
        assetsMutableArray.add(item)
      }
    }
    ProntoAssetHandlingClass.sharedInstance().backgroundDownload(assetsMutableArray)
  }
  /* Search Header View Notification */
  @objc func dismissSearchHeaderView() {
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeCategoryViewSearchListNotification), object: self, userInfo: nil)
    dismissView()
  }
  /// Check selected asset is available locally
  /// - Parameter assetID: selected assetID
  /// - Returns: returns a boolean whether the asset is locally available or not
  func checkAssetIDAvailableLocally(assetID: NSNumber) -> Bool {
    let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(assetID)
    for data in offlineAssets {
      if let offData = data as? Asset {
        if offData.assetID == assetID && offData.isDownloaded == true {
          ZMCDManager.sharedInstance.updateAsset(offData, success: {
            //Update Asset to server
            AbbvieLogging.logInfo(ProntoRedesignAPIConstants.updatingAssetSuccessMsg)
          }) { (error) in
            AbbvieLogging.logError(ProntoRedesignAPIConstants.updatingAssetErrorMsgInLocalDB)
          }
          return true
        }
      }
    }
    return false
  }
  func showSafari(uri: String) {
      if let url = URL(string: uri) {
        if #available(iOS 11.0, *) {
          let config = SFSafariViewController.Configuration()
          config.entersReaderIfAvailable = false
          let vc = SFSafariViewController(url: url, configuration: config)
          vc.dismissButtonStyle = .close
          vc.toolbarItems = []
          present(vc, animated: true)
        } else {
          // Fallback on earlier versions
        }
      }
  }
}
/* Delegate to setup search history view */
extension CategoriesViewController: CategoryProtocol {
  /// add search history view
    /// - Parameters:
    ///   - xAxis: xaxis for search history view
    ///   - yAxis: yaxis for search history view
    ///   - width: width for search history view
    ///   - height: height for search history view
    ///   - mainStackView: superview for search history view
  func addCategorySearchHistory(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int, mainStackView: UIView) {
    addSearchListView(xAxis: xAxis + 6, yAxis: 66, width: width, height: height, mainStackView: mainStackView)
  }
}

extension CategoriesViewController: WeblinkViewProtocol {
  func openWeblink(uri: String) {
    showSafari(uri: uri)
  }
}
