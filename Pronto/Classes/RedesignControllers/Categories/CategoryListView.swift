//
//  CategoryListView.swift
//  Pronto
//
//  Created by Saranya on 18/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
protocol WeblinkListViewProtocol {
  func openWeblinkList(uri: String)
}
class CategoryListView: UIView,
                        UICollectionViewDelegate,
                        UICollectionViewDataSource,
                        UITableViewDelegate,
                        UITableViewDataSource {
    
    var collectionView: UICollectionView!
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    var categoryListContainerView: UIView = UIView()
    var selectedCategory: Categories!
    var parentController: CategoriesViewController?
    var isObjectAvailable = false
    var screenWidth = UIScreen.main.bounds.size
    var audioPlayerView: AudioPlayer?
    var assetForAudio: Asset?
    var tableView: UITableView = UITableView()
    var subfolders: [Categories]? = []
    var assetsArray: [Asset]? = []
    var isMoreCategoryView: Bool = false
    var foldersArray: [Categories]?
    var weblinkListDelegate: WeblinkListViewProtocol?
  
    override init(frame: CGRect) {
        super.init(frame: frame) // calls designated initializer
        
    }
    
    required init?(coder: NSCoder) {
        fatalError(RedesignConstants.commonInitError)
    }
    func setDataForMoreCategories(foldersArray: [Categories],
                                  parentContlr: CategoriesViewController,
                                  objAvailable: Bool) {
        self.parentController = parentContlr
        self.isObjectAvailable = objAvailable
        self.foldersArray = foldersArray
        
        //Converting subfolder set to array
        if self.foldersArray?.count ?? 0 > 0 {
            for item in foldersArray {
                self.subfolders?.append(item)
            }
        }
    }
    func setData(selectedCat: Categories,
                 parentContlr: CategoriesViewController,
                 objAvailable: Bool) {
        self.selectedCategory = selectedCat
        self.parentController = parentContlr
        self.isObjectAvailable = objAvailable
        
        //Converting subfolder set to array
        if let subFoldersArray = self.selectedCategory.subfolders {
            for item in subFoldersArray {
                if item.asset_count?.intValue != 0
                {
                    self.subfolders?.append(item)
                }
            }
        }
        //Converting Assets set to array
        HomeViewModel.sharedInstance.setSortingAssetsArray(selectedCategory: self.selectedCategory)
         if let assetArray = ProntoRedesignSingletonClass.sharedInstance.sortingsasetsArray {
            self.assetsArray = assetArray
            //Bydefault sort by Recently Added
            self.assetsArray?.sort {return $0.publish_on!.compare($1.publish_on!) == .orderedDescending}
        }
    }
    func prepareView() {
        categoryListContainerView.backgroundColor = .gray
        registerForNotification()
        if let subCategory = subfolders {
            if(subCategory.count > 0) {
                loadSubFolders()
            }
            else if assetsArray?.count ?? 0 > 0 {
                 loadCategoriesList()
            }
        }else if assetsArray?.count ?? 0 > 0 {
            loadCategoriesList()
        }
    }
    func loadSubFolders() {
        collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: self.frame, collectionViewLayout: collectionViewFlowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: RedesignConstants.subfolderCollectionCellNib, bundle:nil), forCellWithReuseIdentifier: RedesignConstants.collectionListCellId)
        collectionView.backgroundColor = .white
        collectionView.isScrollEnabled = true
        self.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.leadingAnchor .constraint(equalTo: self.leadingAnchor, constant: 5).isActive = true
        collectionView.trailingAnchor .constraint(equalTo: self.trailingAnchor, constant: -5).isActive = true
        collectionView.topAnchor .constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        collectionView.heightAnchor .constraint(equalToConstant: calculateHeightForCollection()).isActive = true
        decideViewSpacing()
        if (assetsArray?.count ?? 0 > 0) {
            loadCategoriesList()
        }
    }
  /// load categories list
    func loadCategoriesList() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: RedesignConstants.franchiseCellNibName, bundle: nil),
                           forCellReuseIdentifier: RedesignConstants.franchiseCellNibName)
        self.addSubview(tableView)
        tableView.separatorStyle = .none

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor .constraint(equalTo: self.leadingAnchor, constant: 5).isActive = true
        tableView.trailingAnchor .constraint(equalTo: self.trailingAnchor, constant: -5).isActive = true
        if let subCategory = subfolders{
            if(subCategory.count > 0){
                collectionView.bottomAnchor .constraint(equalTo: tableView.topAnchor, constant: 5).isActive = true
            }else{
                tableView.topAnchor .constraint(equalTo: self.topAnchor, constant: 5).isActive = true
            }}else{
            tableView.topAnchor .constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        }
        tableView.bottomAnchor .constraint(equalTo: self.bottomAnchor).isActive = true
    }
  //Register for Notification
    func registerForNotification() {
        NotificationCenter.default.addObserver(self, selector:#selector(handleOrientationChange) , name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadSortingResults(_:)), name: NSNotification.Name(rawValue: RedesignConstants.sortingReloadNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadForFavourites), name: Notification.Name(rawValue: RedesignConstants.favouritesAssetUpdationNotification), object: nil)
    }
    /* Method to reload cells when Favourites is updated in Asset Tool bar */
    @objc func reloadForFavourites() {
        if (isObjectAvailable) {
          tableView.reloadData()
        }
    }
    /* Method to reload view on sorting option */
    @objc func reloadSortingResults(_ notification: NSNotification) {
        let notificationDict = notification.userInfo
        self.assetsArray = notificationDict?[RedesignConstants.sortingTermKey] as? [Asset]
        ProntoRedesignSingletonClass.sharedInstance.sortingsasetsArray = self.assetsArray
        tableView.reloadData()
    }
    /* Method called to handle Orientation Change Notification */
    @objc func handleOrientationChange() {
       if (isObjectAvailable) {
        if (collectionViewFlowLayout != nil) {
            decideViewSpacing()
            collectionView.layoutIfNeeded()
            collectionView.setNeedsLayout()
        }
      }
    }
    /* UICollectionView DataSource & Delegate Methods */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return calculateNoOfItems()
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //create folders
        
        let cell: SubFolderListCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:RedesignConstants.collectionListCellId,
                                                                                  for: indexPath) as! SubFolderListCollectionCell
        cell.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        let category:Categories = (subfolders?[indexPath.row])!
        cell.titleLabel.text =  category.name
        if (category.asset_count!.intValue <= 1)
        {
            cell.totalAssetLabel.text = String(category.asset_count!.intValue) + RedesignConstants.assetTxt
        }
        else
        {
            cell.totalAssetLabel.text = String(category.asset_count!.intValue) + RedesignConstants.assetsTxt
        }
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let category:Categories? = self.subfolders?[indexPath.row]
        if let categoryObj = category {
            if (isMoreCategoryView) {
              HomeViewModel.sharedInstance.getAssetDetails(categoryObj: categoryObj,
                                                           index: (ProntoRedesignSingletonClass.sharedInstance.promotedContentsArray?.count ?? 0) > 0 ?  9 +                                                  indexPath.row : 19 + indexPath.row,
                                                           previousHeaderStringName: parentController?.previousHeaderName ?? "",
                                                           parentView: self,
                                                           nav: (parentController?.navigationController)!)
            } else {
                if (categoryObj.assets?.count ?? 0 > 0 || categoryObj.subfolders?.count ?? 0 > 0) {
                    let categoryViewController = CategoriesViewController()
                    categoryViewController.backButtonViewFromParent = parentController?.backButtonViewFromParent?.duplicate()
                    categoryViewController.selectedCategory = categoryObj
                    categoryViewController.previousHeaderName = parentController?.previousHeaderName
                    ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = false
                    parentController?.navigationController?.pushViewController(categoryViewController, animated: true)
                }
            }
        }
    }
    /* Method to calculate the number of items to be displayed in collection View */
    func calculateNoOfItems()->Int {
        var count = 0
        
        if let subFolder = subfolders {
            count += subFolder.count
        }
    return count
    }
    /* Method to calculate height of collection View */
    func calculateHeightForCollection()-> CGFloat {
        var height = calculateNoOfItems() / 3
        if ((calculateNoOfItems() % 3)>0) {
            height += 1
        }
        if height == 1 {
            return 100
        } else if height == 0 {
            return 0
        } else {
            return CGFloat((height * 75) + 20)
        }
    }
    func decideViewSpacing() {
          let numberOfItemsPerRow:CGFloat = 3
          let spacingBetweenCells:CGFloat = 5
                
          let totalSpacing = (2 * 5) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
          var screenSize:CGFloat = screenWidth.width
          if((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft) || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)) {
              screenSize = max(screenWidth.width, screenWidth.height)
          } else {
              screenSize = min(screenWidth.width, screenWidth.height)
          }
          var size :CGSize = CGSize(width: 0, height: 0)
          let width = (screenSize - totalSpacing)/numberOfItemsPerRow
          size =  CGSize(width: width-5, height: 75)
              
          collectionViewFlowLayout.itemSize = CGSize(width: size.width , height: 75)
          collectionViewFlowLayout.minimumInteritemSpacing = 0
      }
    /* Tableview Delegates and Datasource */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let subFolder = self.assetsArray {
               return subFolder.count
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
        var cell: FranchiseTableViewCell!
        if let cell1 = tableView.dequeueReusableCell(withIdentifier: RedesignConstants.franchiseCellNibName) as? FranchiseTableViewCell {
            cell = cell1
        } else {
            let nib = Bundle.main.loadNibNamed(RedesignConstants.franchiseCellNibName, owner: self, options: nil)
            tableView.register(UINib(nibName: RedesignConstants.franchiseCellNibName, bundle: nil),
                               forCellReuseIdentifier: RedesignConstants.franchiseCellNibName)
                cell = nib?[0] as? FranchiseTableViewCell
        }
        cell.parentFolderView.isHidden = true
        //Hide loader background
        cell.loaderBackgroundView.backgroundColor = UIColor.clear
        cell.loaderBackgroundView.alpha = 1
        cell.loaderBackgroundView.isHidden = true
        cell.audioPlayerView.isHidden = true
        cell.loaderView.isHidden = true
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        if let assetObj = assetsArray {
        if assetObj.count > 0 && indexPath.row < assetObj.count {
                        
            let asset:Asset! = assetObj[indexPath.row]
            cell.labelAssetName.text = asset.title
            let assetType = Constant.getFileType(for: asset)
            var imageName:String = RedesignConstants.pdfName
            if (assetType == ZMProntoFileTypesPDF) {
                imageName = RedesignConstants.pdfName
            } else if (assetType == ZMProntoFileTypesVideo) {
                imageName = RedesignConstants.videoName
            } else if (assetType == ZMProntoFileTypesBrightcove) {
                imageName = RedesignConstants.videoName
            } else if (assetType == ZMProntoFileTypesDocument) {
                imageName = RedesignConstants.pdfName
            } else if(assetType == ZMProntoFileTypesWeblink) {
                imageName = RedesignConstants.weblinkName
            } else if (assetType == ZMProntoFileTypesAudio) {
                imageName = RedesignConstants.centerImageAudio
            }
            
            cell.assetImageType.image = UIImage.init(named: imageName)
            cell.labelAssetType.text = asset.field_description?.replacingOccurrences(of: "&amp;", with: "&")
            cell.isNew = asset.is_new
            if let changetime = asset.changed , let createtime = asset.created
            {
                let myTimeInterval = TimeInterval(changetime.doubleValue)
                let mychgdate = Date(timeIntervalSince1970: myTimeInterval)
                let mycreatInterval = TimeInterval(createtime.doubleValue)
                let mycreatedate = Date(timeIntervalSince1970: mycreatInterval)
                let calendar = Calendar.current
                let numberOfDays = calendar.dateComponents([.day], from: mychgdate, to: Date())
                let order = Calendar.current.compare(mychgdate, to: mycreatedate, toGranularity: .day)
                if (order == .orderedSame && numberOfDays.day! + 1 <= 30)
                {
                  if asset.is_new {
                    cell.updateAvailableButton.isHidden = true
                  } else {
                    cell.updateAvailableButton.isHidden = false
                  }
                }
               else if (order == .orderedDescending && numberOfDays.day! + 1 <= 30)
                {
//                    cell.isUpdateAvailable = true
                    cell.updateAvailableButton.isHidden = false
                }
                else{
//                      cell.isUpdateAvailable = false
                      cell.updateAvailableButton.isHidden = true
                    }
                }
                
                   
            if let assetId = asset.assetID{
                cell.uid = assetId.intValue
            }
        cell.shareButton.addTarget(self, action: #selector(exportIconClicked(_:)), for: .touchUpInside)
        cell.favButton.addTarget(self, action: #selector(favIconClicked(_:)), for: .touchUpInside)
        cell.infoButton.addTarget(self, action: #selector(infoIconClicked(_:)), for: .touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.favButton.tag = indexPath.row
        cell.infoButton.tag = indexPath.row
        
        let favfranchise = ZMCDManager.sharedInstance.getFavFranchise()
        cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
        cell.favButton.isSelected = false
        for fav in favfranchise {
          guard let fav = fav as? FavFranchise else {
            continue
          }
          if cell.uid == fav.asset_ID?.intValue {
            cell.favButton.setImage(UIImage(named: RedesignConstants.favoSelected), for: .selected)
            cell.favButton.isSelected = true
            break
          } else {
            cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
            cell.favButton.isSelected = false
          }
        }
        if let audioAsset = ProntoRedesignSingletonClass.sharedInstance.audioAsset{
          assetForAudio = audioAsset
        }
        if asset.assetID == assetForAudio?.assetID{
          audioPlayerView?.removeFromSuperview()
          if let audioView = ProntoRedesignSingletonClass.sharedInstance.audioAssetView{
            audioPlayerView = audioView
          }
          cell.audioPlayerView.isHidden = false
          if(audioPlayerView != nil){
            //                audioPlayerView?.frame = cell.audioPlayerView.frame
            cell.audioPlayerView.addSubview(audioPlayerView!)
            audioPlayerView?.frame = CGRect(x: 0, y: 0,width: cell.audioPlayerView.frame.size.width, height: cell.audioPlayerView.frame.size.height)
          }
        }
      }
      
    }
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
    if let cell = tableView .cellForRow(at: indexPath) as? FranchiseTableViewCell{
      let indexVar = indexPath.row
      let asset:Asset! = (self.assetsArray?[indexVar])!
      let assetType = Constant.getFileType(for: asset!)
      if assetType == ZMProntoFileTypesWeblink {
        self.weblinkListDelegate?.openWeblinkList(uri: asset.uri_full ?? "")
      } else {
        openAsset(selectedAsset: asset, selectedGridCell: nil, selectedlistItem: cell)
      }
    }
  }
  /* Handling asset options - Share */
  @objc func exportIconClicked(_ sender: UIButton) {
    if sender.isSelected {
      sender.setImage(UIImage(named: RedesignConstants.export_redesign), for: .normal)
      sender.isSelected = false
    } else {
      sender.setImage(UIImage(named: RedesignConstants.export_redesign_selected), for: .selected)
      sender.isSelected = true
      
      CategoryViewModel.sharedInstance.handleExportPopOver(sender: sender,
                                                           parentCategoryGrid: nil,
                                                           parentCategoryList: self,
                                                           parentFavouritesGrid: nil,
                                                           parentFavouritesList: nil,
                                                           selectedAsset: self.assetsArray![sender.tag])
    }
  }
  /* Handling asset options - Info */
  @IBAction func infoIconClicked(_ sender: UIButton) {
    // Pronto new revamp changes: setting selected/deselected info button's image
    if sender.isSelected {
      sender.setImage(UIImage(named: RedesignConstants.info_redesign), for: .normal)
      sender.isSelected = false
    } else {
      sender.setImage(UIImage(named:  RedesignConstants.info_redesign_selected), for: .selected)
      sender.isSelected = true
      
      CategoryViewModel.sharedInstance.configureInfoPopOver(sender,
                                                            parentCategoryGrid: nil,
                                                            parentCategoryList: self,
                                                            parentFavouritesGrid: nil,
                                                            parentFavouritesList: nil,
                                                            selectedAsset: self.assetsArray![sender.tag])
    }
    //End
  }
  /* Handle Opening of Asset */
  func openAsset(selectedAsset: Asset,
                 selectedGridCell: ZMGridItem?,
                 selectedlistItem: FranchiseTableViewCell?){
    CategoryViewModel.sharedInstance.openAsset(selectedAsset: selectedAsset,
                                               selectedGridCell: nil,
                                               selectedlistItem: selectedlistItem,
                                               selectedCategoryListView: self,
                                               selectedCategoryGridView: nil,
                                               selectedFavouritesListView: nil,
                                               selectedFavouritesGridView: nil)
  }
  /* Handle favo icon tapped Action */
  @IBAction func favIconClicked(_ sender: UIButton) {
    if sender.isSelected {
      sender.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .normal)
      sender.isSelected = false
      
      MyFavouritesViewModel.sharedInstance.deleteAssetFromFavourites(asset: self.assetsArray![sender.tag])
      
    } else {
      sender.setImage(UIImage(named: RedesignConstants.favoSelected), for: .selected)
      sender.isSelected = true
      
      MyFavouritesViewModel.sharedInstance.createFavObject(selectedAsset: self.assetsArray![sender.tag])
    }
  }
}
