//
//  SubFolderListCollectionCell.swift
//  Pronto
//
//  Created by Saranya on 18/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class SubFolderListCollectionCell: UICollectionViewCell{
  @IBOutlet weak var titleLabel: UILabel!
  
  @IBOutlet weak var totalAssetLabel: UILabel!
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    contentView.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      contentView.leftAnchor.constraint(equalTo: leftAnchor),
      contentView.rightAnchor.constraint(equalTo: rightAnchor),
      contentView.topAnchor.constraint(equalTo: topAnchor),
      contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
  }
}
