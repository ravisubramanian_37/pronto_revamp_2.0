//
//  CategoryHeaderView.swift
//  Pronto
//
//  Created by Saranya on 13/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class CategoryHeaderView: UIView {
  @IBOutlet weak var sortingOverallView: UIView!
  @IBOutlet weak var backButtonView: UIView!
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var sortByAssetLabel: UILabel!
  @IBOutlet weak var listGridButton: UIButton!
  var sortingAssetsArray: [Asset]? = []
  var parentViewTitle: String?
  var parentController: CategoriesViewController?
  var parentControllerForFavourites: MyFavouritesDetailsViewController?
  var subfolderNumber: Int = 1
  var selectedCategory: Categories?
  let sortingViewController: UIViewController = UIViewController()
  let royalBlueColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)
  var isMoreCategoryView: Bool = false
  var isFromFavourites: Bool = false
  var myFavouritesBackView: MyFavoSubheaderBackButton?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  /* Setup View */
  func doAdditionalSetUp(){
    backButton.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
    if (isFromFavourites) {
      loadSubHeaderForFavourites()
    } else {
      loadSubHeaderForCategories()
    }
    registerForNotifications()
  }
  /* Register Notifications */
  func registerForNotifications(){
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(updateFavouritesCountInHeaderBackButton),
                                           name: NSNotification.Name(rawValue: RedesignConstants.favouritesRemovalNotification),
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(reloadSortingResults(_:)),
                                           name: NSNotification.Name(rawValue: RedesignConstants.sortingReloadNotification),
                                           object: nil)
  }
  
  /* Method to load sub header categories View */
  func loadSubHeaderForCategories(){
    if (isMoreCategoryView) {
      titleLabel.text = RedesignConstants.subFolderTitleString
      subfolderNumber = 0
    } else {
      titleLabel.text = selectedCategory?.name
      subfolderNumber = selectedCategory?.parent?.intValue ?? 0
      if (subfolderNumber == 0 && parentController?.navigationController?.viewControllers.count == 4) {
        subfolderNumber = 2 // For Handling More Category > Category > Subfolders Scenario
      }
    }
    parentController?.previousHeaderName = titleLabel.text
    
    if (selectedCategory?.subfolders?.count ?? 0) > 0 && (selectedCategory?.assets?.count ?? 0) == 0 {
      sortingOverallView.isHidden = true
    } else {
      sortingOverallView.isHidden = false
    }
    titleLabel.textColor = royalBlueColor
    if (isMoreCategoryView) {
      sortByAssetLabel.textColor = UIColor.lightGray
    } else {
      sortByAssetLabel.textColor = royalBlueColor
    }
    checkForListOrGrid()
  }
  /* Method to configure List or Grid View */
  @IBAction func listOrGridTapped(_ sender: Any) {
    let value:Bool = ProntoRedesignSingletonClass.sharedInstance.isListChosen
    if (value) {
      ProntoRedesignSingletonClass.sharedInstance.isListChosen = false
    } else {
      ProntoRedesignSingletonClass.sharedInstance.isListChosen = true
    }
    checkForListOrGrid()
    sortByAssetLabel.text = RedesignConstants.SortingOptions.recentlyAdded.rawValue
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.notificationforListAndGridChange), object: self, userInfo: nil)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewSearchListNotification), object: self, userInfo: nil)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeCategoryViewSearchListNotification), object: self, userInfo: nil)
  }
  /* Method to check whether List or Grid View */
  func checkForListOrGrid() {
    if(ProntoRedesignSingletonClass.sharedInstance.isListChosen){
      listGridButton.setImage(UIImage(named: RedesignConstants.gridImage), for: .normal)
    } else {
      listGridButton.setImage(UIImage(named: RedesignConstants.listImage), for: .normal)
    }
  }
  /* Method to configure back button */
  @objc func backTapped(sender: UIButton!) {
    ProntoRedesignSingletonClass.sharedInstance.isPopDoneInCategories = true
    parentController?.navigationController?.popViewController(animated: true)
  }
  /* Method to configure sorting button */
  @IBAction func sortingButtonTapped(_ sender: Any) {
    if (!isMoreCategoryView && ProntoRedesignSingletonClass.sharedInstance.sortingEnabled) {
      let sortingModel = SortingViewModel()
      sortingModel.showSortingPopOver(sender: sender)
    }
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewSearchListNotification), object: self, userInfo: nil)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeCategoryViewSearchListNotification), object: self, userInfo: nil)    }
  /* Method to reload view on sorting option */
  @objc func reloadSortingResults(_ notification: NSNotification){
    sortByAssetLabel.text = (UserDefaults.standard.value(forKey: RedesignConstants.sortingTermKey) as! String)
  }
  
  /* Method to retrieve cell text */
  func getTextForIndexpath(index:IndexPath)-> String{
    var labelText:String = ""
    switch index.row {
    case 0:
      labelText = RedesignConstants.SortingOptions.recentlyAdded.rawValue
    case 1:
      labelText = RedesignConstants.SortingOptions.mostViewed.rawValue
    default:
      labelText = RedesignConstants.SortingOptions.recentlyUpdated.rawValue
    }
    return labelText
  }
  
  /* Methods to handle header for My Favourites */
  func loadSubHeaderForFavourites(){
    titleLabel.text = ""
    checkForListOrGrid()
    configureBackButtonForMyFavourites()
    sortingOverallView.isHidden = true
  }
  /* Method to configure back button for my favourites */
  func configureBackButtonForMyFavourites(){
    myFavouritesBackView = (Bundle.main.loadNibNamed(RedesignConstants.myFavouritesBackButtonNibName, owner: self, options: nil)?.first as? MyFavoSubheaderBackButton)!
    myFavouritesBackView?.addMoreDetailsToView()
    myFavouritesBackView?.parentControllerForFavourites = self.parentControllerForFavourites
    self.addSubview(myFavouritesBackView!)
  }
  /* Method called to handle Notifiaction */
  @objc func updateFavouritesCountInHeaderBackButton() {
    myFavouritesBackView?.removeFromSuperview()
    configureBackButtonForMyFavourites()
  }
}
