//
//  PromotedContentsViewModel.swift
//  Pronto
//
//  Created by Saranya on 04/08/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class PromotedContentsViewModel: NSObject {
    @objc static let sharedInstance = PromotedContentsViewModel()
    @objc var audioView: AudioPlayer?
    var prontoAssetHandlingClassObject: ProntoAssetHandlingClass?
  /// /* Handle Opening of Asset */
  /// - Parameters:
  ///   - selectedAsset: selected Asset
  ///   - audioView: audio view for the selectedAsset
    func openAsset(selectedAsset:Asset, audioView:UIView?) {
      ProntoAssetHandlingClass.sharedInstance().resetAssetValue()
      var trackingOptions: NSMutableDictionary = [:]
      
      if (ZMUserActions.sharedInstance().getFiltersForTracking() != nil) {
        trackingOptions.setValue(ZMUserActions.sharedInstance().getFiltersForTracking(), forKey: RedesignConstants.scfiltersubcategoryKey)
      }
      
      trackingOptions[RedesignConstants.scfilteractionKey] = RedesignConstants.clickThroughKey
      trackingOptions[RedesignConstants.sctierlevelKey] = RedesignConstants.tier2Key
      
      if selectedAsset.title!.count > 0 {
        trackingOptions[RedesignConstants.scsearchclickthroughfilenameKey] = selectedAsset.title
      }
      ZMTracking.trackSection(
        ZMUserActions.sharedInstance().section,
        withSubsection: RedesignConstants.filterKey,
        withName: "brand-\(String(describing: ZMUserActions.sharedInstance().franchiseName))-default",
        withOptions: trackingOptions )
      
      trackingOptions = [:]
      trackingOptions[RedesignConstants.sccollateralfeaturesKey] = ""
      
      if selectedAsset.title!.count > 0 {
        trackingOptions[RedesignConstants.sccollateralnameKey] = selectedAsset.title
      }
      
      ZMTracking.trackSection(
        ZMUserActions.sharedInstance().section,
        withSubsection: RedesignConstants.assetViewKey,
        withName: "\(String(describing: ZMAssetViewController.getAssetType(selectedAsset)))|\(String(describing: selectedAsset.title))",
        withOptions: trackingOptions)
      if selectedAsset.title!.count > 0 {
        prontoAssetHandlingClassObject = ProntoAssetHandlingClass()
        prontoAssetHandlingClassObject?.isFromHomePromotedContent = true
        if(audioView != nil){
          prontoAssetHandlingClassObject?.homePromotedContentAudioView = audioView!
        }
        prontoAssetHandlingClassObject?.validate(selectedAsset)
      }
    }
}
