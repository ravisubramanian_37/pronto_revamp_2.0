//
//  PromotedContentHomeView.swift
//  Pronto
//
//  Created by Saranya on 06/08/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class PromotedContentHomeView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var centerImageView: UIImageView!
    @IBOutlet weak var thumbnailImageview: UIImageView!
    
    @IBOutlet weak var audioview: UIView!
    @IBAction func closeButtonAction(_ sender: Any) {
        audioview.isHidden = true
    }
}
