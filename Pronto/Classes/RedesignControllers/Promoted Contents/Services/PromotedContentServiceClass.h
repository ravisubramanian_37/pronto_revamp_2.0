//
//  PromotedContentServiceClass.h
//  Pronto
//
//  Created by Saranya on 04/08/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoRedesignAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface PromotedContentServiceClass : ProntoRedesignAPIManager
+ (PromotedContentServiceClass *)sharedAPIManager;
- (void)fetchPromotedHomeContent:(NSArray *)assetArray success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError;
@end

NS_ASSUME_NONNULL_END
