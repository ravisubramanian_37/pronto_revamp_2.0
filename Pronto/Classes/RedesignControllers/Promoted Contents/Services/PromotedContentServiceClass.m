//
//  PromotedContentServiceClass.m
//  Pronto
//
//  Created by Saranya on 04/08/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "PromotedContentServiceClass.h"
#import "Pronto-Swift.h"

@implementation PromotedContentServiceClass
/* Singleton Object Creation */
+ (PromotedContentServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static PromotedContentServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
- (void)fetchPromotedHomeContent:(NSArray *)assetArray success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{
     
  
    //TODO: Uncomment and make services with real time environment
    
            if ([AOFLoginManager sharedInstance].upi) {
                
                [super performBlock:^(CallbackBlock  _Nonnull callback) {
                    
                    NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.getPromotedContentURL];
                    url = [url stringByAppendingString:AOFLoginManager.sharedInstance.baseFranchiseID];

                    
                    NSDictionary *params = @{@"token":[AOFLoginManager sharedInstance].getAccessToken};

                    AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:[NSDictionary new]];
                    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {

                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            if (callback) {
                                callback(nil, responseObject);
                            }
                        });

                        ;
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

                        [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchPromotedContentErrorResponseMsg, error]];
                        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchPromotedContentErrorResponseMsg responseObj:error];
                    }];
                    
                } retryingNumberOfTimes:3 onCompletion:^(NSError * _Nonnull error, NSObject * _Nonnull response) {
                    
                    
                    if (error) {
                        [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchPromotedContentErrorResponseMsg, error.localizedDescription]];
                        if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
                        {
                            onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                        }
                        else
                        {
                            onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                        }
                        
                    }else
                    {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchPromotedContentSuccessResponseMsg ,response]];
                        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchPromotedContentSuccessResponseMsg responseObj:response];
                        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {

                            NSArray *array = (NSArray *)response;
                            
                            // Deleting Assets other than one coming from Response
                            NSFetchRequest * fetchrequestFranchise = [PromotedContents MR_requestAll];
                            NSArray *importedPromotedContents = [PromotedContents MR_executeFetchRequest:fetchrequestFranchise];
                            
                            for (PromotedContents *fav in importedPromotedContents) {
                                NSPredicate *favFilterRemove = [NSPredicate predicateWithFormat:@"id == %@", fav.id];
                                NSUInteger index = [array indexOfObjectPassingTest:
                                             ^(id obj, NSUInteger idx, BOOL *stop) {
                                               return [favFilterRemove evaluateWithObject:obj];
                                             }];

                                  if (index == NSNotFound) {
                                      [fav MR_deleteEntityInContext:localContext];
                                  }
                                  
                            }
                            
                //             Updating Promoted Contents and priorities
                            for (int count=0;count < array.count;count++) {
                                
                                NSMutableDictionary * promotedContentObject = [[NSMutableDictionary alloc]init];
                                promotedContentObject = array[count];
                                
                                
                                NSPredicate *favFilter = [NSPredicate predicateWithFormat:@"id == %@", promotedContentObject[@"id"]];
                                
                                NSArray * promotedContent = [PromotedContents MR_findAllWithPredicate:favFilter inContext:localContext];
                                if(promotedContent.count == 0){
                                   
                                    PromotedContents * favFranchise = [PromotedContents MR_createEntityInContext:localContext];
                                    [favFranchise MR_importValuesForKeysWithObject:promotedContentObject];
                                    
                                    favFranchise.asset.assetID = [NSNumber numberWithInt:[[[promotedContentObject valueForKey:@"asset"] valueForKey:@"assetID"] intValue]];
                                    
                                }else{
                                    PromotedContents * promotedContentObj = promotedContent[0];
                                    promotedContentObj.id = promotedContentObject[@"id"];
                                    promotedContentObj.priority = promotedContentObject[@"priority"];
                                    
                                }
                            }

                        } completion:^(BOOL contextDidSave, NSError *error) {

                            if (error) {
                            }else{
                                NSFetchRequest * fetchrequestFranchise = [PromotedContents MR_requestAll];
                                NSArray *importedFavourites = [PromotedContents MR_executeFetchRequest:fetchrequestFranchise];
                                NSSortDescriptor *sortDescriptor;
                                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"priority"
                                                                               ascending:YES];
                                NSArray *sortedArray = [importedFavourites sortedArrayUsingDescriptors:@[sortDescriptor]];
                                success(sortedArray);
                            }
                        }];
                    }
                }];
            }
            
                            
}
@end
