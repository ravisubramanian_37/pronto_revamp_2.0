//
//  RedesignThemesClass.swift
//  Pronto
//
//  Created by Saranya on 12/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class RedesignThemesClass: NSObject {
    @objc var defaultThemeforUser: UIColor = UIColor.blue
    let blueBackgroundColorForEvents = UIColor.init(red: 0/255, green: 130/255, blue: 186/255, alpha: 1)
    @objc let royalBlueCode = "#071D49"
    let lightGrayForAssetBackground = "#e0e0e0"
    @objc static let sharedInstance : RedesignThemesClass = RedesignThemesClass()
    
    func setThemes(color: String){
        defaultThemeforUser = hexStringToUIColor(hex: color)
    }
    
    func setThemesForView(view:inout UIView, applyColor: UIColor)  {
        view.backgroundColor = applyColor
    }
    
    func setFontAndSize(forElement:inout UILabel, fontName:UIFont, fontSize: CGFloat) {
        forElement.font = UIFont(name:fontName.fontName, size: fontSize)
    }
    
    func applyFranchiseTheme(forView:inout UIView) {
        forView.backgroundColor = defaultThemeforUser
    }
    
    //Function to return UIColor from HEX value
    @objc func hexStringToUIColor (hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue: UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
