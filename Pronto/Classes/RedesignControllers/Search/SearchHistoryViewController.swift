//
//  SearchHistoryViewController.swift
//  Pronto
//
//  Created by Ravi Subramanian on 13/10/21.
//  Copyright © 2021 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class SearchHistoryViewController: UIViewController {
  @objc static let instance = SearchHistoryViewController()
  var searchListView = UIView()
  var searchTableView = UITableView()
  var width: CGFloat = 0
  var height: CGFloat = 0
 
  override func viewDidLoad() {

  }
  
  /// add search history view
  /// - Parameters:
  ///   - xAxis: xaxis for search history view
  ///   - yAxis: yaxis for search history view
  ///   - width: width for search history view
  ///   - height: height for search history view
  ///   - mainStackView: superview for search history view
  @objc func addSearchListView(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int, mainStackView: UIView) {
    searchListView.frame = CGRect(x: xAxis, y: yAxis,
                            width: width, height: CGFloat(height) + 30)
    searchListView.backgroundColor = .clear
    searchListView.layer.cornerRadius = 12
    searchTableView.frame = CGRect(x: 12, y: 0, width: width, height: CGFloat(height) + 30)
    searchTableView.backgroundColor = .white
    if #available(iOS 15.0, *) {
      searchTableView.sectionHeaderTopPadding = 2
    } else {
      // Fallback on earlier versions
    }
    searchTableView.separatorColor = .lightGray
    searchTableView.layer.cornerRadius = 12
    searchTableView.layer.borderWidth = 1
    searchTableView.layer.borderColor = UIColor.lightGray.cgColor
    searchTableView.dataSource = self
    searchTableView.delegate = self
    searchListView.addSubview(searchTableView)
    mainStackView.addSubview(searchListView)
  
    searchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    searchTableView.allowsSelection = true
    searchTableView.isScrollEnabled = true
    searchTableView.isUserInteractionEnabled = true
    searchListView.isUserInteractionEnabled = true
    mainStackView.isUserInteractionEnabled = true
    
    searchTableView.reloadData()
  }
  
  @objc func dismissView() {
    self.searchListView.removeFromSuperview()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    dismissView()
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewSearchListNotification), object: self, userInfo: nil)
  }
  
  /* Method called to handle Notification */
  @objc func handleOrientationChange() {
    self.viewWillLayoutSubviews()
  }
}

extension SearchHistoryViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let searchList = UserDefaults.standard.array(forKey: "keywordTxt") {
      return searchList.count
    }
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? UITableViewCell
    if let searchList = UserDefaults.standard.array(forKey: "keywordTxt"), searchList.count > 0 {
      cell?.textLabel?.text = searchList[indexPath.row] as? String
    } else {
      cell?.textLabel?.text = "No Recent Searches."
    }
    return cell ?? UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let searchList = UserDefaults.standard.array(forKey: "keywordTxt"), searchList.count > 0 {
      HomeMainHeaderView.sharedInstance.searchBarHomeScreen.text = searchList[indexPath.row] as? String
      HomeMainHeaderView.sharedInstance.searchBarSearchButtonClicked(HomeMainHeaderView.sharedInstance.searchBarHomeScreen)
    }
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return "Recent Searches"
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 24
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 36
  }
}
