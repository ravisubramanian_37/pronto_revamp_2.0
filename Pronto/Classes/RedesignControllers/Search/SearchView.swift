//
//  SearchView.swift
//  Pronto
//
//  Created by Saranya on 08/10/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit

protocol SearchProtocol {
  func addSearchResultsHistory(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int, mainStackView: UIView)
}

protocol SearchWeblinkViewProtocol {
  func openWebLink(uri: String)
}

class SearchView: UIView {
    @IBOutlet weak var mainHeaderView: UIView!
    @IBOutlet weak var sortByLabel: UILabel!
    @IBOutlet weak var noOfAssetsLabel: UILabel!
    @IBOutlet weak var searchResultMainHeaderLabel: UILabel!
    @IBOutlet weak var sortingView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sortingButton: UIButton!
    
    var searchListViewObject:SearchListView?
    var sortingAssetsArray: [Asset]?
    var parent: SearchResultsViewController?
    var searchDelegate: SearchProtocol?
    var searchText = ""
    var webLinkDelegate: SearchWeblinkViewProtocol?
      
    let royalBlueColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setData(assetArray:[Asset]?){
        self.sortingAssetsArray = assetArray
        configureView()
    }
    @IBAction func sortingButtonTapped(_ sender: Any) {
        let sortingModel:SortingViewModel = SortingViewModel()
        sortingModel.isFromSearch = true
        sortingModel.showSortingPopOver(sender: sender)
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        parent?.goBackToHome()
    }
    func configureView(){
        //Check for zero search results / not case
        checkSearchResults()
        //Set up themes
        setUpThemes()
        //Set up main header
        setUpMainHeader()
        //Register for notifications
        registerForNotifications()
    }
    func setUpThemes(){
        self.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        searchResultMainHeaderLabel.textColor = royalBlueColor

        noOfAssetsLabel.textColor = royalBlueColor
        sortingView.layer.borderWidth = 0.5
        sortingView.layer.borderColor = UIColor.darkGray.cgColor
    }
    /* Set up main header */
    func setUpMainHeader(){
        let headerView:HomeMainHeaderView = Bundle.main.loadNibNamed(RedesignConstants.mainViewHeaderNib,
                                                                     owner: self, options: nil)?.first as! HomeMainHeaderView
        headerView.searchResultsDelegate = self
        headerView.searchText = searchText
        headerView.flowType = RedesignConstants.searchFlow
        ProntoRedesignSingletonClass.sharedInstance.lastFlowType = headerView.flowType
        headerView.searchBarHomeScreen.text = searchText
        mainHeaderView.addSubview(headerView)
        headerView.setThemes()
        /* Add constraints for Categories View */
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.pinEdges(to: mainHeaderView)
    }
    /* Register For Notifications */
    func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(reloadSortingResults(_:)), name: NSNotification.Name(rawValue: RedesignConstants.searchSortingReloadNotification), object: nil)
    }
    /* Method to reload view on sorting option */
    @objc func reloadSortingResults(_ notification: NSNotification){
        let notificationDict = notification.userInfo
        sortByLabel.text = (UserDefaults.standard.value(forKey: RedesignConstants.searchSortingTermKey) as? String ?? "Recently Added")
        self.sortingAssetsArray = notificationDict?[RedesignConstants.searchSortingTermKey] as? [Asset]
        checkSearchResults()
    }
    //Check if need empty View or TableView
    func checkSearchResults(){
        if(self.sortingAssetsArray?.count ?? 0 > 0){
            //Search results count >0  case
            noOfAssetsLabel.text = String("\(self.sortingAssetsArray!.count)\(RedesignConstants.searchResultsCountText)")
            setUpTable()
        }else{
            //Zero search results case
            noOfAssetsLabel.text = String("0\(RedesignConstants.zeroSearchResultsCountText)")
            setUpZeroSearcResultView()
        }
    }
    //Set Up view for Zero Asset case
    func setUpZeroSearcResultView(){
        sortingButton.isEnabled = false
        sortByLabel.textColor = UIColor.lightGray
        
        let emptyView = RedesignUtilityClass.sharedInstance.createEmptyViewWithText(text: RedesignConstants.zeroSearchResultText, withFontSize: 16)
        self.mainView.addSubview(emptyView)
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        emptyView.pinEdges(to: self.mainView)
        
        emptyView.layer.borderWidth = 0.5
        emptyView.layer.borderColor = UIColor.lightGray.cgColor
        
        
    }
    //Set Up Table
    func setUpTable(){
        sortByLabel.textColor = royalBlueColor

        let searchListViewObj:SearchListView = SearchListView()
        searchListViewObj.weblinkListDelegate = self
        searchListViewObj.sortingAssetsArray = self.sortingAssetsArray
        searchListViewObj.prepareView()
        
        self.addSubview(searchListViewObj)
        searchListViewObject = searchListViewObj

        searchListViewObj.translatesAutoresizingMaskIntoConstraints = false
        self.mainView.pinEdges(to: searchListViewObj)
    }
}

extension SearchView: SearchResultsHistoryProtocol {
  /// setup search history view
  /// - Parameters:
  ///   - xAxis: x axis value for search history view
  ///   - yAxis: y axis for search history view
  ///   - width: width for search history view
  ///   - height: height for search history view
  func addSearchListView(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int) {
    self.searchDelegate?.addSearchResultsHistory(xAxis: xAxis, yAxis: yAxis, width: width, height: height, mainStackView: self)
  }
}

extension SearchView: SearchListViewProtocol {
  func openWeblinkList(uri: String) {
    self.webLinkDelegate?.openWebLink(uri: uri)
  }
}
