//
//  SearchServiceClass.h
//  Pronto
//
//  Created by Saranya on 12/10/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoRedesignAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchServiceClass : ProntoRedesignAPIManager
+ (SearchServiceClass *)sharedAPIManager;
- (void)performSearch:(NSString *)searchText completionBlock:(void(^)(NSArray *searchAssets))complete error:(void(^)(NSError *error))onError;
@end

NS_ASSUME_NONNULL_END
