//
//  SearchServiceClass.m
//  Pronto
//
//  Created by Saranya on 12/10/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "SearchServiceClass.h"
#import "ProntoRedesignServerUtilityClass.h"
#import "Pronto-Swift.h"

@implementation SearchServiceClass
/* Singleton Object Creation */
+ (SearchServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static SearchServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
- (void)performSearch:(NSString *)searchText completionBlock:(void(^)(NSArray *searchAssets))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi)
    {

        NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};

              
        NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.searchAssetsURL];
        url = [url stringByAppendingString:searchText];
        NSLog(@"Search Text %@", url);
              
        [super performBlock:^(CallbackBlock  _Nonnull callback) {
                          
            AFHTTPRequestOperationManager *manager = [super requestManagerWithToken:NO headers:[NSDictionary new]];
            
            [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [[RedesignUtilityClass sharedInstance] removeLoadingIndicatorFromTopController];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.searchSuccessResponseMsg ,responseObject]];
                        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.searchSuccessResponseMsg responseObj:responseObject];
                        
                        // check if the app received valid response
                       if ([responseObject count] > 0)
                       {
                           NSMutableArray *searchArraylist = [NSMutableArray array];
                           NSArray *searchArray = [responseObject objectForKey:@"assets"];
                           for (NSDictionary * asset in searchArray)
                           {
                               [searchArraylist addObject:[asset valueForKey:@"assetID"]];
                           }
                           [self EditToCoreData:responseObject selectedAssetlist:searchArraylist success:^(NSArray *assetsLoaded) {
                               callback(nil, assetsLoaded);
                           } error:^(NSError *error) {
                               [AbbvieLogging logError:[NSString stringWithFormat:@"error saving Assets %@", error.localizedDescription]];
                                callback(error, responseObject);
                           }];
                       }
                       else{
                           callback(nil,responseObject);
                       }
 
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                     [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.searchSuccessResponseMsg responseObj:error];
                    callback(errorCode, nil);
                }
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {
                
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.searchErrorResponseMsg,error.localizedDescription]];
                if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                }
                else
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
            }else{
                NSArray *searchArray = (NSArray *)response;
                complete(searchArray);
               
            }
        }];
    }
}


-(void)EditToCoreData:(id) response selectedAssetlist:(NSArray *)selectedAssetlist success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{
    
    NSMutableDictionary * responseObject = response;
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        if([responseObject valueForKey:@"assets"] != nil) {
            int i= 0;
            NSMutableSet *assetsSet = [NSMutableSet new];

            /* Saving asset in core data removing the duplicates and editing the existing values in asset object*/
            for (NSDictionary * asset in [responseObject valueForKey:@"assets"])
            
            {
                    
                            NSString * assetId = [asset valueForKey:@"assetID"];
                            NSArray * assetArray = [AssetsServiceClass.sharedAPIManager getAsset:assetId withContext:localContext];
                            
                            if (assetArray.count == 0)
                            {
                                Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                                [newAsset MR_importValuesForKeysWithObject:asset];
                                newAsset.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                                [assetsSet addObject:newAsset];
                            }
                            else {}
                            i++;
            }
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
        if (error) {

        }else{
            NSFetchRequest * fetchrequest = [Asset MR_requestAll];
            NSArray *importedAssets = [Asset MR_executeFetchRequest:fetchrequest];
            NSMutableArray* checkAssets = [[NSMutableArray alloc] init];
            
            for (NSDictionary * asset in importedAssets) {
            NSString * assetId = [[asset valueForKey:@"assetID"] stringValue];
             for(NSString* aID in selectedAssetlist)
             {
                if([assetId isEqualToString:aID])
                {
                    [checkAssets addObject:asset];
                }
             }
            }
            
            [[ZMUserActions sharedInstance]removeDuplicatesForAssets:checkAssets];
            success(checkAssets);

        }
    }];
    
    
}


@end
