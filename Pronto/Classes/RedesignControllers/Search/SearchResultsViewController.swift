//
//  SearchResultsViewController.swift
//  Pronto
//
//  Created by Saranya on 06/10/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import SafariServices
class SearchResultsViewController: SearchHistoryViewController {
        
    var searchResultsArray:[Asset]?
    var searchText = ""
  
    override func viewDidLoad() {
        self.view.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        loadAdditionalViews()
      NotificationCenter.default.addObserver(self, selector:#selector(dismissView)
                                             , name: NSNotification.Name(rawValue: RedesignConstants.removeSearchResultsVCListNotification),
                                             object: nil)
      ProntoRedesignSingletonClass.sharedInstance.lastSearchText = searchText
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
  /* Func to load additional views */
    func loadAdditionalViews(){
        let searchResultsView:SearchView = Bundle.main.loadNibNamed(RedesignConstants.searchResultsViewNibName, owner: self, options: nil)?.first as! SearchView
        searchResultsView.searchDelegate = self
        searchResultsView.webLinkDelegate = self
        searchResultsView.searchText = searchText
        searchResultsView.setData(assetArray: searchResultsArray)
        self.view.addSubview(searchResultsView)

        searchResultsView.translatesAutoresizingMaskIntoConstraints = false
        searchResultsView.pinEdges(to: self.view)
        
        searchResultsView.parent = self
    }
  /* Func to go back home */
    func goBackToHome(){
        self.navigationController?.popViewController(animated: true)
    }
  
  @objc func dismissSearchHeaderView() {
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewSearchListNotification), object: self, userInfo: nil)
    dismissView()
  }
  
  func showSafari(uri: String) {
      if let url = URL(string: uri) {
        if #available(iOS 11.0, *) {
          let config = SFSafariViewController.Configuration()
          config.entersReaderIfAvailable = false
          let vc = SFSafariViewController(url: url, configuration: config)
          vc.dismissButtonStyle = .close
          vc.toolbarItems = []
          present(vc, animated: true)
        } else {
          // Fallback on earlier versions
        }
      }
  }
}

extension SearchResultsViewController: SearchProtocol {
  /// Search Results history
  /// - Parameters:
  ///   - xAxis: x axis for search history view
  ///   - yAxis: y axis for search history view
  ///   - width: width of the search history view
  ///   - height: height of the search history view
  ///   - mainStackView: super view of search history view
  func addSearchResultsHistory(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int, mainStackView: UIView) {
    addSearchListView(xAxis: xAxis + 6, yAxis: 66, width: width, height: height, mainStackView: mainStackView)
  }
}

extension SearchResultsViewController: SearchWeblinkViewProtocol {
  func openWebLink(uri: String) {
    showSafari(uri: uri)
  }
}
