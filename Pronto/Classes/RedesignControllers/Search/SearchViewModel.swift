//
//  SearchViewModel.swift
//  Pronto
//
//  Created by Saranya on 06/10/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation

@objc class SearchViewModel: NSObject {
    @objc static let sharedInstance = SearchViewModel()
    var index: IndexPath!
    var searchResultAssets: [Asset]? = []
    var filterResultsAssets: [Asset]? = []
    @objc var parentSearchListCell: FranchiseTableViewCell?
    @objc var parentSearchListView: SearchListView?
    
    
    /* Check for connectivity and perform Search */
    func doSearch(searchText:String?){
        if let searchtxt = searchText{
          var savedArray = UserDefaults.standard.array(forKey: "keywordTxt") as? [String] ?? []
          let duplicate = savedArray.contains { $0.lowercased() == searchtxt.lowercased() }
          //remove duplicate
          if !duplicate {
            savedArray.append(searchtxt)
            savedArray = Array(Set(savedArray))
            if (savedArray.count > 5) {
              let arraySlice = savedArray.prefix(5)
              let newArray = Array(arraySlice)
              savedArray = newArray
            }
            UserDefaults.standard.set(savedArray, forKey: "keywordTxt")
          }
          
            if((ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).isConnectionAvailableForYou()){
                performOnlineSearch(searchText: searchtxt)
            }else{
                performOfflineSearch(searchText: searchtxt)
            }
        }
    }
  /* Func to do online Search */
  @objc func performOnlineSearch(searchText:String){
    let validSearchText = searchText.replacingOccurrences(of: " ", with: "-")
    RedesignUtilityClass.sharedInstance.showLoadingIndicator(inViewController: nil)
    SearchServiceClass.sharedAPIManager().performSearch(validSearchText, completionBlock: { [self] (assetArray:[Any]) in
      
      let filteredArray = assetArray as? [Asset]
      searchResultAssets = filteredArray?.filter {
        ($0.title!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
          ($0.field_description!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
          ($0.uri_full!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
          ($0.field_thumbnail!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
          ($0.file_mime!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
          ($0.path!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil)
      }
      //Remove duplicate Assets by Name
      filterResultsAssets = removeDuplicateElements(assetlist: searchResultAssets!)
      
      //            self.filterResultsAssets = assetArray as? [Asset]
      //Bydefault page sort by "Recently Added"
      
      self.filterResultsAssets?.sort {return $0.publish_on!.compare($1.publish_on!) == .orderedDescending}
      self.loadSearchResultsScreen(searchText: searchText)
    }) { (error) in
      print(error)
    }
  }
    /* Func to perform offline Search */
    func performOfflineSearch(searchText:String){
        let savedAssets = findAllAssets()
        if searchText.isEmpty {
            
            } else {
                NSLog("1")
                searchResultAssets = savedAssets.filter{
                    ($0.title!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
                    ($0.field_description!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
                    ($0.uri_full!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
                    ($0.field_thumbnail!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
                    ($0.file_mime!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil) ||
                    ($0.path!.range(of:searchText, options: NSString.CompareOptions.caseInsensitive) != nil)
                }
                //Remove duplicate Assets by Name
                filterResultsAssets = removeDuplicateElements(assetlist: searchResultAssets!)
                
                
            }

        loadSearchResultsScreen(searchText: searchText)
    }
  /* Func to find offline assets */
    func findAllAssets() -> [Asset]{
        let request : NSFetchRequest =  Asset.mr_requestAll()
        return Asset.mr_execute(request) as! [Asset]
    }
  /* Func to remove duplicate elements */
    func removeDuplicateElements(assetlist: [Asset]) -> [Asset] {
        var uniquePosts = [Asset]()
        for assetvalue in assetlist {
            if !uniquePosts.contains(where: {$0.title == assetvalue.title }) {
                uniquePosts.append(assetvalue)
            }
        }
        return uniquePosts
    }
 
    /* func to navigate to Search Screen */
    func loadSearchResultsScreen(searchText: String){
        let searchController:SearchResultsViewController = SearchResultsViewController()
        searchController.searchResultsArray = filterResultsAssets
        searchController.searchText = searchText
        ProntoRedesignSingletonClass.sharedInstance.searchSortingAssetsArray = filterResultsAssets
        if HomeViewModel.sharedInstance.flowType != RedesignConstants.searchFlow {
          HomeViewController.sharedInstance.getHomeNavController().pushViewController(searchController, animated: true)
        } else {
          let rawDataDict:[String: [Asset]?] = [RedesignConstants.searchSortingTermKey: self.filterResultsAssets]
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.searchSortingReloadNotification), object: nil, userInfo: rawDataDict as [AnyHashable : Any])
        }
    }
    /* Handle Opening of Asset */
    func openAsset(selectedAsset:Asset,selectedSearchListCell:FranchiseTableViewCell, selectedSearchListView:SearchListView){
        
        ProntoAssetHandlingClass.sharedInstance().resetAssetValue()
        ProntoAssetHandlingClass.sharedInstance().isFromHomeView = false
        
        self.parentSearchListView = selectedSearchListView
        self.parentSearchListCell = selectedSearchListCell
        
   
        ProntoAssetHandlingClass.sharedInstance().selectedListCell = selectedSearchListCell

        let assetHandlingViewModel = ProntoAssetHandlingViewModel()
        assetHandlingViewModel.searchParent = self
        assetHandlingViewModel.openAsset(selectedAsset: selectedAsset)
       
    }
    /* Handle Info Options */
    func handleInfoPopOver(sender:UIButton,selectedListCell:FranchiseTableViewCell?,parentListView:SearchListView,selectedAsset:Asset){
        
        index = IndexPath(row: sender.tag, section: 0)
        
        parentSearchListView = parentListView
        parentSearchListCell = selectedListCell
        
        let assetModel = ProntoAssetHandlingViewModel()
        assetModel.searchParent = self
        assetModel.configureInfoPopOver(selectedAsset: selectedAsset,sender: sender)
    }
    /* Method to set back the Share Icon to Normal and close export popover */
    @objc func setExportToNormal(_ dismissExport: Bool) {
        parentSearchListCell?.shareButton.setImage(UIImage(named: RedesignConstants.export_redesign), for: .normal)
        parentSearchListCell?.shareButton.isSelected = false
        parentSearchListView?.searchTableView.reloadRows(at: [index], with: .none)
    }
    /* Handle Export PopOver */
    func handleExportPopOver(sender:UIButton,selectedListCell:FranchiseTableViewCell?,parentListView:SearchListView,selectedAsset:Asset){
        
        index = IndexPath(row: sender.tag, section: 0)
        
        parentSearchListView = parentListView
        parentSearchListCell = selectedListCell
        
        let assetModel = ProntoAssetHandlingViewModel()
        assetModel.searchParent = self
        assetModel.configureExportPopOver(selectedAsset: selectedAsset,sender: sender)
    }
}

