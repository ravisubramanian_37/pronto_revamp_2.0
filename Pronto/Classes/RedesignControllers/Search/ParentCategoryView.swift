//
//  ParentCategoryView.swift
//  Pronto
//
//  Created by Saranya on 27/04/2021.
//  Copyright © 2021 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class ParentCategoryView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var tableview: UITableView = UITableView()
    var parentCategory:[ParentFolder] = []
  
    func loadTable(parent: [ParentFolder], cell: UIButton){
        self.backgroundColor = UIColor.white
        parentCategory = parent
        tableview.backgroundColor = UIColor.white
        tableview.delegate = self
        tableview.dataSource = self

        tableview.frame = CGRect(x: 0, y: 0, width: Int(cell.superview!.frame.size.width), height: 30  * parentCategory.count)

        self.addSubview(tableview)
        self.frame = cell.frame
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parentCategory.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: RedesignConstants.cellId)
        let parentCategoryName = parentCategory[indexPath.row]

        if( !(cell != nil)){
            cell = UITableViewCell(style: .default, reuseIdentifier: RedesignConstants.cellId)
        }
        
        cell!.textLabel?.text = parentCategoryName.name
        cell!.textLabel?.textColor = UIColor.black
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
}
