//
//  SearchListView.swift
//  Pronto
//
//  Created by Saranya on 19/10/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
protocol SearchListViewProtocol {
  func openWeblinkList(uri: String)
}
class SearchListView:UIView, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate {
    var searchTableView:UITableView = UITableView()
    var sortingAssetsArray:[Asset]?
    var assetForAudio:Asset?
    var audioPlayerView:AudioPlayer?
    var weblinkListDelegate: SearchListViewProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame) // calls designated initializer
        
    }
    required init?(coder: NSCoder) {
        fatalError(RedesignConstants.commonInitError)
    }
    /* Register For Notifications */
    func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(reloadSortingResults(_:)), name: NSNotification.Name(rawValue: RedesignConstants.searchSortingReloadNotification), object: nil)
    }
    /* Method to reload view on sorting option */
    @objc func reloadSortingResults(_ notification: NSNotification) {
        let notificationDict = notification.userInfo
        self.sortingAssetsArray = notificationDict?[RedesignConstants.searchSortingTermKey] as? [Asset]
        searchTableView.reloadData()
    }
    /* Method to create Table */
    func prepareView(){
        registerForNotifications()
        
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTableView.register(UINib(nibName: RedesignConstants.franchiseCellNibName, bundle: nil), forCellReuseIdentifier: RedesignConstants.franchiseCellNibName)
        self.addSubview(searchTableView)
        searchTableView.separatorStyle = .none

        searchTableView.translatesAutoresizingMaskIntoConstraints = false
        searchTableView.leadingAnchor .constraint(equalTo: self.leadingAnchor, constant: 0.5).isActive = true
        searchTableView.trailingAnchor .constraint(equalTo: self.trailingAnchor, constant: -0.5).isActive = true
        searchTableView.topAnchor .constraint(equalTo: self.topAnchor, constant: 0.5).isActive = true
        searchTableView.bottomAnchor .constraint(equalTo: self.bottomAnchor,constant: -0.5).isActive = true
        
    }
    /* TableView Delegates & DataSource */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let assetCount = self.sortingAssetsArray {
            return assetCount.count
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:FranchiseTableViewCell!
        if let cell1 = tableView.dequeueReusableCell(withIdentifier: RedesignConstants.franchiseCellNibName) as? FranchiseTableViewCell {
            cell = cell1
        }
        else{
            let nib = Bundle.main.loadNibNamed(RedesignConstants.franchiseCellNibName, owner: self, options: nil)
            tableView.register(UINib(nibName: RedesignConstants.franchiseCellNibName, bundle: nil), forCellReuseIdentifier: RedesignConstants.franchiseCellNibName)
            cell = nib?[0] as? FranchiseTableViewCell
        }

        //Hide loader background
        cell.loaderBackgroundView.backgroundColor = UIColor.clear
        cell.loaderBackgroundView.alpha = 1
        cell.loaderBackgroundView.isHidden = true
        cell.audioPlayerView.isHidden = true
        cell.loaderView.isHidden = true
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        if let assetObj = self.sortingAssetsArray{
        if assetObj.count > 0 && indexPath.row < assetObj.count {
                        
            let asset:Asset! = assetObj[indexPath.row]
            cell.labelAssetName.text = asset.title
            let assetType = Constant.getFileType(for: asset)
            var imageName:String = RedesignConstants.pdfName
            if (assetType == ZMProntoFileTypesPDF) {
                imageName = RedesignConstants.pdfName
            }else if (assetType == ZMProntoFileTypesVideo) {
                imageName = RedesignConstants.videoName
            }else if (assetType == ZMProntoFileTypesBrightcove) {
                imageName = RedesignConstants.videoName
            }else if (assetType == ZMProntoFileTypesDocument) {
                imageName = RedesignConstants.pdfName
            }else if(assetType == ZMProntoFileTypesWeblink){
                imageName = RedesignConstants.weblinkName
            }else if (assetType == ZMProntoFileTypesAudio){
                imageName = RedesignConstants.centerImageAudio
            }
            
            cell.assetImageType.image = UIImage.init(named: imageName)
            cell.labelAssetType.text = asset.field_description?.replacingOccurrences(of: "&amp;", with: "&")
            cell.isNew = asset.is_new
            
            if let changetime = asset.changed , let createtime = asset.created
            {
                        let myTimeInterval = TimeInterval(changetime.doubleValue)
                        let mychgdate = Date(timeIntervalSince1970: myTimeInterval)
                        let mycreatInterval = TimeInterval(createtime.doubleValue)
                        let mycreatedate = Date(timeIntervalSince1970: mycreatInterval)
                        let calendar = Calendar.current
                        let numberOfDays = calendar.dateComponents([.day], from: mychgdate, to: Date())
                        let order = Calendar.current.compare(mychgdate, to: mycreatedate, toGranularity: .day)
                        if (order == .orderedSame && numberOfDays.day! + 1 <= 30)
                        {
                            cell.updateAvailableButton.isHidden = true
                        }
                        else if (order == .orderedDescending && numberOfDays.day! + 1 <= 30)
                        {
                            cell.isUpdateAvailable = true
                            cell.updateAvailableButton.isHidden = false
                        }
                        else{
                            cell.isUpdateAvailable = false
                            cell.updateAvailableButton.isHidden = true
                        }
           }
            if let assetId = asset.assetID{
                cell.uid = assetId.intValue
            }
        cell.shareButton.addTarget(self, action: #selector(exportIconClicked(_:)), for: .touchUpInside)
        cell.favButton.addTarget(self, action: #selector(favIconClicked(_:)), for: .touchUpInside)
        cell.infoButton.addTarget(self, action: #selector(infoIconClicked(_:)), for: .touchUpInside)
        
        cell.shareButton.tag = indexPath.row
        cell.favButton.tag = indexPath.row
        cell.infoButton.tag = indexPath.row
        
        let favfranchise = ZMCDManager.sharedInstance.getFavFranchise()
        cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
        cell.favButton.isSelected = false
        for fav in favfranchise {
            guard let fav = fav as? FavFranchise else {
                continue
            }
            if cell.uid == fav.asset_ID?.intValue {
                cell.favButton.setImage(UIImage(named: RedesignConstants.favoSelected), for: .selected)
                cell.favButton.isSelected = true
                break
            } else {
                cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
                cell.favButton.isSelected = false
            }
        }
        if let audioAsset = ProntoRedesignSingletonClass.sharedInstance.audioAsset{
            assetForAudio = audioAsset
        }
        if asset.assetID == assetForAudio?.assetID{
            audioPlayerView?.removeFromSuperview()
            if let audioView = ProntoRedesignSingletonClass.sharedInstance.audioAssetView{
                audioPlayerView = audioView
            }
            cell.audioPlayerView.isHidden = false
            if(audioPlayerView != nil){
            cell.audioPlayerView.addSubview(audioPlayerView!)
            }
        }
            
            //Adding parent folder name in dropdown
            if(asset.parentCategory != nil){
                cell.parentFolderView.layer.borderColor = UIColor.lightGray.cgColor
                cell.parentFolderView.layer.borderWidth = 0.5
                if(asset.parentCategory?.count == 1){
                    for parent in asset.parentCategory!{
                        cell.parentFolderLabel.text = parent.value(forKey: "name") as? String
                        cell.parentFolderLabel.text = "parent"
                        cell.parentFolderMoreButton.isHidden = true
                    }
                }else{
                    cell.parentFolderMoreButton.addTarget(self, action: #selector(showParent(button:selectedAsset:)), for: .touchUpInside)
                }
            }else{
                cell.parentFolderView.isHidden = true
            }
        }
        //Hide open/update Button
        cell.openButton.isHidden = true
        cell.updateButton.isHidden = true
        }
        
    return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: false)
        if let cell = tableView .cellForRow(at: indexPath) as? FranchiseTableViewCell{
            let indexVar = indexPath.row
            let asset:Asset! = (self.sortingAssetsArray?[indexVar])!
            let assetType = Constant.getFileType(for: asset)
            if assetType == ZMProntoFileTypesWeblink {
              self.weblinkListDelegate?.openWeblinkList(uri: asset.uri_full ?? "")
            } else {
              openAsset(selectedAsset: asset, selectedlistItem: cell)
            }
          }
    }
    /* Prepare cell to Show Parent folder*/
    @objc func showParent(button:UIButton,selectedAsset:Asset){
        let parentViewController:UIViewController = UIViewController()
        
        //Add parent View
        let parentCategoryView = ParentCategoryView()
        let parentCategory:[ParentFolder] = []
        parentCategoryView.loadTable(parent:parentCategory, cell: button)
        parentViewController.view = parentCategoryView
        
        let popoverContent = parentViewController 
        popoverContent.modalPresentationStyle = .popover

            if let popover = popoverContent.popoverPresentationController {
               popover.backgroundColor = .white
                let viewForSource = button as UIView
               popover.sourceView = viewForSource
               popover.permittedArrowDirections = .up
               popover.sourceRect = CGRect(x: 30, y: viewForSource.bounds.maxY - viewForSource.bounds.minY - 10, width: 0, height: 0)
               // the size you want to display
               
                popoverContent.preferredContentSize = CGSize(width: 100,height: 30 * 3)
//                selectedAsset.parentCategory!.count)
                
               popover.delegate = self
               
                //Asset open Case
                RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: popoverContent, animated: true)
                    
               
            }
    }
    //Method to define the presentation style for popover presentation controller
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
              return .none
          }
   
    /* Handle Opening of Asset */
    func openAsset(selectedAsset:Asset, selectedlistItem:FranchiseTableViewCell){
        SearchViewModel.sharedInstance.openAsset(selectedAsset: selectedAsset, selectedSearchListCell: selectedlistItem, selectedSearchListView: self)
    }
    /* Handling asset options - Info */
    @IBAction func infoIconClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.setImage(UIImage(named: RedesignConstants.info_redesign), for: .normal)
            sender.isSelected = false
        } else {
            sender.setImage(UIImage(named: RedesignConstants.info_redesign_selected), for: .selected)
            sender.isSelected = true
                
        let asset:Asset! = (self.sortingAssetsArray?[sender.tag])!
            
        let searchViewModel:SearchViewModel = SearchViewModel()
        searchViewModel.handleInfoPopOver(sender: sender, selectedListCell: nil, parentListView: self, selectedAsset: asset)
            
        }
    }
    /* Handling asset options - Share */
    @objc func exportIconClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.setImage(UIImage(named: RedesignConstants.export_redesign), for: .normal)
            sender.isSelected = false
        } else {
            sender.setImage(UIImage(named: RedesignConstants.export_redesign_selected), for: .selected)
            sender.isSelected = true
            
            let asset:Asset! = (self.sortingAssetsArray?[sender.tag])!

            let searchViewModel:SearchViewModel = SearchViewModel()
            searchViewModel.handleExportPopOver(sender: sender, selectedListCell: nil, parentListView: self, selectedAsset: asset)
        }
    }
    /* Handle favo icon tapped Action */
     @IBAction func favIconClicked(_ sender: UIButton) {
        
         if sender.isSelected {
            sender.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .normal)
            sender.isSelected = false
            
            MyFavouritesViewModel.sharedInstance.deleteAssetFromFavourites(asset: self.sortingAssetsArray![sender.tag])
            
        } else {
            sender.setImage(UIImage(named: RedesignConstants.favoSelected), for: .selected)
            sender.isSelected = true

            MyFavouritesViewModel.sharedInstance.createFavObject(selectedAsset: self.sortingAssetsArray![sender.tag])
        }
    }
}
