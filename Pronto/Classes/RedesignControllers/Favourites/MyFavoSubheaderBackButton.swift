//
//  MyFavoSubheaderBackButton.swift
//  Pronto
//
//  Created by Saranya on 26/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class MyFavoSubheaderBackButton: UIView {
    @IBOutlet weak var myFavouritesBackView: UIView!
    @IBOutlet weak var myFavouritesBackTitleLabelText: UILabel!
    @IBOutlet weak var myFavouritesBackCountLabelText: UILabel!
    var parentControllerForFavourites: MyFavouritesDetailsViewController?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    /* Method to handle backbutton action in myFavourites details screen */
    @IBAction func myFavouritesBackButtonTapped(_ sender: Any) {
        RedesignErrorHandling.sharedInstance().hideMessage()
        parentControllerForFavourites?.navigationController?.popViewController(animated: true)
    }
    /* Configuring the Back button View in MyFavourites details Screen */
    func addMoreDetailsToView() {
        myFavouritesBackTitleLabelText.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)
        myFavouritesBackCountLabelText.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)
        
        let count:NSNumber = NSNumber(integerLiteral: ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.count ?? 0)
        if (count.intValue <= 1) {
            myFavouritesBackCountLabelText.text = String(count.intValue)  + RedesignConstants.assetTxt
        } else {
            myFavouritesBackCountLabelText.text = String(count.intValue)  + RedesignConstants.assetsTxt
        }
    }
}
