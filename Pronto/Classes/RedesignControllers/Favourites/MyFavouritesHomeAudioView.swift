//
//  MyFavouritesHomeAudioView.swift
//  Pronto
//
//  Created by Saranya on 17/07/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
protocol closeAudioDelegate: class {
    func closeAudioView()
}
class MyFavouritesHomeAudioView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var audioView: UIView!
    weak var delegate: closeAudioDelegate?
     override func awakeFromNib() {
           super.awakeFromNib()
           
       }
    /* Method to handle close action in Audio View */
    @IBAction func closeBtnTapAction(_ sender: Any) {
        self.removeFromSuperview()
        delegate?.closeAudioView()
    }
}
