//
//  MyFavouritesListView.swift
//  Pronto
//
//  Created by Saranya on 26/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
protocol FavListViewProtocol {
  func openWeblinkList(uri: String)
}
class MyFavouritesListView: UIView, UITableViewDelegate, UITableViewDataSource, UITableViewDragDelegate, UITableViewDropDelegate {
    var parentController: MyFavouritesDetailsViewController?
    var isObjectAvailable = false
    var assetsArray: [FavFranchise]? = []
    var screenWidth = UIScreen.main.bounds.size
    var assetForAudio: Asset?
    var audioPlayerView: AudioPlayer?
    var tableView: UITableView = UITableView()
    var weblinkListDelegate: FavListViewProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError(RedesignConstants.commonInitError)
    }
    /* Set up data for loading My Favourites List View */
    func setData(parentContlr: MyFavouritesDetailsViewController, objAvailable: Bool) {
        self.parentController = parentContlr
        self.isObjectAvailable = objAvailable
        sortFavAsset()
    }
    /* Method to sort data based on the priority */
    func sortFavAsset() {
        MyFavouritesViewModel.sharedInstance.checkFavoSingletonAssetArray()
        self.assetsArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.sorted(by: { $1.priority!.intValue > $0.priority!.intValue })
    }
    /* Prepare view to present */
    func prepareView() {
        registerForNotification()
        
        tableView.delegate = self
        tableView.dataSource = self
        if #available(iOS 11.0, *) {
            tableView.dragInteractionEnabled = true
            tableView.dragDelegate = self
            tableView.dropDelegate = self
        } else {
            // Fallback on earlier versions
        }
        
        tableView.register(UINib(nibName: RedesignConstants.franchiseCellNibName, bundle: nil), forCellReuseIdentifier: RedesignConstants.franchiseCellNibName)
        self.addSubview(tableView)
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.pinEdges(to: self)
    }
    /* Register for Notification */
    func registerForNotification() {
        NotificationCenter.default.addObserver(self, selector:#selector(handleOrientationChange) , name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange), object: nil)
    }
    /* Method called to handle Orientation Change Notification */
    @objc func handleOrientationChange() {
        if(isObjectAvailable) {
        }
    }
    /* Tableview drag and drop delelgates */
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let item = (self.assetsArray?[indexPath.row])?.asset_ID?.stringValue
        let itemProvider = NSItemProvider(object: (item ?? "") as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = item
        return [dragItem]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        
        guard let destinationIndexPath = coordinator.destinationIndexPath else {
            return
        }
        coordinator.items.forEach { dropItem in
            guard let sourceIndexPath = dropItem.sourceIndexPath else {
                return
            }
            tableView.performBatchUpdates({
                let assetTo = assetsArray?[sourceIndexPath.row]
                assetsArray?.remove(at: sourceIndexPath.row)
                assetsArray?.insert(assetTo!, at: destinationIndexPath.row)
                tableView.deleteRows(at: [sourceIndexPath], with: .none)
                tableView.insertRows(at: [destinationIndexPath], with: .none)
                
            }, completion: { _ in
                coordinator.drop(dropItem.dragItem,
                                 toRowAt: destinationIndexPath)
                ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray = self.assetsArray
                MyFavouritesViewModel.sharedInstance.updateAssetAfterDragAndDrop()
            })
        }
    }
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSString.self)
    }
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal {
            return UITableViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
    }
    
    /* Tableview Delegates and Datasource */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assetsArray?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      var cell:FranchiseTableViewCell!
      if let cell1 = tableView.dequeueReusableCell(withIdentifier: RedesignConstants.franchiseCellNibName) as? FranchiseTableViewCell{
        cell = cell1
      } else {
        let nib = Bundle.main.loadNibNamed(RedesignConstants.franchiseCellNibName, owner: self, options: nil)
        tableView.register(UINib(nibName: RedesignConstants.franchiseCellNibName, bundle: nil), forCellReuseIdentifier: RedesignConstants.franchiseCellNibName)
        cell = nib?[0] as? FranchiseTableViewCell
      }
      cell.parentFolderView.isHidden = true
      //Hide loader background
      cell.loaderBackgroundView.backgroundColor = UIColor.clear
      cell.loaderBackgroundView.alpha = 1
      cell.loaderBackgroundView.isHidden = true
      cell.audioPlayerView.isHidden = true
      cell.loaderView.isHidden = true
      cell.layer.borderColor = UIColor.lightGray.cgColor
      cell.layer.borderWidth = 0.5
      if let assetObj = assetsArray{
        if assetObj.count > 0 && indexPath.row < assetObj.count {
          
          if let asset = getAssetForIndexPath(indexpath: indexPath.row){
            
            cell.labelAssetName.text = asset.title
            let assetType = Constant.getFileType(for: asset)
            var imageName:String = RedesignConstants.pdfName
            if (assetType == ZMProntoFileTypesPDF) {
              imageName = RedesignConstants.pdfName
            }else if (assetType == ZMProntoFileTypesVideo) {
              imageName = RedesignConstants.videoName
            }else if (assetType == ZMProntoFileTypesBrightcove) {
              imageName = RedesignConstants.videoName
            }else if (assetType == ZMProntoFileTypesDocument) {
              imageName = RedesignConstants.pdfName
            }else if(assetType == ZMProntoFileTypesWeblink){
              imageName = RedesignConstants.weblinkName
            }else if(assetType == ZMProntoFileTypesAudio){
              imageName = RedesignConstants.centerImageAudio
            }
            cell.assetImageType.image = UIImage.init(named: imageName)
            
            cell.labelAssetType.text = asset.field_description?.replacingOccurrences(of: "&amp;", with: "&")
            
            cell.isNew = asset.is_new
            if let changetime = asset.changed , let createtime = asset.created
            {
              let myTimeInterval = TimeInterval(changetime.doubleValue)
              let mychgdate = Date(timeIntervalSince1970: myTimeInterval)
              let mycreatInterval = TimeInterval(createtime.doubleValue)
              let mycreatedate = Date(timeIntervalSince1970: mycreatInterval)
              let calendar = Calendar.current
              let numberOfDays = calendar.dateComponents([.day], from: mychgdate, to: Date())
              let order = Calendar.current.compare(mychgdate, to: mycreatedate, toGranularity: .day)
              if (order == .orderedSame && numberOfDays.day! + 1 <= 30) {
                cell.updateAvailableButton.isHidden = true
              } else if (order == .orderedDescending && numberOfDays.day! + 1 <= 30) {
                cell.isUpdateAvailable = true
                cell.updateAvailableButton.isHidden = false
              } else{
                cell.isUpdateAvailable = false
                cell.updateAvailableButton.isHidden = true
              }
            }
            if let assetId = asset.assetID {
              cell.uid = assetId.intValue
            }
            cell.shareButton.addTarget(self, action: #selector(exportIconClicked(_:)), for: .touchUpInside)
            cell.favButton.addTarget(self, action: #selector(favIconClicked(_:)), for: .touchUpInside)
            cell.infoButton.addTarget(self, action: #selector(infoIconClicked(_:)), for: .touchUpInside)
            cell.shareButton.tag = indexPath.row
            cell.favButton.tag = indexPath.row
            cell.infoButton.tag = indexPath.row
            
            let favfranchise = ZMCDManager.sharedInstance.getFavFranchise()
            cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
            cell.favButton.isSelected = false
            for fav in favfranchise {
              guard let fav = fav as? FavFranchise else {
                continue
              }
              if cell.uid == fav.asset_ID?.intValue {
                cell.favButton.setImage(UIImage(named: RedesignConstants.favoSelected), for: .selected)
                cell.favButton.isSelected = true
                break
              } else {
                cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
                cell.favButton.isSelected = false
              }
            }
            if let audioAsset = ProntoRedesignSingletonClass.sharedInstance.audioAsset {
              assetForAudio = audioAsset
            }
            if asset.assetID == assetForAudio?.assetID //&& (audioCurrentPromotedGridCell == nil) {
            {
              audioPlayerView?.removeFromSuperview()
              if let audioView = ProntoRedesignSingletonClass.sharedInstance.audioAssetView {
                audioPlayerView = audioView
              }
              cell.audioPlayerView.isHidden = false
              if(audioPlayerView != nil) {
                //                                audioPlayerView?.frame = cell.audioPlayerView.frame
                cell.audioPlayerView.addSubview(audioPlayerView!)
                audioPlayerView?.frame = CGRect(x: 0, y: 0,width: cell.audioPlayerView.frame.size.width, height: cell.audioPlayerView.frame.size.height)
              }
            }
          }
        }
      }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: false)
        if let cell = tableView .cellForRow(at: indexPath) as? FranchiseTableViewCell{
            if let asset = getAssetForIndexPath(indexpath: indexPath.row){
              let assetType = Constant.getFileType(for: asset)
              if assetType == ZMProntoFileTypesWeblink {
                self.weblinkListDelegate?.openWeblinkList(uri: asset.uri_full ?? "")
              } else {
                openAsset(selectedAsset: asset, selectedGridCell: nil, selectedlistItem: cell)
              }
            }
        }
    }
    /* Get asset for index path */
    func getAssetForIndexPath(indexpath: Int) -> Asset? {
        if let selAsset = self.assetsArray?[indexpath] {
            if selAsset.asset_ID != nil {
              return (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getAllAsset(selAsset.asset_ID!)
            }
        }
        return nil
    }
    /* Handling asset options - Share */
    @objc func exportIconClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.setImage(UIImage(named: RedesignConstants.export_redesign), for: .normal)
            sender.isSelected = false
        } else {
            sender.setImage(UIImage(named: RedesignConstants.export_redesign_selected), for: .selected)
            sender.isSelected = true
            if let asset = getAssetForIndexPath(indexpath: sender.tag) {
                CategoryViewModel.sharedInstance.handleExportPopOver(sender: sender,
                                                                     parentCategoryGrid: nil,
                                                                     parentCategoryList: nil,
                                                                     parentFavouritesGrid: nil,
                                                                     parentFavouritesList: self,
                                                                     selectedAsset: asset)
            }
        }
    }
    /* Handling asset options - Info */
    @IBAction func infoIconClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.setImage(UIImage(named: RedesignConstants.info_redesign), for: .normal)
            sender.isSelected = false
        } else {
            sender.setImage(UIImage(named: RedesignConstants.info_redesign_selected), for: .selected)
            sender.isSelected = true
                
            if let asset = getAssetForIndexPath(indexpath: sender.tag){
                CategoryViewModel.sharedInstance.configureInfoPopOver(sender,
                                                                      parentCategoryGrid: nil,
                                                                      parentCategoryList: nil,
                                                                      parentFavouritesGrid: nil,
                                                                      parentFavouritesList: self,
                                                                      selectedAsset: asset)
            }
        }
    }
    /* Handle Opening of Asset */
    func openAsset(selectedAsset:Asset,selectedGridCell:ZMGridItem?, selectedlistItem:FranchiseTableViewCell?){
        CategoryViewModel.sharedInstance.openAsset(selectedAsset: selectedAsset,
                                                   selectedGridCell: nil,
                                                   selectedlistItem: selectedlistItem,
                                                   selectedCategoryListView: nil,
                                                   selectedCategoryGridView: nil,
                                                   selectedFavouritesListView: self,
                                                   selectedFavouritesGridView: nil)
    }
    /* Handle favo icon tapped Action */
     @IBAction func favIconClicked(_ sender: UIButton) {
        self.assetsArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray
         if let asset = getAssetForIndexPath(indexpath: sender.tag){
            MyFavouritesViewModel.sharedInstance.updateFavourites(selectedFavouriteAsset:asset, isSelected: false)
        }
        ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.remove(at: sender.tag)
        MyFavouritesViewModel.sharedInstance.updateFavouritesPriority()
        self.assetsArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray
        
        let index = IndexPath(row: sender.tag , section: 0)
        tableView.deleteRows(at: [index], with: .automatic)
        tableView.reloadData()
        // post a notification to update favourites Count
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.favouritesRemovalNotification), object: nil, userInfo: nil)
    }
}
