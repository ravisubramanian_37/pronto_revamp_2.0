//
//  MyFavouritesDetailsViewController.swift
//  Pronto
//
//  Created by Saranya on 24/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
class MyFavouritesDetailsViewController: SearchHistoryViewController {
  var favouritesAssetsArray:[FavFranchise]!
    
  class var sharedInstance: MyFavouritesDetailsViewController {
      struct MyFavouritesDetailsVCObject {
          static let instance = MyFavouritesDetailsViewController()
      }
      return MyFavouritesDetailsVCObject.instance
  }

    override func viewDidLoad() {
        self.view.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        loadAdditionalViews()
      NotificationCenter.default.addObserver(self, selector:#selector(dismissView),
                                             name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewControllerSearchListNotification),
                                             object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.clearSearchTextNotification),
                                      object: self,
                                      userInfo: nil)
    }
  override func viewWillDisappear(_ animated: Bool) {
    HomeMainHeaderView.sharedInstance.dismissSearchListView()
  }
  /// load favorites view
  func loadAdditionalViews(){
    let myFavouriteView:MyFavoDetailsView = Bundle.main.loadNibNamed(RedesignConstants.myFavouritesDetailsViewNibName,
                                                                     owner: self,
                                                                     options: nil)?.first as! MyFavoDetailsView
    myFavouriteView.parentController = self
    myFavouriteView.favDelegate = self
    myFavouriteView.webLinkDelegate = self
    myFavouriteView.doAdditionalSetUp()
    self.view.addSubview(myFavouriteView)
    /* Add constraints for My Favourites Detail View */
    myFavouriteView.translatesAutoresizingMaskIntoConstraints = false
    myFavouriteView.pinEdges(to: self.view)
  }
  
  /// function to dismiss search header view
  @objc func dismissSearchHeaderView() {
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewSearchListNotification),
                                    object: self,
                                    userInfo: nil)
    dismissView()
  }
  
  func showSafari(uri: String) {
      if let url = URL(string: uri) {
        if #available(iOS 11.0, *) {
          let config = SFSafariViewController.Configuration()
          config.entersReaderIfAvailable = false
          let vc = SFSafariViewController(url: url, configuration: config)
          vc.dismissButtonStyle = .close
          vc.toolbarItems = []
          present(vc, animated: true)
        } else {
          // Fallback on earlier versions
        }
      }
  }
}

extension MyFavouritesDetailsViewController: FavouriteProtocol {
  /// function to add favourite search history view
  /// - Parameters:
  ///   - xAxis: xAxis for search history view
  ///   - yAxis: yAxis for search history view
  ///   - width: width for search history view
  ///   - height: height for search history view
  ///   - mainStackView: superview for for search history view
  func addFavSearchHistory(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int, mainStackView: UIView) {
    addSearchListView(xAxis: xAxis + 6, yAxis: 66, width: width, height: height, mainStackView: mainStackView)
  }
}

extension MyFavouritesDetailsViewController: FavWeblinkViewProtocol {
  func openWebLink(uri: String) {
    showSafari(uri: uri)
  }
}
