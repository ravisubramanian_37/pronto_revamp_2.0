//
//  MyFavouritesViewModel.swift
//  Pronto
//
//  Created by Saranya on 24/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class MyFavouritesViewModel: NSObject {
    
    @objc static let sharedInstance = MyFavouritesViewModel()
    var audioViewController:UIViewController = UIViewController()
    
    func checkFavoSingletonAssetArray() {
        if let assetArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray {
            var index = 0
            for favAsset in assetArray {
                if (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getAllAsset(favAsset.asset_ID!) != nil {
                    index += 1
                    favAsset.priority = index as? NSNumber
                    continue
                } else {
                    ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.remove(at: index)
                }
            }
        }
    }
    
    /* Method to add/remove asset from favourites */
    func updateFavourites(selectedFavouriteAsset:Asset, isSelected:Bool) {
        //Update Asset in Local DB
        ZMCDManager.sharedInstance.updateAsset(selectedFavouriteAsset, success: {
            //Update Asset to server
            DispatchQueue(label: "save").async(execute: {
                AssetsServiceClass.sharedAPIManager().uploadEvents({
                    AbbvieLogging.logInfo(ProntoRedesignAPIConstants.updatingAssetSuccessMsg)
                }) { (error) in
                    AbbvieLogging.logError("\(ProntoRedesignAPIConstants.updatingAssetErrorMsg)\(error)")
                }
            })
        }) { (error) in
            AbbvieLogging.logError(ProntoRedesignAPIConstants.updatingAssetErrorMsgInLocalDB)
        }
        let favFranchise:NSArray = ZMCDManager.sharedInstance.getFavFranchiseById(selectedFavouriteAsset.assetID!) as NSArray
        if(isSelected){
            if favFranchise.count > 0{
                
            }else{
                var favDict:[AnyHashable : Any] = [:]
                favDict[ProntoRedesignAPIConstants.assetIDKeyForFav] = selectedFavouriteAsset.assetID
                favDict[ProntoRedesignAPIConstants.priorityKeyForFav] = (ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.count)! + 1
                ZMUserActions.sharedInstance()?.insertNewFavouriteFranchise(favDict, complete: {
                    AbbvieLogging.logInfo(ProntoRedesignAPIConstants.favInsertionInCoreDataSuccessMsg)
                    if let favArray = MyFavouritesServiceClass.sharedAPIManager().getAllFavouritesFromCoreData() as? [FavFranchise]{
                        ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray = []
                        let filteredFavArray = favArray.compactMap { data in
                          if data.asset_ID != 0 {
                            ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.append(data)
                            self.checkFavoSingletonAssetArray()
                          }
                        }
                        MyFavouritesServiceClass.sharedAPIManager().editFavorites(self.getLatestAssetList() as [Any], success: { (response: [AnyHashable:Any]) in
                            print(response)
                            // post a notification to refresh the views
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.favouritesAssetUpdationNotification), object: nil, userInfo: nil)
                        }) { (error) in
                            
                        }
                    }
                }, error: { (error) in
                    AbbvieLogging.logError(ProntoRedesignAPIConstants.favInsertionInCoreDataErrorMsg)
                })
            }
        }else{
            ZMUserActions.sharedInstance()?.delete(favFranchise.firstObject as? FavFranchise, complete: {
                AbbvieLogging.logInfo(ProntoRedesignAPIConstants.favDeletionInCoreDataSuccessMsg)
                if let favArray = MyFavouritesServiceClass.sharedAPIManager().getAllFavouritesFromCoreData() as? [FavFranchise]{
                    ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray = []
                    let filteredFavArray = favArray.compactMap { data in
                      if data.asset_ID != 0 {
                        ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.append(data)
                        self.checkFavoSingletonAssetArray()
                      }
                    }
                    MyFavouritesServiceClass.sharedAPIManager().editFavorites(self.getLatestAssetList() as [Any], success: { (response: [AnyHashable:Any]) in
                        // post a notification to refresh the views
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.favouritesAssetUpdationNotification), object: nil, userInfo: nil)
                    }) { (error) in
                        
                    }  
                }
            }, error: { (error) in
                AbbvieLogging.logError(ProntoRedesignAPIConstants.favDeletionInCoreDataErrorMsg)
            })
        }
      }
    /* Function to create favourite Objects */
    @objc func createFavObject(selectedAsset:Asset) {
        //Update Core Data
        MyFavouritesViewModel.sharedInstance.updateFavourites(selectedFavouriteAsset: selectedAsset, isSelected: true)
    }
    /* Method to get the updated asset list to services */
    func getLatestAssetList() -> [Dictionary<String, Any>]{
        var assetArray:[Dictionary<String, Any>] = []
        var index = 0
        while(index < ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.count ?? 0){
            
            if let favAsset = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?[index]{
                var dict:[String:Any] = [:]
                dict["id"] = favAsset.asset_ID
                dict["position"] = favAsset.priority
                assetArray.append(dict)
            }
            index += 1
        }
        return assetArray
    }
    /* Method to update priority */
    func updateFavouritesPriority(){
        var index = 0
        while(index < ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.count ?? 0){
            
            if let favAsset = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?[index]{
                favAsset.priority = NSNumber(integerLiteral: index + 1)
                ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?[index] = favAsset
            }
            
            index += 1
        }
    }
    /* Method to delete an obj in Favourites Array */
    @objc func deleteAssetFromFavourites(asset:Asset){
        //Update Core Data
        MyFavouritesViewModel.sharedInstance.updateFavourites(selectedFavouriteAsset:asset, isSelected: false)
    }
    /* Handle Opening of Asset */
    func openAsset(selectedAsset:Asset){
        
        ProntoAssetHandlingClass.sharedInstance().resetAssetValue()
        
        var trackingOptions: NSMutableDictionary = [:]

        if (ZMUserActions.sharedInstance().getFiltersForTracking() != nil) {
            trackingOptions.setValue(ZMUserActions.sharedInstance().getFiltersForTracking(), forKey: RedesignConstants.scfiltersubcategoryKey)
        }

        trackingOptions[RedesignConstants.scfilteractionKey] = RedesignConstants.clickThroughKey
        trackingOptions[RedesignConstants.sctierlevelKey] = RedesignConstants.tier2Key

        if selectedAsset.title!.count > 0 {
            trackingOptions[RedesignConstants.scsearchclickthroughfilenameKey] = selectedAsset.title
        }
        ZMTracking.trackSection(
                 ZMUserActions.sharedInstance().section,
                 withSubsection: RedesignConstants.filterKey,
                 withName: "brand-\(String(describing: ZMUserActions.sharedInstance().franchiseName))-default",
                withOptions: trackingOptions )
             
        trackingOptions = [:]
        trackingOptions[RedesignConstants.sccollateralfeaturesKey] = ""

        if selectedAsset.title!.count > 0 {
            trackingOptions[RedesignConstants.sccollateralnameKey] = selectedAsset.title
        }

        ZMTracking.trackSection(
                 ZMUserActions.sharedInstance().section,
                 withSubsection: RedesignConstants.assetViewKey,
                 withName: "\(String(describing: ZMAssetViewController.getAssetType(selectedAsset)))|\(String(describing: selectedAsset.title))",
                 withOptions: trackingOptions)
             if selectedAsset.title!.count > 0 {
                ProntoAssetHandlingClass.sharedInstance().validate(selectedAsset)
             }
       }
    /* Update asset after drag and drop */
    func updateAssetAfterDragAndDrop(){
        MyFavouritesViewModel.sharedInstance.updateFavouritesPriority()
        ZMUserActions.sharedInstance()?.editFavouriteFranchise(getLatestAssetList(), complete: {
            AbbvieLogging.logInfo(ProntoRedesignAPIConstants.favUpdationInCoreDataSuccessMsg)
            if let favArray = MyFavouritesServiceClass.sharedAPIManager().getAllFavouritesFromCoreData() as? [FavFranchise]{
                ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray = []
                ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.append(contentsOf: favArray)
                MyFavouritesServiceClass.sharedAPIManager().editFavorites(self.getLatestAssetList() as [Any], success: { (response: [AnyHashable:Any]) in
                    print(response)
                    
                }) { (error) in
                    
                }
            }
        }, error: { (error) in
            AbbvieLogging.logError(ProntoRedesignAPIConstants.favUpdationInCoreDataErrorMsg)
        })
    }
}
