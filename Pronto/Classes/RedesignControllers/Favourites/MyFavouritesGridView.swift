//
//  MyFavouritesGridView.swift
//  Pronto
//
//  Created by Saranya on 26/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
protocol FavGridViewProtocol {
  func openWeblink(uri: String)
}
class MyFavouritesGridView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDragDelegate, UICollectionViewDropDelegate {
    var collectionView: UICollectionView!
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    var parentController: MyFavouritesDetailsViewController?
    var isObjectAvailable = false
    var assetsArray: [FavFranchise]? = []
    var screenWidth = UIScreen.main.bounds.size
    var assetForAudio: Asset?
    var audioPlayerView: AudioPlayer?
    var weblinkDelegate: FavGridViewProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureMainView()
    }
    required init?(coder: NSCoder) {
        fatalError(RedesignConstants.commonInitError)
    }
    /* Set up data to load the Grid View of My Favourites */
    func setData(parentContlr: MyFavouritesDetailsViewController, objAvailable: Bool) {
        self.parentController = parentContlr
        self.isObjectAvailable = objAvailable
        sortFavAsset()
        //Register for Notification
        registerForNotification()
    }
    /* Sort data based on the Priority */
    func sortFavAsset() {
        MyFavouritesViewModel.sharedInstance.checkFavoSingletonAssetArray()
        self.assetsArray = []
        self.assetsArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.sorted(by: { $1.priority!.intValue > $0.priority!.intValue })
    }
    /* Configure Main View */
    func configureMainView() {
        collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: self.frame, collectionViewLayout: collectionViewFlowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        if #available(iOS 11.0, *) {
            collectionView.dragInteractionEnabled = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            collectionView.dragDelegate = self
            collectionView.dropDelegate = self
            
        } else {
            // Fallback on earlier versions
        }
        let collectioncell = UINib.init(nibName: RedesignConstants.collectionCellNibName, bundle: nil)
        collectionView.register(collectioncell, forCellWithReuseIdentifier: RedesignConstants.collectionCellId)
        collectionView.register(UINib.init(nibName: RedesignConstants.collectionGridAssetNibName, bundle: nil),
                                forCellWithReuseIdentifier: RedesignConstants.collectionGridAssetNibName)
        collectionView.backgroundColor = .white
        collectionView.isScrollEnabled = true
        self.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.pinEdges(to: collectionView)
        decideViewSpacing(isReload: false)
    }
    /*  Register for Notification */
    func registerForNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(handleOrientationChange),
                                               name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange),
                                               object: nil)
    }
    /* Method called to handle Orientation Change Notification */
    @objc func handleOrientationChange() {
       if(isObjectAvailable) {
           decideViewSpacing(isReload: true)
           collectionView.setNeedsLayout()
           collectionView.layoutIfNeeded()
       }
    }
    /* Caluclate spacing between cells in collection View */
    func decideViewSpacing(isReload: Bool) {
        var numberOfItemsPerRow: CGFloat = 4
        if((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft) || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)){
            numberOfItemsPerRow = 5
        }
        let spacingBetweenCells: CGFloat = 5
              
        let totalSpacing = (2 * 5) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        var screenSize:CGFloat = screenWidth.width
        if((UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft) || (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight)){
            screenSize = max(screenWidth.width, screenWidth.height)
        }
        else{
            screenSize = min(screenWidth.width, screenWidth.height)
        }
        var size :CGSize = CGSize(width: 0, height: 0)
        let width = (screenSize - totalSpacing)/numberOfItemsPerRow
        size =  CGSize(width: width, height: width)
            
        collectionViewFlowLayout.itemSize = CGSize(width: size.width, height: 200)
        collectionViewFlowLayout.minimumInteritemSpacing = 0
    }
    /* UICollectionView drag and drop delegates */
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let item = (self.assetsArray?[indexPath.row])!
        let itemProvider = NSItemProvider(object:(item.asset?.title! ?? "") as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = item
        return [dragItem]
    }
           
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard let destinationIndexPath = coordinator.destinationIndexPath else {
            return
        }
        coordinator.items.forEach { dropItem in
            guard let sourceIndexPath = dropItem.sourceIndexPath else {
                return
            }
            collectionView.performBatchUpdates({
                let assetTo = assetsArray?[sourceIndexPath.row]
                assetsArray?.remove(at: sourceIndexPath.row)
                assetsArray?.insert(assetTo!, at: destinationIndexPath.row)
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destinationIndexPath])
            }, completion: { _ in
                coordinator.drop(dropItem.dragItem,
                                     toItemAt: destinationIndexPath)
                ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray = self.assetsArray
                MyFavouritesViewModel.sharedInstance.updateAssetAfterDragAndDrop()
            })
        }
    }
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView,
                        dropSessionDidUpdate session: UIDropSession,
                        withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        return UICollectionViewDropProposal(operation: .move,intent: .insertAtDestinationIndexPath)
    }
    /* UICollectionView DataSource & Delegate Methods */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.assetsArray?.count ?? 0
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createAssetCell(index: indexPath)
    }
       
    func createAssetCell(index:IndexPath)-> UICollectionViewCell {
        let cell:ZMGridItem = collectionView.dequeueReusableCell(withReuseIdentifier: RedesignConstants.collectionGridAssetNibName,
                                                                 for: index) as! ZMGridItem

      //  if let _ = self.assetsArray?[index.row]{
            
        //if let asset = self.assetsArray?[index.row].asset {  Nandana Change
            if let asset = getAssetForIndexPath(indexpath: index.row) {
                cell.layer.borderColor = UIColor.lightGray.cgColor
                cell.layer.borderWidth = 1.0
                cell.redesignDotLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)
                cell.nameLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)
                cell.assetFrame.layer.borderColor = UIColor.gray.cgColor
                cell.assetFrame.layer.borderWidth = 0.25
            
                //Hide loader background
                cell.loaderBackgroundView.backgroundColor = UIColor.clear
                cell.loaderBackgroundView.alpha = 1
                cell.loaderBackgroundView.isHidden = true
                cell.audioPlayerView.isHidden = true

                cell.uid = asset.assetID?.intValue ?? 0
                cell.type = ZMGridItemTypeAsset

                cell.layer.shouldRasterize = true
                cell.layer.rasterizationScale = UIScreen.main.scale
                cell.nameLabel.text = asset.title
            
                cell.isVideo = asset.getType() == RedesignConstants.videoText
                cell.assetType = (asset.getType()) as NSString
                cell.thumbnailPath = asset.field_thumbnail as NSString?

                cell.reviews = Int32(asset.votes?.intValue ?? 0)
                cell.totalViews = Int32(asset.view_count?.intValue ?? 0)
                cell.totalDuration = asset.field_length
                cell.isPopularDocument = false
            
                cell.shareButton.isSelected = false
                cell.infoButton.isSelected = false
                cell.favButton.isSelected = true

                cell.shareButton.tag = index.row
                cell.favButton.tag = index.row
                cell.infoButton.tag = index.row

                cell.shareButton.addTarget(self, action: #selector(exportIconClicked(_:)), for: .touchUpInside)
                cell.favButton.addTarget(self, action: #selector(favIconClicked(_:)), for: .touchUpInside)
                cell.infoButton.addTarget(self, action: #selector(infoIconClicked(_:)), for: .touchUpInside)
                if let changetime = asset.changed , let createtime = asset.created {
                  let myTimeInterval = TimeInterval(changetime.doubleValue)
                  let mychgdate = Date(timeIntervalSince1970: myTimeInterval)
                  let mycreatInterval = TimeInterval(createtime.doubleValue)
                  let mycreatedate = Date(timeIntervalSince1970: mycreatInterval)
                  let calendar = Calendar.current
                  let numberOfDays = calendar.dateComponents([.day], from: mychgdate, to: Date())
                  let order = Calendar.current.compare(mychgdate, to: mycreatedate, toGranularity: .day)
                  if (order == .orderedSame && numberOfDays.day! + 1 <= 30) {
                    cell.updateLabel.isHidden = true
                  } else if (order == .orderedDescending && numberOfDays.day! + 1 <= 30) {
                    cell.isUpdateAvailable = true
                    cell.updateLabel.isHidden = false
                  } else {
                    cell.isUpdateAvailable = false
                    cell.updateLabel.isHidden = true
                  }
                }
                
                cell.isNew = asset.is_new
                cell.downloadButton.isHidden = true

                if !(asset.path == "") || (asset.path == RedesignConstants.brightCoveText) {
                    cell.read = true
                } else {
                    cell.read = false
                }
                let favfranchise = ZMCDManager.sharedInstance.getFavFranchise()
                cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
                cell.favButton.isSelected = false

                for fav in favfranchise {
                    guard let fav = fav as? FavFranchise else {
                        continue
                    }
                    if cell.uid == fav.asset_ID?.intValue {
                        cell.favButton.setImage(UIImage(named: RedesignConstants.favoSelected), for: .selected)
                        cell.favButton.isSelected = true
                        break
                    } else {
                        cell.favButton.setImage(UIImage(named: RedesignConstants.favoUnSelected), for: .selected)
                        cell.favButton.isSelected = false
                    }
                }
                if let audioAsset = ProntoRedesignSingletonClass.sharedInstance.audioAsset {
                    assetForAudio = audioAsset
                }
                if asset.assetID == assetForAudio?.assetID {
                    cell.audioPlayerView.isHidden = false
                    audioPlayerView?.removeFromSuperview()
                    if let audioView = ProntoRedesignSingletonClass.sharedInstance.audioAssetView {
                        audioPlayerView = audioView
                    }
                    audioPlayerView?.frame = CGRect(x: 0, y: 0, width: cell.audioPlayerView.frame.size.width, height: cell.audioPlayerView.frame.size.height)
                    if let audioView = audioPlayerView {
                        cell.audioPlayerView.addSubview(audioView)
                    }
                }
                centerImage(cell: cell, asset: asset)
            }
        //}
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.removeFavViewSearchListNotification),
                                      object: self, userInfo: nil)
        let cell = collectionView .cellForItem(at: indexPath) as? ZMGridItem
        if (cell != nil){
            if let asset = getAssetForIndexPath(indexpath: indexPath.row){
              let assetType = Constant.getFileType(for: asset)
              if assetType == ZMProntoFileTypesWeblink {
                self.weblinkDelegate?.openWeblink(uri: asset.uri_full ?? "")
              } else {
                openAsset(selectedAsset: asset, selectedGridCell: cell, selectedlistItem: nil)
              }
            }
        }
    }
    /* Get asset for index path */
    func getAssetForIndexPath(indexpath:Int)->Asset?{
        if let selAsset = self.assetsArray?[indexpath]{
            if selAsset.asset_ID != nil
            {
                return (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).getAllAsset(selAsset.asset_ID!)
            }            
        }
        return nil
    }
    /* Set center Image */
    func centerImage(cell: ZMGridItem, asset: Asset) {
        //Redesign center view
        var imageName: String = ""
        let assetType = Constant.getFileType(for: asset)
        
        var centerImageName: String = ""
        
        if (assetType == ZMProntoFileTypesPDF) {
            imageName = RedesignConstants.pdfName
            if(cell.thumbnail.image == nil) {
                centerImageName = RedesignConstants.centerImagePDF
            }
        } else if (assetType == ZMProntoFileTypesVideo) {
            imageName = RedesignConstants.videoName
            if(cell.thumbnail.image == nil){
                centerImageName = RedesignConstants.centerImageVideo
            }
        } else if (assetType == ZMProntoFileTypesBrightcove) {
            imageName = RedesignConstants.videoName
            if(cell.thumbnail.image == nil) {
                centerImageName = RedesignConstants.centerImageVideo
            }
        } else if (assetType == ZMProntoFileTypesDocument) {
            imageName = RedesignConstants.pdfName
            if(cell.thumbnail.image == nil) {
                centerImageName = RedesignConstants.centerImagePDF
            }
        } else if(assetType == ZMProntoFileTypesWeblink) {
            imageName = RedesignConstants.weblinkName
            if(cell.thumbnail.image == nil) {
                centerImageName = RedesignConstants.centerImageWeblink
            }
        } else if(assetType == ZMProntoFileTypesAudio) {
            imageName = RedesignConstants.centerImageAudio
            if(cell.thumbnail.image == nil){
                centerImageName = RedesignConstants.audioLinkName
            }
        }
        cell.redesignCenterImgView.image = UIImage(named: imageName)
        if(centerImageName.count > 0) {
            cell.assetBackGroundView.backgroundColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.lightGrayForAssetBackground)
            cell.centerTopAssetImage.image = UIImage(named: centerImageName)

        } else {
            cell.centerTopAssetImage.image = nil
            cell.assetBackGroundView.backgroundColor = .white
        }
    }
    /* Handling asset options - Share */
    @objc func exportIconClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.setImage(UIImage(named: RedesignConstants.export_redesign), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async {
                self.collectionView.reloadItems(at: [IndexPath(row: sender.tag, section: 0)])
            }
        } else {
            sender.setImage(UIImage(named: RedesignConstants.export_redesign_selected), for: .selected)
            sender.isSelected = true
            if let asset = getAssetForIndexPath(indexpath: sender.tag) {
                CategoryViewModel.sharedInstance.handleExportPopOver(sender: sender,
                                                                     parentCategoryGrid: nil,
                                                                     parentCategoryList: nil,
                                                                     parentFavouritesGrid: self,
                                                                     parentFavouritesList: nil,
                                                                     selectedAsset:asset)
            }
        }
    }
    /* Handling asset options - Info */
    @IBAction func infoIconClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.setImage(UIImage(named: RedesignConstants.info_redesign), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async {
                self.collectionView.reloadItems(at: [IndexPath(row: sender.tag, section: 0)])
            }

        } else {
            sender.setImage(UIImage(named:  RedesignConstants.info_redesign_selected), for: .selected)
            sender.isSelected = true
            if let asset = getAssetForIndexPath(indexpath: sender.tag){
                CategoryViewModel.sharedInstance.configureInfoPopOver(sender,
                                                                      parentCategoryGrid: nil,
                                                                      parentCategoryList: nil,
                                                                      parentFavouritesGrid: self,
                                                                      parentFavouritesList: nil,
                                                                      selectedAsset: asset)
            }
        }
    }
    /* Handle Opening of Asset */
    func openAsset(selectedAsset: Asset, selectedGridCell: ZMGridItem?, selectedlistItem: FranchiseTableViewCell?){

        CategoryViewModel.sharedInstance.openAsset(selectedAsset: selectedAsset,
                                                   selectedGridCell: selectedGridCell,
                                                   selectedlistItem: nil,
                                                   selectedCategoryListView: nil,
                                                   selectedCategoryGridView: nil,
                                                   selectedFavouritesListView: nil,
                                                   selectedFavouritesGridView: self)
    }
    /* Handle favo icon tapped Action */
     @IBAction func favIconClicked(_ sender: UIButton) {
        if let asset = getAssetForIndexPath(indexpath: sender.tag){
            MyFavouritesViewModel.sharedInstance.updateFavourites(selectedFavouriteAsset:asset, isSelected: false)
        }
        ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray?.remove(at: sender.tag)
        MyFavouritesViewModel.sharedInstance.updateFavouritesPriority()
        self.assetsArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray

        collectionView.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
        collectionView.removeFromSuperview()
        configureMainView()
         // post a notification to update favourites Count
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.favouritesRemovalNotification),
                                        object: nil, userInfo: nil)
    }
}

