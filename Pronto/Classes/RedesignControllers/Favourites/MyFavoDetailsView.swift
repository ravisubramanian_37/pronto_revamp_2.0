//
//  MyFavoDetailsView.swift
//  Pronto
//
//  Created by Saranya on 25/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
protocol FavouriteProtocol {
  func addFavSearchHistory(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int, mainStackView: UIView)
}

protocol FavWeblinkViewProtocol {
  func openWebLink(uri: String)
}

class MyFavoDetailsView: UIView {
    @IBOutlet weak var mainHeaderView: UIView!
    @IBOutlet weak var myFavouritesSubHeaderView: UIView!
    @IBOutlet weak var mainContentView: UIView!
    
    var listView: UIView!
    var collectionView: UIView!
    var assetsArray: [FavFranchise]? = []

    var parentController: MyFavouritesDetailsViewController?
    var subHeader: CategoryHeaderView!
    var isMoreCategoryView: Bool = false
    
    var favDelegate: FavouriteProtocol?
    var webLinkDelegate: FavWeblinkViewProtocol?
    @objc static let sharedInstance: MyFavoDetailsView = createSharedInstance()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
  class func createSharedInstance()-> MyFavoDetailsView{
      Bundle.main.loadNibNamed(RedesignConstants.myFavouritesDetailsViewNibName, owner: self, options: nil)?.first as! MyFavoDetailsView
  }
    func doAdditionalSetUp() {
        self.backgroundColor = RedesignThemesClass.sharedInstance.defaultThemeforUser
        assetsArray = ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray
        let headerView:HomeMainHeaderView = Bundle.main.loadNibNamed(RedesignConstants.mainViewHeaderNib, owner: self, options: nil)?.first as! HomeMainHeaderView
        headerView.delegate = self
        mainHeaderView.addSubview(headerView)
        headerView.setThemes()
        /* Add constraints for Categories View */
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.pinEdges(to: mainHeaderView)
        
        /* Load SubHeader */
        configureSubHeader()
        //Register for Notification
        registerForNotification()
        
        //Load main Content
         handleGridOrListChange()
        
        //Set Category Model Obj for Favourites
        ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen = true
    }
  /* configure SubHeader */
    func configureSubHeader(){
        subHeader = Bundle.main.loadNibNamed(RedesignConstants.categoryHeaderNib, owner: self, options: nil)?.first as? CategoryHeaderView
        subHeader.isFromFavourites = true
        subHeader.parentControllerForFavourites = self.parentController
        myFavouritesSubHeaderView.addSubview(subHeader)
        /* Add constraints for Categories header View */
        subHeader.translatesAutoresizingMaskIntoConstraints = false
        subHeader.pinEdges(to: myFavouritesSubHeaderView)
        subHeader.doAdditionalSetUp()
        
    }
  //Register for Notification
    func registerForNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleGridOrListChange), name: Notification.Name(rawValue: RedesignConstants.notificationforListAndGridChange), object: nil)
    }
    /* Method called to handle List or Grid Notification */
     @objc func handleGridOrListChange() {
        if(ProntoRedesignSingletonClass.sharedInstance.isListChosen){
           configureList()
        }
        else{
            configureGrid()
        }
    }
    /* Configure Grid View */
    func configureGrid(){
        if listView != nil{
            listView.removeFromSuperview()
        }
        let grid = MyFavouritesGridView()
        grid.weblinkDelegate = self
        grid.setData(parentContlr: parentController!, objAvailable: true)
        collectionView =  grid
        mainContentView.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        mainContentView.pinEdges(to: collectionView)
    }
    /* Configure List View */
    func configureList(){
        if collectionView != nil{
            collectionView.removeFromSuperview()
        }
        let list = MyFavouritesListView()
        list.weblinkListDelegate = self
        list.setData(parentContlr: parentController!, objAvailable: true)
        list.prepareView()
        listView = list
        mainContentView.addSubview(list)
        listView.translatesAutoresizingMaskIntoConstraints = false
        mainContentView.pinEdges(to: listView)
    }
}

extension MyFavoDetailsView: FavSearchHistoryProtocol {
  /// add search history view
    /// - Parameters:
    ///   - xAxis: xaxis for search history view
    ///   - yAxis: yaxis for search history view
    ///   - width: width for search history view
    ///   - height: height for search history view
    ///   - mainStackView: superview for search history view
  func addSearchListView(xAxis: CGFloat, yAxis: CGFloat, width: CGFloat, height: Int) {
    self.favDelegate?.addFavSearchHistory(xAxis: xAxis, yAxis: yAxis, width: width, height: height, mainStackView: self)
  }
}

extension MyFavoDetailsView: FavGridViewProtocol {
  func openWeblink(uri: String) {
    self.webLinkDelegate?.openWebLink(uri: uri)
  }
}

extension MyFavoDetailsView: FavListViewProtocol {
  func openWeblinkList(uri: String) {
    self.webLinkDelegate?.openWebLink(uri: uri)
  }
}

