//
//  MyFavouritesServiceClass.h
//  Pronto
//
//  Created by Saranya on 03/07/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoRedesignAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyFavouritesServiceClass : ProntoRedesignAPIManager
+ (MyFavouritesServiceClass *)sharedAPIManager;
- (void)fetchFavouritesHomeContent:(NSArray *)assetArray success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError;
-(NSArray *)getAllFavouritesFromCoreData;
- (void)editFavorites:(NSArray *)assetArray success:(void (^)(NSDictionary *))success error:(void (^)(NSError *))onError;
@end

NS_ASSUME_NONNULL_END
