//
//  MyFavouritesServiceClass.m
//  Pronto
//
//  Created by Saranya on 03/07/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "MyFavouritesServiceClass.h"
#import "Pronto-Swift.h"

@implementation MyFavouritesServiceClass
/* Singleton Object Creation */
+ (MyFavouritesServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static MyFavouritesServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
- (void)fetchFavouritesHomeContent:(NSArray *)assetArray success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        
        [super performBlock:^(CallbackBlock  _Nonnull callback) {
 
             NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.getFavoritesURL];

            NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
             
            AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:[NSDictionary new]];
            [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        callback(nil, responseObject);
                    }
                });
    
                ;
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchFavoritesErrorResponseMsg, error]];
                [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchFavoritesErrorResponseMsg responseObj:error];
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError * _Nonnull error, NSObject * _Nonnull response) {
            
            
            if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchFavoritesErrorResponseMsg, error.localizedDescription]];
                if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                }
                else
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
                
            }else
            {

                    [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchFavoritesResponseMsg responseObj:response];
                    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                        
                        NSArray *array = (NSArray *)response;
                        
                        /* Deleting Assets other than one coming from Response */
                        NSFetchRequest * fetchrequestFranchise = [FavFranchise MR_requestAll];
                        NSArray *importedFavourites = [FavFranchise MR_executeFetchRequest:fetchrequestFranchise];
                        
                        for (FavFranchise *fav in importedFavourites) {
                            NSPredicate *favFilterRemove = [NSPredicate predicateWithFormat:@"asset_ID == %@", fav.asset_ID];
                            NSUInteger index = [array indexOfObjectPassingTest:
                                         ^(id obj, NSUInteger idx, BOOL *stop) {
                                           return [favFilterRemove evaluateWithObject:obj];
                                         }];

                              if (index == NSNotFound) {
//                                return NO;
                                  [fav MR_deleteEntityInContext:localContext];
                              }
                              
                        }
                        
                        /* Updating Fav Assets and priorities */
                        for (int count=0;count < array.count;count++) {
                            
                            NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
                            NSMutableDictionary * favObject = [[NSMutableDictionary alloc]init];
                            favFranchiseObject = array[count];
                
                            if ( [[favFranchiseObject objectForKey:@"asset"]count] <= 0)
                            {
                                [favObject setObject:[favFranchiseObject objectForKey:@"id"] forKey:@"id"];
                                [favObject setObject:[favFranchiseObject objectForKey:@"position"] forKey:@"position"];
                                
                                NSPredicate *favFilter = [NSPredicate predicateWithFormat:@"asset_ID == %@", favObject[@"id"]];
                                
                                NSArray * favourites = [FavFranchise MR_findAllWithPredicate:favFilter inContext:localContext];
                                if(favourites.count == 0){
                                    FavFranchise * favFranchise = [FavFranchise MR_createEntityInContext:localContext];
                                    [favFranchise MR_importValuesForKeysWithObject:favObject];
                                    favFranchise.asset.assetID = [NSNumber numberWithInt:[[[favObject valueForKey:@"asset"] valueForKey:@"assetID"] intValue]];
                                }else{
                                    FavFranchise * favFranchise = favourites[0];
                                    favFranchise.asset_ID = favObject[@"id"];
                                    favFranchise.priority = favObject[@"position"];
                                }
                                
                            }
                            else
                            {
                                NSPredicate *favFilter = [NSPredicate predicateWithFormat:@"asset_ID == %@", favFranchiseObject[@"id"]];
                                
                                NSArray * favourites = [FavFranchise MR_findAllWithPredicate:favFilter inContext:localContext];
                                if(favourites.count == 0){
    
                                    FavFranchise * favFranchise = [FavFranchise MR_createEntityInContext:localContext];
                                    [favFranchise MR_importValuesForKeysWithObject:favFranchiseObject];
                                    favFranchise.asset.assetID = [NSNumber numberWithInt:[[[favFranchiseObject valueForKey:@"asset"] valueForKey:@"assetID"] intValue]];
                                }else{
                                    FavFranchise * favFranchise = favourites[0];
                                    favFranchise.asset_ID = favFranchiseObject[@"id"];
                                    favFranchise.priority = favFranchiseObject[@"position"];
                                }
                            }

                         
                        }
            \
                        
                    } completion:^(BOOL contextDidSave, NSError *error) {
            
                        if (error) {
                        }else{
                            NSFetchRequest * fetchrequestFranchise = [FavFranchise MR_requestAll];
                            NSArray *importedFavourites = [FavFranchise MR_executeFetchRequest:fetchrequestFranchise];
                            NSSortDescriptor *sortDescriptor;
                            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"priority"
                                                                           ascending:YES];
                            NSArray *sortedArray = [importedFavourites sortedArrayUsingDescriptors:@[sortDescriptor]];
                            success(sortedArray);
                        }
                    }];
                }
            }];
        }
    
}
-(NSArray *)getAllFavouritesFromCoreData{
    NSFetchRequest * fetchrequestFranchise = [FavFranchise MR_requestAll];
    NSArray *importedFavourites = [FavFranchise MR_executeFetchRequest:fetchrequestFranchise];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"priority"
                                                               ascending:YES];
    NSArray *sortedArray = [importedFavourites sortedArrayUsingDescriptors:@[sortDescriptor]];

    return sortedArray;
}
- (void)editFavorites:(NSArray *)assetArray success:(void (^)(NSDictionary *))success error:(void (^)(NSError *))onError{
        
        if ([AOFLoginManager sharedInstance].upi) {
      
            [super performBlock:^(CallbackBlock  _Nonnull callback) {
                
                 NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.editFavouritesURL];
                
                NSDictionary *params = @{@"token":[AOFLoginManager sharedInstance].getAccessToken,@"data":assetArray };
               
              [ProntoRedesignServerUtilityClass.sharedInstance printResponse:@"EDIT FAV PARAMS" responseObj:assetArray];
                 
                AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:[NSDictionary new]];
                [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (callback) {
                            callback(nil, responseObject);
                        }
                    });
        
                    ;
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                    [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.editOrDeleteFavoritesErrorResponseMsg, error]];
                    [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.editOrDeleteFavoritesErrorResponseMsg responseObj:error];
                }];
                
            } retryingNumberOfTimes:3 onCompletion:^(NSError * _Nonnull error, NSObject * _Nonnull response) {
                
                
                if (error) {
                    [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.editOrDeleteFavoritesErrorResponseMsg, error.localizedDescription]];
                    if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
                    {
                        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                    }
                    else
                    {
                        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                    }
                    
                }else
                {
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.editOrDeleteFavoritesResponseMsg ,response]];
                    [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.editOrDeleteFavoritesResponseMsg responseObj:response];

                    NSDictionary *result = @{ @"response" : response};
                    success(result);
                }
            }];
        }
    
}

@end
