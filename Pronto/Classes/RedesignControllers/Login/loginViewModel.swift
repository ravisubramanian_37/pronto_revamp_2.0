//
//  loginViewModel.swift
//  Pronto
//
//  Created by Saranya on 02/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class LoginViewModel: NSObject {
    
    @objc static var aofNavigationController: UINavigationController?

    /* function to invoke user login call */
     func invokeUserLogin(nav: UINavigationController?) {
        
      LoginViewModel.aofNavigationController = nav
        
        HomeViewServiceClass.sharedAPIManager().userLogin({ (token) in
            RedesignConstants.utilityVar.showBottomIndicatorView(false, withMessage: RedesignConstants.loginMsg)
            ProntoUserDefaults.singleton().setCurrentUserID(AOFLoginManager.sharedInstance.upi)
            self.afterLogin()
            (ProntoInitialLoadController.sharedInstance() as AnyObject).checkIfPreferencesViewIsRequired()


        }) { (error) in
            
            let dict: Bool = (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).isConnectionAvailableForYou()
            if (!dict) {
                (ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).showError(error)
            }
            let dictionary: NSMutableDictionary = NSMutableDictionary.init(dictionaryLiteral: (RedesignConstants.loginStatus,
                                                                                              RedesignConstants.unsuccessfulTxt))
            ZMTracking.trackSection(RedesignConstants.loginTitle, withSubsection: nil, withName: nil, withOptions: dictionary)
            //Retrying again
            self.invokeUserLogin(nav: nav)
        }
    }
    /* function to do initial setup */
    func prepareForInitialLoad() {
        UserDefaults.standard.set(true, forKey: RedesignConstants.isPreferenceRequired)
    }
    /*Function to setup things after login*/
    func afterLogin() {
        (ProntoInitialSetUpController.sharedInstance() as AnyObject).performInitialSetUp()
        
        ProntoRedesignSingletonClass.sharedInstance.isListChosen = false
        ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen = false
      ProntoRedesignSingletonClass.sharedInstance.savedCategoryIDs = []
        
        UserDefaults.standard.set(true, forKey: RedesignConstants.isLoadingFirstTime)
        // Sorting default key
        UserDefaults.standard.set(RedesignConstants.SortingOptions.recentlyAdded.rawValue, forKey: RedesignConstants.sortingTermKey)
        
        // Settings field user or not in UserActions
        if (AOFLoginManager.sharedInstance.salesFranchiseID.count > 0) {
          ZMUserActions.sharedInstance().isFieldUser = true;
        } else {
          ZMUserActions.sharedInstance().isFieldUser = false;
        }
        
        //load notifications
        NotificationsRedesign.sharedInstance().getNotifications()
    }
}
