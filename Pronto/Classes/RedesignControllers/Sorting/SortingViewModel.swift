//
//  SortingViewModel.swift
//  Pronto
//
//  Created by Saranya on 12/10/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class SortingViewModel: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    let sortingViewController: UIViewController = UIViewController()
    var isFromSearch: Bool = false
    var searchView: SearchView?
    var categoryHeaderView: CategoryHeaderView?
    var sortingArray: [Asset]?
    var customDataSource: UITableViewDataSource?
    /* Show Sorting PopOver */
    func showSortingPopOver(sender: Any) {
        let sortingTableView: UITableView = UITableView()
        sortingTableView.delegate = self
        sortingTableView.dataSource = self
        customDataSource = sortingTableView.dataSource
        sortingTableView.tableHeaderView = nil
        sortingTableView.tableFooterView = nil
        sortingTableView.isScrollEnabled = false
                
        sortingViewController.view.addSubview(sortingTableView)
        sortingViewController.view.superview?.layer.cornerRadius = 0
        sortingTableView.frame = sortingViewController.view.frame
                
        let popoverContent = sortingViewController
        popoverContent.modalPresentationStyle = .popover

        if let popover = popoverContent.popoverPresentationController {
            popover.backgroundColor = .white
            let viewForSource = sender as! UIView
            popover.sourceView = viewForSource
            popover.permittedArrowDirections = .init(rawValue: 0)
            popover.sourceRect = CGRect(x: viewForSource.bounds.minX + RedesignConstants.sortingTableBufferX,
                                        y: viewForSource.bounds.maxY + RedesignConstants.sortingTableBufferY,
                                        width: 0, height: 0)
                               
            popoverContent.preferredContentSize = CGSize(width: Int(viewForSource.bounds.width),
                                                         height: RedesignConstants.sortingTableHeight)
                        
            popover.delegate = self as? UIPopoverPresentationControllerDelegate
                RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: popoverContent, animated: true)
        }
    }
    /* Sorting Table View Delegates and Data Source */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RedesignConstants.numberOfRowsInSortingTable
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: RedesignConstants.cellId)

        if( !(cell != nil)){
            cell = UITableViewCell(style: .default, reuseIdentifier: RedesignConstants.cellId)
        }
                    
        cell!.textLabel?.text = getTextForIndexpath(index: indexPath)
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sortingKey = getTextForIndexpath(index: indexPath)
        if(isFromSearch){
            UserDefaults.standard.set(sortingKey, forKey: RedesignConstants.searchSortingTermKey)
            self.sortingArray = ProntoRedesignSingletonClass.sharedInstance.searchSortingAssetsArray

        }else{
            UserDefaults.standard.set(sortingKey, forKey: RedesignConstants.sortingTermKey)
            self.sortingArray = ProntoRedesignSingletonClass.sharedInstance.sortingsasetsArray

        }
        if let assetArray = self.sortingArray{
            
            self.sortingArray = RedesignUtilityClass.sharedInstance.sortcategoryArray(isFromSearch: isFromSearch,assetsArray: assetArray) as? [Asset]
          
            if(isFromSearch){
                let rawDataDict:[String: [Asset]?] = [RedesignConstants.searchSortingTermKey: self.sortingArray]
                //post a notification
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.searchSortingReloadNotification), object: nil, userInfo: rawDataDict as [AnyHashable : Any])
            }else{
                let rawDataDict:[String: [Asset]?] = [RedesignConstants.sortingTermKey: self.sortingArray]
                //post a notification
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.sortingReloadNotification), object: nil, userInfo: rawDataDict as [AnyHashable : Any])
            }
                        
        }
                    
        sortingViewController.dismiss(animated: true, completion:nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Double(RedesignConstants.sortingTableHeight)/3.0)
    }
    /* Method to retrieve cell text */
    func getTextForIndexpath(index:IndexPath)-> String{
        var labelText:String = ""
        switch index.row {
            case 0:
                labelText = RedesignConstants.SortingOptions.recentlyAdded.rawValue
            case 1:
                labelText = RedesignConstants.SortingOptions.mostViewed.rawValue
            default:
                labelText = RedesignConstants.SortingOptions.recentlyUpdated.rawValue
            }
        return labelText
    }  
}
