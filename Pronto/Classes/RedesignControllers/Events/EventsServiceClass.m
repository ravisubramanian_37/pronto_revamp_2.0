//
//  EventsServiceClass.m
//  Pronto
//
//  Created by Saranya on 02/09/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "EventsServiceClass.h"
#import "EventListViewModel.h"
#import "Pronto-Swift.h"
#import "EventCoreDataManager.h"
#import "UIImage+ImageDirectory.h"
@implementation EventsServiceClass
/* Singleton Object Creation */
+ (EventsServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static EventsServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}

- (void)syncAllEvents:(void(^)(NSDictionary *allEvents))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi)
    {
        NSString *lastUpdate = [[ProntoUserDefaults userDefaults] getCMSLastSynchronizationForEvents];
        
        if ([lastUpdate isEqualToString:@"(null)"] || !lastUpdate || lastUpdate == nil || lastUpdate.length <= 0) {
            lastUpdate = @"0";
        }

        NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
                                       @"date":lastUpdate,
                                       @"per_page":@"50",
                                       @"page":[NSString stringWithFormat:@"0"]};
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"eventsAll params:%@",params]];
        
        NSDictionary *headers = @{@"1.1":@"services_asset_getvocabulary_version", @"keep-alive":@"Connection"};
        
        NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.eventsAllUrl];
        
        [self performBlock:^(CallbackBlock callback) {
            
            AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:headers];
            NSOperationQueue *operationQueue = manager.operationQueue;
            
            [manager.reachabilityManager startMonitoring];
            [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status)]];
                switch (status) {
                    case AFNetworkReachabilityStatusReachableViaWWAN:
                    case AFNetworkReachabilityStatusReachableViaWiFi:
                        [operationQueue setSuspended:NO];
                        break;
                    case AFNetworkReachabilityStatusNotReachable:
                    default:
                        [operationQueue setSuspended:YES];
                        break;
                }
            }];
            
            [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"resp of eventsAll.uri:%@",responseObject]];
                        
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                    callback(errorCode, nil);
                }
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {
                
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS eventsAll.uri", (long)error.code, error.localizedDescription]];
                
                onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
            }else{
                
                NSDictionary *allEvents = (NSDictionary *)response;
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"eventsAll:%@",allEvents]];
                complete(allEvents);
            }
        }];
    }
}
-(void)invokeEventsAllServiceCall:(void(^)(NSDictionary *allEvents))complete error:(void(^)(NSError *error))onError{
    
    [self getEvents:^(NSDictionary *allEvents) {
    
        if (allEvents != nil &&
            allEvents.count > 0 &&
            [allEvents isKindOfClass:[NSDictionary class]])
        {
            [[EventCoreDataManager sharedInstance] saveAllEvents:allEvents withCompletionHandler:^(BOOL success, NSArray *events) {
                
                if (success == YES && events != nil)
                {
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"All Events data %@", events]];
                }
                
                //Remove Bottom indicator every operation completes.
                [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
                if (complete) {
                    complete(allEvents);
                }
            }];
        }
        else
        {
            NSDictionary *userInfo = nil;
           userInfo = @{NSURLErrorFailingURLErrorKey : @"No events found."};
            NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCancelled userInfo:userInfo];
            onError(error);
           
        }
        } error:^(NSError *error) {
   
            onError(error);
        }];
}

/* New implementation meta data service call for display the tile details alone. Call no:1 for Events */
- (void)getEvents:(void(^)(NSDictionary *eventsFetched))complete error:(void(^)(NSError *error))onError
{
    NSDate *date = [NSDate date];
    if ([AOFLoginManager sharedInstance].upi != nil)
    {
        [self performBlock:^(CallbackBlock callback) {

                NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};

                NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.getEventsMetaDataUrl];

                [AbbvieLogging logInfo:[NSString stringWithFormat:@"events url:%@",url]];
                AFHTTPRequestOperationManager *manager = [self requestManagerWithToken:YES headers:[NSDictionary new]];
                [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {

                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (callback)
                        {
                            if ([responseObject count] > 0)
                            {
                                NSInteger timeStamp = [date timeIntervalSinceNow];

                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@:%@ and took time  %ld",ProntoRedesignAPIConstants.fetchEventsMetaDataSuccessMsg,responseObject, (long)(-timeStamp)]];
                                callback(nil, responseObject);
                            }
                            else{
                                callback(nil,responseObject);
                            }
                            
                        }
                    });

                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

                    if (callback)
                    {
                        [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchEventsMetaDataErrorMsg, error]];
                        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchEventsMetaDataErrorMsg responseObj:error];
                        callback(error, nil);
                    }
                }];
             
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error)
            {
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchEventsMetaDataErrorMsg, error.localizedDescription]];
                if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                }
                else
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
            }
            else
            {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchEventsMetaDataSuccessMsg ,response]];
                [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchEventsMetaDataSuccessMsg responseObj:response];
                    NSDictionary *events = (NSDictionary *)response;
                     complete(events);
               
            }
        }];
    }
}
#pragma mark - Helper Methods
-(NSArray *)getAllEvents{
    NSFetchRequest *fetchRequest = [Event MR_requestAll];
    NSArray *events = [Event MR_executeFetchRequest:fetchRequest];
    return events;
}

-(Event *)getEventsbyId:(NSString *)idvalue{
        
    NSFetchRequest *fetchRequest = [Event MR_requestAll];
    NSArray *events = [Event MR_executeFetchRequest:fetchRequest];
    
    for (Event *event in events){
        if(event.eventId == idvalue){
            return event;
        }
    }
    return nil;
}

- (NSString*)getMonthOfStartDate:(NSString *)date
{
    NSDate *startDate = [NSDate GetDateSameAsStringFromDateString:date];

    NSString *monthOfStartDate = [NSDate GetMonthInMMMMFormateFromDate:startDate];

    return monthOfStartDate;
}
- (NSString *)getEventsHomeMonthString:(NSString *)startDate endDate:(NSString *)endDate{
       
    NSDate *sDate = [NSDate GetDateSameAsStringFromDateString:startDate];
    NSDate *eDate   = [NSDate GetDateSameAsStringFromDateString:endDate];
        
    NSString *monthOfStartDate = [NSDate GetMonthInMMMMFormateFromDate:sDate];
    NSString *monthOfEndDate   = [NSDate GetMonthInMMMMFormateFromDate:eDate];

    if([monthOfStartDate isEqualToString:monthOfEndDate]){
        return monthOfStartDate;
    }else{
        NSString *monthOfStartDate = [NSDate GetMonthInMMMFormateFromDate:sDate];
        NSString *monthOfEndDate   = [NSDate GetMonthInMMMFormateFromDate:eDate];
        NSString *formattedEventRangeString = [NSString stringWithFormat:@"%@    %@", monthOfStartDate, monthOfEndDate];
        return formattedEventRangeString;
    }
        
}
- (NSString *)getEventsDateString:(NSString *)date {
       
    NSDate *sDate = [NSDate GetDateSameAsStringFromDateString:date];
    NSString *getDate  = [NSString stringWithFormat:@"%ld",(long) [NSDate GetDateInUTCFromDate:sDate]];
    return getDate;
}
- (NSString *)getEventsHomeDateString:(NSString *)startDate endDate:(NSString *)endDate{
    NSDate *sDate = [NSDate GetDateSameAsStringFromDateString:startDate];
    NSDate *eDate   = [NSDate GetDateSameAsStringFromDateString:endDate];
        
    NSString *dateOfStartDate  = [NSString stringWithFormat:@"%ld",(long) [NSDate GetDateInUTCFromDate:sDate]];
    NSString *dateOfEndDate    = [NSString stringWithFormat:@"%ld", (long)[NSDate GetDateInUTCFromDate:eDate]];
        
    NSString *formattedEventRangeString = [NSString stringWithFormat:@"%@ - %@", dateOfStartDate, dateOfEndDate];
    
    return formattedEventRangeString;
}

- (void)invokeEventsServiceForHomeScreen:(void(^)(NSDictionary *allEvents))complete error:(void(^)(NSError *error))onError{
    [self getEvents:^(NSDictionary *eventsFetched) {
       
        if (eventsFetched != nil &&
            eventsFetched.count > 0 &&
            [eventsFetched isKindOfClass:[NSDictionary class]])
        {
            //success
            [[EventCoreDataManager sharedInstance] saveEvents:eventsFetched withCompletionHandler:^(BOOL success, NSArray *events) {
                
                if (success)
                {

                }
                else
                {
                    
                }
                complete(eventsFetched);
            }];
        }
        else
        {
            //if last update is zero and events count is zero. it means no events available at this moment.
            NSString   *lastUpdate = [[ProntoUserDefaults userDefaults] getCMSLastSynchronizationForEvents];
            if (lastUpdate.integerValue <= 0 &&
                [[eventsFetched objectForKey:@"total"] integerValue] <= 0)
            {
                NSError *error = [NSError errorWithDomain:nil code:nil userInfo:@"No events found."];
                onError(error);
            }
            
        }
        
       
    } error:^(NSError *error) {
        
        [AbbvieLogging logError:[NSString stringWithFormat:@"getEvents error:%@", error]];
        onError(error);
        
        
       
    }];
}



// Service call for getting event details
- (void)getEventDetails:(NSString*)eventId complete:(void(^)(NSDictionary *sessionDetails))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi)
    {
        [self performBlock:^(CallbackBlock callback) {
            
            if([AOFLoginManager sharedInstance].upi != nil)
            {
                
                NSDictionary *params = @{kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
                
                NSString *url = [NSString stringWithFormat:@"%@/%@",[ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.getEventDetailsUrl],eventId];
                
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"sessions url:%@",url]];
                
                [[self requestManagerWithToken:NO headers:[NSDictionary new]] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        if (callback)
                        {
                            callback(nil, responseObject);
                        }
                    });
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (callback)
                    {
                        [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchEventDetailsErrorMsg, error]];
                        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchEventDetailsErrorMsg responseObj:error];
                        callback(error, nil);
                    }
                }];
            }
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchEventDetailsErrorMsg, error.localizedDescription]];
                if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                }
                else
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
            }
            else
            {
                NSDictionary *sessions = (NSDictionary *)response;
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchEventDetailsSuccessMsg ,sessions]];
                [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchEventDetailsSuccessMsg responseObj:sessions];
                
                complete(sessions);
            }
        }];
    }
}
/* Service to fetch viewed asset in Events */
// fetchViewed - fetching the already visited assets
- (void)fetchViewed:(void(^)(NSDictionary *assetDetailsFetched))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi != nil)
    {
        [self performBlock:^(CallbackBlock callback) {
        
            //changes made for Token
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            
            NSString *url = [NSString stringWithFormat:@"%@?token=%@",[ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.fetchViewedEventsAssetsUrl],[AOFLoginManager sharedInstance].getAccessToken];
            
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"viewed assets url:%@",url]];
            
                [[self requestManagerWithToken:NO headers:[NSDictionary new]] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (callback)
                        {
                            [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchViewedAssetSuccessMsg ,responseObject]];
                            callback(nil, responseObject);
                        }
                    });
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (callback)
                    {
                        [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchViewedAssetErrorMsg, error]];
                        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.fetchViewedAssetErrorMsg responseObj:error];
                        callback(error, nil);
                    }
                }];
            //}
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error)
            {
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchViewedAssetErrorMsg, error.localizedDescription]];
                if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                }
                else
                {
                    onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                }
            }
            else
            {
                NSDictionary *assetsViewed = (NSDictionary *)response;
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchViewedAssetSuccessMsg ,assetsViewed]];
                complete(assetsViewed);
            }
        }];
    }
}
/* Service to mark any visited asset as read in Events*/
- (void)saveViewed:(NSString*) eventId session:(NSString*) sessionId asset:(NSString*) assetId type:(NSString*)assetType complete:(void(^)(NSDictionary *savedAssetDetails))complete error:(void(^)(NSError *error))onError
{
    if ([AOFLoginManager sharedInstance].upi)
    {

        NSDictionary *params = @{@"nid":assetId,@"type":assetType,kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
        NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.saveViewedEventsAssetsUrl];
        
        
        [self performBlock:^(CallbackBlock callback) {
            
            [[self requestManagerWithToken:NO headers:[NSDictionary new]] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback)
                    {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.saveViewedAssetSuccessMsg ,responseObject]];
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                        [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.saveViewedAssetErrorMsg, error]];
                        [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.saveViewedAssetErrorMsg responseObj:error];
                        callback(error, nil);
                    }
                });
            }];
            
        }
     retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
         
         if (error)
         {
             [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.saveViewedAssetErrorMsg, error.localizedDescription]];
             if ([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code])
             {
                 onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
             }
             else
             {
                 onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
             }
         }
         else
         {
             
             [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.saveViewedAssetSuccessMsg ,response]];
             complete((NSDictionary *)response);
         }
     }];
    }
}
- (void)downloadCoverflowImage:(NSString *)path complete:(void(^)(UIImage *image, NSString *p))complete fromCache:(BOOL)cache {
    
    __block UIImage *img;
    
    NSString *basePath = [NSString stringWithFormat:@"%@/%@/",AOFLoginManager.sharedInstance.cachesDir, ProntoEventsCoverflowImageFolderName];
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"basePath:%@",basePath]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:basePath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:basePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *filPath = [NSString stringWithFormat:@"%@%@", basePath, [path lastPathComponent]];
    if (cache && [[NSFileManager defaultManager] fileExistsAtPath:filPath]) {
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filPath])
        {
            img = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:filPath]];
        }
        
        complete(img, path);
        
    }
    else
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSURL *url = [NSURL URLWithString:path];
            
            
            [self downloadFileFromURL:url toFilePath:filPath completionHandler:^(NSString *responsePath, NSError *error) {
                
                if (error) {
                    
                    [AbbvieLogging logError:[NSString stringWithFormat:@"*ERROR: Thumbnail couldn't be downloaded %@", error.localizedDescription]];
                }
                else {
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filPath]) {
                        img = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:filPath]];
                    }
                }
                
                complete(img, path);
            }];
        });
    }
}
- (void)downloadThumbnail:(NSString *)path complete:(void(^)(UIImage *thumbnail, NSString *p))complete fromCache:(BOOL)cache includeWaterMark:(NSString *)waterMark {
    
    __block UIImage *thumb;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage GetOverlayIconImage]];
    imageView.alpha = 0.5; //Alpha runs from 0.0 to 1.0
    
    NSString *basePath = [NSString stringWithFormat:@"%@/com.abbvie.grid.thumbnails/",AOFLoginManager.sharedInstance.cachesDir];
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"basePath:%@",basePath]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:basePath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:basePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    //@ABT-138 Fix to force the reload of all thumbnails. Added extra 2 character. Not recommended.
    NSString *filPath = [NSString stringWithFormat:@"%@%@2", basePath, [path lastPathComponent]];
    if (cache && [[NSFileManager defaultManager] fileExistsAtPath:filPath]) {
        
        //Verify if there is a cached version of the thumbnail
        if ([[NSFileManager defaultManager] fileExistsAtPath:filPath]) {
            thumb = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:filPath]];

        }
        
        complete(thumb, path);
        
    }
    else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            NSURL *url = [NSURL URLWithString:path];
            
            [self downloadFileFromURL:url toFilePath:filPath completionHandler:^(NSString *responsePath, NSError *error) {
                if (error) {
                    
                    [AbbvieLogging logError:[NSString stringWithFormat:@"*ERROR: Thumbnail couldn't be downloaded %@", error.localizedDescription]];
                }
                else {
                    if ([[NSFileManager defaultManager] fileExistsAtPath:filPath]) {
                        thumb = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:filPath]];

                    }
                }
                complete(thumb, path);
            }];
        });
    }
}
-(void) downloadEventOnlyAsset:(EventOnlyAsset *)asset
{
    __weak EventsServiceClass *weakSelf = self;
    [weakSelf initialDownloadValidateForEventOnlyAsset:asset completion:^(BOOL assetNeedsDownload)
     {
         if(assetNeedsDownload == YES)
         {
             NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
             //   Download in background
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                 
                 [weakSelf downloadEventOnlyAsset:asset withProgress:^(CGFloat progress) {
                   
                 } completion:^{
                     if(asset.eoa_title != nil)
                     {
                         if(asset.eoa_id != nil)
                         {
                             NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[NSNumber numberWithInteger:[asset.eoa_id integerValue]]];

                             [weakSelf initialDownloadValidateForEventOnlyAsset:asset completion:^(BOOL assetNeedsDownload){
                                 NSString *status = keyCompleted;
                                 if(assetNeedsDownload == YES)
                                 {
                                     status = keyFailed;
                                     [AbbvieLogging logInfo:[NSString stringWithFormat:@"Fail completion updated for: %@ %@",asset.eoa_id,asset.eoa_title]];
                                 }
                                 else
                                 {
                                     [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download Complete: %@ %@, size is %@",asset.eoa_id,asset.eoa_title,asset.eoa_file_size]];
                                 }
                                 
                                 if (offlineData.count > 0)
                                 {
                                     //if  insertation and deletetion has to be happen, it will happen one by one
                                     OfflineDownload *offlineAsset = (OfflineDownload*)[offlineData lastObject];
                                     offlineAsset.status = status;
                                     [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:^(BOOL success) {
                                         [self broadcastingToProviderForEventOnlyAsset:asset];
                                     }];
                                 }
                                 else
                                 {
                                     [initialDownloadAsset setObject:[NSNumber numberWithInteger:[asset.eoa_id integerValue]] forKey:@"assetID"];
                                     [initialDownloadAsset setObject:status forKey:@"status"];
                                     [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                         [self broadcastingToProviderForEventOnlyAsset:asset];
                                     }];
                                 }
                                 
                             }failure:^(NSString *messageFailure) {
                                 [AbbvieLogging logError:[NSString stringWithFormat:@"Updating in CD failed: %@ %@",asset.eoa_id,asset.eoa_title]];
                             }];
                         }
                     }
                 } onError:^(NSError *error) {
                     
                     if(asset.eoa_title != nil)
                     {
                         [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download Failed: %@ %@",asset.eoa_id,asset.eoa_title]];
                         
                         if(asset.eoa_id != nil)
                         {
                             NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[NSNumber numberWithInteger:[asset.eoa_id integerValue]]];
                             
                             if (offlineData.count > 0)
                             {
                                 //if  insertation and deletetion has to be happen, it will happen one by one
                                 OfflineDownload *offlineAsset = (OfflineDownload*)[offlineData lastObject];
                                 offlineAsset.status = keyFailed;
                                 [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:^(BOOL success) {
                                     [self broadcastingToProviderForEventOnlyAsset:asset];
                                 }];
                             }
                             else
                             {
                                 [initialDownloadAsset setObject:[NSNumber numberWithInteger:[asset.eoa_id integerValue]] forKey:@"assetID"];
                                 [initialDownloadAsset setObject:keyFailed forKey:@"status"];
                                 [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                     [self broadcastingToProviderForEventOnlyAsset:asset];
                                 }];
                             }
                         }
                     }
                 }];
             });
         }
         else
         {
             //updating core data
             
             NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[NSNumber numberWithInteger:[asset.eoa_id integerValue]]];
             
             if (offlineData.count > 0)
             {
                 //if  insertation and deletetion has to be happen, it will happen one by one
                 OfflineDownload *offlineAsset = (OfflineDownload*)[offlineData lastObject];
                 offlineAsset.status = keyCompleted;
                 [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:^(BOOL success) {
                     [self broadcastingToProviderForEventOnlyAsset:asset];
                 }];
             }
             else
             {
                 NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
                 [initialDownloadAsset setObject:[NSNumber numberWithInteger:[asset.eoa_id integerValue]] forKey:@"assetID"];
                 [initialDownloadAsset setObject:keyCompleted forKey:@"status"];
                 [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                     [self broadcastingToProviderForEventOnlyAsset:asset];
                 }];
             }
             
         }
     }failure:^(NSString *messageFailure) {
     }];
}
-(void) downloadFileFromURL:(NSURL *)url toFilePath:(NSString *)filepath completionHandler:(void(^)(NSString *responsePath, NSError *error)) completionHandler
{
    [super performBlock:^(CallbackBlock callback) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
        [request setTimeoutInterval:60];
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
            if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
            {
                [request setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
            else{
            [request setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:filepath append:NO]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
        }];
        
        [operation.outputStream open];
        [operation start];
        
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
        
        if (error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"ERR: %@", [error description]]];
            completionHandler(@"",error);
        } else {
            completionHandler(@"",nil);
        }
    }];
}
- (void)broadcastingToProviderForEventOnlyAsset:(EventOnlyAsset*)asset
{
    if(asset != nil && asset.eoa_id != nil)
    {
        NSDictionary* userInfo = @{@"asset": asset};
        [[NSNotificationCenter defaultCenter]postNotificationName:kEventOnlyAssetDownloadProviderCompletionNotification object:nil userInfo:userInfo];
    }
}
//EventOnlyAssets validate before the initial download
-(void)initialDownloadValidateForEventOnlyAsset:(EventOnlyAsset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.eoa_path error:nil] fileSize];
    NSString * uri = asset.eoa_uri_full;
    NSString * file_mime = asset.eoa_file_mime;
    if (![UIApplication sharedApplication].protectedDataAvailable)
    {
        [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
        /// This should occur during application:didFinishLaunchingWithOptions:
        //AOFKit Implementation
        NSError *error;
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.eoa_path error:&error];
        
        
        if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
        {
            attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.eoa_path error:&error];
            if (!success)
                [AbbvieLogging logError:@"Set ~/Documents attrsAssetPath NOT successfull"];
        }
        
        /// ....
        NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
        [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.eoa_path error:nil];
        [AbbvieLogging logError:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
    }
    
    // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
    if (![[NSFileManager defaultManager] fileExistsAtPath:asset.eoa_path] && [uri rangeOfString:@"brightcove://"].location == 0 && ![file_mime isEqualToString:@"weblink" ])
    {
        if ([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
        {
            completion(YES);
        }
    }
    else
    {
        //Check if the asset size matches the metada info meaning the asset is correct
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"** size Asset %d size %d", asset.eoa_file_size.intValue, (int)fileSize]];
        if (asset.eoa_file_size.integerValue == (NSInteger)fileSize)
        {
            //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
            completion(NO);
        }
        else
        {
            completion(YES);
        }
    }
}
- (void) downloadEventOnlyAsset: (EventOnlyAsset *) eventOnlyAsset
          withProgress:(void (^)(CGFloat progress))progressBlock
            completion:(void (^)(void)) completionBlock
               onError:(void (^)(NSError *error))errorBlock
{
    NSString *baseDir = AOFLoginManager.sharedInstance.documentsDir;
    NSString *filename = [[AssetsServiceClass sharedAPIManager] MD5HashForString:eventOnlyAsset.eoa_uri_full];
    NSString *extension = [[eventOnlyAsset.eoa_uri_full componentsSeparatedByString:@"."] lastObject];
    NSString *dest = [[NSString alloc] initWithFormat:@"%@/%@.%@", baseDir, filename, extension];
    
    NSURL *assetURL;
    // for testing in staging
    if(nil != eventOnlyAsset.eoa_uri_full && ([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame))
    {
        assetURL = [[NSURL alloc] initWithString:eventOnlyAsset.eoa_uri_full];
    }
    else
    {
        assetURL = [[NSURL alloc] initWithString:@"https://stage.dlpronto.com/system/files/assets/URL-Text-Feb19.pdf"];
        extension = @"pdf";
    }
    
    // PRONTO-29 - Fix Crashes - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: sccollateralname)'
    if([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame)
    {
        eventOnlyAsset.eoa_path = dest;
    }
    
    //    __block NSString * range = [[NSString alloc]init];
    
    /// This should occur during application:didFinishLaunchingWithOptions: [ASFFileProtectionManager stopProtectingLocation:path];
    NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
    [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:dest error:nil];
    
    
    [self performBlock:^(CallbackBlock callback) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:assetURL];
        [request setTimeoutInterval:60];
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
            if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
            {
                [request setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
            else{
            [request setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:dest append:NO]];
        
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            CGFloat written = totalBytesRead;
            CGFloat total = totalBytesExpectedToRead;
            CGFloat percentageCompleted = written/total;
            progressBlock(percentageCompleted);
        }];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
        }];
        
        [operation.outputStream open];
        [operation start];
        
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
        
        if (error)
        {
            [AbbvieLogging logError:[NSString stringWithFormat:@"ERR: %@", [error description]]];
            errorBlock(error);
        }
        else
        {
            if (dest)
            {
                if (eventOnlyAsset)
                {
                    [self initialDownloadValidateForEventOnlyAsset:eventOnlyAsset completion:^(BOOL assetNeedsDownload){
                        if(!assetNeedsDownload)
                        {
                            [eventOnlyAsset updateEventOnlyAssetPath:dest withCompletionHandler:^(BOOL success) {
                                 completionBlock();
                            }];
                        }
                        else
                        {
                            errorBlock(error);
                        }
                    }failure:^(NSString *messageFailure) {
                        errorBlock(error);
                    }];
                }
            }
        }
    }];
    
}
//Download call for assets
-(void) downloadAssetFor:(Asset *)asset {
    
    __weak AssetsServiceClass *weakSelf = [AssetsServiceClass sharedAPIManager];
    [weakSelf initialDownloadValidate:asset completion:^(BOOL assetNeedsDownload)
    {
        if(assetNeedsDownload == YES)
        {
            NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
            //   Download in background
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [self downloadAsset:asset completion:^{
                    if([asset title] != nil){
                        
                        if([asset valueForKey:@"assetID"] != nil) {
                            [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                            [initialDownloadAsset setObject:@"completed" forKey:@"status"];
                            NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
                        
                            if(weakSelf.initialDownloadAssetArray.count > 0) {
                                for(NSDictionary *dict in [weakSelf.initialDownloadAssetArray copy])
                                {
                                    if([dict objectForKey:@"assetID"] == [asset valueForKey:@"assetID"]) {
                                        
                                        [weakSelf.initialDownloadAssetArray replaceObjectAtIndex:[weakSelf.initialDownloadAssetArray indexOfObject:dict] withObject:initialDownloadAsset];
                                        
                                    }
                                }
                            }
                            
                            [weakSelf initialDownloadValidate:asset completion:^(BOOL assetNeedsDownload){
                                if(assetNeedsDownload == YES) {
                                    [initialDownloadAsset setObject:@"failed" forKey:@"status"];
                                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Fail completion updated for: %@ %@",[asset valueForKey:@"assetID"],[asset title]]];
                                }
                                else
                                {
                                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download Complete: %@ %@, size is %@",[asset valueForKey:@"assetID"],[asset title],[asset file_size]]];
                                }
                                
                                if (offlineData.count > 0)
                                {
                                    //if  insertation and deletetion has to be happen, it will happen one by one
                                    [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                                        
                                        [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                             [self broadcastingToProvider:asset];
                                        }];
                                    }];
                                }
                                else
                                {
                                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                         [self broadcastingToProvider:asset];
                                    }];
                                }
                                
                            }failure:^(NSString *messageFailure) {
                                [AbbvieLogging logError:[NSString stringWithFormat:@"Updating in CD failed: %@ %@",[asset valueForKey:@"assetID"],[asset title]]];
                            }];
                        }
                    }
                }
                onError:^(NSError *error)  {

                     if([asset title] != nil)
                     {
                         [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download Failed: %@ %@",[asset valueForKey:@"assetID"],[asset title]]];
                         
                         if([asset valueForKey:@"assetID"] != nil) {
                             [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                             [initialDownloadAsset setObject:@"failed" forKey:@"status"];
                             NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
                             
                             if(weakSelf.initialDownloadAssetArray.count > 0) {
                                 for(NSDictionary *dict in [weakSelf.initialDownloadAssetArray copy])
                                 {
                                     if([dict objectForKey:@"assetID"] == [asset valueForKey:@"assetID"]) {
                                         
                                         [weakSelf.initialDownloadAssetArray replaceObjectAtIndex:[weakSelf.initialDownloadAssetArray indexOfObject:dict] withObject:initialDownloadAsset];
                                         
                                     }
                                 }
                             }
                             if (offlineData.count > 0)
                             {
                                 //if  insertation and deletetion has to be happen, it will happen one by one
                                 [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                                     [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                          [self broadcastingToProvider:asset];
                                     }];
                                 }];
                             }
                             else
                             {
                                 [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                                      [self broadcastingToProvider:asset];
                                 }];
                             }
                         }
                     }
                 }];
            });
        }
        else
        {
            //updating core data
            NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
            [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
            [initialDownloadAsset setObject:@"completed" forKey:@"status"];
            NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];

            if (offlineData.count > 0)
            {
                //if  insertation and deletetion has to be happen, it will happen one by one
                [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                         [self broadcastingToProvider:asset];
                    }];
                }];
            }
            else
            {
                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:^{
                     [self broadcastingToProvider:asset];
                }];
            }

        }
    }failure:^(NSString *messageFailure) {
    }];
}
//Download assets for the first time

- (void) downloadAsset: (Asset *) asset
            completion:(void (^)(void)) completionBlock
               onError:(void (^)(NSError *error))errorBlock {
    
    NSString *baseDir = AOFLoginManager.sharedInstance.documentsDir;
    NSString *filename = [[AssetsServiceClass sharedAPIManager] MD5HashForString:asset.uri_full];
    NSString *extension = [[asset.uri_full componentsSeparatedByString:@"."] lastObject];
    NSString *dest = [[NSString alloc] initWithFormat:@"%@/%@.%@", baseDir, filename, extension];
    
    NSURL *assetURL;
    // for testing in staging
    if(nil != asset.uri_full && ([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame))
    {
        assetURL = [[NSURL alloc] initWithString:asset.uri_full];
    }
    
    
    // PRONTO-29 - Fix Crashes - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: sccollateralname)'
    //Nil check to avoid crash
    if(([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame) &&
       asset != nil && (asset.assetID != nil) )
    {
        asset.path = dest;
    }
    
    /// This should occur during application:didFinishLaunchingWithOptions: [ASFFileProtectionManager stopProtectingLocation:path];
    NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
    [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:dest error:nil];
    
    
    [super performBlock:^(CallbackBlock callback) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:assetURL];
        [request setTimeoutInterval:60];
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
            if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
            {
                [request setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
            else{
            [request setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:dest append:NO]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
        }];
        
        [operation.outputStream open];
        [operation start];
        
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
        
        if (error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"ERR: %@", [error description]]];
            errorBlock(error);
        } else {
            if (dest) {
                if (asset && (asset.path != nil)) {
                    asset.path = dest;
                    //Removing update for offline download to avoid duplicates
                    //                    [[ZMCDManager sharedInstance] updateAsset:asset success:^{
                    completionBlock();
                    //                    } error:^(NSError * error) {
                    //                    }];
                }
            }
        }
    }];
    
}

- (void)broadcastingToProvider:(Asset*)asset
{
    if(asset != nil && asset.assetID != nil)
    {
        NSDictionary* userInfo = @{@"asset": asset};
        [[NSNotificationCenter defaultCenter]postNotificationName:kAssetDownloadProviderCompletionNotification object:nil userInfo:userInfo];
    }
}
- (NSString *)getThumbnailPath:(NSString *)externalPath {
    
    NSString *path;
    NSString *basePath = [NSString stringWithFormat:@"%@/com.abbvie.grid.thumbnails/", AOFLoginManager.sharedInstance.cachesDir];
    //@ABT-138 Fix to force the reload of all thumbnails. Added extra 2 character. Not recommended.
    path = [NSString stringWithFormat:@"%@%@2", basePath, [externalPath lastPathComponent]];
    return path;
}
- (NSString *)getCoverflowImagePath:(NSString *)externalPath
{
    NSString *path;
    NSString *basePath = [NSString stringWithFormat:@"%@/%@/", AOFLoginManager.sharedInstance.cachesDir,ProntoEventsCoverflowImageFolderName];
    path = [NSString stringWithFormat:@"%@%@", basePath, [externalPath lastPathComponent]];
    return path;
}
@end
