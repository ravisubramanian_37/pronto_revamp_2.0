//
//  EventsServiceClass.h
//  Pronto
//
//  Created by Saranya on 02/09/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoRedesignAPIManager.h"
#import "EventOnlyAsset+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface EventsServiceClass : ProntoRedesignAPIManager
typedef void(^OperationStatusCompletion)(BOOL success);
//old service to fetch complete events details
-(void)invokeEventsAllServiceCall:(void(^)(NSDictionary *allEvents))complete error:(void(^)(NSError *error))onError;
+ (EventsServiceClass *)sharedAPIManager;
- (NSArray *)getAllEvents;
- (Event *)getEventsbyId:(NSString *)idvalue;
- (NSString *)getMonthOfStartDate:(NSString *)date;
- (NSString *)getEventsDateString:(NSString *)date;
- (NSString *)getEventsHomeMonthString:(NSString *)startDate endDate:(NSString *)endDate;
- (NSString *)getEventsHomeDateString:(NSString *)startDate endDate:(NSString *)endDate;
/* New implementation meta data service call for display the tile details alone. Call no:1 for Events */
- (void)getEvents:(void(^)(NSDictionary *eventsFetched))complete error:(void(^)(NSError *error))onError;
/* New implementation for getting event details */
-(void)makeEventDetailServiceRequestWithCMS:(OperationStatusCompletion)completionHandler;
- (void)getEventDetails:(NSString*)eventId complete:(void(^)(NSDictionary *sessionDetails))complete error:(void(^)(NSError *error))onError;
- (void)fetchViewed:(void(^)(NSDictionary *assetDetailsFetched))complete error:(void(^)(NSError *error))onError;
- (void)saveViewed:(NSString*) eventId session:(NSString*) sessionId asset:(NSString*) assetId type:(NSString*)assetType complete:(void(^)(NSDictionary *savedAssetDetails))complete error:(void(^)(NSError *error))onError;
- (void)downloadCoverflowImage:(NSString *)path complete:(void(^)(UIImage *image, NSString *p))complete fromCache:(BOOL)cache;
- (void)downloadThumbnail:(NSString *)path complete:(void(^)(UIImage *thumbnail, NSString *p))complete fromCache:(BOOL)cache includeWaterMark:(NSString *)waterMark;
- (NSString *)getThumbnailPath:(NSString *)externalPath;
- (NSString *)getCoverflowImagePath:(NSString *)externalPath;
- (void) downloadEventOnlyAsset: (EventOnlyAsset *) eventOnlyAsset
          withProgress:(void (^)(CGFloat progress))progressBlock
            completion:(void (^)(void)) completionBlock
                        onError:(void (^)(NSError *error))errorBlock;
-(void) downloadEventOnlyAsset:(EventOnlyAsset *)asset;
- (void)syncAllEvents:(void(^)(NSDictionary *allEvents))complete error:(void(^)(NSError *error))onError;
-(void) downloadAssetFor:(Asset *)asset;
@end

NS_ASSUME_NONNULL_END
