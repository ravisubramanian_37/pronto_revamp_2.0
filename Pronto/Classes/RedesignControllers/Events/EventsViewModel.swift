//
//  EventsViewModel.swift
//  Pronto
//
//  Created by Saranya on 07/09/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
class EventsViewModel:NSObject{
    /* Method to load events landing Screen*/
    func loadEventsLandingController(){
        ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen = false
        let eventsLoadingController:EventsViewController = EventsViewController()
        HomeViewController.sharedInstance.getHomeNavController().pushViewController(eventsLoadingController, animated: true)
    }
    /* Method to load event details on tap of event in Home Screen */
    func loadEventSelected(selectedEvet:Event){
        ProntoRedesignSingletonClass.sharedInstance.isFavouritesChosen = false
        let eventDetails:EventsCalendarDetailsScreenVC = EventsCalendarDetailsScreenVC.getEventsCalendarDetailsScreen()
        eventDetails.selectedEvent = selectedEvet
        HomeViewController.sharedInstance.getHomeNavController().pushViewController(eventDetails, animated: true)
    }
}
