//
//  ProntoAssetHandlingViewModel.swift
//  Pronto
//
//  Created by Saranya on 22/10/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
class ProntoAssetHandlingViewModel: NSObject, UIPopoverPresentationControllerDelegate {
    var exportMenu: ExportMenu?
    var categoryParent: CategoryViewModel?
    var searchParent: SearchViewModel?
    var popOverDelegate: UIPopoverPresentationControllerDelegate?

  /// Opening Asset
  /// - Parameter selectedAsset: selectedAsset
    func openAsset(selectedAsset: Asset) {
        var trackingOptions: NSMutableDictionary = [:]

             if (ZMUserActions.sharedInstance().getFiltersForTracking() != nil) {
                trackingOptions.setValue(ZMUserActions.sharedInstance().getFiltersForTracking(), forKey: RedesignConstants.scfiltersubcategoryKey)
             }

             trackingOptions[RedesignConstants.scfilteractionKey] = RedesignConstants.clickThroughKey
             trackingOptions[RedesignConstants.sctierlevelKey] = RedesignConstants.tier2Key

             if selectedAsset.title!.count > 0 {
                 trackingOptions[RedesignConstants.scsearchclickthroughfilenameKey] = selectedAsset.title
             }

             
             ZMTracking.trackSection(
                 ZMUserActions.sharedInstance().section,
                 withSubsection: RedesignConstants.filterKey,
                 withName: "brand-\(String(describing: ZMUserActions.sharedInstance().franchiseName))-default",
                withOptions: trackingOptions )
             
        trackingOptions = [:]
             trackingOptions[RedesignConstants.sccollateralfeaturesKey] = ""

             if selectedAsset.title!.count > 0 {
                  trackingOptions[RedesignConstants.sccollateralnameKey] = selectedAsset.title
             }

             ZMTracking.trackSection(
                 ZMUserActions.sharedInstance().section,
                 withSubsection: RedesignConstants.assetViewKey,
                 withName: "\(String(describing: ZMAssetViewController.getAssetType(selectedAsset)))|\(String(describing: selectedAsset.title))",
                 withOptions: trackingOptions)
              let offlineAssets = ZMCDManager.sharedInstance.getAssetArray(selectedAsset.assetID ?? 0)
              var selectedAssetInDB = Asset()
              for data in offlineAssets {
                if let offData = data as? Asset, offData.assetID == selectedAsset.assetID {
                  selectedAssetInDB = offData
                }
              }
      let assetType = Constant.getFileType(for: selectedAsset)
      if selectedAsset.title!.count > 0 && assetType == ZMProntoFileTypesPDF {
        if ((selectedAssetInDB.title?.count ?? 0) > 0) && (selectedAssetInDB.assetID != 0) {
          ProntoAssetHandlingClass.sharedInstance().validate(selectedAssetInDB)
        } else {
          ProntoAssetHandlingClass.sharedInstance().validate(selectedAsset)
        }
      } else {
        ProntoAssetHandlingClass.sharedInstance().validate(selectedAsset)
      }
    }
  /// Configure Info PopOver
  /// - Parameters:
  ///   - selectedAsset: selectedAsset
  ///   - sender: sender type
    func configureInfoPopOver(selectedAsset: Asset, sender: UIButton){
        let infoMenu: InfoMenu = (Bundle.main.loadNibNamed(RedesignConstants.infoMenuBundleName,
                                                           owner: self, options: nil)?[0] as? InfoMenu)!

        // expiredDate
        let expiredOn = selectedAsset.unpublish_on
        let timeInterval = TimeInterval(expiredOn?.doubleValue ?? 0.0)
        let date = Date(timeIntervalSince1970: timeInterval)
        let df = DateFormatter()
        df.dateFormat = RedesignConstants.dateFormat
        let dateString = df.string(from: date)
        
        // lastUpdated Date
        let lastUpdated = selectedAsset.changed
        let lastTimeInterval = TimeInterval(lastUpdated?.doubleValue ?? 0.0)
        let lastDate = Date(timeIntervalSince1970: lastTimeInterval)
        let lastDateString = df.string(from: lastDate)
        
        if searchParent != nil{
            infoMenu.totalViewsCount = selectedAsset.view_count?.stringValue
        } else {
            if (ProntoRedesignSingletonClass.sharedInstance.isListChosen) {
                infoMenu.totalViewsCount = selectedAsset.view_count?.stringValue
            } else {
                if let parentCategoryModel = categoryParent{

                    infoMenu.totalViewsCount = parentCategoryModel.selectedGridCell?.totalViews.description
                }
            }
        }
        
        infoMenu.expiredDate = dateString
        infoMenu.lastUpdatedDate = lastDateString
        infoMenu.medRegNo = selectedAsset.medRegNo
        
        infoMenu.infoArray = []
        infoMenu.infoArray.add(RedesignConstants.infoOption_No_Of_Views)
        infoMenu.infoArray.add(RedesignConstants.infoOption_Meg_Reg_Number)
        infoMenu.infoArray.add(RedesignConstants.infoOption_Last_Updated)
        infoMenu.infoArray.add(RedesignConstants.infoOption_Exp_Date)
        
        let popoverContent = infoMenu as UIViewController
        popoverContent.modalPresentationStyle = .popover

        if let popover = popoverContent.popoverPresentationController {
            popover.backgroundColor = .white
            let viewForSource = sender as UIView
            popover.sourceView = viewForSource
            popover.permittedArrowDirections = .any
            popover.sourceRect = CGRect(x: viewForSource.bounds.maxX/2, y: viewForSource.bounds.maxY - viewForSource.bounds.minY, width: 0, height: 0)
            // the size you want to display
            popoverContent.preferredContentSize = CGSize(width: RedesignConstants.assetsExportPopOverWidth,
                                                         height: (infoMenu.infoArray.count * 44) + 10)
            popover.delegate = self
            popOverDelegate = popover.delegate
            RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: popoverContent, animated: true)
        }
    }
    //PopOver Delegate to perform action after popover dismissal
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
        exportMenu?.dismiss(animated: false)
        if let parentCategoryModel = categoryParent{
            parentCategoryModel.setExportToNormal(false)
        } else if let searchParent = searchParent {
            searchParent.setExportToNormal(false)
        }
    }
    //Method to define the presentation style for popover presentation controller
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
              return .none
    }
    //Export PopOver
    func configureExportPopOver(selectedAsset: Asset,
                                sender:UIButton) {
        exportMenu = (Bundle.main.loadNibNamed(RedesignConstants.exportMenuBundleName, owner: self, options: nil)?[0] as! ExportMenu)
        exportMenu?.selectingAsset(withDetails: selectedAsset, withSender: sender)

        exportMenu?.exportArray = []
        // check and display Print Option
        if selectedAsset.field_print?.count ?? 0 > 0 && (selectedAsset.field_print == "1") {
            exportMenu?.exportArray.add(RedesignConstants.exportMenu_Print)
        }
        exportMenu?.exportArray.add(RedesignConstants.exportMenu_Share)
        // check and display Email to self Option
        if selectedAsset.field_email_to_self?.count ?? 0 > 0 && (selectedAsset.field_email_to_self == "1") {
            exportMenu?.exportArray.add(RedesignConstants.exportMenu_Email_To_Self)
        }
               
        let popoverContent = (exportMenu)! as UIViewController
        popoverContent.modalPresentationStyle = .popover

        if let popover = popoverContent.popoverPresentationController {
            popover.backgroundColor = .white
            let viewForSource = sender as UIView
            popover.sourceView = viewForSource
            popover.permittedArrowDirections = .any
            popover.sourceRect = CGRect(x: viewForSource.bounds.maxX/2, y: viewForSource.bounds.maxY - viewForSource.bounds.minY, width: 0, height: 0)
            // the size you want to display
            popoverContent.preferredContentSize = CGSize(width: RedesignConstants.assetsExportPopOverWidth,height:(exportMenu!.exportArray.count * 44) + 10)
            popover.delegate = self
            popOverDelegate = popover.delegate
            RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: popoverContent, animated: true)
        }
    }
}
