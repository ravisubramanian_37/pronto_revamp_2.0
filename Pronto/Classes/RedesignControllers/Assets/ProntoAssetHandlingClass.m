//
//  ProntoAssetHandlingClass.m
//  Pronto
//
//  Created by Saranya on 05/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoAssetHandlingClass.h"
#import "Constant.h"
#import "Pronto-Swift.h"
#import "AssetsServiceClass.h"
#import "RedesignErrorHandling.h"
#import "FastPDFViewController.h"
#import <FastPdfKit/FastPdfKit.h>
@class FastPDFViewController;
@interface ProntoAssetHandlingClass (){
    BOOL promotedTappedForAudio;
    //For Audio Player
    AudioPlayer *audioPlayerView;
    FranchiseTableViewCell *audioCurrentCell;
    Asset *assetForAudio;
    ZMGridItem *audioCurrentGridCell;
}
@property (nonatomic, strong) Asset *currentSelectedAssetToSeeDetail;
@end

@implementation ProntoAssetHandlingClass
BOOL _isDownloadignAsset;
/* Singleton Object Creation */
+ (ProntoAssetHandlingClass *)sharedInstance {
    
    @synchronized(self)
    {
        static ProntoAssetHandlingClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
-(void)resetAssetValue{
    self.currentSelectedAssetToSeeDetail = nil;
}
-(void)resetHomeAudioView{
    if(audioPlayerView != nil)
        [audioPlayerView stopAudio];
    [ProntoAssetHandlingClass.sharedInstance setHomeFavouritesAudioView:[UIView new]];
    [ProntoAssetHandlingClass sharedInstance].isFromHomeView = false;
}
-(void)resetAudioPlayer{
    if(audioPlayerView != nil){
        [audioPlayerView stopAudio];
    }
}

/*
 * To enable background download of assets when a category is Selected
 */
- (void)backgroundDownload:(NSMutableArray *)array{
    _assetsDownloadArray = [NSMutableArray new];
    [_assetsDownloadArray addObjectsFromArray:array];
    if((_assetsDownloadArray.count > 0)  && [ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])//&& [[[NSUserDefaults standardUserDefaults] objectForKey:@"Off_Start"] isEqualToString:@"NO"]
    {
        [self offlineDownloadOfAssets];
    }
}
//Offline download main function
-(void) offlineDownloadOfAssets{

    __strong NSMutableArray* assetsPageDuplicate;
    assetsPageDuplicate = [[NSMutableArray alloc]init];
    [assetsPageDuplicate addObjectsFromArray:self.assetsDownloadArray];
    // is it in background
    dispatch_async(dispatch_get_main_queue(), ^{
         NSArray *initialDownloadDbArray = [[ZMCDManager sharedInstance]getOfflineDownloadArray];
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        //If coredata not present
        if (initialDownloadDbArray.count != 0) {

            //Add assets which are newly added
            for (Asset * asset in assetsPageDuplicate) {
                bool dataPresent = NO;
                for(OfflineDownload * data in initialDownloadDbArray) {
                    if ([[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"]) {
                        if(asset.assetID == data.assetID) {
                            dataPresent = YES;
                            break;
                        }
                    }
                    else {
                        dataPresent = YES;
                        break;
                    }
                }
                if(!dataPresent) {
                    [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                    [initialDownloadAsset setObject:@"in_progress" forKey:@"status"];
                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                    
                    //Downloading the Asset added new
//                    NSNumber * bucketId = [_actions getBucketID:asset];
                    if(asset != nil)
                        // &&[[bucketId stringValue] isEqualToString:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]])
                    {
                        [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
                    }
//                    else if(asset != nil && bucketId == nil)
//                    {
//                        [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
//                    }
                }
            }
           
            //Download the data which is in progress
            for(OfflineDownload * data in initialDownloadDbArray) {
                if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"] || [data.status isEqualToString:@"failed"]) {
                    
                    //Download initiate for failed assets
                    if([data.status isEqualToString:@"completed_partialy"] || [data.status isEqualToString:@"failed"]) {
                        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:data.assetID ];
                        [initialDownloadAsset setObject:data.assetID forKey:@"assetID"];
                        [initialDownloadAsset setObject:@"in_progress" forKey:@"status"];
                        
                        if (offlineData.count > 0)
                        {
                            //if  insertation and deletetion has to be happen, it will happen one by one
                            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                            }];
                        }
                        else
                        {
                            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                        }
                    }
                    
                    NSArray * assetVal = [[ZMCDManager sharedInstance]getAssetArrayById:data.assetID.stringValue];
                    if (assetVal.count > 0) {
                        Asset * assetCache = assetVal[0];
//                        NSNumber * bucketId = [_actions getBucketID:assetCache];
                        if(assetCache != nil)
                            //&& [[bucketId stringValue] isEqualToString:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]])
                        {
                            [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:assetCache];
                        }
//                        else if(assetCache != nil && bucketId == nil)
//                        {
//                            [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:assetCache];
//                        }
                       
                    }
                }
            }
        }
        //If coredata not present or first download
        else {
            NSMutableArray * assetArrayToDownload = [[NSMutableArray alloc]init];
            for (Asset * asset in assetsPageDuplicate) {
                if ([[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"]) {
                    [assetArrayToDownload addObject:asset];
                }
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset:%@ uri:%@",asset.assetID,asset.uri_full]];
            }
            [AssetsServiceClass.sharedAPIManager offlineDownload:assetArrayToDownload];
            for(Asset * asset in assetArrayToDownload) {
                [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
            }
      
        }

});
}
/**
 *  validateAssetBeforeOpen : Check if the assets is local or initiates the download
 *
 *  @param asset      asset to validate
 *  @param completion block completion to indicate if asset is valid, these return indicator know if asset needs download or not
 *  @param failure    block failure completion to indicate something was wrong
 */

-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    [self trackGetAsset:asset];
    NSLog(@"savedPath 123 :%@\n",asset.path);
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
    if (assetType == ZMProntoFileTypesBrightcove)
    {
        UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
        if (topController.presentedViewController) {
            topController = topController.presentedViewController;
            NSLog(@" inside topcontroller");
             }
        else{
            UINavigationController *nav = (UINavigationController *)topController;
            topController = nav.topViewController;
            
            NSLog(@"Not inside topcontroller");
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            UIViewController <ZMAssetViewControllerDelegate> *controller;
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
            controller.asset = asset;
            controller.videoId = asset.uri_full;
            //                HelpViewController * controller = (HelpViewController*)[storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
           // controller.videoId = asset.uri_full;
            //controller.ispresented = NO;
            [topController.navigationController pushViewController:controller animated:NO];
            //[self.navigationController pushViewController:controller animated:NO];
        }
    }
    else
    {
        if (!_isDownloadignAsset) {
            _isDownloadignAsset = YES;
            // check if asset is protected
            
            if (![UIApplication sharedApplication].protectedDataAvailable) {
                [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
                /// This should occur during application:didFinishLaunchingWithOptions:
                //[ASFFileProtectionManager stopProtectingLocation:asset.path];
                
                // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
                NSError *error;
                NSFileManager *fileManager = [[NSFileManager alloc] init];
                NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];
                
                
                if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
                {
                    attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                    BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
                    if (!success)
                        [AbbvieLogging logInfo:@"Set ~/Documents attrsAssetPath NOT successfull"];
                }
                
                /// ....
                NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
                [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
                [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
            }
            
            // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
            if([[NSFileManager defaultManager] fileExistsAtPath:asset.path]) {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"File Exists at %@", asset.path]];
                
            }
            
            if ((![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && assetType != ZMProntoFileTypesBrightcove) && (assetType != ZMProntoFileTypesWeblink))
            {
                if ([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
                    completion(YES);
                }
                else {
                    _isDownloadignAsset = NO;
                    failure(@"The internet connection is not available, please check your network connectivity");
                }
            }else{
                //Check if the asset size matches the metada info meaning the asset is correct
                if (asset.file_size.intValue == (int)fileSize || asset.isDownloaded == true) {
                    //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
                    _isDownloadignAsset = NO;
                    completion(NO);
                }else{
                    completion(YES);
                }
            }
        } else {
            // PRONTO-16 - Opening an Asset in AirPlane Mode
            if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
                
                if(_isDownloadignAsset == NO)
                {
                    
                    failure(@"The internet connection is not available, please check your network connectivity");
                    
                    _isDownloadignAsset = NO;
                }
                
            }
            else
                failure(@"Download in progress. Please wait and then try again.");
        }
    }

    
    
 
   
}
/**
 *  trackGetAsset: make traking to event
 *
 *  @param asset asset to track
 */
- (void)trackGetAsset:(Asset *) asset {
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    
    if (![asset.path isEqualToString:@""] || [[ZMAssetViewController getAssetType:asset] isEqualToString:RedesignConstants.videoText]) {
        [trackingOptions setObject:[NSString stringWithFormat:@"newpanel:assetview:%@",[ZMAssetViewController getAssetType:asset]] forKey:@"prop32"];
        [trackingOptions setObject:[NSString stringWithFormat:@"newpanel:assetview:%@",[ZMAssetViewController getAssetType:asset]] forKey:@"eVar32"];
        [trackingOptions setObject:@"event52" forKey:@"events"];
        [ZMTracking trackSection:@"scscreenname" withSubsection:@"newpanel-assetview" withName:[NSString stringWithFormat:@"%ld-%@",asset.assetID.longValue,asset.title] withOptions:trackingOptions];
    }
}

// New Revamping - Download selected asset
- (void)downloadOneAsset:(Asset *)selectedAsset completion:(void (^)(BOOL assetDownloaded))completion
                 failure:(void (^)(NSString *assetDownloadFail))failure {
    
    _currentAssetDownload = selectedAsset;
    _isDownloadignAsset = YES;
    [self setProgress:0.001000];
    
    [[AssetsServiceClass sharedAPIManager] downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        _isDownloadignAsset = NO;
        completion(YES);
    } onError:^(NSError *error) {
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        _isDownloadignAsset = NO;
        completion(NO);
    }];
}
- (void)downloadAssetDidNotFinish:(Asset *)asset error:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([RedesignErrorHandling sharedInstance].currentMessage){
            [[RedesignErrorHandling sharedInstance] hideMessage];
        }
        
        _isDownloadignAsset = NO;
        _isTapDownloadInitiated = NO;
        
        NSString *message;
        enum ZMProntoMessages type = 1;
                
        if (error.code == 503) {
            type = ZMProntoMessagesTypeWarning;
            message = @"The server is currently under maintenance mode.";
        }
        
        else if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
            type = ZMProntoMessagesTypeWarning;
            message = @"The Internet connection is not available, please check your network connectivity.";
        }
        else if(error.code == 401) {
            //Restoring the user session
            //TODO:Check this
//            [mainDelegate initPronto];
            return;
        }
        else if (error.code == 404) {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"Asset not found %@", asset.title];
        }
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        else if (error.code == 5303 || error.code == -5303)
        {
            message = @"Error while retreiving assets, please try again later";
        }
        else if (error.code == 1009 || error.code == -1009)
        {
            message = @"Please check the network connection";
        }
        else if (error.code == 1003 || error.code == -1003)
        {
            message = @"A server with the specified hostname could not be found";
        }
        else {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"There is a problem downloading %@", asset.title];
        }
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[RedesignErrorHandling sharedInstance] showMessage:message whithType:type withAction:^{
                [[RedesignErrorHandling sharedInstance] hideMessage];
                [self validateAsset:asset];
            }];
        });
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        
        
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        if ([message containsString:@"ERROR_CODE"])
        {
            // errorcode consists of digits
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",@"Please check the network connection"] forKey:@"scerrorcode"];
        }
        else
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",message] forKey:@"scerrorcode"];
        
        ZMGridItem *currentDownloadItem;
        if(_selectedGridCell != nil){
            currentDownloadItem = _selectedGridCell;
        }
//        for (ZMGridItem *cell in [grid visibleCells]) {
//
//            if(_currentAssetDownload.assetID.longValue == cell.uid) {
//                currentDownloadItem = cell;
//                break;
//            }
//        }
        currentDownloadItem.progress = 1;
        _currentAssetDownload = nil;
        
        /**
         * @Tracking
         * Download Did Not finished
         **/
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"assetview" withName:nil withOptions:trackingOptions];
    });
}
- (void)setProgress:(float)progress {
    if([CategoryViewModel sharedInstance].selectedGridCell != nil){
        _selectedGridCell = [CategoryViewModel sharedInstance].selectedGridCell;
    }
    //Progress for Grid Item
  if(_selectedGridCell != nil){
    ZMGridItem *currentDownloadItem = _selectedGridCell;
    
    //    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@",currentAssetDownload.assetID];
    //    NSArray * cells = [[grid visibleCells]filteredArrayUsingPredicate:predicate];
    //    if (cells.count > 0) {
    //        currentDownloadItem = cells.firstObject;
    currentDownloadItem.progress = progress;
    currentDownloadItem.loaderBackgroundView.hidden = NO;
    [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
    [currentDownloadItem.loaderBackgroundView setAlpha:0.5];
    //    }
    
    //    NSArray * cells1 = [[promotedGrid visibleCells]filteredArrayUsingPredicate:predicate];
    //    if (cells1.count > 0) {
    //        currentDownloadItem = cells1.firstObject;
    //        currentDownloadItem.progress = progress;
    //        currentDownloadItem.loaderBackgroundView.hidden = NO;
    //        [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
    //        [currentDownloadItem.loaderBackgroundView setAlpha:0.5];
    //    }
    
    if (progress >= 1) {
      [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
      [currentDownloadItem.loaderBackgroundView setAlpha:1];
      currentDownloadItem.loaderBackgroundView.hidden = YES;
    }
  }
    if([CategoryViewModel sharedInstance].selectedListCell != nil){
        _selectedListCell = [CategoryViewModel sharedInstance].selectedListCell;
    }
    //Progress for list item
    if(_selectedListCell != nil){
    FranchiseTableViewCell *currentCell;
    
//    NSPredicate * predicateForList = [NSPredicate predicateWithFormat:@"uid == %@",currentAssetDownload.assetID];
//    NSArray * cellsForList = [[listTableView visibleCells]filteredArrayUsingPredicate:predicateForList];
//    if (cellsForList.count > 0) {
//        currentCell = cellsForList.firstObject;
        currentCell = _selectedListCell;
        currentCell.progress = progress;
        
        currentCell.loaderBackgroundView.hidden = NO;
        currentCell.loaderView.hidden = NO;
        
        [currentCell.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
        [currentCell.loaderBackgroundView setAlpha:0.5];
//    }
    if (progress >= 1) {
        _currentAssetDownload = nil;
        
        currentCell.loaderBackgroundView.bounds = currentCell.bounds;
        currentCell.loaderView.center = currentCell.center;
        
        [currentCell.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
        [currentCell.loaderBackgroundView setAlpha:1];
        
        currentCell.loaderBackgroundView.hidden = YES;
        currentCell.loaderView.hidden = YES;
    }
    }
}
/**
 *  With the given asset validates if the assets is local or initiates the download
 *  @param selectedAsset
 */
- (void)validateAsset:(Asset *)selectedAsset {
    
//    // PRONTO-15 - Popup Getting Displayed Over PDF
//    if(headerView.categoryPopOver)
//        [[headerView.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
//
//    // removing deprecated warnings
//    if(headerView.sortMenuPopOver)
//        [[headerView.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
//
    
    [AbbvieLogging logInfo:@"*****Intro validate Asset ********"];
    [self validateAssetBeforeOpen:selectedAsset completion:^(BOOL assetNeedsDownload) {
        if (assetNeedsDownload) {
            //Restricting user from opening asset if offline download is in progress
            NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:selectedAsset.assetID];
            for(OfflineDownload * data in offlineDownloadArray) {
                if(data.assetID == selectedAsset.assetID) {
                    if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
    
                            if(!_isTapDownloadInitiated)
                            {
                                [self downloadSelectedAsset:selectedAsset];
                                [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                            }
                        });
                        _isDownloadignAsset = NO;
                    }
                    else {
                        if(!_isTapDownloadInitiated)
                        {
                            [self downloadSelectedAsset:selectedAsset];
                            [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                        }
                    }
                }
            }
            if(offlineDownloadArray.count == 0) {
                if(!_isTapDownloadInitiated)
                {
                    [self downloadSelectedAsset:selectedAsset];
                    [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                }
//                _isDownloadignAsset = NO;
            }
        }else
        {
          if (self.selectedGridCell != nil) {
            self.selectedGridCell.downloadButton.hidden = YES;
            selectedAsset.isDownloaded = true;
            [[ZMCDManager sharedInstance]updateAsset:selectedAsset success:^{
              NSLog(@"saved Asset", selectedAsset.isDownloaded);
            } error:^(NSError * error) {
              [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on documment %@", error.localizedDescription]];
            }];
          }
            if(self.currentSelectedAssetToSeeDetail)
            {
                //Return in case if open of any asset is in progress
                return;
            }
            else
            {
                _currentSelectedAssetToSeeDetail = selectedAsset;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[RedesignErrorHandling sharedInstance] showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                    selectedAsset.isUpdateAvailable = NO;
                    
                });
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [AbbvieLogging logInfo:@"------Asset needs opened ------"];
                    if([ProntoAssetHandlingClass sharedInstance].isFromHomeView){
                        [self openAudioAsset:selectedAsset];
                    }else if(self.homePromotedContentAudioView != nil){
                       [self handleAudioOpeningInHomePromotedContent:selectedAsset];
                    }else{
                      [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                        [self openAsset:selectedAsset];
                    }
                    
                    promotedTappedForAudio = false;
                });
            }
        }
    } failure:^(NSString *messageFailure) {
        [[RedesignErrorHandling sharedInstance] showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    }];
}

//Download of an asset if not found in coredata / new

- (void) downloadSelectedAsset:(Asset *)selected {
    _isTapDownloadInitiated = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RedesignErrorHandling sharedInstance] showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    });
    [AbbvieLogging logInfo:@"------Asset needs download ------"];
    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    if(![selected.file_mime isEqualToString:@"weblink"])
        [self downloadAsset:selected];
}
- (void)downloadAsset:(Asset *)selectedAsset{
    _currentAssetDownload = selectedAsset;
    _isDownloadignAsset = YES;
    [self setProgress:0.001000];
    
    [[AssetsServiceClass sharedAPIManager] downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        selectedAsset.isDownloaded = true;
        selectedAsset.savedPath = selectedAsset.path;
        [self downloadAssetDidFinish:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"completed"];
        // New revamp changes - remove the update label once the asset is downloaded successfully.
        selectedAsset.isUpdateAvailable = NO;
//        [self reloadAssets];
    } onError:^(NSError *error) {
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAssetOnFailure:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"failed"];
    }];
}

- (void)downloadAssetDidFinish:(Asset *)asset {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RedesignErrorHandling sharedInstance] hideMessage];
      [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.hideBottomViewNotification object:self userInfo:nil];
    });
    
    _isDownloadignAsset = NO;
    _isTapDownloadInitiated = NO;
    [[RedesignErrorHandling sharedInstance] showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", asset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self validateAsset:asset];
    });
}
- (void)updateOfflineDownloadCoreData:(Asset *)asset status:(NSString*)status
{
    if([asset valueForKey:@"assetID"] != nil)
    {
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
        [initialDownloadAsset setObject:status forKey:@"status"];
        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
        
        if (offlineData.count > 0)
        {
            //if  insertation and deletetion has to be happen, it will happen one by one
            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
            }];
        }
        else
        {
            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
        }
    }
}
/**
 *  Open selected asset, the asset could be of type PDF-VIDEO-DOCUMENT,the assets of type PDF or document will be open through FastPDFKIT, the ones of type Video thorugh Brightcove
 *
 *  @param selectedAsset Asset to open
 */

- (void)openAsset:(Asset *)selectedAsset
{
    [ProntoRedesignSingletonClass sharedInstance].audioAssetView = nil;
    [ProntoRedesignSingletonClass sharedInstance].audioAsset = nil;
    if([selectedAsset.type isEqualToString:[EventOnlyAsset TypeAttributeValueAsString]])
    {

    }
    else
    {
        //If Selected path or Selected path is not present then asset can't be opened.
        if (selectedAsset == nil && selectedAsset.savedPath == nil && selectedAsset.path == nil) { return ;}
//      NSString * path = [[ZMProntoManager sharedInstance]validatePdfWithAsset: selectedAsset];
        
        NSString *path = selectedAsset.file_size.intValue == [[ZMProntoManager sharedInstance] fileSizeWithPath:selectedAsset.savedPath] ? selectedAsset.savedPath : selectedAsset.path;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", path]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[RedesignErrorHandling sharedInstance] hideMessage];
        });
        
        enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
        
        _isDownloadignAsset = NO;
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UIViewController <ZMAssetViewControllerDelegate> *controller;
        
        if (assetType == ZMProntoFileTypesPDF) {
          MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath: path]];
            if(document != nil)
            {
                [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
            }
            else{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[RedesignErrorHandling sharedInstance] showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                    self.currentSelectedAssetToSeeDetail = nil;
                });
            }
        }
        else if (assetType == ZMProntoFileTypesBrightcove) {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];

            controller.asset = selectedAsset;
          controller.videoId = selectedAsset.uri_full;

            controller.userFranchise = AOFLoginManager.sharedInstance.salesFranchise;
            if(self.navigationController == nil)
            {
                [self getNavigationOfAppDelegate:controller];
            }
            else
            {
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        else if (assetType == ZMProntoFileTypesDocument) {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];

            controller.asset = selectedAsset;

            controller.userFranchise = AOFLoginManager.sharedInstance.salesFranchise;
            if(self.navigationController == nil)
            {
                [self getNavigationOfAppDelegate:controller];
            }
            else
            {
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        // PRONTO-25 Web View - Ability to provide a Web link as an asset type.
        //
        //
        else if(assetType == ZMProntoFileTypesWeblink)
        {
            // open the weblink URL
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
            controller.asset = selectedAsset;
            controller.userFranchise = AOFLoginManager.sharedInstance.salesFranchise;
            [[ProntoUserDefaults userDefaults] setPreviousAsset:selectedAsset];
            [[HomeViewController sharedInstance] showSafariWithUri:selectedAsset.uri_full];

          
          // Safari browser is used instead of this viewcontroller
//            if(self.navigationController == nil)
//            {
//                [self getNavigationOfAppDelegate:controller];
//            }
//            else
//            {
//                [self.navigationController pushViewController:controller animated:YES];
//            }
            
        }
        else if(assetType == ZMProntoFileTypesAudio)
        {
            [self stopAudio];
            assetForAudio = selectedAsset;
            [AudioPlayer.sharedInstance setAssetWithAssetVal:selectedAsset];
            if(!audioPlayerView)
            {
                audioPlayerView = (AudioPlayer*)[[[NSBundle mainBundle] loadNibNamed:@"AudioPlayerView" owner:self options:nil] firstObject];
            }
            [[CategoryViewModel sharedInstance]setAudioParamsToParentWithSelectedAsset:assetForAudio audioPlayerView:audioPlayerView];
            [audioPlayerView initialSetupWithAssetVal:selectedAsset];
            self.currentSelectedAssetToSeeDetail = nil;

                if(_selectedListCell != nil){

                    audioCurrentCell = _selectedListCell;
                        audioPlayerView.frame = CGRectMake(0, 0,audioCurrentCell.audioPlayerView.frame.size.width, audioCurrentCell.audioPlayerView.frame.size.height);
                        [audioPlayerView.layer setMasksToBounds:YES];
                        audioCurrentCell.audioPlayerView.hidden = NO;
                        [audioCurrentCell.audioPlayerView addSubview:audioPlayerView];
                
                }
                else  if(_selectedGridCell != nil){

                        audioCurrentGridCell = _selectedGridCell;
                        audioPlayerView.frame = CGRectMake(0, 0,audioCurrentGridCell.audioPlayerView.frame.size.width, audioCurrentGridCell.audioPlayerView.frame.size.height);
                        [audioPlayerView.layer setMasksToBounds:YES];
                        audioCurrentGridCell.audioPlayerView.hidden = NO;
                        [audioCurrentGridCell.audioPlayerView addSubview:audioPlayerView];
                }else{
                    [audioPlayerView stopAudio];
                    audioPlayerView = nil;
                }

        }
        else
        {
            // Fix - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** -[NSURL initFileURLWithPath:]: nil string parameter'
            if (assetType == ZMProntoFileTypesPDF) {
                
                if(selectedAsset.path)
                {
                    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
                    if(document != nil)
                    {
                        [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
                    }else{
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [[RedesignErrorHandling sharedInstance] showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                            self.currentSelectedAssetToSeeDetail = nil;
                        });
                    }
                }
            }
        }
    }
}
-(void)openAudioAsset:(Asset *)selectedAsset{
    
    [ProntoRedesignSingletonClass sharedInstance].audioAssetView = nil;
    [ProntoRedesignSingletonClass sharedInstance].audioAsset = nil;
    
    //If Selected path or Selected path is not present then asset can't be opened.
    if (selectedAsset == nil || selectedAsset.path == nil) { return ;}
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RedesignErrorHandling sharedInstance] hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
    
    _isDownloadignAsset = NO;
        
    if(assetType == ZMProntoFileTypesAudio && [ProntoAssetHandlingClass sharedInstance].isFromHomeView){
        [self stopAudio];
        assetForAudio = selectedAsset;
        [AudioPlayer.sharedInstance setAssetWithAssetVal:selectedAsset];
        if(!audioPlayerView)
        {
            audioPlayerView = (AudioPlayer*)[[[NSBundle mainBundle] loadNibNamed:@"AudioPlayerView" owner:self options:nil] firstObject];
        }
        [audioPlayerView initialSetupWithAssetVal:selectedAsset];
                   self.currentSelectedAssetToSeeDetail = nil;
        if(_homeFavouritesAudioView != nil){
            audioPlayerView.frame = CGRectMake(0, 30,_homeFavouritesAudioView.frame.size.width, _homeFavouritesAudioView.frame.size.height - 30);
                                [audioPlayerView.layer setMasksToBounds:YES];
                                [_homeFavouritesAudioView addSubview:audioPlayerView];
            }
    }

}
-(void)handleAudioOpeningInHomePromotedContent:(Asset *)selectedAsset{
    //If Selected path or Selected path is not present then asset can't be opened.
    if (selectedAsset == nil || selectedAsset.path == nil) { return ;}
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RedesignErrorHandling sharedInstance] hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
    
    _isDownloadignAsset = NO;
        
    if(assetType == ZMProntoFileTypesAudio){
        [self stopAudio];
        assetForAudio = selectedAsset;

        if(!audioPlayerView)
        {
            audioPlayerView = (AudioPlayer*)[[[NSBundle mainBundle] loadNibNamed:@"AudioPlayerView" owner:self options:nil] firstObject];
        }
        [audioPlayerView initialSetupWithAssetVal:selectedAsset];
        self.currentSelectedAssetToSeeDetail = nil;
        
        if(self.homePromotedContentAudioView != nil){
            
            [audioPlayerView.layer setMasksToBounds:YES];
            [self.homePromotedContentAudioView addSubview:audioPlayerView];
            audioPlayerView.frame = CGRectMake(0, 0, self.homePromotedContentAudioView.frame.size.width, self.homePromotedContentAudioView.frame.size.height);
            [PromotedContentsViewModel sharedInstance].audioView = audioPlayerView;

            

        }
    }
}
- (void)stopAudio
{
    if(assetForAudio != nil)
    {
        [audioPlayerView stopAudio];
        
        if(audioCurrentCell)
        {
            audioCurrentCell.audioPlayerView.hidden = YES;
            audioCurrentCell = nil;
        }
        else if(audioCurrentGridCell)
        {
            audioCurrentGridCell.audioPlayerView.hidden = YES;
            audioCurrentGridCell = nil;
        }

        assetForAudio = nil;
    }
}
-(void)getNavigationOfAppDelegate:(UIViewController*)viewController
{
    AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController* navigationController = (UINavigationController*)(mainDelegate.window.rootViewController);
    navigationController.view.frame = CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height);
    [navigationController pushViewController:viewController animated:YES];
}
- (void)openFastPDFWithAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
      
    controller.asset = asset;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    viewController.view.frame = CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, viewController.view.frame.size.height);
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, view.frame.size.height);
    
    [viewController.view addSubview:view];
    viewController.view.backgroundColor = [RedesignThemesClass sharedInstance].defaultThemeforUser;
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
        
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor whiteColor];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]; [webview addSubview:spinner];
    spinner.center = webview.center;
    spinner.color = [UIColor grayColor];
    [viewController.view addSubview:webview];
    [spinner startAnimating];
    UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(viewController.view.center.x - 48, viewController.view.center.y + 24, UIScreen.mainScreen.bounds.size.width, 24)];
      labelView.backgroundColor = [UIColor whiteColor];
      labelView.text = @"Please wait...";
      [labelView setFont:[UIFont boldSystemFontOfSize:16.0]];
      labelView.textColor = [UIColor blackColor];
      [viewController.view addSubview:labelView];

    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    [[RedesignErrorHandling sharedInstance] hideMessage];
    controller.userFranchise = AOFLoginManager.sharedInstance.salesFranchise;
    controller.viewMain = view;
    controller.viewMain.frame = CGRectMake(controller.viewMain.frame.origin.x, controller.viewMain.frame.origin.y, UIScreen.mainScreen.bounds.size.width, controller.viewMain.frame.size.height);
    controller.webviewScreen = webview;
    controller.webviewScreen.frame = CGRectMake(controller.webviewScreen.frame.origin.x, controller.webviewScreen.frame.origin.y, UIScreen.mainScreen.bounds.size.width, controller.webviewScreen.frame.size.height);
    controller.webviewScreen.hidden = NO;
    
    double delayInSeconds = 2.5; // set the time
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        controller.webviewScreen.hidden = YES;
        [spinner stopAnimating];
      [labelView removeFromSuperview];
    });
    
    //To perform push if the navigation controller is nil
    if(self.navigationController == nil)
    {
        [self getNavigationOfAppDelegate:viewController];
    }
    else
    {
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
}


@end
