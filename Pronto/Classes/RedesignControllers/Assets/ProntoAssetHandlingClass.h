//
//  ProntoAssetHandlingClass.h
//  Pronto
//
//  Created by Saranya on 05/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Asset.h"
#import "ZMGridItem.h"
#import "FranchiseTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProntoAssetHandlingClass : NSObject
{
    BOOL _isTapDownloadInitiated;
}
@property (assign) BOOL isFromHomeView;
@property (nonatomic, strong) UIView *homeFavouritesAudioView;

@property (assign) BOOL isFromHomePromotedContent;
@property (nonatomic, strong) UIView *homePromotedContentAudioView;

@property (nonatomic, strong) ZMGridItem *selectedGridCell;
@property (nonatomic, strong) FranchiseTableViewCell *selectedListCell;
@property (nonatomic, strong) Asset *currentAssetDownload;
@property (nonatomic, strong) NSMutableArray *assetsDownloadArray;
+ (ProntoAssetHandlingClass *)sharedInstance;
@property (nonatomic, strong) UINavigationController *navigationController;

-(void)validateAssetBeforeOpen:(Asset *)asset
                                completion:(void (^)(BOOL assetNeedsDownload))completion
                                failure:(void (^)(NSString *messageFailure))failure;
- (void)downloadOneAsset:(Asset *)selectedAsset completion:(void (^)(BOOL assetDownloaded))completion
                 failure:(void (^)(NSString *assetDownloadFail))failure;
- (void)openAsset:(Asset *)selectedAsset;
- (void)validateAsset:(Asset *)selectedAsset;
- (void)resetAssetValue;
- (void)resetHomeAudioView;
- (void)resetAudioPlayer;
- (void)backgroundDownload:(NSMutableArray *)array;
- (void)downloadAsset:(Asset *)selectedAsset;
- (void)downloadSelectedAsset:(Asset *)selected;
@end

NS_ASSUME_NONNULL_END
