//
//  AssetsServiceClass.h
//  Pronto
//
//  Created by Saranya on 05/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProntoRedesignAPIManager.h"
#import "Asset.h"
NS_ASSUME_NONNULL_BEGIN

@interface AssetsServiceClass :  ProntoRedesignAPIManager
@property(nonatomic, strong) NSMutableArray * initialDownloadAssetArray;
+ (AssetsServiceClass *)sharedAPIManager;
-(void) offlineDownload: (NSMutableArray *)assetDownloadArray;
- (void) downloadAsset: (Asset*) asset
                        withProgress:(void (^)(CGFloat progress))progressBlock
                        completion:(void (^)(void)) completionBlock
                        onError:(void (^)(NSError *error))errorBlock;
-(void)sendEmailToself:(NSString *)assetId success:(void (^)(NSDictionary *))success error:(void (^)(NSError *))onError;
- (void)uploadEvents:(void(^)(void))complete error:(void(^)(NSError *error))onError;
-(void)getAssets:(NSDictionary *)assets success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError;
-(NSArray*)getAsset:(NSString*)object withContext:(NSManagedObjectContext*)context;
- (NSString *)MD5HashForString:(NSString *)input;
-(void)initialDownloadValidate:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure;
@end

NS_ASSUME_NONNULL_END
