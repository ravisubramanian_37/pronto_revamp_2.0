//
//  AssetsServiceClass.m
//  Pronto
//
//  Created by Saranya on 05/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "AssetsServiceClass.h"
#import "Pronto-Swift.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSMutableDictionary+Utilities.h"

typedef void (^AssetCompletionBlock) (void);
typedef void (^CallbackBlock)(NSError* error, NSObject* response);

@implementation AssetsServiceClass

/* Singleton Object Creation */
+ (AssetsServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static AssetsServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
/**
 *  Download asset inform progress, this method using AFHTTPRequestOperation to tracking progres. Each time a error comes, the request will try execute itself max 3 times.
 *
 *  @param asset           Asset to download
 *  @param progressBlock   Progress block
 *  @param completionBlock Complete block
 *  @param errorBlock      Error block
 */
- (void) downloadAsset: (Asset *) asset
                 withProgress:(void (^)(CGFloat progress))progressBlock
                   completion:(void (^)(void)) completionBlock
                      onError:(void (^)(NSError *error))errorBlock {

    NSString *baseDir = AOFLoginManager.sharedInstance.documentsDir;
    NSString *filename = [self MD5HashForString:asset.uri_full];
    NSString *extension = [[asset.uri_full componentsSeparatedByString:@"."] lastObject];
    NSString *dest = [[NSString alloc] initWithFormat:@"%@/%@.%@", baseDir, filename, extension];
    
    NSURL *assetURL;
    // for testing in staging
    if(nil != asset.uri_full && ([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame))
    {
        assetURL = [[NSURL alloc] initWithString:asset.uri_full];
    }
    else if(nil != asset.uri_full && ([extension isEqualToString:@"mp3"] || [extension caseInsensitiveCompare:@"mp3"] == NSOrderedSame))
    {
        assetURL = [[NSURL alloc] initWithString:asset.uri_full];
    }
    else
    {
        assetURL = [[NSURL alloc] initWithString:@"https://stage.dlpronto.com/system/files/assets/URL-Text-Feb19.pdf"];
        extension = @"pdf";
    }
    
    // PRONTO-29 - Fix Crashes - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: sccollateralname)'
    if([extension isEqualToString:@"pdf"] || [extension caseInsensitiveCompare:@"pdf"] == NSOrderedSame)
        asset.path = dest;
    else if([extension isEqualToString:@"mp3"] || [extension caseInsensitiveCompare:@"mp3"] == NSOrderedSame)
        asset.path = dest;
    
    NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
    [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:dest error:nil];
    
    
    [self performBlock:^(CallbackBlock callback) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:assetURL];
        [request setTimeoutInterval:60];
        
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"] != nil){
            if([[NSUserDefaults standardUserDefaults]valueForKey:@"SimpleSAMLSessionID"] != nil)
            {
                [request setValue:[NSString stringWithFormat:@"SimpleSAMLSessionID=%@; %@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SimpleSAMLSessionID"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
            else{
            [request setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"sessionId"]] forHTTPHeaderField:@"Cookie"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:dest append:NO]];
        
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            CGFloat written = totalBytesRead;
            CGFloat total = totalBytesExpectedToRead;
            CGFloat percentageCompleted = written/total;
            progressBlock(percentageCompleted);
        }];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (callback) {
                    callback(nil, responseObject);
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (callback) {
                NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                callback(errorCode, nil);
            }
        }];
        
        [operation.outputStream open];
        [operation start];
        
    } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
      
        if (error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"ERR: %@", [error description]]];
            errorBlock(error);
        } else {
            if (dest) {
                if (asset) {
                    [self initialDownloadValidate:asset completion:^(BOOL assetNeedsDownload){
                        if(!assetNeedsDownload) {
                            asset.path = dest;
                            [[ZMCDManager sharedInstance] updateAsset:asset success:^{
                                completionBlock();
                            } error:^(NSError * error) {
                                [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset %@", error.localizedDescription]];
                            }];
                        }
                        else
                        {
                            errorBlock(error);
                        }
                    }failure:^(NSString *messageFailure) {
                        errorBlock(error);
                    }];
                }
            }
        }
    }];

}
- (NSString *)MD5HashForString:(NSString *)input {
    
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    
    // Convert NSString into C-string and generate MD5 Hash
    CC_MD5([input UTF8String], (int)[input length], result);
    
    // Convert C-string (the hash) into NSString
    NSMutableString *hash = [NSMutableString string];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [hash appendFormat:@"%02X", result[i]];
    }
    
    return [hash lowercaseString];
}
/*
 * Offline Download of Assets
 */
-(void) offlineDownload: (NSMutableArray *)assetDownloadArray {
    for(Asset * asset in assetDownloadArray) {
        //change already present path
        if(asset.path != nil || ![asset.path  isEqual: @""]) {
            NSString *baseDir = AOFLoginManager.sharedInstance.documentsDir;
            NSString *filename = [self MD5HashForString:asset.uri_full];
            NSString *extension = [[asset.uri_full componentsSeparatedByString:@"."] lastObject];
            NSString *dest = [[NSString alloc] initWithFormat:@"%@/%@.%@", baseDir, filename, extension];
            asset.path = dest;
            
        }
        //check if asset needs download
        [self initialDownloadValidate:asset completion:^(BOOL assetNeedsDownload){
            if(assetNeedsDownload) {
                [AbbvieLogging logInfo:@"Needs Download"];
                
                //keep in track of an array of dictionary
                NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
                if(_initialDownloadAssetArray.count == 0) {
                    _initialDownloadAssetArray = [[NSMutableArray alloc]init];
                }
                BOOL check = NO;
                for(NSDictionary *dict in [_initialDownloadAssetArray copy])
                {
                    if([dict objectForKey:@"assetID"] == [asset valueForKey:@"assetID"])
                    {
                        check = YES;
                        break;
                    }
                }
                if(!check) {
                    [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                    [initialDownloadAsset setObject:@"in_progress" forKey:@"status"];
                    [_initialDownloadAssetArray addObject:initialDownloadAsset];
                    
                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                }

                
            }
        } failure:^(NSString *messageFailure) {
        }];
    }
}

//Assets validate before the initial download
-(void)initialDownloadValidate:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    NSString * uri = [asset valueForKey:@"uri_full"];
    NSString * file_mime = [asset valueForKey:@"file_mime"];
    if (![UIApplication sharedApplication].protectedDataAvailable) {
        [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
        /// This should occur during application:didFinishLaunchingWithOptions:
        //AOFKit Implementation
        NSError *error;
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];
        
        
        if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
        {
            attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
            if (!success)
            [AbbvieLogging logError:@"Set ~/Documents attrsAssetPath NOT successfull"];
        }
        
        /// ....
        NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
        [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
        [AbbvieLogging logError:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
    }
    
    // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
    if (![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && [uri rangeOfString:@"brightcove://"].location == 0 && ![file_mime isEqualToString:@"weblink" ])
    {
        if ([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
            //                _isDownloadignAsset = YES;
            completion(YES);
        }
        else {
            //            _isDownloadignAsset = NO;
            [AbbvieLogging logError:@"The internet connection is not available, please check your network connectivity"];
        }
    }else{
        //Check if the asset size matches the metada info meaning the asset is correct
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"** size Asset %d size %d", asset.file_size.intValue, (int)fileSize]];
        if (asset.file_size.intValue == (int)fileSize) {
            //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
            completion(NO);
        }else{
            completion(YES);
        }
    }
}
/**
 *  New Revamping changes - send feedback details to CMS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */
-(void)sendEmailToself:(NSString *)assetId success:(void (^)(NSDictionary *))success error:(void (^)(NSError *))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        //Changes for token
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"aid":assetId,
        kTokenKey:[AOFLoginManager sharedInstance].getAccessToken
        }];
        
         NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.sendEmailToSelfURI];
        
        [[self requestManagerWithToken:NO headers:[NSDictionary new]] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.emailToSelfSuccessResponseTxt, responseObject]];
            
            success((NSDictionary *)success);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.emailToSelfErrorResponseTxt, error]];
            
        }];
    }
}

/*
 * Method to update asset details
 */
- (void)uploadEvents:(void(^)(void))complete error:(void(^)(NSError *error))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        //AOFKit Implementation
        NSString* franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;

        NSArray *events = [[ZMAssetsLibrary defaultLibrary] events:@"JSON" withSaleFranchiseId:franchiseId];
        
        if ([events count] > 0) {
            
            NSMutableArray *toJSON = [NSMutableArray array];
            for (ZMEvent * event in events){
                
                //if value is not available, May be api fails and it will be taken care next time.
                NSString *sf = event.sf != nil ? event.sf : @"";
                NSString *eventValue = event.eventValue != nil ? event.eventValue : @"";
               
                NSDictionary *eventToSend = [NSDictionary dictionaryWithObjectsAndKeys:
                                             [NSString stringWithFormat:@"%ld",event.assetId], @"id",
                                             [NSString stringWithFormat:@"%ld",event.eventType], @"type",
                                             eventValue, @"value",
                                             [NSString stringWithFormat:@"%ld", event.eventDate],@"date",
                                             [NSString stringWithFormat:@"%ld",event.createdFromBucketId], @"bid",
                                             sf,@"sf", nil];
                [toJSON addObject:eventToSend];
            }
            
            NSError *err;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:toJSON options:0 error:&err];
            NSString *JSONToPost = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObjectIfValueAvailable:[AOFLoginManager sharedInstance].getAccessToken forKey:kTokenKey];
            [params setObjectIfValueAvailable:JSONToPost forKey:@"events"];
           
            
            NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.uploadAssetViewURL];
            
            [self performBlock:^(CallbackBlock callback) {
                
                [[self requestManagerWithToken:YES headers:[NSDictionary new]] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (callback) {
                            callback(nil, responseObject);
                        }
                    });
                    
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    if (callback) {
                        NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                        callback(errorCode, nil);
                    }
                    
                }];
                
            } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
               
                if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error code %ld , Error Description %@ on WS notification.uri", (long)error.code, error.localizedDescription]];
                    
                    if (error.code == serverAlert107 ) {
                        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
                    }else if (error.code == serverAlert401 || [ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code]) {
                        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                    }else {
                        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:serverAlert000 alternateMessage:ProntoRedesignAPIConstants.userInformationErrorMsg]);
                    }

                }else{
                
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"WS Upload Events Response: %@", response]];
                    
                    for (ZMEvent *event in events) {
                        [[ZMAssetsLibrary defaultLibrary] deleteEvent:event];
                    }
                    
                    complete();
                }
            }];

        }
        else {
            complete();
        }
    }
}
-(NSString*)setRating:(NSDictionary*)rating{
    return [rating valueForKey:@"votes"];
}

// PRONTO-1 - Likes not Working
-(NSString*)setRatingUserVote:(NSDictionary*)rating{
    return [rating valueForKey:@"user_vote"];
}
/**
 *  Format JSON Asset to get struct to mapping data into CoreData
 *
 *  @param assets Asset Dictionary
 *
 *  @return new Asset format
 */

-(NSDictionary*)formatAssets:(NSDictionary*)assets{
    
    NSMutableArray * assetObject = [[NSMutableArray alloc]init];
    NSMutableDictionary * newAssets = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary * assetsToFormat = [[NSMutableDictionary alloc]initWithDictionary:assets];
    [assetsToFormat removeObjectForKey:@"pages"];
    [assetsToFormat removeObjectForKey:@"unpublished"];
    [assetsToFormat removeObjectForKey:@"deleted"];
    [assetsToFormat removeObjectForKey:@"total"];
    [assetsToFormat removeObjectForKey:@"changed"];
    
    for (id key in assetsToFormat) {
        id asset = [assetsToFormat objectForKey:key];

        NSMutableDictionary * newAssetObject = [[NSMutableDictionary alloc]initWithDictionary:asset];
        [newAssetObject removeObjectForKey:@"field_brand"];
        [newAssetObject removeObjectForKey:@"field_categories"];
        [newAssetObject removeObjectForKey:@"rating"];

        [newAssetObject setObject:[self setRating:[asset valueForKey:@"rating"]] forKey:@"votes"];
        // PRONTO-1 - Likes not Working
        [newAssetObject setObject:[self setRatingUserVote:[asset valueForKey:@"rating"]] forKey:@"user_vote"];
        [assetObject addObject:newAssetObject];
    }
    
    [newAssets setObject:assetObject forKey:@"assets"];
    [newAssets setObject:[assets valueForKey:@"changed"] forKey:@"changed"];
    [newAssets setObject:[assets valueForKey:@"total"] forKey:@"total"];
    [newAssets setObject:[assets valueForKey:@"deleted"] forKey:@"deleted"];
    [newAssets setObject:[assets valueForKey:@"unpublished"] forKey:@"unpublished"];
    [newAssets setObject:[assets valueForKey:@"pages"] forKey:@"pages"];
    
    return newAssets;
}
/**
 *  Mapping Assets into CoreData Database
 *
 *  @param assets  Dictionary with JSON to parse assets
 *  @param success Block success
 *  @param onError Block error
 */


-(void)getAssets:(NSMutableArray *)assetsArray success:(void (^)(NSArray *))success error:(void (^)(NSError *))onError
{

    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"asset to import %@", assetsArray]];
    
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            int i= 0;
            for (NSDictionary * asset in assetsArray) {
                
                NSString * assetId = [asset valueForKey:@"assetID"];
                NSArray * assetArray = [self getAsset:assetId withContext:localContext];
                
                if (assetArray.count == 0)
                {
                    Asset * newAsset = [Asset MR_createEntityInContext:localContext];
                    [newAsset MR_importValuesForKeysWithObject:asset];
                    newAsset.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                }
                else
                {
                    
                    Asset * assetToCheck = assetArray[0];
                    NSArray * brandsInAssetToImport = [asset valueForKey:@"brands"];
                    
                    NSArray * categoiresInAssetToImport = [asset valueForKey:@"categories"];
                    NSString * valueChanged = [asset valueForKey:@"changed"];
                    
                    // PRONTO-1 - Likes not Working
                    NSString * user_vote = [asset valueForKey:@"user_vote"];
                    NSNumber * valueCount = [NSNumber numberWithInt:[[asset valueForKey:@"view_count"] intValue]];
                    NSNumber * valueVotes = [NSNumber numberWithInt:[[asset valueForKey:@"votes"] intValue]];
                    
                    // New Revamping - check the Update Available
                    if(![assetToCheck.changed.stringValue isEqualToString: valueChanged])
                    {
                        NSNumber  *lastUpdated = [NSNumber numberWithInteger:[assetToCheck.changed.stringValue integerValue]];
                        NSNumber  *currentUpdate = [NSNumber numberWithInteger:[valueChanged integerValue]];
                        
                        if(currentUpdate > lastUpdated)
                        {
                            assetToCheck.isUpdateAvailable = YES;
                            [assetToCheck setValue:[NSNumber numberWithBool:YES] forKey:@"isUpdateAvailable"];
                            [asset setValue:[NSNumber numberWithBool:YES] forKey:@"isUpdateAvailable"];
                            [asset setValue:currentUpdate forKey:@"changed"];
                            
                            [[ZMCDManager sharedInstance]updateAsset:assetToCheck success:^{
                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"count -> Asset updated %d ", i]];
                                [[ZMAssetsLibrary defaultLibrary] notifyAssetsChanged];
                                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Succesfully updated Asset with Id%@ and title %@",assetToCheck.assetID,assetToCheck.title]];
                            } error:^(NSError * error) {
                                [AbbvieLogging logError:[NSString stringWithFormat:@"Error at updating asset %@", error.localizedDescription]];
                            }];
                        }
                        else
                        {
                            assetToCheck.isUpdateAvailable = NO;
                            [assetToCheck setValue:[NSNumber numberWithBool:NO] forKey:@"isUpdateAvailable"];
                        }
                    }
                    
                    if (assetToCheck.brands.count != brandsInAssetToImport.count

                        || assetToCheck.categories.count != categoiresInAssetToImport.count
                        || !([assetToCheck.changed.stringValue isEqualToString: valueChanged])
                        || assetToCheck.view_count.intValue != valueCount.intValue
                        || assetToCheck.votes.intValue != valueVotes.intValue
                        || assetToCheck.user_vote.intValue != user_vote.intValue) {
                        
                        [assetToCheck MR_deleteEntityInContext:localContext];
                        [localContext MR_saveToPersistentStoreAndWait];
                        assetToCheck = [Asset MR_createEntityInContext:localContext];
                        assetToCheck.assetID = [NSNumber numberWithInt:[[asset valueForKey:@"assetID"] intValue]];
                        [assetToCheck MR_importValuesForKeysWithObject:asset];
                        
                    }
                }
                i++;
            }

        } completion:^(BOOL contextDidSave, NSError *error) {
            if (error) {
                onError(error);
            }else{
                NSFetchRequest * fetchrequest = [Asset MR_requestAll];
                NSArray *importedCategories = [Asset MR_executeFetchRequest:fetchrequest];
                success(importedCategories);
            }
        }];

}
/**
 *  Get Array to Assets from Asset entity
 *
 *  @param object  assetID to find
 *  @param context Context to performed query
 *
 *  @return Array of Assets
 */

-(NSArray*)getAsset:(NSString*)object withContext:(NSManagedObjectContext*)context{
    NSPredicate *assetFilter = [NSPredicate predicateWithFormat:@"assetID == %@", object];
    NSArray * asset = [Asset MR_findAllWithPredicate:assetFilter inContext:context];
    return asset;
}

@end
