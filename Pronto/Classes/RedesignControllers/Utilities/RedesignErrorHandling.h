//
//  RedesignErrorHandling.h
//  Pronto
//
//  Created by Saranya on 08/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMMessage.h"
NS_ASSUME_NONNULL_BEGIN

@interface RedesignErrorHandling : NSObject<ZMProntoMessagesDelegate>{
    NSURL *_assetToOpenURL;
}
@property (strong,nonatomic) ZMMessage *currentMessage;
+ (RedesignErrorHandling *)sharedInstance;
/**
 *  Shows a messages with a given type and hides after delay
 *  @param message The message to display
 *  @param type the type of the message
 *  @param time to close the message
 */
-(void) showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withTime:(NSTimeInterval)time;
/**
 *  Shows a messages with a given type
 *  @param message The message to display
 *  @param type the type of the message
 */
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type;
- (void)hideMessage;
/**
*  Shows a messages with a given type
*  @param message The message to display
*  @param type the type of the message
*  @param action the action for the retry button
*/
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withAction:(void(^)(void))mAction;
@end
NS_ASSUME_NONNULL_END
