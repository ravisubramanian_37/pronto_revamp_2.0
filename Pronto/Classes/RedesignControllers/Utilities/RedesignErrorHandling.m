//
//  RedesignErrorHandling.m
//  Pronto
//
//  Created by Saranya on 08/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "RedesignErrorHandling.h"
#import "Pronto-Swift.h"

@implementation RedesignErrorHandling

/* Singleton Object Creation */
+ (RedesignErrorHandling *)sharedInstance {
    
    @synchronized(self)
    {
        static RedesignErrorHandling *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
/**
 *  Shows a messages with a given type and hides after delay
 *  @param message The message to display
 *  @param type the type of the message
 *  @param time to close the message
 */
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withTime:(NSTimeInterval)time {
    [self showMessage:message whithType:type];
    
    if(_currentMessage) {
        [self performSelector:@selector(hideMessage) withObject:nil afterDelay:time];
    }
}
/**
 *  Shows a messages with a given type
 *  @param message The message to display
 *  @param type the type of the message
 */
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type {
    
    BOOL alerDisplayed = NO;
    
    if (_assetToOpenURL) {
        
        if ([message isEqualToString:@"The asset was not found"]) {
            
            alerDisplayed = YES;
            
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //[alert show];
            
            // Alternative AlertView
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""  message:message  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle OK button
                                       }];
            
            [alert addAction:okButton];
            UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
                viewController = viewController.presentedViewController;
            }
            [viewController presentViewController:alert animated:YES completion:nil];
            
            
        }
    }
    
    if (!alerDisplayed) {
        
        if(!_currentMessage) {
//            UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
            UINavigationController *topController = [HomeViewController sharedInstance].navigationController;

            
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
                UIViewController* viewController = topController.visibleViewController;
                _currentMessage = [[ZMMessage alloc] initMessage:viewController.view withMessage:message andType:type];
            }
            else {
                _currentMessage = [[ZMMessage alloc] initMessage:topController.view withMessage:message andType:type];
            }
            _currentMessage.delegate = self;
        }
        else {
            _currentMessage.type = type;
            _currentMessage.message = message;
        }
    }
}

/**
 *  Hides the message
 */
- (void)hideMessage {
    if(_currentMessage) {
        
        [_currentMessage hideMessage:^(BOOL finished) {
            [_currentMessage removeFromSuperview];
            _currentMessage = nil;
        }];
    }
}
# pragma mark - ZMProntoMessagesDelegate

- (void) messageRemoved {
    
    _currentMessage = nil;
}
/**
 *  Shows a messages with a given type
 *  @param message The message to display
 *  @param type the type of the message
 *  @param action the action for the retry button
 */
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withAction:(void(^)(void))mAction {
    
    if (!_currentMessage) {
//        UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
        UINavigationController *topController = [HomeViewController sharedInstance].navigationController;
        
        _currentMessage = [[ZMMessage alloc] initMessage:topController.view withMessage:message andType:type withAction:mAction];
        _currentMessage.delegate = self;
    }
}
@end
