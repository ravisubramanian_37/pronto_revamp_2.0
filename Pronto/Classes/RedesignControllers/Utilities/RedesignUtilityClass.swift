//
//  RedesignUtilityClass.swift
//  Pronto
//
//  Created by Saranya on 20/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc public class RedesignUtilityClass: NSObject {
  
  @objc static let sharedInstance = RedesignUtilityClass()
  
  // Set Shadow for any element
  func setShadow<T:UIView>(view:inout T){
    
    view.layer.borderWidth = 0.0
    view.layer.shadowColor = UIColor.lightGray.cgColor
    view.layer.shadowOffset = CGSize(width: 0, height: 0)
    view.layer.shadowRadius = 5.0
    view.layer.shadowOpacity = 1
    view.layer.masksToBounds = false
  }
  
  // Invoke Home Screen
  @objc func invokeHomeScreen(withNav: UINavigationController?){
    if let navController = withNav {
      if (navController.viewControllers .contains(HomeViewController.sharedInstance)) {
        navController.popToViewController(HomeViewController.sharedInstance, animated: false)
        HomeViewController.sharedInstance.viewWillAppear(false)
      } else {
        navController.pushViewController(HomeViewController.sharedInstance, animated: true)
      }
    }
    else {
      let navController  = UINavigationController.init(rootViewController: HomeViewController.sharedInstance)
      presentViewControllerFromWindow(viewController: navController, animated: true)
    }
  }
  // Save details in user defaults
  @objc func saveInUserDefaultsForString(key: NSString, value: NSString) {
    UserDefaults.standard.set(value, forKey: key as String)
    UserDefaults.standard.synchronize()
  }
  // Present any controller
  @objc public func presentViewControllerFromWindow(viewController: Any?, animated: Bool) {
    DispatchQueue.main.async{
      UIApplication.shared.keyWindow?.rootViewController?.present(viewController as! UIViewController, animated: animated, completion: nil)
    }
  }
  // Show laert
  public func showMessageAsAlert(title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert);
    //no event handler (just close dialog box)
    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style:.cancel, handler: nil));
    presentViewControllerFromWindow(viewController: alert, animated: true)
  }
  // Create Empty View with given text
  func createEmptyViewWithText(text: String, withFontSize:CGFloat?) -> UIView {
    let emptyView: UIView = UIView()
    emptyView.backgroundColor = .white
    
    let label:UILabel = UILabel()
    emptyView.addSubview(label)
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = text
    label.leadingAnchor .constraint(equalTo: emptyView.leadingAnchor, constant: 5).isActive = true
    label.trailingAnchor .constraint(equalTo: emptyView.trailingAnchor, constant: -5).isActive = true
    label.heightAnchor .constraint(greaterThanOrEqualToConstant: 64).isActive = true
    label.centerYAnchor .constraint(equalTo: emptyView.centerYAnchor).isActive = true
    
    label.backgroundColor = .white
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    label.textAlignment = .center
    if let fontSize = withFontSize{
      label.font = UIFont.systemFont(ofSize: fontSize)
    } else {
      label.font = UIFont.systemFont(ofSize: 12)
    }
    
    return emptyView
  }
  /* To sort given array */
  func sortAssetsArray(isFromSearch: Bool, assetsArray: [Asset]) -> [AnyObject]? {
    var assets:[AnyObject] = NSMutableArray() as [AnyObject]
    ZMCDManager.sharedInstance.getAssetsWithFilterForNewImplementation(isFromSearch: isFromSearch, assetsArray: assetsArray) { (resultsArray) in
      assets = resultsArray
    }
    return assets
  }
  // New Sort Method - Written By Nandana
  func sortcategoryArray(isFromSearch: Bool, assetsArray: [Asset]) -> [AnyObject]? {
    var assets: [AnyObject] = NSMutableArray() as [AnyObject]
    var sortingKey: String?
    if (isFromSearch) {
      sortingKey = UserDefaults.standard.value(forKey: RedesignConstants.searchSortingTermKey) as? String
      
    } else {
      sortingKey = UserDefaults.standard.value(forKey: RedesignConstants.sortingTermKey) as? String
    }
    let sortingTerm: String = sortingKey!
    //var keySortDescriptor = String()
    switch sortingTerm {
    case RedesignConstants.SortingOptions.recentlyAdded.rawValue:
      //  keySortDescriptor = "publish_on"
      assets = assetsArray.sorted {
        return $0.publish_on!.compare($1.publish_on!) == .orderedDescending
      }
    case RedesignConstants.SortingOptions.mostViewed.rawValue:
      //  keySortDescriptor = "view_count"
      assets = assetsArray.sorted {
        return $0.view_count!.compare($1.view_count!) == .orderedDescending
      }
    case RedesignConstants.SortingOptions.recentlyUpdated.rawValue:
      // keySortDescriptor = "changed"
      assets = assetsArray.sorted {
        return $0.changed!.compare($1.changed!) == .orderedDescending
      }
    default:
      break;
    }
    return assets
    
  }
  /* To get topcontroller */
  @objc func getTopController() -> UIViewController? {
    if var topController = UIApplication.shared.keyWindow?.rootViewController {
      while let presentedViewController = topController.presentedViewController {
        topController = presentedViewController
      }
      return topController
    }
    return nil
  }
  /* To show loading indicator */
  @objc func showLoadingIndicator(inViewController inViewCtr: UIViewController?) {
    if let showInViewController = inViewCtr {
      showInViewController.showSpinner(onView: showInViewController.view)
    }else{
      if let topController = getTopController() {
        topController.showSpinner(onView: topController.view)
      }
    }
  }
  /* Remove Loading Indicator */
  @objc func removeLoadingIndicatorFromTopController() {
    if let topController = getTopController() {
      topController.removeSpinner()
    }
  }
}

/* Extension to add constraints(Pin edges to its super View) to any View*/
@objc extension UIView {
    @objc func pinEdges(to other: UIView) {
        leadingAnchor.constraint(equalTo: other.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: other.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: other.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: other.bottomAnchor).isActive = true
    }
}

/* Extension for UIPageControl */
class ProntoPageControl: UIPageControl {
    var borderColor: UIColor = .gray

    override var currentPage: Int {
        didSet {
            updateBorderColor()
        }
    }
    func updateBorderColor() {
        subviews.enumerated().forEach { index, subview in
            if index != currentPage {
                subview.layer.borderColor = borderColor.cgColor
                subview.layer.borderWidth = 1
            } else {
                subview.layer.borderWidth = 0
            }
        }
    }
    
}
/* To create copy of button Object */
extension UIButton {
    
    /// Creates a duplicate of the terget UIButton
    /// The caller specified the UIControlEvent types to copy across to the duplicate
    ///
    /// - Parameter controlEvents: UIControlEvent types to copy
    /// - Returns: A UIButton duplicate of the original button
    func duplicate() -> UIButton? {
        
        // Attempt to duplicate button by archiving and unarchiving the original UIButton
        let archivedButton = NSKeyedArchiver.archivedData(withRootObject: self)
        guard let buttonDuplicate = NSKeyedUnarchiver.unarchiveObject(with: archivedButton) as? UIButton else { return nil }
        
        // Copy targets and associated actions
        self.allTargets.forEach { target in
                self.actions(forTarget: target, forControlEvent: .touchUpInside)?.forEach { action in
                    buttonDuplicate.addTarget(target, action: Selector(action), for: .touchUpInside)
            }
        }
        
        return buttonDuplicate
    }
}

var vSpinner : UIView?
 
extension UIViewController {
  func showSpinner(onView: UIView) {
    let spinnerView = UIView.init(frame: onView.bounds)
    spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
    let ai = UIActivityIndicatorView.init(style: .whiteLarge)
    ai.startAnimating()
    ai.center = spinnerView.center
    
    DispatchQueue.main.async {
      spinnerView.addSubview(ai)
      onView.addSubview(spinnerView)
    }
    
    vSpinner = spinnerView
  }
  
  func removeSpinner() {
    DispatchQueue.main.async {
      vSpinner?.removeFromSuperview()
      vSpinner = nil
    }
  }
  func isVisible() -> Bool {
    return self.isViewLoaded && (self.view.window != nil)
  }
}
