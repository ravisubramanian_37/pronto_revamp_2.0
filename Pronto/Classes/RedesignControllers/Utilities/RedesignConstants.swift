//
//  RedesignConstants.swift
//  Pronto
//
//  Created by Saranya on 13/03/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
@objc class RedesignConstants: NSObject {
    
    // HomeView
    static let homeScreenCollectionRows = 10
    static let homeScreenCollectionColumns = 10
    static let widthBetweenHomeCollectionCellsLandscape = 4
    static let leftPaddingHomeViewLandscape = 7
    static let pageControlHeight:CGFloat = 50.0
    // Settings PopOver
    static let settingsHeight = 180
    static let settingsWidth = 170
    static let numberOfRowsInSettingsForHomeUser = 4
    static let numberOfRowsInSettingsForFieldUser = 3
    static let defaultAssetId = "999"
    // Sorting PopOver
    static let sortingTableHeight = 150
    static let sortingTableBufferX:CGFloat = 100.0
    static let sortingTableBufferY:CGFloat = 80.0
    static let cellId = "Cell"
    static let numberOfRowsInSortingTable = 3
    static let sortingTermKey = "sortingTerm"
    static let searchSortingTermKey = "searchSortingTerm"
    static let searchText = "searchText"


    // HomeView - Titles
    static let titleString = "Title"
    static let favouritesTitle = "My Favorites"
    static let eventsTitle = "Events"
    static let homeTitle = "Home"
    static let assetsTxt = " Assets"
    static let assetTxt = " Asset"
    // Subfolder View - Titles
    static let subFolderTitleString = "Categories"
    // Asset Options
    static let assetsExportPopOverWidth = 150
    // nib file names
    static let mainHomeNibName = "HomeViewMainScreen"
    static let settingsTableViewCellNibName = "SettingsTableViewCell"
    static let settingsTitleCellNibName = "SettingsTableViewTitleCell"
    static let promotedContentNib = "PromotedContentView"
    static let categoriesNib = "CategoryView"
    static let categoryHeaderNib = "CategoryHeaderView"
    static let myFavouritesDetailsViewNib = "MyFavouritesDetailsView"
    @objc static let mainViewHeaderNib = "HomeMainHeader"
    static let myFavouritesHomeViewNib = "MyFavouritesHomeView"
    static let eventsMainViewNib = "HomeViewEventsMain"
    static let eventsHomeViewNib = "EventsHomeView"
    static let collectionCellNibName = "homeScreenCollectionViewCell"
    static let collectionCellId = "collectionCell"
    static let collectionListCellId = "collectionListCellId"
    static let feedBackControllerNibName = "FeedbackViewController"
    static let subfolderCollectionCellNib = "SubFolderListCollectionCell"
    @objc static let notificationsNib = "Notifications"
    static let franchiseCellNibName = "FranchiseTableViewCell"
    static let collectionGridAssetNibName = "AssetItem"
    static let myFavouritesBackButtonNibName = "MyFavoSubheaderBackButton"
    
    // Favourites
    // static let noAssetFavouritesText = "No Assets being added in Favourites.Kindly mark assets as favourites to view here."
    static let noAssetFavouritesText = "You need to add a favourite asset first in order to see content."
    static let myFavouritesDetailsViewNibName = "MyFavoDetailsView"
    
    // Search Results View
    static let searchResultsViewNibName = "SearchResultsVieww"
    static let searchResultsCountText = " Assets Match"
    static let zeroSearchResultsCountText = " Asset Match"
    static let zeroSearchResultText = "No Matching Results found for the given search text"

    
    // Promoted Contents
    static let noPromotedContentText = "There are no promoted content for selected Franchise."
    static let promotedContentServerError = "Server connection is not available for displaying promoted content, please try again later."
    // Events
    static let noEventsContentText = "No Events for the Selected Franchise"
    @objc static let noEventsContentInDetailScreen = "There are no events for selected franchise. Please come back later"
    // Asset options
    static let pdfName = "pdf"
    static let videoName = "video"
    static let weblinkName = "Weblink"
    static let audioLinkName = "Podcast_Background"
    static let centerImageAudio = "Podcast"
    static let centerImagePDF = "Pdf_Home"
    static let centerImageVideo = "Video_Home"
    static let centerImageWeblink = "WebLink_home"
    
    @objc static let favoUnSelected = "Favo_unselected"
    @objc static let favoSelected = "Favo_selected"
    @objc static let export_redesign = "Export_redesign"
    @objc static let export_redesign_selected = "export_selected"
    static let info_redesign = "Info_redesign"
    static let info_redesign_selected = "info_selected"
    
    static let sccollateralnameKey = "sccollateralname"
    static let sccollateralfeaturesKey = "sccollateralfeatures"
    static let filterKey = "filter"
    static let scfiltersubcategoryKey = "scfiltersubcategory"
    static let clickThroughKey = "click-through"
    static let scfilteractionKey = "scfilteraction"
    static let sctierlevelKey = "sctierlevel"
    static let tier2Key = "tier2"
    static let scsearchclickthroughfilenameKey = "scsearchclickthroughfilename"
    static let assetViewKey = "assetview"
    
    // Grid View
    @objc static let brightCoveText = "BrigthcoveUpdated"
    @objc static let videoText = "Video"
    
    // Info Menu
    static let infoMenuBundleName = "InfoMenu"
    static let dateFormat = "dd/MM/yyyy"
    static let infoOption_No_Of_Views = "Number Of Views"
    static let infoOption_Meg_Reg_Number = "Med/Reg Number"
    static let infoOption_Last_Updated = "Last Updated"
    static let infoOption_Exp_Date = "Expiration Date"

    // Export Menu
    static let exportMenuBundleName = "ExportMenu"
    static let exportMenu_Print = "Print"
    static let exportMenu_Share = "Share"
    static let exportMenu_Email_To_Self = "Email to self"
    static let exportMenu_Feedback = "Feedback"
    
    // imageName
    static let nextItemImageNormal = "itemArrow"
    static let nextItemImageDisabled = "Next Arrow"
    static let listImage = "New_List"
    static let gridImage = "Menu_grid"

    // Settings
    static let commonInitError = "init(coder:) has not been implemented"
    static let settingsCellIdentifier = "settingsIdentifier"
    static let settingsTitleCellIdentifier = "settingsTitleCellIdentifier"
    @objc static let settingsOptionForAsset = "isAssetOpenCase"
    
    static let prontoAofKitPath = Bundle.main.path(forResource: "AOFKitDetails", ofType: "plist")
    // Color code for My Favo & Events HomeHeaderView
    static let myFavoEventsHomeHeaderTextColor = "#0D1C49"
    
    // Notification Constants
    @objc static let notificationforOrientationChange = "OrientationChange"
    @objc static let notificationforListAndGridChange = "ListAndGridChange"
    @objc static let removeSelectedFavouritesNotification = "removeSelectedFavouritesNotification"
    static let logOutNotification = "logOutNotification"
    @objc static let preferencesReadyNotification = "PreferencesReadyNotification"
    @objc static let totalNotificationLibraryTxt = "TOTAL_NOTIFICATIONS"
    @objc static let lastNotificationUpdateLibraryText = "NOTIFICATIONS_LASTUPDATE"
    static let sortingReloadNotification = "SortingReloadNotification"
    static let searchSortingReloadNotification = "SeachSortingReloadNotification"

    static let clearSearchTextNotification = "clearSearchTextNotification"
    @objc static let favouritesRemovalNotification = "FavouritesRemovalNotification"
    static let removeFavViewSearchListNotification = "removeFavViewSearchListNotification"
    static let removeFavViewControllerSearchListNotification = "removeFavViewControllerSearchListNotification"
    @objc static let hideBottomViewNotification = "hideBottomViewNotification"
    static let removeCategoryViewSearchListNotification = "removeCategoryViewSearchListNotification"
    static let removeCategoryViewControllerSearchListNotification = "removeCategoryViewControllerSearchListNotification"
    static let removeSearchResultsListNotification = "removeSearchResultsListNotification"
    static let removeSearchResultsVCListNotification = "removeSearchResultsVCListNotification"
    @objc static let favouritesAssetUpdationNotification = "FavouritesAssetUpdationNotification"
    @objc static let subjectKey = "subject"
    @objc static let changedKey = "changed"
    @objc static let deletedKey = "deleted"
    @objc static let MsgsTitleKey = "Messages"
    @objc static let okText = "OK"
    @objc static let threadIdKey = "thread_id"
    @objc static let bodyKey = "body"
    @objc static let typeKey = "type"
    @objc static let assetKey = "asset"
    @objc static let lastUpdatedKey = "last_updated"
    @objc static let UrlKey = "Url"
    @objc static let urlKey = "url"
    @objc static let appKey = "app"
    @objc static let undKey = "und"
    
    // Mail Composer
    @objc static let mailSentSuccess = "Your mail has been sent successfully"
    @objc static let mailConnectionError = "Your mail will be sent successfully once the network connection is available"
    @objc static let mailSentError = "Message sending failed."
    // Info PopOver
    @objc static let infoID = "InfoMenuCell"
    @objc static let infoFontName = "Lato-Regular"

    // Initial load flags
    @objc static let isPreferenceRequired = "isPreferencesViewRequired"
    @objc static let isLoadingFirstTime = "isLoadingFirstTime"
    @objc static let baseFranchiseID = "baseFranchiseID"
    @objc static let isAppAlreadyLaunchedOnce = "isAppAlreadyLaunchedOnce"
    @objc static let firstTimeloadingAssets = "firstTimeloadingAssets"
    
    // AOFKit Constants
    static let clientIdKey = "ClientID"
    static let scopeKey = "Scope"
    static let redirectKey = "RedirectURI"
    static let grantTypeKey = "GrantType"
    static let loginTitle = "Login"
    static let bearerTxt = "Bearer"
    static let authorizationTxt = "Authorization"
    static let loginStatus = "scloginStatus"
    static let unsuccessfulTxt = "unsuccessful"
    
    // Messages
    static let loginMsg = "Checking your login credentials. Please wait..."
    static let refetchTokenMsg = "Trying to refetch access token."
    static let tokenSuccess = "Refething access token is success, navigating to home view."
    static let accessTokenKey = "access_token"
    static let refetchTokenFailure = "Refething access token is failed, error:"
    static let fetchProfileFailedMsg = "Fetching Profile Details Failed:"
    static let fetchUserDetailsErrorMsg = "Error while getting user details from Server and error is"
    static let userDetails = "User Details:"
    static let errorInUserData = "Unable to get user Profile data from server"
    static let errorInDataFetching = "Unable to get data from server - AOF kit"
  
  // Flow Types
  @objc static let eventsFlow = "events"
  @objc static let searchFlow = "search"
  
    
    static let utilityVar: AppUtilities = AppUtilities.sharedUtilities() as! AppUtilities
     // Enum for image in category folders
     enum ImageType: String {
         case newAssetCategory = "New Assets"
         case updatedAssetCategory = "Updated Assets"
         case normalCategory = "Folder icon"
         case moreCategory = "More Categories"
     }
     // Enum for title in category folders
     enum FolderTitle: String {
         case newAssetCategory = "New Assets"
         case updatedAssetCategory = "Updated Assets"
         case moreCategory = "More Categories"
     }
    // Settings title
    enum SettingsTitle: String {
        case franchise = "Change Franchise"
        case feedback = "Feedback"
        case logout = "Logout"
        case help = "Help"
    }
    // User Type
    enum UserType: String {
        case inHouse = "In House"
        case field = "Field"
    }
    enum SettingsOptionsImage: String {
        case userIcon = "userIcon"
        case chnageFranchise = "switch"
        case feedback = "feedbackIcon"
        case logOut = "logout"
        case help = "Question Mark"
    }
    // Sorting Options
    enum SortingOptions: String {
           case recentlyAdded = "Recently Added"
           case mostViewed   = "Most Viewed"
           case recentlyUpdated = "Recently Updated"
       }

    // Logout
    static let defaultsDisplayText = "Deleted Userdefaults Data:"
    struct TabBarColors {
        static let franchiseColor = UIColor.white
    }
    // Return Device name
    class func isBigDevice() -> Bool {
        let size: CGSize = UIScreen.main.bounds.size
        let maxValue = max(size.width, size.height)
        if (maxValue < 1366) {
            return false
        } else {
            return true
        }
    }
    // Return Device name
    class func isiPad9_7() -> Bool {
        let size: CGSize = UIScreen.main.bounds.size
        let maxValue = max(size.width, size.height)
        if (maxValue <= 1024) {
            return true
        } else {
            return false
        }
    }
    // Return Device name
      class func isiPad11() -> Bool {
          let size: CGSize = UIScreen.main.bounds.size
          let maxValue = max(size.width, size.height)
          if (maxValue == 1194) {
              return true
          } else {
              return false
          }
      }
    // Return Device name
    class func isiPad10_5() -> Bool {
        let size: CGSize = UIScreen.main.bounds.size
        let maxValue = max(size.width, size.height)
        if (maxValue == 1112) {
            return true
        } else {
            return false
        }
    }
  
  /// Send email using mailto
  /// - Parameters:
  ///   - to: array of email addresses
  ///   - subject: subject
  ///   - body: body
  ///   - isHtml: isHtml (only <br/> and <br /> are supported)
  /// - Returns: true if sent
  @objc class func sendByURL(to:String,subject:String,body:String, isHtml:Bool) -> Bool {
          var txtBody = body
          if isHtml {
              txtBody = body.replacingOccurrences(of: "<br />", with: "\n")
              txtBody = txtBody.replacingOccurrences(of: "<br/>", with: "\n")
              if txtBody.contains("/>") {
                  print("Can't send html email with url interface")
                  return false
              }
          }

//          let toJoined = to.joined(separator: ",")
          guard var feedbackUrl = URLComponents.init(string: "mailto:\(to)") else {
              return false
          }
                   

          var queryItems: [URLQueryItem] = []
          queryItems.append(URLQueryItem.init(name: "SUBJECT", value: subject))
          queryItems.append(URLQueryItem.init(name: "BODY",
                                              value: txtBody))
          feedbackUrl.queryItems = queryItems
          
          if let url = feedbackUrl.url {
  //            This is probably an unnecessary check
  //            You do need to add 'mailto' to your LSApplicationQuerySchemes for the check to be allowed
  //            <key>LSApplicationQueriesSchemes</key>
  //            <array>
  //                <string>mailto</string>
  //            </array>
              if UIApplication.shared.canOpenURL(url){
                  UIApplication.shared.open(url)
                  return true
              }
          }
          
          return false
       
      }

}


extension Bundle {
    var apiBaseURL: String {
        return object(forInfoDictionaryKey: "BASE_URL") as? String ?? ""
    }
}
