//
//  ProntoRedesignSingletonClass.swift
//  Pronto
//
//  Created by Saranya on 18/06/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
@objc class ProntoRedesignSingletonClass: NSObject {
    @objc var audioAssetView: AudioPlayer?
    @objc var audioAsset: Asset?
    @objc var folderDetails: NSArray? = []
    @objc static let sharedInstance = ProntoRedesignSingletonClass()
    var sortingsasetsArray: [Asset]? = []
    var searchSortingAssetsArray: [Asset]? = []
    var sortingEnabled: Bool = true
    var backHeaderButtons: [UIButton]? = []
    var backHeaderButtonTitles: [String] = ["Home"]
    var isPopDoneInCategories: Bool = false
    var favouritesAssetArray: [FavFranchise]? = []
    var promotedContentsArray: [Asset]? = []
    @objc var eventsAllArray: [Event]? = []
    var isListChosen: Bool = false
    @objc var isFavouritesChosen: Bool = false
    @objc var savedPreferences: [Franchise]? = []
    @objc var lastSearchText: String = ""
    @objc var lastFlowType: String = ""
    @objc var savedCategoryIDs: [NSNumber]? = []
    @objc var callPreferencesApi: Bool = true
}
