//
//  settingsTableViewCell.swift
//  Pronto
//
//  Created by Saranya on 06/04/2020.
//  Copyright © 2020 Saranya. All rights reserved.
//

import Foundation
import UIKit
class SettingsTableViewCell: UITableViewCell {
    @IBOutlet weak var settingsImageView: UIImageView!
    @IBOutlet weak var settingsTitleLabel: UILabel!
}
