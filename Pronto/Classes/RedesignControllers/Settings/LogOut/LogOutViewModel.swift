//
//  LogOutViewModel.swift
//  Pronto
//
//  Created by Saranya on 09/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit

class LogOutViewModel: NSObject {
    static let sharedInstance = LogOutViewModel()
    func cleanUpForLogOut() {
        //Logout Service Call
        _ = [SettingsServiceClass.sharedAPIManager() .logOutService({
            AbbvieLogging.logInfo("\(ProntoRedesignAPIConstants.logOutSuccessMsg)")
            print("logout success")
        }, error: { (error) in
            print("logout failure")
            AbbvieLogging.logError("\(ProntoRedesignAPIConstants.logOutErrorMsg)")
        })];
        
        
        //Cleaning data stored in NSUserDefaults
//        let domain = Bundle.main.bundleIdentifier!
//        UserDefaults.standard.removePersistentDomain(forName: domain)
//        UserDefaults.standard.synchronize()
//        print("\(RedesignConstants.defaultsDisplayText) \(Array(UserDefaults.standard.dictionaryRepresentation().keys).count))")
        
//      ZMProntoManager.sharedInstance.cleanAndResetupDB()
        //Cleaning AOF kit
        DispatchQueue.main.async {
            if(AOFLoginManager.sharedInstance.checkForAOFLogin()){
                AOFLoginManager.sharedInstance.AOFKitLogout()
            }
        }
        ZMUserActions.sharedInstance().shoudRefreshEventData = true
        //ViewedAsset
        ProntoUserDefaults.singleton()?.setViewedNormalAssetIds([])
        ProntoUserDefaults.singleton()?.setViewedAssetFetchedFromCMSValue(false)
        ProntoUserDefaults.singleton()?.setViewedEventOnlyAssetIds([])
        ProntoUserDefaults.singleton()?.setEventV3RequestMadeFromCMSValue(false)
        
        (AssetDownloadProvider.sharedAssetDownloadProvider() as AnyObject).clearOnLogout()
        (EventAssetDownloadProvider.sharedEventAssetDownloadProvider() as AnyObject).clearOnLogout()
        ZMUserActions.sharedInstance()?.resetUserActionSharedInstanceValuesOnLogout()
        
        //Reset Preference required bool
        UserDefaults.standard.set(true, forKey: RedesignConstants.isAppAlreadyLaunchedOnce)
        UserDefaults.standard.set(false, forKey: RedesignConstants.isPreferenceRequired)
        UserDefaults.standard.set(false, forKey: RedesignConstants.isLoadingFirstTime)
        ProntoRedesignSingletonClass.sharedInstance.isListChosen = false
        //Reset notification badge number for Application
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //Reset other singleton objects
        ProntoRedesignSingletonClass.sharedInstance.favouritesAssetArray = []
        ProntoRedesignSingletonClass.sharedInstance.backHeaderButtons = []
        ProntoRedesignSingletonClass.sharedInstance.sortingsasetsArray = []
            
        //Removing Push token from Database
        (ProntoInitialSetUpController.sharedInstance() as AnyObject).sessionEnd()
    }

}
