//
//  SettingsServiceClass.m
//  Pronto
//
//  Created by Saranya on 08/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "SettingsServiceClass.h"
#import "ProntoRedesignServerUtilityClass.h"
#import "Pronto-Swift.h"

typedef void (^AssetCompletionBlock) (void);
typedef void (^CallbackBlock)(NSError* error, NSObject* response);

@implementation SettingsServiceClass

/* Singleton Object Creation */
+ (SettingsServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static SettingsServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}

/**
 *  New Revamping changes - send feedback details to CMS
 *
 *  @param complete Block complete
 *  @param onError  Block error
 */
-(void)sendFeedback:(NSString *)assetId type:(NSString*)feedbackType comments:(NSString*)feedbackComments success:(void (^)(NSString *))success error:(void (^)(NSError *))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {
        
        // From Profile, app should send only "App" type
        if(!(assetId.length > 0) || [assetId isEqualToString:@"0"])
        {
            assetId = @"0";
            feedbackType = @"App";
        }
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{ProntoRedesignAPIConstants.nIdParamKey:assetId,
                                        ProntoRedesignAPIConstants.typeKey:feedbackType,
                                        kTokenKey:[AOFLoginManager sharedInstance].getAccessToken,
                                        ProntoRedesignAPIConstants.feedbackKey:feedbackComments
                                                                                             }];
        NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.feedbackURl];
        
        [[super requestManagerWithToken:NO headers:[NSDictionary new]] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.feedbackSuccessResponeseText, responseObject]];
            [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.feedbackSuccessResponeseText responseObj:responseObject];
            success((NSString *)success);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.feedbackErrorResponse, error]];
            [ProntoRedesignServerUtilityClass.sharedInstance printResponse:ProntoRedesignAPIConstants.feedbackErrorResponse responseObj:error];
        }];
    }
    
}
/* Method to get notifications with date - */
- (void)getNotificationsWithDate:(NSString *)date completeBlock:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError{
    
    if ([AOFLoginManager sharedInstance].upi) {

        NSDictionary *params = @{@"page":@"0",
                                        @"per_page":@"80",
                                        @"new":@"1",
                                        kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
               
        
        if (date) {
            params = @{@"page":@"0",
                       @"per_page":@"80",
                       @"new":@"1",
                       @"date":date,
                       kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
        }
        
        NSString *url = [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.fetchNotificationsURL];
        
        [self performBlock:^(CallbackBlock callback) {
                        
            [[super requestManagerWithToken:NO headers:[NSDictionary new]] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (callback) {
                         [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchNotificationSuccessResponeseText, responseObject]];
                        callback(nil, responseObject);
                    }
                });
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if (callback) {
                    NSError* errorCode = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ NSLocalizedDescriptionKey :error.localizedDescription}];
                    [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchNotificationErrorResponse, error]];
                    callback(errorCode, nil);
                }
            }];
            
        } retryingNumberOfTimes:3 onCompletion:^(NSError *error, NSObject *response) {
            
            if (error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchNotificationErrorResponse, error]];
                                    
                if (error.code == serverAlert107 ) {
                     onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.applicationUnavailableErrorMsg]);
                 }else if (error.code == serverAlert401 || [ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code]) {
                     onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:ProntoRedesignAPIConstants.serverConnectionMsg]);
                 }
                 else {
                      onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:error.code alternateMessage:error.localizedDescription]);
                 }
                
            }else
            {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchNotificationSuccessResponeseText, response]];
                complete((NSDictionary *)response);
            }
        }];
    }
}
/* Method to mark notifications as read - */
- (void)markNotificationsAsRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError {

    NSDictionary *params = @{ kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};

    NSString *url = [NSString stringWithFormat:@"%@/%d", [ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.fetchNotificationsURL], notificationId];
    
    [[self requestManagerWithToken:YES headers:[NSDictionary new]] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@: %@",ProntoRedesignAPIConstants.markNotificationsAsReadSuccessResponse,responseObject]];
        complete((NSDictionary *)responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [AbbvieLogging logError:[NSString stringWithFormat:@"%@: %@",ProntoRedesignAPIConstants.markNotificationsAsReadErrorResponse, error]];
        
        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:serverAlert108 alternateMessage:ProntoRedesignAPIConstants.markNotificationsAsReadErrorResponse]);
    }];
}
/* Method to mark notifications as unread - */
- (void)markNotificationsAsUNRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError {

    NSDictionary *params = @{ kTokenKey:[AOFLoginManager sharedInstance].getAccessToken};
    
    NSString *url = [NSString stringWithFormat:@"%@/%d",[ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.markNotificationsAsUnreadURL], notificationId];
    
    [[self requestManagerWithToken:YES headers:[NSDictionary new]] GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.markNotificationsAsUnreadSuccessResponse, responseObject]];
        complete((NSDictionary *)responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [AbbvieLogging logError:[NSString stringWithFormat:@"%@: %@",ProntoRedesignAPIConstants.markNotificationsAsUnreadErrorResponse, error]];
        
        onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:serverAlert108 alternateMessage:ProntoRedesignAPIConstants.markNotificationsAsUnreadErrorResponse]);
    }];
}


/* Service call for  logout - */
- (void)logOutService:(void (^)(void))complete error:(void (^)(NSError *))onError {
    
    NSString *url = [NSString stringWithFormat:@"%@",[ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.logOutServiceURI]];
    
    [[self requestManagerWithToken:YES headers:[NSDictionary new]] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"%@ : %@",ProntoRedesignAPIConstants.logOutSuccessMsg, responseObject]];
        
        complete();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
         onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:serverAlert000 alternateMessage:ProntoRedesignAPIConstants.logOutErrorMsg]);
        
    }];
}
@end
