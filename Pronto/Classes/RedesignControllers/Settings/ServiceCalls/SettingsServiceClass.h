//
//  SettingsServiceClass.h
//  Pronto
//
//  Created by Saranya on 08/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProntoRedesignAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingsServiceClass : ProntoRedesignAPIManager
+ (SettingsServiceClass *)sharedAPIManager;
-(void)sendFeedback:(NSString *)assetId type:(NSString*)feedbackType comments:(NSString*)feedbackComments success:(void (^)(NSString *))success error:(void (^)(NSError *))onError;
- (void)getNotificationsWithDate:(NSString *)date completeBlock:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError;
- (void)markNotificationsAsRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError;
- (void)markNotificationsAsUNRead:(int)notificationId complete:(void(^)(NSDictionary *notifications))complete error:(void(^)(NSError *error))onError;
- (void)logOutService:(void (^)(void))complete error:(void (^)(NSError *))onError;
@end

NS_ASSUME_NONNULL_END
