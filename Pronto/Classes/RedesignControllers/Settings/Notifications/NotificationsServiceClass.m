//
//  NotificationsServiceClass.m
//  Pronto
//
//  Created by Saranya on 30/03/2021.
//  Copyright © 2021 Abbvie. All rights reserved.
//

#import "NotificationsServiceClass.h"
#import "Pronto-Swift.h"

@implementation NotificationsServiceClass

/* Singleton Object Creation */
+ (NotificationsServiceClass *)sharedAPIManager {
    
    @synchronized(self)
    {
        static NotificationsServiceClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}

/* Method for Registering Access Token - Adding the push token to Database */
- (void)registerToken:(NSString *)deviceToken complete:(void(^)(void))complete error:(void(^)(NSError *error))onError {
    
    if ([AOFLoginManager sharedInstance].upi) {

        NSDictionary *params = @{ProntoRedesignAPIConstants.accessTokenTxt:[AOFLoginManager sharedInstance].getAccessToken,
        ProntoRedesignAPIConstants.tokenTxt: deviceToken,
        ProntoRedesignAPIConstants.typeKey:ProntoRedesignAPIConstants.typeValueTxt};
        
        NSString *url = [NSString stringWithFormat:@"%@%@",[ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.registerTokenURL], deviceToken];
        
        
        [[self requestManagerWithToken:YES headers:[NSDictionary new]] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            complete();
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:serverAlert000 alternateMessage:ProntoRedesignAPIConstants.apnsRegistrationError]);
            
        }];
    }
}
/* Method for Unregistering Access Token - Removing the push token from Database */
- (void)unRegisterTokenNoSession:(NSString *)deviceToken complete:(void (^)(void))complete error:(void (^)(NSError *))onError {
    
    NSString *url = [NSString stringWithFormat:@"%@%@",[ProntoRedesignServerUtilityClass.sharedInstance getServiceResourceUrl:ProntoRedesignAPIConstants.unRegisterTokenURL], deviceToken];
    
    [[self requestManagerWithToken:YES headers:[NSDictionary new]] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        complete();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
         onError([ProntoRedesignServerUtilityClass.sharedInstance getErrorWithCode:serverAlert000 alternateMessage:ProntoRedesignAPIConstants.unRegisterPushTokenErrorMsg]);
        
    }];
}
@end
