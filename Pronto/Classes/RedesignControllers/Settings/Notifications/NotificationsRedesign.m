//
//  NotificationsRedesign.m
//  Pronto
//
//  Created by Saranya on 28/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "NotificationsRedesign.h"
#import "SettingsServiceClass.h"
#import "ZMAssetsLibrary.h"
#import "Pronto-Swift.h"

@implementation NotificationsRedesign
HomeMainHeaderView * homeHeaderView;

/* Singleton Object Creation */
+ (NotificationsRedesign *)sharedInstance {
    
    @synchronized(self)
    {
        static NotificationsRedesign *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}

/* Method called on tap of notification */
-(void)didTapNotificationButton:(UIView *)parent {
    UIView *backgroundView;
    parentView = parent;
    if (!notificationsPannel.parent)
    {
        notificationsPannel = nil;
        backgroundView      = nil; // check whether we need to remove
        notificationsPannel = (ZMNotifications *)[[[NSBundle mainBundle] loadNibNamed:RedesignConstants.notificationsNib owner:self options:nil] objectAtIndex:0];
        backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [notificationsPannel initWithParent:parentView background:backgroundView];
        [notificationsPannel populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
        notificationsPannel.delegate = (id<notificationDelegate>)self;
        //Show arrow delayed
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self showHideArrow:NO];
        });
        
        [notificationsPannel adjustNotifications];
    }
}
//Get total notifications for badge count
- (void)geTotalOfNotifications
{
    int total = [[ZMAssetsLibrary defaultLibrary] getTotalUnreadNotifications];
    [[ZMAssetsLibrary defaultLibrary] setProperty:RedesignConstants.totalNotificationLibraryTxt value:[NSString stringWithFormat:@"%d", total]];
    [self changeBadgeNumber:total header:HomeMainHeaderView.sharedInstance];
    
}
/* Delegate to chnage badge number */
-(void)notification:(ZMNotifications*)notificationView actionForChangeBadgeNumber:(int)badgeNumber
{
    [self changeBadgeNumber:badgeNumber header:HomeMainHeaderView.sharedInstance];
}
/* Delegate to close notification window */
-(void)notification:(ZMNotifications*)notificationView didCloseLayout:(BOOL)closeLayout
{
    [self showHideArrow:closeLayout];
}
/* Method to hide/show the arrow when notification is open/close */
- (void)showHideArrow:(BOOL) hide {
    if (!hide) {
        homeHeaderView.notificationArrow.hidden = NO;
        [homeHeaderView showHideBadgeWithValue:YES];
    }
    else {
        homeHeaderView.notificationArrow.hidden = YES;
        [homeHeaderView showHideBadgeWithValue:NO];

    }
}
/* Method to fetch all notifications */
- (void)getNotifications{
    homeHeaderView = [HomeMainHeaderView sharedInstance];
    [self loadNotification];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [SettingsServiceClass.sharedAPIManager getNotificationsWithDate:[[ZMAssetsLibrary defaultLibrary] property:RedesignConstants.lastNotificationUpdateLibraryText] completeBlock:^(NSDictionary *notifications) {
            
            //@Fix solving issues with quotes
            NSArray *keys = [notifications allKeys];
            NSMutableDictionary *formatedNotifications = [[NSMutableDictionary dictionaryWithDictionary:notifications] mutableCopy];
            
            for (NSString *key in keys) {
                
                if ([[formatedNotifications objectForKey:key] isKindOfClass:[NSDictionary class]]) {
                    
                    if ([[formatedNotifications objectForKey:key] objectForKey:RedesignConstants.subjectKey]) {
                        
                        NSMutableDictionary *notif = [[formatedNotifications objectForKey:key] mutableCopy];
                        NSString *subject = [notif objectForKey:RedesignConstants.subjectKey];
                        subject = [subject stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
                        [notif setObject:subject forKey:RedesignConstants.subjectKey];
                        [formatedNotifications setObject:notif forKey:key];
                    }
                }
            }
            
            [self saveNotifications:(NSDictionary *) formatedNotifications];
            [self geTotalOfNotifications];
            [AbbvieLogging logDebug:[NSString stringWithFormat:@"%@%@",ProntoRedesignAPIConstants.fetchNotificationSuccessResponeseText,notifications]];
        } error:^(NSError *error) {
            [AbbvieLogging logError:ProntoRedesignAPIConstants.fetchNotificationErrorResponse];
        }];
        
    });
}
/* Method to load notifications */
- (void)loadNotification{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (notificationsPannel) {
            [notificationsPannel populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
        }
    });
    
    int totalNotifications = 0;
    if ([[ZMAssetsLibrary defaultLibrary] property:RedesignConstants.totalNotificationLibraryTxt]) {
        totalNotifications = [[[ZMAssetsLibrary defaultLibrary] property:RedesignConstants.totalNotificationLibraryTxt] intValue];
    }
    
    [self changeBadgeNumber:totalNotifications header:homeHeaderView];
    _notificationsLoaded = YES;
}
/* Method to change badge number */
-(void) changeBadgeNumber:(int)badgeNumber header:(HomeMainHeaderView *) headerForBadge
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNumber];
        if (badgeNumber < 0) {
            [headerForBadge setBadgeNumberWithBadgeNumber:0];
        }
        [headerForBadge setBadgeNumberWithBadgeNumber:badgeNumber];
    });
}
/* Method to save notifications */
- (void)saveNotifications:(NSDictionary *) notifications {
    
    for (NSDictionary *item in notifications) {
        
        if ([item isKindOfClass:[NSDictionary class]]) {
            [self saveNotification:item];
        }
        else {
            
            if([item isEqual:RedesignConstants.changedKey]){
                [[ZMAssetsLibrary defaultLibrary] setProperty:RedesignConstants.lastNotificationUpdateLibraryText value:[notifications objectForKey:item]];
            }
            else if(![item isEqual:RedesignConstants.deletedKey]){
                [self saveNotification:[notifications objectForKey:item]];
            }
            else {
                //Prepare to delete expired or deleted notifications
                NSArray *notificationsToRemove = (NSArray *)[notifications objectForKey:item];
                NSString *message;
                for (NSString *notificationId in notificationsToRemove){
                    [[ZMAssetsLibrary defaultLibrary] deleteNotification:[notificationId intValue]];
                    
                    if (notificationsPannel) {
                        message = ProntoRedesignAPIConstants.notificationExpired;
                    }
                }
                if (message && notificationsPannel != nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:RedesignConstants.MsgsTitleKey  message:message preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* okButton = [UIAlertAction
                                                   actionWithTitle:RedesignConstants.okText
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Handle OK button
                                                       
                                                   }];
                        
                        [alert addAction:okButton];
                        [RedesignUtilityClass.sharedInstance presentViewControllerFromWindowWithViewController:alert animated:YES];
                    });
                }
            }
        }
    }
    
    _notificationsLoaded = YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (notificationsPannel != nil) {
            [notificationsPannel populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
        }
    });
}

/* Method to verify saved notifcations */
- (void)saveNotification:(NSDictionary *)not {
    ZMNotification *notification;
    ZMNotification *verifyNotification;
    verifyNotification = [[ZMAssetsLibrary defaultLibrary] selectNotification:[[not objectForKey:RedesignConstants.threadIdKey] integerValue]];
    
    if (verifyNotification.notificationId != [[not objectForKey:RedesignConstants.threadIdKey] integerValue]) {
        notification = [[ZMNotification alloc] init];
        notification.notificationId = [[not objectForKey:RedesignConstants.threadIdKey] integerValue];
        notification.subject = [not objectForKey:RedesignConstants.subjectKey];
        notification.message = [not objectForKey:RedesignConstants.bodyKey];
        notification.action = [not objectForKey:RedesignConstants.typeKey];
        notification.assetId = [[not objectForKey:RedesignConstants.assetKey] integerValue];
        notification.date = [not objectForKey:RedesignConstants.lastUpdatedKey];
        notification.viewed = 0;
        
        if ([[not objectForKey:RedesignConstants.typeKey] isEqualToString:RedesignConstants.UrlKey]) {
            NSArray *urlInfo = [[not objectForKey:RedesignConstants.urlKey] objectForKey:RedesignConstants.undKey];
            notification.url = [[urlInfo objectAtIndex:0] objectForKey:RedesignConstants.urlKey];
        }
        if (![[not objectForKey:RedesignConstants.appKey] isEqualToString:@""]) {
            notification.url = [not objectForKey:RedesignConstants.appKey];
        }
        [[ZMAssetsLibrary defaultLibrary] saveNotification:notification];
    }
}

/* Method to dismiss all notifications */
- (void)dismissAllNotifications:(NSArray *)notifications {
    
    [self changeBadgeNumber:0 header:HomeMainHeaderView.sharedInstance];
    
    for (ZMNotification *notification in notifications) {
        
        dispatch_async(dispatch_queue_create("Mark the notification as read", NULL), ^{
            
            [SettingsServiceClass.sharedAPIManager markNotificationsAsRead:(int)notification.notificationId complete:^(NSDictionary *notifications) {
                
                [AbbvieLogging logInfo:ProntoRedesignAPIConstants.notificationRemovedText];
                [[ZMAssetsLibrary defaultLibrary] deleteNotification:notification.notificationId];
                
                
            } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.notificationRemovingError, [error localizedDescription]]];
            }];
        });
    }
}
@end
