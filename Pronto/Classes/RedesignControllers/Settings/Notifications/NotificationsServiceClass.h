//
//  NotificationsServiceClass.h
//  Pronto
//
//  Created by Saranya on 30/03/2021.
//  Copyright © 2021 Abbvie. All rights reserved.
//

#import "ProntoRedesignAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface NotificationsServiceClass : ProntoRedesignAPIManager

+ (NotificationsServiceClass *)sharedAPIManager;
- (void)unRegisterTokenNoSession:(NSString *)deviceToken complete:(void (^)(void))complete error:(void (^)(NSError *))onError;
- (void)registerToken:(NSString *)deviceToken complete:(void(^)(void))complete error:(void(^)(NSError *error))onError;

@end

NS_ASSUME_NONNULL_END
