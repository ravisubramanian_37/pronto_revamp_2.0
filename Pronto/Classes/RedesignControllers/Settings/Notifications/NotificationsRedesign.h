//
//  NotificationsRedesign.h
//  Pronto
//
//  Created by Saranya on 28/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMNotifications.h"
NS_ASSUME_NONNULL_BEGIN

@interface NotificationsRedesign : NSObject{
 ZMNotifications *notificationsPannel;
    UIView *parentView;
    
}
@property (nonatomic) BOOL notificationsLoaded;
-(void)didTapNotificationButton:(UIView *)parent;
-(void)notification:(ZMNotifications*)notificationView actionForChangeBadgeNumber:(int)badgeNumber;
- (void)getNotifications;
- (void)geTotalOfNotifications;
+ (NotificationsRedesign *)sharedInstance;
- (void)dismissAllNotifications:(NSArray *)notifications;
@end

NS_ASSUME_NONNULL_END
