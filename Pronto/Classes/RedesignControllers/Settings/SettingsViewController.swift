//
//  SettingsViewController.swift
//  Pronto
//
//  Created by Saranya on 07/04/2020.
//  Copyright © 2020 Saranya. All rights reserved.
//

import Foundation
import UIKit
class SettingsViewController: UIViewController, SettingsFeedbackDelegate {
    var helpOption: Bool = false
    var flowType = ""
   override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillLayoutSubviews() {
          super.viewWillLayoutSubviews()
    }

    /* Method for doing additional initialisation */
    func commonInit() {
        let settingsSection: SettingsView = SettingsView()
        settingsSection.helpOptions = helpOption
        settingsSection.flowType = flowType
        settingsSection.backgroundColor = .white
        self.view.addSubview(settingsSection)
        
        settingsSection.delegate = self
        // Register for Notification
        NotificationCenter.default.addObserver(self, selector:#selector(handleOrientationChange) , name: NSNotification.Name(rawValue: RedesignConstants.notificationforOrientationChange), object: nil)
    }
     /* Method called to handle Notification */
     @objc func handleOrientationChange() {
        self.viewWillLayoutSubviews()
        
    }
     /* Feedback Delegate Method */
    func settings(_ settingsView: SettingsView?, displayFeedbackFor selectionOption: String?, openedFrom openOption: String?) {
       
    }
  /* Method to close settings popover - */
    func closeSettingsPopOver() {
         dismiss(animated: true, completion: nil)
    }
}
