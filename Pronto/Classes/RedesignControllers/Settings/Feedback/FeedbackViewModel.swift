//
//  FeedbackViewModel.swift
//  Pronto
//
//  Created by Saranya on 08/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class FeedbackViewModel: NSObject{
    @objc static let sharedInstance = FeedbackViewModel()
    @objc func loadFeedbackForAsset(assetId: String){
        loadFeedback(withOption: ProntoRedesignAPIConstants.FeedbackOption.asset, assetId: assetId)
    }
    /* Method to load Feedback View - Integrated from old design */
    func loadFeedback(withOption: ProntoRedesignAPIConstants.FeedbackOption, assetId: String?){
        let feedbackView = Bundle.main.loadNibNamed(RedesignConstants.feedBackControllerNibName,
                                                    owner: self, options: nil)?[0] as? FeedbackView
        feedbackView?.assetId = assetId ?? RedesignConstants.defaultAssetId
        // display feedback view on top of the view
        let backgroundView = UIView(frame: UIScreen.main.bounds)
        backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        feedbackView?.initWithParent( UIApplication.shared.keyWindow, background: backgroundView,
                                      openedFrom: withOption.rawValue)
        
    }
}
