//
//  SettingsView.swift
//  Pronto
//
//  Created by Saranya on 06/04/2020.
//  Copyright © 2020 Saranya. All rights reserved.
//

import Foundation
import UIKit
protocol SettingsFeedbackDelegate {
    func closeSettingsPopOver()
    func settings(_ settingsView: SettingsView?, displayFeedbackFor selectionOption: String?, openedFrom openOption: String?)
}

@objc class SettingsView: UIView,
                          UITableViewDelegate,
                          UITableViewDataSource {

    @objc static let sharedInstance = SettingsView()
    var helpOptions:Bool = false
    let tableView = UITableView()
    var delegate:SettingsFeedbackDelegate!
    var flowType = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame) // calls designated initializer
        prepareView()
    }
    
    required init?(coder: NSCoder) {
        fatalError(RedesignConstants.commonInitError)
    }
    
    //Method to prepare view to load
    func prepareView() {
        self.tableView.register(UINib(nibName: RedesignConstants.settingsTableViewCellNibName, bundle: nil), forCellReuseIdentifier: RedesignConstants.settingsCellIdentifier)
        self.tableView.register(UINib(nibName: RedesignConstants.settingsTitleCellNibName, bundle: nil), forCellReuseIdentifier: RedesignConstants.settingsTitleCellIdentifier)
        self.addSubview(self.tableView)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.isScrollEnabled = false
        self.tableView.heightAnchor .constraint(equalToConstant: 120).isActive = true
        self.pinEdges(to: self.tableView)
        self.frame = CGRect(x: 0, y: 5, width: RedesignConstants.settingsWidth, height: RedesignConstants.settingsHeight)
    }
   
    //Table View Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(ZMUserActions.sharedInstance()!.isFieldUser){
            return RedesignConstants.numberOfRowsInSettingsForFieldUser
        }
        else{
            return RedesignConstants.numberOfRowsInSettingsForHomeUser
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: RedesignConstants.settingsTitleCellIdentifier,
                                                     for: indexPath) as! SettingsTitleTableViewCell
            cell.userNameLabel.text = AOFLoginManager.sharedInstance.displayName
            if AOFLoginManager.sharedInstance.salesFranchiseID.count > 0 {
                cell.userTypeLabel.text = AOFLoginManager.sharedInstance.salesFranchise
            }
            else {
                cell.userTypeLabel.text = RedesignConstants.UserType.inHouse.rawValue
            }

            cell.settingsTitleImageView.image = UIImage(named:RedesignConstants.SettingsOptionsImage.userIcon.rawValue)
            cell.isUserInteractionEnabled = false
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: RedesignConstants.settingsCellIdentifier, for: indexPath) as! SettingsTableViewCell
            cell.settingsTitleLabel.textColor = RedesignThemesClass.sharedInstance.hexStringToUIColor(hex: RedesignThemesClass.sharedInstance.royalBlueCode)

            var titleString:String = RedesignConstants.SettingsTitle.franchise.rawValue
            var imageTitle:String = RedesignConstants.SettingsOptionsImage.chnageFranchise.rawValue
            
            switch (indexPath.row, ZMUserActions.sharedInstance()!.isFieldUser) {
                case (1, false):
                    titleString = RedesignConstants.SettingsTitle.franchise.rawValue
                    imageTitle = RedesignConstants.SettingsOptionsImage.chnageFranchise.rawValue
                    if (helpOptions) {
                        let overlayView:UIView = UIView()
                        overlayView.backgroundColor = UIColor.lightText
                        cell.contentView.addSubview(overlayView)
                        overlayView.frame = cell.contentView.frame
                        cell.isUserInteractionEnabled = false
                    }
            case (1, true), (2, false):
                titleString = RedesignConstants.SettingsTitle.logout.rawValue
                imageTitle = RedesignConstants.SettingsOptionsImage.logOut.rawValue
            case (2, true), (3, false) :
                titleString = RedesignConstants.SettingsTitle.help.rawValue
                imageTitle = RedesignConstants.SettingsOptionsImage.help.rawValue
                if (helpOptions) {
                    let overlayView:UIView = UIView()
                    overlayView.backgroundColor = UIColor.lightText
                    cell.contentView.addSubview(overlayView)
                    overlayView.frame = cell.contentView.frame
                    cell.isUserInteractionEnabled = false
                }
                default:
                    break;
            }
            cell.settingsTitleLabel.text = titleString
            cell.settingsImageView.image = UIImage(named: imageTitle)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate.closeSettingsPopOver()
        switch (indexPath.row, ZMUserActions.sharedInstance()!.isFieldUser) {
        case (1, false):
                if(helpOptions){
                    break;
                }else{
                    UserDefaults.standard.set(true, forKey: "changepreference")
                    loadPreferences()
                }

        case (1,true), (2,false):
            LogOutViewModel.sharedInstance.cleanUpForLogOut()
            postLogOutNotification()

        case (2,true), (3,false):
            if(helpOptions){
                break;
            }else{
                 HelpViewModel.sharedInstance.loadHelp()
            }
            default:
                break;
        }
    }

    /* Method to post notification for logout */
    @objc func postLogOutNotification(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RedesignConstants.logOutNotification), object: self, userInfo: nil)
    }
    /* Method to load preferences */
    @objc func loadPreferences(){
      let preferencesViewController = PreferencesListViewController.presentPreferencesViewController(from: HomeViewController.sharedInstance.navigationController, flowType: self.flowType)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.6 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            preferencesViewController!.markSelectedToHomeOfficeFranchiese()
        })
    }
}


