//
//  HelpViewModel.swift
//  Pronto
//
//  Created by Saranya on 06/05/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class HelpViewModel: NSObject {
  @objc static let sharedInstance = HelpViewModel()
  var navcontroller:UINavigationController?
  
  var kVidoeID = "5803237506001"
  /* Method to load Help View - Integrated from old design */
  func loadHelp() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let controller:HelpViewController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
    controller.videoId = kVidoeID
    controller.userFranchise = HomeMainHeaderView.sharedInstance.userNameLabel.text
    controller.ispresented = true
    
    navcontroller = UINavigationController.init(rootViewController: controller)
    navcontroller?.modalPresentationStyle = .custom
    RedesignUtilityClass.sharedInstance.presentViewControllerFromWindow(viewController: navcontroller, animated: true)
  }
  /* Help Delegate to close the help */
  func help(_ helpView: HelpViewController!, didTapClose sender: UIButton!) {
    navcontroller?.dismiss(animated: true, completion: nil)
  }
}
