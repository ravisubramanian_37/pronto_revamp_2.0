//
//  SettingsTitleTableViewCell.swift
//  Pronto
//
//  Created by Saranya on 07/04/2020.
//  Copyright © 2020 Saranya. All rights reserved.
//

import Foundation
import UIKit
class SettingsTitleTableViewCell: UITableViewCell {
    @IBOutlet weak var settingsTitleImageView: UIImageView!
    @IBOutlet weak var userTypeLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
}
