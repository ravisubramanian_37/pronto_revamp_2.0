//
//  ProntoRedesignAPIConstants.swift
//  Pronto
//
//  Created by Saranya on 02/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

import Foundation
import UIKit
@objc class ProntoRedesignAPIConstants: NSObject {
    
    @objc static let sharedInstance = ProntoRedesignAPIConstants()

    // Content type for service
    @objc static let contentType1 = "application/json"
    @objc static let contentType2 = "text/json"
    @objc static let contentType3 = "text/javascript"
    @objc static let contentType4 = "text/html"
    @objc static let contentType5 = "text/plain"
    
    // Session params
    @objc static let sessionNameKey = "sessionName"
    @objc static let simpleSAMLSessionIDKey = "SimpleSAMLSessionID"
    @objc static let sessionIdKey = "sessionId"
    @objc static let cookieKey = "Cookie"
    @objc static let contentTypeKey = "Content-Type"
    @objc static let serverTxt = "server"
    @objc static let isFirstTimeTxt = "isFirstTime"
    @objc static let yesTxt = "YES"
    @objc static let accessTokenTxt = "accessToken"
    @objc static let tokenTxt = "token"
    @objc static let typeValueTxt = "ios"

    // AOF
    static let profileServiceURL = "https://federation.abbvie.com/idp/userinfo.openid"
    static let errorAlertButtonTitle = "Retry with Login"
    static let logInURL = "https://idp.abbvie.com/Login.aspx?resumePath=%2Fidp%2Fzr1ce%2FresumeSAML20%2Fidp%2FSSO.ping&allowInteraction=true&reauth=false"
    // Error Msg
    @objc static let abbvieLoggingErrorMsg = "** trying renew sesion"
    @objc static let actualErrorMsg = "actual error code:"
    @objc static let errorMsgFormat = "ERROR_CODE_"
    @objc static let applicationUnavailableErrorMsg = "The aplication is unavailable at this moment."
    @objc static let serverConnectionMsg = "Server connection is not available at the moment, please try again later."
    @objc static  let internetConnectionErrorMsg = "The Internet connection is not available, please check your network connectivity."
    @objc static let underMaintenance = "The server is currently under maintenance mode."
    @objc static let userInformationErrorMsg = "The application cannot send user information."
    @objc static let sendFeedbackError = "Error in submitting the feedback:"
    @objc static let emailToSelfSuccessResponseTxt = "Email to self Success Response:"
    @objc static let emailToSelfErrorResponseTxt = "Email to self Error:"
    @objc static let mailSentSuccessTxt = "Mail sent successfully to your mail id:"
    static let errorTxt = "Error"
    
    @objc static let googleAddress = "google.com"
    @objc static let prontoPlistPath = "Pronto-Config.plist"
    @objc static let prontoPlistName = "Pronto-Config"
    @objc static let serverConfigName = "ServerConfig"
    @objc static let prontoConfigPath = Bundle.main.path(forResource: ProntoRedesignAPIConstants.prontoPlistName, ofType: "plist")
    @objc static let domain = "com.pronto.security.library"
    @objc static let plistType = "plist"
    
    @objc static let loginResponseMsg = "logiinnn WS Response:"
    @objc func returnLoginString(code: Double, description: NSString) -> NSString {
        return NSString.localizedStringWithFormat("Error code %ld , Error Description %@ on WS login uri", code, description)
    }
    @objc static let categoryAssetCountResponseMsg = "Category count Response:"
    @objc static let categoryAssetCountErrorMsg = "Category Count Error Response:"
    @objc static let categorySubfolderResponseMsg = "Category Subfolder Response:"
    @objc static let categorySubfolderErrorMsg = "Category Subfolder Error Response:"
    @objc static let searchSuccessResponseMsg = "Search Success Response:"
    @objc static let searchErrorResponseMsg = "Search Error Response:"
    @objc static let fetchFavoritesResponseMsg = "Fetch Favorties Response:"
    @objc static let fetchFavoritesErrorResponseMsg = "Fetch Favorties Error Response:"
    @objc static let editOrDeleteFavoritesResponseMsg = "EditOrDelete Favorties Response:"
    @objc static let editOrDeleteFavoritesErrorResponseMsg = "EditOrDelete Favorties Error Response:"
    @objc static let fetchPromotedContentSuccessResponseMsg = "Fetch Promoted Content Success Response:"
    @objc static let fetchPromotedContentErrorResponseMsg = "Fetch Promoted Content Error Response:"
    @objc static let fetchEventsMetaDataErrorMsg = "Fetch Events Meta data Error:"
    @objc static let fetchEventsMetaDataSuccessMsg = "Fetch Events Meta data Success:"
    @objc static let fetchEventDetailsErrorMsg = "Fetch Event details Error:"
    @objc static let fetchEventDetailsSuccessMsg = "Fetch Event details Success response:"
    @objc static let fetchViewedAssetErrorMsg = "Fetch viewed asset service Error:"
    @objc static let fetchViewedAssetSuccessMsg = "Fetch viewed asset service Success response:"
    @objc static let saveViewedAssetErrorMsg = "Save viewed asset service Error:"
    @objc static let saveViewedAssetSuccessMsg = "Save viewed asset service Success response:"
    @objc static let logOutSuccessMsg = "Logout Success"
    @objc static let logOutErrorMsg = "Logout Error"
    @objc static let apnsRegistrationError = "APNS Token couldn't be registered"
    @objc static let apnsUnRegisteredError = "APNS Token couldn't be UNRegistered"
    @objc static let apnsRegistrationSuccessMsg = "APNS Token registered with the CMS"
    // Service Calls
    @objc static let eventsAllUrl = "eventsAll.uri" // old call
    
    @objc static let getEventsMetaDataUrl = "getEventsMetaData.uri"// new call No:1
    @objc static let getEventDetailsUrl = "getEventDetails.uri"// new call No:2
    @objc static let fetchViewedEventsAssetsUrl = "fetchViewedEventsAsset.uri"// new call no:3
    @objc static let saveViewedEventsAssetsUrl = "saveViewedEventsAsset.uri"// new call no:4
    @objc static let userLoginUrl = "ws.login.url"
    @objc static let feedbackURl = "feedback.uri"
    @objc static let brandsURL = "brands.uri"
    @objc static let statisticsURL = "statistics.uri"
    @objc static let categoriesURL = "category.uri"
    @objc static let fetchPreferencesURL = "fetchPreferences.uri"
    @objc static let savePreferencesURL = "savePreferences.uri"
    @objc static let fetchNotificationsURL = "notification.uri"
    @objc static let markNotificationsAsUnreadURL = "services/privatemsg_unread/"
    @objc static let unRegisterTokenURL = "services/push_notification_token/"
    @objc static let registerTokenURL = "apssave.uri"
    @objc static let emailToSelfURL = "sendmail.uri" // old
    @objc static let uploadAssetViewURL = "events.uri"
    @objc static let searchAssetsURL = "search.uri"
    @objc static let sendEmailToSelfURI = "sendEmailToSelf.uri" // new
    @objc static let logOutServiceURI = "logOutService.uri"
    @objc static let categoryAssetCountURL = "categoryAssetCount.uri"
    @objc static let getAssetsURL = "getAssets.uri"
  
    
    // new services
    @objc static let getPreferencesURL = "getPreferences.uri"
    @objc static let getFavoritesURL = "getFavorites.uri"
    @objc static let editFavouritesURL = "editFavorites.uri"
    @objc static let getPromotedContentURL = "getPromotedContent.uri"
    // User Login Call
    @objc static let userNameText = "username"
    @objc static let passwordText = "password"
    @objc static let responseSessId = "sessid"
    @objc static let responseSess_Name = "session_name"
    @objc static let infoKey = "info"
    @objc static let kNewUsername = "abbvie_services"
    @objc static let kRNNewEncryptionKey = "fJLWwDeAYJtruHCyOuLICWkzQmqtdf2E"
    
    // Feedback types
    enum FeedbackOption: String {
        case settings = "settings"
        case asset = "asset"
    }
    @objc static let feedbackSuccessResponeseText = "Feedback Success Response:"
    @objc static let feedbackErrorResponse = "Feedback Error Response:"
    @objc static let nIdParamKey = "assetID"  // "nid"
    @objc static let typeKey = "type"
    @objc static let feedbackKey = "feedback"
    // Notifications
    @objc static let fetchNotificationSuccessResponeseText = "Fetch Notifications Success Response:"
    @objc static let fetchNotificationErrorResponse = "Fetch Notification Error Response:"
    @objc static let markNotificationsAsUnreadErrorResponse = "Mark Notifications As UnRead Error Response"
    @objc static let markNotificationsAsUnreadSuccessResponse = "Mark Notifications As UnRead Success Response:"
    @objc static let markNotificationsAsReadErrorResponse = "Mark Notifications As Read Error Response"
    @objc static let markNotificationsAsReadSuccessResponse = "Mark Notifications As Read Success Response:"
    @objc static let markNotificationAsReadText = "Mark the notification as read"
    @objc static let notificationRemovedText = "Notification removed"
    @objc static let notificationRemovingError = "Error removing Notification:"
    @objc static let notificationExpired = "One or more assets related to the listed messages are not available."
    + "The assets might be expired or deleted by an Administrator. Messages related will be removed from the Messages list."
    @objc static let unRegisterPushTokenErrorMsg = "APNS Token couldn't be UNRegistered"
    // Brands
    @objc static let brandsErrorResponseTxt = "Error in fetching the Brands Response:"
    
    // Favorites
    static let updatingAssetSuccessMsg = "Assets Synced"
    static let updatingAssetErrorMsg = "Error in syncing asset Details:"
    static let updatingAssetErrorMsgInLocalDB = "Error updating asset in Local DB"
    static let assetIDKeyForFav = "id"
    static let priorityKeyForFav = "position"
    static let favInsertionInCoreDataErrorMsg = "Not successful fav Insertion"
    static let favInsertionInCoreDataSuccessMsg = "Successful fav Insertion"
    static let favDeletionInCoreDataSuccessMsg = "Successful fav Deletion"
    static let favDeletionInCoreDataErrorMsg = "No Successful fav Deletion"
    static let favUpdationInCoreDataErrorMsg = "Not successful fav Updation"
    static let favUpdationInCoreDataSuccessMsg = "Successful fav Updation"

    // Search
    @objc static let searchTextKey = "key"
    @objc static let searchPerPageKey = "per_page"
    @objc static let searchCurrentPageKey = "page"
}
