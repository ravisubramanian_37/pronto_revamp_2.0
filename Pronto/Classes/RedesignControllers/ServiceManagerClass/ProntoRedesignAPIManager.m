//
//  ProntoRedesignAPIManager.m
//  Pronto
//
//  Created by Saranya on 02/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoRedesignAPIManager.h"
#import "ProntoRedesignServerUtilityClass.h"
#import "Pronto-Swift.h"

@implementation ProntoRedesignAPIManager


/* Method to create Singleton Object for this class */
+ (id)sharedInstance {
    
    @synchronized(self)
    {
        static ProntoRedesignAPIManager *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}

/**
 *  Init RequestOperation Manager to get data from WS
 *
 *  @param hasToken Bool to indicate if a user has a valid token
 *  @param headers  Headers of request
 *
 *  @return Instance of AFHTTPRequestOperationManager
 */

- (AFHTTPRequestOperationManager *)requestManagerWithToken:(BOOL)hasToken headers:(NSDictionary *)headers {
    
    NSDictionary* serverCOnfig = [[ProntoRedesignServerUtilityClass sharedInstance] getServerConfigFile:ProntoRedesignAPIConstants.serverConfigName];
    NSURL *baseURL = [NSURL URLWithString:[serverCOnfig objectForKey:CONFIG]];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setStringEncoding:NSUTF8StringEncoding];
    [manager.requestSerializer setValue:ProntoRedesignAPIConstants.contentType1 forHTTPHeaderField:ProntoRedesignAPIConstants.contentTypeKey];

    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer = /*[AFJSONResponseSerializer serializer]*/responseSerializer;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:ProntoRedesignAPIConstants.contentType1, ProntoRedesignAPIConstants.contentType2, ProntoRedesignAPIConstants.contentType3,ProntoRedesignAPIConstants.contentType4,ProntoRedesignAPIConstants.contentType5, nil];
   
    if([[NSUserDefaults standardUserDefaults] valueForKey:ProntoRedesignAPIConstants.sessionNameKey] != nil){
        if([[NSUserDefaults standardUserDefaults]valueForKey:ProntoRedesignAPIConstants.simpleSAMLSessionIDKey] != nil)
        {
            [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@=%@; %@=%@",ProntoRedesignAPIConstants.simpleSAMLSessionIDKey,[[NSUserDefaults standardUserDefaults] valueForKey:ProntoRedesignAPIConstants.simpleSAMLSessionIDKey],[[NSUserDefaults standardUserDefaults] valueForKey:ProntoRedesignAPIConstants.sessionNameKey],[[NSUserDefaults standardUserDefaults] valueForKey:ProntoRedesignAPIConstants.sessionIdKey]] forHTTPHeaderField:ProntoRedesignAPIConstants.cookieKey];
        }
        else{
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@=%@",[[NSUserDefaults standardUserDefaults] valueForKey:ProntoRedesignAPIConstants.sessionNameKey],[[NSUserDefaults standardUserDefaults] valueForKey:ProntoRedesignAPIConstants.sessionIdKey]] forHTTPHeaderField:ProntoRedesignAPIConstants.cookieKey];
        }
    }
    
    for (NSString *key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    }
    
    return manager;
}

/**
 *  Handle WS response, in case a WS give a error, the function will try sending the request three times, if the error is still present after 3 times, the function will return the error, in ohter case, will return the resonse object.
 *
 *  @param blockToExecute Block with request to perform
 *  @param ntimes         number of times to try request in case on error comes
 *  @param onCompletion   Block completion.
 */
- (void) performBlock:(void (^)(CallbackBlock callback)) blockToExecute retryingNumberOfTimes:(NSUInteger)ntimes onCompletion:(void (^)(NSError* error, NSObject* response)) onCompletion {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
    dispatch_async(queue, ^{
        blockToExecute(^(NSError* error, NSObject* response){
            if (error == nil) {
                onCompletion(nil, response);
            }else{
                if (ntimes <= 0) {
                    if (onCompletion) {
                       onCompletion(error, nil);
                    }
                } else {
                    if([ProntoRedesignServerUtilityClass.sharedInstance checkErrorCode:error.code]) {
                        [ProntoRedesignServerUtilityClass.sharedInstance handleAbbvieLoggingForErrors:error];
                            [self performBlock:blockToExecute retryingNumberOfTimes:(ntimes - 1) onCompletion:onCompletion];

                    }else{
                        [self performBlock:blockToExecute retryingNumberOfTimes:(ntimes - 1) onCompletion:onCompletion];
                    }
                }
            }
        });
    });
}
/* Method to retrieve response from JSON - */
- (NSDictionary *)retrieveResponseFromJSON:(NSString *)fileName
{
    NSDictionary *dict = [self JSONFromFile:fileName];

    return dict;
}

/* Method to get JSON from file - */
- (NSDictionary *)JSONFromFile:(NSString *)filename
{
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}
@end
