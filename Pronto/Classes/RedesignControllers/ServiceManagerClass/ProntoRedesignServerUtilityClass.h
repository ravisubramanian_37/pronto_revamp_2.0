//
//  ProntoRedesignServerUtilityClass.h
//  Pronto
//
//  Created by Saranya on 02/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMMessage.h"
#import "Asset.h"
@protocol AlertErrorDelegate <NSObject>
-(void)OkAlertDelegateMethod;

@end
@interface ProntoRedesignServerUtilityClass : NSObject{
    BOOL thumbLoading;
}
//Error codes
typedef enum
{
    serverAlertPlus1011 = 1011,
    serverAlert1011 = -1011,
    serverAlert1001 = -1001,
    serverAlert1005 = -1005,
    serverAlert1004 = -1004,
    serverAlert1003 = -1003,
    serverAlert3840 = 3840,
    serverAlert503 = 503,
    serverAlert107 = 107,
    serverAlert401 = 401,
    serverAlert108 = 108,
    serverAlert000 = 000
    
}errorCodes;
@property (strong,nonatomic) ZMMessage *currentMessage;
@property (nonatomic, weak) id <AlertErrorDelegate> alertDelegate;
+ (id)sharedInstance;
- (NSDictionary *)getServerConfigFile:(NSString*)key;
- (NSString *)getServiceResourceUrl:(NSString*)root;
- (NSString *)getBaseUrl;
- (BOOL)checkErrorCode:(NSInteger)errorCode;
- (void)handleAbbvieLoggingForErrors:(NSError *)error;
- (NSError *)getErrorWithCode:(NSInteger)code alternateMessage:(NSString *)aMessage;
- (BOOL)isConnectionAvailableForYou;
- (void)showError:(NSError *)error;
- (void)showErrorAlert:(NSString *)msg title:(NSString *)titleAlt;
- (void)printResponse:(NSString *)responseMsg responseObj:(id)responseObj;
- (Asset *)getAllAsset:(NSNumber *)assetID;
- (UIImage *)setThumbnailPath:(NSString *)thumbnailPath;
- (Asset *)getPromoAsset:(NSNumber *)assetID;
@end

