//
//  ProntoRedesignServerUtilityClass.m
//  Pronto
//
//  Created by Saranya on 02/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import "ProntoRedesignServerUtilityClass.h"
#import "Pronto-Swift.h"

@implementation ProntoRedesignServerUtilityClass
@synthesize currentMessage,alertDelegate;
/* Method to create Singleton Object for this class */
+ (id)sharedInstance {
    
    @synchronized(self)
    {
        static ProntoRedesignServerUtilityClass *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}
 /* Method to check error codes */
-(BOOL)checkErrorCode:(NSInteger)errorCode{
    switch (errorCode) {
        case serverAlert3840:
        case serverAlert1011:
        case serverAlert1001:
        case serverAlert1005:
        case serverAlert1004:
        case serverAlert1003:
        case serverAlertPlus1011:
            return YES;
            break;
            
        default:
            return false;
            break;
    }
}
/* Method to handle logging Error messages */
-(void)handleAbbvieLoggingForErrors:(NSError *)error{
    
    if([self checkErrorCode:error.code]){
         [AbbvieLogging logError:ProntoRedesignAPIConstants.abbvieLoggingErrorMsg];
    }
    else{
         [AbbvieLogging logError:error.localizedDescription];
    }
}
/* Method to get Error messages for Error Code */
- (NSError *)getErrorWithCode:(NSInteger)code alternateMessage:(NSString *)aMessage {
    
    NSString *messageString = [NSString stringWithFormat:@"%@%ld",ProntoRedesignAPIConstants.errorMsgFormat,(long)code];
    NSString *desc = NSLocalizedString(messageString, aMessage);
    NSDictionary *info = @{ NSLocalizedDescriptionKey : desc };
    
    [AbbvieLogging logError:[NSString stringWithFormat:@"%@%@",ProntoRedesignAPIConstants.actualErrorMsg,messageString]];
    return [NSError errorWithDomain:ProntoRedesignAPIConstants.domain code:code userInfo:info];
}
/* Print Suucess and Error */
-(void)printResponse:(NSString *)responseMsg responseObj:(id)responseObj{
    NSLog(@"%@,%@",responseMsg,responseObj);
}
/* This method reads the data from Config file automatically based on the scheme it is being build.*/
- (NSDictionary *)getServerConfigFile:(NSString*)key {
    NSString *file = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:ProntoRedesignAPIConstants.prontoPlistPath];
    return [[[NSMutableDictionary alloc] initWithContentsOfFile:file] objectForKey:key];
}
/* This method reads the service plist file.*/
- (NSString *)getServiceResourceUrl:(NSString*)root {
    
    NSString *file = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:ProntoRedesignAPIConstants.prontoPlistPath];
    NSMutableDictionary *config = [NSMutableDictionary dictionaryWithContentsOfFile:file];
    NSString *valueRoot = [config objectForKey:root];
    
    return valueRoot;
}
- (NSString *)getBaseUrl {
  NSDictionary* serverCOnfig = [self getServerConfigFile:@"ServerConfig"];
  NSURL *baseURL = [NSURL URLWithString:[serverCOnfig objectForKey:CONFIG]];
  return baseURL.absoluteString;
}


/**
 * check if theres a internet connection available
 */
- (BOOL)isConnectionAvailableForYou {
    
    SCNetworkReachabilityFlags flags;
    BOOL receivedFlags;
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [ProntoRedesignAPIConstants.googleAddress UTF8String]);
    receivedFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    
    if (!receivedFlags || (flags == 0)) {
        return FALSE;
    }
    
    return TRUE;
}
/**
 *  Shows the error
 *  @param error
 */
- (void)showError:(NSError *)error {
    
    if (error.code) {
        
        //Validate if there is internet connection
        NSString *message = ProntoRedesignAPIConstants.internetConnectionErrorMsg;
        if ([self isConnectionAvailableForYou]) {
            message = [NSString stringWithFormat:@"%@", error.localizedDescription];
        }
        //Checking if the server is not under maintenance
        if (error.code == serverAlert503) {
            message = ProntoRedesignAPIConstants.underMaintenance;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self showMessage:message whithType:ZMProntoMessagesTypeWarning withAction:^{
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"session %@", [AOFLoginManager sharedInstance].upi]];
                [self hideMessage];
                [self performSelector:@selector(initPronto) withObject:nil afterDelay:.5];
            }];
        });
        
    }
}
/**
 *  Shows a messages with a given type
 *  @param message The message to display
 *  @param type the type of the message
 *  @param action the action for the retry button
 */
- (void)showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withAction:(void(^)(void))mAction {
    
//    if (!_currentMessage) {
//        UINavigationController *topController = (UINavigationController *)self.window.rootViewController;
//
//        _currentMessage = [[ZMMessage alloc] initMessage:topController.view withMessage:message andType:type withAction:mAction];
//        _currentMessage.delegate = self;
//    }
}
/**
 *  Hides the message
 */
- (void)hideMessage {
    if(currentMessage) {
        
        [currentMessage hideMessage:^(BOOL finished) {
            [currentMessage removeFromSuperview];
            currentMessage = nil;
        }];
    }
}
/* Method to show error alert - */
-(void)showErrorAlert:(NSString *)msg title:(NSString *)titleAlt{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titleAlt
                                                                                message:msg
                                                                         preferredStyle:UIAlertControllerStyleAlert];
    //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
        [self okayAlertDelegateMethod];
    }]; 

    [alertController addAction:actionOk];
    
    [[RedesignUtilityClass sharedInstance] presentViewControllerFromWindowWithViewController:alertController animated:YES];
}
/* Delegate method for alert */
-(void)okayAlertDelegateMethod
{
    [self.alertDelegate OkAlertDelegateMethod];
}
/* Method to get all assets */
-(Asset *)getAllAsset:(NSNumber *)assetID{
        
    NSFetchRequest * fetchrequest = [Asset MR_requestAll];
    NSArray *importedAssets = [Asset MR_executeFetchRequest:fetchrequest];
    
    for (Asset *asset in importedAssets){
        if(asset.assetID == assetID){
            return asset;
        }
    }
    return nil;
}
/* Method to get Promo asset */
-(Asset *)getPromoAsset:(NSNumber *)assetID{
        
    NSFetchRequest * fetchrequest = [Asset MR_requestAll];
    NSArray *importedAssets = [Asset MR_executeFetchRequest:fetchrequest];
    
    for (Asset *asset in importedAssets){
        if(asset.assetID == assetID){  
            return asset;
        }
    }
    return nil;
}
/* set thumbnail path */
- (UIImage *)setThumbnailPath:(NSString *)thumbnailPath {
    UIImageView *thumbnail = [UIImageView new];
    thumbnail.image = nil;
    thumbnail.alpha = 1.0;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[[EventsServiceClass sharedAPIManager] getThumbnailPath:thumbnailPath]]) {
        thumbnail.image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:[[EventsServiceClass sharedAPIManager] getThumbnailPath:thumbnailPath]]];
    }
    else {
        if (!thumbLoading) {
            thumbLoading = YES;
            //Downloads the thumbnail and caches it
            [[EventsServiceClass sharedAPIManager] downloadThumbnail:thumbnailPath complete:^(UIImage *thumbnailImage, NSString *p) {
                thumbLoading = NO;
            
                if (![p isEqualToString:thumbnailPath]) {
                    thumbnailImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:[[EventsServiceClass sharedAPIManager]getThumbnailPath:thumbnailPath]]];
                }

                thumbnail.image = thumbnailImage;
              [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.notificationforOrientationChange object:self userInfo:nil];
            } fromCache:YES includeWaterMark:@""];
        }
    }
    if(thumbnail.image == nil){
        thumbnail.image = [UIImage imageNamed:@""];
    }
    return thumbnail.image;
}
@end
