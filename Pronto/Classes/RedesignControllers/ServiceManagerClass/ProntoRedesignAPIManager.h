//
//  ProntoRedesignAPIManager.h
//  Pronto
//
//  Created by Saranya on 02/04/2020.
//  Copyright © 2020 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
NS_ASSUME_NONNULL_BEGIN

typedef void (^AssetCompletionBlock) (void);
typedef void (^CallbackBlock)(NSError* error, NSObject* response);

@interface ProntoRedesignAPIManager : NSObject
+ (id)sharedInstance;
- (AFHTTPRequestOperationManager *)requestManagerWithToken:(BOOL)hasToken headers:(NSDictionary *)headers;
- (void) performBlock:(void (^)(CallbackBlock callback)) blockToExecute retryingNumberOfTimes:(NSUInteger)ntimes onCompletion:(void (^)(NSError* error, NSObject* response)) onCompletion;

- (NSDictionary *)retrieveResponseFromJSON:(NSString *)fileName;
@end

NS_ASSUME_NONNULL_END
