//
//  AudioPlayer.swift
//  Pronto
//
//  Created by Praveen Kumar on 09/08/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

public class AudioPlayer: UIView {


    var myAudioPlayer: AVAudioPlayer!
    @objc static let sharedInstance = AudioPlayer()

  // swiftlint:disable:next identifier_name
    @IBOutlet weak var play_PauseButton: UIButton!
    @IBOutlet weak var myAudioController: UISlider!
    @IBOutlet weak var maxTimer: UILabel!
    @IBOutlet weak var currentTimer: UILabel!
    
    @objc var timer: Timer? = nil
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func getAudioPlayer(withFrame frame: CGRect) -> AudioPlayer
    {
      // swiftlint:disable:next force_cast
        let audioPlayer : AudioPlayer = Bundle.main.loadNibNamed("AudioPlayer", owner: self, options: nil)?.first as! AudioPlayer
        audioPlayer.frame = frame
        return audioPlayer
    }
    
    @objc func setAsset(assetVal:Asset)
    {
//        asset = assetVal
    }
    
    @objc func initialSetup(assetVal:Asset)
    {
        if let myFilePathString = assetVal.path
        {
            let myFilePathURL = NSURL(fileURLWithPath: myFilePathString)
            do{
                try myAudioPlayer = AVAudioPlayer(contentsOf: myFilePathURL as URL)
                myAudioPlayer.play()
                play_PauseButton.setImage(UIImage(named: "pauseIcon"), for: .normal)
                myAudioController.maximumValue = Float(myAudioPlayer.duration)
                
                let maxTime = self.secondsConverter(seconds: Int(myAudioPlayer.duration))
                maxTimer.text = maxTime
                
                //                myVolumeController.isHidden = true
            }
            catch{
                print("Error")
            }
            
        }
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(AudioPlayer.updateAudioController), userInfo: nil, repeats: true)
    }
    
    @objc func stopAudio() {
        myAudioPlayer.stop()
        myAudioPlayer.currentTime=0
        play_PauseButton.setImage(UIImage(named: "playIcon"), for: .normal)
        timer?.invalidate()
    }
    
    @IBAction func play_pauseAudio(_ sender: Any) {
        let image = play_PauseButton.currentImage
        if(image == UIImage(named: "pauseIcon"))
        {
            myAudioPlayer.pause()
            play_PauseButton.setImage(UIImage(named: "playIcon"), for: .normal)
            timer?.invalidate()
        }
        else
        {
            myAudioPlayer.play()
            play_PauseButton.setImage(UIImage(named: "pauseIcon"), for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(AudioPlayer.updateAudioController), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func changeAudioTime(_ sender: Any) {
        
        myAudioPlayer.stop()
        myAudioPlayer.currentTime = TimeInterval(myAudioController.value)
        let image = play_PauseButton.currentImage
        if(image == UIImage(named: "pauseIcon"))
        {
            myAudioPlayer.prepareToPlay()
            myAudioPlayer.play()
        }
    }
    
    @objc func updateAudioController() {
        myAudioController.value = Float(myAudioPlayer.currentTime)
        let time = self.secondsConverter(seconds: Int(myAudioPlayer.currentTime))
        currentTimer.text = time
        
        let maxTime = self.secondsConverter(seconds: Int(myAudioPlayer.duration))
        
        if(time == maxTime)
        {
            self.stopAudio()
        }
    }
    @IBAction func audioControllerValueChanged(_ sender: UISlider) {
        let time = self.secondsConverter(seconds: Int(sender.value))
        currentTimer.text = time
    }
    
    @objc func secondsConverter(seconds : Int) -> String {
        let mins = (seconds % 3600) / 60
        let seconds = (seconds % 3600) % 60
        
        let minsString = self.handleSingleDigitValues(value: mins)
        let secsString = self.handleSingleDigitValues(value: seconds)
        
        let timeToReturn = minsString + ":" + secsString
        return timeToReturn
    }
    
    @objc func handleSingleDigitValues(value : Int) -> String {
        var valueToReturn = String(value)
        if(value < 10)
        {
            valueToReturn = "0" + String(value)
        }
        return valueToReturn
    }
}
