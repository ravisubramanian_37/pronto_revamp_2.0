//
//  Briefcase+CoreDataProperties.m
//  Pronto
//
//  Created by Oscar Robayo on 22/10/15.
//  Copyright © 2015 Zemoga. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Briefcase+CoreDataProperties.h"

@implementation Briefcase (CoreDataProperties)

@dynamic birefcase_date;
@dynamic assets;
@dynamic folder;

@end
