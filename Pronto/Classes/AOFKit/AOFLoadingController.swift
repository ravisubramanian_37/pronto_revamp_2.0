//
//  AOFLoadingController.swift
//  Pronto
//
//  Created by Praveen Kumar on 27/03/19.
//  Rewritten by Saranya - 06/04/20
//  Copyright © 2019 Abbvie. All rights reserved.
//

import UIKit
import AOFKit
import WebKit

@objc class AOFLoadingController: UIViewController, AOFOAuthDelegate, WKNavigationDelegate, WKUIDelegate {
  @objc static let sharedInstance = AOFLoadingController()
    @IBOutlet weak var redesignLoadingIndicator: UIActivityIndicatorView!
    
    @objc var prontoUserDefaults = ProntoUserDefaults.singleton()
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    var failureCount: Int = 0
    var AOFDictionary: NSDictionary?
    var isLoginInProgress = true
    var hostUrl = ""
    var wkWebView: WKWebView = WKWebView()
    @objc var fromDeeplink = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []
        self.navigationController?.navigationBar.isHidden = true
        if let path = RedesignConstants.prontoAofKitPath {
            AOFDictionary = NSDictionary(contentsOfFile: path)
        }
        AOFManager.register(withClientID: AOFDictionary?.value(forKey: RedesignConstants.clientIdKey) as? String ?? "", withEnvironment: .prod,
                            withDelegate: self,
                            withScope: AOFDictionary?.value(forKey: RedesignConstants.scopeKey) as? String ?? "",
                            withRedirectUri: AOFDictionary?.value(forKey: RedesignConstants.redirectKey) as? String ?? "",
                            withGrantType: AOFDictionary?.value(forKey: RedesignConstants.grantTypeKey) as? String ?? "")
        
        self.checkLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        redesignLoadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        redesignLoadingIndicator.centerXAnchor .constraint(equalTo: self.view.centerXAnchor).isActive = true
        redesignLoadingIndicator.centerYAnchor .constraint(equalTo: self.view.centerYAnchor).isActive = true

      if UserDefaults.standard.bool(forKey: RedesignConstants.isLoadingFirstTime) && !isLoginInProgress {
            UserDefaults.standard.set(false, forKey: RedesignConstants.isLoadingFirstTime)
            (ProntoInitialLoadController.sharedInstance() as AnyObject).postPreferencesFinishNotification()
        } else {
          if (!isLoginInProgress && AOFLoadingController.sharedInstance.fromDeeplink == false) {
            self.loadWebView()
          } else if !isLoginInProgress && AOFLoadingController.sharedInstance.fromDeeplink == true {
            HomeViewModel.sharedInstance.displayHomeView()
            HomeViewController.sharedInstance.openDeeplink()
          }
        }
    }
  
  func loadWebView() {
    let configuration = WKWebViewConfiguration()
    wkWebView = WKWebView(frame: .zero, configuration: configuration)
    wkWebView.frame = UIScreen.main.bounds
    wkWebView.translatesAutoresizingMaskIntoConstraints = false
    wkWebView.navigationDelegate = self
    wkWebView.uiDelegate = self
    wkWebView.contentMode = UIView.ContentMode.center
    self.view.addSubview(wkWebView)
    
    [wkWebView.topAnchor.constraint(equalTo: view.topAnchor),
     wkWebView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
     wkWebView.leftAnchor.constraint(equalTo: view.leftAnchor),
     wkWebView.rightAnchor.constraint(equalTo: view.rightAnchor)].forEach  { anchor in
      anchor.isActive = true
     }
    let urlString = ProntoRedesignAPIConstants.logInURL
    if let url = URL(string: urlString) {
      self.hostUrl = urlString
      wkWebView.load(URLRequest(url: url))
    }
  }
  
  open func webView(_ webView: WKWebView,
                    didFinish navigation: WKNavigation!) {
    }
  
  open func webView(_ webView: WKWebView,
                    decidePolicyFor navigationAction: WKNavigationAction,
                    decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
  {
    if navigationAction.navigationType == .formSubmitted {
      // JS Query : "document.querySelectorAll(\"input[type='text']\")[0].value" for reference
      webView.evaluateJavaScript("document.querySelectorAll(\"input[type='text']\")[0].value") { (result, error) in
        if (result != nil) {
          self.showLogin()
        }
      }
    }
    decisionHandler(WKNavigationActionPolicy.allow)
  }
  
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // Show Login Page
    func showLogin(){
      DispatchQueue.main.async {
        if self.wkWebView != nil {
          self.wkWebView.isHidden = true
          self.redesignLoadingIndicator.isHidden = false
        }
      }
      self.isLoginInProgress = true
      AOFManager.authorize(withPresenter: self) { (presentedViewController: UIViewController) -> Void in
        (presentedViewController as? UINavigationController)?.topViewController?.title = RedesignConstants.loginTitle
      }
    }
    
    // Check if login is required or not
    func checkLogin() {
        
        if AOFManager.hasValidAccessToken() == false {
          showLogin()
            
        } else {
            if ((ProntoRedesignServerUtilityClass.sharedInstance() as AnyObject).isConnectionAvailableForYou())
            {
                RedesignConstants.utilityVar.showBottomIndicatorView(true, withMessage: RedesignConstants.loginMsg)
                // refetch access tokens..
                AbbvieLogging.logInfo(RedesignConstants.refetchTokenMsg)
                self.refetchToken()
            }
            else
            {
                self.prontoUserDefaults?.setAOFAccessToken(AOFManager.accessToken())
                aof__showMessage(title: ProntoRedesignAPIConstants.errorTxt, message: ProntoRedesignAPIConstants.internetConnectionErrorMsg)
            }
        }
    }
    // Function to show/remove Animating
    func activityIndicator(isShow: Bool) {
        DispatchQueue.main.async{
            if (isShow) {
                self.redesignLoadingIndicator.isHidden = false
                self.redesignLoadingIndicator.startAnimating()
            }
            else {
                if self.redesignLoadingIndicator.isAnimating {
                    self.redesignLoadingIndicator.stopAnimating()
                }
            }
        }
    }
    // Function to refetch the access token
    func refetchToken() {
        self.activityIndicator(isShow: true)
//        AOFLoginManager.sharedInstance.AOFKitLogout()
        AOFManager.oauthClient?.refetchAccessToken()
    }
    
    
    // MARK: - AOFOAuthDelegate
    
    func willBegin()
    {
        AOFManager.oauthClient?.showORHideNetworkActivityIndicator(true)
        AOFHUD.dismiss()
    }
    
    func didEnd()
    {
        AOFManager.oauthClient?.showORHideNetworkActivityIndicator(false)
        AOFHUD.dismiss()
    }
    
    // Tap cancel button
    func cancel()
    {
        AOFManager.oauthClient?.showORHideNetworkActivityIndicator(true)
        AOFHUD.dismiss()
        self.checkLogin()
    }
    
    // Error handling for AOFKit Calls
    func failureFromAOFKit(error: Error?, msg: String?) {
      AOFManager.oauthClient?.showORHideNetworkActivityIndicator(false)
      RedesignConstants.utilityVar.showBottomIndicatorView(false, withMessage: RedesignConstants.loginMsg)
      let errorMessage = " Error while trying to get access token."
      if let errorMsg = msg, errorMessage == msg {
        AbbvieLogging.logError(errorMsg)
        showAlert(msg: errorMsg, title: ProntoRedesignAPIConstants.errorTxt)
      } else {
        //            AbbvieLogging.LogError(error?.localizedDescription ?? RedesignConstants.errorInDataFetching)
        //            showAlert(msg: (error?.localizedDescription ?? RedesignConstants.errorInDataFetching), title: ProntoRedesignAPIConstants.errorTxt)
        showLogin()
      }
    }
    // Show alert for Error
    func showAlert(msg: String, title: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ProntoRedesignAPIConstants.errorAlertButtonTitle, style: .default, handler: { action in
            self.isLoginInProgress = false
            self.showLogin()
        }))
        DispatchQueue.main.async{
            self.present(alert, animated: true)
        }
    }
    // Invoke User Login Service
    func invokeUserLogin(navCont: UINavigationController?) {
      LoginViewModel().invokeUserLogin(nav: navCont)
    }
    
    // Success handling for User Details Call
    func successForUserDetails(withAccessToken accessToken: String?, withUserData data: NSDictionary?) {
        AbbvieLogging.logInfo("\(RedesignConstants.userDetails) \(String(describing: data))")
        (ProntoInitialSetUpController.sharedInstance() as AnyObject).checkForHomeUser()
        AOFLoginManager.sharedInstance.setValues(jsonData: ((data)!))
        if (AOFManager.user511Id != nil)
        {
            self.prontoUserDefaults?.setAOF511Id(AOFManager.user511Id)
        }
        AOFLoginManager.sharedInstance.setSettingsValue()
        AOFHUD.dismiss()
      if (self.appDelegate?.featureSwitchkey == true) {
            self.failureCount = 0
            DispatchQueue.main.async {
                self.isLoginInProgress = false
                RedesignConstants.utilityVar.showBottomIndicatorView(true, withMessage: RedesignConstants.loginMsg)
                self.invokeUserLogin(navCont: self.navigationController)
            }
        }
    }
  
  // Run block in main thread
      func executeOnMainThred(block: @escaping () -> Void) {
          if Thread.isMainThread {
              block()
          } else {
              DispatchQueue.main.async {
                  block()
              }
          }
      }
}
