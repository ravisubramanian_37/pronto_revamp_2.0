//
//  AOFLoginManager.swift
//  Pronto
//
//  Created by Praveen Kumar on 26/03/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

import UIKit
import AOFKit
import Foundation

@objc class AOFLoginManager: NSObject, AOFOAuthDelegate {
    
    @objc static let sharedInstance = AOFLoginManager()
    @objc var prontoUserDefaults = ProntoUserDefaults()
    
    @objc var adDomain = String()
    @objc var adLogon = String()
    @objc var country = String()
    @objc var company = String()
    @objc var department = String()
    @objc var departmentcode = String()
    @objc var displayName = String()
    @objc var division = String()
    @objc var divisionCode = String()
    @objc var divisionName = String()
    @objc var employeeType = String()
    @objc var fullName = String()
    @objc var givenName = String()
    @objc var initials = String()
    @objc var location = String()
    @objc var locationCode = String()
    @objc var ledgerdepartment = String()
    @objc var mail = String()
    @objc var managerDisplayName = String()
    @objc var managerEmail = String()
    @objc var managerUPI = String()
    @objc var preferredLanguage = String()
    @objc var salesArea = String()
    @objc var salesCategoryID = String()
    @objc var salesDistrict = String()
    @objc var salesForceName = String()
    @objc var salesForceNumber = String()
    @objc var salesFranchise = String()
    @objc var salesFranchiseID = String()
    @objc var salesPMI = String()
    @objc var salesRegion = String()
    @objc var salesSampling = String()
    @objc var sn = String()
    @objc var spaceStructureID = String()
    @objc var telephoneNumber = String()
    @objc var territory = String()
    @objc var title = String()
    @objc var upi = String()
    @objc var salesTerritoryTypeID = String()
    @objc var salesSalesForce = String()
    @objc var spacePropertyID = String()
    @objc var spaceLocationID = String()
    @objc var userType = String()
    @objc var baseFranchiseID = String()
    
    // Reset all AOFKit values
    @objc func resetAOFManager() {
        self.adDomain = ""
        self.adLogon = ""
        self.country = ""
        self.company = ""
        self.department = ""
        self.displayName = ""
        self.division = ""
        self.divisionCode = ""
        self.divisionName = ""
        self.employeeType = ""
        self.fullName = ""
        self.givenName = ""
        self.initials = ""
        self.mail = ""
        self.managerDisplayName = ""
        self.managerEmail = ""
        self.managerUPI = ""
        self.preferredLanguage = ""
        self.salesArea = ""
        self.salesCategoryID = ""
        self.salesDistrict = ""
        self.salesForceName = ""
        self.salesForceNumber = ""
        self.salesFranchise = ""
        self.salesFranchiseID = ""
        self.salesPMI = ""
        self.salesRegion = ""
        self.salesSampling = ""
        self.sn = ""
        self.spaceStructureID = ""
        self.telephoneNumber = ""
        self.territory = ""
        self.title = ""
        self.upi = ""
        self.location = ""
        self.locationCode = ""
        self.ledgerdepartment = ""
        self.departmentcode = ""
        self.spacePropertyID = ""
        self.spaceLocationID = ""
        self.salesTerritoryTypeID = ""
        self.salesSalesForce = ""
        self.baseFranchiseID = ""
    }
    
    @objc func setValues(jsonData: NSDictionary)
    {
        self.adDomain = (jsonData["adDomain"] as? String) ?? ""
        self.adLogon = (jsonData["adLogon"] as? String) ?? ""
        self.country = (jsonData["c"] as? String) ?? ""
        self.company = (jsonData["company"] as? String) ?? ""
        self.department = (jsonData["department"] as? String) ?? ""
        self.displayName = (jsonData["displayName"] as? String) ?? ""
        self.division = (jsonData["divisionName"] as? String) ?? ""
        self.divisionCode = (jsonData["divisionCode"] as? String) ?? ""
        self.divisionName = (jsonData["divisionName"] as? String) ?? ""
        self.employeeType = (jsonData["employeeType"] as? String) ?? ""
        self.fullName = (jsonData["displayName"] as? String) ?? ""
        self.givenName = (jsonData["givenName"] as? String) ?? ""
        self.initials = (jsonData["initials"] as? String) ?? ""
        self.mail = (jsonData["mail"] as? String) ?? ""
        self.managerDisplayName = (jsonData["managerDisplayName"] as? String) ?? ""
        self.managerEmail = (jsonData["managerEmail"] as? String) ?? ""
        self.managerUPI = (jsonData["managerUpi"] as? String) ?? ""
        self.preferredLanguage = (jsonData["preferredLanguage"] as? String) ?? ""
        self.salesArea = (jsonData["salesArea"] as? String) ?? ""
        self.salesCategoryID = (jsonData["salesCategoryID"] as? String) ?? ""
        self.salesDistrict = (jsonData["salesDistrict"] as? String) ?? ""
        self.salesForceName = (jsonData["salesForceName"] as? String) ?? ""
        self.salesForceNumber = (jsonData["salesForceNumber"] as? String) ?? ""
        self.salesFranchise = (jsonData["salesFranchise"] as? String) ?? ""
        self.salesFranchiseID = (jsonData["salesFranchiseID"] as? String) ?? ""
        self.salesPMI = (jsonData["salesPMI"] as? String) ?? ""
        self.salesRegion = (jsonData["salesRegion"] as? String) ?? ""
        self.salesSampling = (jsonData["salesSampling"] as? String) ?? ""
        self.sn = (jsonData["sn"] as? String) ?? ""
        self.spaceStructureID = (jsonData["spaceStructureID"] as? String) ?? ""
        self.telephoneNumber = (jsonData["telephoneNumber"] as? String) ?? ""
        self.territory = (jsonData["territory"] as? String) ?? ""
        self.title = (jsonData["title"] as? String) ?? ""
        self.upi = (jsonData["upi"] as? String) ?? ""
        self.location = (jsonData["location"] as? String) ?? ""
        self.locationCode = (jsonData["locationCode"] as? String) ?? ""
        self.ledgerdepartment = (jsonData["ledgerdepartment"] as? String) ?? ""
        self.departmentcode = (jsonData["departmentcode"] as? String) ?? ""
        self.spacePropertyID = (jsonData["spacePropertyID"] as? String) ?? ""
        self.spaceLocationID = (jsonData["spaceLocationID"] as? String) ?? ""
        self.salesTerritoryTypeID = (jsonData["salesTerritoryTypeID"] as? String) ?? ""
        self.salesSalesForce = (jsonData["salesSalesForce"] as? String) ?? ""
        if self.salesFranchise.count > 0 {
            self.userType = kUserTypeFU
            ZMUserActions.sharedInstance()?.isFieldUser = true
        } else {
            self.userType = kUserTypeHO
            ZMUserActions.sharedInstance()?.isFieldUser = false
        }
    }
    
    @objc func getValuesAndSetParams() -> (Bool)
    {
        let aofUserData = AOFManager.oauthClient?.decryptDictionaryAndReturnDetails()
        var valuesSaved = false
        if ((aofUserData?.count)! > 0)
        {
            self.setValues(jsonData: aofUserData!)
            valuesSaved = true
        }
        return valuesSaved
    }
    
    @objc func saveToCoreData() {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStore(completion: { (bool, errorM) -> Void in
            if (errorM == nil)
            {
                AbbvieLogging.logInfo("Saving AOF user values to Core data success")
            }
            else
            {
                AbbvieLogging.logError("Saving AOF user values to Core data failed: \(String(describing: errorM))")
            }
        })
    }
    
    // Set settings bundle value
    @objc func setSettingsValue()
    {
        if let k511Id = self.prontoUserDefaults.getAOF511Id()
        {
            UserDefaults.standard.set(k511Id, forKey: "AOFKitUser")
        }
        UserDefaults.standard.set(self.adDomain, forKey: "AOFKitDomain")
    }
    
    // Reset setting bundle value
    @objc func resetSettingsValue()
    {
        UserDefaults.standard.set(kNotLoggedIn, forKey: "AOFKitUser")
        UserDefaults.standard.set(kNotLoggedIn, forKey: "AOFKitDomain")
    }
    
    // Set version from AOF framework
    @objc func setAOFKitVersion()
    {
        var AOFDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "AOFKitDetails", ofType: "plist") {
            AOFDictionary = NSDictionary(contentsOfFile: path)
        }
        let bundleID = AOFDictionary?.value(forKey: "BundleID") as? String
        if let bundle = Bundle(identifier: bundleID ?? "") {
          if let build = bundle.infoDictionary?["CFBundleShortVersionString"] {
                UserDefaults.standard.set(build, forKey: "AOFKitVersion")
            }
        }
    }
    
    // Get Access token
    @objc func getAccessToken() -> String
    {
        var accessToken = ""
        if AOFManager.hasValidAccessToken() == true
        {
            accessToken = AOFManager.accessToken()!
        }
        return accessToken
    }

    
    // Check if the app is already logged in
    @objc func checkForAOFLogin() -> Bool
    {
        var hasValidAccessToken = false
        if AOFManager.hasValidAccessToken() == true {
            hasValidAccessToken = true
        }
        return hasValidAccessToken
    }
    
    
    // Get document directory path
    @objc func documentsDir() -> String
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    // Get cache directory path
    @objc func cachesDir() -> String
    {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    // Reset AOF value and process logout
    @objc func AOFKitLogout()
    {
        if AOFManager.hasValidAccessToken() == true {
            if AOFManager.clearKeyChain() == true {
                NotificationCenter.default.post(name: NSNotification.Name.aofLogoutSuccess, object: nil)
                self.prontoUserDefaults.setAOF511Id("")
                self.resetAOFManager()
                self.resetSettingsValue()
                AOFManager.oauthClient?.clearUserDetails()
            }
        }
    }
}
