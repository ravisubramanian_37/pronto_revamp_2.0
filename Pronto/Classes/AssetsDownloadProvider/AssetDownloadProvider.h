//
//  AssetDownloadProvider.h
//  Pronto
//
//  Created by cccuser on 04/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Asset;
@class EventOnlyAsset;

@interface AssetDownloadProvider : NSObject

+ (id)sharedAssetDownloadProvider;
+ (AssetDownloadProvider *)sharedInstance;

-(void)submitRequestToDownloadAsset:(Asset*) asset;

- (void)clearOnLogout;

- (void)handleTapToDownloadScenarioForAsset:(Asset*)asset;

- (void)handleTapToDownloadScenarioForAssetOnFailure:(Asset*)asset;

- (void)pauseOfflineDownload:(BOOL)pause;

@end
