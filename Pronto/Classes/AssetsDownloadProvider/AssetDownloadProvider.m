//
//  AssetDownloadProvider.m
//  Pronto
//
//  Created by cccuser on 04/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "AssetDownloadProvider.h"
#import "Asset.h"
//#import "ZMAbbvieAPI.h"
#import "Constant.h"
#import "Pronto-Swift.h"

#define MAX_NNUMBER_ASSET_DOWNLOAD_SIMULATANIOUSLY 2
@interface AssetDownloadProvider()

@property(nonatomic, strong)NSMutableArray * assetDownloadInProgress;
@property(nonatomic, strong)NSMutableArray * totalSubmitionAssetDownload;

@property (nonatomic, assign) BOOL isPauseOfflineDownloadEnabled;

@end

@implementation AssetDownloadProvider
+ (AssetDownloadProvider *)sharedInstance {
  
  @synchronized(self)
  {
      static AssetDownloadProvider *manager = nil;
      static dispatch_once_t onceToken;
      
      dispatch_once(&onceToken, ^{
          manager = [[self alloc] init];
      });
      return manager;
  }
}

- (void)pauseOfflineDownload:(BOOL)pause
{
    self.isPauseOfflineDownloadEnabled = pause;
    
    if (self.isPauseOfflineDownloadEnabled == NO)
    {
        if(self.totalSubmitionAssetDownload.count > 0)
        {
            Asset* newAsset = [self.totalSubmitionAssetDownload firstObject];
            
            if(newAsset != nil && newAsset.assetID != nil)
            {
                [self.assetDownloadInProgress addObject:newAsset];
                [self.totalSubmitionAssetDownload removeObject:newAsset];
                
                [[EventsServiceClass sharedAPIManager] downloadAssetFor:newAsset];
                
                [AbbvieLogging logInfo:@"eoa -- asset Downloading start from pause"];
            }
        }
    }
    else
    {
        [AbbvieLogging logInfo:@"eoa -- asset pausing offline download"];
    }
}

+ (id)sharedAssetDownloadProvider
{
    static AssetDownloadProvider *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
        manager.assetDownloadInProgress = [NSMutableArray array];
        manager.totalSubmitionAssetDownload = [NSMutableArray array];
        manager.isPauseOfflineDownloadEnabled = NO;
        [[NSNotificationCenter defaultCenter] addObserver:manager selector:@selector(recievedNotification:) name:kAssetDownloadProviderCompletionNotification object:nil];
    });
    
    return manager;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)submitRequestToDownloadAsset:(Asset*) asset
{
    BOOL isPresent = NO;
    for(Asset *checkAsset in _totalSubmitionAssetDownload)
    {
        if([checkAsset.assetID longValue] == [asset.assetID longValue])
        {
            isPresent = YES;
            break;
        }
    }
    if(!isPresent && [[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"])
    {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Adding Asset into provider: %@ name is : %@",asset.assetID,asset.title]];
        
        [self.totalSubmitionAssetDownload addObject:asset];
        if(self.assetDownloadInProgress.count < MAX_NNUMBER_ASSET_DOWNLOAD_SIMULATANIOUSLY)
        {
            if (self.isPauseOfflineDownloadEnabled == NO)
            {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Initial Download started for: %@ name is : %@",asset.assetID,asset.title]];
                [self.assetDownloadInProgress addObject:asset];
                [self.totalSubmitionAssetDownload removeObject:asset];
                [[EventsServiceClass sharedAPIManager] downloadAssetFor:asset];
                
                [AbbvieLogging logInfo:@"eoa -- asset Downloading start from submitRequest"];
            }
        }
    }
}

- (void)recievedNotification:(NSNotification *)notification
{
    NSDictionary* userInfo = notification.userInfo;
    Asset* asset = (Asset*)userInfo[@"asset"];
    if([self.assetDownloadInProgress containsObject:asset])
    {
        [self.assetDownloadInProgress removeObject:asset];
        if(self.totalSubmitionAssetDownload.count > 0)
        {
            Asset* newAsset = [self.totalSubmitionAssetDownload firstObject];
            
            if(newAsset.assetID != nil && newAsset != nil)
            {
                if (self.isPauseOfflineDownloadEnabled == NO)
                {
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download started for: %@ name is : %@",newAsset.assetID,newAsset.title]];
                    
                    [self.assetDownloadInProgress addObject:newAsset];
                    [self.totalSubmitionAssetDownload removeObject:newAsset];
                    [[EventsServiceClass sharedAPIManager] downloadAssetFor:newAsset];
                    
                    [AbbvieLogging logInfo:@"eoa -- asset Downloading start from Notification"];
                }
            }
        }
    }
}

- (void)handleTapToDownloadScenarioForAsset:(Asset*)asset
{
    if([self.totalSubmitionAssetDownload containsObject:asset] == YES)
    {
        [self.totalSubmitionAssetDownload removeObject:asset];
    }
}

- (void)handleTapToDownloadScenarioForAssetOnFailure:(Asset*)asset
{
    if([self.totalSubmitionAssetDownload containsObject:asset] == NO)
    {
        [self.totalSubmitionAssetDownload addObject:asset];
    }
}

- (void)clearOnLogout {
    [self.assetDownloadInProgress removeAllObjects];
    [self.totalSubmitionAssetDownload removeAllObjects];
}
@end
