
#import <UIKit/UIKit.h>
@protocol FlatTabBarDelegate;

@interface FlatTabBarViewController : UIViewController
@property (weak, nonatomic) id<FlatTabBarDelegate> delegate;
@property (readwrite, nonatomic) int numberOfTabs;
@property (strong, nonatomic) NSMutableDictionary *tabBarItems;
@property (strong, nonatomic) NSString *selectedIndex;
@property (strong, nonatomic) UIColor *highlightedTextColor;
@property (strong, nonatomic) UIColor *bottomSepratorColor;
@property (strong, nonatomic) UIColor *labelBottomSepratorColor;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIFont *textFont;
@property (strong, nonatomic) UIFont *highlightedTextFont;
@property (readwrite, nonatomic) BOOL shouldAutomanageWidth;
@property (readwrite, nonatomic) float spaceBetweenTabs;
-(void)selectItemAtIndex:(NSInteger)index andNotifyBack:(BOOL)shouldNotify;
-(void)deselectAll;
-(void)reloadTabBar;

@end


@protocol FlatTabBarDelegate<NSObject>
@required
-(int)numberOfTabsInTabBar:(FlatTabBarViewController*) tabBar;
-(NSString*)titleForTabAtIndex:(NSInteger)index inTabBar:(FlatTabBarViewController*)tabBar;
-(BOOL)shouldAutomanageTabWidthInTabBar:(FlatTabBarViewController*) tabBar;
@optional
-(UIColor*)textColorForTabsinTabBar:(FlatTabBarViewController*)tabBar;
-(UIColor*)highlightedTextColorForTabsinTabBar:(FlatTabBarViewController*)tabBar;
-(UIFont*)fontForTabTextinTabBar:(FlatTabBarViewController*)tabBar;
-(UIFont*)highlightedFontForTabTextinTabBar:(FlatTabBarViewController*)tabBar;
-(void)didSelectedTabAtIndex:(NSInteger)index forTabBar:(FlatTabBarViewController*)tabBar;
-(float)widthForTabItemAtIndex:(NSInteger)index inTabBar:(FlatTabBarViewController*)tabBar;
-(float)spacingBetweenTabsForTabsInTabBar:(FlatTabBarViewController*)tabBar;
-(UIColor*)bottomSepratorColorForTabBar:(FlatTabBarViewController*)tabBar;
@end
