
#import "FlatTabBarViewController.h"

@interface FlatTabBarViewController ()

@end

@implementation FlatTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _numberOfTabs=0;
    _textColor=[UIColor lightGrayColor];
    _bottomSepratorColor=_textColor;
    _highlightedTextColor=[UIColor whiteColor];
    _textFont=[UIFont fontWithName:@"Verdana" size:14];
    _shouldAutomanageWidth=YES;
    _spaceBetweenTabs=5;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initStructs
{
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(numberOfTabsInTabBar:)])
    {
        _numberOfTabs=[_delegate numberOfTabsInTabBar:self];
    }
    
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(textColorForTabsinTabBar:)])
    {
        _textColor=[_delegate textColorForTabsinTabBar:self];
    }
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(bottomSepratorColorForTabBar:)])
    {
        _bottomSepratorColor=[_delegate bottomSepratorColorForTabBar:self];
    }
    
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(highlightedTextColorForTabsinTabBar:)])
    {
        _highlightedTextColor=[_delegate highlightedTextColorForTabsinTabBar:self];
    }
    
    
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(fontForTabTextinTabBar:)])
    {
        _textFont=[_delegate fontForTabTextinTabBar:self];
    }
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(highlightedFontForTabTextinTabBar:)])
    {
        _highlightedTextFont=[_delegate highlightedFontForTabTextinTabBar:self];
    }
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(shouldAutomanageTabWidthInTabBar:)])
    {
        _shouldAutomanageWidth=[_delegate shouldAutomanageTabWidthInTabBar:self];
    }
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(spacingBetweenTabsForTabsInTabBar:)])
    {
        _spaceBetweenTabs=[_delegate spacingBetweenTabsForTabsInTabBar:self];
    }
    
    [self loadTabBar];
}
-(void)setDelegate:(id<FlatTabBarDelegate>)delegate
{
    _delegate=delegate;
    [self initStructs];
}


-(void)loadTabBar
{
    float itemWidth=0;
    if(_numberOfTabs>0)
    {
        if(_tabBarItems==nil)
        {
            _tabBarItems=[[NSMutableDictionary alloc]init];
        }
        UIView *seprator=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-1, self.view.bounds.size.width, 1)];
        seprator.backgroundColor=_bottomSepratorColor; //_textColor;
        seprator.autoresizingMask=UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:seprator];
        float xPosition=0;
        for(int i=0;i<_numberOfTabs;i++)
        {
            if(_shouldAutomanageWidth==YES)
            {
                itemWidth=self.view.frame.size.width/_numberOfTabs;
            }
            else
            {
                if(_delegate!=nil && [_delegate respondsToSelector:@selector(widthForTabItemAtIndex:inTabBar:)])
                {
                    itemWidth=[_delegate widthForTabItemAtIndex:i inTabBar:self];
                }
                else
                {
                    itemWidth=self.view.frame.size.width/_numberOfTabs;
                }
            }
        
            UIView *item=[[UIView alloc]initWithFrame:CGRectMake(xPosition, 0, itemWidth, self.view.frame.size.height)];
            xPosition+=item.frame.size.width;
            xPosition+=_spaceBetweenTabs;
            //item.autoresizingMask=UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            UILabel *label=[[UILabel alloc]initWithFrame:item.bounds/*CGRectMake(0, 0, item.frame.size.width, item.frame.size.height)*/];
            
            label.autoresizingMask=UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            label.font=_textFont;
            label.textColor=_textColor;
            if(_delegate!=nil && [_delegate respondsToSelector:@selector(titleForTabAtIndex:inTabBar:)])
            {
                label.text=[_delegate titleForTabAtIndex:i inTabBar:self];
            }
            else
            {
                label.text = [NSString stringWithFormat:NSLocalizedString(@"Tab %d",nil),i];
            }
            label.tag=0;
            label.textAlignment = NSTextAlignmentCenter;
            label.adjustsFontSizeToFitWidth = YES;
            label.minimumScaleFactor = 0.7;

            //UIView *itemSeprator=[[UIView alloc]initWithFrame:CGRectMake(0, label.frame.size.height, item.frame.size.width, 2)];
            UIView *itemSeprator=[[UIView alloc]initWithFrame:CGRectMake(0, item.frame.size.height-3, item.frame.size.width, 3)];
            itemSeprator.backgroundColor=[UIColor clearColor];;
            itemSeprator.autoresizingMask=UIViewAutoresizingFlexibleWidth;
            itemSeprator.tag=1;
            [item addSubview:label];
            [item addSubview:itemSeprator];
            [self.view addSubview:item];
            [self.view bringSubviewToFront:item];
            [_tabBarItems setObject:item forKey:[NSString stringWithFormat:@"%d",i]];
        }
        
    }
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    for(int i=0;i<_tabBarItems.count;i++)
    {
        UIView *temp=[_tabBarItems objectForKey:[NSString stringWithFormat:@"%d",i]];
        if(temp!=nil)
        {
            CGPoint location = [touches.anyObject locationInView:temp];
            if(CGRectContainsPoint(temp.bounds, location))
            {
                [self selectItemAtIndex:i andNotifyBack:YES];
                break;
            }
        }
    }
    
}


-(void)deselectAll
{
    UIView *tabView=_tabBarItems[_selectedIndex];
    if(tabView)
    {
        for(int i=0;i<tabView.subviews.count;i++)
        {
            UIView *subView=tabView.subviews[i];
            if([subView isKindOfClass:[UILabel class]] && subView.tag==0)
            {
                ((UILabel*)subView).textColor=_textColor;
            }
            else if([subView isKindOfClass:[UIView class]] && subView.tag==1)
            {
                subView.backgroundColor=[UIColor clearColor];
            }
        }
    }
    _selectedIndex=nil;
}

-(void)selectItemAtIndex:(NSInteger)index andNotifyBack:(BOOL)shouldNotify
{
    if(index>=0 && index<_numberOfTabs)
    {
        NSString *indexToSelect=[NSString stringWithFormat:@"%ld",(long)index];
        if([indexToSelect isEqualToString:_selectedIndex]) return;
        
        //First Unselect
        UIView *tabView=_tabBarItems[_selectedIndex];
        if(tabView)
        {
            for(int i=0;i<tabView.subviews.count;i++)
            {
                UIView *subView=tabView.subviews[i];
                if([subView isKindOfClass:[UILabel class]] && subView.tag==0)
                {
                    ((UILabel*)subView).textColor=_textColor;
                    ((UILabel*)subView).font=_textFont;
                }
                else if([subView isKindOfClass:[UIView class]] && subView.tag==1)
                {
                    subView.backgroundColor=[UIColor clearColor];
                }
            }
        }
        //Select the new one now
        tabView= _tabBarItems[indexToSelect];
        if(tabView)
        {
            for(int i=0;i<tabView.subviews.count;i++)
            {
                UIView *subView=tabView.subviews[i];
                if([subView isKindOfClass:[UILabel class]] && subView.tag==0)
                {
                    ((UILabel*)subView).textColor=_highlightedTextColor;
                    ((UILabel*)subView).font=_highlightedTextFont;
                }
                else if([subView isKindOfClass:[UIView class]] && subView.tag==1)
                {
                    subView.backgroundColor= _labelBottomSepratorColor;
                }
            }
        }
        _selectedIndex=indexToSelect;
        if(_delegate!=nil && [_delegate respondsToSelector:@selector(didSelectedTabAtIndex:forTabBar:)] && shouldNotify)
        {
            [_delegate didSelectedTabAtIndex:index forTabBar:self];
        }
    }
}

- (void)reloadTabBar
{
    [self deselectAll];
    [self initStructs];
}

@end
