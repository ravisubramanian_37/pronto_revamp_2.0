//
// DividerView.swift
//
//
//  Created by m-666346 on 20/12/17.
//

import UIKit
class DividerView: UIView
{
    @IBInspectable  var overrideColor:UIColor? {
        didSet{
            super.backgroundColor = overrideColor
        }
    }
    
    override var backgroundColor: UIColor? {
                get
                {
                    return super.backgroundColor
                }
                set
                {
                    guard let overrideColor = self.overrideColor else {
                        super.backgroundColor = UIColor.getFromHexValueDAE4EF()
                        return
                    }
                    super.backgroundColor = overrideColor
                }
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.backgroundColor = UIColor.getFromHexValueDAE4EF()
    }
   
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    class func GetDividerView(withFrame frame: CGRect) -> DividerView
    {
        let view    = DividerView(frame:frame)
        return view
    }
}
