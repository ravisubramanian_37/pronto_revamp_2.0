//
//  ZMContentHeader.m
//  Pronto
//
//  Created by m-666346 on 19/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "ZMContentHeader.h"
#import "ZMProntoTheme.h"
#import "ZMUserActions.h"
#import "Pronto-Swift.h"
#import "UIImage+ImageDirectory.h"
#import "Constant.h"


@interface ZMContentHeader ()
@property (weak, nonatomic) IBOutlet UILabel *franchiseCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *sortByButton;
@property (weak, nonatomic) IBOutlet UIButton *listGridButton;

//Specific to Global Search
@property (weak, nonatomic) IBOutlet UILabel *searchResultLabel;
@end

@implementation ZMContentHeader
+ (NSString*)GetZMContentHeaderNibName
{
    return @"ZMContentHeader";
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
    [self registerNotification:kZMSortMenuDidSelectRowNotification];
}

- (void)removeNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:name object:nil];
}
- (void)registerNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:name object:nil];
}

- (void)recievedNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:kZMSortMenuDidSelectRowNotification])
    {
        NSDictionary* userInfo = notification.userInfo;
        NSString *label = userInfo[ZMSortMenuSelectedCellLabelValue];
        [self formatButton:@"()" withText:label andButton:self.sortByButton];
    }
}


- (void)setupUI
{
    self.franchiseCountLabel.font       = [[ZMProntoTheme sharedTheme] bigNormalLato];
    self.franchiseCountLabel.textColor  = [[ZMProntoTheme sharedTheme] franchiseCountColor];
    
    self.searchResultLabel.font       = [[ZMProntoTheme sharedTheme] bigBoldLato];
    self.searchResultLabel.textColor  = [[ZMProntoTheme sharedTheme] franchiseCountColor];
    
    [self prepareGridListButton];
}

- (IBAction)recentButtonAction:(UIButton *)sender
{
    if (self.zmContentHeaderDelegate != nil && [self.zmContentHeaderDelegate  respondsToSelector:@selector(conentHeader:didSelectRecentlyButton:)])
    {
        [self.zmContentHeaderDelegate conentHeader:self didSelectRecentlyButton:sender];
    }
}

-(void) updateFranchiseLabelCount:(long)count currentTab:(NSString*)tabSelected
{
    NSString *itemString = (count == 1)?@"Asset":@"Assets";
    if(count == 0)
    {
        [ZMUserActions sharedInstance].selectLeftMenu = @"All Franchise Assets";
        [ZMUserActions sharedInstance].assetsCount = 0;
    }
    _franchiseCountLabel.text = [NSString stringWithFormat:@"%@ (%ld %@)", tabSelected, count, itemString];
    NSRange range = [_franchiseCountLabel.text rangeOfString:[NSString stringWithFormat:@"%@ ",tabSelected]];
    NSMutableAttributedString *attText = [[NSMutableAttributedString alloc] initWithString:_franchiseCountLabel.text];
    [attText setAttributes:@{NSFontAttributeName:[[ZMProntoTheme sharedTheme] bigBoldLato]}
                     range:range];
    _franchiseCountLabel.attributedText = attText;
}

/*
 - (IBAction)viewChangesButtonTapped:(id)sender {
 if (grid.hidden) {
 grid.hidden = false;
 listTableView.hidden = true;
 [listGridbutton setImage:[UIImage imageNamed:@"listIcon"] forState:UIControlStateNormal];
 [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Grid_Franchise"];
 } else {
 grid.hidden = true;
 listTableView.hidden = false;
 listTableView.delegate = self;
 listTableView.dataSource = self;
 [listGridbutton setImage:[UIImage imageNamed:@"thumbnailIcon"] forState:UIControlStateNormal];
 [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Grid_Franchise"];
 }
 [self reloadAssets];
 }
 
 */

- (IBAction)gridButtonAction:(UIButton *)sender
{
    LibraryViewType viewType = LibraryViewTypeList;
    
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if ([showingGridView isEqualToString:@"YES"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:keyUserDefaultGridFranchise];
        [self.listGridButton setImage:[UIImage GetThumbnailIconImage] forState:UIControlStateNormal];
        viewType = LibraryViewTypeList;
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:keyUserDefaultGridFranchise];
        [self.listGridButton setImage:[UIImage GetListIconImage] forState:UIControlStateNormal];
        viewType = LibraryViewTypeGrid;
    }
    
    if (self.zmContentHeaderDelegate != nil && [self.zmContentHeaderDelegate  respondsToSelector:@selector(conentHeader:didSelectGridButton:)])
    {
        [self.zmContentHeaderDelegate conentHeader:self didSelectGridButton:viewType];
    }
}

- (IBAction)backButtonAction:(UIButton *)sender
{
    if (self.zmContentHeaderDelegate != nil &&
        [self.zmContentHeaderDelegate respondsToSelector:@selector(conentHeader:didTapBackButtonSender:)])
    {
        [self.zmContentHeaderDelegate conentHeader:self didTapBackButtonSender:sender];
    }
}
- (void)prepareGridListButton
{
    // Check for nil condition
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if ([showingGridView isEqualToString:@"YES"]) // If it nil, it means default to thumbnail
    {
        [self.listGridButton setImage:[UIImage GetListIconImage] forState:UIControlStateNormal];
    }
    else
    {
        [self.listGridButton setImage:[UIImage GetThumbnailIconImage] forState:UIControlStateNormal];
    }
}

-(void) formatButton:(NSString *)stringPattern withText:(NSString *)labelText andButton:(UIButton *)button
{
    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:stringPattern options:0 error:nil];
    [expression enumerateMatchesInString:labelText options:0 range:NSMakeRange(0,[labelText length])
                              usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                  NSRange hashRange = [result rangeAtIndex:1];
                                  
                                  NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithString:labelText] attributes:[[ZMProntoTheme sharedTheme] sortButtonTitleStyle]];
                                  [attributedText setAttributes:[[ZMProntoTheme sharedTheme] buttonTitleMainStyle] range:hashRange];
                                  [button setAttributedTitle:attributedText forState:UIControlStateNormal];
                              }];
}

/**
 *  This method updates the label on the sort action acordingly to user selection
 */
- (void)updateSortByButton {
    
    // New Search Implementation
    NSString *sortLabel = @"View Count";
    
    NSString* selectedValue = [ZMUserActions sharedInstance].selectedSortByMenu;
    NSString* gettingValue = @"";
    
    if([ZMProntoManager sharedInstance].sortBy.count > 0)
    {
        for (NSDictionary *sort in [ZMProntoManager sharedInstance].sortBy) {
            if ([[sort objectForKey:@"selected"] intValue] == 1) {
                sortLabel = [NSString stringWithFormat:@"%@ ", [sort objectForKey:@"label"]];
                
                gettingValue = [sort objectForKey:@"label"];
            }
        }
    }
    else
    {
        for (NSDictionary *sort in [ZMUserActions sharedInstance].sorts) {
            
            if ([[sort objectForKey:@"selected"] intValue] == 1) {
                sortLabel = [NSString stringWithFormat:@"%@ ", [sort objectForKey:@"label"]];
                
                gettingValue = [sort objectForKey:@"label"];
            }
        }
    }
    
    
    if(![selectedValue isEqualToString:gettingValue])
    {
//        if(headerView.isResetSelected)
//        {
//            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
//            headerView.isResetSelected = NO;
//
//        }
//        else if(headerView.isResetFranchiseSelected)
//        {
//            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
//            headerView.isResetFranchiseSelected = NO;
//
//        }
//        else
//        {
//            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
//            headerView.isResetSelected = NO;
//
//        }
    }
    else if([selectedValue isEqualToString:gettingValue])
    {
        sortLabel = [NSString stringWithFormat:@"%@ ", selectedValue];
        
    }
    else
    {
        sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
        
    }
    
    [self formatButton:@"()" withText:[NSString stringWithString:sortLabel] andButton:self.sortByButton];
}

/*
 FranchiseCount
 _library.franchiseCount.font = [theme bigNormalLato];
 _library.franchiseCount.textColor = [theme franchiseCountColor];
 */

/*
 NSString *totalAsset = [NSString stringWithFormat:@"%@ (%d %@)", @"Franchise ", total, itemString];
 _library.franchiseCount.text = totalAsset;
 */

- (void)updateFranchiselLabelForGlobalSearchWithText:(NSString*)text
{
    _franchiseCountLabel.font       = [[ZMProntoTheme sharedTheme] bigBoldLato]; // make combination of normal bold
    _franchiseCountLabel.textColor  = [[ZMProntoTheme sharedTheme] franchiseCountColor];
}

- (void)shouldEnableRecentlyAddedAndGridSelectionControl:(BOOL)enable
{
    self.sortByButton.userInteractionEnabled = enable;
    self.listGridButton.userInteractionEnabled = enable;
}

@end
