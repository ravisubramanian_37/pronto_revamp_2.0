//
//  ZMContentHeader.h
//  Pronto
//
//  Created by m-666346 on 19/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, LibraryViewType) {
    LibraryViewTypeList,
    LibraryViewTypeGrid,
};

@class ZMContentHeader;

@protocol ZMContentHeaderDelegate <NSObject>

//Define enum
- (void)conentHeader:(ZMContentHeader*)contentHeader didSelectRecentlyButton:(UIButton*)sender;
- (void)conentHeader:(ZMContentHeader*)contentHeader didSelectGridButton:(LibraryViewType)viewType;
//Specific to Global Search Implementation
- (void)conentHeader:(ZMContentHeader*)contentHeader didTapBackButtonSender:(UIButton*)sender;
@end
// zm remove
@interface ZMContentHeader : UIView

@property (nonatomic, assign) id <ZMContentHeaderDelegate> zmContentHeaderDelegate;

+ (NSString*)GetZMContentHeaderNibName;
- (void)updateSortByButton;
-(void) updateFranchiseLabelCount:(long)count currentTab:(NSString*)tabSelected;
- (void)updateFranchiselLabelForGlobalSearchWithText:(NSString*)text;
- (void)shouldEnableRecentlyAddedAndGridSelectionControl:(BOOL)enable;

// How to load nib name with delegate
@end
