//
//  Folder+CoreDataProperties.m
//  Pronto
//
//  Created by Oscar Robayo on 22/10/15.
//  Copyright © 2015 Zemoga. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Folder+CoreDataProperties.h"

@implementation Folder (CoreDataProperties)

@dynamic date;
@dynamic folderID;
@dynamic name;
@dynamic qty;
@dynamic assets;
@dynamic briefcase;

@end
