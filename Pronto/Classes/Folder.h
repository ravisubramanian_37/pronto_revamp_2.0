//
//  Folder.h
//  Pronto
//
//  Created by Oscar Robayo on 22/10/15.
//  Copyright © 2015 Zemoga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset, Briefcase;

NS_ASSUME_NONNULL_BEGIN

@interface Folder : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Folder+CoreDataProperties.h"
