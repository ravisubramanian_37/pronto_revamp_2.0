//
//  Constant.m
//  Pronto
//
//  Created by m-666346 on 21/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "Constant.h"
#import "ZMUserActions.h"
#import "Asset+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Brand.h"
#import "Franchise.h"
#import "Preferences+CoreDataClass.h"
#import "Preferences+CoreDataProperties.h"
#import "Event+CoreDataProperties.h"
#import "Pronto-Swift.h"
#import "EventCoreDataManager.h"

#import "EventDuration+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "Session+CoreDataClass.h"

#pragma mark - NSUserDefaults Keys
NSString *const keyUserDefaultGridFranchise = @"Grid_Franchise";



NSString *const kPreferenceListVCWillShowNotification               = @"PreferencesListVCWillShow";
NSString *const kPreferenceListVCDidSavePreferencesNotification     = @"PreferencesListVCDidSaveNotification";
NSString *const kPreferenceListVCDismissNotification                = @"PreferencesListVCDismissNotification";
NSString *const kZMAssetViewControllerGoBackNotification            = @"ZMAssetViewControllerGoBackNotification";
NSString *const kZMPDFDocumentDidCloseButtonPressedNotification     = @"ZMPDFDocumentDidCloseButtonPressedNotification";
NSString *const kZMSortMenuDidSelectRowNotification                 = @"ZMSortMenuDidSelectRowNotification";
NSString *const kAssetDownloadProviderCompletionNotification        = @"AssetDownloadProviderCompletionNotification";
NSString *const kEventOnlyAssetDownloadProviderCompletionNotification   = @"EventOnlyAssetDownloadProviderCompletionNotification";
NSString *const kSignOutOnLaunchNotification                        = @"SignOutOnLaunchNotification";
NSString *const kAssetV2CompletionNotification                      = @"AssetV2CompletionNotification";
NSString *const kEventDeletedFromCMSNotification                    = @"EventDeletedFromCMSNotification";
NSString *const kOpenDeeplinkNotification                           = @"OpenDeeplinkNotification";


NSString *const keyCompleted = @"completed";
NSString *const keyFailed = @"failed";
NSString *const keyIn_progress = @"in_progress";
NSString *const keyCompleted_partialy = @"completed_partialy";

NSString *const ProntoTabOptionToolsAndTrainingAllAssets = @"All Field Tools Training Assets";
NSString *const ProntoTabOptionToolsAndTrainingFavoriteAssets = @"My Field Tools Training Favorites";

NSString *const ProntoTabOptionCompetitorsAllAssets = @"All Competitors Assets";
NSString *const ProntoTabOptionCompetitorsFavoriteAssets = @"My Competitors Favorites";

NSString *const ProntoTabOptionFranchise = @"Franchise";
NSString *const ProntoTabOptionCompetitor = @"Competitors";
NSString *const ProntoTabOptionToolsAndTraining = @"Tools Training"; //Tools Training
NSString *const ProntoTabOptionEvent = @"Events";

// Global Search
NSString *const keyMyFranchise      = @"Franchise";
NSString *const keyMyCompetitors    = @"Competitors";
NSString *const keyMyToolsTraing    = @"Field Tools Training";
NSString *const keyMyEvents         = @"Events";

NSString *const ZMSortMenuSelectedCellLabelValue = @"ZMSortMenuSelectedCellLabelValue";

// Messaging Text

NSString *const ProntoMessageGettingAssets = @"Getting Assets";

NSString *const ProntoMessageGettingEvents = @"Getting Events";

NSString *const ProntoMessageGettingEventDetails = @"Getting Event Details";

NSString *const ProntoMessageGettingSearchResults = @"Getting Search Results";

NSString *const ProntoMessagePreparingDataToPrint = @"Preparing data to print";

NSString *const ProntoEventsCoverflowImageFolderName = @"Events_Coverflow";

NSString *const ProntoMessageGettingAllEvents = @"Please wait while getting events";

NSString *const ProntoMessageDownloadingAsset = @"Downloading asset";

NSString *const ProntoMessageAssetNotFound = @"Asset not found";

NSString *const keySearchString = @"search";
NSString *const keyMainSync     = @"mainSync";



NSString *const ProntoTabOptionEventIDValue = @"9999";

NSString *const keyAssetString = @"asset";

NSString *const PreviousAssetEmptyString = @"--";

NSString *const kAssetAssociated = @"Asset Associated";

NSString *const kAssetsAssociated = @"Assets Associated";

//For Print
NSString *const PrintConstantLocation           = @"Location: ";
NSString *const PrintConstantDescription        = @"Description: ";


NSString *const EventHasBeenDeletedMessage = @"This event has been deleted.";
NSString *const AlertOKTitle = @"OK";

//For AOFKit
NSString *const kAOFLoginSuccessNotification = @"AOFLoginSuccessNotification";
NSString *const kAOFLogoutSuccessNotification = @"AOFLogoutSuccessNotification";
NSString *const kNotLoggedIn = @"Not Logged In";
NSString *const kUserTypeHO = @"USInHouse";
NSString *const kUserTypeFU = @"USFieldSales";
NSString *const kAOFAccessTokenName = @"aofAccessToken";
NSString *const dlServerHostName = @"dlserver";

NSString *const kSecureValueToEncrypt = @"isFromSecuredApp";
NSString *const keyToEncrypt = @"54 52 d7 16 87 b6 bc 2c 93 89 c3 34 9f dc 17 fb 3d fb ba 62 24 af fb 76 76 e1 33 79 36 cd d6 03";

NSString *const appCenterConstStage = @"1c62710f-91da-434c-9a65-3031a20f5f65";
NSString *const appCenterConstPreProd = @"01a81cbd-8e23-cf78-f27e-f8143b4a1494";
NSString *const appCenterConstProd = @"443cf6e6-ba29-1151-3828-582777dc844f";

NSString *const kMultipleFranchiseForHomeOfficeMessage = @"You must select a single franchise to view Promoted Contents";

//Encryption and login Keys
NSString *const kUsername = @"abbvie_services";
NSString *const kNewUsername = @"abbvie_services";
NSString *const kPassword = @"services";
NSString *const kRNEncryptionKey = @"NcRfUjXn2r5u8x/A?D(G-KaPdSgVkYp3";
NSString *const kRNEncryptionKeyForStage = @"fJLWwDeAYJtruHCyOuLICWkzQmqtdf2E";
NSString *const kRNEncryptionKeyForProduction = @"TjWnZr4u7x!z%C*F-JaNdRgUkXp2s5v8";
NSString *const kRNNewEncryptionKey = @"fJLWwDeAYJtruHCyOuLICWkzQmqtdf2E";

//For Service calls
NSString *const kTokenKey = @"token";

@implementation Constant


+ (id)sharedConstant
{
    @synchronized(self)
    {
        static Constant *constant = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            constant = [[self alloc] init];
            constant.ProntoTabOptionFranchiseID = @"";
            constant.ProntoTabOptionCompetitorID = @"";
            constant.ProntoTabOptionToolsAndTrainingID = @"";
            constant.ProntoTabOptionEventID            = ProntoTabOptionEventIDValue;
        });
        return constant;
    }
}

- (NSString*)getDefaultBucketID
{
    return self.ProntoTabOptionFranchiseID;
}



+ (enum ZMProntoFileTypes)GetFileTypeForAsset:(Asset *)selectedAsset
{
    enum ZMProntoFileTypes type = ZMProntoFileTypesDocument;
    
   // if ([selectedAsset.uri_full rangeOfString:@"brightcove://"].location == 0)
    if ([selectedAsset.file_mime isEqualToString:@"video/brightcove"])
    {
        type = ZMProntoFileTypesBrightcove;
    }
    else if ([selectedAsset.file_mime isEqualToString:@"weblink" ]) //Web View - Ability to provide a Web link as an asset type.
    {
        type = ZMProntoFileTypesWeblink;
    }
    else if ([[[selectedAsset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"])
    {
        type = ZMProntoFileTypesPDF;
    }
    else if ([[[selectedAsset.uri_full pathExtension] lowercaseString] isEqualToString:@"mp3"])
    {
        type = ZMProntoFileTypesAudio;
    }
    
    return type;
}


+ (NSString*)GetCurrentActiveTabOptionAsString
{
    NSInteger currentTabIndex = [ZMUserActions sharedInstance].currentSelectedTabIndex;
    switch (currentTabIndex)
    {
        case 1:
            return ProntoTabOptionCompetitor;
            break;
        case 2:
            return ProntoTabOptionToolsAndTraining;
            break;
        case 3:
            return ProntoTabOptionEvent;
            break;
            
        default:
            return ProntoTabOptionFranchise;
            break;
    }
    return ProntoTabOptionFranchise;
}

- (NSString*)getCurrentActiveTabOptionIDAsString
{
    NSInteger currentTabIndex = [ZMUserActions sharedInstance].currentSelectedTabIndex;
    switch (currentTabIndex)
    {
        case 1:
            return self.ProntoTabOptionCompetitorID;
            break;
        case 2:
            return self.ProntoTabOptionToolsAndTrainingID;
            break;
        case 3:
            return self.ProntoTabOptionEventID;
            break;
            
        default:
            return self.ProntoTabOptionFranchiseID;
            break;
    }
    return self.ProntoTabOptionFranchiseID;
}


+ (NSString*)GetAllAssetTagName
{
    return [NSString stringWithFormat:@"All %@ Assets",[self GetCurrentActiveTabOptionAsString]];
}
+ (NSString*)GetFavoritesAssetTagName
{
    return [NSString stringWithFormat:@"My %@ Favorites",[self GetCurrentActiveTabOptionAsString]];
}

+ (int)GetPointOnOrientation
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    int point = 700;
    if(screenWidth == iPadPro12_width) {
        point = 875;
    } else if(screenWidth == iPadProGlobal) {
        point = 590;
    }
    return point;
}

+ (void)removeFile:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success)
    {
        [AbbvieLogging logInfo:@"Assets.db removed succesfully"];
    }
    else
    {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Could not delete file -:%@ ",[error localizedDescription]]];
    }
}

+ (void)removeAllAssetsFromDB
{
    //synchronization is required
    @synchronized(self)
    {
        // Everything between the braces is protected by the @synchronized directive.
        NSFetchRequest * fetchrequest = [Asset MR_requestAll];
        NSArray *importedCategories = [Asset MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedCategories];
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset count ->  before deletion %lu",(unsigned long)importedCategories.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block Asset *tempAsset = nil;
            
            [temp enumerateObjectsUsingBlock:^(Asset * asset, NSUInteger idx, BOOL * _Nonnull stop) {
                tempAsset = importedCategories[idx];
                [tempAsset MR_deleteEntityInContext:localContext];
            }];
            [localContext MR_saveToPersistentStoreAndWait];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
            
            NSFetchRequest * fetchrequest = [Asset MR_requestAll];
            NSArray *importedCategories = [Asset MR_executeFetchRequest:fetchrequest];

            if (importedCategories.count > 0)
            {
                [Constant removeAllAssetsFromDB];
            }
        }];
    }
    
}

+ (void)removeAssetsFromDBWithCompletion:(void(^)(void))complete
{
    @synchronized(self)
    {
        // Everything between the braces is protected by the @synchronized directive.
        NSFetchRequest * fetchrequest = [Asset MR_requestAll];
        NSArray *importedCategories = [Asset MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedCategories];
        
        // NSLog(@"Asset count ->  before deletion %lu",(unsigned long)importedCategories.count);
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block Asset *tempAsset = nil;
            
            [temp enumerateObjectsUsingBlock:^(Asset * asset, NSUInteger idx, BOOL * _Nonnull stop) {
                tempAsset = importedCategories[idx];
                [tempAsset MR_deleteEntityInContext:localContext];
            }];
            [localContext MR_saveToPersistentStoreAndWait];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
            
            NSFetchRequest * fetchrequest = [Asset MR_requestAll];
            NSArray *importedCategories = [Asset MR_executeFetchRequest:fetchrequest];
            
            if (importedCategories.count > 0)
            {
                [Constant removeAllAssetsFromDB];
            }
            if (complete != nil)
            {
                complete();
            }
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"New changes Assets after deletion %ld", (unsigned long)importedCategories.count]];
            
        }];
    }
}

+ (void)removeAllCategoriesFromDB
{
    @synchronized(self)
    {
        NSFetchRequest * fetchrequest = [Categories MR_requestAll];
        NSArray *importedCategories = [Categories MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedCategories];
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Categories count ->  before deletion %lu",(unsigned long)importedCategories.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block Categories *tempCategories = nil;
            
            [temp enumerateObjectsUsingBlock:^(Asset * asset, NSUInteger idx, BOOL * _Nonnull stop) {
                tempCategories = importedCategories[idx];
                [tempCategories MR_deleteEntityInContext:localContext];
            }];
            [localContext MR_saveToPersistentStoreAndWait];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
            if (error) {
            }else{
                NSFetchRequest * fetchrequest = [Categories MR_requestAll];
                NSArray *importedCategories1 = [Categories MR_executeFetchRequest:fetchrequest];
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Categories count ->  after deletion %lu",(unsigned long)importedCategories1.count]];
                // if categorites is not cleaned up properly
                if (importedCategories1.count > 0)
                {
                    [Constant removeAllCategoriesFromDB];
                }
            }
        }];
    }
}

+ (void)removeAllOfflineFromDB
{
    @synchronized(self)
    {
        // Put time on start and end
        NSFetchRequest * fetchrequest = [OfflineDownload MR_requestAll];
        NSArray *importedCategories = [OfflineDownload MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedCategories];
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"OfflineDownload count ->  before deletion %lu",(unsigned long)importedCategories.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block OfflineDownload *tempCategories = nil;
            
            [temp enumerateObjectsUsingBlock:^(OfflineDownload * asset, NSUInteger idx, BOOL * _Nonnull stop) {
                tempCategories = importedCategories[idx];
                [tempCategories MR_deleteEntityInContext:localContext];
            }];
            [localContext MR_saveToPersistentStoreAndWait];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
            NSFetchRequest * fetchrequest = [OfflineDownload MR_requestAll];
            NSArray *importedCategories = [OfflineDownload MR_executeFetchRequest:fetchrequest];
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"new changesCategories count ->  after deletion %lu",(unsigned long)importedCategories.count]];
            if (importedCategories.count > 0)
            {
                [Constant removeAllOfflineFromDB];
            }
        }];
        
    }
    
}

+ (void)removeAllFranchiseFromDB
{
    @synchronized(self)
    {
        // Put time on start and end
        NSFetchRequest * fetchrequest = [Franchise MR_requestAll];
        NSArray *importedCategories = [Franchise MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedCategories];

        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Franchise count ->  before deletion %lu",(unsigned long)importedCategories.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block Franchise *tempCategories = nil;
            
            [temp enumerateObjectsUsingBlock:^(Franchise * asset, NSUInteger idx, BOOL * _Nonnull stop) {
                tempCategories = importedCategories[idx];
                [tempCategories MR_deleteEntityInContext:localContext];
                [localContext MR_saveToPersistentStoreAndWait];
            }];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
        }];
        
    }
}

+ (void)removeAllFavFranchiseFromDB
{
    @synchronized(self)
    {
        // Put time on start and end
        NSFetchRequest * fetchrequest = [FavFranchise MR_requestAll];
        NSArray *importedCategories = [FavFranchise MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedCategories];

        [AbbvieLogging logInfo:[NSString stringWithFormat:@"FavFranchise count ->  before deletion %lu",(unsigned long)importedCategories.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block FavFranchise *tempCategories = nil;
            
            [temp enumerateObjectsUsingBlock:^(FavFranchise * asset, NSUInteger idx, BOOL * _Nonnull stop) {
                tempCategories = importedCategories[idx];
                [tempCategories MR_deleteEntityInContext:localContext];
                [localContext MR_saveToPersistentStoreAndWait];
            }];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
        }];
        
    }
    
}

+ (void)removeAllBrandFromDB
{
    @synchronized(self)
    {
        // Put time on start and end
        NSFetchRequest * fetchrequest = [Brand MR_requestAll];
        NSArray *importedCategories = [Brand MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedCategories];

        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Brand count ->  before deletion %lu",(unsigned long)importedCategories.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block Brand *tempCategories = nil;
            
            [temp enumerateObjectsUsingBlock:^(Brand * asset, NSUInteger idx, BOOL * _Nonnull stop) {
                tempCategories = importedCategories[idx];
                [tempCategories MR_deleteEntityInContext:localContext];
                [localContext MR_saveToPersistentStoreAndWait];
            }];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
        }];
        
    }
    
}

+ (void)removeAllSavedPreferencesFromDB
{
    @synchronized(self)
    {
        // Put time on start and end
        NSFetchRequest * fetchrequest = [Preferences MR_requestAll];
        NSArray *importedPreferences = [Preferences MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedPreferences];

        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Preferences count ->  before deletion %lu",(unsigned long)importedPreferences.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block Preferences *tempPreferences = nil;
            [temp enumerateObjectsUsingBlock:^(Preferences * preferences, NSUInteger idx, BOOL * _Nonnull stop) {
                tempPreferences = importedPreferences[idx];
                [tempPreferences MR_deleteEntityInContext:localContext];
                [localContext MR_saveToPersistentStoreAndWait];
            }];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
        }];
    }
}

#pragma mark -
#pragma mark - Events related table

+ (void)removeAllEventOnlyAssetFromDB
{
    @synchronized(self)
    {
        // Put time on start and end
        NSFetchRequest * fetchrequest = [EventOnlyAsset MR_requestAll];
        NSArray *importedEOA = [EventOnlyAsset MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedEOA];
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"EventOnlyAsset count ->  before deletion %lu",(unsigned long)importedEOA.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block EventOnlyAsset *tempEventOnlyAsset = nil;
            [temp enumerateObjectsUsingBlock:^(EventOnlyAsset * eoa, NSUInteger idx, BOOL * _Nonnull stop) {
                tempEventOnlyAsset = importedEOA[idx];
                [tempEventOnlyAsset MR_deleteEntityInContext:localContext];
            }];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
            
        }];
    }
}

+ (void)removeAllSessionsFromDB
{
    @synchronized(self)
    {
        // Put time on start and end
        NSFetchRequest * fetchrequest = [Session MR_requestAll];
        NSArray *importedSession = [Session MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedSession];
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"EventOnlyAsset count ->  before deletion %lu",(unsigned long)importedSession.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block Session *tempSession = nil;
            [temp enumerateObjectsUsingBlock:^(Session * eoa, NSUInteger idx, BOOL * _Nonnull stop) {
                tempSession = importedSession[idx];
                [tempSession MR_deleteEntityInContext:localContext];
            }];
            
        } completion:^(BOOL contextDidSave, NSError *error) {
        }];
    }
}

+ (void)removeAllEventsFromDB
{
    //synchronization is required
    @synchronized(self)
    {
        // Everything between the braces is protected by the @synchronized directive.
        NSFetchRequest * fetchrequest = [Event MR_requestAll];
        NSArray *importedEvents = [Event MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedEvents];
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Event count ->  before deletion %lu",(unsigned long)importedEvents.count]];
        
        [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
            
            __block Event *tempEvent = nil;
            
            [temp enumerateObjectsUsingBlock:^(Event * event, NSUInteger idx, BOOL * _Nonnull stop) {
                tempEvent = importedEvents[idx];
                //Removing thumbnail image
                [[EventCoreDataManager sharedInstance] removeThumnailImageForEvent:event];
                //Removing priorities
                tempEvent.priorities = nil;
                tempEvent.eventDurations = nil;
                //Finally removing event.
                [tempEvent MR_deleteEntityInContext:localContext];
            }];
        }];
    }
}

+ (void)removeAllEventsDurationFromDB

{
    //synchronization is required
    @synchronized(self)
    {
        // Everything between the braces is protected by the @synchronized directive.
        NSFetchRequest * fetchrequest = [EventDuration MR_requestAll];
        NSArray *importedEvents = [EventDuration MR_executeFetchRequest:fetchrequest];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:importedEvents];
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"EventDuration count ->  before deletion %lu",(unsigned long)importedEvents.count]];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            __block EventDuration *tempEventDuration = nil;
            
            [temp enumerateObjectsUsingBlock:^( EventDuration *eventDurationObj, NSUInteger idx, BOOL * _Nonnull stop) {
                tempEventDuration = importedEvents[idx];
                
                //Finally removing event.
                [tempEventDuration MR_deleteEntityInContext:localContext];
            }];
        } completion:^(BOOL contextDidSave, NSError *error) {
            
            NSFetchRequest * fetchrequest = [EventDuration MR_requestAll];
            NSArray *importedEvent = [EventDuration MR_executeFetchRequest:fetchrequest];
            
            if (importedEvent.count > 0)
            {
                [Constant removeAllEventsDurationFromDB];
            }
        }];
    }
}


+ (NSArray*)getStringsValuesSeperatedByUnderscoreFromString:(NSString*)theString
{
    return  [theString componentsSeparatedByString:@"_"];
}

+ (BOOL)doesContainValue:(NSString*)value inArray:(NSArray*)arrayValue
{
   // BOOL doesContainValue = [arrayValue containsObject:value]; keeping it for testing purpose
    return [arrayValue containsObject:value];
}

+ (BOOL)doesContainSubstring:(NSString*)substring inString:(NSString*)string
{
    return [string containsString:substring];
}

/**
    It returns the other bucket name if belongs to other bucket along with current bucket
    Otherwise if it belongs to current buckter then it will return nil.
 */
+ (NSString*)getModifiedBucketNameFromBucket:(NSString*)bucketName byRemovingBucketName:(NSString*)bucketNameToBeRemoved
{
    NSArray *bucketNames = [Constant getStringsValuesSeperatedByUnderscoreFromString:bucketName];
    NSMutableArray *tempArry = [NSMutableArray arrayWithArray:bucketNames];
    [tempArry removeObject:bucketNameToBeRemoved];
    
    if (tempArry.count > 0)
    {
        NSString *tempString = nil;
        for (NSString *otherBucketName in tempArry)
        {
            if (tempString == nil)
            {
                tempString = otherBucketName;
            }
            else
            {
                tempString = [NSString stringWithFormat:@"%@_%@",tempString,otherBucketName];
            }
        }
        
        return tempString;
    }
    return nil;
}

+ (NSString*)getModifiedBucketNameWithBucket:(NSString*)bucketName
                          byAddingBucketName:(NSString*)bucketNameToBeAdded
{
    NSArray *bucketNames = [Constant getStringsValuesSeperatedByUnderscoreFromString:bucketName];
    NSMutableArray *tempArry = [NSMutableArray arrayWithArray:bucketNames];
    [tempArry addObject:bucketNameToBeAdded];
    if (bucketNames.count > 0)
    {
        NSString *tempString = nil;
        for (NSString *otherBucketName in tempArry)
        {
            if (tempString == nil)
            {
                tempString = otherBucketName;
            }
            else
            {
                tempString = [NSString stringWithFormat:@"%@_%@",tempString,otherBucketName];
            }
        }
        return tempString;
    }
    
    return bucketNameToBeAdded;
}

@end
