//
//  Constant.h
//  Pronto
//
//  Created by m-666346 on 21/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Asset;

#define iPadPro10_width 834.0
#define iPadProGlobal 768.0
#define iPadPro12_width 1024.0

#pragma mark - NSUserDefaults Keys
FOUNDATION_EXPORT NSString *const keyUserDefaultGridFranchise;

#pragma mark - Block declaration
//returnType (^blockName)(parameterTypes) = ^returnType(parameters) {...};
typedef  void (^completionHandler)();
typedef void(^OperationStatusCompletion)(BOOL success);

#pragma mark - Enums
/**
 *  Enumerates the supported file types
 */
enum ZMProntoFileTypes
{
    ZMProntoFileTypesPDF = 0,
    ZMProntoFileTypesDocument = 1,
    ZMProntoFileTypesVideo = 2,
    ZMProntoFileTypesEpub = 3,
    ZMProntoFileTypesBrightcove = 4,
    ZMProntoFileTypesWeblink = 5,
    ZMProntoFileTypesAudio = 6// PRONTO-25 - Web View - Ability to provide a Web link as an asset type.
};

typedef NS_ENUM(NSUInteger, SelectedTab)
{
    SelectedTabFranchise = 0,
    SelectedTabCompetitors,
    SelectedTabToolsTraining,
    SelectedTabEvents
};

typedef NS_ENUM(NSUInteger, ProntoTabBarItem)
{
    ProntoTabBarItemFranchise = 0,
    ProntoTabBarItemCompetitors,
    ProntoTabBarItemToolsTraining,
    ProntoTabBarItemEvents
};

typedef NS_ENUM(NSUInteger, GlobalSearchSection)
{
    MyFranchise,
    MyCompetitors,
    MyToolsTraining,
    MyEvents
};

typedef NS_ENUM(NSUInteger, AssetDownloadStatus)
{
    AssetDownloadStatusStarted = 0,
    AssetDownloadStatusCompleted,
    AssetDownloadStatusInProgress,
    AssetDownloadStatusFailed,
    AssetDownloadStatusNone
};
//[Name of associated class] + [Did | Will] + [UniquePartOfName] + Notification
//Ref. to how to decalre notification name
FOUNDATION_EXPORT NSString *const kPreferenceListVCWillShowNotification;
FOUNDATION_EXPORT NSString *const kPreferenceListVCDidSavePreferencesNotification;
FOUNDATION_EXPORT NSString *const kPreferenceListVCDismissNotification;
FOUNDATION_EXPORT NSString *const kZMAssetViewControllerGoBackNotification;
FOUNDATION_EXPORT NSString *const kZMPDFDocumentDidCloseButtonPressedNotification;
FOUNDATION_EXPORT NSString *const kZMSortMenuDidSelectRowNotification;
FOUNDATION_EXPORT NSString *const kAssetDownloadProviderCompletionNotification;
FOUNDATION_EXPORT NSString *const kEventOnlyAssetDownloadProviderCompletionNotification;
FOUNDATION_EXPORT NSString *const kAssetV2CompletionNotification;
FOUNDATION_EXPORT NSString *const kEventDeletedFromCMSNotification;
FOUNDATION_EXPORT NSString *const kOpenDeeplinkNotification;




FOUNDATION_EXPORT NSString *const kSignOutOnLaunchNotification;
FOUNDATION_EXPORT NSString *const keyCompleted;
FOUNDATION_EXPORT NSString *const keyFailed;
FOUNDATION_EXPORT NSString *const keyIn_progress;
FOUNDATION_EXPORT NSString *const keyCompleted_partialy;

#pragma mark - Tabs Releated String constant
FOUNDATION_EXPORT NSString *const ProntoTabOptionFranchise;
FOUNDATION_EXPORT NSString *const ProntoTabOptionCompetitor;
FOUNDATION_EXPORT NSString *const ProntoTabOptionToolsAndTraining;
FOUNDATION_EXPORT NSString *const ProntoTabOptionEvent;

FOUNDATION_EXPORT NSString *const ProntoTabOptionToolsAndTrainingAllAssets;
FOUNDATION_EXPORT NSString *const ProntoTabOptionToolsAndTrainingFavoriteAssets;

FOUNDATION_EXPORT NSString *const ProntoTabOptionCompetitorsAllAssets;
FOUNDATION_EXPORT NSString *const ProntoTabOptionCompetitorsFavoriteAssets;

FOUNDATION_EXPORT NSString *const ZMSortMenuSelectedCellLabelValue;

//Global Search
FOUNDATION_EXPORT NSString *const keyMyFranchise;
FOUNDATION_EXPORT NSString *const keyMyCompetitors;
FOUNDATION_EXPORT NSString *const keyMyToolsTraing;
FOUNDATION_EXPORT NSString *const keyMyEvents;

FOUNDATION_EXPORT NSString *const ProntoMessageGettingAssets;

FOUNDATION_EXPORT NSString *const ProntoMessageGettingEvents;

FOUNDATION_EXPORT NSString *const ProntoMessageGettingEventDetails;

FOUNDATION_EXPORT NSString *const ProntoMessageGettingSearchResults;

FOUNDATION_EXPORT NSString *const ProntoMessagePreparingDataToPrint;

FOUNDATION_EXPORT NSString *const ProntoEventsCoverflowImageFolderName;

FOUNDATION_EXPORT NSString *const ProntoMessageGettingAllEvents;

FOUNDATION_EXPORT NSString *const ProntoMessageAssetNotFound;

FOUNDATION_EXPORT NSString *const keySearchString;

FOUNDATION_EXPORT NSString *const ProntoTabOptionEventIDValue;

FOUNDATION_EXPORT NSString *const ProntoMessageDownloadingAsset;

FOUNDATION_EXPORT NSString *const keyAssetString;
FOUNDATION_EXPORT NSString *const keyMainSync;

FOUNDATION_EXPORT NSString *const PreviousAssetEmptyString;

FOUNDATION_EXPORT NSString *const kAssetAssociated;
FOUNDATION_EXPORT NSString *const kAssetsAssociated;

//For Print
FOUNDATION_EXPORT NSString *const PrintConstantLocation;
FOUNDATION_EXPORT NSString *const PrintConstantDescription;

FOUNDATION_EXPORT NSString *const EventHasBeenDeletedMessage;
FOUNDATION_EXPORT NSString *const AlertOKTitle;

FOUNDATION_EXPORT NSString *const kAOFLoginSuccessNotification;
FOUNDATION_EXPORT NSString *const kAOFLogoutSuccessNotification;
FOUNDATION_EXPORT NSString *const kNotLoggedIn;
FOUNDATION_EXPORT NSString *const kUserTypeHO;
FOUNDATION_EXPORT NSString *const kUserTypeFU;

FOUNDATION_EXPORT NSString *const kAOFAccessTokenName;
FOUNDATION_EXPORT NSString *const dlServerHostName;

FOUNDATION_EXPORT NSString *const kSecureValueToEncrypt;
FOUNDATION_EXPORT NSString *const keyToEncrypt;

FOUNDATION_EXPORT NSString *const appCenterConstStage;
FOUNDATION_EXPORT NSString *const appCenterConstPreProd;
FOUNDATION_EXPORT NSString *const appCenterConstProd;

FOUNDATION_EXPORT NSString *const kMultipleFranchiseForHomeOfficeMessage;

//Encryption and login Keys
FOUNDATION_EXPORT NSString *const kUsername;
FOUNDATION_EXPORT NSString *const kPassword;
FOUNDATION_EXPORT NSString *const kRNEncryptionKey;
FOUNDATION_EXPORT NSString *const kRNEncryptionKeyForStage;
FOUNDATION_EXPORT NSString *const kRNEncryptionKeyForProduction;
FOUNDATION_EXPORT NSString *const kRNNewEncryptionKey;
FOUNDATION_EXPORT NSString *const kNewUsername;
//For Service calls
FOUNDATION_EXPORT NSString *const kTokenKey;

@interface Constant : NSObject

//Bucket ID
@property (nonatomic, strong) NSString *ProntoTabOptionFranchiseID;
@property (nonatomic, strong) NSString *ProntoTabOptionToolsAndTrainingID;
@property (nonatomic, strong) NSString *ProntoTabOptionCompetitorID;

/**
 As Confirmed by the CMS/Testing Team/Product Owner that EventId will not come from
 CMS. And it's needs to be hardcoded from app side.
 Providing it 99999 as value.
 */
@property (nonatomic, strong) NSString *ProntoTabOptionEventID;


- (NSString*)getCurrentActiveTabOptionIDAsString;
- (NSString*)getDefaultBucketID;

+ (Constant*)sharedConstant;



+ (NSString*)GetCurrentActiveTabOptionAsString;
+ (NSString*)GetAllAssetTagName;
+ (NSString*)GetFavoritesAssetTagName;
+ (int)GetPointOnOrientation;
+ (enum ZMProntoFileTypes)GetFileTypeForAsset:(Asset *)selectedAsset;

//Database Helper Method
+ (void)removeFile:(NSString *)filename;
+ (void)removeAllAssetsFromDB;
+ (void)removeAllCategoriesFromDB;
+ (void)removeAllOfflineFromDB;
+ (void)removeAllFranchiseFromDB;
+ (void)removeAllFavFranchiseFromDB;
+ (void)removeAllBrandFromDB;
+ (void)removeAllSavedPreferencesFromDB;
+ (void)removeAllEventsFromDB;
+ (void)removeAllEventsDurationFromDB;
+ (void)removeAllEventOnlyAssetFromDB;
+ (void)removeAllSessionsFromDB;
+ (void)removeAssetsFromDBWithCompletion:(void(^)(void))complete;

// Move below method to app utilities class after merge
+ (NSArray*)getStringsValuesSeperatedByUnderscoreFromString:(NSString*)theString;
+ (BOOL)doesContainValue:(NSString*)value inArray:(NSArray*)arrayValue;
+ (BOOL)doesContainSubstring:(NSString*)substring inString:(NSString*)string;
+ (NSString*)getModifiedBucketNameFromBucket:(NSString*)bucketName
                        byRemovingBucketName:(NSString*)bucketNameToBeRemoved;
+ (NSString*)getModifiedBucketNameWithBucket:(NSString*)bucketName
                        byAddingBucketName:(NSString*)bucketNameToBeAdded;

@end
