//
//  ZMTracking.m
//  Pronto
//
//  Created by Andres Ramirez on 11/29/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import "ZMTracking.h"
//#import "ADBMobile.h"
#import <sys/sysctl.h>
#import "Pronto-Swift.h"

@import BrightcovePlayerSDK;
@import BrightcovePlayerUI;

@interface NetworkMonitor : NSObject
- (int) currentNetworkType;
@end


@implementation ZMTracking

+ (void)configure{
    
    NSString *bundleIdentifier = [[NSBundle bundleForClass:[self class]] pathForResource:@"ADBMobileConfig" ofType:@"json"];
//    [ADBMobile overrideConfigPath:bundleIdentifier];
//    [ADBMobile setDebugLogging:YES];
}

#pragma mark - Rechability


# pragma mark - Utils

+ (void)trackCustomEvents:(NSString *)events {

	NSMutableDictionary *contextData = [NSMutableDictionary dictionary];
	[contextData setObject:@"value" forKey:@"contextKey"];

//    [ADBMobile trackAction:events data:contextData];
}

+ (void)trackCustomAppState:(NSString *)appState {
	NSMutableDictionary *contextData = [NSMutableDictionary dictionary];
	[contextData setObject:@"value" forKey:@"contextKey"];

//    [ADBMobile trackState:appState data:contextData];
}


+ (void) addEvarVideoName:(NSString *)name
{
    //ADMS_Measurement *measurement = [ADMS_Measurement sharedInstance];
    //[measurement setEvar:65 toValue:name];
}


+ (NSString *) platformString {
    // Gets a string with the device model
    size_t size;
    sysctlbyname("hw.machine", nil, &size, nil, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    return platform;
}

+ (void)trackSection:(NSString *)section withSubsection:(NSString *)subsection withName:(NSString *)name withOptions:(NSMutableDictionary *)options{
    //ADMS_Measurement *measurement = [ADMS_Measurement sharedInstance];
    NSMutableDictionary *contextData = [NSMutableDictionary dictionary];
    NSMutableString *string = [NSMutableString stringWithString:@"us|pronto"];
   
    //section | subsection | screen level content
    if(section){
        [string appendString:@"|"];
        [string appendString:section];
        
    }
    if(subsection){
        [string appendString:@"|"];
        [string appendString:subsection];
        
    }
    
    
    if(name){
        [string appendString:@"|"];
        [string appendString:name];
    }
    
    [contextData setObject:string forKey:@"scscreen_name"];
    [contextData setObject:@"pronto" forKey:@"scsiteIndex"];
    
    
    if(section){
        [contextData setObject:section forKey:@"scsiteSection"];
    }
    if(subsection){
        [contextData setObject:subsection forKey:@"scsubSection"]; // ignore this variable if your screen does not have Sub Section
    }
    if(name){
        [contextData setObject:name forKey:@"scsubSection2"]; // ignore this variable if your screen does not have Sub Section2
    }
        
    /*!
     * New network type implementation
     */
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        __block  BOOL hasConnection = false;
        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            hasConnection = [mainDelegate hasConnectivity];
        });
        
        NSString *connectionStatus = @"Offline";
        if(hasConnection)
        {
            connectionStatus = @"Online";
        }

        [options setObject:[NSString stringWithFormat:@"%@:%@:%@", [self platformString], connectionStatus, [UIDevice currentDevice].systemVersion] forKey:@"schardwareos"];
    }
    
    NSString *userBranchValue = @"Not Logged In";
    if ([AOFLoginManager sharedInstance].checkForAOFLogin){
        // PRONTO-22 - Upgrading ASF framework 2.2.1
        //NSString *userSpace = [[ASFSession sharedSession] getSpaceStructureID];
        NSString *userSpace = [AOFLoginManager sharedInstance].spaceStructureID;
        userBranchValue = [ZMTracking userBranchSelection:userSpace];
    }

    [contextData setObject:userBranchValue forKey:@"scusertype"];
    
    
    for (NSString* key in options) {
        NSString *value = [options objectForKey:key];
        [contextData setObject:value forKey:key];
    }
}

+ (NSString *) userBranchSelection:(NSString *)userSpace{
    //    ASFProfile *profile = [ASFSession sharedSession].authorization.profile;
    NSString *userBranchValue;
    userBranchValue = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",
                       [self stringForTrack:[AOFLoginManager sharedInstance].userType],
                       [self stringForTrack:[AOFLoginManager sharedInstance].locationCode],
                       [self stringForTrack:[AOFLoginManager sharedInstance].location],
                       [self stringForTrack:[AOFLoginManager sharedInstance].ledgerdepartment],
                       [self stringForTrack:[AOFLoginManager sharedInstance].departmentcode],
                       [self stringForTrack:[AOFLoginManager sharedInstance].department],
                       [self stringForTrack:[AOFLoginManager sharedInstance].divisionCode],
                       [self stringForTrack:[AOFLoginManager sharedInstance].divisionName],
                       [self stringForTrack:[AOFLoginManager sharedInstance].spaceStructureID],
                       [self stringForTrack:[AOFLoginManager sharedInstance].spacePropertyID],
                       [self stringForTrack:[AOFLoginManager sharedInstance].spaceLocationID],
                       @"no-value",
                       @"no-value",
                       @"no-value",
                       [self stringForTrack:[AOFLoginManager sharedInstance].title]];
    
    if ([AOFLoginManager sharedInstance].salesFranchiseID.length > 0 && [AOFLoginManager sharedInstance].salesFranchise.length > 0) {
        userBranchValue = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",
                           [self stringForTrack:[AOFLoginManager sharedInstance].userType],
                           [self stringForTrack:[AOFLoginManager sharedInstance].locationCode],
                           [self stringForTrack:[AOFLoginManager sharedInstance].location],
                           [self stringForTrack:[AOFLoginManager sharedInstance].ledgerdepartment],
                           [self stringForTrack:[AOFLoginManager sharedInstance].departmentcode],
                           [self stringForTrack:[AOFLoginManager sharedInstance].department],
                           [self stringForTrack:[AOFLoginManager sharedInstance].salesFranchiseID],
                           [self stringForTrack:[AOFLoginManager sharedInstance].salesFranchise],
                           [self stringForTrack:[AOFLoginManager sharedInstance].salesSalesForce],
                           [self stringForTrack:[AOFLoginManager sharedInstance].salesArea],
                           [self stringForTrack:[AOFLoginManager sharedInstance].salesRegion],
                           [self stringForTrack:[AOFLoginManager sharedInstance].salesDistrict],
                           [self stringForTrack:[AOFLoginManager sharedInstance].territory],
                           [self stringForTrack:[AOFLoginManager sharedInstance].salesCategoryID],
                           [self stringForTrack:[AOFLoginManager sharedInstance].title]];
        
    }
    return userBranchValue;
}

+ (NSString *) stringForTrack:(NSString *) string {
    
    if (string.length > 0) {
        
        NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        return [trimmedString lowercaseString];
    }
    
    return @"no-value";
}

+ (NSString *)userType{
    return [self userBranchSelection:@""];
}

+(void)settignMedia:(NSString*)name 
             length:(double)length
         playerName:(nullable NSString *)playerName
           playerID:(nullable NSString *)playerID
{

}

+(void)trackMediaEvent:(NSString*)event named:(NSString*)name offset:(double)offset{

}

@end
