//
//  ZMUserActions.m
//  Pronto
//
//  Created by Andres Ramirez on 2/14/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMUserActions.h"
#import "ZMAssetsGroup.h"
#import "ZMAssetsLibrary.h"
#import "ZMTracking.h"
#import "Pronto-Swift.h"
//Revamp(5/12)-Import Magical Record
#import <MagicalRecord/MagicalRecord.h>
#import "Filter.h"
#import "ZMNotificationCell.h"
#import "Constant.h"

//End of Revamp

@interface ZMUserActions ()
{
    NSArray *defaultFranchisesAndBrands;
}
@end

@implementation ZMUserActions


static ZMUserActions *sharedInstance = nil;


+ (ZMUserActions *)sharedInstance{
    
    if(sharedInstance == nil){
        sharedInstance = [[super allocWithZone:NULL] init];
        [sharedInstance initializeDataSources];
    }
    return sharedInstance;
}

- (void)initializeDataSources
{
    //Initialize all mutable data here
    self.categoryTools       = [NSMutableArray array];
    self.categoryCompetitors = [NSMutableArray array];
    self.brandsSelected      = [NSMutableArray array];
    self.currentSelectedTabIndex =  0;
    self.currentFilter = [[Filter alloc]init];
    self.currentSelectedCell = [[ZMNotificationCell alloc] init];
}

-(void) setSection:(NSString *)section
{
    _section = section;
}


-(void) setIsDefault:(BOOL)isDefault
{
    _isDefault = isDefault;
    defaultFranchisesAndBrands = [_franchisesAndBrands copy];
}


-(void) setFranchisesAndBrands:(NSArray *)franchisesAndBrands
{
    _franchisesAndBrands = franchisesAndBrands;
    
    // PRONTO-10 - Button Visibility not Consistent
    // check the franchise array instead of checking string
    NSCountedSet *set1 = [[NSCountedSet alloc] initWithArray:_franchisesAndBrands];
    NSCountedSet *set2 = [[NSCountedSet alloc] initWithArray:defaultFranchisesAndBrands];
  
    _isDefault = [set1 isEqualToSet:set2];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"isEqual:%d", _isDefault]];
    
}


// New Search Implementation
-(NSArray*)getResultantAssets:(NSArray*) searchedAssets
{
    
    return searchedAssets;
}


-(void) setAllFranchisesChecked:(BOOL)allFranchisesChecked
{
    _allFranchisesChecked = allFranchisesChecked;
}

//Revamp(4/12) - Inserting and Deleting into Fav Franchise
/**
 *  Save new FavouriteFranchise into FavFranchise entity
 *
 *  @param franchiseData     Franchise
 */
-(void)insertNewFavouriteFranchise:(NSDictionary*)franchiseData complete:(void(^)(void))complete error:(void(^)(NSError *error))onError {
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        FavFranchise * favFranchise = [FavFranchise MR_createEntityInContext:localContext];
        [favFranchise MR_importValuesForKeysWithObject:franchiseData];
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        
        if (error) {
            [AbbvieLogging logError:@"Favorite Insertion Error"];
        }else{
            [AbbvieLogging logError:@"Favorite Insertion Success"];
            complete();
        }
    }];
}
-(void)editFavouriteFranchise:(NSArray*)franchiseData complete:(void(^)(void))complete error:(void(^)(NSError *error))onError {
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        /* Updating Fav Assets and priorities */
        for (int count=0;count < franchiseData.count;count++) {
            
            NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
            favFranchiseObject = franchiseData[count];
            
            NSPredicate *favFilter = [NSPredicate predicateWithFormat:@"asset_ID == %@", favFranchiseObject[@"id"]];
            
            NSArray * favourites = [FavFranchise MR_findAllWithPredicate:favFilter inContext:localContext];
            if(favourites.count == 0){
               
                FavFranchise * favFranchise = [FavFranchise MR_createEntityInContext:localContext];
                [favFranchise MR_importValuesForKeysWithObject:favFranchiseObject];
            }else{
                FavFranchise * favFranchise = favourites[0];
                favFranchise.asset_ID = favFranchiseObject[@"id"];
                favFranchise.priority = favFranchiseObject[@"position"];
            }
        }
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        
        if (error) {
            [AbbvieLogging logError:@"Favorite Reordering Error"];
        }else{
            [AbbvieLogging logError:@"Favorite Reordering Success"];
            complete();
        }
    }];
}

-(void)deleteFavFranchise:(FavFranchise*)franchiseData complete:(void(^)(void))complete error:(void(^)(NSError *error))onError
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        FavFranchise * favFranchiseToDelete = franchiseData;
        [favFranchiseToDelete MR_deleteEntityInContext:localContext];
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        if (error) {
            [AbbvieLogging logError:@"Error"];
        }else{
            complete();
        }
    }];
    
}

// Make sure don't do heavy task on this
- (void) updatePersistentStoreWithCurrentContext
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

-(void)insertNewOfflineDownloadData:(NSDictionary*)offlineDownloadData withCompletionHandeler:(void(^)(void))complete
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        OfflineDownload * offlineDownload = [OfflineDownload MR_createEntityInContext:localContext];
        [offlineDownload MR_importValuesForKeysWithObject:offlineDownloadData];
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        [AbbvieLogging logInfo:@"offline download insertion completed"];
        if (complete != nil)
        {
            complete();
        }
    }];
}

-(void)deleteOfflineDownloadData:(OfflineDownload*)offlineDownloadData withCompletion:(void (^)(void))complete
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        OfflineDownload * offlineDownloadDataToDelete = offlineDownloadData;
        [offlineDownloadDataToDelete MR_deleteEntityInContext:localContext];
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        
        if (complete != nil)
        {
            [AbbvieLogging logInfo:@"offline download Deletion completed"];
            complete();
        }
    }];
}
//End of Revamp

//Gets bucket name from the asset
-(NSNumber *)getBucketID:(Asset*)asset
{
    NSNumber * bucketId = 0;
    NSSet * cats = asset.categories;
    for(Categories *cat in cats)
    {
        bucketId = cat.bucketId;
        break;
    }
    return bucketId;
}

//Removes the duplicate in assets list and updates with the asset with update available
-(NSMutableArray *)removeDuplicatesForAssets:(NSMutableArray *)orginalAssetsArray
{
    NSMutableArray *assetToBeRemoved = [NSMutableArray array];
    NSMutableArray *alreadyCheckedIds = [NSMutableArray array];
    NSMutableArray *duplicatesOfOrginalAssetsArray = [orginalAssetsArray mutableCopy];
    
    for (Asset *asset in duplicatesOfOrginalAssetsArray)
    {
        if ([alreadyCheckedIds containsObject:asset.assetID])
        {
            // If already Asset id has been checked, need to check it again.
            continue;
        }
        NSPredicate *predicateForID = [NSPredicate predicateWithFormat:@"assetID == %ld", asset.assetID.integerValue];
        NSMutableArray *filteredArray = [[orginalAssetsArray filteredArrayUsingPredicate:predicateForID] mutableCopy];
        [filteredArray sortUsingComparator:^NSComparisonResult(Asset *obj1, Asset *obj2) {
            
            if (obj1.isUpdateAvailable > obj2.isUpdateAvailable) {
                return NSOrderedAscending;
            }
            return NSOrderedDescending;
        }];
        
        if (filteredArray.count > 1) // It means duplicate exist
        {
            //if there is mix of isUpdateAvailable == true and isUpdateAvailable == false
            //then isUpdateAvailable == true will be first. otherwise take blindly firstone
            Asset *assetNotBeRemoved = filteredArray.firstObject;
            [filteredArray removeObject:assetNotBeRemoved];
            [assetToBeRemoved addObjectsFromArray:filteredArray];
        }
        if (asset.assetID != nil)
        {
            [alreadyCheckedIds addObject:asset.assetID];
        }
    }
    
    [orginalAssetsArray removeObjectsInArray:assetToBeRemoved];
    return orginalAssetsArray;
}

- (void)setSorts:(NSMutableArray *)sorts {
    
    if (!_defaultSortSelection && !_sorts) {
        _defaultSortSelection = [sorts copy];
    }
    _sorts = sorts;
    //This validates if the sort is the default or not
    if (![_sorts isEqual:_defaultSortSelection]) {
        _isDefaultSort = NO;
    }
    else {
        _isDefaultSort = YES;
    }
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject:@"" forKey:@"scsortaction"];
    [trackingOptions setObject:@"" forKey:@"scsorttype"];
    [trackingOptions setObject:@"" forKey:@"scsortorder"];
    [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    /**
     * @Tracking
     * Sorting
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"sortcolumn" withName:nil withOptions:trackingOptions];
}


- (NSString *)getFiltersForTracking {
    
    NSString *trackFilters = @"filter|";
    
    for(NSDictionary *franchise in [ZMProntoManager sharedInstance].franchisesToUpdateHeader){
        
        trackFilters = [trackFilters stringByAppendingFormat:@"%@:", [franchise objectForKey:@"name"]];
        
        //Brands
        for(NSDictionary *brand in [franchise objectForKey:@"brands"]){

            if ([[brand objectForKey:@"selected"] intValue] == 1) {
                
                trackFilters = [trackFilters stringByAppendingFormat:@"%@,", [brand objectForKey:@"name"]];
            }
        }
        
        trackFilters = [trackFilters substringToIndex:[trackFilters length]-1]; //remove the last coma
        trackFilters = [trackFilters stringByAppendingString:@"::"]; //In case there are more franchises
    }
    
    trackFilters = [trackFilters substringToIndex:[trackFilters length]-2]; //remove the last 2 semicolons
        
    return trackFilters;
}

- (void)checkUnCheckAllUserTracking:(BOOL)isAllFranchiseSelected {
    
    if (!self.allFranchisesChecked) {

        /**
         * @Tracking
         * Uncheck
         **/
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        [trackingOptions setObject:@"filter|franchise:brand" forKey:@"scfiltercategory"];
        [trackingOptions setObject:[self getFiltersForTracking] forKey:@"scfiltersubcategory"];
        [trackingOptions setObject:@"reset" forKey:@"scfilteraction"];
        [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
        [trackingOptions setObject:[NSString stringWithFormat:@"%d", _assetsCount] forKey:@"scfilterresultcount"];
        [trackingOptions setObject:(_assetsCount >0)?@"successful":@"unsuccessful" forKey:@"scsearchaction"];
        [trackingOptions setObject:/*_search*/[[ZMProntoManager sharedInstance].search getTitle]forKey:@"scsearchterm"];
        [trackingOptions setObject:@"recently added" forKey:@"scsortaction"];
        [trackingOptions setObject:@"all" forKey:@"scsortcategory"];
        [trackingOptions setObject:@"single" forKey:@"scsorttype"];
        
        [ZMTracking trackSection:@"library"
                  withSubsection:@"filter"
                        withName:[NSString stringWithFormat:@"franchise-%@-uncheckall", _franchiseName]
                     withOptions:trackingOptions];
    } else {
        /**
         * @Tracking
         * Check all
         **/
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        [trackingOptions setObject:@"filter|franchise:brand" forKey:@"scfiltercategory"];
        [trackingOptions setObject:[self getFiltersForTracking] forKey:@"scfiltersubcategory"];
        [trackingOptions setObject:@"set" forKey:@"scfilteraction"];
        [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
        [trackingOptions setObject:[NSString stringWithFormat:@"%d", _assetsCount] forKey:@"scfilterresultcount"];
        [trackingOptions setObject:(_assetsCount >0)?@"successful":@"unsuccessful" forKey:@"scsearchaction"];
        [trackingOptions setObject:/*_search*/[[ZMProntoManager sharedInstance].search getTitle] forKey:@"scsearchterm"];
        [trackingOptions setObject:@"recently added" forKey:@"scsortaction"];
        [trackingOptions setObject:@"all" forKey:@"scsortcategory"];
        [trackingOptions setObject:@"single" forKey:@"scsorttype"];
        
        [ZMTracking trackSection:@"library"
                  withSubsection:@"filter"
                        withName:[NSString stringWithFormat:@"franchise-%@-checkall", _franchiseName]
                     withOptions:trackingOptions];
    }
}


+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedInstance];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

-(BOOL) finalSearch
{
    _isFinalSearch = true;
    
    return _isFinalSearch;
}

- (void)setBrandsArray:(NSArray *)brandsArray
{
    NSArray *sortedArray = [brandsArray sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
        
        NSComparisonResult result = [obj1 compare:obj2 options:NSNumericSearch | NSCaseInsensitiveSearch];
        if (result == -1)
        {
            return NSOrderedAscending;
        }
        else if (result == 1)
        {
            return NSOrderedDescending;
        }
        return NSOrderedSame;
    }];
    
    _brandsArray = sortedArray;
}

- (void)resetPropertiseToDefaultValue
{
    self.shouldSwitchToOtherTab = NO;
}

-(void) resetUserActionSharedInstanceValuesOnLogout
{
    self.deepLinkUserInfo = nil;
}

@end
