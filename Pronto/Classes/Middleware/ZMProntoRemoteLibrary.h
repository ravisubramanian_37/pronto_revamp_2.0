//
//  ZMProntoRemoteLibrary.h
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13. 
//  Copyright (c) 2013 Zemoga Inc. All rights reserved.
//

#import "ZMRemoteLibraryService.h"
#import "ZMURLRequest.h"
#import <ASFKit/ASFURLRequest.h>
//#import "UIImageView+WebCache.h"

// This will define if Abbott Security Framework will be used to access restriction son this App
#define ACTIVE_SECURITY_FRAMEWORK TRUE

/**
 * This object will implement some methods of the ZMRemoteLibraryService 
 * (expecially the URL Connection Functions)
 */
@interface ZMProntoRemoteLibrary : ZMRemoteLibraryService <ASFURLRequestDelegate, NSURLConnectionDelegate >
{
    /** used on HTTP Connections with Certificate Auth */
    SecIdentityRef identity;
    /** used on HTTP Connections with Certificate Auth */
    SecTrustRef trust;
    /** used on HTTP Connections with Certificate Auth */
    NSData *PKCS12Data;
    /** bool to handle if the certificate is imported and the identity was extracted from it */
    BOOL importedCertificate;
}

-(NSDictionary *) loadUserData;

@end
