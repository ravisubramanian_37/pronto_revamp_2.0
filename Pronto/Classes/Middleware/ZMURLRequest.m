//
//  ZMURLRequest.m
//  Pronto
//
//  Created by Sebastian Romero on 5/28/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ZMURLRequest.h"

@interface ZMURLRequest()
{
    NSMutableURLRequest *request;
    NSMutableData *responseData;
    NSURLConnection *connection;
    NSMutableString *parametersString;
    NSURL *fullURL;
    NSRunLoop *loop;
    long long downloadSize;
}

@end

@implementation ZMURLRequest


- (instancetype) init{
    return nil;
}


- (instancetype) initRequestWithURL:(NSURL *)url params:(NSDictionary *)params method:(ZMURLRequestType)method headers:(NSDictionary *)headers completionHandler:(void(^)(NSString *response, NSError *error)) completionHandler
{
    if (self = [super init]) {
        fullURL = url;
        request = [NSMutableURLRequest requestWithURL:fullURL];
        /**
         *  Preparing the method parameter and headers
         */
        [self setMethod:method];
        [self setParameters:params];
        [self setHeaders:headers];
        [self setComplete:completionHandler];
        [self prepareRequest];
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              // do something with the data
                                          }];
        [dataTask resume];
//        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        /**
//         *  Catching the url and queue in order to handling the request in a different thread
//         */
//        if(![NSThread isMainThread]){
//            loop = [NSRunLoop currentRunLoop];
//            [connection scheduleInRunLoop:loop forMode:NSRunLoopCommonModes];
//            [loop run];
//        }
    }
    return self;
}



-(void) downloadFileFromURL:(NSURL *)url toFilePath:(NSString *)filepath completionHandler:(void(^)(NSString *path, NSError *error)) completionHandler
{
    fullURL = url;
    request = [NSMutableURLRequest requestWithURL:fullURL];
    _destinationPath = filepath;
    [self setComplete:completionHandler];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          // do something with the data
                                      }];
    [dataTask resume];
    

    [self start];
}



-(void) start
{
    if(connection){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [connection start];
        });
    }
}


-(void) prepareRequest
{
    /**
     *  Only prepers the request if the method is different to R4URLRequestTypeGET
     */
    if(request && _method != ZMURLRequestTypeGET){
        NSData *data = [parametersString dataUsingEncoding:NSUTF8StringEncoding];
        [request addValue:@"8bit" forHTTPHeaderField:@"Content-Transfer-Encoding"];
        [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:data];
    }
}


-(void) URLByAppendingQueryString:(NSString *)queryString {
    NSString *URLString = [[NSString alloc] initWithFormat:@"%@%@%@", [fullURL absoluteString],[fullURL query] ? @"&" : @"?", queryString];
    fullURL = [NSURL URLWithString:URLString];
}


-(void) setParameters:(NSDictionary *)parameters
{
    _parameters = parameters;
    parametersString = [[NSMutableString alloc] initWithString:@""];
    if([_parameters count] > 0){
        for(NSString* key in [_parameters allKeys]){
            [parametersString appendString:[NSString stringWithFormat:@"%@=%@&", key, [_parameters objectForKey:key]]];
        }
    }
    /**
     *  If is a get Method we should handle the request differently
     */
    if(_method == ZMURLRequestTypeGET){
        [self URLByAppendingQueryString:parametersString];
        if(request){
            request.URL = fullURL;
        }
    }
}


-(void) setHeaders:(NSDictionary *)headers
{
    _headers = headers;
    if(request){
        for(NSString *key in _headers){
            [request addValue:[_headers objectForKey:key] forHTTPHeaderField:key];
        }
    }
}


-(void) setMethod:(ZMURLRequestType)method
{
    _method = method;
    if(request){
        switch (_method) {
            case ZMURLRequestTypePOST:
                [request setHTTPMethod:@"POST"];
                break;
            case ZMURLRequestTypePUT:
                [request setHTTPMethod:@"PUT"];
                break;
            case ZMURLRequestTypeDELETE:
                [request setHTTPMethod:@"DELETE"];
                break;
            default:
                [request setHTTPMethod:@"GET"];
                break;
        }
        
    }
}


# pragma mark - NSURLConnectionDelegate


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    downloadSize = [response expectedContentLength];
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if(_complete){
        NSString *response = [NSString stringWithUTF8String:[data bytes]];
        _complete(response, nil);
    }
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if(_destinationPath){
        [responseData writeToFile:_destinationPath atomically:YES];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if(_complete) {
        _complete(nil, error);
    }
}

@end
