//
//  ZMTracking.h
//  Pronto
//
//  Created by Andres Ramirez on 11/29/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"


@interface ZMTracking : NSObject

/*
 * Configures tracking for the application
 * @param properties NSDictionary with the server and RSID for the corresponding environment
 *
 */
+(void) configure;

/**
 *  Configure global media measurement to Omniture
 */
+(void) addEvarVideoName:(NSString *_Nullable)name;

/**
 *  Track navigation inside App
 *
 *  @param section    Current section like (Library-category-MyBriefcase)
 *  @param subsection Sub-section to tracking
 *  @param name       Name to variable
 *  @param options    Adicional parameters
 */
+(void)trackSection:(NSString *_Nullable) section withSubsection:(NSString *_Nullable)subsection withName:(NSString *_Nullable)name withOptions:(NSMutableDictionary *_Nullable)options;

/**
 *  Track events into video which user has performed (Play - Close - Stop)
 *
 *  @param event Event into BrightCove framework
 *  @param name  named Event
 */
+(void)trackMediaEvent:(NSString*_Nullable)event named:(NSString*_Nullable)name offset:(double)offset;

/**
 * Helpers
 **/

/**
 * Gets the user typer requiered when the user authenticates
 **/
+ (NSString *_Nullable) userType;

+(void)settignMedia:(NSString*_Nullable)name
             length:(double)length
         playerName:(nullable NSString *)playerName
           playerID:(nullable NSString *)playerID;


@end
