//
//  ZMUserActions.h
//  Pronto
//
//  Created by Andres Ramirez on 2/14/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
//Revamp(1/12) - Import FavFranchise core data
#import "FavFranchise+CoreDataClass.h"
#import "OfflineDownload+CoreDataClass.h"
//End of Revamp
@class ZMAssetsGroup;
@class Filter;
@class GlobalSearchVC;
@class ZMNotificationCell;
@class Constant;

@interface ZMUserActions : NSObject
/**
 *  Franchise to update filters into SQLITE
 */
@property(strong, nonatomic) NSArray *franchisesAndBrands;
/**
 *  Categories to update filters into SQLITE
 */
@property(strong, nonatomic) NSArray *categories;
/**
 *  Sort to order assets
 */
@property(strong, nonatomic) NSMutableArray *sorts;
/**
 *  Sort to containt default franchise of user
 */
@property(strong, nonatomic) NSMutableArray *defaultSortSelection;
/**
 *  Handle total asset selected
 */
@property int assetsCount;
/**
 *  Handle total asset count without filter
 */
@property int totalAssetsCount;
/**
 *  Handle total franchises selected
 */
@property int totalOfFranchises;

//Revamp(1/12) - Inserting & Deleting into Fav
/**
 *  Save new FavouriteFranchise into FavFranchise entity
 *
 *  @param franchiseData     Franchise Object
 */
-(void)insertNewFavouriteFranchise:(NSDictionary*)franchiseData complete:(void(^)(void))complete error:(void(^)(NSError *error))onError;

-(void)editFavouriteFranchise:(NSArray*)franchiseData complete:(void(^)(void))complete error:(void(^)(NSError *error))onError;
/**
 *  Delete FavouriteFranchise from FavFranchise entity
 *
 *  @param franchiseData     Franchise Object
 */
-(void)deleteFavFranchise:(FavFranchise*)franchiseData complete:(void(^)(void))complete error:(void(^)(NSError *error))onError;
//End of Revamp

//Revamp(1/12) - Inserting & Deleting into Fav

//Removing duplicates from list of asset response
-(NSMutableArray *)removeDuplicatesForAssets:(NSMutableArray *)assetsArray;

//Gets bucket name from the asset
-(NSNumber *)getBucketID:(Asset*)asset;

/**
 *  Handle text searched by user
 */
@property(nonatomic) BOOL allFranchisesChecked;
@property(nonatomic,assign) BOOL isVersionUpgraded;
@property(nonatomic) BOOL isDefaultSort;
@property(nonatomic) BOOL isDefault;
@property(nonatomic) BOOL isAllCategorySelected;
@property(strong, nonatomic) NSString *section;
@property(strong, nonatomic) NSString *franchiseName;
@property(strong, nonatomic) NSString *franchiseId;
// PRONTO-88 - Backlog issue - Asset selection not highlighting the Folder added
@property(strong, nonatomic) NSDictionary *existingFolders;
@property BOOL isUserChange;
//PRONTO-37 Deeplinking to a Page in a PDF
@property (assign, nonatomic) NSInteger pageNo;
// PRONTO-31 Optimization of Deep Linking Feature
@property(nonatomic) BOOL checkURLSchema;
@property(nonatomic) BOOL isCategoriesSelected;

@property(strong, nonatomic) NSDictionary *allFolders;
// New Search Implementation
@property(strong, nonatomic) NSArray * selectedFranchise;
@property(strong, nonatomic) NSArray * searchedKeys;
@property(strong, nonatomic) NSArray * resultantAssets;
@property (nonatomic) BOOL updatedAssets;
@property (strong, nonatomic) NSString* searchingText;
@property (nonatomic) BOOL searchCompletion;
@property (nonatomic) BOOL isFirstSearch;
@property (strong, nonatomic) NSString* sortingKey;
@property (nonatomic) BOOL isKeywordSaved;
@property (nonatomic) BOOL enableSearch;
@property(strong, nonatomic) NSMutableArray *totalSearchedAssets;
@property (nonatomic) int totalPageCount ;
@property (nonatomic) BOOL isServiceHitted ;
@property (nonatomic) BOOL isServiceSuccess;
@property int searchedCount;
@property (nonatomic) BOOL isFinalSearch;
@property (strong,nonatomic) NSString* selectedSortByMenu;
@property(strong, nonatomic) NSArray* brandsArray;
// Revamp changes - franchise color for header
@property (strong,nonatomic) NSString* franchiseColor;
// check the selection
@property(strong, nonatomic) NSString * selectLeftMenu;
// New Revamp Changes - Promoted Content Offline
@property (strong,nonatomic) NSDictionary *promotedAssetsOffline;
@property(strong, nonatomic) NSMutableArray *brandsSelected;
// check successful login
@property (nonatomic) BOOL isLoginSuccess;
@property (nonatomic) BOOL isPromotedLoaded;
// orientation for toolbar
@property (strong,nonatomic) NSString* currentOrientation;
@property (nonatomic) BOOL isFieldUser;
@property (nonatomic) BOOL isTileViewTapped;
@property (nonatomic) BOOL isBookmarkTapped;
@property (nonatomic) BOOL isLoggedoutFromHelp;
@property (nonatomic) BOOL isLoggedoutFromHelpOtherTab;
@property (nonatomic, assign) BOOL shouldSwitchToOtherTab;

@property (strong,nonatomic) Filter* currentFilter;
@property (strong, nonatomic) ZMNotificationCell *currentSelectedCell;

/**Tells which tab is selected currently
 if currentSelectedTabIndex = 0 -> First tab selected.
*/
@property(nonatomic, assign) NSUInteger currentSelectedTabIndex;

// Tools Training
@property (strong,nonatomic) NSMutableArray* categoryTools;


// Competitors
@property (strong,nonatomic) NSMutableArray* categoryCompetitors;

/**
 name: shouldShowFranchiseListView
 Param:It decides that Either we need to show Preferences List view on toggling of tabs controll
*/
@property (nonatomic) BOOL shouldShowFranchiseListView;

/**
 name: shoudRefreshEventData
 Param: It decides that Either we need  refresh event local database from CMS;
 */
@property (nonatomic) BOOL shoudRefreshEventData;


@property (nonatomic) BOOL shouldShowInitialWalkthrough;


@property (nonatomic, strong) GlobalSearchVC * globalSearchViewController;

//Deeplinking
@property (nonatomic, strong) NSDictionary *deepLinkUserInfo;

+ (ZMUserActions *)sharedInstance;

-(void) checkUnCheckAllUserTracking:(BOOL)isAllFranchiseSelected;
-(NSString *) getFiltersForTracking;

-(BOOL) finalSearch;

@property int countOfAssetDict;

@property int export_infoMenuValOnOrientation;

- (void) updatePersistentStoreWithCurrentContext;

/**
 *  Save new offlineDownloadData into OfflineDownload entity
 *
 *  @param offlineDownloadData
 *  @param complete
 */
-(void)insertNewOfflineDownloadData:(NSDictionary*)offlineDownloadData withCompletionHandeler:(void(^)(void))complete;

/**
 *  Delete offlineDownloadData from OfflineDownload entity
 *
 *  @param offlineDownloadData
 *  @param complete
 */
-(void)deleteOfflineDownloadData:(OfflineDownload*)offlineDownloadData withCompletion:(void (^)(void))complete;


- (void)resetPropertiseToDefaultValue;
-(void) resetUserActionSharedInstanceValuesOnLogout;
@end
