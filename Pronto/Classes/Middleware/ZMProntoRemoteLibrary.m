//
//  ZMProntoRemoteLibrary.m
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Zemoga Inc. All rights reserved.
//

#import "ZMProntoRemoteLibrary.h"
#import "SystemConfiguration/SystemConfiguration.h"
#import <ASFKit/ASFKit.h>
#import <ASFKit/ASFUtilities.h>
#import "UIImage+Util.h"
#import "NSString+Util.h"

@implementation ZMProntoRemoteLibrary
    
/**
 *  this will extract an identtity based from a PKCS12Data (its a p12 certificate with an identity
 *  @param identity a reference to a SecIdentifyRef wich will be filled with the identity provided by the PKCS12Data
 *  @param trust variable used to help on identity extract
 *  @param PLCS12Data the contents of a p12 certificate
 *  @return BOOL indicating if the identity was extracted or not
 */
    
+(BOOL)extractIdentity:(SecIdentityRef *)identity trust:(SecTrustRef *)trust PKCS12Data:(NSData *)PKCS12Data
{
    BOOL extracted;
    OSStatus securityError = errSecSuccess;
    CFDataRef inPKCS12Data = (__bridge CFDataRef)PKCS12Data;
        
    CFStringRef password = CFSTR("cgarcia");
    const void *keys[] =   { kSecImportExportPassphrase };
    const void *values[] = { password };
    CFDictionaryRef optionsDictionary = CFDictionaryCreate(NULL, keys,values, 1,NULL, NULL);
    CFArrayRef items = CFArrayCreate(NULL, 0, 0, NULL);
    securityError = SecPKCS12Import(inPKCS12Data,optionsDictionary,&items);
        if (securityError == 0) {
            CFDictionaryRef myIdentityAndTrust = CFArrayGetValueAtIndex (items, 0);
            const void *tempIdentity = NULL;
            tempIdentity = CFDictionaryGetValue (myIdentityAndTrust, kSecImportItemIdentity);
            *identity = (SecIdentityRef)tempIdentity;
            const void *tempTrust = NULL;
            tempTrust = CFDictionaryGetValue (myIdentityAndTrust, kSecImportItemTrust);
            *trust = (SecTrustRef)tempTrust;
            extracted = YES;
        } else {
            NSLog(@"Failed with error code %d",(int)securityError);
            extracted = NO;
        }
    CFRelease(optionsDictionary);
    return extracted;
}
    
    /**
     *  Implementation of ZMRemoteLibraryService:contentByUri in order to login with a custom certificate
 */
    -(NSString *) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int)requestType error:(NSError **)error{
        return [self contentByUri:uri parameters:params requestType:requestType headers:[NSDictionary dictionary] error:error];
    }
    
    
/**
 *
 **/
-(NSString *) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int)requestType headers:(NSDictionary *)headers error:(NSError **)error{
        return nil;
}

/**
 *
 **/
-(void) contentByUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int) requestType headers:(NSDictionary *)headers error:(NSError *)error complete:(void(^)(NSString *response, NSError *error))complete{
    NSMutableDictionary* errorDetails = [NSMutableDictionary dictionary];
    if(ACTIVE_SECURITY_FRAMEWORK){
        if (![ASFSession sharedSession].sessionStarted && ![ASFSession sharedSession].upi){
            NSLog(@"Session not active, stoping donwloads.");
            [errorDetails setValue:@"Abbott Session Not Found" forKey:NSLocalizedDescriptionKey];
            if (error){
                error = [[NSError alloc] initWithDomain:@"URLRequestError" code:90000 userInfo:errorDetails];
            }
            return;
        }
        
        if ([[ASFSession sharedSession] getSalesFranchiseID]) {
            
            NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:params];
            [tempParams setValue:[[ASFSession sharedSession] getSpaceStructureID] forKey:@"ssid"];
            [tempParams setValue:[[ASFSession sharedSession] getSalesFranchiseID] forKey:@"sf"];
            [tempParams setValue:[[ASFSession sharedSession] getUpi] forKey:@"upi"];
            params = [NSDictionary dictionaryWithDictionary:tempParams];
        }
    }
    if(!importedCertificate){
        PKCS12Data = [[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"carlos" ofType:@"p12"]];
        importedCertificate=[ZMProntoRemoteLibrary extractIdentity:&identity trust:&trust PKCS12Data:PKCS12Data];
    }
    
    if(importedCertificate){
        if(![self checkParameter:@"login.token" error:&error]){
            [errorDetails setObject:@"No sesion token found. Can't continue" forKey:@"errorDetails"];
            if (error){
                error = [NSError errorWithDomain:@"SessionTokenError" code:104 userInfo:errorDetails];
            }
        }
        NSString *userToken = [self checkParameter:@"login.token" error:&error];
        if([userToken isEqualToString:@""]){
            [errorDetails setObject:@"No sesion token found. Can't continue" forKey:@"errorDetails"];
            if (error){
                error = [NSError errorWithDomain:@"SessionTokenError" code:104 userInfo:errorDetails];
            }
        }
        NSMutableString* postData = [[NSMutableString alloc] initWithString:@""];
        if([params count] > 0){
            for(NSString* key in [params allKeys]){
                [postData appendFormat:@"%@=%@&",key,[params objectForKey:key]];
            }
        }
        NSString *requestUri = [[NSString alloc] initWithString:uri];
        if(requestType==ZMURLRequestTypeGET && [params count] > 0){
            requestUri = [NSString stringWithFormat:@"%@?%@",uri,postData];
        }
        /*ZMURLRequest *urlRequest = [[ZMURLRequest alloc] init];
        [urlRequest requestWithURL:[NSURL URLWithString:requestUri] params:params headers:headers completionHandler:^(NSString *response, NSError *error) {
            complete(response, error);
        }];*/
        
        NSURL *url = [NSURL URLWithString:requestUri];
        [[[ZMURLRequest alloc] initRequestWithURL:url params:params method:ZMURLRequestTypePOST headers:nil completionHandler:^(NSString *response, NSError *error) {
            complete(response, error);
        }] start];
    }
}


/**
 *
 **/
-(NSString *) requestUri:(NSString *)uri parameters:(NSDictionary *)params requestType:(int) requestType headers:(NSDictionary *)headers error:(NSError **)error {
    
    NSMutableDictionary* errorDetails = [NSMutableDictionary dictionary];
    
    if (ACTIVE_SECURITY_FRAMEWORK) {
        
        if (![ASFSession sharedSession].sessionStarted && ![ASFSession sharedSession].upi){
            
            NSLog(@"Session not active, stoping donwloads.");
            [errorDetails setValue:@"Abbott Session Not Found" forKey:NSLocalizedDescriptionKey];
            
            if (error){
                *error = [[NSError alloc] initWithDomain:@"URLRequestError" code:90000 userInfo:errorDetails];
            }
            return nil;
        }
        
        //@Warning0108 Space Structure ID shouldn't be used meaning that for some user these parameters might not be populated
        if ([[ASFSession sharedSession] getSpaceStructureID]) {
            
            NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:params];
            [tempParams setValue:[[ASFSession sharedSession] getSpaceStructureID] forKey:@"ssid"];
            [tempParams setValue:[[ASFSession sharedSession] getSalesFranchiseID] forKey:@"sf"];
            [tempParams setValue:[[ASFSession sharedSession] getUpi] forKey:@"upi"];
            params = [NSDictionary dictionaryWithDictionary:tempParams];
        }
    }
    
    if(!importedCertificate){
        PKCS12Data = [[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"carlos" ofType:@"p12"]];
        importedCertificate = [ZMProntoRemoteLibrary extractIdentity:&identity trust:&trust PKCS12Data:PKCS12Data];
    }
    
    if(importedCertificate){
        
        if(![self checkParameter:@"login.token" error:error]) {
            [errorDetails setObject:@"No sesion token found. Can't continue" forKey:@"errorDetails"];
            if (error){
                *error = [NSError errorWithDomain:@"SessionTokenError" code:104 userInfo:errorDetails];
            }
        }

        NSString *userToken = [self checkParameter:@"login.token" error:error];
        if ([userToken isEqualToString:@""]) {
            [errorDetails setObject:@"No sesion token found. Can't continue" forKey:@"errorDetails"];
            if (error){
                *error = [NSError errorWithDomain:@"SessionTokenError" code:104 userInfo:errorDetails];
            }
        }
        
        NSMutableString* postData = [[NSMutableString alloc] initWithString:@""];
        if([params count] > 0){
            for(NSString* key in [params allKeys]){
                [postData appendFormat:@"%@=%@&", key,[params objectForKey:key]];
            }
        }
        
        NSString *requestUri = [[NSString alloc] initWithString:uri];
        if (requestType == ZMRequestGET && [params count] > 0) {
            requestUri = [NSString stringWithFormat:@"%@?%@", uri, postData];
        }
        
        //dispatch_async(dispatch_get_main_queue(), ^{
        ASFURLRequest *r = [ASFURLRequest requestWithURL:[NSURL URLWithString:requestUri] completionHandler:nil];
        r.synchronous = YES;
        [r setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        if (![userToken isEqualToString:@"revalidate"]) {
            if(userToken){
                [r setValue:userToken forHTTPHeaderField:@"X-CSRF-Token"];
            }
        }
        
        for (NSString *key in headers) {
            NSLog(@"key: %@",key);
            [r setValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
        
        if ([ZMRequestTypeName(requestType) isEqualToString:@"POST"]) {
            [r setHTTPMethod:ASFURLRequestTypePOST];
            
            if(postData!=nil) {
                [r setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        else if([ZMRequestTypeName(requestType) isEqualToString:@"GET"]) {
            [r setHTTPMethod:ASFURLRequestTypeGET];
        }
        else if([ZMRequestTypeName(requestType) isEqualToString:@"PUT"]) {
            [r setHTTPMethod:ASFURLRequestTypePUT];
        }
        else {
            [r setHTTPMethod:ASFURLRequestTypeDELETE];
        }
        r.delegate = self;
        r.synchronous = YES;
        [r start];

        NSLog(@"The url >> %@", requestUri);
        NSLog(@"The params >> %@", postData);
        NSLog(@"The Headers >> %@", headers);
        
        NSString *response = nil;
        if (r.responseData) {
            if([r.responseData bytes]){
                response = [NSString stringWithUTF8String:[r.responseData bytes]];
            }
        }
        
        if (r.responseCode) {
            self.lastServerCodeResponse = r.responseCode;
        }
        if (r.responseCode != 200) {
            [errorDetails setValue:response forKey:NSLocalizedDescriptionKey];
            if (error){
                *error = [[NSError alloc] initWithDomain:@"URLRequestError" code:r.responseCode userInfo:errorDetails];
            }
            return nil;
        }
        else {
            error = nil;
        }
        //NSLog(@"resonse code %d: %@",responseCode,response);
        return response;
    }
    
    return nil;
}

- (NSDictionary *) loadUserData {
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:[ASFSession sharedSession].adDomain forKey:@"adDomain"];
    [data setValue:[ASFSession sharedSession].adLogon forKey:@"adLogon"];
    [data setValue:[ASFSession sharedSession].c forKey:@"c"];
    [data setValue:[ASFSession sharedSession].company forKey:@"company"];
    [data setValue:[(NSString *)[ASFSession sharedSession].department urlEncode] forKey:@"department"];
    [data setValue:[ASFSession sharedSession].displayName forKey:@"displayName"];
    [data setValue:[ASFSession sharedSession].divisionName forKey:@"division"];
    [data setValue:[ASFSession sharedSession].divisionCode forKey:@"divisionCode"];
    [data setValue:[ASFSession sharedSession].divisionName forKey:@"divisionName"];
    [data setValue:[ASFSession sharedSession].employeeType forKey:@"employeeType"];
    [data setValue:[ASFSession sharedSession].displayName forKey:@"fullName"];
    [data setValue:[ASFSession sharedSession].givenName forKey:@"givenName"];
    [data setValue:[ASFSession sharedSession].initials forKey:@"initial"];
    [data setValue:[(NSString *)[ASFSession sharedSession].location urlEncode] forKey:@"location"];
    [data setValue:[ASFSession sharedSession].mail forKey:@"mail"];
    [data setValue:[ASFSession sharedSession].managerDisplayName forKey:@"managerDisplayName"];
    [data setValue:[ASFSession sharedSession].managerEmail forKey:@"managerEmail"];
    [data setValue:[ASFSession sharedSession].managerUPI forKey:@"managerUPI"];
    [data setValue:[ASFSession sharedSession].preferredLanguage forKey:@"preferredLanguage"];
    [data setValue:[(NSString *)[ASFSession sharedSession].salesArea urlEncode] forKey:@"salesArea"];
    [data setValue:[ASFSession sharedSession].salesCategoryID forKey:@"salesCategoryID"];
    [data setValue:[ASFSession sharedSession].salesDistrict forKey:@"salesDistrict"];
    [data setValue:[(NSString *)[ASFSession sharedSession].salesForceName urlEncode] forKey:@"salesForceName"],
    [data setValue:[ASFSession sharedSession].salesForceNumber forKey:@"salesForceNumber"],
    [data setValue:[ASFSession sharedSession].salesFranchise forKey:@"salesFranchise"],
    [data setValue:[ASFSession sharedSession].salesFranchiseID forKey:@"salesFranchiseID"],
    [data setValue:[ASFSession sharedSession].salesPMI forKey:@"salesPMI"];
    [data setValue:[ASFSession sharedSession].salesRegion forKey:@"salesRegion"];
    [data setValue:[ASFSession sharedSession].salesSampling forKey:@"salesSampling"];
    [data setValue:[ASFSession sharedSession].sn  forKey:@"sn"];
    [data setValue:[ASFSession sharedSession].spaceStructureID forKey:@"spaceStructureID"];
    [data setValue:[ASFSession sharedSession].telephoneNumber forKey:@"telephoneNumber"];
    [data setValue:[ASFSession sharedSession].territory forKey:@"territory"];
    [data setValue:[(NSString *)[ASFSession sharedSession].title urlEncode] forKey:@"title"] ;
    [data setValue:[ASFSession sharedSession].upi forKey:@"upi"];
    //[data setValue:[ASFSession sharedSession].sessionIndex forKey:@"sessionIndex"];
    [data setValue:@"" forKey:@"sessionIndex"];
    [data setValue:[ASFSession sharedSession].expires forKey:@"expires"];
    //[data setValue:[ASFSession sharedSession].subjectNameID forKey:@"subjectNameID"];
    [data setValue:@"" forKey:@"subjectNameID"];
    [data setValue:[ASFSession sharedSession].frameworkVersion forKey:@"frameworkVersion"];
    
    //TODO: Check the profile dictionary
    //[ASFSession sharedSession].authorization.profile.salesTerritoryTypeID
    
    //[data setValue:[[ASFSession sharedSession] getSAMLAttribute:@"salesTerritoryTypeID"] forKey:@"salesTerritoryTypeID"];
    [data setValue:[ASFSession sharedSession].authorization.profile.salesTerritoryTypeID forKey:@"salesTerritoryTypeID"];
    
    //[data setValue:[[ASFSession sharedSession] getSAMLAttribute:@"salesSalesForce"] forKey:@"salesSalesForce"];
    [data setValue:[ASFSession sharedSession].authorization.profile.salesSalesForce forKey:@"salesSalesForce"];
    return data;
}

- (NSString *)getThumbnailPath:(NSString *)externalPath {
    
    NSString *path;
    NSString *basePath = [NSString stringWithFormat:@"%@/com.abbvie.grid.thumbnails/", [ASFUtilities cachesDirectory]];
    //@ABT-138 Fix to force the reload of all thumbnails. Added extra 2 character. Not recommended.
    path = [NSString stringWithFormat:@"%@%@2", basePath, [externalPath lastPathComponent]];
    return path;
}

/*!
 * ABT-80
 */
- (void)downloadThumbnail:(NSString *)path complete:(void(^)(UIImage *thumbnail, NSString *p))complete fromCache:(BOOL)cache includeWaterMark:(NSString *)waterMark {
    
    __block UIImage *thumb;
    
    NSString *basePath = [NSString stringWithFormat:@"%@/com.abbvie.grid.thumbnails/", [ASFUtilities cachesDirectory]];

    if (![[NSFileManager defaultManager] fileExistsAtPath:basePath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:basePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    //@ABT-138 Fix to force the reload of all thumbnails. Added extra 2 character. Not recommended.
    NSString *filPath = [NSString stringWithFormat:@"%@%@2", basePath, [path lastPathComponent]];
    if (cache && [[NSFileManager defaultManager] fileExistsAtPath:filPath]) {

        //Verify if there is a cached version of the thumbnail
        if ([[NSFileManager defaultManager] fileExistsAtPath:filPath]) {
            thumb = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:filPath]];
        }
        complete(thumb, path);
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSURL *url = [NSURL URLWithString:path];

            [[ASFURLRequest downloadFileFromURL:url toFilePath:filPath completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *error) {
                    
                if ([[NSFileManager defaultManager] fileExistsAtPath:filPath]) {
                    thumb = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:filPath]];
                }
                
                complete(thumb, path);
            }] start];
        });
    }
}

/**
 * Implementation of ZMRemoteLibraryService:downloadAsset But With a delegate for progress indicator purposes
 */
- (NSString *)downloadAsset:(ZMAsset *)asset error:(NSError **)error withDelegate:(id)progressDelegate completionHandler:(void(^)(NSString *path, NSError *error)) completionHandler{
    
    asset = [asset copy];
    if (![ASFSession sharedSession].sessionStarted ||![ASFSession sharedSession].upi){
        if(error != NULL){
            *error = [NSError errorWithDomain:@"download_error" code:9000 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"No session Detected",NSLocalizedDescriptionKey, nil]];
        }
    }
    NSString *baseDir = [ASFUtilities getDocumentsDirectory];
    NSString *filename = [self MD5HashForString:asset.url];
    NSString *extension = [[asset.url componentsSeparatedByString:@"."] lastObject];
    NSString *destFilename = [[NSString alloc] initWithFormat:@"%@/%@.%@",baseDir,filename,extension];
    NSURL *remoteUrl = [[NSURL alloc] initWithString:asset.url];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        ASFURLRequest *request = [ASFURLRequest downloadFileFromURL:remoteUrl toFilePath:destFilename completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *error) {
            self.lastServerCodeResponse = request.responseCode;
            completionHandler(destFilename, error);
        }];
        if(progressDelegate!=nil){
            [request setDelegate:progressDelegate];
        }
        [request start];
        
    });
    return destFilename;
}

/**
 * Implementation of ZMRemoteLibraryService:downloadAsset But With a delegate for progress indicator purposes
 */
- (NSString *) downloadAsset:(ZMAsset *)asset error:(NSError **)error withDelegate:(id)progressDelegate{
        if (![ASFSession sharedSession].sessionStarted ||![ASFSession sharedSession].upi){
            if(error != NULL){
                *error = [NSError errorWithDomain:@"download_error" code:9000 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"No session Detected",NSLocalizedDescriptionKey, nil]];
            }
        }
        NSString *baseDir = [ASFUtilities getDocumentsDirectory];
        NSString *filename = [self MD5HashForString:asset.url];
        NSString *extension = [[asset.url componentsSeparatedByString:@"."] lastObject];
        NSString *destFilename = [[NSString alloc] initWithFormat:@"%@/%@.%@",baseDir,filename,extension];
        NSURL *remoteUrl = [[NSURL alloc] initWithString:asset.url];
        
        /*ASFURLRequest *fileRequest2 = [ASFURLRequest downloadFileFromURL:remoteUrl toFilePath:destFilename delegate:self];
        
        [fileRequest2 setDelegate:progressDelegate];
        fileRequest2 cont*/
        
        dispatch_async(dispatch_get_main_queue(), ^{
            ASFURLRequest *request = [ASFURLRequest downloadFileFromURL:remoteUrl toFilePath:destFilename completionHandler:^(ASFURLRequest *request, NSData *responseData, NSError *error) {
            }];
            if(progressDelegate!=nil){
                [request setDelegate:progressDelegate];
            }
            //request.synchronous = YES;
            [request start];
            
        });
        
        return destFilename;
        
        /*if(request.responseCode != 200 && request.responseCode != 206){
            if(request.responseCode == 0){
                if(error != NULL){
                    *error = [NSError errorWithDomain:@"download_error" code:200 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"The connection was reset %d", request.responseCode], NSLocalizedDescriptionKey, nil]];
                }
            }else if (error != NULL){
                *error = [NSError errorWithDomain:@"download_error" code:200 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[request request],NSLocalizedDescriptionKey, nil]];
            }
            return nil;
        }
        
        return destFilename;*/
        
}
    

/**
 * check if theres a internet connection available
 */
- (BOOL)isConnectionAvailable {
    
    SCNetworkReachabilityFlags flags;
    BOOL receivedFlags;
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [@"google.com" UTF8String]);
    receivedFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
        
    if (!receivedFlags || (flags == 0)) {
        return FALSE;
    }

    return TRUE;
}

- (BOOL)requestShouldStartAutomatically{
    return YES;
}

@end