//
//  ZMURLRequest.h
//  Pronto
//
//  Created by Sebastian Romero on 5/28/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

//#import <ASFKit/ASFKit.h>

#import <Foundation/Foundation.h>

/**
 *  Reuquest type
 */
typedef enum
{
    ZMURLRequestTypeGET = 1,
    ZMURLRequestTypePOST = 2,
    ZMURLRequestTypePUT = 3,
    ZMURLRequestTypeDELETE = 4
} ZMURLRequestType;


@interface ZMURLRequest : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

/**
 *  Convenience method to initialize a request to a URL specifying a block to be invoked when the request is finished.
 *  @param url The targetted URL of the request.
 *  @param params
 *  @param method Method for the request
 *  @param headers Optional headers for the reuqest
 *  @param completionHandler The block to be invoked when the request finishes.
 *  @return R4URLRequest instance, if you want to retry the call
 */
- (instancetype) initRequestWithURL:(NSURL *)url params:(NSDictionary *)params method:(ZMURLRequestType)method headers:(NSDictionary *)headers completionHandler:(void(^)(NSString *response, NSError *error)) completionHandler;


/**
 *  Downloads a file into the specified filepath
 *  @param url The targetted URL of the request.
 *  @param filepath destination target
 */
-(void) downloadFileFromURL:(NSURL *)url toFilePath:(NSString *)filepath completionHandler:(void(^)(NSString *path, NSError *error)) completionHandler;


/**
 *  Starts the request. This method should not be overridden. This will trigger the requestShouldStart: method just before sending the request and requestDidStart
 */
-(void) start;

/**
 *  Paraments for the request
 */
@property (nonatomic, weak) NSDictionary *parameters;
/**
 *  Headers for the request
 */
@property (nonatomic, weak) NSDictionary *headers;
/**
 *  Method for the request
 */
@property (nonatomic, readwrite) ZMURLRequestType method;
/**
 *  Complete handler
 */
@property (nonatomic, copy) void (^complete)(NSString *response, NSError *error);
/**
 *  Destination of the request
 */
@property (nonatomic, weak) NSString *destinationPath;


@end
