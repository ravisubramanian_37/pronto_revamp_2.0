//
//  Franchaise.h
//  Pronto
//
//  Created by Oscar Robayo on 10/19/15.
//  Copyright © 2015 Zemoga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset, Brand;

NS_ASSUME_NONNULL_BEGIN

@interface Franchaise : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Franchaise+CoreDataProperties.h"
