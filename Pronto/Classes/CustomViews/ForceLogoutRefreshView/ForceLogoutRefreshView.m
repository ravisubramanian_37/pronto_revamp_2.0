//
//  ForceLogoutRefreshView.m
//  Pronto
//
//  Created by m-666346 on 05/11/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "ForceLogoutRefreshView.h"
#import "ViewUtils.h"
#import "UIImage+ImageDirectory.h"
#import "Pronto-Swift.h"


@interface ForceLogoutRefreshView()

@property (nonatomic, weak) IBOutlet UIButton *refreshButton;
@property (nonatomic, weak) IBOutlet UIButton *logoutButton;

@property (nonatomic, weak) IBOutlet UIView *errorView;
@property (nonatomic, weak) IBOutlet UILabel *errorLabel;


@end;

@implementation ForceLogoutRefreshView

- (void)showErroView:(BOOL)show
{
    self.errorView.hidden = !show;
}
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setupUI
{
    //Set up UI
    self.backgroundColor = [UIColor clearColor];
    self.errorView.backgroundColor = [UIColor clearColor];
    
    [self.logoutButton setImage:[UIImage GetLogoutWithTextIcon] forState:UIControlStateNormal];
    [self.refreshButton setImage:[UIImage GetLoadingRefreshIcon] forState:UIControlStateNormal];
    
//    self.errorLabel.text = [NSString stringWithFormat:@"Oops, some error occured.\nPlease Refresh to start over."];
    self.errorLabel.text = [NSString stringWithFormat:@"Oops, some error occured.\nPlease Refresh"];
}

+ (NSString*)GetForceLogoutRefreshViewNibName
{
    return @"ForceLogoutRefreshView";
}

+ (ForceLogoutRefreshView*)GetForceLogoutRefreshView
{
    ForceLogoutRefreshView *forceLogoutRefreshView = (ForceLogoutRefreshView*)[[[NSBundle mainBundle] loadNibNamed:[ForceLogoutRefreshView GetForceLogoutRefreshViewNibName]
                                                                                                             owner:self options:nil] lastObject];
    return forceLogoutRefreshView;
}

- (void)adjustFrameWithRespectToSuperView
{
    CGRect superviewFrame = self.superview == nil ? [[UIScreen mainScreen] bounds] : self.superview.frame;
    self.top    = superviewFrame.size.height - (self.height + 50); //
    self.left   = (superviewFrame.size.width - self.width) / 2 ;
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"---- > %f %f ", self.logoutButton.width, self.logoutButton.height]];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"---- > force view %f  %f ", self.top, self.left]];
}

- (IBAction)logoutButtonAction:(UIButton *)sender
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(forceLogoutRefreshView:didTapLogoutButton:)])
    {
        [self.delegate forceLogoutRefreshView:self didTapLogoutButton:sender];
    }
}

- (IBAction)refreshButtonAction:(UIButton *)sender
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(forceLogoutRefreshView:didTapRefreshButton:)])
    {
        [self.delegate forceLogoutRefreshView:self didTapRefreshButton:sender];
    }
    
    //Stoping user from tapping continuosly refresh button.
    __weak ForceLogoutRefreshView *weakself = self;
    self.refreshButton.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakself.refreshButton.userInteractionEnabled = YES;
    });
}

@end
