//
//  ForceLogoutRefreshView.h
//  Pronto
//
//  Created by m-666346 on 05/11/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ForceLogoutRefreshView;

@protocol ForceLogoutRefreshViewDelegate<NSObject>
- (void)forceLogoutRefreshView:(ForceLogoutRefreshView*)forceLogoutRefreshView didTapLogoutButton:(UIButton*)logoutButton;
- (void)forceLogoutRefreshView:(ForceLogoutRefreshView*)forceLogoutRefreshView didTapRefreshButton:(UIButton*)refreshButton;

@end

@interface ForceLogoutRefreshView : UIView
@property (nonatomic, weak) id <ForceLogoutRefreshViewDelegate> delegate;

+ (ForceLogoutRefreshView*)GetForceLogoutRefreshView;
- (void)adjustFrameWithRespectToSuperView;
- (void)showErroView:(BOOL)show;

@end
