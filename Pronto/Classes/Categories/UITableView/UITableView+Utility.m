//
//  UITableView+Utility.m
//  Pronto
//
//  Created by m-666346 on 15/01/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

#import "UITableView+Utility.h"

@implementation UITableView (Utility)

- (void)reloadDataWithCompletion:(completionHandler)completion
{
    [self reloadData];
    //1
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (completion != nil)
        {
            completion();
        }
    });
}

@end
