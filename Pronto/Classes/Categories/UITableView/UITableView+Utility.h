//
//  UITableView+Utility.h
//  Pronto
//
//  Created by m-666346 on 15/01/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface UITableView (Utility)

- (void)reloadDataWithCompletion:(completionHandler)completion;

@end
