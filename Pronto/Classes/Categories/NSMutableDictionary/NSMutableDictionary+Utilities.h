//
//  NSMutableDictionary+Utilities.h
//  Pronto
//
//  Created by m-666346 on 19/07/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (Utilities)

- (void)setObjectIfValueAvailable:(id)object forKey:(id)key;

@end
