//
//  NSMutableDictionary+Utilities.m
//  Pronto
//
//  Created by m-666346 on 19/07/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "NSMutableDictionary+Utilities.h"

@implementation NSMutableDictionary (Utilities)

- (void)setObjectIfValueAvailable:(id)object forKey:(id)key;
{
    if (object != nil && object != [NSNull null] &&
        key != nil && key != [NSNull null])
    {
        [self setObject:object forKey:key];
    }
}
@end
