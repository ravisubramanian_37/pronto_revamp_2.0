//
//  UICollectionView+Utility.m
//  Pronto
//
//  Created by m-666346 on 27/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "UICollectionView+Utility.h"

@implementation UICollectionView (Utility)

- (void)reloadDataWithCompletion:(completionHandler)completion
{
    [self reloadData];
    //1
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.33 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (completion != nil)
        {
            completion();
        }
    });
}

@end
