//
//  UICollectionView+Utility.h
//  Pronto
//
//  Created by m-666346 on 27/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface UICollectionView (Utility)

- (void)reloadDataWithCompletion:(completionHandler)completion;

@end
