//
//  Franchaise+CoreDataProperties.m
//  Pronto
//
//  Created by Oscar Robayo on 10/19/15.
//  Copyright © 2015 Zemoga. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Franchaise+CoreDataProperties.h"

@implementation Franchaise (CoreDataProperties)

@dynamic field_active;
@dynamic field_private;
@dynamic franchaiseID;
@dynamic name;
@dynamic tid;
@dynamic vid;
@dynamic assets;
@dynamic brands;

@end
