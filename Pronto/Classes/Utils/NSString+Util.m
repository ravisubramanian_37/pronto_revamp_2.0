//
//  NSString+Util.m
//  Pronto
//
//  Created by Sebastian Romero on 2/4/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "NSString+Util.h"

@implementation NSString (Util)

-(NSString *) urlEncode
{
   // return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)self, NULL,(CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8 ));
    
#if (TARGET_OS_MAC && (MAC_OS_X_VERSION_MIN_REQUIRED > MAC_OS_X_VERSION_10_9))
    NSString * encodedString = (NSString *)
    CFURLCreateStringByReplacingPercentEscapes(
                                               NULL,
                                               (CFStringRef)self,
                                               NULL);
#else
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    NSString * encodedString = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                              NULL,
                                                              (CFStringRef)self,
                                                              NULL,
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8 ));
    #pragma clang diagnostic pop
#endif
    return encodedString;
    
    
}


-(BOOL)contains:(NSString *)string
{
    NSRange range = [self rangeOfString:string];
    return (range.location != NSNotFound);
}

- (BOOL)isEmpty
{
    if ([self isKindOfClass:[NSString class]] == YES && self.length <= 0)
    {
        return YES;
    }
    return NO;
}

@end
