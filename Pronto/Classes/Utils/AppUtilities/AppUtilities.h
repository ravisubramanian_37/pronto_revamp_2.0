//
//  AppUtilities.h
//  Pronto
//
//  Created by m-666346 on 22/05/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"

#import "EventDuration+CoreDataProperties.h"
#import "EventDuration+CoreDataClass.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "NSDate+DateDirectory.h"

//@class ProntoTabbarViewController;

@interface AppUtilities : NSObject

+ (id)sharedUtilities;

- (BOOL)isLandscape;
- (void)showBottomIndicatorView:(BOOL)show withMessage:(NSString*)message;
//For Print
- (NSDictionary*)renderPrintDictionaryForEvent:(Event*)event;

@end
