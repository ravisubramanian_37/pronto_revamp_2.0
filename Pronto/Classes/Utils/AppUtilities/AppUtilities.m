//
//  AppUtilities.m
//  Pronto
//
//  Created by m-666346 on 22/05/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "AppUtilities.h"
#import "AppDelegate.h"
//#import "ProntoTabbarViewController.h"
#import "Constant.h"
#import "Pronto-Swift.h"

@implementation AppUtilities

+ (id)sharedUtilities
{
    @synchronized(self)
    {
        static AppUtilities *sharedInstance = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            sharedInstance = [[self alloc] init];
        });
        return sharedInstance;
    }
}



- (BOOL)isLandscape
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIInterfaceOrientationLandscapeLeft ||
        orientation == UIInterfaceOrientationLandscapeRight)
    {
        return YES;
    }
    return NO;
}

- (void)showBottomIndicatorView:(BOOL)show withMessage:(NSString*)message
{
//    #warning remove Temp
//    return;
    dispatch_async(dispatch_get_main_queue(), ^{
        
   
    AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(show == YES)
    {
        [mainDelegate showMessage:message whithType:ZMProntoMessagesTypeSync];
    }
    else
    {
        [mainDelegate hideMessage];
    }
    });
}


- (NSDictionary*)renderPrintDictionaryForEvent:(Event*)event
{
    NSMutableDictionary* dictionaryForPrintTable = [NSMutableDictionary dictionary];
    
    [dictionaryForPrintTable setValue:event.title forKey:@"EventTitle"];
    [dictionaryForPrintTable setValue:event.location forKey:@"EventLocation"];
    [dictionaryForPrintTable setValue:event.startDate forKey:@"EventStartDate"];
    [dictionaryForPrintTable setValue:event.endDate forKey:@"EventEndDate"];
    [dictionaryForPrintTable setValue:event.eventTimezone forKey:@"EventTimeZone"];
    
    NSMutableArray *sortedDurations = [NSMutableArray array];
    NSArray* sessionUniqueDateKeys = [event getUniqueDatesOfAllSessionSortedAccordingToStartTime];
    for(NSDate *date in sessionUniqueDateKeys)
    {
        NSDate *timeWithLocalTimeZone = [NSDate GetDateInLocalTimeZoneFromUTCDate:date];
        NSArray *durations = [event getSortedDurationForDateString:timeWithLocalTimeZone];
        [sortedDurations addObjectsFromArray:durations];
    }
    
    [dictionaryForPrintTable setValue:sessionUniqueDateKeys forKey:@"DurationDates"];
    
    [dictionaryForPrintTable setValue:sortedDurations forKey:@"Durations"];
    return (NSDictionary *)dictionaryForPrintTable;
}


@end
