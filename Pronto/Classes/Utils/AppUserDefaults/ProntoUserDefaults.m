//
//  ProntoUserDefaults.m
//  Pronto
//
//  Created by m-666346 on 10/05/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "ProntoUserDefaults.h"
#import "Constant.h"
#import "Pronto-Swift.h"


NSString *const kAppCMSLastSynchronization                  = @"com.ProntoUserDefaults_App_CMS_Last_Synchronization";
NSString *const kAppCMSLastSynchronizationForEvents         = @"com.ProntoUserDefaults_App_CMS_Last_Synchronization_For_Events";
NSString *const kCurrentUserID                              = @"com.ProntoUserDefaults_Current_User_ID";
NSString *const kCouldNotDeleteOrUnpublishAssetIds          = @"com.ProntoUserDefaults_CouldNotDeleteOrUnpublishAssetIds";
NSString *const kCouldNotDeleteOrUnpublishEventIds          = @"com.ProntoUserDefaults_CouldNotDeleteOrUnpublishEventIds";
NSString *const kAppVersion                                 = @"com.ProntoUserDefaults_App_Version";
NSString *const kPreviousAsset                              = @"com.ProntoUserDefaults_Previous_Asset";
NSString *const kIsAppFreshlyInstalled                      = @"com.ProntoUserDefaults_IsAppFreshlyInstalled";
NSString *const kViewedNormalAssetIds                       = @"com.ProntoUserDefaults_ViewedNormalAssetIds";
NSString *const kViewedEventOnlyAssetIds                    = @"com.ProntoUserDefaults_ViewedEventOnlyAssetIds";
NSString *const kFetchViewedAssetsFromCMS                   = @"com.ProntoUserDefaults_FetchViewedAssetsFromCMS";
NSString *const kEventV3RequestBeenMadeFromCMS              = @"com.ProntoUserDefaults_EventV3RequestBeenMadeFromCMS";
NSString *const kAOFAccessToken                             = @"com.ProntoUserDefaults_AOF_App_Access_Token";
NSString *const kAOF511Id                             = @"com.ProntoUserDefaults_AOF_511_Id";
NSString *const kdlServerEncryptionTime               = @"com.ProntoUserDefaults_dlServer_Encryption_Time";
NSString *const kdlServerEncryptionValue              = @"com.ProntoUserDefaults_dlServer_Encryption_Value";
NSString *const kIsReadyToMakeServiceCalls            = @"com.ProntoUserDefaults_IsReadyToMakeServiceCalls";


@implementation ProntoUserDefaults


+ (ProntoUserDefaults *)userDefaults
{
    @synchronized(self)
    {
        static ProntoUserDefaults *sharedInstance = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            sharedInstance = [[self alloc] init];
        });
        return sharedInstance;
    }
}
+(ProntoUserDefaults *)singleton
{
    return [ProntoUserDefaults userDefaults];
}
//Set if user logged In/Refetched token
-(NSString *)isReadyToMakeServiceCalls{
    NSString  *isReady =  [[NSUserDefaults standardUserDefaults] valueForKey:kIsReadyToMakeServiceCalls];
    return isReady;
}
-(void)isReadyToMakeServiceCalls:(int)value
{
    if(value == 0)
           [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:kIsReadyToMakeServiceCalls];
       else
           [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:kIsReadyToMakeServiceCalls];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
//For AOFKit
- (NSString*)getAOFAccessToken
{
    NSString *accessToken =  [[NSUserDefaults standardUserDefaults] valueForKey:kAOFAccessToken];
    return accessToken == nil ? @"": accessToken;
}

- (void)setAOFAccessToken:(NSString*)accessToken
{
    [[NSUserDefaults standardUserDefaults] setValue:accessToken forKey:kAOFAccessToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)getAOF511Id
{
    NSString *accessToken =  [[NSUserDefaults standardUserDefaults] valueForKey:kAOF511Id];
    return accessToken == nil ? @"": accessToken;
}

- (void)setAOF511Id:(NSString*)k511Id
{
    [[NSUserDefaults standardUserDefaults] setValue:k511Id forKey:kAOF511Id];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDate*)getdlServerEncryptionTime
{
    NSDate *time =  [[NSUserDefaults standardUserDefaults] valueForKey:kdlServerEncryptionTime];
    return time == nil ? nil : time;
}

- (void)setdlServerEncryptionTime:(NSDate*)time
{
    [[NSUserDefaults standardUserDefaults] setValue:time forKey:kdlServerEncryptionTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)getdlServerEncryptionValue
{
    NSString *value =  [[NSUserDefaults standardUserDefaults] valueForKey:kdlServerEncryptionValue];
    return value == nil ? @"": value;
}

- (void)setdlServerEncryptionValue:(NSString*)value
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:kdlServerEncryptionValue];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)getCMSLastSynchronization
{
    NSString *lastSync =  [[NSUserDefaults standardUserDefaults] valueForKey:kAppCMSLastSynchronization];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Last Sync ->  getCMSLastSynchronization %@", lastSync]];
    return lastSync == nil ? @"": lastSync;
}

- (void)setCMSLastSynchronization:(NSString*)lastSynchronization
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Last Sync ->  setCMSLastSynchronization %@", lastSynchronization]];
    
    NSString *lastSync = @"";
    if (lastSynchronization != nil)
    {
        if (![lastSynchronization isKindOfClass:[NSString class]])
        {
            lastSync = [NSString stringWithFormat:@"%@", lastSynchronization];
        }
        else
        {
            lastSync = lastSynchronization;
        }
    }
    [[NSUserDefaults standardUserDefaults] setValue:lastSync forKey:kAppCMSLastSynchronization];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//CMS Last Synchornization for Events
- (NSString*)getCMSLastSynchronizationForEvents
{
    NSString *lastSync =  [[NSUserDefaults standardUserDefaults] valueForKey:kAppCMSLastSynchronizationForEvents];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Last Sync ->  getCMSLastSynchronizationForEvents %@", lastSync]];
    return lastSync == nil ? @"0": lastSync;
}

- (void)setCMSLastSynchronizationForEvents:(NSString*)lastSynchronization
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Last Sync ->  setCMSLastSynchronizationForEvents %@", lastSynchronization]];
    
    NSString *lastSync = @"";
    if (lastSynchronization != nil)
    {
        if (![lastSynchronization isKindOfClass:[NSString class]])
        {
            lastSync = [NSString stringWithFormat:@"%@", lastSynchronization];
        }
        else
        {
            lastSync = lastSynchronization;
        }
    }
    [[NSUserDefaults standardUserDefaults] setValue:lastSync forKey:kAppCMSLastSynchronizationForEvents];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (NSString*)getCurrentUserID
{
    NSString *currentUserID =  [[NSUserDefaults standardUserDefaults] valueForKey:kCurrentUserID];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"APP PREFERENCES ->  getCurrentUserID %@", currentUserID]];
    return currentUserID == nil ? @"": currentUserID;
}

- (void)setCurrentUserID:(NSString*)userID
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"APP PREFERENCES ->  setCurrentUserID %@", userID]];
    
    NSString *currentUserID = @"";
    if (userID != nil)
    {
        if (![userID isKindOfClass:[NSString class]])
        {
            currentUserID = [NSString stringWithFormat:@"%@", userID];
        }
        else
        {
            currentUserID = userID;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:currentUserID forKey:kCurrentUserID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setCouldNotDeleteOrUnpulishAssetIds:(NSArray*)assetIDs
{
    
    NSString *assetIds = nil;
    for (id assetid in assetIDs)
    {
        if ([assetid isKindOfClass:[NSNumber class]])
        {
            if (assetIds == nil)
            {
                assetIds = [NSString stringWithFormat:@"%@",assetid];
            }
            else
            {
                assetIds = [NSString stringWithFormat:@"%@_%@",assetIds,assetid];
            }
        }
        else if ([assetid isKindOfClass:[NSString class]])
        {
            if (assetIds == nil)
            {
                assetIds = assetid;
            }
            else
            {
                assetIds = [NSString stringWithFormat:@"%@_%@",assetIds,assetid];
            }
        }
    }
    
    if (assetIds == nil)
    {
        assetIds = @"";
    }
    //saving to prefernces
    [[NSUserDefaults standardUserDefaults] setValue:assetIds forKey:kCouldNotDeleteOrUnpublishAssetIds];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray*)getCouldNotDeleteOrUnpulishAssetIds
{
    NSString *couldNotDeleteOrUnpulishAssetIds = [[NSUserDefaults standardUserDefaults] valueForKey:kCouldNotDeleteOrUnpublishAssetIds];
    NSArray *tempArray = [Constant getStringsValuesSeperatedByUnderscoreFromString:couldNotDeleteOrUnpulishAssetIds];
    NSMutableArray *tempArray2 = [NSMutableArray array];
    for (NSString *assetId in tempArray)
    {
        if (assetId.length > 0)
        {
            [tempArray2 addObject:[NSNumber numberWithInteger:assetId.integerValue]];
        }
    }
    
    return tempArray2;
}

- (void)setPreviousAsset:(Asset*)asset
{
    NSMutableDictionary * assetDetail = [[NSMutableDictionary alloc]init];
    if(asset != nil && asset.assetID != nil && asset.title)
    {
        [assetDetail setValue:asset.assetID forKey:@"id"];
        [assetDetail setValue:asset.title forKey:@"name"];
        [assetDetail setValue:asset.file_mime forKey:@"file_mime"];
    }
    [[NSUserDefaults standardUserDefaults] setValue:assetDetail forKey:kPreviousAsset];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDictionary*)getPreviousAsset
{
    //Changes for Warnings
    NSDictionary *tempDict = [[NSUserDefaults standardUserDefaults] valueForKey:kPreviousAsset];
    return tempDict;
}

- (void)setIsAppFreshlyInstalled:(BOOL)showPermission
{
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:showPermission] forKey:kIsAppFreshlyInstalled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)getIsAppFreshlyInstalled
{ 
    NSNumber *freshInstalled = [[NSUserDefaults standardUserDefaults] valueForKey:kIsAppFreshlyInstalled] ;
    return freshInstalled == nil ? YES : NO;
}

- (void)saveAppVersion //kAppVersion
{
    NSString *currentAppVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *oldAppVersion = [[ProntoUserDefaults userDefaults] getAppVersion];
    
    if (currentAppVersion == nil || currentAppVersion.length <= 0  ||
        ([[currentAppVersion lowercaseString] isEqualToString:[oldAppVersion lowercaseString]] == NO))
    {
        [[NSUserDefaults standardUserDefaults] setValue:currentAppVersion forKey:kAppVersion];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSString*)getAppVersion
{
    NSString *currentAppVersion =  [[NSUserDefaults standardUserDefaults] valueForKey:kAppVersion];
    return currentAppVersion == nil ? @"": currentAppVersion;
}


//undeleted or unpublished events ids
- (NSArray*)getCouldNotDeleteOrUnpulishEventIds
{
    NSString *couldNotDeleteOrUnpulishAssetIds = [[NSUserDefaults standardUserDefaults] valueForKey:kCouldNotDeleteOrUnpublishEventIds];
    NSArray *tempArray = [Constant getStringsValuesSeperatedByUnderscoreFromString:couldNotDeleteOrUnpulishAssetIds];
    NSMutableArray *tempArray2 = [NSMutableArray array];
    for (NSString *assetId in tempArray)
    {
        if (assetId.length > 0)
        {
            [tempArray2 addObject:[NSNumber numberWithInteger:assetId.integerValue]];
        }
    }
    
    return tempArray2;
}

- (void)setCouldNotDeleteOrUnpulishEventIds:(NSArray*)eventIDs
{
    NSString *eventIds = nil;
    for (id eventId in eventIDs)
    {
        if ([eventId isKindOfClass:[NSNumber class]])
        {
            if (eventIds == nil)
            {
                eventIds = [NSString stringWithFormat:@"%@",eventId];
            }
            else
            {
                eventIds = [NSString stringWithFormat:@"%@_%@",eventIds,eventId];
            }
        }
        else if ([eventId isKindOfClass:[NSString class]])
        {
            if (eventIds == nil)
            {
                eventIds = eventId;
            }
            else
            {
                eventIds = [NSString stringWithFormat:@"%@_%@",eventIds,eventId];
            }
        }
    }
    
    if (eventIds == nil)
    {
        eventIds = @"";
    }
    //saving to prefernces
    [[NSUserDefaults standardUserDefaults] setValue:eventIds forKey:kCouldNotDeleteOrUnpublishEventIds];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

//Set and Get Normal asset
- (void)setViewedNormalAssetIds:(NSArray*)viewedNormalAssetIds
{
    NSString *viewedAssetIds = nil;
    for (id normalAssetId in viewedNormalAssetIds)
    {
        if ([normalAssetId isKindOfClass:[NSNumber class]])
        {
            if (viewedAssetIds == nil)
            {
                viewedAssetIds = [NSString stringWithFormat:@"%@",normalAssetId];
            }
            else
            {
                viewedAssetIds = [NSString stringWithFormat:@"%@_%@",viewedAssetIds,normalAssetId];
            }
        }
        else if ([normalAssetId isKindOfClass:[NSString class]])
        {
            if (viewedAssetIds == nil )
            {
                viewedAssetIds = normalAssetId;
            }
            else
            {
                viewedAssetIds = [NSString stringWithFormat:@"%@_%@",viewedAssetIds,normalAssetId];
            }
        }
    }
    
    if (viewedAssetIds == nil)
    {
        viewedAssetIds = @"";
    }
    //saving to prefernces
    [[NSUserDefaults standardUserDefaults] setValue:viewedAssetIds forKey:kViewedNormalAssetIds];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray*)getViewedNormalAssetIds
{
    NSString *viewedNormalAssetIds = [[NSUserDefaults standardUserDefaults] valueForKey:kViewedNormalAssetIds];
    NSArray *tempArray = [Constant getStringsValuesSeperatedByUnderscoreFromString:viewedNormalAssetIds];
    NSMutableArray *tempArray2 = [NSMutableArray array];
    for (NSString *assetId in tempArray)
    {
        if (assetId.length > 0)
        {
            [tempArray2 addObject:[NSNumber numberWithInteger:assetId.integerValue]];
        }
    }
    
    //make array unique, if duplicate exist.
    NSSet *removingDuplicates = [NSSet setWithArray:tempArray2];
    [tempArray2 removeAllObjects];
    [tempArray2 addObjectsFromArray:[removingDuplicates allObjects]];
    return tempArray2;
}

//Set and Get Event only asset
- (void)setViewedEventOnlyAssetIds:(NSArray*)viewedEventOnlyAssetIds
{
    NSString *viewedEOAAssetIds = nil;
    for (id normalAssetId in viewedEventOnlyAssetIds)
    {
        if ([normalAssetId isKindOfClass:[NSNumber class]])
        {
            if (viewedEOAAssetIds == nil)
            {
                viewedEOAAssetIds = [NSString stringWithFormat:@"%@",normalAssetId];
            }
            else
            {
                viewedEOAAssetIds = [NSString stringWithFormat:@"%@_%@",viewedEOAAssetIds,normalAssetId];
            }
        }
        else if ([normalAssetId isKindOfClass:[NSString class]])
        {
            if (viewedEOAAssetIds == nil)
            {
                viewedEOAAssetIds = normalAssetId;
            }
            else
            {
                viewedEOAAssetIds = [NSString stringWithFormat:@"%@_%@",viewedEOAAssetIds,normalAssetId];
            }
        }
    }
    
    if (viewedEOAAssetIds == nil)
    {
        viewedEOAAssetIds = @"";
    }
    //saving to prefernces
    [[NSUserDefaults standardUserDefaults] setValue:viewedEOAAssetIds forKey:kViewedEventOnlyAssetIds];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray*)getViewedEventOnlyAssetIds
{
    NSString *viewedEOAAssetIds = [[NSUserDefaults standardUserDefaults] valueForKey:kViewedEventOnlyAssetIds];
    NSArray *tempArray = [Constant getStringsValuesSeperatedByUnderscoreFromString:viewedEOAAssetIds];
    NSMutableArray *tempArray2 = [NSMutableArray array];
    for (NSString *assetId in tempArray)
    {
        if (assetId.length > 0)
        {
            [tempArray2 addObject:assetId];
        }
    }
    
    //make array unique, if duplicate exist.
    NSSet *removingDuplicates = [NSSet setWithArray:tempArray2];
    [tempArray2 removeAllObjects];
    [tempArray2 addObjectsFromArray:[removingDuplicates allObjects]];
    return tempArray2;
}

// fetch viewed asset
- (void)setViewedAssetFetchedFromCMSValue:(BOOL)value
{
    //saving to prefernces
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:value]
                                             forKey:kFetchViewedAssetsFromCMS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)hasViewedAssetFetchedFromCMS
{
    BOOL viewedAssetFetched = [[[NSUserDefaults standardUserDefaults] valueForKey:kFetchViewedAssetsFromCMS] boolValue];
    return viewedAssetFetched;
}

//EventV3 related Method.
- (void)setEventV3RequestMadeFromCMSValue:(BOOL)value
{
    //saving to Event3 Request status
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:value]
                                             forKey:kEventV3RequestBeenMadeFromCMS];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (BOOL)hasEventV3RequestBeenMadeFromCMS
{
    BOOL eventV3Request = [[[NSUserDefaults standardUserDefaults] valueForKey:kEventV3RequestBeenMadeFromCMS] boolValue];
    return eventV3Request;
}


@end
