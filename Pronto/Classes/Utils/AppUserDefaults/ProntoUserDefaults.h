//
//  ProntoUserDefaults.h
//  Pronto
//
//  Created by m-666346 on 10/05/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Asset.h"

@interface ProntoUserDefaults : NSObject

+ (ProntoUserDefaults *)userDefaults;

//CMS Last Synchornization for Assets
- (NSString*)getCMSLastSynchronization;
- (void)setCMSLastSynchronization:(NSString*)lastSynchronization;

//CMS Last Synchornization for Events
- (NSString*)getCMSLastSynchronizationForEvents;
- (void)setCMSLastSynchronizationForEvents:(NSString*)lastSynchronization;

//Fetching Access Token for AOFKit
- (NSString*)getAOFAccessToken;
- (void)setAOFAccessToken:(NSString*)accessToken;

//Fetching 511 id from AOFKit
- (NSString*)getAOF511Id;
- (void)setAOF511Id:(NSString*)k511Id;

//Current user id
- (NSString*)getCurrentUserID;
- (void)setCurrentUserID:(NSString*)userID;

//undeleted or unpublished asset ids
- (NSArray*)getCouldNotDeleteOrUnpulishAssetIds;
- (void)setCouldNotDeleteOrUnpulishAssetIds:(NSArray*)assetIDs;

//undeleted or unpublished events ids
- (NSArray*)getCouldNotDeleteOrUnpulishEventIds;
- (void)setCouldNotDeleteOrUnpulishEventIds:(NSArray*)assetIDs;

//AppVersion
- (void)saveAppVersion;
- (NSString*)getAppVersion;

//Previous Asset Array
- (void)setPreviousAsset:(Asset*)asset;
- (NSDictionary*)getPreviousAsset;

//Showing walkthrough initially
- (void)setIsAppFreshlyInstalled:(BOOL)showPermission;
- (BOOL)getIsAppFreshlyInstalled;

//Set and Get Normal asset
- (void)setViewedNormalAssetIds:(NSArray*)viewedNormalAssetIds;
- (NSArray*)getViewedNormalAssetIds;

//Set and Get Event only asset
- (void)setViewedEventOnlyAssetIds:(NSArray*)viewedEventOnlyAssetIds;
- (NSArray*)getViewedEventOnlyAssetIds;

// fetch viewed asset
- (void)setViewedAssetFetchedFromCMSValue:(BOOL)value;
- (BOOL)hasViewedAssetFetchedFromCMS;

//EventV3 related Method.
- (void)setEventV3RequestMadeFromCMSValue:(BOOL)value;
- (BOOL)hasEventV3RequestBeenMadeFromCMS;

//dlServer Encryption Time
- (NSDate*)getdlServerEncryptionTime;
- (void)setdlServerEncryptionTime:(NSDate*)time;

//dlServer Encryption Value
- (NSString*)getdlServerEncryptionValue;
- (void)setdlServerEncryptionValue:(NSString*)value;

//Set if user logged In/Refetched token
-(NSString *)isReadyToMakeServiceCalls;
-(void)isReadyToMakeServiceCalls:(int)value;
+(ProntoUserDefaults *)singleton;
@end
