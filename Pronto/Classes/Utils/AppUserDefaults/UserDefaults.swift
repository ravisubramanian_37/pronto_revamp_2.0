//
//  UserDefaults.swift
//  Pronto
//
//  Created by Ravi Subramanian on 12/10/21.
//  Copyright © 2021 Abbvie. All rights reserved.
//

import Foundation
import UIKit

class UserDefaultsModel: NSObject {
    private class func userDefaults() -> UserDefaults {
        return UserDefaults.standard
    }

    class func setObject(object: Any?, forKey: String) {
        userDefaults().set(object, forKey: forKey)
        userDefaults().synchronize()
    }

    class func removeObjectForKey(key: String) {
        userDefaults().removeObject(forKey: key)
        userDefaults().synchronize()
    }

    class func getSearchText() -> [String]? {
      return userDefaults().stringArray(forKey: RedesignConstants.searchText)
    }
}
