//
//  UIFont+FontDirectory.m
//  Pronto
//
//  Created by m-666346 on 02/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "UIFont+FontDirectory.h"

@implementation UIFont (FontDirectory)

+ (UIFont*)FontLatoRegularSize24
{
    return [UIFont fontWithName:@"Lato-Regular" size:24];
}

+ (UIFont*)FontLatoBoldSize24
{
    return [UIFont fontWithName:@"Lato-Bold" size:24];
}

+ (UIFont*)FontLatoRegularSize14
{
    return [UIFont fontWithName:@"Lato-Regular" size:14];
}

+ (UIFont*)FontLatoRegularSize11
{
    return [UIFont fontWithName:@"Lato-Regular" size:11];
}

+ (UIFont*)FontLatoRegularSize12
{
    return [UIFont fontWithName:@"Lato-Regular" size:12];
}

+ (UIFont*)FontLatoRegularSize13
{
    return [UIFont fontWithName:@"Lato-Regular" size:13];
}

+ (UIFont*)FontLatoRegularSize17
{
    return [UIFont fontWithName:@"Lato-Regular" size:17];
}

+ (UIFont*)FontLatoRegularSize16
{
    return [UIFont fontWithName:@"Lato-Regular" size:16];
}

+ (UIFont*)FontLatoBoldSize16
{
    return [UIFont fontWithName:@"Lato-Bold" size:16];
}

+ (UIFont*)FontLatoBoldSize12
{
    return [UIFont fontWithName:@"Lato-Bold" size:12];
}

+ (UIFont*)FontLatoRegularSize20
{
    return [UIFont fontWithName:@"Lato-Regular" size:20];
}

+ (UIFont*)FontLatoBoldSize14
{
    return [UIFont fontWithName:@"Lato-Bold" size:14];
}

+ (UIFont*)FontLatoBoldSize10
{
    return [UIFont fontWithName:@"Lato-Bold" size:10];
}

+ (UIFont*)FontLatoRegularSize10
{
    return [UIFont fontWithName:@"Lato-Regular" size:10];
}

@end
