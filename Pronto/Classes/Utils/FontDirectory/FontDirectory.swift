//
//  UIColor+ColorDirectory.h
//  Pronto
//
//  Created by m-666346 on 21/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

import Foundation
import UIKit

extension UIFont
{
    class func LatoFont24Regular()-> UIFont
    {
        return UIFont(name: "Lato", size: 24)!
    }
    
    class func LatoFont14Regular()-> UIFont
    {
        return UIFont(name: "Lato", size: 14)!
    }
    
    class func SystemFont14Regular()-> UIFont
    {
        return UIFont.systemFont(ofSize: 14.0, weight: .regular)
    }
    
    class func SystemFont12Regular()-> UIFont
    {
        return UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }
    
    class func SystemFont14Semibold()-> UIFont
    {
        return UIFont.systemFont(ofSize: 14.0, weight: .semibold)
    }
}
