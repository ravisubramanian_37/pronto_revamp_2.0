//
//  UIFont+FontDirectory.h
//  Pronto
//
//  Created by m-666346 on 02/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (FontDirectory)

+ (UIFont*)FontLatoRegularSize24;
+ (UIFont*)FontLatoBoldSize24;
+ (UIFont*)FontLatoRegularSize14;
+ (UIFont*)FontLatoRegularSize17;
+ (UIFont*)FontLatoBoldSize12;
+ (UIFont*)FontLatoRegularSize11;
+ (UIFont*)FontLatoRegularSize12;
+ (UIFont*)FontLatoRegularSize13;
+ (UIFont*)FontLatoRegularSize20;
+ (UIFont*)FontLatoRegularSize16;
+ (UIFont*)FontLatoBoldSize16;
+ (UIFont*)FontLatoBoldSize14;
+ (UIFont*)FontLatoBoldSize10;
+ (UIFont*)FontLatoRegularSize10;
@end
