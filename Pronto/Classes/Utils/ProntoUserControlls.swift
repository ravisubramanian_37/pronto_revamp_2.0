//
//  prontoUserControlls.swift
//  Pronto
//
//  Created by osmarogo on 4/18/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//

import Foundation

@objc enum MainView: Int {
    case library
    case myBriefcase
}

@objc class Search: NSObject {
    
    @objc var myLibrarySearh = ""
    @objc var myBriefcaseSearh = ""
    @objc var currentView = 0
    
    @objc var name: String?
    
    @objc func getTitle() -> String {
        switch ZMProntoManager.sharedInstance.currentLibraryView {
        case 0:
            return myLibrarySearh
        case 1:
            return myBriefcaseSearh
        default:
            return ""
        }
    }
    
    @objc func setTitle(_ title:String){
        switch ZMProntoManager.sharedInstance.currentLibraryView {
        case 0:
            myLibrarySearh = title
        case 1:
            myBriefcaseSearh = title
        default:
            break
        }
    }
    
    @objc func resetTitleToCurrentView(){
        switch ZMProntoManager.sharedInstance.currentLibraryView {
        case 0:
            myLibrarySearh = ""
        case 1:
            myBriefcaseSearh = ""
        default:
            break
        }
    }
    
    @objc func reset() {
        myLibrarySearh = ""
        myBriefcaseSearh = ""
    }
}
