//
//  NSString+Util.h
//  Pronto
//
//  Created by Sebastian Romero on 2/4/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Util)

/**
 * Url encode the string and returns the validated string
 * @return NSString
 **/
-(NSString *) urlEncode;


/**
 * Checks if the given string is contained on the NSString
 * @param NSString string
 **/
-(BOOL)contains:(NSString *)string;

- (BOOL)isEmpty;

@end
