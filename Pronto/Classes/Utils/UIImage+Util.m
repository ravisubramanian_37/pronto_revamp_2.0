//
//  UIImage+Util.m
//  Pronto
//
//  Created by Sebastian Romero on 2/10/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "UIImage+Util.h"
#import "ZMProntoTheme.h"

@implementation UIImage (Util)


- (UIImage *)imageWithColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextClipToMask(context, rect, self.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


+ (UIImage *)imageWithName:(NSString *)name scaledToSize:(CGSize)newSize
{
    UIImage *image = [UIImage imageNamed:name];
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    id <ZMProntoTheme> theme = [ZMProntoTheme sharedTheme];
    return [theme brightImage:newImage];
}

@end
