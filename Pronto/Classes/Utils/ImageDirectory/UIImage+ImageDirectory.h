//
//  UIImage+ImageDirectory.h
//  Pronto
//
//  Created by m-666346 on 21/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageDirectory)

+ (UIImage*)GetCheckListImage;
+ (UIImage*)GetSelectedCheckListImage;
+ (UIImage*)GetExportIconImage;
+ (UIImage*)GetExportIconSelectedImage;
+ (UIImage*)GetInfoIconImage;
+ (UIImage*)GetInfoIconSelectedImage;
+ (UIImage*)GetListIconImage;
+ (UIImage*)GetThumbnailIconImage;
+ (UIImage*)GetFavIconImage;
+ (UIImage*)GetFavIconSelectedImage;
+ (UIImage*)GetCloseIconImage;
+ (UIImage*)GetOverlayIconImage;
+ (UIImage*)GetCalendarIconImage;
+ (UIImage*)GetEventDownloadIconImage;
+ (UIImage*)GetLocationIconImage;
+ (UIImage*)GetMessageSelectIconImage;
+ (UIImage*)GetMessageNormalIconImage;
+ (UIImage*)GetPrintAssetDetailIconImage;
+ (UIImage*)GetBlueCloseIconImage;
+ (UIImage*)GetGrayBackIconImage;
+ (UIImage*)GetCalendarDetailTimeIcon;
+ (UIImage*)GetCalendarDetailPDFIcon;
+ (UIImage*)GetCalendarDetailLocationIcon;
+ (UIImage*)GetCalendarDetailReadIcon;
+ (UIImage*)GetPreferencesIcon;
+ (UIImage*)GetCalendarDetailPDFIconSelected;
+ (UIImage*)GetRefreshImage;
+ (UIImage*)GetArrowDownIcon;
+ (UIImage*)GetArrowRightIcon;
+ (UIImage*)GetPageControlDotIcon;
+ (UIImage*)GetPageControlDotIconSelected;
+ (UIImage*)GetLoadingRefreshIcon;
+ (UIImage*)GetLogoutWithTextIcon;
@end
