//
//  UIImage+ImageDirectory.m
//  Pronto
//
//  Created by m-666346 on 21/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "UIImage+ImageDirectory.h"

@implementation UIImage (ImageDirectory)

+ (UIImage*)GetCheckListImage
{
    return [UIImage imageNamed:@"checkList"];
}

+ (UIImage*)GetSelectedCheckListImage
{
    return [UIImage imageNamed:@"selectedChecklist"];
}

+ (UIImage*)GetExportIconImage
{
    return [UIImage imageNamed:@"Export_redesign"];
}

+ (UIImage*)GetExportIconSelectedImage
{
    return [UIImage imageNamed:@"export_selected"];
}

+ (UIImage*)GetInfoIconImage
{
    return [UIImage imageNamed:@"Info_redesign"];
}

+ (UIImage*)GetInfoIconSelectedImage
{
    return [UIImage imageNamed:@"info_selected"];
}

+ (UIImage*)GetListIconImage
{
    return [UIImage imageNamed:@"listIcon"];
}

+ (UIImage*)GetThumbnailIconImage
{
    return [UIImage imageNamed:@"thumbnailIcon"];
}

+ (UIImage*)GetFavIconImage
{
    return [UIImage imageNamed:@"Favo_unselected"];
}

+ (UIImage*)GetFavIconSelectedImage
{
    return [UIImage imageNamed:@"Favo_selected"];
}

+ (UIImage*)GetCloseIconImage
{
    return [UIImage imageNamed:@"closeBtn"];
}

+ (UIImage*)GetOverlayIconImage
{
    return [UIImage imageNamed:@""];
}

+ (UIImage*)GetCalendarIconImage
{
    return [UIImage imageNamed:@"calendarIcon"];
}

+ (UIImage*)GetEventDownloadIconImage
{
    return [UIImage imageNamed:@"EventDownloadIcon"];
}

+ (UIImage*)GetLocationIconImage
{
    return [UIImage imageNamed:@"locationIcon"];
}

+ (UIImage*)GetMessageSelectIconImage
{
    return [UIImage imageNamed:@"messageSelectIcon"];
}

+ (UIImage*)GetMessageNormalIconImage
{
    return [UIImage imageNamed:@"messagesIcon"];
}

+ (UIImage*)GetPrintAssetDetailIconImage
{
     return [UIImage imageNamed:@"printIcon"];
}

+ (UIImage*)GetBlueCloseIconImage
{
    return [UIImage imageNamed:@"deletebutton"];
}

+ (UIImage*)GetGrayBackIconImage
{
    return [UIImage imageNamed:@"left_arrow_gray"];
}

+ (UIImage*)GetCalendarDetailTimeIcon
{
    return [UIImage imageNamed:@"calendarDetailTimeIcon"];
}

+ (UIImage*)GetCalendarDetailPDFIcon
{
    return [UIImage imageNamed:@"calendarDetailPDFIcon"];
}

+ (UIImage*)GetCalendarDetailPDFIconSelected
{
    return [UIImage imageNamed:@"calendarDetailPDFIconSelected"];
}

+ (UIImage*)GetCalendarDetailLocationIcon
{
    return [UIImage imageNamed:@"calendarDetailLocationIcon"];
}

+ (UIImage*)GetCalendarDetailReadIcon
{
    return [UIImage imageNamed:@"tickIconGreen"];
}

+ (UIImage*)GetPreferencesIcon
{
    return [UIImage imageNamed:@"preferencesIcon"];
}

+ (UIImage*)GetRefreshImage
{
    return [UIImage imageNamed:@"refresh"];
}

+ (UIImage*)GetArrowDownIcon
{
    return [UIImage imageNamed:@"eventGlobalSearchArrowDown"];
}

+ (UIImage*)GetArrowRightIcon
{
    return [UIImage imageNamed:@"eventGlobalSearchArrowRight"];
}

+ (UIImage*)GetPageControlDotIcon
{
    return [UIImage imageNamed:@"pageControlDot"];
}

+ (UIImage*)GetPageControlDotIconSelected
{
    return [UIImage imageNamed:@"pageControlDotSelected"];
}
    
+ (UIImage*)GetLoadingRefreshIcon
{
    return [UIImage imageNamed:@"loadingRefreshIcon"];
}

+ (UIImage*)GetLogoutWithTextIcon
{
    return [UIImage imageNamed:@"logoutIconWithText"];
}

@end
