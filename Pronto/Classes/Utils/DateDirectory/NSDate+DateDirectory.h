//
//  NSDate+DateDirectory.h
//  Pronto
//
//  Created by m-666346 on 20/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (DateDirectory)

+ (NSString*)GetWeekdayForDate:(NSDate*)date; // Tue
+ (NSDate*)GetDateFromDateString:(NSString*)dateString;
+ (NSDate*)GetDateForCoverFlowResponseFromDateString:(NSString*)dateString;
+ (NSInteger)GetMonthNameFromDate:(NSDate*)date;
+ (NSInteger)GetDateFromDate:(NSDate*)date; // 25
+ (NSInteger)GetYearFromDate:(NSDate*)date;
+ (NSString*)GetMonthInMMMFormateFromDate:(NSDate*)date; ////MMM
+ (NSString*)GetMonthInMMMMFormateFromDate:(NSDate*)date; //MMMM
+ (BOOL)isDateInWeekend:(NSDate*)date;
+ (NSDate*)findEndDateFromStartDate:(NSDate*)date excludingWeekendsByAddingDay:(NSInteger)day;
+ (NSArray*)GetAllDatesLiesBetween:(NSDate*)startDate andEndDate:(NSDate*)endDate;
+ (BOOL)isDate1:(NSDate*)date1 lessThanDate:(NSDate*)date2;
+ (BOOL)isDate1:(NSDate*)date1 greaterThanDate:(NSDate*)date2;
+ (BOOL)isDate1:(NSDate*)date1 equalToDate:(NSDate*)date2;
+ (NSString*)GetDateStringInFormateDDMMYYYYFromDate:(NSDate*)date;
+ (NSString*)GetDateStringFromFormaterWithUTC:(NSDate*)date;
+ (BOOL)isToday:(NSDate*)date;
+ (NSDate*)GetDateByAddingDayComponent:(NSInteger)dayComponent inDate:(NSDate*)date;
+ (BOOL)isPastDate:(NSDate*)date;
+ (BOOL)isDate1:(NSDate*)date1 lessThanEqualToDate:(NSDate*)date2;
+ (BOOL)isDate1:(NSDate*)date1 greaterThanEqualToDate:(NSDate*)date2;
+ (NSDate*)GetStartDateOfMonthFromDate:(NSDate*)date;
+ (NSDate*)GetEndDateOfMonthFromDate:(NSDate*)date;
+ (NSInteger)GetNoOfWeeksBetweenDates:(NSDate*)date1 and:(NSDate*)date2;
+ (NSDate*)GetDateSameAsStringFromDateString:(NSString*)dateString;
+ (BOOL)isDate1:(NSDate*)date1 fallsInSameYearAsDate2:(NSDate*)date2;

+ (NSUInteger)GetTimeStampOfDate:(NSDate*)date;

+ (NSString*)getFormattedDateStringForGlobalSearchFromDate:(NSDate*)date;
+ (NSString*)GetWeekdayInEEEEFormateForDate:(NSDate*)date;
+ (NSDate*)GetDateInLocalTimeZoneFromUTCDateExactAsUTCDate:(NSDate*)date;

#pragma mark -Week Calendar View Starts
// Below methods are related to Week calendar view.
// Change it with caution
+ (NSDate*)findEndDateFromStartDate:(NSDate*)date byAddingDay:(NSInteger)day;
+ (NSDate*)GetNearestPreviousSundayFromDate:(NSDate*)date;
+ (NSDate*)GetNearestNextSaturdayFromDate:(NSDate*)date;
+ (NSInteger)GetDateInUTCFromDate:(NSDate*)date;
+ (NSArray*)GetAllDatesInUTCLiesBetween:(NSDate*)startDate andEndDate:(NSDate*)endDate;
+ (NSString*)GetWeekdayInUTCFormateForDate:(NSDate*)date;
+ (BOOL)isDateWeekend:(NSDate*)date;
+ (NSDate*)GetDateInLocalTimeZoneFromUTCDate:(NSDate*)date;
+ (NSDate*)GetDateUTCTimeZoneFromLocalTimeZoneDate:(NSDate*)date;
+ (NSDate*)GetDateInLocalTimeZoneFromUTCDateForDayLightSavings:(NSDate*)date;
+(NSString*)GetDateForCalendatDetailTableHeader:(NSDate*)date;
+ (NSString*)Get12HoursTime:(NSString*)time;
+ (NSString*)getTimeFromDate:(NSDate*)date;
+ (NSString*)getDateAsString:(NSDate*)date;
#pragma mark -Week Calendar View Ends

+ (BOOL)isUTCDate1:(NSDate*)date1 lessThanDate:(NSDate*)date2 forCalendar:(NSCalendar*)calendar;

@end

