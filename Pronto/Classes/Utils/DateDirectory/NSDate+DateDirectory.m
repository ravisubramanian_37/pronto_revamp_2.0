//
//  NSDate+DateDirectory.m
//  Pronto
//
//  Created by m-666346 on 20/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "NSDate+DateDirectory.h"

@implementation NSDate (DateDirectory)

+(NSString*)GetDateForCalendatDetailTableHeader:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [self GetDefaultDateFormatter];
    dateFormatter.dateFormat = @"EEEE dd, yyyy";
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    return formattedDateString;
}

+ (NSString*)GetWeekdayForDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEE";
    NSString *weekday = [dateFormatter stringFromDate:date];
    return weekday;
}

+ (NSDate*)GetDateFromDateString:(NSString*)dateString
{
    //it is place holder method, if in future all the dates will come in same date
    //formate it will be used. Otherwise, for each response/Date Formater need to create new methods
    return [NSDate date];
}



+(NSDate*)GetDateForCoverFlowResponseFromDateString:(NSString*)dateString
{
    if (dateString == nil) { return nil; }
    
    //"2018-08-21 05:00:00"
    NSDateFormatter *dateFormatter  = [[NSDateFormatter alloc] init];;
    dateFormatter.dateFormat        = @"YYYY-MM-dd";
    //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *date    = [dateFormatter dateFromString:dateString];
    return date;
}

+ (NSDate*)GetDateSameAsStringFromDateString:(NSString*)dateString
{

    if (dateString == nil) { return nil; }

    NSDateFormatter *dateFormatter  = [[NSDateFormatter alloc] init];;
    dateFormatter.dateFormat        = @"MM/dd/yyyy";
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *date    = [dateFormatter dateFromString:dateString];
    return date;

}

+ (NSInteger)GetMonthNameFromDate:(NSDate*)date
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    [components setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSInteger month = [components month];
    return month;
}

+ (BOOL)isDate1:(NSDate*)date1 fallsInSameYearAsDate2:(NSDate*)date2
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDateComponents *component1 = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date1];
    [component1 setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSInteger year1 = [component1 year];
    
    NSDateComponents *component2 = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date2];
    [component2 setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSInteger year2 = [component2 year];
    if (year1 == year2)
    {
        return YES;
    }
    return NO;
}

+ (NSInteger)GetDateFromDate:(NSDate*)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:date];
    return [components day];
}

+ (NSInteger)GetYearFromDate:(NSDate*)date
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSInteger year = [calendar component:NSCalendarUnitYear fromDate:date];
    return year;
}

+ (NSString*)GetMonthInMMMFormateFromDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [self GetDefaultDateFormatter];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"MMM"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString*)GetMonthInMMMMFormateFromDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [self GetDefaultDateFormatter];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"MMMM"];
    return [dateFormatter stringFromDate:date];
}

+ (BOOL)isDateInWeekend:(NSDate*)date
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    return [calendar isDateInWeekend:date];
}

+ (NSDateFormatter*) GetDefaultDateFormatter
{
    NSDateFormatter *defaultDateFormatter = [[NSDateFormatter alloc] init];;
    return defaultDateFormatter;
}

+ (NSCalendar*) GetDefaultCalendar
{
    NSCalendar *defaultCalendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];;
    return defaultCalendar;
}

+ (NSDateComponents*) GetDefaultComponents
{
    NSDateComponents *defaultComponents = [[NSDateComponents alloc] init];;
    return defaultComponents;
}

+ (BOOL)isDate1:(NSDate*)date1 equalToDate:(NSDate*)date2
{
    //Explicit comparison
    NSCalendar *calendar    = [self GetDefaultCalendar];
    NSDateComponents *component1 = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                               fromDate:date1];
    
    NSDateComponents *component2 = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                               fromDate:date2];
    if (([component1 day]   == [component2 day]) &&
        ([component1 month] == [component2 month]) &&
        ([component1 year]  == [component2 year]))
    {
        return YES;
    }
    return NO;
}

+ (BOOL)isDate1:(NSDate*)date1 greaterThanDate:(NSDate*)date2
{
    NSCalendar *calendar    = [self GetDefaultCalendar];
    NSDateComponents *component1 = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                               fromDate:date1];
    
    NSDateComponents *component2 = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                               fromDate:date2];
    
    if ((([component1 year] > [component2 year])) ||
        (([component1 year] >= [component2 year]) && [component1 month] > [component2 month]) ||
        (([component1 year] >= [component2 year]) && [component1 month] >= [component2 month] && [component1 day] > [component2 day]))
    {
        return YES;
        
    }
    return NO;
}

+ (BOOL)isDate1:(NSDate*)date1 lessThanDate:(NSDate*)date2
{
    //Explicit comparison
    NSCalendar *calendar    = [self GetDefaultCalendar];
    NSDateComponents *component1 = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                               fromDate:date1];
    NSDateComponents *component2 = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                               fromDate:date2];
    
    if ((([component1 year] < [component2 year])) ||
        (([component1 year] <= [component2 year]) && [component1 month] < [component2 month]) ||
        (([component1 year] <= [component2 year]) && [component1 month] <= [component2 month] && [component1 day] < [component2 day]))
    {
        return YES;
        
    }
    return NO;
}

+ (BOOL)isUTCDate1:(NSDate*)date1 lessThanDate:(NSDate*)date2 forCalendar:(NSCalendar*)calendar
{
    //Explicit comparison

    NSDateComponents *component1 = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                               fromDate:date1];
    NSDateComponents *component2 = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                               fromDate:date2];
    
    if ((([component1 year] < [component2 year])) ||
        (([component1 year] <= [component2 year]) && [component1 month] < [component2 month]) ||
        (([component1 year] <= [component2 year]) && [component1 month] <= [component2 month] && [component1 day] < [component2 day]))
    {
        return YES;
    }
    return NO;
}

+ (BOOL)isDate1:(NSDate*)date1 lessThanEqualToDate:(NSDate*)date2
{
    BOOL isEqualTo = [self isDate1:date1 equalToDate:date2];
    BOOL isLessThan = [self isDate1:date1 lessThanDate:date2];
    if(isLessThan == YES || isEqualTo == YES)
    {
        return YES;
    }
    return NO;
}

+ (BOOL)isDate1:(NSDate*)date1 greaterThanEqualToDate:(NSDate*)date2
{
    BOOL isEqualTo = [self isDate1:date1 equalToDate:date2];
    BOOL isGreaterThan = [self isDate1:date1 greaterThanDate:date2];
    if(isGreaterThan == YES || isEqualTo == YES)
    {
        return YES;
    }
    return NO;
}

+ (NSArray*)GetAllDatesLiesBetween:(NSDate*)startDate andEndDate:(NSDate*)endDate
{
    if ([self isDate1:startDate equalToDate:endDate] == YES)
    {
        return @[startDate];
    }
    
    NSMutableArray *dates = [@[startDate] mutableCopy];
    NSCalendar *gregorianCalendar = [self GetDefaultCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    for (int i = 1; i < components.day; ++i)
    {
        NSDateComponents *newComponents = [self GetDefaultComponents];
        newComponents.day = i;
        NSDate *date = [gregorianCalendar dateByAddingComponents:newComponents
                                                          toDate:startDate
                                                         options:0];
        [dates addObject:date];
    }
    
    [dates addObject:endDate];
    return dates;
}

+ (NSString*)GetDateStringInFormateDDMMYYYYFromDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [self GetDefaultDateFormatter]; //YYYY-MM-dd hh:mm:ss
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString*)GetDateStringFromFormaterWithUTC:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [self GetDefaultDateFormatter]; //YYYY-MM-dd hh:mm:ss
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    return [dateFormatter stringFromDate:date];
}

+ (NSDate*)findEndDateFromStartDate:(NSDate*)date byAddingDay:(NSInteger)day
{
    if (day <= 0) { return nil;}
    
    NSInteger dayCount  = 0;
    NSInteger counter   = 0;
    NSDate *endDate = [NSDate date];
    NSDateComponents *component = [self GetDefaultComponents];
    NSCalendar *calendar        = [self GetDefaultCalendar];
    
    while (counter <= day)
    {
        [component setDay:dayCount];
        endDate = [calendar dateByAddingComponents:component toDate:date
                                           options:0];
        counter++;
        dayCount++;
    }
    
    return endDate;
}

+ (BOOL)isDateSaturDay:(NSDate*)date
{
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday
                                                                  fromDate:date];
    [component setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    if ([component weekday] == 7)
    {
        return YES;
    }
    return NO;
}

+ (BOOL)isDateSunDay:(NSDate*)date
{
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday
                                                                  fromDate:date];
    [component setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    if ([component weekday] == 1)
    {
        //it is already saturday.
        return YES;
    }
    return NO;
}

+ (NSInteger)differenceBetweenDateFrom:(NSDate*)date1 to:(NSDate*)date2
{
    NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    NSInteger numberOfDays = secondsBetween / 86400;
    return numberOfDays;
}

+ (NSDate*)GetStartDateOfMonthFromDate:(NSDate*)date
{
    NSCalendar* calendar = [self GetDefaultCalendar];
    NSDateComponents* comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekOfMonth|NSCalendarUnitWeekday|NSCalendarUnitDay fromDate:date]; // Get necessary date components
    
    [comps setMonth:[comps month]];
    
    [comps setDay:0];
    NSDate *tDateMonth = [calendar dateFromComponents:comps];
    return tDateMonth;
}

+ (NSDate*)GetEndDateOfMonthFromDate:(NSDate*)date
{
    NSCalendar* calendar = [self GetDefaultCalendar];
    NSDateComponents* comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekOfMonth|NSCalendarUnitWeekday|NSCalendarUnitDay fromDate:date]; // Get necessary date components
    
    // set last of month
    
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    NSDate *tDateMonth = [calendar dateFromComponents:comps];
    return tDateMonth;
}

+ (NSInteger)GetNoOfWeeksBetweenDates:(NSDate*)date1 and:(NSDate*)date2
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    int unitFlags = NSCalendarUnitWeekOfMonth;
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:date1  toDate:date2 options:0];
    NSInteger weeksInBetween = [dateComponents weekOfMonth];
    return weeksInBetween;
}

+ (NSUInteger)GetTimeStampOfDate:(NSDate*)date
{
    return [date timeIntervalSince1970];
}

+ (NSString*)getFormattedDateStringForGlobalSearchFromDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd, YYYY"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+ (NSString*)GetWeekdayInEEEEFormateForDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEEE";
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *weekday = [dateFormatter stringFromDate:date];
    return weekday;
}






#pragma mark -Week Calendar View Starts
// Below methods related to Week calendar view.
// Change it with caution
+ (NSDate*)findEndDateFromStartDate:(NSDate*)date excludingWeekendsByAddingDay:(NSInteger)day
{
    if (day <= 0) { return nil;}
    
    NSInteger dayCount  = 0;
    NSInteger counter   = 0;
    NSDate *endDate = [NSDate date];
    NSDateComponents *component = [self GetDefaultComponents];
    NSCalendar *calendar        = [self GetDefaultCalendar];
    
    while (counter < day)
    {
        [component setDay:dayCount];
        endDate = [calendar dateByAddingComponents:component toDate:date
                                           options:0];
        if ([self isDateInWeekend:endDate] == NO)
        {
            counter++;
        }
        dayCount++;
    }
    
    return endDate;
}


+ (NSString*)GetWeekdayInUTCFormateForDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEE";
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *weekday = [dateFormatter stringFromDate:date];
    return weekday;
}

+ (NSDate*)GetNearestPreviousSundayFromDate:(NSDate*)date
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDateComponents *component = [calendar components:NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay
                                                                  fromDate:date];
    [component setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSUInteger day = [component weekday];
    if (day == 1)
    {
        //it is already Sunday.
        return date;
    }

    NSTimeInterval  timeInterval = ((3600 * 24) * (day - 1));
    NSDate *previousSunday = [date dateByAddingTimeInterval: -timeInterval]; // next two days
    return previousSunday;
}

+ (NSDate*)GetNearestNextSaturdayFromDate:(NSDate*)date
{
    //is it already saturday.
    if ([self isDateSaturDay:date])
    {
        return date;
    }
    
    // if we send weekend like friday date, we will get friday date itself, no change
    NSCalendar *calendar = [self GetDefaultCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *weekend = [[NSDate alloc]init];
    NSTimeInterval timeInterval = 0;
    //Set region
    
    //Got Friday, Fetching saturday
    [calendar nextWeekendStartDate:&weekend interval:&timeInterval options:NSCalendarMatchFirst afterDate:date];
    NSDate *nearestSaturDay = [weekend dateByAddingTimeInterval:3600 * 24];
    
    return nearestSaturDay;
}

+ (BOOL)isToday:(NSDate*)date
{
    if ([self isDate1:date equalToDate:[NSDate date]])
    {
        return YES;
    }
    return NO;
}

+ (NSDate*)GetDateByAddingDayComponent:(NSInteger)dayComponent inDate:(NSDate*)date
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    NSDateComponents *component = [[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                                  fromDate:date];
    [component setDay:dayComponent];
     NSDate *anotherDate = [calendar dateByAddingComponents:component toDate:date
                                                    options:0];
    
    return anotherDate;
   
}

+ (NSInteger)GetDateInUTCFromDate:(NSDate*)date
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDateComponents *components =
    [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                fromDate:date];
    [components setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    return [components day];
}

+ (NSArray*)GetAllDatesInUTCLiesBetween:(NSDate*)startDate andEndDate:(NSDate*)endDate
{
    if ([self isDate1:startDate equalToDate:endDate] == YES)
    {
        return @[startDate];
    }
    
    NSMutableArray *dates = [@[startDate] mutableCopy];
    NSCalendar *gregorianCalendar = [self GetDefaultCalendar];
    [gregorianCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    [components  setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    for (int i = 1; i <= components.day; ++i)
    {
        NSDateComponents *newComponents = [self GetDefaultComponents];
        [newComponents  setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        newComponents.day = i;
        NSDate *date = [gregorianCalendar dateByAddingComponents:newComponents
                                                          toDate:startDate
                                                         options:0];
        if (![dates containsObject:date])
        {
            [dates addObject:date];
        }
    }
    if ((dates.count % 7 == 0) == NO) // Logic by hit and trail.
    {
        if (![dates containsObject:endDate])
        {
            [dates addObject:endDate];
        }
    }
    
    return dates;
}

+ (BOOL)isDateWeekend:(NSDate*)date
{
    NSCalendar *calendar = [self GetDefaultCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [calendar isDateInWeekend:date];
}

+ (NSDate*)GetDateInLocalTimeZoneFromUTCDate:(NSDate*)date
{
    //utc to local time zone
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *dateInLocalTimeZone    = [date dateByAddingTimeInterval:timeZoneSeconds];
    return dateInLocalTimeZone;
}

+ (NSDate*)GetDateInLocalTimeZoneFromUTCDateForDayLightSavings:(NSDate*)date
{
    //utc time for daylight savings time zone
    NSDate *dateInLocalTimeZone    = [date dateByAddingTimeInterval:3600];
    return dateInLocalTimeZone;
}

+ (NSDate*)GetDateInLocalTimeZoneFromUTCDateExactAsUTCDate:(NSDate*)date
{
    //utc to local time zone
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *dateInLocalTimeZone    = [date dateByAddingTimeInterval:-timeZoneSeconds];
    return dateInLocalTimeZone;
}

+ (NSDate*)GetDateUTCTimeZoneFromLocalTimeZoneDate:(NSDate*)date
{
    //local time zone to utc.
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *dateInUTCTimeZone      = [date dateByAddingTimeInterval: -timeZoneSeconds];
    return dateInUTCTimeZone;
}

+ (NSString*)Get12HoursTime:(NSString*)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm";
    NSDate *date = [dateFormatter dateFromString:time];
    
    dateFormatter.dateFormat = @"hh:mm a";
    NSString *pmamDateString = [dateFormatter stringFromDate:date];
    return pmamDateString;
}

+ (NSString*)getTimeFromDate:(NSDate*)date
{
    NSDate * now = date;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm"];
    NSString *timeString = [outputFormatter stringFromDate:now];
    return timeString;
}

+ (NSString*)getDateAsString:(NSDate*)date
{
    NSDate * now = date;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [outputFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *timeString = [outputFormatter stringFromDate:now];
    return timeString;
}

+ (BOOL)isPastDate:(NSDate*)date
{
    if ([NSDate isDate1:date lessThanDate:[NSDate date]]) // comparring between current date and date provided.
    {
        return YES;
    }
    return NO;
}
#pragma mark -Week Calendar View Ends


@end
















