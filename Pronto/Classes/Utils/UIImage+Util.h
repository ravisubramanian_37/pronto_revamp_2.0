//
//  UIImage+Util.h
//  Pronto
//
//  Created by Sebastian Romero on 2/10/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Util)

/**
 *  Tints the image with the given color
 *  @param color
 *  @return reference of the image
 */
- (UIImage *)imageWithColor:(UIColor *)color;

/**
 *  Loads an image and scales up to a given size
 *  @param name image path
 *  @param newSize new size of the image
 *  @return reference of the image
 */
+ (UIImage *)imageWithName:(NSString *)name scaledToSize:(CGSize)newSize;

@end
