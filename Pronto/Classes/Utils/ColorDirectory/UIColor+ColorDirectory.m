//
//  UIColor+ColorDirectory.m
//  Pronto
//
//  Created by m-666346 on 21/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "UIColor+ColorDirectory.h"

@implementation UIColor (ColorDirectory)

+(UIColor*)GetColorFromHexValue:(int)hexValue
{
    return [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 \
                           green:((float)((hexValue & 0x00FF00) >>  8))/255.0 \
                            blue:((float)((hexValue & 0x0000FF) >>  0))/255.0 \
                           alpha:1.0];
}

+(UIColor*)GetColorFromHexValue:(int)hexValue andOpacity:(CGFloat)opacity
{
    return [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 \
                           green:((float)((hexValue & 0x00FF00) >>  8))/255.0 \
                            blue:((float)((hexValue & 0x0000FF) >>  0))/255.0 \
                           alpha:opacity];
}

+ (UIColor *)ColorFromHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    if ([hexString hasPrefix:@"0x"] == YES)
    {
        [scanner setScanLocation:2];
    }
    else if ([hexString hasPrefix:@"#"] == YES)
    {
        [scanner setScanLocation:1];
    }
    else
    {
        // as it is.
    }
    // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(UIColor*)GetColorFromHexValue2d2926
{
   return  [UIColor GetColorFromHexValue:0x2d2926];
}

+(UIColor*)GetColorFromHexValue817f7d
{
    return  [UIColor GetColorFromHexValue:0x817f7d];
}

+(UIColor*)GetColorFromHexValue36a2c6
{
    return  [UIColor GetColorFromHexValue:0x36a2c6];
}

+(UIColor*)GetColorFromHexValued6dbe8
{
    return  [UIColor GetColorFromHexValue:0xd6dbe8];
}

+ (UIColor*)GetColorFromHexValue8E8E8E
{
    return  [UIColor GetColorFromHexValue:0x8E8E8E];
}

+(UIColor*)GetTransparentOverlayColor
{
    return [UIColor colorWithWhite:0.0 alpha:0.70];
}

+ (UIColor*)GetColorFromHexValueaba9a8
{
    return  [UIColor GetColorFromHexValue:0xaba9a8];
}

+ (UIColor*)GetColorFromHexValuef2f2f2
{
     return  [UIColor GetColorFromHexValue:0xf2f2f2];
}

+ (UIColor*)GetColorFromHexValueFFFFFF
{
    return  [UIColor GetColorFromHexValue:0xFFFFFF];
}

+ (UIColor*)GetColorFromHexValue000000
{
    return  [UIColor GetColorFromHexValue:0x000000];
}

+ (UIColor*)GetColorFromHexValue7d91b4
{
    return  [UIColor GetColorFromHexValue:0x7d91b4];
}

// BLUE COLOR
+ (UIColor*)GetColorFromHexValue0082ba
{
    return  [UIColor GetColorFromHexValue:0x0082ba];
}

+(UIColor*)GetColorFromHexString:(NSString*)hexString
{
    return  [UIColor ColorFromHexString:hexString];
}

+ (UIColor*)GetColorFromHexValue417BAF
{
    return [UIColor GetColorFromHexValue:0x417BAF];
}

+ (UIColor*)GetColorFromHexValueE6EDF4
{
    return  [UIColor GetColorFromHexValue:0xE6EDF4];
}


+ (UIColor*)GetColorFromHexValueEEF3F7
{
    return  [UIColor GetColorFromHexValue:0xEEF3F7];
}

+ (UIColor*)GetColorFromHexValueB0AFB0
{
    return  [UIColor GetColorFromHexValue:0xB0AFB0];
}

+ (UIColor*)GetColorFromHexValueE7EDF3
{
    return  [UIColor GetColorFromHexValue:0xE7EDF3];
}

// Gold Label COLOR
+ (UIColor*)GetColorFromHexValueaa8b40
{
    return  [UIColor GetColorFromHexValue:0xaa8b40];
}

// Gold View COLOR
+ (UIColor*)GetColorFromHexValuefaf1de
{
    return  [UIColor GetColorFromHexValue:0xfaf1de];
}
+ (UIColor*)GetColorFromHexValueDAE4EF
{
     return  [UIColor GetColorFromHexValue:0xDAE4EF];
}
    
+ (UIColor*)GetColorFromHexValue1AC9A8
{
    return  [UIColor GetColorFromHexValue:0x1AC9A8];
}

+ (UIColor*)GetColorFromHexValue0081B9
{
    return  [UIColor GetColorFromHexValue:0x0081B9];
}

+ (UIColor*)GetColorFromHexValue417BAE
{
    return  [UIColor GetColorFromHexValue:0x417BAE];
}

+ (UIColor*)GetColorFromHexValue00809f
{
    return [UIColor GetColorFromHexValue:0x00809f];
}

+ (UIColor*)GetColorFromHexValueD9E3EE
{
    return  [UIColor GetColorFromHexValue:0xD9E3EE];
}

+ (UIColor*)GetColorFromHexValue0e398e
{
    return  [UIColor GetColorFromHexValue:0x0e398e];
}

+ (UIColor*)GetColorFromHexValue417BAEWith30PercentOpacity
{
    return  [UIColor GetColorFromHexValue:0x417BAE andOpacity:0.3];
}

+ (UIColor*)GetColorFromHexValue2d2926With30PercentOpacity
{
    return  [UIColor GetColorFromHexValue:0x2d2926 andOpacity:0.3];
}

+ (UIColor*)GetColorFromHexValueDCE4EE
{
    return  [UIColor GetColorFromHexValue:0xDCE4EE];
}

//Light Grey
+ (UIColor*)GetColorFromHexValue605E5C
{
    return  [UIColor GetColorFromHexValue:0x605E5C];
}

//Dark Grey
+ (UIColor*)GetColorFromHexValue999897
{
    return  [UIColor GetColorFromHexValue:0x999897];
}

+ (UIColor*)GetColorFromHexValue0E398F
{
    return  [UIColor GetColorFromHexValue:0x0E398F];
}

+ (UIColor*)GetColorFromHexValue071D49
{
    return  [UIColor GetColorFromHexValue:0x071D49];
}

+ (UIColor*)GetColorFromHexValue3880B5
{
    return  [UIColor GetColorFromHexValue:0x3880B5];
}

+ (UIColor*)GetColorFromHexValueE0E6ED
{
    return  [UIColor GetColorFromHexValue:0xE0E6ED];
}
@end
