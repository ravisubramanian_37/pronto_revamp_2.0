//
//  UIColor+ColorDirectory.h
//  Pronto
//
//  Created by m-666346 on 21/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorDirectory)
+(UIColor*)GetColorFromHexString:(NSString*)hexString;

+(UIColor*)GetColorFromHexValue2d2926;
+(UIColor*)GetColorFromHexValue817f7d;
+(UIColor*)GetColorFromHexValue36a2c6;
+(UIColor*)GetColorFromHexValued6dbe8;
+(UIColor*)GetTransparentOverlayColor;
+ (UIColor*)GetColorFromHexValue8E8E8E;
+ (UIColor*)GetColorFromHexValueaba9a8;
+ (UIColor*)GetColorFromHexValuef2f2f2;
+ (UIColor*)GetColorFromHexValue0082ba;
+ (UIColor*)GetColorFromHexValueFFFFFF;
+ (UIColor*)GetColorFromHexValue000000;
+ (UIColor*)GetColorFromHexValue7d91b4;
+ (UIColor*)GetColorFromHexValue999897;
+ (UIColor*)GetColorFromHexValue605E5C;
+ (UIColor*)GetColorFromHexValueDCE4EE;
+ (UIColor*)GetColorFromHexValue3880B5;

//CALENDAR DETAIL SCREEN CELL
+ (UIColor*)GetColorFromHexValue417BAF;
+ (UIColor*)GetColorFromHexValueE6EDF4;
+ (UIColor*)GetColorFromHexValueEEF3F7;
+ (UIColor*)GetColorFromHexValueB0AFB0;
+ (UIColor*)GetColorFromHexValuefaf1de;
+ (UIColor*)GetColorFromHexValueaa8b40;
+ (UIColor*)GetColorFromHexValueE7EDF3;
+ (UIColor*)GetColorFromHexValueDAE4EF;
+ (UIColor*)GetColorFromHexValue1AC9A8;
+ (UIColor*)GetColorFromHexValue0081B9;
+ (UIColor*)GetColorFromHexValue417BAE;
+ (UIColor*)GetColorFromHexValue00809f;
+ (UIColor*)GetColorFromHexValueD9E3EE;
+ (UIColor*)GetColorFromHexValue0e398e;

+ (UIColor*)GetColorFromHexValue0E398F;
+ (UIColor*)GetColorFromHexValue071D49;
+ (UIColor*)GetColorFromHexValueE0E6ED;

+ (UIColor*)GetColorFromHexValue417BAEWith30PercentOpacity;
+ (UIColor*)GetColorFromHexValue2d2926With30PercentOpacity;
@end
