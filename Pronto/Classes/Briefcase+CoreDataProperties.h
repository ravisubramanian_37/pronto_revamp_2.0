//
//  Briefcase+CoreDataProperties.h
//  Pronto
//
//  Created by Oscar Robayo on 22/10/15.
//  Copyright © 2015 Zemoga. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Briefcase.h"

NS_ASSUME_NONNULL_BEGIN

@interface Briefcase (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *birefcase_date;
@property (nullable, nonatomic, retain) NSSet<Asset *> *assets;
@property (nullable, nonatomic, retain) NSSet<Folder *> *folder;

@end

@interface Briefcase (CoreDataGeneratedAccessors)

- (void)addAssetsObject:(Asset *)value;
- (void)removeAssetsObject:(Asset *)value;
- (void)addAssets:(NSSet<Asset *> *)values;
- (void)removeAssets:(NSSet<Asset *> *)values;

- (void)addFolderObject:(Folder *)value;
- (void)removeFolderObject:(Folder *)value;
- (void)addFolder:(NSSet<Folder *> *)values;
- (void)removeFolder:(NSSet<Folder *> *)values;

@end

NS_ASSUME_NONNULL_END
