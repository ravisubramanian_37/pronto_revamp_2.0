//
//  Franchise+CoreDataProperties.m
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Franchise+CoreDataProperties.h"

@implementation Franchise (CoreDataProperties)

// New Pronto Revamp - color code
@dynamic field_franchise_color;
@dynamic field_active;
@dynamic field_private;
@dynamic franchiseID;
@dynamic name;
@dynamic tid;
@dynamic vid;
//@dynamic asset;
//@dynamic brands;

@end
