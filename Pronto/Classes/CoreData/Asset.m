//
//  Asset.m
//  Pronto
//
//  Created by osmarogo on 1/4/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//

#import "Asset.h"
#import "Brand.h"
#import "Constant.h"
#import "Pronto-Swift.h"

@implementation Asset

// Insert code here to add functionality to your managed object subclass

- (void)didImport:(id)data{
    
    NSDictionary *dataDictionary = (NSDictionary *)data;
    [AbbvieLogging  logInfo:[NSString stringWithFormat:@"Asset-didImport-datadict:%@",dataDictionary]];
    if (![dataDictionary objectForKey:@"path"]) {
        self.path = @"";
    }
    
    // PRONTO-25 - Web View - Ability to provide a Web link as an asset type.
    if([[dataDictionary objectForKey:@"file_mime"] isEqualToString:@"weblink"] || [[dataDictionary objectForKey:@"file_mime"] isKindOfClass:[NSNull class]])
    {
        self.file_mime = @"weblink";
    }
    
    // PRONTO-1 - Likes not Working
    self.user_vote = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"user_vote"]];

    // condition check to avoid crash due for Null exception
    if ([[dataDictionary objectForKey:@"file_timestamp"] isEqual:[NSNull null]]  || [dataDictionary objectForKey:@"file_timestamp"] == NULL|| ![[dataDictionary objectForKey:@"file_timestamp"] intValue] || [[dataDictionary objectForKey:@"file_timestamp"] isKindOfClass:[NSNull class]])
    {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset with corrupt data %@", data]];
        NSDate * date = [NSDate date];
        NSNumber *timeStampObj = [NSNumber numberWithDouble: date.timeIntervalSince1970];
        self.file_timestamp = timeStampObj;
    }
    
    // med/reg no.
    if([[dataDictionary objectForKey:@"medRegNo"] count]> 0 )
    {
        NSMutableArray* medRegNos = [dataDictionary objectForKey:@"medRegNo"];
        if(medRegNos.count > 1)
        {
            self.medRegNo = [medRegNos componentsJoinedByString:@","];
        }
        else
        {
            self.medRegNo = [medRegNos objectAtIndex:0];
            
            if(self.medRegNo.length == 0)
                self.medRegNo = @"NA";
        }
    }

    // set the promoted field as NO by default
    self.field_promote = NO;
    self.assetBucketId = [self getAssetBucketIdWithAssetDictionary:dataDictionary];

    self.isViewed = dataDictionary[@"isViewed"];
}

/**
 As one asset can belong to one or multiple bucket.
 And it could be like below.
 a. if there is no value for key "field_asset_bucket" then it's belong franchise
 b. Otherwise it can have one or more values for key "field_asset_bucket"
 Therefore, Idea to keep it in core data is keep each id's seperated with _(Underscore) value
 and get it seperated while reading it.
 */

- (NSString*)getAssetBucketIdWithAssetDictionary:(NSDictionary*)dataDictionary
{
    //If field_asset_bucket value is available in dict, get it else default value is ProntoTabOptionFranchiseID
    id fieldAssetBucket = dataDictionary[@"field_asset_bucket"];
    NSString *bucketId = nil;
    if ([fieldAssetBucket isKindOfClass:[NSArray class]])
    {
        NSArray *tempValue = ((NSArray*)fieldAssetBucket);
        for (NSDictionary *dict in tempValue)
        {
            NSString *tempID = @"";
            if (([dict isKindOfClass:[NSDictionary class]]) && [NSString stringWithFormat:@"%@", dict[@"tid"]].length > 0)
            {
                tempID = [NSString stringWithFormat:@"%@", dict[@"tid"]];
            }
            
            if (bucketId == nil)
            {
                bucketId = tempID;
            }
            else
            {
                bucketId = [NSString stringWithFormat:@"%@_%@",bucketId,tempID];
            }
        }
    }
    else
    {
        bucketId = [NSString stringWithFormat:@"%@",[[Constant sharedConstant] getDefaultBucketID]];
    }
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset bucket id -> %@", bucketId]];
    return bucketId;
}


- (NSArray*)getMockedDictionaryOfArray
{
    NSMutableArray *mutableArr = [NSMutableArray array];
    NSMutableDictionary *tids1 = [NSMutableDictionary dictionary];
    [tids1 setObject:[NSNumber numberWithInt:5157] forKey:@"tid"];
    [mutableArr addObject:tids1];
    
    NSMutableDictionary *tids2 = [NSMutableDictionary dictionary];
    [tids2 setObject:[NSNumber numberWithInt:5158] forKey:@"tid"];
    [mutableArr addObject:tids2];
    
    NSMutableDictionary *tids3 = [NSMutableDictionary dictionary];
    [tids3 setObject:[NSNumber numberWithInt:5159] forKey:@"tid"];
    [mutableArr addObject:tids3];
    return mutableArr;
}

-(NSString *)getAssetType{

    NSString *extension = [self.uri_full pathExtension];
    BOOL isVideo = [self.file_mime isEqualToString:@"video/brightcove"];
    BOOL isImage = [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"jpg|png|gif|jpeg|bmp"] evaluateWithObject:extension];
    // PRONTO-25 - Web View - Ability to provide a Web link as an asset type.
    BOOL isWeblink = [self.file_mime isEqualToString:@"weblink"];

    if(isVideo){
        return RedesignConstants.videoText;
    }
    
    if(isImage){
        return @"Image";
    }
    // PRONTO-25
    if(isWeblink)
     return @"weblink";
    
    return @"Document";
}



@end
