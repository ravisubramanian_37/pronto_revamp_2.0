//
//  OfflineDownload+CoreDataProperties.h
//  Pronto
//
//  Created by cccuser on 17/01/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "OfflineDownload+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface OfflineDownload (CoreDataProperties)

+ (NSFetchRequest<OfflineDownload *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *assetID;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, retain) Asset *asset;

@end

@interface OfflineDownload (CoreDataGeneratedAccessors)

- (void)addAssetObject:(Asset *)value;
- (void)removeAssetObject:(Asset *)value;
- (void)addAsset:(NSSet<Asset *> *)values;
- (void)removeAsset:(NSSet<Asset *> *)values;

@end

NS_ASSUME_NONNULL_END
