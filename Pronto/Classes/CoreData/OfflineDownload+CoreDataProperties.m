//
//  OfflineDownload+CoreDataProperties.m
//  Pronto
//
//  Created by cccuser on 17/01/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "OfflineDownload+CoreDataProperties.h"

@implementation OfflineDownload (CoreDataProperties)

+ (NSFetchRequest<OfflineDownload *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"OfflineDownload"];
}

@dynamic assetID;
@dynamic status;
@dynamic asset;

@end
