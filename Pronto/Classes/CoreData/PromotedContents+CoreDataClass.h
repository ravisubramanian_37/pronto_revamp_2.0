//
//  PromotedContents+CoreDataClass.h
//  
//
//  Created by Saranya on 01/02/2021.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset;

NS_ASSUME_NONNULL_BEGIN

@interface PromotedContents : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "PromotedContents+CoreDataProperties.h"
