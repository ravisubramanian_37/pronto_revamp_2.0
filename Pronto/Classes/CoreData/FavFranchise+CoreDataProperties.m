//
//  FavFranchise+CoreDataProperties.m
//  Pronto
//
//  Created by cccuser on 01/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//
//

#import "FavFranchise+CoreDataProperties.h"

@implementation FavFranchise (CoreDataProperties)

+ (NSFetchRequest<FavFranchise *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FavFranchise"];
}

@dynamic asset_ID;
//@dynamic bucketName;
@dynamic asset;
@dynamic priority;
//@dynamic bucketID;

@end
