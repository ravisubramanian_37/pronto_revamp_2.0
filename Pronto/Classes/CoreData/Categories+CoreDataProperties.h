//
//  Categories+CoreDataProperties.h
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Categories.h"

NS_ASSUME_NONNULL_BEGIN

@interface Categories (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *categoryID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *vid;
 // Revamp - added for bucket selection
@property (nullable, nonatomic, retain) NSString *bucketName;
@property (nullable, nonatomic, retain) NSNumber *bucketId;
@property (nullable, nonatomic, retain) NSSet<Asset *> *assets;
//Added for Redesign
@property (nullable, nonatomic, retain) NSNumber *asset_count;
@property (nullable, nonatomic, retain) NSNumber *parent;
@property (nullable, nonatomic, retain) NSSet<Categories *> *subfolders;
//@property (nullable, nonatomic, retain) NSArray *assets;
@end

@interface Categories (CoreDataGeneratedAccessors)

- (void)addAssetObject:(Asset *)value;
- (void)removeAssetObject:(Asset *)value;
- (void)addAsset:(NSSet<Asset *> *)values;
- (void)removeAsset:(NSSet<Asset *> *)values;

@end

NS_ASSUME_NONNULL_END
