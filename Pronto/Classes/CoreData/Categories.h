//
//  Categories.h
//  Pronto
//
//  Created by osmarogo on 1/4/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset;

NS_ASSUME_NONNULL_BEGIN

@interface Categories : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Categories+CoreDataProperties.h"
