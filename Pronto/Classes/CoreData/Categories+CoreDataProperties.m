//
//  Categories+CoreDataProperties.m
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Categories+CoreDataProperties.h"

@implementation Categories (CoreDataProperties)

@dynamic categoryID;
@dynamic name;
@dynamic vid;
//@dynamic asset;
@dynamic bucketId;
@dynamic bucketName;
@dynamic parent;
@dynamic asset_count;
@dynamic subfolders;
@dynamic assets;
@end
