//
//  Brand+CoreDataProperties.h
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Brand.h"

NS_ASSUME_NONNULL_BEGIN

@interface Brand (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *brandID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *vid;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *asset;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *franchise;

@end

@interface Brand (CoreDataGeneratedAccessors)

- (void)addAssetObject:(NSManagedObject *)value;
- (void)removeAssetObject:(NSManagedObject *)value;
- (void)addAsset:(NSSet<NSManagedObject *> *)values;
- (void)removeAsset:(NSSet<NSManagedObject *> *)values;

- (void)addFranchiseObject:(NSManagedObject *)value;
- (void)removeFranchiseObject:(NSManagedObject *)value;
- (void)addFranchise:(NSSet<NSManagedObject *> *)values;
- (void)removeFranchise:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
