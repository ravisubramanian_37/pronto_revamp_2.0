//
//  Preferences+CoreDataClass.h
//  Pronto
//
//  Created by m-666346 on 23/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSArray;

NS_ASSUME_NONNULL_BEGIN

@interface Preferences : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Preferences+CoreDataProperties.h"
