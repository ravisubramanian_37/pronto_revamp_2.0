//
//  Preferences+CoreDataProperties.h
//  Pronto
//
//  Created by m-666346 on 23/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Preferences+CoreDataClass.h"
#import "Franchise.h"

NS_ASSUME_NONNULL_BEGIN

@interface Preferences (CoreDataProperties)

+ (NSFetchRequest<Preferences *> *)fetchRequest;

//@property (nullable, nonatomic, retain) NSArray *franchieses;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, retain) NSSet<Franchise *> *franchieses;
@property (nonatomic) BOOL savedFranchise;
+ (NSString*)GetUserNameAsString;

@end

NS_ASSUME_NONNULL_END
