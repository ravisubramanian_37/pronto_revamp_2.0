//
//  Preferences+CoreDataProperties.m
//  Pronto
//
//  Created by m-666346 on 23/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Preferences+CoreDataProperties.h"

@implementation Preferences (CoreDataProperties)

+ (NSFetchRequest<Preferences *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Preferences"];
}

@dynamic franchieses;
@dynamic userName;
@dynamic savedFranchise;
+ (NSString*)GetUserNameAsString
{
    return @"userName";
}

@end
