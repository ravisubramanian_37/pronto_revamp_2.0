//
//  BucketDetail+CoreDataProperties.h
//  
//
//  Created by m-666346 on 20/06/18.
//
//

#import "BucketDetail+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BucketDetail (CoreDataProperties)

+ (NSFetchRequest<BucketDetail *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *toolsTrainingBucketID;
@property (nullable, nonatomic, copy) NSString *franchiseBucketID;
@property (nullable, nonatomic, copy) NSString *competitorsBucketID;
@property (nullable, nonatomic, copy) NSString *eventBucketID;

@end

NS_ASSUME_NONNULL_END
