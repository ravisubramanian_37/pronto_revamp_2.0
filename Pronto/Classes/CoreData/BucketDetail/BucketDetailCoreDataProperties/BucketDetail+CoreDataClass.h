//
//  BucketDetail+CoreDataClass.h
//  
//
//  Created by m-666346 on 20/06/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BucketDetail : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BucketDetail+CoreDataProperties.h"
