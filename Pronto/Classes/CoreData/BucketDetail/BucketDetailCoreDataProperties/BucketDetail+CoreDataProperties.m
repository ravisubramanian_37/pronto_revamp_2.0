//
//  BucketDetail+CoreDataProperties.m
//  
//
//  Created by m-666346 on 20/06/18.
//
//

#import "BucketDetail+CoreDataProperties.h"

@implementation BucketDetail (CoreDataProperties)

+ (NSFetchRequest<BucketDetail *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BucketDetail"];
}

@dynamic toolsTrainingBucketID;
@dynamic franchiseBucketID;
@dynamic competitorsBucketID;
@dynamic eventBucketID;

@end
