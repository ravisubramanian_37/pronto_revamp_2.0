//
//  BucketDetailDataControl.h
//  Pronto
//
//  Created by m-666346 on 20/06/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BucketDetailDataControl : NSObject

/**
 *  Responsible for parsing the bucket id and bucket name from
 *  get categories services call.
 */
- (void)saveBucketDetailFromCategories:(NSDictionary*)categories;

/**
 *  Responsible for initializing the bucket id value from core data.
 */
- (void)initializeBucketIDValues;

@end
