//
//  BucketDetailDataControl.m
//  Pronto
//
//  Created by m-666346 on 20/06/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "BucketDetailDataControl.h"
#import <MagicalRecord/MagicalRecord.h>
#import "BucketDetail+CoreDataClass.h"
#import "BucketDetail+CoreDataProperties.h"
#import "Pronto-Swift.h"

#import "Constant.h"


@implementation BucketDetailDataControl

/**
 *  Responsible for parsing the bucket id and bucket name from
 *  get categories services call.
 */

- (void)saveBucketDetailFromCategories:(NSDictionary*)categories
{
    //update two times the constant file one from appdelegate another from abbvie api
    // to make sure constant file always have value
    // don't update on logout
    NSDictionary * allCategories = [categories valueForKey:@"categories"];
    NSString *franchiseBucketID     = nil;
    NSString *competitorsBucketID   = nil;
    NSString *toolsTrainingBucketID = nil;
    
    if ([allCategories isKindOfClass:[NSDictionary class]])
    {
        for (id key in allCategories)
        {
            NSDictionary  *category = [allCategories objectForKey:key];
            if ([category isKindOfClass:[NSDictionary class]])
            {
                if ([category[@"bucketname"] isEqualToString:ProntoTabOptionFranchise])
                {
                    franchiseBucketID = [NSString stringWithFormat:@"%@", category[@"bucketid"]];
                }
                else if ([category[@"bucketname"] isEqualToString:ProntoTabOptionCompetitor])
                {
                    competitorsBucketID = [NSString stringWithFormat:@"%@", category[@"bucketid"]];
                }
                else if ([category[@"bucketname"] isEqualToString:ProntoTabOptionToolsAndTraining])
                {
                    toolsTrainingBucketID = [NSString stringWithFormat:@"%@", category[@"bucketid"]];
                }
                
                if (franchiseBucketID != nil && competitorsBucketID != nil && toolsTrainingBucketID != nil)
                {
                    break;
                }
            }
        }
    }
    
    if (franchiseBucketID != nil && competitorsBucketID != nil && toolsTrainingBucketID != nil)
    {
        //update the value of constant.
        Constant.sharedConstant.ProntoTabOptionFranchiseID          = franchiseBucketID;
        Constant.sharedConstant.ProntoTabOptionCompetitorID         = competitorsBucketID;
        Constant.sharedConstant.ProntoTabOptionToolsAndTrainingID   = toolsTrainingBucketID;
        Constant.sharedConstant.ProntoTabOptionEventID              = ProntoTabOptionEventIDValue;
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            NSArray *existingBucketDetail = [BucketDetail MR_findAllInContext:localContext];
            if (existingBucketDetail.count <= 0) // No bucket detail exist
            {
                BucketDetail * bucketDetail = [BucketDetail MR_createEntityInContext:localContext];
                bucketDetail.franchiseBucketID      = franchiseBucketID;
                bucketDetail.competitorsBucketID    =  competitorsBucketID;
                bucketDetail.toolsTrainingBucketID  = toolsTrainingBucketID;
            }
            else // Already bucket detail exist, update the value.
            {
                //if the value is same don't update otherwise update it.
                BucketDetail *bucketDetail  = (BucketDetail*)existingBucketDetail.firstObject;
                bucketDetail.franchiseBucketID      = franchiseBucketID;
                bucketDetail.competitorsBucketID    =  competitorsBucketID;
                bucketDetail.toolsTrainingBucketID  = toolsTrainingBucketID;
            }
        } completion:^(BOOL contextDidSave, NSError *error) {
            [AbbvieLogging logInfo:@"saveBucketDetailFromCategories error"];
        }];
    }
}

- (void)initializeBucketIDValues
{
     NSArray *existingBucketDetail = [BucketDetail MR_findAll];
    if (existingBucketDetail.count > 0)
    {
        BucketDetail *bucketDetail  = (BucketDetail*)existingBucketDetail.firstObject;
        Constant.sharedConstant.ProntoTabOptionFranchiseID          = bucketDetail.franchiseBucketID;
        Constant.sharedConstant.ProntoTabOptionCompetitorID         = bucketDetail.competitorsBucketID;
        Constant.sharedConstant.ProntoTabOptionToolsAndTrainingID   = bucketDetail.toolsTrainingBucketID;
        Constant.sharedConstant.ProntoTabOptionEventID              = ProntoTabOptionEventIDValue;
    }
}

@end
