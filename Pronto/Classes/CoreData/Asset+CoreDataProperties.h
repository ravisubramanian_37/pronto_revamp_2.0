//
//  Asset+CoreDataProperties.h
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Asset.h"
#import "Categories+CoreDataProperties.h"
NS_ASSUME_NONNULL_BEGIN

@interface Asset (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *assetID;
@property (nullable, nonatomic, retain) NSNumber *changed;
@property (nullable, nonatomic, retain) NSNumber *created;
@property (nullable, nonatomic, retain) NSString *field_description;
@property (nullable, nonatomic, retain) NSString *field_length;
@property (nullable, nonatomic, retain) NSString *field_thumbnail;
@property (nullable, nonatomic, retain) NSString *file_mime;
@property (nullable, nonatomic, retain) NSNumber *file_size;
@property (nullable, nonatomic, retain) NSNumber *file_timestamp;
@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSNumber *popular;
@property (nullable, nonatomic, retain) NSNumber *publish_on;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSNumber *unpublish_on;
@property (nullable, nonatomic, retain) NSString *uri_full;
@property (nullable, nonatomic, retain) NSNumber *view_count;
// New Revamping - Med/Reg no.,field_print,field_promote,field_email_to_self,is_new
@property (nullable, nonatomic, retain) NSString * medRegNo;
@property (nullable, nonatomic, retain) NSString * field_print;
@property (nullable, nonatomic, retain) NSString * assetBucketId;
@property (nonatomic) BOOL field_promote;
@property (nullable, nonatomic, retain) NSString * field_email_to_self;
@property (nonatomic) BOOL isUpdateAvailable;
@property (nonatomic) BOOL is_new;
@property (nonatomic) BOOL isDownloaded;
@property (nullable, nonatomic, retain) NSString *savedPath;

@property (nullable, nonatomic, copy) NSNumber *isViewed;


// PRONTO-1 - Likes not Working
@property (nullable, nonatomic, retain) NSString *user_vote;
@property (nullable, nonatomic, retain) NSNumber *votes;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *brands;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *categories;
//@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *franchise;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *tags;
@property (nullable, nonatomic,retain) NSSet<NSManagedObject *> *parentCategory;
@end

@interface Asset (CoreDataGeneratedAccessors)

- (void)addBrandsObject:(Brand *)value;
- (void)removeBrandsObject:(Brand *)value;
- (void)addBrands:(NSSet<NSManagedObject *> *)values;
- (void)removeBrands:(NSSet<NSManagedObject *> *)values;

- (void)addCategoriesObject:(NSManagedObject *)value;
- (void)removeCategoriesObject:(NSManagedObject *)value;
- (void)addCategories:(NSSet<NSManagedObject *> *)values;
- (void)removeCategories:(NSSet<NSManagedObject *> *)values;

- (void)addFranchiseObject:(NSManagedObject *)value;
- (void)removeFranchiseObject:(NSManagedObject *)value;
- (void)addFranchise:(NSSet<NSManagedObject *> *)values;
- (void)removeFranchise:(NSSet<NSManagedObject *> *)values;

- (void)addTagsObject:(NSManagedObject *)value;
- (void)removeTagsObject:(NSManagedObject *)value;
- (void)addTags:(NSSet<NSManagedObject *> *)values;
- (void)removeTags:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
