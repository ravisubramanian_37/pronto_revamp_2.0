//
//  Asset.h
//  Pronto
//
//  Created by osmarogo on 1/4/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Brand;

NS_ASSUME_NONNULL_BEGIN

@interface Asset : NSManagedObject


-(NSString *)getAssetType;


@end

NS_ASSUME_NONNULL_END

#import "Asset+CoreDataProperties.h"
