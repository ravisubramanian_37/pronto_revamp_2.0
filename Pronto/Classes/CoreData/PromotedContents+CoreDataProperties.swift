//
//  PromotedContents+CoreDataProperties.swift
//  Pronto
//
//  Created by Saranya on 01/02/2021.
//  Copyright © 2021 Abbvie. All rights reserved.
//
//

import Foundation
import CoreData


extension PromotedContents {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PromotedContents> {
        return NSFetchRequest<PromotedContents>(entityName: "PromotedContents")
    }

    @NSManaged public var id: NSNumber?
    @NSManaged public var priority: NSNumber?
    @NSManaged public var asset: Asset?

}
