//
//  FavFranchise+CoreDataProperties.h
//  Pronto
//
//  Created by cccuser on 01/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//
//

#import "FavFranchise+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FavFranchise (CoreDataProperties)

+ (NSFetchRequest<FavFranchise *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *asset_ID;
//@property (nullable, nonatomic, copy) NSString *bucketName;

//Bucket id value is not being used, Use bucket name for any reference.
//@property (nullable, nonatomic, copy) NSNumber *bucketID;
@property (nullable, nonatomic, retain) Asset *asset;
@property (nullable, nonatomic, retain) NSNumber *priority;

@end

@interface FavFranchise (CoreDataGeneratedAccessors)

- (void)addAssetObject:(Asset *)value;
- (void)removeAssetObject:(Asset *)value;
- (void)addAsset:(NSSet<Asset *> *)values;
- (void)removeAsset:(NSSet<Asset *> *)values;

@end

NS_ASSUME_NONNULL_END
