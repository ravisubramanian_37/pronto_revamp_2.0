//
//  ParentFolder+CoreDataProperties.swift
//  
//
//  Created by Saranya on 27/04/2021.
//
//

import Foundation
import CoreData


extension ParentFolder {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ParentFolder> {
        return NSFetchRequest<ParentFolder>(entityName: "ParentFolder")
    }

    @NSManaged public var id: NSNumber?
    @NSManaged public var name: String?

}
