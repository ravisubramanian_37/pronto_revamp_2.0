//
//  OfflineDownload+CoreDataClass.h
//  Pronto
//
//  Created by cccuser on 17/01/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset;

NS_ASSUME_NONNULL_BEGIN

@interface OfflineDownload : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "OfflineDownload+CoreDataProperties.h"
