//
//  AssetDuration+CoreDataProperties.m
//  Pronto
//
//  Created by osmarogo on 1/4/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AssetDuration+CoreDataProperties.h"

@implementation AssetDuration (CoreDataProperties)

@dynamic asset_duration;
@dynamic asset_id;

@end
