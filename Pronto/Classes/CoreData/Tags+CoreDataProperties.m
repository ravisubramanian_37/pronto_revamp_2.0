//
//  Tags+CoreDataProperties.m
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Tags+CoreDataProperties.h"

@implementation Tags (CoreDataProperties)

@dynamic key;
@dynamic asset;

@end
