//
//  Franchise.m
//  Pronto
//
//  Created by osmarogo on 1/4/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//

#import "Franchise.h"
#import "Asset.h"
#import "Brand.h"

@implementation Franchise

// Insert code here to add functionality to your managed object subclass

- (NSArray*)getFranchiseAndBrandIds
{
    NSMutableArray *franchiseAndBrands = [NSMutableArray array];
    if (self.franchiseID != nil)
    {
      [franchiseAndBrands addObject:self.franchiseID];
    }
    return franchiseAndBrands;
}

@end
