//
//  EventOnlyAsset+CoreDataClass.h
//  Pronto
//
//  Created by m-666346 on 11/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Constant.h"

@class NSObject;
@class Event;

NS_ASSUME_NONNULL_BEGIN

@interface EventOnlyAsset : NSManagedObject
+ (NSString*)IdAttributeAsString;
+ (NSString*)TypeAttributeValueAsString;
- (enum ZMProntoFileTypes)getFileTypeOfEventOnlyAsset;
- (Asset*)getEquivalentAssetObject;
- (void)updateEventOnlyAssetPath:(NSString*)eoa_path withCompletionHandler:(OperationStatusCompletion)completionHandler;

- (void)updateEventOnlyAssetWithDictionary:(NSDictionary*)dict inLocalContext:(NSManagedObjectContext*)localContext;

/**
    If a Event only Asset belongs to multiple franchise. We will take name of very first franchise.
 */
- (NSString*)franchiseName;

/**
 One EoA can belong to many Events. It will provide any event among them.
 Don't use this method for getting some particual Event.
 */
//- (Event*)getEventForCurrentEventOnlyAsset;

- (NSArray*)getAllEvents;
- (BOOL)doesEventOnlyAssetBelongsFromSelectedFranchise;

@end

NS_ASSUME_NONNULL_END

#import "EventOnlyAsset+CoreDataProperties.h"
