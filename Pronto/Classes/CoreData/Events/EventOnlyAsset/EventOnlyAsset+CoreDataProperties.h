//
//  EventOnlyAsset+CoreDataProperties.h
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "EventOnlyAsset+CoreDataClass.h"

@class Session;

NS_ASSUME_NONNULL_BEGIN

@interface EventOnlyAsset (CoreDataProperties)

+ (NSFetchRequest<EventOnlyAsset *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *eo_changed;
@property (nullable, nonatomic, copy) NSNumber *eoa_created;
@property (nullable, nonatomic, copy) NSString *eoa_file_mime;
@property (nullable, nonatomic, copy) NSNumber *eoa_file_size;
@property (nullable, nonatomic, copy) NSNumber *eoa_file_timestamp;
@property (nullable, nonatomic, copy) NSString *eoa_id;
@property (nullable, nonatomic, copy) NSNumber *eoa_isUpdate_Available;
@property (nullable, nonatomic, copy) NSString *eoa_path;
@property (nullable, nonatomic, copy) NSNumber *eoa_print;
@property (nullable, nonatomic, copy) NSString *eoa_title;
@property (nullable, nonatomic, copy) NSString *eoa_type;
@property (nullable, nonatomic, copy) NSNumber *eoa_unpublishOn;
@property (nullable, nonatomic, copy) NSString *eoa_uri_full;
@property (nullable, nonatomic, retain) NSArray *franchiseIds;
@property (nullable, nonatomic, copy) NSNumber *isViewed;
@property (nullable, nonatomic, retain) NSSet<Session *> *eoa_sessions;

@end

@interface EventOnlyAsset (CoreDataGeneratedAccessors)

- (void)addEoa_sessionsObject:(Session *)value;
- (void)removeEoa_sessionsObject:(Session *)value;
- (void)addEoa_sessions:(NSSet<Session *> *)values;
- (void)removeEoa_sessions:(NSSet<Session *> *)values;

@end

NS_ASSUME_NONNULL_END
