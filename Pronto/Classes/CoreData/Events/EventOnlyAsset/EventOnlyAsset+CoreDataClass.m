//
//  EventOnlyAsset+CoreDataClass.m
//  Pronto
//
//  Created by m-666346 on 11/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "EventOnlyAsset+CoreDataClass.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Pronto-Swift.h"
#import "EventCoreDataManager.h"
#import "ZMUserActions.h"
#import "ProntoUserDefaults.h"
#import "Brand.h"

#import "Asset.h"
#import "Franchise.h"

#import "Event+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventDuration+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"


@implementation EventOnlyAsset

+ (NSString*)IdAttributeAsString
{
    return @"eoa_id";
}

+ (NSString*)TypeAttributeValueAsString
{
    return @"event_only_asset";
}
/* Method to update event only asset with dictionary */
- (void)updateEventOnlyAssetWithDictionary:(NSDictionary*)dict inLocalContext:(NSManagedObjectContext*)localContext
{    
    self.franchiseIds = dict[@"eoa_franchise"];
    self.eo_changed = [NSNumber numberWithUnsignedInteger:[dict[@"eoa_changed"] integerValue]];
    self.eoa_created = [NSNumber numberWithUnsignedInteger:[dict[@"eoa_created"] integerValue]];
    self.eoa_file_mime = dict[@"eoa_file_mime"];
    self.eoa_file_size = [NSNumber numberWithUnsignedInteger:[dict[@"eoa_file_size"] integerValue]];
    self.eoa_file_timestamp = [NSNumber numberWithUnsignedInteger:[dict[@"eoa_file_timestamp"] integerValue]];
    self.eoa_id   = [NSString stringWithFormat:@"%@", dict[@"eoa_id"]];
    self.eoa_print = [NSNumber numberWithBool:[dict[@"eoa_print"]  isEqualToString: @"0"] ? NO : YES]; 
    self.eoa_title     = dict[@"eoa_title"];
    self.eoa_type      = dict[@"eoa_type"];
    self.eoa_unpublishOn = [NSNumber numberWithUnsignedInteger:[dict[@"eoa_unpublished_date"] integerValue]];
    self.eoa_uri_full  = dict[@"eoa_uri_full"];
    if(self.eoa_path == nil)
    {
       self.eoa_path   = @"";
    }
    
    [self setEOAIsViewedPropertiesForId:self.eoa_id];
}
/* Set EOA isViewed for asset ids */
- (void)setEOAIsViewedPropertiesForId:(NSString*)assetId
{
    NSArray *viewedAssetIds = [[ProntoUserDefaults userDefaults] getViewedEventOnlyAssetIds];
    NSMutableArray *eoaToBeSaved = [NSMutableArray arrayWithArray:viewedAssetIds];
    if ([viewedAssetIds containsObject:assetId])
    {
        self.isViewed = [NSNumber numberWithBool:YES];
        [eoaToBeSaved removeObject:assetId];
        
        [[ProntoUserDefaults userDefaults] setViewedEventOnlyAssetIds:eoaToBeSaved];
    }
}
/* Get file type for event only asset */
- (enum ZMProntoFileTypes)getFileTypeOfEventOnlyAsset
{
    enum ZMProntoFileTypes type = ZMProntoFileTypesDocument;
    
    if ([self.eoa_uri_full rangeOfString:@"brightcove://"].location == 0)
    {
        type = ZMProntoFileTypesBrightcove;
    }
    else if ([[[self.eoa_uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"])
    {
        type = ZMProntoFileTypesPDF;
    }
    else if ([self.eoa_file_mime isEqualToString:@"weblink" ]) //Web View - Ability to provide a Web link as an asset type.
    {
        type = ZMProntoFileTypesWeblink;
    }
    
    return type;
}
/* update event only asset path */
- (void)updateEventOnlyAssetPath:(NSString*)eoa_path withCompletionHandler:(OperationStatusCompletion)completionHandler
{    
    self.eoa_path = eoa_path;
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error)
     {
         if (completionHandler != nil)
         {
             completionHandler(contextDidSave);
         }
     }];
}
/* Get equivalent asset object */
- (Asset*)getEquivalentAssetObject
{    
    [[EventCoreDataManager sharedInstance] removeAssetOfTypeEOAFromAssetTableForAssetID:[NSNumber numberWithInteger:[self.eoa_id integerValue]] andWithCompletionHandler:NULL];

    Asset * equivalentAsset     = [Asset MR_createEntity];
    equivalentAsset.assetID     = [NSNumber numberWithInteger:[self.eoa_id integerValue]];
    equivalentAsset.title       = self.eoa_title;
    equivalentAsset.path        = self.eoa_path;
    equivalentAsset.field_print = self.eoa_print.boolValue == YES ? @"YES":@"NO";
    equivalentAsset.type        = self.eoa_type;
    equivalentAsset.file_mime   = self.eoa_file_mime;
    equivalentAsset.uri_full    = self.eoa_uri_full;
    //Other properties can be updated on demand;
    return equivalentAsset;
}
/* Get franchise name */
- (NSString*)franchiseName
{
    if (self.franchiseIds != nil && self.franchiseIds.count > 0)
    {
        NSString *franchiseTid = [NSString stringWithFormat:@"%@", self.franchiseIds.lastObject];  //;
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseByTid:franchiseTid];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            return franchise.name;
        }
        else
        {
            // First priority will be given to last object if not found then loop through and first found will be taken
            for (NSString *franchiseTid in self.franchiseIds)
            {
                NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseByTid:[NSString stringWithFormat:@"%@", franchiseTid]];
                if (franchises != nil && franchises.count > 0)
                {
                    Franchise *franchise = (Franchise*)franchises.lastObject;
                    return franchise.name;
                }
            }
        }
    }
    //if not found for any franchise, it will be grouped with # key.
    return @"#";
}
/* Method to get all events */
- (NSArray*)getAllEvents
{
    NSArray *allSessions = [self.eoa_sessions allObjects];
    NSMutableArray *parentID = [NSMutableArray array];
    NSMutableArray *event = [NSMutableArray array];
    for (Session *sesion in allSessions)
    {
        for (EventDuration *eventDuration in sesion.sessionEventDurations.allObjects)
        {
            if ([parentID containsObject:eventDuration.parentEvent.eventId] == NO &&
                eventDuration.parentEvent != nil && eventDuration.parentEvent.eventId != nil)
            {
                [event addObject:eventDuration.parentEvent];
                [parentID addObject:eventDuration.parentEvent.eventId];
                break;
            }
        }
    }
    
    return event;
}
/* check EOA available in selected franchise */
- (BOOL)doesEventOnlyAssetBelongsFromSelectedFranchise
{
   __block BOOL doesBelongs = NO;
    
    if ([[ZMUserActions sharedInstance] isFieldUser] == YES)
    {
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseById:[ZMUserActions sharedInstance].franchiseId];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            //collecting franchise id and brands id's.
            NSArray *franchiseAndBrands = [franchise getFranchiseAndBrandIds];
            [self.franchiseIds enumerateObjectsUsingBlock:^(NSDictionary   *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                NSNumber *eoa_franchiseId = [NSNumber numberWithInteger:[obj[@"tid"] integerValue]];
                
                if ([franchiseAndBrands containsObject:eoa_franchiseId])
                {
                    doesBelongs = YES;
                    *stop = YES;
                }
            }];
        }
    }
    else
    {
        //for field us
        NSArray *selectedFranchise = [ZMProntoManager sharedInstance].franchisesAndBrands;
        
        [self.franchiseIds enumerateObjectsUsingBlock:^(NSDictionary   *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSNumber *franichiseId = [NSNumber numberWithInteger:[obj[@"tid"] integerValue]];
            if ([selectedFranchise containsObject:franichiseId])
            {
                doesBelongs = YES;
                *stop = YES;
            }
        }];
    }
    
    return doesBelongs;
}


@end
