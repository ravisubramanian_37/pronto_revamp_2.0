//
//  EventOnlyAsset+CoreDataProperties.m
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "EventOnlyAsset+CoreDataProperties.h"

@implementation EventOnlyAsset (CoreDataProperties)

+ (NSFetchRequest<EventOnlyAsset *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"EventOnlyAsset"];
}

@dynamic eo_changed;
@dynamic eoa_created;
@dynamic eoa_file_mime;
@dynamic eoa_file_size;
@dynamic eoa_file_timestamp;
@dynamic eoa_id;
@dynamic eoa_isUpdate_Available;
@dynamic eoa_path;
@dynamic eoa_print;
@dynamic eoa_title;
@dynamic eoa_type;
@dynamic eoa_unpublishOn;
@dynamic eoa_uri_full;
@dynamic franchiseIds;
@dynamic isViewed;
@dynamic eoa_sessions;

@end
