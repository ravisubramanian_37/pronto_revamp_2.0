//
//  EventDuration+CoreDataProperties.h
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "EventDuration+CoreDataClass.h"

@class Session;

NS_ASSUME_NONNULL_BEGIN

@interface EventDuration (CoreDataProperties)

+ (NSFetchRequest<EventDuration *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *endTime;
@property (nullable, nonatomic, copy) NSString *parentEventId;
@property (nullable, nonatomic, copy) NSString *sessionId;
@property (nullable, nonatomic, copy) NSString *startTime;
@property (nullable, nonatomic, retain) Event *parentEvent;
@property (nullable, nonatomic, retain) Session *session;

@end

NS_ASSUME_NONNULL_END
