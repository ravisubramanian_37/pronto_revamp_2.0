//
//  EventDuration+CoreDataClass.m
//  Pronto
//
//  Created by m-666346 on 10/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "EventDuration+CoreDataClass.h"
#import "NSDate+DateDirectory.h"

#import "Event+CoreDataProperties.h"
#import "Event+CoreDataClass.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"


@implementation EventDuration
/* Method to get date string */
- (NSDate*)eventDate
{
    return  [NSDate GetDateSameAsStringFromDateString:self.date];
}
/* Method to get formatted start & end time string */
- (NSString*)getFormattedStartAndEndTimeString
{
    NSMutableString *start = [NSMutableString stringWithString:self.startTime];
    NSString *startTime = [start substringToIndex:start.length - 3];
    startTime = [NSDate Get12HoursTime:startTime];
    
    NSMutableString *end = [NSMutableString stringWithString:self.endTime];
    NSString *endTime = [end substringToIndex:end.length - 3];
    endTime = [NSDate Get12HoursTime:endTime];
    
    return [NSString stringWithFormat:@"%@ - %@ %@", startTime, endTime, self.parentEvent.eventTimezone];
}

/* Method to get formatted start time string */
- (NSString*)getFormattedStartTimeString
{
    NSMutableString *start = [NSMutableString stringWithString:self.startTime];
    NSString *startTime = [start substringToIndex:start.length - 3];
    startTime = [NSDate Get12HoursTime:startTime];
    return startTime;
}
/* Method to get formatted end time string */
- (NSString*)getFormattedEndTimeString
{
    NSMutableString *end = [NSMutableString stringWithString:self.endTime];
    NSString *endTime = [end substringToIndex:end.length - 3];
    endTime = [NSDate Get12HoursTime:endTime];

    return endTime;
}
/* Method to get event duration title */
- (NSString*)getEventDurationTitle
{
    return self.session.title;
}
/* Method to get session location */
- (NSString*)getSessionLocation
{
    return [self.session getLocationName];
}
/* Total no. of assets available for event duration */
- (NSInteger)totalNumberOfAssetAvailableForEventDuration
{
    return [self.session totalNumberOfAssetAvailableForSession];
}
/* Method to get total assets for event duration */
- (NSArray*)totalAssetForEventDuration
{
    return [self.session totalAssetForSession];
}
/* Method to get session description */
- (NSString*)getSessionDescription
{
    return self.session.sessionDescription;
}
@end
