//
//  EventDuration+CoreDataProperties.m
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "EventDuration+CoreDataProperties.h"

@implementation EventDuration (CoreDataProperties)

+ (NSFetchRequest<EventDuration *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"EventDuration"];
}

@dynamic date;
@dynamic endTime;
@dynamic parentEventId;
@dynamic sessionId;
@dynamic startTime;
@dynamic parentEvent;
@dynamic session;

@end
