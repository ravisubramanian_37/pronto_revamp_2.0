//
//  EventDuration+CoreDataClass.h
//  Pronto
//
//  Created by m-666346 on 10/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event;

NS_ASSUME_NONNULL_BEGIN

@interface EventDuration : NSManagedObject

- (NSDate*)eventDate;
- (NSString*)getFormattedStartAndEndTimeString;
- (NSString*)getFormattedStartTimeString;
- (NSString*)getFormattedEndTimeString;
- (NSString*)getEventDurationTitle;
- (NSString*)getSessionLocation;
- (NSInteger)totalNumberOfAssetAvailableForEventDuration;
- (NSArray*)totalAssetForEventDuration;
- (NSString*)getSessionDescription;

@end

NS_ASSUME_NONNULL_END

#import "EventDuration+CoreDataProperties.h"
