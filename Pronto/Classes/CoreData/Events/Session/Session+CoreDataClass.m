//
//  Session+CoreDataClass.m
//  Pronto
//
//  Created by m-666346 on 10/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Session+CoreDataClass.h"
#import "Pronto-Swift.h"

#import "Asset.h"
#import "Franchise.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"

#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"




@implementation Session

+ (NSString*)IdAttributeAsString
{
    return @"sessionId";
}

/* Total no. of assets available for session */
- (NSInteger)totalNumberOfAssetAvailableForSession
{
    return [[self totalAssetForSession] count];
}
/* Calculate Total no. of assets available for session */
- (NSArray*)totalAssetForSession
    {
        NSMutableArray *allAsset = [NSMutableArray array];
        
        if ([self.assetIds isKindOfClass:[NSArray class]] &&
            self.assetIds != nil &&  self.assetIds.count > 0)
        {
            for (NSString *assetId in self.assetIds)
            {
              NSString *assetIdAsString = [NSString stringWithFormat:@"%@", [assetId valueForKey:@"assetID"]];
                NSArray *assets = [[ZMCDManager sharedInstance] getAssetArrayById:assetIdAsString];
                if (assets != nil && assets.count > 0)
                {
                    Asset *asset = (Asset*)[assets lastObject];
                    [allAsset addObject:asset];
                }
            }
        }
        
        if ([self.eventOnlyAssets isKindOfClass:[NSSet class]] &&
            [self.eventOnlyAssets allObjects] != nil &&
            [self.eventOnlyAssets allObjects].count > 0)
        {
            for (EventOnlyAsset *eoa in [self.eventOnlyAssets allObjects])
            {
                BOOL doesBelongs = [eoa doesEventOnlyAssetBelongsFromSelectedFranchise];
                if (doesBelongs == YES)
                {
                    [allAsset addObject:eoa];
                }
            }
        }
        
        return allAsset;
    }
/* Total tids for franchises */
- (NSArray*)getTidsForFranchises:(NSArray*)franchise
{
    NSMutableArray *tids = [NSMutableArray array];
    for (NSString *franchiseId in franchise)
    {
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseById:[NSString stringWithFormat:@"%@", franchiseId]];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            [tids addObject:franchise.tid];
        }
    }
    return tids;
}
/* Get all events for current session */
- (NSArray*)getAllEventsOfCurrentSession
{
    NSMutableArray *events = [NSMutableArray array];
    NSMutableArray *eventIds = [NSMutableArray array];
    
    for (EventDuration *eventDuration in self.sessionEventDurations.allObjects)
    {
        if ([eventIds containsObject:eventDuration.parentEvent.eventId] == NO &&
            eventDuration.parentEvent != nil && eventDuration.parentEvent.eventId != nil)
        {
            [events addObject:eventDuration.parentEvent];
        }
    }
    
    return events;
}
/* check session belongs to selected franchise */
- (BOOL)doesSessionBelongsFromSelectedFranchise
{
    BOOL doesBelongs = NO;
    for (EventDuration *eventDuration in [self.sessionEventDurations allObjects])
    {
        Event *event = eventDuration.parentEvent;
        doesBelongs = [event doesEventBelongsFromSelectedFranchise];
        if (doesBelongs == YES)
        {
            //If match found, don't need to iterate through loop.
            break;
        }
    }
    return doesBelongs;
}
/* get event for current session */
- (Event*)getEventForCurrentSession
{
    NSArray *allEvents = [self getAllEventsOfCurrentSession];
    Event *resulantEvent = nil;
    
    for (Event *event in allEvents)
    {
        if ([[self.location lowercaseString] isEqualToString:[event.location lowercaseString]])
        {
             resulantEvent = event;
        }
        else
        {
            BOOL isSubset = [[NSSet setWithArray: [self.sessionEventDurations allObjects]] isSubsetOfSet: [NSSet setWithArray: [event.eventDurations allObjects]]];
            if (isSubset == YES)
            {
                resulantEvent = event;
            }
        }
    }
    return resulantEvent;
}

/* Get location name */
- (NSString*)getLocationName
{
    if (self.location == nil ||
        self.location.length <= 0)
    {
        return @"";
    }
    return self.location;
}
/* Get event duration for current session */
- (NSArray*)getEventDurationForCurrentSession
{
    NSMutableArray *tempEvent = [NSMutableArray array];
    for (EventDuration *eventDuraiton in [self.sessionEventDurations allObjects])
    {
        if (eventDuraiton.parentEvent != nil)
        {
            [tempEvent addObject:eventDuraiton];
        }
    }
    return tempEvent;
}


@end
