//
//  Session+CoreDataProperties.m
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Session+CoreDataProperties.h"

@implementation Session (CoreDataProperties)

+ (NSFetchRequest<Session *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Session"];
}

@dynamic assetIds;
@dynamic location;
@dynamic sessionDescription;
@dynamic sessionId;
@dynamic title;
@dynamic sessionEventDurations;
@dynamic eventOnlyAssets;

@end
