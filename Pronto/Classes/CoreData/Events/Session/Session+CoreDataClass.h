//
//  Session+CoreDataClass.h
//  Pronto
//
//  Created by m-666346 on 10/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventOnlyAsset, NSObject;
@class Event;


NS_ASSUME_NONNULL_BEGIN

@interface Session : NSManagedObject

+ (NSString*)IdAttributeAsString;
- (NSInteger)totalNumberOfAssetAvailableForSession;
- (NSArray*)totalAssetForSession;
- (NSArray*)getAllEventsOfCurrentSession;
- (BOOL)doesSessionBelongsFromSelectedFranchise;
- (NSString*)getLocationName;
/**
 One Session can belong to many Events. It will provide any event among them.
 Don't use this method for getting some particual Event.
 */
- (Event*)getEventForCurrentSession;
- (NSArray*)getEventDurationForCurrentSession;


@end

NS_ASSUME_NONNULL_END

#import "Session+CoreDataProperties.h"
