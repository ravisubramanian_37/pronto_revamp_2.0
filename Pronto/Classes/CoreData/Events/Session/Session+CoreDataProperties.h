//
//  Session+CoreDataProperties.h
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Session+CoreDataClass.h"

@class EventDuration;

NS_ASSUME_NONNULL_BEGIN

@interface Session (CoreDataProperties)

+ (NSFetchRequest<Session *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSArray *assetIds;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSString *sessionDescription;
@property (nullable, nonatomic, copy) NSString *sessionId;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) NSSet<EventDuration *> *sessionEventDurations;
@property (nullable, nonatomic, retain) NSSet<EventOnlyAsset *> *eventOnlyAssets;

@end

@interface Session (CoreDataGeneratedAccessors)

- (void)addSessionEventDurationsObject:(EventDuration *)value;
- (void)removeSessionEventDurationsObject:(EventDuration *)value;
- (void)addSessionEventDurations:(NSSet<EventDuration *> *)values;
- (void)removeSessionEventDurations:(NSSet<EventDuration *> *)values;

- (void)addEventOnlyAssetsObject:(EventOnlyAsset *)value;
- (void)removeEventOnlyAssetsObject:(EventOnlyAsset *)value;
- (void)addEventOnlyAssets:(NSSet<EventOnlyAsset *> *)values;
- (void)removeEventOnlyAssets:(NSSet<EventOnlyAsset *> *)values;

@end

NS_ASSUME_NONNULL_END
