//
//  EventCoreDataManager.m
//  Pronto
//
//  Created by m-666346 on 04/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventCoreDataManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Pronto-Swift.h"
#import "Event+CoreDataProperties.h"
#import "ProntoUserDefaults.h"
#import "ZMAbbvieAPI.h"
#import "ZMUserActions.h"

#import "Event+CoreDataClass.h"
#import "Priority+CoreDataProperties.h"

#import "Priority+CoreDataClass.h"
#import "Franchise+CoreDataProperties.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"


//Understanding: Below keys value can't be change anywhere in file. If it has to be changed, It has to be changed here. No where else.So,
NSString *const keySession = @"session";
NSString *const keyEvent   = @"event";
NSString *const keyEventOnlyAsset = @"event_only_asset";
NSString *const keyId      = @"id";
NSString *const keyEventId = @"event_id";
NSString *const keyType    = @"type";
NSString *const keyEventSessionAssets = @"event_session_assets";
NSString *const keyTargetId    = @"target_id";

@interface EventCoreDataManager()

@property (nonatomic, strong) NSMutableArray *eventSearchResults;
@property (nonatomic, strong) NSMutableArray *eventOnlyAssets;
@property (nonatomic, strong) NSMutableArray *assets;

@end

@implementation EventCoreDataManager

/**
 This method provides the singleton instance of EventCoreDataManager
 */
+(instancetype)sharedInstance
{
    static EventCoreDataManager *singletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonInstance = [[[self class] alloc]init];
        singletonInstance.eventSearchResults = [NSMutableArray array];
        singletonInstance.eventOnlyAssets    = [NSMutableArray array];
        singletonInstance.assets             = [NSMutableArray array];
    });
    return singletonInstance;
}

/* check is event empty in local DB */
- (BOOL)isEventEmptyInLocalDatabase
{
    NSFetchRequest *fetchRequest = [Event MR_requestAll];
    NSArray *events = [Event MR_executeFetchRequest:fetchRequest];
    return (events.count <= 0) ? YES: NO;
}
/* Get all events */
- (NSArray*)getEvents
{
    //Get All Events which belongs to selected preferences.
    //if it is field user take the franchise id.
    
    NSFetchRequest *fetchRequest = [Event MR_requestAll];
    NSArray *events = [Event MR_executeFetchRequest:fetchRequest];
    NSMutableArray *filteredEvents = [NSMutableArray array];
    
    if ([[ZMUserActions sharedInstance] isFieldUser] == YES)
    {
        for (Event *event in events)
        {
            if ([event doesThisEventBelongsForCurrentFranchise] == YES)
            {
                 [filteredEvents addObject:event];
            }
        }
    }
    else
    {
        NSArray *selectedFranchise = [ZMProntoManager sharedInstance].franchisesAndBrands;
        NSArray *tids = [self getTidsForFranchises:selectedFranchise];
        
        for (Event *event in events)
        {
            NSArray *priorities = [event.priorities allObjects];
            for (Priority *priority in priorities)
            {
                /* Commenting for Now */
                //TODO: check the below code if required
                /*
                if ([tids containsObject:priority.franchiseTid])
                {
                    [filteredEvents addObject:event];
                    break;
                }
                */
                [filteredEvents addObject:event];
            }
        }
    }
    /* Commenting for Now */
    //TODO: check the below code if required
//    return filteredEvents;
    return  events;
}
/* Get tids for franchises */
- (NSArray*)getTidsForFranchises:(NSArray*)franchise
{
    NSMutableArray *tids = [NSMutableArray array];
    NSArray *allFranchise = [[ZMCDManager sharedInstance] getAllFranchise];
    for (NSString *franchiseId in franchise)
    {
        [allFranchise enumerateObjectsUsingBlock:^(Franchise *franchise, NSUInteger idx, BOOL *stop) {
           
            if ([franchise.franchiseID intValue] == [franchiseId integerValue])
            {
                [tids addObject: franchise.tid];
                *stop = YES;
            }
        }];
    }
    
    return tids;
}
/* Get tids of all selected franchise */
- (NSArray*)getTidsOfAllSelectedFranchise
{
    NSArray *selectedFranchise = [ZMProntoManager sharedInstance].franchisesAndBrands;
    
    NSMutableArray *tids = [NSMutableArray array];
    for (NSString *franchiseId in selectedFranchise)
    {
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseById:[NSString stringWithFormat:@"%@", franchiseId]];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            [tids addObject:franchise.tid];
        }
    }
    return tids;
}
/* Save event detail to db */
- (void)saveEventDetail:(NSDictionary*)dict withCompletionHandler:(eventDetailSaveCompletion)completionHandler
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        NSDictionary *eventDetail = [dict valueForKey:[[dict allKeys] lastObject]];
        
        if (eventDetail != nil && [eventDetail isKindOfClass:[NSDictionary class]] == YES && eventDetail.count > 0)
        {
            
            //Doing so will stop saving of duplicate events, If any.
            Event *event = [Event MR_findFirstByAttribute:[Event attributeEventIdAsString]
                                                withValue:eventDetail[@"id"] != nil ? eventDetail[@"id"] : eventDetail[@"nid"]
                                                inContext:localContext];
            
            if (event == nil)
            {
                //Event is already not created. Create and save it.
                event = [Event MR_createEntityInContext:localContext];
                [event updateEventDetailWithDictionary:eventDetail inLocalContext:localContext];
            }
            else
            {
                //Event is already  created. update it.
                [event updateEventDetailWithDictionary:eventDetail inLocalContext:localContext];
            }
        }
        
    } completion:^(BOOL success, NSError *error) {
        
        if (completionHandler != nil)
        {
            completionHandler(YES, nil);
        }
    }];

}
/* Save all events to db */
- (void)saveAllEvents:(NSDictionary*)dict withCompletionHandler:(eventSaveCompletion)completionHandler
{
    NSArray * eventsToDelete    = [dict valueForKey:@"deleted"];
    NSArray * eventsToUnpublish = [dict valueForKey:@"unpublished"];
    NSArray * previousEventIds  = [[ProntoUserDefaults userDefaults] getCouldNotDeleteOrUnpulishEventIds];
        
    NSMutableArray * eventsToProcess = [NSMutableArray array];
    [eventsToProcess addObjectsFromArray:eventsToDelete];
    [eventsToProcess addObjectsFromArray:eventsToUnpublish];
    [eventsToProcess addObjectsFromArray:previousEventIds];
    
    NSMutableArray *eventCouldNotBeProcessed = [NSMutableArray array];
    
    NSMutableDictionary *events = [[NSMutableDictionary alloc]initWithDictionary:dict];
    [events removeObjectForKey:@"deleted"];
    [events removeObjectForKey:@"unpublished"];
    [events removeObjectForKey:@"total"];
    [events removeObjectForKey:@"last_sync"];
    [events removeObjectForKey:@"pages"];
    
    
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        if (eventsToProcess.count > 0)
        {
            for (id eventId in eventsToProcess)
            {
                Event *eventToDelete = [Event MR_findFirstByAttribute:[Event attributeEventIdAsString]
                                                            withValue:eventId
                                                            inContext:localContext];
                
                if (eventToDelete != nil)
                {
                    [self removeThumnailImageForEvent:eventToDelete];
                    
                    BOOL didSave =  [eventToDelete MR_deleteEntityInContext:localContext];
                    [localContext MR_saveToPersistentStoreAndWait];
                    if (didSave == NO)
                    {
                        [eventCouldNotBeProcessed addObject: eventId];
                    }
                }
            }
            
            //save to userdefaults
            [[ProntoUserDefaults userDefaults] setCouldNotDeleteOrUnpulishEventIds:eventCouldNotBeProcessed];
        }
        
        NSMutableArray *allEvents = [NSMutableArray array];
        if (events != nil && [events isKindOfClass:[NSDictionary class]] == YES && events.count > 0)
        {
            for (NSString *key in [events allKeys])
            {
                NSDictionary *singleEvent = events[key];
                
                //Doing so will stop saving of duplicate events, If any.
                Event *event = [Event MR_findFirstByAttribute:[Event attributeEventIdAsString]
                                                    withValue:key
                                                    inContext:localContext];
                
                if (event == nil)
                {
                    //Event is already not created. Create and save it.
                    event = [Event MR_createEntityInContext:localContext];
                    //[event updatePropertiesWithDictionary:singleEvent inLocalContext:localContext];
                }
                [event saveEventHavingCompleteDetail:singleEvent inLocalContext:localContext];
                [allEvents addObject:event];
            }
        }
        
    } completion:^(BOOL success, NSError *error) {
        
        if (completionHandler != nil)
        {
            completionHandler(YES, nil);
        }
        
        //Last time Events were synced from CMS.
        // Saving last_sync from make sure that whatever events we have got from server
        // it has been saved to local database. Otherwise, it could led to scenario where
        // We got the response from server and somehow we could not save and then when we do
        // next time CMS request, it will not give us events.
        
        [[ProntoUserDefaults userDefaults] setCMSLastSynchronizationForEvents:[dict objectForKey:@"last_sync"]];
    }];
}

/* Save events to db */
- (void)saveEvents:(NSDictionary*)dict withCompletionHandler:(eventSaveCompletion)completionHandler
{
    NSArray * eventsToDelete    = [dict valueForKey:@"deleted"];
    NSArray * eventsToUnpublish = [dict valueForKey:@"unpublished"];
    NSArray * previousEventIds  = [[ProntoUserDefaults userDefaults] getCouldNotDeleteOrUnpulishEventIds];
    
    NSMutableArray * eventsToProcess = [NSMutableArray array];
    [eventsToProcess addObjectsFromArray:eventsToDelete];
    [eventsToProcess addObjectsFromArray:eventsToUnpublish];
    [eventsToProcess addObjectsFromArray:previousEventIds];
    
//    //For testing purpose
//    [eventsToProcess addObject:@"3702"];
//    [eventsToProcess addObject:@"3703"];
    
    NSMutableArray *eventCouldNotBeProcessed = [NSMutableArray array];

    NSMutableDictionary *events = [[NSMutableDictionary alloc]initWithDictionary:dict];
    [events removeObjectForKey:@"deleted"];
    [events removeObjectForKey:@"unpublished"];
    [events removeObjectForKey:@"total"];
    [events removeObjectForKey:@"last_sync"];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        if (eventsToProcess.count > 0)
        {
            for (id eventId in eventsToProcess)
            {
                Event *eventToDelete = [Event MR_findFirstByAttribute:[Event attributeEventIdAsString]
                                                    withValue:eventId
                                                    inContext:localContext];
                
                if (eventToDelete != nil)
                {
                    [self removeThumnailImageForEvent:eventToDelete];
                    
                    BOOL didSave =  [eventToDelete MR_deleteEntityInContext:localContext];
                    [localContext MR_saveToPersistentStoreAndWait];
                    if (didSave == NO)
                    {
                        [eventCouldNotBeProcessed addObject: eventId];
                    }
                }
            }
            
            //save to userdefaults
            [[ProntoUserDefaults userDefaults] setCouldNotDeleteOrUnpulishEventIds:eventCouldNotBeProcessed];
        }
        
        
        if (events != nil && [events isKindOfClass:[NSDictionary class]] == YES && events.count > 0)
        {
            for (NSString *key in [events allKeys])
            {
                NSDictionary *singleEvent = events[key];
                
                //Doing so will stop saving of duplicate events, If any.
                Event *event = [Event MR_findFirstByAttribute:[Event attributeEventIdAsString]
                                                    withValue:key
                                                    inContext:localContext];

                if (event == nil)
                {
                    //Event is already not created. Create and save it.
                    event = [Event MR_createEntityInContext:localContext];
                    [event updatePropertiesWithDictionary:singleEvent inLocalContext:localContext];
                }
                else
                {
                    //Event is already  created. update it.
                    [event updatePropertiesWithDictionary:singleEvent inLocalContext:localContext];
                }
                
            }
        }
        
    } completion:^(BOOL success, NSError *error) {
        
        if (completionHandler != nil)
        {
            completionHandler(YES, nil);
        }
        
        //Last time Events were synced from CMS.
        // Saving last_sync from make sure that whatever events we have got from server
        // it has been saved to local database. Otherwise, it could led to scenario where
        // We got the response from server and somehow we could not save and then when we do
        // next time CMS request, it will not give us events.
        
        [[ProntoUserDefaults userDefaults] setCMSLastSynchronizationForEvents:[dict objectForKey:@"last_sync"]];
    }];
}
/* Get event for event id */
- (Event*)getEventForEventId:(NSString*)eventId
{
    NSPredicate *predicate      = [NSPredicate predicateWithFormat:@"eventId == %@", eventId];
    NSFetchRequest *fetchRequest= [Event MR_requestAllWithPredicate:predicate];
    NSArray *eventArray         = [Event MR_executeFetchRequest:fetchRequest];
    if (eventArray != nil && eventArray.count > 0)
    {
        return  (Event*)[eventArray lastObject];
    }
    return nil;
}

/* Remove thumbnail image for event */
- (void)removeThumnailImageForEvent:(Event*)event
{
    if (event.imageURL != nil)
    {
        NSError *error;
        NSString *imagePath = [[EventsServiceClass sharedAPIManager] getCoverflowImagePath:event.imageURL];
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath])
        {
            // Need to check this whether this is successfully removed
            if ([[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error] == YES)
            {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Deleted Event thumbnail Image %@",event.imageLocalPath]];
            }
            else
            {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Unable to delete Event thumbnail Image at path %@,%@",event.imageLocalPath,error]];
            }
        }
    }
}
/* Save current local context */
- (void)saveCurrentContextWithCompletionHandler:(OperationStatusCompletion)completionHandler
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error)
     {
         if (completionHandler != nil)
         {
             completionHandler(contextDidSave);
         }
     }];
}

#pragma mark -
#pragma mark Event Only Asset Search Results

- (NSMutableArray*)getEventSearchedAssets
{
    return self.eventSearchResults;
}

- (void)resetEventSearchedResults
{
    [self.eventSearchResults removeAllObjects];
}

- (void)prepareGlobalSearchResultsForEvent:(NSDictionary*)dict
{
    if (dict != nil && [dict isKindOfClass:[NSDictionary class]] && dict.count > 0)
    {
        NSArray* ids = [dict allKeys];
        for (NSString *singleId in ids)
        {
            NSDictionary *responseDict = dict[singleId];
            if (responseDict != nil && responseDict.count > 0 && [responseDict isKindOfClass:[NSDictionary class]])
            {
                NSString *type = responseDict[keyType];
                
                if ([type isEqualToString:keyEvent])
                {
                    //parse the data according to Event and store it in eventSearchResults
                    [self parseEventsResponse:responseDict];
                }
                else if ([type isEqualToString:keySession])
                {
                    //parse the data according to Session and store it in eventSearchResults
                    [self parseSessionResponse:responseDict];
                }
                else if ([type isEqualToString:keyEventOnlyAsset])
                {
                    //parse the data according to EventOnlyAsset and store it in eventSearchResults
                    [self parseEventOnlyAssetResponse:responseDict];
                }
            }
        }
    }
    
}

/* Parse event response */
- (void)parseEventsResponse:(NSDictionary*)eventResponse
{
    NSManagedObjectContext *defaultContext = [NSManagedObjectContext MR_defaultContext];
    // Event location, time, same event id should not come multiple time
    NSString *eventId = eventResponse[keyId];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventId == %@", eventId];
    NSArray *events     = [Event MR_findAllWithPredicate:predicate inContext:defaultContext];
    if (events != nil && events.count > 0)
    {
        Event *event = (Event*)[events lastObject];
        if ([event doesEventBelongsFromSelectedFranchise] == YES)
        {
            [self.eventSearchResults addObject:event];
        }
    }
}
/* Parse session response */
- (void)parseSessionResponse:(NSDictionary*)sessionResponse
{
    NSManagedObjectContext *defaultContext = [NSManagedObjectContext MR_defaultContext];
    
    NSString *sessionId = sessionResponse[keyId];
    NSString *eventId   = sessionResponse[keyEventId];
    NSArray *assetIdArray   = sessionResponse[keyEventSessionAssets];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sessionId == %@", sessionId];
    NSArray *sessions      = [Session MR_findAllWithPredicate:predicate inContext:defaultContext];
    if (sessions != nil && sessions.count > 0)
    {
        for (Session *session in sessions)
        {
            NSArray *sessionDurations = session.sessionEventDurations.allObjects;
            BOOL matchFound = NO;
            
            for (EventDuration *eventDuration in sessionDurations)
            {
                NSString *parentEventId = [NSString stringWithFormat:@"%@", eventDuration.parentEvent.eventId];
                if ([parentEventId isEqualToString:eventId])
                {
                    if ([session doesSessionBelongsFromSelectedFranchise] == YES)
                    {
                        [self.eventSearchResults addObject:session];
                        matchFound = YES;
                        break;
                    }
                }
            }
            if (matchFound == YES)
            {
                break;
            }
        }
    }
    
    if(assetIdArray.count > 0)
    {
        for(NSDictionary* assetIdDict in assetIdArray)
        {
            NSString *assetId = assetIdDict[keyTargetId];
            NSArray* assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId];
            if (assetArray != nil && assetArray.count > 0)
            {
                Asset * asset = (Asset*)[assetArray lastObject];
                if([self doesAssetBelongsFromSelectedFranchise:asset])
                {
                    [self.eventSearchResults addObject:asset];
                }
               
            }
        }
    }
}

/* Check asset belongs to selected franchise */
- (BOOL)doesAssetBelongsFromSelectedFranchise: (Asset*)asset
{
    __block BOOL doesBelongs = NO;
    
    if ([[ZMUserActions sharedInstance] isFieldUser] == YES)
    {
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseById:[ZMUserActions sharedInstance].franchiseId];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            //collecting franchise id and brands id's.
            NSArray *selectedFranchise = [franchise getFranchiseAndBrandIds];
            NSMutableArray* franchiseArray = [NSMutableArray array];
            [franchiseArray enumerateObjectsUsingBlock:^(Franchise   *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                NSNumber *franichiseId = [NSNumber numberWithInteger:[obj.franchiseID integerValue]];
                if ([selectedFranchise containsObject:franichiseId])
                {
                    doesBelongs = YES;
                    *stop = YES;
                }
            }];
        }
    }
    else
    {
        //for field us
        NSArray *selectedFranchise = [ZMProntoManager sharedInstance].franchisesAndBrands;
        NSMutableArray* franchiseArray = [NSMutableArray array];
        [franchiseArray enumerateObjectsUsingBlock:^(Franchise   *obj, NSUInteger idx, BOOL * _Nonnull stop) {

            NSNumber *franichiseId = [NSNumber numberWithInteger:[obj.franchiseID integerValue]];
            if ([selectedFranchise containsObject:franichiseId])
            {
                doesBelongs = YES;
                *stop = YES;
            }
        }];
    }
    
    return doesBelongs;
}
/* Parse event onlty asset response */
- (void)parseEventOnlyAssetResponse:(NSDictionary*)eventOnlyAssetResponse
{
    NSManagedObjectContext *defaultContext = [NSManagedObjectContext MR_defaultContext];
    NSString *eao_id = eventOnlyAssetResponse[keyId];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eoa_id == %@", eao_id];
    NSArray *eoaArray      = [EventOnlyAsset MR_findAllWithPredicate:predicate inContext:defaultContext];
    if (eoaArray != nil && eoaArray.count > 0)
    {
        EventOnlyAsset *eoa = (EventOnlyAsset*)[eoaArray lastObject];
        if ([eoa doesEventOnlyAssetBelongsFromSelectedFranchise])
        {
            [self.eventSearchResults addObject:eoa];
        }
    }
}
/* Remove asset of type EOA from DB */
-(void) removeAssetOfTypeEOAFromAssetTableForAssetID:(NSNumber*)assetID andWithCompletionHandler:(OperationStatusCompletion)completionHandler
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assetID == %@", assetID];
        NSArray *assetArray      = [Asset MR_findAllWithPredicate:predicate];
        if(assetArray != nil && assetArray.count > 0)
        {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                for (Asset *asset in assetArray)
                {
                   [asset MR_deleteEntityInContext:localContext];
                }
            } completion:^(BOOL contextDidSave, NSError *error) {
                if (completionHandler != nil)
                {
                    completionHandler(contextDidSave);
                }
            }];
            
        }
    }
/* Get EOA for asset id */
-(EventOnlyAsset*) getEventOnlyAssetForID:(NSString*)assetID
{
    NSPredicate *predicate      = [NSPredicate predicateWithFormat:@"eoa_id == %@", assetID];
    NSFetchRequest *fetchRequest= [EventOnlyAsset MR_requestAllWithPredicate:predicate];
    NSArray *eoaArray         = [EventOnlyAsset MR_executeFetchRequest:fetchRequest];
    if (eoaArray != nil && eoaArray.count > 0)
    {
        return  (EventOnlyAsset*)[eoaArray lastObject];
    }
    return nil;
}
/* Get EOA from global search */
- (NSMutableArray*)getEventOnlyAssetsFromGlobalSearch
{
    return self.eventOnlyAssets;
}
/* Get EOA from global search */
- (void)setEventOnlyAssetsFromGlobalSearch:(NSMutableArray*)eventOnlyAssets
{
    [self.eventOnlyAssets addObjectsFromArray:eventOnlyAssets];
}
/* Reset EOA */
- (void)resetEventOnlyAssets
{
    [self.eventOnlyAssets removeAllObjects];
}
/* Get Assets from global search */
- (NSMutableArray*)getAssetsFromGlobalSearch
{
    return self.assets;
}
/* Set EOA from global search */
- (void)setAssetsFromGlobalSearch:(NSMutableArray*)assets
{
    [self.assets addObjectsFromArray:assets];
}
/* Reset EOA from global search */
- (void)resetAssets
{
    [self.assets removeAllObjects];
}
/* Get All EOA from global search session */
- (NSArray*)getAllEOAsFoundInGlobalSearchForSession:(Session*)session
{
    NSArray *eoasOfSession = [session.eventOnlyAssets allObjects];
    NSMutableArray *eoasFoundInGlobalSearch = [NSMutableArray array];
    NSArray *eoasFromGlobalSearch = [[EventCoreDataManager sharedInstance] getEventOnlyAssetsFromGlobalSearch];
    for (EventOnlyAsset *eoa in eoasOfSession)
    {
        if ([eoasFromGlobalSearch containsObject:eoa])
        {
            [eoasFoundInGlobalSearch addObject:eoa];
        }
    }
    
    return eoasFoundInGlobalSearch;
}
/* Get All assets from global search session */
- (NSArray*)getAllAssetsFoundInGlobalSearchForSession:(Session*)session
{
    NSArray *assetIds = session.assetIds;
    NSMutableArray *assetsOfSession = [NSMutableArray array];
    for (NSString* assetId in assetIds)
    {
        NSArray *assetFromId = [[ZMCDManager sharedInstance]getAssetArrayById:assetId];
        if(assetFromId.count > 0)
        {
            [assetsOfSession addObject:[assetFromId lastObject]];
        }
    }
    NSMutableArray *assetsFoundInGlobalSearchForEvents = [NSMutableArray array];
    NSArray *assetsFromGlobalSearch = [[EventCoreDataManager sharedInstance] getAssetsFromGlobalSearch];
    for (Asset *asset in assetsOfSession)
    {
        if ([assetsFromGlobalSearch containsObject:asset])
        {
            [assetsFoundInGlobalSearchForEvents addObject:asset];
        }
    }
    
    return assetsFoundInGlobalSearchForEvents;
}
/* remove event */
- (void)removeEvent:(Event*)event withCompletionHandler:(OperationStatusCompletion)completionHandler
{
    if (event == nil)
    {
        if (completionHandler != nil)
        {
            completionHandler(NO);
        }
    }
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
         [event MR_deleteEntityInContext:localContext];
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        
        if (completionHandler != nil)
        {
            completionHandler(contextDidSave);
        }
    }];
}

@end
