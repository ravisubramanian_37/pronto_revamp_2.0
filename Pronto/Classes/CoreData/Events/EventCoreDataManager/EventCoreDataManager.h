//
//  EventCoreDataManager.h
//  Pronto
//
//  Created by m-666346 on 04/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@class Event;
@class Session;
@class EventOnlyAsset;

typedef void(^eventSaveCompletion)(BOOL success, NSArray *events);
typedef void(^eventDetailSaveCompletion)(BOOL success, Event *event);

/**
 This Class is responsible for handling all the Event core data operations.
 */
@interface EventCoreDataManager : NSObject

+ (instancetype)sharedInstance;

//Event Global search results
- (NSMutableArray*)getEventSearchedAssets;
- (void)resetEventSearchedResults;

//EventOnlyAsset from Event Global search
- (NSMutableArray*)getEventOnlyAssetsFromGlobalSearch;
- (void)setEventOnlyAssetsFromGlobalSearch:(NSMutableArray*)eventOnlyAssets;
- (void)resetEventOnlyAssets;
- (void)setAssetsFromGlobalSearch:(NSMutableArray*)assets;
- (void)resetAssets;

- (NSArray*)getEvents;
- (Event*)getEventForEventId:(NSString*)eventId;
- (void)removeThumnailImageForEvent:(Event*)event;

- (BOOL)isEventEmptyInLocalDatabase;
- (void)saveEvents:(NSDictionary*)dict withCompletionHandler:(eventSaveCompletion)completionHandler;
- (void)saveAllEvents:(NSDictionary*)dict withCompletionHandler:(eventSaveCompletion)completionHandler;
- (void)saveEventDetail:(NSDictionary*)dict withCompletionHandler:(eventDetailSaveCompletion)completionHandler;
- (void)saveCurrentContextWithCompletionHandler:(OperationStatusCompletion)completionHandler;
- (void)prepareGlobalSearchResultsForEvent:(NSDictionary*)dict;
-(void) removeAssetOfTypeEOAFromAssetTableForAssetID:(NSNumber*)assetID andWithCompletionHandler:(OperationStatusCompletion)completionHandler;
-(EventOnlyAsset*) getEventOnlyAssetForID:(NSString*)assetID;

- (NSArray*)getTidsOfAllSelectedFranchise;
- (NSArray*)getAllEOAsFoundInGlobalSearchForSession:(Session*)session;
- (NSArray*)getAllAssetsFoundInGlobalSearchForSession:(Session*)session;
- (void)removeEvent:(Event*)event withCompletionHandler:(OperationStatusCompletion)completionHandler;

@end
