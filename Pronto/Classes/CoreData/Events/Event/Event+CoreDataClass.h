//
//  Event+CoreDataClass.h
//  Pronto
//
//  Created by m-666346 on 04/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject, Priority;

NS_ASSUME_NONNULL_BEGIN

@interface Event : NSManagedObject

- (NSDate*)eventStartDate;
- (NSDate*)eventEndDate;

- (NSString*)getFormattedDateString;
- (NSString*)getEventOccuranceDateString;
- (NSString*)getFooterColor;
- (NSInteger)getPriority;
- (BOOL)doesThisEventBelongsForCurrentFranchise;
- (BOOL)doesHaveEventDetails;
//- (NSArray*)getSortedDurationForDateString:(NSString*)dateString;
- (NSArray*)getSortedDurationForDateString:(NSDate*)date;
- (NSArray*)sortEventDurationAccordingToStartTime;
- (NSArray*)getUniqueDatesOfAllSessionSortedAccordingToStartTime;

+(NSString*)attributeEventIdAsString; //eventId

- (void)updatePropertiesWithDictionary:(NSDictionary*)dict inLocalContext:(NSManagedObjectContext*)localContext;
- (void)updateEventDetailWithDictionary:(NSDictionary*)dict inLocalContext:(NSManagedObjectContext*)localContext;
- (void)saveEventHavingCompleteDetail:(NSDictionary*)dict inLocalContext:(NSManagedObjectContext*)localContext;

- (BOOL)doesEventBelongsFromSelectedFranchise;

- (NSString*)getEventLocation;
- (NSString*)getEventPlanner;


@end

NS_ASSUME_NONNULL_END

#import "Event+CoreDataProperties.h"
