//
//  Event+CoreDataProperties.m
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Event+CoreDataProperties.h"

@implementation Event (CoreDataProperties)

+ (NSFetchRequest<Event *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Event"];
}

@dynamic changed;
@dynamic created;
@dynamic endDate;
@dynamic eventId;
@dynamic eventPlanner;
@dynamic eventTimezone;
@dynamic imageLocalPath;
@dynamic imageURL;
@dynamic isUpdateAvailable;
@dynamic location;
@dynamic print;
@dynamic published;
@dynamic startDate;
@dynamic title;
@dynamic eventDurations;
@dynamic priorities;

@end
