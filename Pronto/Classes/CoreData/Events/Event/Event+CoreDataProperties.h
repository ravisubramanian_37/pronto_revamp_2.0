//
//  Event+CoreDataProperties.h
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Event+CoreDataClass.h"

@class EventDuration;


NS_ASSUME_NONNULL_BEGIN

@interface Event (CoreDataProperties)

+ (NSFetchRequest<Event *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *changed;
@property (nullable, nonatomic, copy) NSNumber *created;
@property (nullable, nonatomic, copy) NSString *endDate;
@property (nullable, nonatomic, copy) NSString *eventId;
@property (nullable, nonatomic, copy) NSString *eventPlanner;
@property (nullable, nonatomic, copy) NSString *eventTimezone;
@property (nullable, nonatomic, copy) NSString *imageLocalPath;
@property (nullable, nonatomic, copy) NSString *imageURL;
@property (nullable, nonatomic, copy) NSNumber *isUpdateAvailable;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSNumber *print;
@property (nullable, nonatomic, copy) NSNumber *published;
@property (nullable, nonatomic, copy) NSString *startDate;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) NSSet<EventDuration *> *eventDurations;
@property (nullable, nonatomic, retain) NSSet<Priority *> *priorities;

@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addEventDurationsObject:(EventDuration *)value;
- (void)removeEventDurationsObject:(EventDuration *)value;
- (void)addEventDurations:(NSSet<EventDuration *> *)values;
- (void)removeEventDurations:(NSSet<EventDuration *> *)values;

- (void)addPrioritiesObject:(Priority *)value;
- (void)removePrioritiesObject:(Priority *)value;
- (void)addPriorities:(NSSet<Priority *> *)values;
- (void)removePriorities:(NSSet<Priority *> *)values;

@end

NS_ASSUME_NONNULL_END
