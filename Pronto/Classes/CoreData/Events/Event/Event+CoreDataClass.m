//
//  Event+CoreDataClass.m
//  Pronto
//
//  Created by m-666346 on 04/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Event+CoreDataClass.h"
#import "Pronto-Swift.h"
#import "NSDate+DateDirectory.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Priority+CoreDataClass.h"
#import "ZMUserActions.h"
#import "ProntoUserDefaults.h"
#import "Asset.h"

#import "Priority+CoreDataProperties.h"
#import "Franchise+CoreDataProperties.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

@implementation Event

+(NSString*)attributeEventIdAsString
{
    return @"eventId";
}
/* Saving event having complete detail */
- (void)saveEventHavingCompleteDetail:(NSDictionary*)dict inLocalContext:(NSManagedObjectContext*)localContext
{
    [self updatePropertiesWithDictionary:dict inLocalContext:localContext];
    [self updateEventDetailWithDictionary:dict inLocalContext:localContext];
    
}
- (void)updatePropertiesWithDictionary:(NSDictionary*)dict inLocalContext:(NSManagedObjectContext*)localContext
{
    NSInteger currentChanged = [dict[@"changed"] integerValue];
    
    if (self.changed.integerValue > 0 &&
        currentChanged > self.changed.integerValue)
    {
        self.isUpdateAvailable = [NSNumber numberWithBool:YES];
    }
    else
    {
        self.isUpdateAvailable = [NSNumber numberWithBool:NO];
    }
    
    self.eventId = dict[@"id"];
    self.changed = [NSNumber numberWithInteger:[dict[@"changed"] integerValue]];
    self.created = [NSNumber numberWithInteger:[dict[@"created"] integerValue]];
    if([dict valueForKey:@"location"] == [NSNull null])
    {
        self.location = @"";
    }
    else
    {
        self.location = dict[@"location"];
    }
    self.title    = dict[@"title"];
    self.imageURL = dict[@"image_url"];
    
    self.eventTimezone = [dict valueForKeyPath:@"duration.timezone"];
    self.startDate     = [dict valueForKeyPath:@"duration.start_date"];
    self.endDate       = [dict valueForKeyPath:@"duration.end_date"];
    
    //As it is in process of creating and saving the entity, No need to save franchise in different block.
    NSArray *priorities = dict[@"franchise"];
    if (priorities != nil &&
        [priorities isKindOfClass:[NSArray class]] &&
        priorities.count > 0)
    {
        NSMutableArray *tempPriorities = [NSMutableArray array];
        for (NSDictionary *priorityDict in priorities)
        {
            Priority *priority      = [Priority MR_createEntityInContext:localContext];
            priority.priorityId     = [NSNumber numberWithInteger:[priorityDict[@"pid"] integerValue]];
            priority.franchiseTid   = [NSNumber numberWithInteger:[priorityDict[@"tid"] integerValue]];
            priority.parentEvent    = self;
            
            [tempPriorities addObject:priority];
        }
        
        if (tempPriorities.count > 0)
        {
            self.priorities = nil;
            [self addPriorities:[NSSet setWithArray:tempPriorities]];
        }
    }
}
/* Update event details with dictionary */
- (void)updateEventDetailWithDictionary:(NSDictionary*)dict inLocalContext:(NSManagedObjectContext*)localContext
{
    NSInteger currentChanged = [dict[@"changed"] integerValue];
    if (self.changed.integerValue > 0 &&
        currentChanged > self.changed.integerValue)
    {
        self.isUpdateAvailable = [NSNumber numberWithBool:YES];
    }
    else
    {
        self.isUpdateAvailable = [NSNumber numberWithBool:NO];
    }
    
    self.changed = [NSNumber numberWithInteger:[dict[@"changed"] integerValue]];
    if([dict valueForKey:@"planner"] == [NSNull null])
    {
      self.eventPlanner = @"";
    }
    else
    {
      self.eventPlanner = dict[@"planner"];
    }
    self.print = [NSNumber numberWithBool:[dict[@"print"] boolValue]];
    
    self.eventTimezone = [dict valueForKeyPath:@"duration.timezone"];
    self.startDate     = [dict valueForKeyPath:@"duration.start_date"];
    self.endDate       = [dict valueForKeyPath:@"duration.end_date"];
    
    NSArray *sessions = dict[@"sessions"];

    //if  (sessions != nil && [sessions isKindOfClass:[NSDictionary class]] && sessions.count > 0)
    if  (sessions != nil && sessions.count > 0)
    {
        NSMutableArray *eventDurationTempArray  = [NSMutableArray array];
        NSMutableArray *eventOnlyAssetTempArray = [NSMutableArray array];
        
        for (int i = 0; i < [sessions count]; i++)
        {
            NSDictionary *singleSession = [sessions objectAtIndex:i];
             if  (singleSession != nil &&
                  [singleSession isKindOfClass:[NSDictionary class]] &&
                  singleSession.count > 0)
             {
                 NSMutableArray *eventDurationsForSingleSession = [NSMutableArray array];
                 Session *session = [Session MR_findFirstByAttribute:[Session IdAttributeAsString]
                                                                                withValue:singleSession[@"id"]
                                                                                inContext:localContext];
                 
                 if (session == nil)
                 {
                    session    = [Session MR_createEntityInContext:localContext];
                 }
                 
                 //Session Related Properties update
                 session.sessionId   = singleSession[@"id"];
                 session.title       = singleSession[@"title"];
                 session.sessionDescription = singleSession[@"description"];  //change desc to description
                 if([singleSession valueForKey:@"location"] == [NSNull null])
                 {
                     session.location = @"";
                 }
                 else
                 {
                     session.location = singleSession[@"location"];
                 }
                 NSArray *allAssets = singleSession[@"session_assets"];
                 
                 //Event duration related
                 NSArray *durations = singleSession[@"duration"];
                 if (durations != nil &&
                     [durations isKindOfClass:[NSArray class]] &&
                     durations.count > 0)
                 {
                     for (NSDictionary *duration in durations)
                     {
                         EventDuration *eventDuration = [EventDuration MR_createEntityInContext:localContext];
                         //Properties
                         eventDuration.parentEventId = self.eventId;
                         eventDuration.date = duration[@"date"];  //change session_date to date
                         eventDuration.startTime     = duration[@"start"]; //change session_start_time to start
                         eventDuration.endTime       = duration[@"end"]; //change session_end_time to end
                         
                         //Relationship
                         eventDuration.parentEvent   = self;
                         eventDuration.session       = session;
                         eventDuration.sessionId = singleSession[@"id"];
                         //Relation: One session has many event duration and
                         // All sessions duration is collection of event duration.
                         
                         //EventDuration for session
                         [eventDurationsForSingleSession addObject:eventDuration];
                         
                         //Event duration for Event
                         [eventDurationTempArray addObject:eventDuration];
                     }
                     
                     if (eventDurationsForSingleSession.count > 0)
                     {
                         //Removing old entries if any
                         [session addSessionEventDurations:[NSSet setWithArray:eventDurationsForSingleSession]];
                         [eventDurationsForSingleSession removeAllObjects];
                     }
                 }
                 
                 NSMutableArray *eventAssets = [[NSMutableArray alloc] init];
                 NSMutableArray *assetList = [[NSMutableArray alloc] init];
                 if (allAssets != nil && allAssets.count > 0)
                 {
                     for (int i = 0; i < [allAssets count]; i++)
                     {
                         if([[allAssets objectAtIndex:i]objectForKey:@"assetID"]){
                             NSLog(@"asset Available"); //dictionary type object
                             [assetList addObject:[allAssets objectAtIndex:i]];
                         } else {
                             [eventAssets addObject:[allAssets objectAtIndex:i]];
                             NSLog(@"event only asset Available");
                         }
                     }
                 }
                     
                  
                 session.assetIds = assetList; //Nandana change
                 
                 session.assetIds = [NSArray arrayWithArray:assetList];
                [self setNormalAssetIsViewedPropertiesForAssetIds:session.assetIds inLocalContext:localContext];
                 
                 //Event only related.
                 
                 NSArray *eventOnlyAssets = [NSArray arrayWithArray:eventAssets];
                 if (eventOnlyAssets != nil && eventOnlyAssets.count > 0)
                 {
                     for (int i = 0; i < [eventOnlyAssets count]; i++)
                     {
                         NSDictionary *singleEventOnlyAssetDict = [eventOnlyAssets objectAtIndex:i];
                         if (singleEventOnlyAssetDict != nil &&
                             [singleEventOnlyAssetDict isKindOfClass:[NSDictionary class]] &&
                             singleEventOnlyAssetDict.count > 0)
                         {
                             
                             EventOnlyAsset *eventOnlyAsset = [EventOnlyAsset MR_findFirstByAttribute:[EventOnlyAsset IdAttributeAsString]
                                                                                            withValue:singleEventOnlyAssetDict[@"eoa_id"]
                                                                                            inContext:localContext];
                             
                             if (eventOnlyAsset == nil)
                             {
                                 //Event is already not created. Create and save it.
                                 eventOnlyAsset = [EventOnlyAsset MR_createEntityInContext:localContext];
                                 [eventOnlyAsset updateEventOnlyAssetWithDictionary:singleEventOnlyAssetDict inLocalContext:localContext];
                             }
                             else
                             {
                                 //Event is already  created. update it.
                                  [eventOnlyAsset updateEventOnlyAssetWithDictionary:singleEventOnlyAssetDict inLocalContext:localContext];
                             }
                             
                             //Adding relationship between EventOnlyAsset and session.
                             [eventOnlyAsset addEoa_sessionsObject:session];
                             
                             [eventOnlyAssetTempArray addObject:eventOnlyAsset];
                         }
                     }
                     //establishing relationship with session and eventOnlyAsset
                     if (eventOnlyAssetTempArray.count > 0)
                     {
                         //Removing old entries if any
                         NSSet *oldSessionEOA = session.eventOnlyAssets;
                         if(oldSessionEOA != nil)
                         {
                             [session removeEventOnlyAssets:oldSessionEOA];
                         }
                         
                         [session addEventOnlyAssets:[NSSet setWithArray:eventOnlyAssetTempArray]];
                         [eventOnlyAssetTempArray removeAllObjects];
                     }
                 }
                 else
                 {
                     //Remove all the earlier eoas if the response is empty
                     NSSet *oldSessionEOA = session.eventOnlyAssets;
                     if(oldSessionEOA != nil)
                     {
                         [session removeEventOnlyAssets:oldSessionEOA];
                     }
                 }
             }
        }
        //establishing relationship with event and eventduration
        if (eventDurationTempArray.count > 0)
        {
            //Removing old entries if any
            NSSet *oldEventDuration = self.eventDurations;
            if(oldEventDuration != nil)
            {
                [self removeEventDurations:oldEventDuration];
            }
            
            //Adding new Entries
            [self addEventDurations:[NSSet setWithArray:eventDurationTempArray]];
            [eventDurationTempArray removeAllObjects];
        }
        else
        {
            //Remove all the earlier durations if the response is empty
            NSSet *oldEventDuration = self.eventDurations;
            if(oldEventDuration != nil)
            {
                [self removeEventDurations:oldEventDuration];
            }
        }
    }
    else
    {
        //Removing all the event durations, sessions and eoa for the event list empty response
        NSSet* eventDurationsToRemove = self.eventDurations;
        for(EventDuration* duration in eventDurationsToRemove)
        {
            Session* session = duration.session;
            NSSet* eoasToRemove = session.eventOnlyAssets;
            if(eoasToRemove != nil)
            {
                [session removeEventOnlyAssets:eoasToRemove];
            }
        }
        if(eventDurationsToRemove != nil)
        {
            [self removeEventDurations:eventDurationsToRemove];
        }
    }
}
/* Check assetId is available for isViewed propoerty */
- (void)setNormalAssetIsViewedPropertiesForAssetIds:(NSArray*)assetIds inLocalContext:(NSManagedObjectContext*)localContext
{
    if (assetIds.count <= 0) { return; }
    
    NSArray *viewedAssetIds = [[ProntoUserDefaults userDefaults] getViewedNormalAssetIds];
    NSMutableArray *viewedAssetToBeSaved = [NSMutableArray arrayWithArray:viewedAssetIds];
    NSMutableArray *assetTobeSaved       = [NSMutableArray array];
    
    for (NSNumber *assetId in assetIds)
    {
        NSString *assetIdAsString = [NSString stringWithFormat:@"%@", [assetId valueForKey:@"assetID"]];
        
        if ([self doesAssetIds:viewedAssetIds containsAssetId:assetIdAsString])
        {
            Asset *asset = [Asset MR_findFirstByAttribute:@"assetID" withValue:[assetId valueForKey:@"assetID"] inContext:localContext];
            
            if (asset != nil )
            {
                asset.isViewed = [NSNumber numberWithBool:YES];
                [assetTobeSaved addObject:asset];
                
                NSArray *tempArray = [self removeAssetId:assetIdAsString fromAssetIds:viewedAssetToBeSaved];
                [viewedAssetToBeSaved removeAllObjects];
                [viewedAssetToBeSaved addObjectsFromArray:tempArray];
            }
        }
    }
    
    if (assetTobeSaved.count > 0)
    {
        //Saved Remaining asset
        [[ProntoUserDefaults userDefaults] setViewedNormalAssetIds:viewedAssetToBeSaved];
    }
}

/* Check assets ids available */
- (BOOL)doesAssetIds:(NSArray*)assetIds containsAssetId:(NSString*)assetId
{
    __block BOOL isAvailable = NO;
    [assetIds enumerateObjectsUsingBlock:^(NSString *singleAssetId, NSUInteger idx, BOOL *stop) {
        
        if ((singleAssetId.integerValue == assetId.integerValue) == YES) {
            *stop = YES;
            isAvailable = YES;
        }
        
    }];
    return isAvailable;
}
/* Remove asset ids */
- (NSArray*)removeAssetId:(NSString*)assetId fromAssetIds:(NSArray*)assetIds
{
    __block NSMutableArray *copyAssetIds = [NSMutableArray arrayWithArray:assetIds];
    
    [assetIds enumerateObjectsUsingBlock:^(NSString *singleAssetId, NSUInteger idx, BOOL *stop) {
        
        if ((singleAssetId.integerValue == assetId.integerValue) == YES) {

            [copyAssetIds removeObject:singleAssetId];
            *stop = YES;
        }
    }];
    
    return copyAssetIds;
}

#pragma mark - Helper Methods
/* Get formatted date string for events */
- (NSString*)getFormattedDateString
{
    NSDate *startDate = [NSDate GetDateSameAsStringFromDateString:self.startDate];
    NSDate *endDate   = [NSDate GetDateSameAsStringFromDateString:self.endDate];
    
    NSString *monthOfStartDate = [NSDate GetMonthInMMMFormateFromDate:startDate];
    NSString *monthOfEndDate   = [NSDate GetMonthInMMMFormateFromDate:endDate];
    
    NSString *dateOfStartDate  = [NSString stringWithFormat:@"%ld",(long) [NSDate GetDateInUTCFromDate:startDate]];
    NSString *dateOfEndDate    = [NSString stringWithFormat:@"%ld", (long)[NSDate GetDateInUTCFromDate:endDate]];
    
    NSString *yearOfStartDate  = [NSString stringWithFormat:@"%ld", (long)[NSDate GetYearFromDate:startDate]];
    NSString *yearOfEndDate    = [NSString stringWithFormat:@"%ld", (long)[NSDate GetYearFromDate:endDate]];
    
    NSString *formattedEventRangeString = [NSString stringWithFormat:@"%@ %@ %@ - %@ %@ %@", monthOfStartDate, dateOfStartDate, yearOfStartDate, monthOfEndDate, dateOfEndDate, yearOfEndDate];
    
    return formattedEventRangeString;
}
/* Get event occurence date string */
- (NSString*)getEventOccuranceDateString
{
    NSDate *date = [NSDate GetDateSameAsStringFromDateString:self.startDate];
    NSString *monthInMMMM = [NSDate GetMonthInMMMMFormateFromDate:date];
    NSString *year        = [NSString stringWithFormat:@"%ld",(long)[NSDate GetYearFromDate:date]];
    NSString *eventsOccurance = [NSString stringWithFormat:@"%@  %@", monthInMMMM, year];
    return eventsOccurance;
}
/* Get event start date */
- (NSDate*)eventStartDate
{
    return  [NSDate GetDateSameAsStringFromDateString:self.startDate];
}
/* Get event date */
- (NSDate*)eventEndDate
{
    return [NSDate GetDateSameAsStringFromDateString:self.endDate];
}
/* Get footer color */
- (NSString*)getFooterColor
{
    NSString *footerColor = @"0d1c49";  // Default footer color
    for (Priority  *priority in [self.priorities allObjects])
    {
        NSString *franchiseTid = [NSString stringWithFormat:@"%@", priority.franchiseTid];
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseByTid:franchiseTid];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            if ([franchise.field_franchise_color length] > 0)
            {
                footerColor = franchise.field_franchise_color;
                break;
            }
        }
    }
    return footerColor;
}

//For Field user, priority will be taken  franchise id of current logged in user;
- (NSInteger)getPriority
{
    NSInteger eventPriority = 0;
    
    for (Priority *priority in [self.priorities allObjects])
    {
        NSString *franchiseTid = [NSString stringWithFormat:@"%@", priority.franchiseTid];
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseByTid:franchiseTid];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            NSString *franchiseId = [NSString stringWithFormat:@"%@",franchise.tid];
            if ([franchiseId isEqualToString:[ZMUserActions sharedInstance].franchiseId])
            {
                eventPriority = [priority.priorityId integerValue];
                break;
            }
        }
    }
    
    return eventPriority;
}
/* Check this event belongs to selected franchise */
- (BOOL)doesThisEventBelongsForCurrentFranchise
{
    BOOL doesBelongs = NO;
    
    for (Priority *priority in [self.priorities allObjects])
    {
        NSString *franchiseTid = [NSString stringWithFormat:@"%@", priority.franchiseTid];
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseByTid:franchiseTid];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            NSString *franchiseId = [NSString stringWithFormat:@"%@",franchise.franchiseID];
            if ([franchiseId isEqualToString:[ZMUserActions sharedInstance].franchiseId])
            {
                doesBelongs = YES;
                break;
            }
        }
    }
    
    return doesBelongs;
}
/* Check event duration have all event details */
- (BOOL)doesHaveEventDetails
{
    if (self.eventDurations != nil && [self.eventDurations allObjects].count > 0)
    {
        return YES;
    }
    return NO;
}
/* Get Sorted event duration according to date string */
- (NSArray*)getSortedDurationForDateString:(NSDate*)date;
{
    
    NSMutableArray *temp = [NSMutableArray array];
    
    for (EventDuration *eventDuration in [self.eventDurations allObjects])
    {
        NSDate *eventDurationDate = [eventDuration eventDate];
        
        if ([NSDate isDate1:date equalToDate:eventDurationDate] &&
            eventDuration.session != nil)
        {
            [temp addObject:eventDuration];
        }
    }
    
    if (temp.count > 0)
    {
        NSArray *durations = (NSArray*)[self sortEventDurationAccordingToStartTimeAndTitle:temp];
        return durations;
    }
    
    //Returning empty array as of now
    return @[];
}
/* Sort event duration according to start time and title */
- (NSMutableArray*)sortEventDurationAccordingToStartTimeAndTitle:(NSMutableArray*)eventDurations
{
    
    [eventDurations sortUsingComparator:^NSComparisonResult(EventDuration *obj1, EventDuration *obj2) {
        
        //start date and title
        NSComparisonResult result = [obj1.startTime compare:obj2.startTime options:NSNumericSearch | NSCaseInsensitiveSearch];
        
        if (result == -1)
        {
            return NSOrderedAscending;
        }
        else if (result == 1)
        {
            return NSOrderedDescending;
        }
        else  // Same 10:30 = 10:30
        {
            NSComparisonResult result = [obj1.session.title compare:obj2.session.title options:NSNumericSearch | NSCaseInsensitiveSearch];
            
            if (result == -1)
            {
                return NSOrderedAscending;
            }
            else if (result == 1)
            {
                return NSOrderedDescending;
            }
            return NSOrderedSame;
        }
        
    }];
    
    return eventDurations;
}

/* Sort event duration according to start time */
- (NSArray*)sortEventDurationAccordingToStartTime
{
    NSMutableArray *mutableEventDurations = [NSMutableArray arrayWithArray:[self.eventDurations allObjects]];
    [mutableEventDurations sortUsingComparator:^NSComparisonResult(EventDuration *obj1, EventDuration *obj2) {
        
        // date
        NSDate *eventDurationDate1 = [obj1 eventDate];
        NSDate *eventDurationDate2 = [obj2 eventDate];
        
        if ([NSDate isDate1:eventDurationDate1 greaterThanDate:eventDurationDate2] )
        {
            return NSOrderedDescending ;
        }
        else if ([NSDate isDate1:eventDurationDate1 equalToDate:eventDurationDate2])
        {
            return NSOrderedSame;
        }
        return NSOrderedAscending;
        
    }];
    
    return [NSArray arrayWithArray:mutableEventDurations];
}
/* Get unqiue dates of all session sorted according to start time */
- (NSArray*)getUniqueDatesOfAllSessionSortedAccordingToStartTime
{
    NSMutableSet *uniqueDateSet = [NSMutableSet set];
    for (EventDuration *eventDuration in [self.eventDurations allObjects])
    {
         [uniqueDateSet addObject:[eventDuration eventDate]];
    }
    //check if it empty
    
    if ([uniqueDateSet allObjects].count > 0)
    {
        NSMutableArray *sortedDates = [NSMutableArray arrayWithArray:[uniqueDateSet allObjects]];
        //Sorting dates according time
        
        [sortedDates sortUsingComparator:^NSComparisonResult(NSDate *eventDurationDate1, NSDate *eventDurationDate2) {
            
            if ([NSDate isDate1:eventDurationDate1 greaterThanDate:eventDurationDate2] )
            {
                return NSOrderedDescending ;
            }
            else if ([NSDate isDate1:eventDurationDate1 equalToDate:eventDurationDate2])
            {
                return NSOrderedSame;
            }
            return NSOrderedAscending;
            
        }];
        
        return (NSArray*)sortedDates;
    }
    
    return @[];
}
/* Get events that belongs to selected franchise */
- (BOOL)doesEventBelongsFromSelectedFranchise
{
    BOOL doesBelongs = NO;
    
    if ([[ZMUserActions sharedInstance] isFieldUser] == YES)
    {
        doesBelongs = [self doesThisEventBelongsForCurrentFranchise];
    }
    else
    {
        NSArray *selectedFranchise = [ZMProntoManager sharedInstance].franchisesAndBrands;
        NSArray *tids = [self getTidsForFranchises:selectedFranchise];
        
        NSArray *priorities = [self.priorities allObjects];
        for (Priority *priority in priorities)
        {
            if ([tids containsObject:priority.franchiseTid])
            {
                doesBelongs = YES;
                break;
            }
        }
    }
    return doesBelongs;
}
/* Get tids for franchises */
- (NSArray*)getTidsForFranchises:(NSArray*)franchise
{
    NSMutableArray *tids = [NSMutableArray array];
    for (NSString *franchiseId in franchise)
    {
        NSArray *franchises = [[ZMCDManager sharedInstance] getFranchiseById:[NSString stringWithFormat:@"%@", franchiseId]];
        if (franchises != nil && franchises.count > 0)
        {
            Franchise *franchise = (Franchise*)franchises.lastObject;
            [tids addObject:franchise.tid];
        }
    }
    return tids;
}
/* Get event location */
- (NSString*)getEventLocation
{
    if (self.location == nil ||
        self.location.length <= 0)
    {
        return @"";
    }
    return self.location;
}
/* Get event planner */
- (NSString*)getEventPlanner
{
    if (self.eventPlanner == nil ||
        self.eventPlanner.length <= 0)
    {
        return @"";
    }
    return self.eventPlanner;
}
@end

