//
//  Priority+CoreDataProperties.m
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Priority+CoreDataProperties.h"

@implementation Priority (CoreDataProperties)

+ (NSFetchRequest<Priority *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Priority"];
}

@dynamic franchiseTid;
@dynamic priorityId;
@dynamic parentEvent;

@end
