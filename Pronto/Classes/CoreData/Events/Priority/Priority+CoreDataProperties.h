//
//  Priority+CoreDataProperties.h
//  Pronto
//
//  Created by m-666346 on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import "Priority+CoreDataClass.h"

@class Event;

NS_ASSUME_NONNULL_BEGIN

@interface Priority (CoreDataProperties)

+ (NSFetchRequest<Priority *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *franchiseTid;
@property (nullable, nonatomic, copy) NSNumber *priorityId;
@property (nullable, nonatomic, retain) Event *parentEvent;

@end

NS_ASSUME_NONNULL_END
