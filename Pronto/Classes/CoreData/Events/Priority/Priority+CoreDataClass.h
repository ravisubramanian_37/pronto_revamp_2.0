//
//  Priority+CoreDataClass.h
//  Pronto
//
//  Created by m-666346 on 04/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Priority : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Priority+CoreDataProperties.h"
