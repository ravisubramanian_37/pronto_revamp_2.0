//
//  Tags+CoreDataProperties.h
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Tags.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tags (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *key;
@property (nullable, nonatomic, retain) NSSet<Asset *> *asset;

@end

@interface Tags (CoreDataGeneratedAccessors)

- (void)addAssetObject:(Asset *)value;
- (void)removeAssetObject:(Asset *)value;
- (void)addAsset:(NSSet<Asset *> *)values;
- (void)removeAsset:(NSSet<Asset *> *)values;

@end

NS_ASSUME_NONNULL_END
