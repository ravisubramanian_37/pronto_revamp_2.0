//
//  FavFranchise+CoreDataClass.h
//  Pronto
//
//  Created by cccuser on 01/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset;

NS_ASSUME_NONNULL_BEGIN

@interface FavFranchise : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "FavFranchise+CoreDataProperties.h"
