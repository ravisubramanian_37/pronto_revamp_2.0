//
//  Franchise.h
//  Pronto
//
//  Created by osmarogo on 1/4/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset, Brand;

NS_ASSUME_NONNULL_BEGIN

@interface Franchise : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
- (NSArray*)getFranchiseAndBrandIds;

@end

NS_ASSUME_NONNULL_END

#import "Franchise+CoreDataProperties.h"
