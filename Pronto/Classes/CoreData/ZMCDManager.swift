//
//  ZMCDManager.swift
//  Pronto
//
//  Created by Oscar Robayo on 1/5/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//

import Foundation

@objc class ZMCDManager: NSObject {
    @objc static let sharedInstance = ZMCDManager()
    @objc func getSearchedAssets(_ offSet: Int, limit: Int, success: (_ data: [AnyObject]) -> ()) {
        let currentTab = Constant.getCurrentActiveTabOptionAsString()
        var categories = [AnyObject]()
         if(currentTab == ProntoTabOptionFranchise) {
             categories = ZMProntoManager.sharedInstance.selectedCategories
         } else {
          if let unwrapCategories = ZMUserActions.sharedInstance().currentFilter.selectedCategories
          {
            categories = unwrapCategories as [AnyObject]
          }
        }
        let assetsToFilter = ZMProntoManager.sharedInstance.assetsIdToFilter
        let textToSearch = ZMProntoManager.sharedInstance.search.getTitle()
        var franchisesToFilter = [AnyObject]()
        
        // If fav is tapped
        var isFav = false
        if (categories.count > 1) {
            if(categories[1] as? Int == 1) {
                isFav = true
                categories.removeAll()
            }
        }

        var sortingArray: [AnyObject] = ZMProntoManager.sharedInstance.sortBy
        sortingArray = ZMProntoManager.sharedInstance.sortBy.filter{ $0 ["selected"] as! String == "1"}
        var isAscending = true
        var sortingTerm = String()
        let assetArrayIds = ZMProntoManager.sharedInstance.assetArrayIds
        switch ZMProntoManager.sharedInstance.search.currentView {
        case 1:
            let franchises = getFranchises()
            franchisesToFilter = getIdsToFranchisesAndBrands(franchises)
        default:
            franchisesToFilter = ZMProntoManager.sharedInstance.franchisesAndBrands
        }
        
        //Search for favourite in home office
        if(isFav && AOFLoginManager.sharedInstance.salesFranchiseID == "") {
            let franArray = getFranchises()
            for franchiseVal in franArray {
                let franchise = franchiseVal as! Franchise
                franchisesToFilter.append(franchise.tid!)
                franchisesToFilter.append(franchise.franchiseID!)
            }
        }
        
        if let sortBy = sortingArray.first?.value(forKey: "label") {
            sortingTerm = sortBy as! String
        }else{
            sortingTerm = "Recently Added"
        }
        
        //2.
        let assetSortDescriptor: NSSortDescriptor = {
            var keySortDescriptor = String()
            switch sortingTerm {
            case "Recently Added":
                keySortDescriptor = "publish_on"
                isAscending = false
                break
            case "Most Viewed":
                keySortDescriptor = "view_count"
                isAscending = false
                break
            case "Recently Updated":
                keySortDescriptor = "changed"
                isAscending = false
                break
            default:
                break
            }
            
            // check the sorting term
            ZMUserActions.sharedInstance().sortingKey = keySortDescriptor
            
            let sd = NSSortDescriptor(key: keySortDescriptor, ascending: isAscending)
            return sd
        }()
                
        let assetSortDescriptorMostPopular: NSSortDescriptor = NSSortDescriptor(key: "popular", ascending: false)
        
        //3.
        let predicate: NSPredicate? = {
            var predicateDescriptor: NSPredicate?
    
            predicateDescriptor =
                textToSearch.isEmpty ?
                    categories.isEmpty ?
                        assetsToFilter.isEmpty ?
            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)", franchisesToFilter,franchisesToFilter)
                            : NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@) AND (assetID IN %@)", franchisesToFilter,franchisesToFilter, assetsToFilter)
                        : assetsToFilter.isEmpty ?
            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                          "AND (ANY categories.categoryID IN %@) OR (assetID IN %@) ",
                        franchisesToFilter,franchisesToFilter, categories,assetsToFilter) :
            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                          "AND (ANY categories.categoryID IN %@) AND (ANY assetID IN %@) ",
                        franchisesToFilter,franchisesToFilter, categories, assetsToFilter)
                : categories.isEmpty ?
                    assetsToFilter.isEmpty ?
            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                          "AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@) OR (assetID IN %@) OR (assetID IN %@))",
                        franchisesToFilter,franchisesToFilter,
                        textToSearch,textToSearch,assetsToFilter,assetArrayIds) :
            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                          "AND (assetID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                        franchisesToFilter,franchisesToFilter, assetsToFilter, textToSearch,textToSearch)
                : assetsToFilter.isEmpty ?
            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                          "AND (ANY categories.categoryID IN %@) AND ((title CONTAINS[cd] %@)" +
                          "OR (tags.key CONTAINS[cd] %@) OR (assetID IN %@))",
                        franchisesToFilter,franchisesToFilter,
                        categories, textToSearch,textToSearch,assetArrayIds) :
            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                          "AND (ANY categories.categoryID IN %@) AND (assetID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                        franchisesToFilter,franchisesToFilter, categories, assetsToFilter,textToSearch,textToSearch);
            
            print("searchPredicateDescriptor:",predicateDescriptor!);
            return predicateDescriptor
        }()
        
        
    
        let request: NSFetchRequest =  Asset.mr_requestAll(with: predicate!)
        
        if sortingTerm == "Recently Added"{
            request.sortDescriptors = [assetSortDescriptorMostPopular,assetSortDescriptor]
        }else{
            request.sortDescriptors = [assetSortDescriptor]
        }
      
        //If fav is tapped
        var savedAssets = Asset.mr_execute(request)
        
        if (isFav) {
            var count = 0
            for asset in savedAssets! {
                var isPresent = false
                let ass = asset as! Asset
                let favoritefranchise = getFavFranchise()
                for fav in favoritefranchise {
                    let favFar = fav as! FavFranchise
                    if(ass.assetID! == favFar.asset_ID!) {
                        isPresent = true
                        break
                    }
                }
                if (!isPresent) {
                    savedAssets?.remove(at: count)
                }
                else {
                    count += 1
                }
            }
        }
        
        if let unwrappedSavedAssets = savedAssets
        {
            if let assets = unwrappedSavedAssets as? [Asset]
            {
                let filteredAssets  = self.removeDuplicatesOfAssets(assets)
                success(filteredAssets)
            }
            else
            {
                success([]) // Empty array
            }
        }
        else
        {
            success([]) // Empty array
        }
    
    }

    func getAssetsWithFilterForNewImplementation(isFromSearch:Bool, assetsArray:[Asset], andCompletionHandler  completionHandler:(_ data:[AnyObject]) -> ()){
        var sortingKey:String?
        if(isFromSearch){
            sortingKey = UserDefaults.standard.value(forKey: RedesignConstants.searchSortingTermKey) as? String
            
        }else{
            sortingKey = UserDefaults.standard.value(forKey: RedesignConstants.sortingTermKey) as? String
        }
        let sortingTerm:String = sortingKey!
        var keySortDescriptor = String()
        switch sortingTerm {
        case RedesignConstants.SortingOptions.recentlyAdded.rawValue:
             keySortDescriptor = "publish_on"
        case RedesignConstants.SortingOptions.mostViewed.rawValue:
            keySortDescriptor = "view_count"
        case RedesignConstants.SortingOptions.recentlyUpdated.rawValue:
            keySortDescriptor = "changed"
        default:
            break;
        }
        
//        assetsArray.sorted(by: { $0.publish_on > $1.publish_on })
        
        let predicate: NSPredicate? = {
        var predicateDescriptor: NSPredicate?
        predicateDescriptor = NSPredicate(format: "(ANY assetID IN %@)", assetsArray)
        return predicateDescriptor
        }()
        let sortDescriptor = NSSortDescriptor(key: keySortDescriptor , ascending: false)
        //TODO:Uncomment Below with real Data

        let request: NSFetchRequest =  Asset.mr_requestAll()
        request.sortDescriptors = [sortDescriptor]
         
        let savedAssets = Asset.mr_execute(request)
        if let unwrappedSavedAssets = savedAssets{
            let filteredAssets = self.removeDuplicatesOfAssets(unwrappedSavedAssets as? [Asset] ?? [])
            completionHandler(filteredAssets)
        }else{
            completionHandler([])
        }

    }
  }
extension ZMCDManager {
    @objc func getAssetsWithFilter(_ filter: Filter,
                                   offSet theOffSet: Int, limit theLimit: Int,
                                   andCompletionHandler  completionHandler:(_ data: [AnyObject]) -> ())
    {
        //1.
        let favArrayIds: [AnyObject] = filter.favoriteIdsArray! as [AnyObject]
        let categories: [AnyObject] = filter.selectedCategories! as [AnyObject]
        let assetsToFilter: [String] = filter.assetsIdsToFilter as! [String]
        // Search is moved to global search and it's not belong to competitor and tools training tab.
        let textToSearch:String         = ""
        var franchisesToFilter = [Any]()
        
        var sortingArray: [AnyObject] = ZMProntoManager.sharedInstance.sortBy
        sortingArray = ZMProntoManager.sharedInstance.sortBy.filter{$0 ["selected"] as! String == "1"}
        
        var isAscending = true
        var sortingTerm = String()
        
        print("\(filter.description)")
      
        switch ZMProntoManager.sharedInstance.search.currentView {
        case 1:
            let franchises = getFranchises()
            franchisesToFilter = getIdsToFranchisesAndBrands(franchises)
        default:
            if (ZMUserActions.sharedInstance().isFieldUser == true)
            {
                franchisesToFilter = ZMProntoManager.sharedInstance.franchisesAndBrands
            }
            else
            {
                franchisesToFilter =  PreferencesVCViewModel.getFranchisesAndBrands()
            }
        }
        if let sortBy = sortingArray.first?.value(forKey: "label") {
            sortingTerm = sortBy as! String
        } else{
            sortingTerm = "Recently Added"
        }
        
        print("sortingTerm",sortingTerm);
        
        print("categories2:",categories);
        //2.
        let assetSortDescriptor: NSSortDescriptor = {
            var keySortDescriptor = String()
            switch sortingTerm {
            case "Recently Added":
                keySortDescriptor = "publish_on"
                isAscending = false
                break
            case "Most Viewed":
                keySortDescriptor = "view_count"
                isAscending = false
                break
            case "Recently Updated":
                keySortDescriptor = "changed"
                isAscending = false
                break
            default:
                break
            }
            
            // check the sorting term
            ZMUserActions.sharedInstance().sortingKey = keySortDescriptor
            
            let sd = NSSortDescriptor(key: keySortDescriptor , ascending: isAscending)
            return sd
        }()
        
        let assetSortDescriptorMostPopular : NSSortDescriptor = NSSortDescriptor(key: "popular", ascending: false)
        
        //3.
        //Changed/Commented:Added by Buvana: 30-Sep-2016
        //Comments:
        // PRONTO-30 - App Crash - When you search for a term with Apostrophe, the app crashes all the time
        // PRONTO-29 - Crash Fix - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: 'The left hand side for an ALL or ANY operator must be either an NSArray or an NSSet.
        let predicate: NSPredicate? = {
            var predicateDescriptor: NSPredicate?
            predicateDescriptor =
                textToSearch.isEmpty ?
                    categories.isEmpty ?
                        assetsToFilter.isEmpty ?
                            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)", franchisesToFilter,franchisesToFilter)
                            : NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@) AND (assetID IN %@)", franchisesToFilter,franchisesToFilter, assetsToFilter)
                        : assetsToFilter.isEmpty ?
      NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@) AND (ANY categories.categoryID IN %@) OR (assetID IN %@)",
                  franchisesToFilter,
                  franchisesToFilter, categories,favArrayIds) :
      NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@) AND (ANY categories.categoryID IN %@) AND (assetID IN %@)", franchisesToFilter,franchisesToFilter,
                  categories, assetsToFilter)
                : categories.isEmpty ?
                    assetsToFilter.isEmpty ?
      NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))", franchisesToFilter,
                  franchisesToFilter,
                  textToSearch,textToSearch) :
      NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                    "AND (ANY assetID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                  franchisesToFilter,franchisesToFilter, assetsToFilter, textToSearch,textToSearch)
                : assetsToFilter.isEmpty ?
      NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@) AND (ANY categories.categoryID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                  franchisesToFilter,franchisesToFilter, categories, textToSearch,textToSearch) :
                NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                              "AND (ANY categories.categoryID IN %@) AND (ANY assetID IN %@) AND ((title CONTAINS[cd] %@)" +
                              "OR (tags.key CONTAINS[cd] %@))",
                            franchisesToFilter,franchisesToFilter, categories, assetsToFilter,textToSearch,textToSearch)
            
            print("predicateDescriptor2:",predicateDescriptor!);
            return predicateDescriptor
        }()
        
        //End of fix/function
        let request : NSFetchRequest =  Asset.mr_requestAll(with: predicate!)
        
        if sortingTerm == "Recently Added"{
            request.sortDescriptors = [assetSortDescriptorMostPopular,assetSortDescriptor]
        }else{
            request.sortDescriptors = [assetSortDescriptor]
        }
        
        if (theOffSet > 0) || (theLimit > 0) {
            let offSetData = theOffSet * theLimit
            request.fetchOffset = offSetData
            request.fetchLimit = theLimit
        }
        let savedAssets = Asset.mr_execute(request)
        
        if let unwrappedSavedAssets = savedAssets
        {
            var filteredAssets = self.getFilteredAssetsAccordingToActiveBucketFromAllAssets(unwrappedSavedAssets as? [Asset])
            
            filteredAssets = self.removeDuplicatesOfAssets(filteredAssets)

            completionHandler(filteredAssets)
        }
        else
        {
            completionHandler([]) // Empty array
        }
        
    }
    
    /**
     Get asset since CoreData Database with limit, the function perform the next validations.
     
     1. Get the active filters of Categories, Brands, Text to searc, sorting Assets.
     2. Build SortDescriptor to request CoreData Entity, depending on sort, the descriptor could changed
     3. Build predicate to request CoreData.
     
     - parameter offSet:  Init value to start getting data
     - parameter limit:   Limit value to stop getting data
     - parameter success: Closure with resulting data of Entity Asset.
     */
    
    @objc func getAssets(_ offSet: Int, limit: Int, success: (_ data: [AnyObject]) -> ()){
        
        let lock: NSLock = NSLock()
        lock.lock()
        //1.
        let favArrayIds = ZMProntoManager.sharedInstance.favArrayIds
        let categories = ZMProntoManager.sharedInstance.selectedCategories
        let assetsToFilter = ZMProntoManager.sharedInstance.assetsIdToFilter
        //Search is moved to global search and it's not belong to library tab
        let textToSearch = ""
        var franchisesToFilter = [AnyObject]()
    
        var sortingArray: [AnyObject] = ZMProntoManager.sharedInstance.sortBy
        sortingArray = ZMProntoManager.sharedInstance.sortBy.filter{ $0 ["selected"] as! String == "1"}
        
        var isAscending = true
        var sortingTerm = String()
        switch ZMProntoManager.sharedInstance.search.currentView {
        case 1:
            let franchises = getFranchises()
            franchisesToFilter = getIdsToFranchisesAndBrands(franchises)
        default:
            franchisesToFilter = ZMProntoManager.sharedInstance.franchisesAndBrands
        }

        if let sortBy = sortingArray.first?.value(forKey: "label"){
            sortingTerm = sortBy as! String
        }else{
            sortingTerm = "Recently Added"
        }
        
        print("sortingTerm",sortingTerm);

       print("categories2:",categories);
        //2.
        let assetSortDescriptor: NSSortDescriptor = {
            var keySortDescriptor = String()
            switch sortingTerm {
            case "Recently Added":
                keySortDescriptor = "publish_on"
                isAscending = false
                break
            case "Most Viewed":
                keySortDescriptor = "view_count"
                isAscending = false
                break
            case "Recently Updated":
                keySortDescriptor = "changed"
                isAscending = false
                break
            default:
                break
            }
            
            // check the sorting term
            ZMUserActions.sharedInstance().sortingKey = keySortDescriptor
            
            let sd = NSSortDescriptor(key: keySortDescriptor , ascending: isAscending)
            return sd
        }()
        
        let assetSortDescriptorMostPopular : NSSortDescriptor = NSSortDescriptor(key: "popular", ascending: false)
        
        //3.
        //Changed/Commented:Added by Buvana: 30-Sep-2016
        //Comments:
        // PRONTO-30 - App Crash - When you search for a term with Apostrophe, the app crashes all the time
        // PRONTO-29 - Crash Fix - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: 'The left hand side for an ALL or ANY operator must be either an NSArray or an NSSet.
                
        let predicate: NSPredicate? = {
            var predicateDescriptor: NSPredicate?
            predicateDescriptor =
                textToSearch.isEmpty ?
                    categories.isEmpty ?
                        assetsToFilter.isEmpty ?
                            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)", franchisesToFilter,franchisesToFilter)
                            : NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@) AND (assetID IN %@)", franchisesToFilter,franchisesToFilter, assetsToFilter)
                        : assetsToFilter.isEmpty ?
                            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                          "AND (ANY categories.categoryID IN %@) OR (assetID IN %@)",
                                        franchisesToFilter,franchisesToFilter, categories,favArrayIds) :
                        NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                      "AND (ANY categories.categoryID IN %@) AND (assetID IN %@) ",
                                    franchisesToFilter,franchisesToFilter, categories, assetsToFilter)
                : categories.isEmpty ?
                    assetsToFilter.isEmpty ?
                        NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                      "AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                                    franchisesToFilter,franchisesToFilter,textToSearch,textToSearch) :
                        NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                      "AND (ANY assetID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                                    franchisesToFilter,franchisesToFilter, assetsToFilter, textToSearch,textToSearch)
                : assetsToFilter.isEmpty ?
                    NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                  "AND (ANY categories.categoryID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                                franchisesToFilter,franchisesToFilter, categories, textToSearch,textToSearch) :
                NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                              "AND (ANY categories.categoryID IN %@) AND (ANY assetID IN %@) AND ((title CONTAINS[cd] %@)" +
                              "OR (tags.key CONTAINS[cd] %@))",
                            franchisesToFilter,
                            franchisesToFilter, categories, assetsToFilter,
                            textToSearch,
                            textToSearch)
            
            print("predicateDescriptor2:", predicateDescriptor!);
            /*
             Add bucket descriptor here
             */
            return predicateDescriptor
        }()
        
        //End of fix/function

        let request : NSFetchRequest =  Asset.mr_requestAll(with: predicate!)
        
        if sortingTerm == "Recently Added"{
            request.sortDescriptors = [assetSortDescriptorMostPopular,assetSortDescriptor]
        } else {
            request.sortDescriptors = [assetSortDescriptor]
        }
        
        if (offSet > 0) || (limit > 0) {
            let offSetData = offSet * limit
            request.fetchOffset = offSetData
            request.fetchLimit = limit
        }
        
        
        let savedAssets = Asset.mr_execute(request)
        let count:Int = (savedAssets?.count)!
        if (limit == -1)
        {
            ZMUserActions.sharedInstance().totalAssetsCount = Int32(count)
        }
        
        var filteredAssets = self.getFilteredAssetsAccordingToActiveBucketFromAllAssets(savedAssets as? [Asset])
        
        filteredAssets = self.removeDuplicatesOfAssets(filteredAssets)
        
        success(filteredAssets)
        lock.unlock()
    }
  }
extension ZMCDManager {
    // New Revamping changes - getFavAssets
    @objc func getFavAssets(_ offSet: Int, limit: Int, success: (_ data: [AnyObject]) -> ()) {
        
        //1.
        let favArrayIds = ZMProntoManager.sharedInstance.favArrayIds
        print("favArrayIds", favArrayIds);
        let categories = ZMProntoManager.sharedInstance.selectedCategories
        let assetsToFilter = ZMProntoManager.sharedInstance.assetsIdToFilter
        let textToSearch = ZMProntoManager.sharedInstance.search.getTitle()
        var franchisesToFilter = [AnyObject]()
        
        var sortingArray: [AnyObject] = ZMProntoManager.sharedInstance.sortBy
        sortingArray = ZMProntoManager.sharedInstance.sortBy.filter{$0 ["selected"] as! String == "1"}
        
        var isAscending = true
        var sortingTerm = String()
        
        switch ZMProntoManager.sharedInstance.search.currentView {
        case 1:
            let franchises = getFranchises()
            franchisesToFilter = getIdsToFranchisesAndBrands(franchises)
        default:
            franchisesToFilter = ZMProntoManager.sharedInstance.franchisesAndBrands
        }
        
        if let sortBy = sortingArray.first?.value(forKey: "label") {
            sortingTerm = sortBy as! String
        } else {
            sortingTerm = "Recently Added"
        }
        
        //2.
        let assetSortDescriptor: NSSortDescriptor = {
            var keySortDescriptor = String()
            switch sortingTerm {
            case "Recently Added":
                keySortDescriptor = "publish_on"
                isAscending = false
                break
            case "Most Viewed":
                keySortDescriptor = "view_count"
                isAscending = false
                break
            case "Recently Updated":
                keySortDescriptor = "changed"
                isAscending = false
                break
            default:
                break
            }
            
            // check the sorting term
            ZMUserActions.sharedInstance().sortingKey = keySortDescriptor
            
            let sd = NSSortDescriptor(key: keySortDescriptor , ascending: isAscending)
            return sd
        }()
        
        let assetSortDescriptorMostPopular : NSSortDescriptor = NSSortDescriptor(key: "popular", ascending: false)
        
        let predicate: NSPredicate? = {
            var predicateDescriptor: NSPredicate?
            predicateDescriptor =
                textToSearch.isEmpty ?
                    categories.isEmpty ?
                        assetsToFilter.isEmpty ?
                            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)",
                                        franchisesToFilter,franchisesToFilter)
                            : NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@) AND (assetID IN %@)", franchisesToFilter,franchisesToFilter, assetsToFilter)
                        : assetsToFilter.isEmpty ?
                            NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                          "AND (ANY categories.categoryID IN %@) OR (assetID IN %@)",
                                        franchisesToFilter,franchisesToFilter, categories,favArrayIds) :
                        NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                      "AND (ANY categories.categoryID IN %@) AND (ANY assetID IN %@) ",
                                    franchisesToFilter,franchisesToFilter, categories, assetsToFilter)
                : categories.isEmpty ?
                    assetsToFilter.isEmpty ?
                        NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                      "AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                                    franchisesToFilter,franchisesToFilter,textToSearch,textToSearch) :
                        NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                      "AND (ANY assetID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                                    franchisesToFilter,franchisesToFilter, assetsToFilter, textToSearch,textToSearch)
                : assetsToFilter.isEmpty ?
                    NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                                  "AND (ANY categories.categoryID IN %@) AND ((title CONTAINS[cd] %@) OR (tags.key CONTAINS[cd] %@))",
                                franchisesToFilter,franchisesToFilter, categories, textToSearch,textToSearch) :
                NSPredicate(format: "(ANY franchise.franchiseID IN %@) AND (ANY brands.brandID IN %@)" +
                              "AND (ANY categories.categoryID IN %@) AND (ANY assetID IN %@) AND ((title CONTAINS[cd] %@)" +
                              "OR (tags.key CONTAINS[cd] %@))",
                            franchisesToFilter,franchisesToFilter, categories, assetsToFilter,textToSearch,textToSearch)
            
            print("predicateDescriptor3:",predicateDescriptor!);
            return predicateDescriptor
        }()
        
        //End of fix/function

        let request: NSFetchRequest =  Asset.mr_requestAll(with: predicate!)
        
        if sortingTerm == "Recently Added"{
            request.sortDescriptors = [assetSortDescriptorMostPopular,assetSortDescriptor]
        } else {
            request.sortDescriptors = [assetSortDescriptor]
        }
        
        if (offSet > 0) || (limit > 0) {
            let offSetData = offSet * limit
            request.fetchOffset = offSetData
            request.fetchLimit = limit
        }
        
        let savedAssets = Asset.mr_execute(request)
        var filteredAssets = self.getFilteredAssetsAccordingToActiveBucketFromAllAssets(savedAssets as? [Asset])
        
        filteredAssets = self.removeDuplicatesOfAssets(filteredAssets)
        
        success(filteredAssets)
    }
    

    /**
     Get Asset by Franchise ID, use this fucntion to filter Asset by Franchise ID
     
     - parameter franchises: Franchise ids to filter
     
     - returns: Array assets
     */
    @objc func getAssetArrayByFranchise(_ franchises: [AnyObject], success: (_ data:[AnyObject]) -> ()) {
        let predicate: NSPredicate = NSPredicate(format: "(ANY franchise.franchiseID IN %@)", franchises)
        let request: NSFetchRequest =  Asset.mr_requestAll(with: predicate)
        let assetArray =  Asset.mr_execute(request)
        success(assetArray!)
    }
    
    /**
     get Asset CoreData Entity filtering by assetID
     
     - parameter assetId: AssetID
     
     - returns: array of data.
     */
    @objc func getAssetArrayById(_ assetId: String) -> [AnyObject] {
        
        guard assetId.isEmpty == false else {
            return [AnyObject]()
        }
        // PRONTO-37 Deeplinking to a Page in a PDF
        var assetIdString = assetId
        if (assetId.range(of: "pageNum")) != nil
        {
            let arr = assetId.split(separator: "&").map(String.init)
             assetIdString = arr[0]
            
        }
        
        let predicate : NSPredicate = NSPredicate(format: "assetID == \(assetIdString)")
        let request : NSFetchRequest =  Asset.mr_requestAll(with: predicate)
        let assetArray =  Asset.mr_execute(request)
        return assetArray!
    }
  
  @objc func getDownloadedAsset(_ assetId: String) -> [AnyObject] {
    
    guard assetId.isEmpty == false else {
      return [AnyObject]()
    }
    // PRONTO-37 Deeplinking to a Page in a PDF
    var assetIdString = assetId
    if (assetId.range(of: "pageNum")) != nil
    {
      let arr = assetId.split(separator: "&").map(String.init)
      assetIdString = arr[0]
      
    }
    
    let p1: NSPredicate = NSPredicate(format: "assetID == \(assetIdString)")
    let p2 = NSPredicate(format: "isDownloaded == \(true)")
    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1, p2])
    let request : NSFetchRequest =  Asset.mr_requestAll(with: predicate)
    let assetArray =  Asset.mr_execute(request)
    return assetArray!
  }
  
  @objc func getAssetArray(_ assetId: NSNumber)->[AnyObject] {
    let predicate : NSPredicate = NSPredicate(format: "assetID == %@", assetId)
    let request : NSFetchRequest =  Asset.mr_requestAll(with: predicate)
    let assetArray =  Asset.mr_execute(request)
    return assetArray!
  }
    
    // Revamp(24/1) - Get all assets
    /**
     Get all Assets
     */
    @objc func getAllAssets()->[AnyObject] {
        let request : NSFetchRequest = Asset.mr_requestAll()
        let savedAssets = Asset.mr_execute(request)
        return savedAssets!
    }
    
    // Revamp(1/12) - Get fav asset array
    /**
     Get Fav Asset Array
     */
    @objc func getFavFranchise() -> [AnyObject] {
        let mrDescription = NSManagedObjectContext.mr_default().mr_description()
        
        if (mrDescription != "") {
        let request: NSFetchRequest = FavFranchise.mr_requestAll()
        let savedFavFranchises = FavFranchise.mr_execute(request)
        return savedFavFranchises!
        }
        return []
    }
    
    @objc func getFavFranchiseWithoutFilter() -> [AnyObject] {
        let mrDescription = NSManagedObjectContext.mr_default().mr_description()
        
        if (mrDescription != "") {
            let request: NSFetchRequest = FavFranchise.mr_requestAll()
            let savedFavFranchises = FavFranchise.mr_execute(request)
            return savedFavFranchises!
        }
        return []
    }
    
    /**
     Get Franchise Entity filtering by franchiseID
     
     - parameter franchiseId: franchiseID to filter
     
     - returns: Franchises Array.
     */
    @objc func getFavFranchiseById(_ assetId: NSNumber) -> [AnyObject] {
        let predicate: NSPredicate = NSPredicate(format: "asset_ID == %@", assetId)
        let favFranchiseArray = FavFranchise.mr_findAll(with: predicate)
        
        return favFranchiseArray!
    }
    // End of Revamp
    
    // Revamp(17/01) - Get offline download array
    /**
     Get Offline Download Array
     */
    @objc func getOfflineDownloadArray() -> [AnyObject] {
        // Avoiding crash on first logout
        let mrDescription = NSManagedObjectContext.mr_default().mr_description()
        
        if (mrDescription != "") {
        let request : NSFetchRequest = OfflineDownload.mr_requestAll()
        let savedOfflineDownloadArray = OfflineDownload.mr_execute(request)
            if(savedOfflineDownloadArray != nil) {
                return savedOfflineDownloadArray!
            }
            else {
                return []
            }
        }
        return []
    }
    
    /**
     Get Offline Download Array filtering by assetID
     
     */
    @objc func getOfflineDownloadArrayById(_ assetId: NSNumber) -> [AnyObject] {
        let predicate: NSPredicate = NSPredicate(format: "assetID == %@", assetId)
        let offlineDownloadArray = OfflineDownload.mr_findAll(with: predicate)
        
        // Avoid crash when logout
        if(offlineDownloadArray != nil) {
            return offlineDownloadArray!
        }
        else {
            return []
        }
    }
    // End of Revamp
    
    /**
     Get Franchise CoreData Entity sorting by name
     
     - returns: array of data.
     */
    @objc func getFranchises() -> [AnyObject] {
        let sortDescritpor: NSSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let request: NSFetchRequest = Franchise.mr_requestAll()
        request.sortDescriptors = [sortDescritpor]
        let savedFranchises = Franchise.mr_execute(request)
        if (savedFranchises != nil) {
            return savedFranchises!
        } else {
            return [];
        }
    }
    
    /**
     Get Categories Entity sorting by name
     
     - returns: array of categories
     */
    @objc func getCategories() -> [AnyObject] {
        let sortDescritpor: NSSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let request: NSFetchRequest = Categories.mr_requestAll()
        request.sortDescriptors = [sortDescritpor]
        let savedCategories = Categories.mr_execute(request)
        return savedCategories!
    }
  }
extension ZMCDManager {
  @objc func getCategoriesByID(categoryId: NSNumber) -> [AnyObject] {
     let predicate: NSPredicate = NSPredicate(format: "categoryID == %@", categoryId)
     let request: NSFetchRequest = Categories.mr_requestAll(with: predicate)
     let savedCategories = Categories.mr_execute(request)
     return savedCategories!
   }
    /**
     Get Franchise Entity filtering by franchiseID
     
     - parameter franchiseId: franchiseID to filter
     
     - returns: Franchises Array.
     */
    @objc func getFranchiseById(_ franchiseId: String) -> [AnyObject] {
        if (!franchiseId.isEmpty) {
            let predicate: NSPredicate = NSPredicate(format: "franchiseID == %@", franchiseId)
            let franchiseArray = Franchise.mr_findAll(with: predicate)
            return franchiseArray!
        } else {
            return []
        }
    }
    
    /**
     Get Franchise Entity filtering by franchiseTID
     
     - parameter franchiseTid: franchiseTid to filter
     
     - returns: Franchises Array.
     */
    @objc func getFranchiseByTid(_ franchiseTid: String) -> [AnyObject] {
        if (!franchiseTid.isEmpty) {
            let predicate : NSPredicate = NSPredicate(format: "tid == %@", franchiseTid)
            let franchiseArray = Franchise.mr_findAll(with: predicate)
            return franchiseArray!
        } else {
            return []
        }
    }
    
    
    /**
     Get Franchises entity sorting by name
     
     - parameter franchiseName: franchise name
     
     - returns: franchise Data
     */
    
    @objc func getFranchiseByName(_ franchiseName: String) -> [AnyObject] {
        let predicate: NSPredicate = NSPredicate(format: "name == %@", franchiseName)
        let franchiseArray = Franchise.mr_findAll(with: predicate)
        if (franchiseArray != nil) {
            return franchiseArray!
        } else {
            return [];
        }
    }
    
    @objc func getCategories(_ success: @escaping (_ data:[AnyObject]) -> ()) {
        
        var savedCategories = [AnyObject]()
        
        getAssetArrayByFranchise(ZMProntoManager.sharedInstance.franchisesAndBrands) { (data) -> () in
            
            var assetsIds = [AnyObject]()
            
            for asset in data  {
                if let unwrapedAsset = asset as? Asset, let assetId =  unwrapedAsset.assetID {
                    assetsIds.append(assetId)
                }
            }
            let predicate: NSPredicate = NSPredicate(format: "ANY asset.assetID IN %@", assetsIds)
            let request: NSFetchRequest = Categories.mr_requestAll(with: predicate)
            savedCategories = Categories.mr_execute(request)!
            
            savedCategories.sort(by: { (category1, category2) -> Bool in
                
                if let unwrapcategory1 = category1 as? Categories, let unwrapcategory2 = category2 as? Categories {
                    if let categoryName1 = unwrapcategory1.name, let categoryName2 = unwrapcategory2.name
                    {
                        return categoryName1.localizedCaseInsensitiveCompare(categoryName2) == .orderedAscending
                    }
                }
                return false
            })
            
            success(savedCategories)
        }
    }
    
    
    /**
     Get active categories to selected Assets.
     
     - parameter success: Closure success.
     */
    
    @objc func getActiveCategories(_ success: @escaping (_ data: [AnyObject]) -> ()) {
        
        let downloadGroup = DispatchGroup()
        var savedCategories = [AnyObject]()
        
        downloadGroup.enter();
        
        getAssetArrayByFranchise(ZMProntoManager.sharedInstance.franchisesAndBrands) { (data) -> () in

            var assetsIds = [AnyObject]()

            for asset in data {
                if let unwrapedAsset = asset as? Asset, let assetId =  unwrapedAsset.assetID
                {
                    assetsIds.append(assetId)
                }
            }
            let predicate: NSPredicate = NSPredicate(format: "ANY asset.assetID IN %@", assetsIds)
            let request: NSFetchRequest = Categories.mr_requestAll(with: predicate)
            savedCategories = Categories.mr_execute(request)!
            savedCategories.sort(by: { (category1, category2) -> Bool in
                
               if let unwrapcategory1 = category1 as? Categories, let unwrapcategory2 = category2 as? Categories {
                if let categoryName1 = unwrapcategory1.name, let categoryName2 = unwrapcategory2.name
                {
                     return categoryName1.localizedCaseInsensitiveCompare(categoryName2) == .orderedAscending
                }
               }
                return false
            })
            
            downloadGroup.leave();
        }
        
        downloadGroup.notify(queue: DispatchQueue.main) { () -> Void in
            
            let activeTabSelection =  Constant.getCurrentActiveTabOptionAsString()
            var categoryNodes = [AnyObject]()
            for value in savedCategories {
                let category = value as! Categories
                // New Revamping - Set all nodes as "0" other than "All Franchise Assets"
                if (category.categoryID != nil && category.name != nil)
                {
                    var bucketName =  self.getBucketNameForCategory(category)
                    
                    if (category.bucketName == nil)
                    {
                        bucketName = "Franchise"
                        category.bucketName = bucketName
                    }
                    if (bucketName.lowercased() == activeTabSelection?.lowercased())
                    {
                        let categoryNode = ["id": category.categoryID!, "bucketId": category.bucketId!,"name": category.name!, "selected":"0", "bucketName": category.bucketName!] as [String : Any]
                        categoryNodes.append(categoryNode as AnyObject)
                    }
                }
            }
            
            // Removing Duplicate Category
            categoryNodes = self.removeDuplicatesEntryFromCategoriesNodes(categoryNodes)
            
            // New Revamping - add two fields in categories
            let dict = [ "id": 0, "name": "All Franchise Assets", "selected": "1"] as [String: Any]
            categoryNodes.insert(dict as AnyObject, at: 0)
            let favDict = [ "id": 1, "name": "Franchise Favorites", "selected": "0"] as [String: Any]
            categoryNodes.insert(favDict as AnyObject, at: 1)
            // brands Array
            let arraySample = ZMUserActions.sharedInstance().brandsArray
            if (arraySample != nil)
            {
                if(arraySample!.count > 1) {
                    var k = 2;
                    for g in 0...ZMUserActions.sharedInstance().brandsArray.count - 1
                    {
                        
                        let brandDict = ["id":k, "name":ZMUserActions.sharedInstance().brandsArray[g], "selected":"0"] as [String : Any]
                        categoryNodes.insert(brandDict as AnyObject, at: k)
                        k += 1;
                    }
                }
            }
            
            success(categoryNodes)
        }
    }
  }
extension ZMCDManager {
    @objc func getActiveCategoriesWithCategories(_ categories:[AnyObject],
                                                 withCompletionHandler completionHandler: @escaping (_ data: [AnyObject]) -> ()){
        
        let activeTabSelection:String = Constant.getCurrentActiveTabOptionAsString()!
        let allAssetTagName:String = Constant.getAllAssetTagName()!
        let favoriteAssetTagName:String = Constant.getFavoritesAssetTagName()!
        
        var categoryNodes = [AnyObject]()
        for value in categories
        {
            let category = value as! Categories
            if(category.categoryID != nil && category.name != nil)
            {
                let bucketName = self.getBucketNameForCategory(category)
                if (bucketName.lowercased() == activeTabSelection.lowercased())
                {
                    let categoryNode = ["id":category.categoryID!, "bucketId":category.bucketId!,"name":category.name!, "selected":"0", "bucketName":category.bucketName!] as [String : Any]
                    categoryNodes.append(categoryNode as AnyObject)
                }
            }
        }
       
        // Removing Duplicate Category
        categoryNodes = self.removeDuplicatesEntryFromCategoriesNodes(categoryNodes)
        let dict = [ "id" : 0, "name" : allAssetTagName, "selected":"1"] as [String : Any]
        categoryNodes.insert(dict as AnyObject, at: 0)
        let favDict = [ "id" : 1, "name" : favoriteAssetTagName, "selected":"0"] as [String : Any]
        categoryNodes.insert(favDict as AnyObject, at: 1)
        completionHandler(categoryNodes)
    }
    
    func removeDuplicatesEntryFromCategoriesNodes(_ categories: [AnyObject]) -> [AnyObject]
    {
        var noDuplicatesCategories = [AnyObject]()
        var usedNames = [String]()
        for category in categories
        {
            if let unwrapCategory = category as? [String : Any]
            {
                if let nameAsString = unwrapCategory["name"] as? String, !usedNames.contains(nameAsString)
                {
                    noDuplicatesCategories.append(category)
                    usedNames.append(nameAsString)
                }
            }
        }
        return noDuplicatesCategories
    }
    
    /**
     Update data on Asset Entity.
     
     - parameter asset:   Asset to update
     - parameter success: Closure succes
     - parameter error:   Closure Error.
     */
    @objc func updateAsset(_ newAsset: Asset?, success: @escaping ()->(), error: @escaping (_ error: NSError)->()) {
        
        guard let asset = newAsset, let assetId = asset.assetID?.stringValue else {
            let err = NSError(domain: "com.abbvienet.pronto", code: 999, userInfo: ["error":"Asset not found."])
            return  error(err)
        }
    
        let predicate : NSPredicate = NSPredicate(format: "assetID == \(assetId)")
        let request : NSFetchRequest =  Asset.mr_requestAll(with: predicate)
        let assetArray =  Asset.mr_execute(request)
        
        if let unwrappedAssetArray = assetArray, unwrappedAssetArray.isEmpty == false,
           let assetToUpdate = unwrappedAssetArray[0] as? Asset
        {
            assetToUpdate.setValue(asset.assetID, forKey: "assetID")
            assetToUpdate.setValue(asset.changed, forKey: "changed")
            assetToUpdate.setValue(asset.created, forKey: "created")
            assetToUpdate.setValue(asset.field_description, forKey: "field_description")
            assetToUpdate.setValue(asset.field_length, forKey: "field_length")
            assetToUpdate.setValue(asset.field_thumbnail, forKey: "field_thumbnail")
            assetToUpdate.setValue(asset.file_mime, forKey: "file_mime")
            assetToUpdate.setValue(asset.file_size, forKey: "file_size")
            assetToUpdate.setValue(asset.file_timestamp, forKey: "file_timestamp")
            assetToUpdate.setValue(asset.popular, forKey: "popular")
            assetToUpdate.setValue(asset.publish_on, forKey: "publish_on")
            assetToUpdate.setValue(asset.title, forKey: "title")
            assetToUpdate.setValue(asset.type, forKey: "type")
            assetToUpdate.setValue(asset.unpublish_on, forKey: "unpublish_on")
            assetToUpdate.setValue(asset.uri_full, forKey: "uri_full")
            assetToUpdate.setValue(asset.view_count, forKey: "view_count")
            assetToUpdate.setValue(asset.votes, forKey: "votes")
            assetToUpdate.setValue(asset.path, forKey: "path")
            assetToUpdate.setValue(asset.user_vote, forKey: "user_vote")
            // New revamping changes - asset fields
            assetToUpdate.setValue(asset.medRegNo, forKey: "medRegNo")
            assetToUpdate.setValue(asset.field_print, forKey: "field_print")
            assetToUpdate.setValue(asset.field_promote, forKey: "field_promote")
            assetToUpdate.setValue(asset.field_email_to_self, forKey: "field_email_to_self")
            assetToUpdate.setValue(asset.isUpdateAvailable, forKey: "isUpdateAvailable")
            assetToUpdate.setValue(asset.isDownloaded, forKey: "isDownloaded")
            assetToUpdate.setValue(asset.savedPath, forKey: "savedPath")
            assetToUpdate.setValue(asset.is_new, forKey: "is_new")
            assetToUpdate.setValue(asset.assetBucketId, forKey: "assetBucketId")
            assetToUpdate.setValue(asset.isViewed, forKey: "isViewed")
            
            NSManagedObjectContext.mr_default().mr_saveToPersistentStore(completion: { (Bool, errorM) -> Void in
                if (errorM == nil)
                {
                    success()
                }
                else
                {
                    error(errorM! as NSError)
                }
            })
        }
    }
  
  @objc func updateDownloadedAsset(_ newAsset: Asset?, success: @escaping ()->(), error: @escaping (_ error:NSError)->()) {
    guard let asset = newAsset, let assetId = asset.assetID?.stringValue else {
      let err = NSError(domain: "com.abbvienet.pronto", code: 999, userInfo: ["error":"Asset not found."])
      return  error(err)
    }
    
    let predicate : NSPredicate = NSPredicate(format: "assetID == \(assetId)")
    let request : NSFetchRequest =  Asset.mr_requestAll(with: predicate)
    let assetArray =  Asset.mr_execute(request)
    
    if let unwrappedAssetArray = assetArray, unwrappedAssetArray.isEmpty == false,
       let assetToUpdate = unwrappedAssetArray[0] as? Asset
    {
      assetToUpdate.setValue(asset.assetID, forKey: "assetID")
      assetToUpdate.setValue(asset.isDownloaded, forKey: "isDownloaded")
      
      NSManagedObjectContext.mr_default().mr_saveToPersistentStore(completion: { (Bool, errorM) -> Void in
        if (errorM == nil)
        {
          success()
        }
        else
        {
          error(errorM! as NSError)
        }
      })
    }
  }

    @objc func getIdsToFranchisesAndBrands(_ franchises: [AnyObject]) -> [AnyObject] {
        
        var franchisesToFilter = [AnyObject]()
        
        print("franchises:",franchisesToFilter)
        
        for item in franchises {
            
            let franchise = item as! Franchise
            franchisesToFilter.append(franchise.franchiseID!.intValue as AnyObject)
        }
        
        return franchisesToFilter
    }
    
    
    /**
     Global method to set selectedFranchiseBrands swift collection, because Objective-C can't read Swift structurs this setup can't be make in Objective.
     
     - parameter defaultFranchise: Default Franchise to user
     */
    
    @objc func setFranchiseAndBrandsCollection(_ defaultFranchise: String) {
        let franchisesToCheck = getFranchises() as! [Franchise]
        checkAllFranchisesWithDefault(franchisesToCheck, nameDefault: defaultFranchise)
        

    }
    
    // New Search Implementation
    @objc func setUpdatedFranchiseAndBrandsCollection(_ selectedFranchise: String) {
        
        let franchisesToCheck = getFranchises() as! [Franchise]
        
        checkFranchisesSelected(franchisesToCheck, franchiseName: selectedFranchise)
       
        
    }
    
    
    /**
     New Search Implementation
     This function allow selected all Franchise, and reset Franchises Menu as default, to perfomr the reset the nameDefault must containt data.
     
     - parameter franchisesToCheck: total Franchises to user
     - parameter nameDefault:       name of searched Franchise.
     */
    
    @objc func checkFranchisesSelected(_ franchisesToChecked: [AnyObject], franchiseName: String?) {
        
        
        for (_,value) in franchisesToChecked.enumerated() {
            let franchise: Franchise = value as! Franchise
            let franchiseToCheckStatus = FranchiseAndBrands(uid: franchise.franchiseID!.intValue,
                                                            name: franchise.name!,
                                                            type: TypeObject.franchise)
            
            if let _ = franchiseName {
                if franchise.tid!.stringValue == ZMUserActions.sharedInstance().franchiseId {
              
                }
            } else {

            }
        }
        
    }

    
    /**
     This function allow selected all Franchise, and reset Franchises Menu as default, to perfomr the reset the nameDefault must containt data.
     
     - parameter franchisesToCheck: total Franchises to user
     - parameter nameDefault:       name of defualt Franchise.
     */
    
    @objc func checkAllFranchisesWithDefault(_ franchisesToCheck: [AnyObject], nameDefault: String?) {
        
        for (_, value) in franchisesToCheck.enumerated() {
            let franchise: Franchise = value as! Franchise
            let franchiseToCheckStatus = FranchiseAndBrands(uid: franchise.franchiseID!.intValue,
                                                            name: franchise.name!,
                                                            type: TypeObject.franchise)
            
            if let _ = nameDefault {
                if(ZMUserActions.sharedInstance().franchiseId != nil) {
                if franchise.franchiseID!.stringValue == ZMUserActions.sharedInstance().franchiseId {
                    ZMProntoManager.sharedInstance.selectedFranchiseBrands.removeAll()
                }
            }
            } else {
              
            }
        }
    }
    
    /**
     This function allow know if a franchise or brand has been selected already, to do that, the function will iterate over selectedFranchiseBrands array to find the id object (Brand or Franchise), also, if a (Brand or Franchise) has been selected before, the function let us deleted it.
     
     - parameter franchiseToAdd: Franchise or Brands to check if exists on selected items
     - parameter removeIfFound:   Bool to indicate if the selected object will be removed.
     
     - returns: New selection of Franchise and Brands Ids, to check status on Franchise Menu
     */
    
  func addObjectToSelectedItems(_ franchiseToAdd: FranchiseAndBrands, removeIfFound: Bool) -> [FranchiseAndBrands] {
        
        var franchisesAdded = [FranchiseAndBrands]()
        
        let franchiseSelection = ZMProntoManager.sharedInstance.selectedFranchiseBrands.filter {
            $0.uid == franchiseToAdd.uid && $0.name == franchiseToAdd.name && $0.type == franchiseToAdd.type
        }
        
        if franchiseSelection.isEmpty {
            ZMProntoManager.sharedInstance.selectedFranchiseBrands.append(franchiseToAdd)
            franchisesAdded.append(franchiseToAdd)
        } else {
            if removeIfFound {
                removeItemToFranchiseAndBrands(franchiseSelection.first! as FranchiseAndBrands)
            }
        }
        return franchisesAdded
    }
    
    /**
     Function to delete an object to FranchiseAndBrands array which contain the ids of Franchises or Brands
     
     - parameter franchiseOrBrand: Object of type FranchiseAndBrands
     */
    
    func removeItemToFranchiseAndBrands(_ franchiseOrBrand: FranchiseAndBrands) {
        for (index,value) in ZMProntoManager.sharedInstance.selectedFranchiseBrands.enumerated() {
            if value.uid == franchiseOrBrand.uid && value.name == franchiseOrBrand.name {
                ZMProntoManager.sharedInstance.selectedFranchiseBrands.remove(at: index)
            }
        }
    }
    
   func removeDuplicatesOfAssets(_ assetArray: [Asset]) -> [Asset] {
        //Can have duplicate Assets
        var noDuplicatesCategories = [Asset]()
        var usedAssetIds = [NSNumber]()
        for asset in assetArray
        {
            var checkCount = 0
            if let assetId = asset.assetID, !usedAssetIds.contains(assetId) {
                noDuplicatesCategories.append(asset)
                usedAssetIds.append(assetId)
            } else {
                if asset.isUpdateAvailable {
                    for assetToCompare in noDuplicatesCategories {
                        if asset.assetID == assetToCompare.assetID {
                            noDuplicatesCategories.remove(at: checkCount)
                            break
                        }
                        checkCount += 1
                    }
                    noDuplicatesCategories.append(asset)
                }
            }
        }
        return noDuplicatesCategories
    }
    
    //MARK:- Helper Methods
    func getBucketNameForCategory(_ category: Categories) -> String {
        var bucketName = ""
        if category.bucketName == nil {
            // make it constant
            bucketName = "Franchise";
        } else {
            if category.bucketName!.isEmpty == true {
                bucketName = "Franchise";
            } else {
                bucketName = category.bucketName!;
            }
        }
        return bucketName
    }
    
    @objc  func getFilteredAssetsAccordingToActiveBucketFromAllAssets(_ allAssets: [Asset]?) -> [Asset] { // 3622
        var filteredAssets: [Asset] = [Asset]()
        if let unwrappedSavedAssets = allAssets {
            for asset in unwrappedSavedAssets {
                var assetBucketId = asset.assetBucketId
                if assetBucketId == nil || assetBucketId?.isEmpty == true {
                    // Assigning default id
                    assetBucketId = Constant.shared().getDefaultBucketID()
                }
//                print("Asset -> BucketId -> \(assetBucketId!) -> Name -> \(asset.field_description!)")
                
                let activeTabSelection: String = Constant.shared().getCurrentActiveTabOptionIDAsString()

                let assetBucketIDSeperatedByUnderscore = Constant.getStringsValuesSeperatedByUnderscore(from: assetBucketId)

                if (Constant.doesContainValue(activeTabSelection, in: assetBucketIDSeperatedByUnderscore) == true) {
                  filteredAssets.append(asset)
                }
            }
        }
        return filteredAssets
    }
    
    @objc  public func getAllFranchise() -> [Franchise] {
        
        let mrDescription = NSManagedObjectContext.mr_default().mr_description()
        if mrDescription.isEmpty == false {
            let request: NSFetchRequest = Franchise.mr_requestAll()
            
            if  let allFranchise = Franchise.mr_execute(request)  as? [Franchise] {
                return allFranchise
            }
        }
        return []
    }
}
