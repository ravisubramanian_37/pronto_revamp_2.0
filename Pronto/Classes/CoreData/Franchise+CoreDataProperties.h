//
//  Franchise+CoreDataProperties.h
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Franchise.h"

NS_ASSUME_NONNULL_BEGIN

@interface Franchise (CoreDataProperties)

// New Pronto Revamp - color code
@property (nullable, nonatomic, retain) NSString* field_franchise_color;
@property (nullable, nonatomic, retain) NSNumber *field_active;
@property (nullable, nonatomic, retain) NSNumber *field_private;
@property (nullable, nonatomic, retain) NSNumber *franchiseID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *tid;
@property (nullable, nonatomic, retain) NSNumber *vid;
//@property (nullable, nonatomic, retain) NSSet<Asset *> *asset;
//@property (nullable, nonatomic, retain) NSSet<Brand *> *brands;

@end

@interface Franchise (CoreDataGeneratedAccessors)

- (void)addAssetObject:(Asset *)value;
- (void)removeAssetObject:(Asset *)value;
- (void)addAsset:(NSSet<Asset *> *)values;
- (void)removeAsset:(NSSet<Asset *> *)values;

- (void)addBrandsObject:(Brand *)value;
- (void)removeBrandsObject:(Brand *)value;
- (void)addBrands:(NSSet<Brand *> *)values;
- (void)removeBrands:(NSSet<Brand *> *)values;

@end

NS_ASSUME_NONNULL_END
