//
//  Asset+CoreDataProperties.m
//  Pronto
//
//  Created by osmarogo on 4/13/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Asset+CoreDataProperties.h"

@implementation Asset (CoreDataProperties)

@dynamic assetID;
@dynamic changed;
@dynamic created;
@dynamic field_description;
@dynamic field_length;
@dynamic field_thumbnail;
@dynamic file_mime;
@dynamic file_size;
@dynamic file_timestamp;
@dynamic path;
@dynamic popular;
@dynamic publish_on;
@dynamic title;
@dynamic type;
@dynamic unpublish_on;
@dynamic uri_full;
@dynamic view_count;
@dynamic votes;
@dynamic brands;
@dynamic categories;
//@dynamic franchise;
@dynamic tags;
// PRONTO-1 - Likes not Working
@dynamic user_vote;
// New Revamping changes - fields added
@dynamic medRegNo;
@dynamic field_print;
@dynamic field_promote;
@dynamic field_email_to_self;
@dynamic isUpdateAvailable;
@dynamic isDownloaded;
@dynamic is_new;
@dynamic assetBucketId;
@dynamic isViewed;
@dynamic parentCategory;
@end
