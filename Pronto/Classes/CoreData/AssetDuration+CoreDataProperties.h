//
//  AssetDuration+CoreDataProperties.h
//  Pronto
//
//  Created by osmarogo on 1/4/16.
//  Copyright © 2016 Abbvie. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AssetDuration.h"

NS_ASSUME_NONNULL_BEGIN

@interface AssetDuration (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *asset_duration;
@property (nullable, nonatomic, retain) NSNumber *asset_id;

@end

NS_ASSUME_NONNULL_END
