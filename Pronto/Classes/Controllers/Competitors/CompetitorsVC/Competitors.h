//
//  Competitors.h
//  Pronto
//
//  Created by cccuser on 27/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMHeader.h"
#import "ZMUserActions.h"
#import "ZMAbbvieAPI.h"
#import "ZMProntoViewController.h"
#import "TabBarController.h"
//#import "ZMLibrary.h"
#import "ProntoTabbarViewController.h"
#import "ZMEmpty.h"


@interface Competitors : ZMProntoViewController < UICollectionViewDelegateFlowLayout, UIScrollViewDelegate,UIPopoverPresentationControllerDelegate> {//ZMGridItemDelegate
     ZMEmpty* empty;
}

/**
 *  Displays the notification pannel on view
 */
-(void) displayNotifications;

/**
 *  Displays the walkthrough component on main view
 */
-(void) displayWalkthrough;

/**
 *  Get notifications from server
 */
-(void) getNotifications;

/**
 *  Changes the badge number, for both application icon and header badge
 *  @param badgeNumber
 */
-(void) changeBadgeNumber:(int)badgeNumber;

//Get total notifications for badge count
- (void)geTotalOfNotifications;

//Dismiss all notifications on button click
- (void)dismissAllNotifications:(NSArray *)notifications;

//Arrow for notifications
- (void)showHideArrow:(BOOL) hide;

//Open Asset
- (void)openAsset:(Asset *)selectedAsset;

//For Feedback
- (void)initiateFeedback:(NSString*) selectedAsset openedFrom:(NSString*) choice;

//For Asset detail selection
- (void)markLiked:(Asset *)selectedAsset type:(NSInteger)type;

@property (strong, nonatomic) Asset *currentSelectedAsset;

//Classes property declaration
@property (strong, nonatomic) ZMHeader *headerViewCompetitors;
@property (strong, nonatomic) ZMUserActions *actions;
@property (strong, nonatomic) ProntoTabbarViewController *tabBar;
//@property (strong, nonatomic) ZMLibrary *library;

// Export Menu
//@property (nonatomic, weak) ExportMenu *exportMenu;
@property (nonatomic) BOOL exportMenuIsPresented;
@property(nonatomic, strong) UIPopoverPresentationController* exportMenuPopOver;
- (void)resetGlobalSearchFromCompetitors;

- (void)validateAndShowAsset:(Asset *)selectedAsset;

- (void)setExportToNormal:(BOOL) dismissExport;

- (void)downloadOneAsset:(Asset *)selectedAsset completion:(void (^)(BOOL assetDownloaded))completion
                 failure:(void (^)(NSString *assetDownloadFail))failure;

-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure;
@end
