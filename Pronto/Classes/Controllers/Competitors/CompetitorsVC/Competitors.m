//
//  Competitors.m
//  Pronto
//
//  Created by cccuser on 27/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import "Competitors.h"
#import "Pronto-Swift.h"
#import "ZMNotifications.h"
#import "ZMProntoTheme.h"
#import "Franchise.h"
#import "CCategoryMenuVC.h"
#import "ViewUtils.h"
#import "ZMContentHeader.h"
#import "ZMSortMenu.h"
#import "OfflineDownload+CoreDataClass.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "ZMAbbvieAPI.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "ZMUserActions.h"
#import "CLibraryViewController.h"
#import "FranchiseTableViewCell.h"
#import "CLibraryViewModel.h"
#import "CCategoryMenuVC.h"
#import "CCategoryMenuViewModel.h"
#import "ZMGridItem.h"
#import "ZMEmpty.h"
#import "ZMAssetsLibrary.h"
#import "Filter.h"
#import "FavFranchiseAsset.h"
#import "FeedbackView.h"
#import "HelpViewController.h"
#import "GlobalSearchVC.h"
#import "AppUtilities.h"
#import "AssetDownloadProvider.h"
#import "NSMutableDictionary+Utilities.h"
#import "EventCoreDataManager.h"
#import "EventOnlyAssetHandler.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"
#import "EventsServiceManager.h"
#import "FastPDFViewController.h"
#import <FastPdfKit/FastPdfKit.h>
#import "UIColor+ColorDirectory.h"

@interface Competitors () <CLibraryViewControllerDelegate, UIPopoverPresentationControllerDelegate> {
    id <ZMProntoTheme> theme;
    __weak IBOutlet UIView *header;
    ZMNotifications *notificationsPannel;
    BOOL favIconTapped;
    FeedbackView *feedback;
    UIView *backgroundView;
    NSInteger selectedItem;
    
    //For Audio Player
    AudioPlayer *audioPlayerView;
    Asset *assetForAudio;
    FranchiseTableViewCell *audioCurrentCell;
    ZMGridItem *audioCurrentGridCell;
}


@property (nonatomic, strong) CCategoryMenuVC *cCategoryMenuVC;
@property (nonatomic, strong) ZMContentHeader *contentHeader;
@property (nonatomic, strong) CLibraryViewController *cLibraryVC;

@property (nonatomic, weak) AppDelegate *mainDelegate;
@property (nonatomic, weak) ZMAbbvieAPI *apiManager;
@property (nonatomic, assign) BOOL isDownloadingAsset;
@property (nonatomic, assign) BOOL isTapDownloadInitiated;
@property (nonatomic, assign) BOOL shouldResetFranchises;
@property (nonatomic, strong) Asset *currentAssetDownload; // Make it nil at proper places

//THIS PROPERTY WILL BE USED WHILE SUPPORTING TO LAZY LOADING
@property (nonatomic) int itemsPerPage;
@property (nonatomic) int pageIndex;
@property (strong, nonatomic) Asset *currentlySelectedAsset;

//
//VERIFY WHY DO WE NEED TO SEND THE PARENT AND LIBRARY
@property (nonatomic, strong) ZMSortMenu *cSortMenu;
@property (nonatomic, strong) UIPopoverPresentationController *cSortMenuPopOver;
@property (nonatomic, strong) UIButton *cSortMenuButton;

// Info Menu
@property (nonatomic, weak) InfoMenu *infoMenu;
@property (nonatomic, weak) UIPopoverPresentationController *infoMenuPopOver;
@property (nonatomic) BOOL infoMenuIsPresented;


//This properties will hold the value of cell(Grid/List view) which presenting the popover.
@property (nonatomic, strong) id popOverPresentingCell;
@property (nonatomic, strong) ZMEmpty *emptyView;

// Global Search View
@property (nonatomic, strong) GlobalSearchVC *globalSearcVC;
@property (nonatomic, strong) Asset *currentSelectedAssetToSeeDetail;
    
//Event Only Asset Handler
@property (nonatomic, strong) EventOnlyAssetHandler *eventOnlyAssetHandler;

@end

@implementation Competitors

@synthesize headerViewCompetitors;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeData];
    headerViewCompetitors = (ZMHeader *)[[[NSBundle mainBundle] loadNibNamed:@"Header" owner:self options:nil] objectAtIndex:0];
    headerViewCompetitors.delegate = (id<headerActions>)self;
    [headerViewCompetitors initWithOptions:header];
    theme = [ZMProntoTheme sharedTheme];
//    _library = [[ZMLibrary alloc]init];
//    headerViewCompetitors.library = _library;
    headerViewCompetitors.sourceViewController = self;
    
    /** If you want to have full selection list turn showResumeSelection to NO */
    headerViewCompetitors.showResumeSelection = YES;
    headerViewCompetitors.competitors = self;
    
    _tabBar = [[ProntoTabbarViewController alloc] init];
    
    //Adding the Library as a valid observer for the ZMAssetsLibrary @see
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Off_Start"];
    
    backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    [self loadCCategoryMenuView];
    [self loadZMContenHeaderView];
    [self loadCLibraryView];
    [self setupViewsFrames];
    [self.cLibraryVC changeTintColor];
}

- (void)initializeData
{
    self.mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.apiManager   = [ZMAbbvieAPI sharedAPIManager];
    self.actions      = [ZMUserActions sharedInstance];
    [self addSelfToListenForNotificationCenter];
}

- (void)addSelfToListenForNotificationCenter
{
    [self registerNotification:kZMAssetViewControllerGoBackNotification];
    [self registerNotification:kZMPDFDocumentDidCloseButtonPressedNotification];
    [self registerNotification:kZMSortMenuDidSelectRowNotification];
    [self registerNotification:kPreferenceListVCDidSavePreferencesNotification];
//    [self registerNotification:@"OPEN_SCHEMA_ASSET"];
//    [self registerNotification:@"OPEN_SCHEMA_ASSET_APP_CLOSED"];
    [self registerNotification:@"CMS_BRING_ALL_DATA"];
    [self registerNotification:@"HIDE_LIBRARY_POPOVERS"];
//    [self registerNotification:@"OPEN_TEMP_ASSET"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET_FOUND"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET_NOT_FOUND"];
    [self registerNotification:UIApplicationWillEnterForegroundNotification];
}

- (void)removeSelfFromListeningToNotificationCenter
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidLayoutSubviews
{
    [notificationsPannel adjustNotifications];
    [notificationsPannel changeOfOrientation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[ZMAssetsLibrary defaultLibrary] removeObserver:self];
    [self removeNotification:kZMSortMenuDidSelectRowNotification];
    //[self removeNotification:@"OPEN_SCHEMA_ASSET_APP_CLOSED"];
    self.currentSelectedAssetToSeeDetail = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self stopAudio];
}

- (void)removeNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:name object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setupViewsFrames];
    [self.headerViewCompetitors adjustHeader];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
    [self addSelfToListenForNotificationCenter];
    [ZMUserActions sharedInstance].currentSelectedTabIndex = 1;
    headerViewCompetitors.tabBarTag = 1;
    notificationsPannel.tabBarTag = SelectedTabCompetitors;
//    [_library headerUpdate:headerViewCompetitors];
//    [_library updateHeader:headerViewCompetitors];
    [self getNotifications];
    [_tabBar updateTabSelection:1];
    
    
    if([headerViewCompetitors.userFranchise.text  isEqual: @"In House"] || [headerViewCompetitors.userFranchise.text  isEqual: @"Home Office"]) {
        [theme styleHOUser:headerViewCompetitors.UserInformation];
    } else {
        [theme styleFGUser:headerViewCompetitors.UserInformation franchise:headerViewCompetitors.userFranchise.text colorCode:[ZMUserActions sharedInstance].franchiseColor];
    }
    
    // Profile Icon
    [headerViewCompetitors.profileButton addTarget:headerViewCompetitors action:@selector(profileSelected:) forControlEvents: UIControlEventTouchUpInside];
    [self geTotalOfNotifications];
    [self loadCAssetLibrary];
    [self.navigationController setNavigationBarHidden:YES];
    [self setupViewsFrames];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    [headerViewCompetitors settingsHideOnRotate];
    [headerViewCompetitors searchHideOnRotate];
    if (_exportMenuIsPresented) {
        [[_exportMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        if ([showingGridView isEqualToString:@"YES"])
        {
            ZMGridItem *tempCell = (ZMGridItem *)[_cLibraryVC.cLibraryCollectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (tempCell && tempCell.shareButton.isSelected)
            {
                [tempCell makeShareButtonSelected:NO];
            }
        }
        else
        {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_cLibraryVC.cLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.shareButton.isSelected)
            {
                [cell makeShareButtonSelected:NO];
            }
        }
        
    }
    if (_infoMenuIsPresented)
    {
        [[_infoMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        if ([showingGridView isEqualToString:@"YES"])
        {
            ZMGridItem *tempCell = (ZMGridItem *)[_cLibraryVC.cLibraryCollectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (tempCell && tempCell.infoButton.isSelected)
            {
                [tempCell makeInfoButtonSelected:NO];
            }
        }
        else
        {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_cLibraryVC.cLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.infoButton.isSelected)
            {
                [cell makeInfoButtonSelected:NO];
            }
        }
    }
}

//Capture the rotation of screens
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
//    [_library headerUpdate:headerViewCompetitors];
    [headerViewCompetitors settingsShowOnRotate];
    [headerViewCompetitors searchShowOnRotate];
    [headerViewCompetitors adjustStatusBar];
    if (_exportMenuIsPresented) {
        if([ZMUserActions sharedInstance].export_infoMenuValOnOrientation == 0)
        {
            if ([showingGridView isEqualToString:@"YES"])
            {
                ZMGridItem *tempCell = (ZMGridItem *)[_cLibraryVC.cLibraryCollectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (tempCell && !tempCell.shareButton.isSelected)
                {
                    [tempCell makeShareButtonSelected:YES];
                    [self presentExportMenu:tempCell button:tempCell.shareButton controller:_cLibraryVC];
                }
            }
            else
            {
                FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_cLibraryVC.cLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (cell && !cell.shareButton.isSelected)
                {
                    [cell makeShareButtonSelected:YES];
                    [self presentExportMenu:cell button:cell.shareButton controller:_cLibraryVC];
                }
            }
        }
        else
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
            _exportMenuIsPresented = NO;
        }
    }
    
    if (_infoMenuIsPresented)
    {
        if([ZMUserActions sharedInstance].export_infoMenuValOnOrientation == 0)
        {
            if ([showingGridView isEqualToString:@"YES"])
            {
                ZMGridItem *tempCell = (ZMGridItem *)[_cLibraryVC.cLibraryCollectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (tempCell && !tempCell.infoButton.isSelected)
                {
                    [tempCell makeInfoButtonSelected:YES];
                    [self presentInfoMenu:tempCell button:tempCell.infoButton controller:_cLibraryVC];
                }
            }
            else
            {
                FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_cLibraryVC.cLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (cell && !cell.infoButton.isSelected)
                {
                    [cell makeInfoButtonSelected:YES];
                    [self presentInfoMenu:cell button:cell.infoButton controller:_cLibraryVC];
                }
            }
        }
        else
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
            _infoMenuIsPresented = NO;
        }
    }
    
    if(assetForAudio != nil)
    {
        if (!self.cLibraryVC.cLibraryCollectionView.hidden)
        {
            [self.cLibraryVC.cLibraryCollectionView reloadData];
        }
    }
}

- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self setupViewsFrames];
    [self.cLibraryVC reloadTableViewOrCollectionView];
    [self resetTheSourceRectValueOfCSortMenuView];
    [self.view layoutSubviews];
    [feedback adjustFrame:self.view];
}


/**
 *  Displays the walkthrough component on main view
 */
- (void)displayWalkthrough {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HelpViewController * controller = (HelpViewController*)[storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    controller.userFranchise = headerViewCompetitors.userFranchise.text;
//    controller.competitors = self;
    [self.navigationController pushViewController:controller animated:NO];
}

/**
 *  Displays the notification pannel on view
 */
-(void) displayNotifications {
    
    if (!notificationsPannel.parent) {
//        if (_notificationsLoaded) {
        
            notificationsPannel = (ZMNotifications *)[[[NSBundle mainBundle] loadNibNamed:@"Notifications" owner:self options:nil] objectAtIndex:0];
            backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
            //Revamp - Make message pannel appear above tabbar
            //            [mainDelegate showHideOverlayForTabBar:NO];
            [notificationsPannel initWithParent:self.view background:backgroundView];
            //End of Revamp
            notificationsPannel.tabBarTag = SelectedTabCompetitors;
            notificationsPannel.competitors = self;
            [notificationsPannel populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
            
            //Revamp(13/12) - Show arrow delayed
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self showHideArrow:NO];
            });
        
            [notificationsPannel adjustNotifications];
            //End of Revamp
//        }
    }
}

//Revamp(13/12) - funtion to be executed when notifications is tapped
- (void)showHideArrow:(BOOL) hide {
    if (!hide) {
        headerViewCompetitors.arrow.hidden = NO;
        [headerViewCompetitors.messagesButton setImage:[UIImage imageNamed:@"messageSelectIcon"] forState:UIControlStateNormal];
        [headerViewCompetitors showHideBadge:YES];
    }
    else {
        headerViewCompetitors.arrow.hidden = YES;
        [headerViewCompetitors.messagesButton setImage:[UIImage imageNamed:@"messagesIcon"] forState:UIControlStateNormal];
        [headerViewCompetitors showHideBadge:NO];
    }
}
//End of Revamp

//Dismiss all notifications on button click
- (void)dismissAllNotifications:(NSArray *)notifications {
    
//    [_library dismissAllNotifications:notifications];
}

/**
 *  Get notifications from server
 */
-(void) getNotifications{
//    [_library getNotifications];
}

/**
 *  Changes the badge number, for both application icon and header badge
 *  @param badgeNumber
 */
-(void) changeBadgeNumber:(int)badgeNumber{
//    [_library changeBadgeNumber:badgeNumber header:headerViewCompetitors];
}

//Get total notifications for badge count
- (void)geTotalOfNotifications{
    int total = [[ZMAssetsLibrary defaultLibrary] getTotalUnreadNotifications];
    [[ZMAssetsLibrary defaultLibrary] setProperty:@"TOTAL_NOTIFICATIONS" value:[NSString stringWithFormat:@"%d", total]];
    [self changeBadgeNumber:total];
    
}

- (void)loadCCategoryMenuView
{
    if(self.cCategoryMenuVC == nil)
    {
        self.cCategoryMenuVC = [CCategoryMenuVC GetCCategoryMenuViewController];
        self.cCategoryMenuVC.cCategoryMenuVCDelegate = (id<CCategoryMenuVCDelegate>)self;
    }
    [self addChildViewController:self.cCategoryMenuVC];
    
    self.cCategoryMenuVC.view.frame = CGRectMake(16, 80, 240, self.view.height - 80);
    [self.view addSubview:self.cCategoryMenuVC.view];
    [self.cCategoryMenuVC didMoveToParentViewController:self];
}

- (void)loadCLibraryView
{
    if(self.cLibraryVC == nil)
    {
        self.cLibraryVC = [CLibraryViewController GetCLibraryViewController];
        self.cLibraryVC.cLibraryVCDelegate = (id<CLibraryViewControllerDelegate>)self;
    }
    [self addChildViewController:self.cLibraryVC];
    
    //    self.self.cLibraryVC.view.frame = CGRectMake(270, 118, 485, self.view.height - 118);
    CGFloat width  = (self.view.width - (3 * 16 + self.cCategoryMenuVC.view.width)); //16 margin,
    CGFloat height = (self.view.height - (67 + 8 + self.contentHeader.height));
    self.cLibraryVC.view.frame = CGRectMake((self.cCategoryMenuVC.view.right + 16 ), (67 + self.contentHeader.height + 8), height, width);
    
    [self.view addSubview:self.cLibraryVC.view];
    [self.cLibraryVC didMoveToParentViewController:self];
}

- (void)loadZMContenHeaderView
{
    if (self.contentHeader == nil)
    {
        self.contentHeader = (ZMContentHeader *)[[[NSBundle mainBundle] loadNibNamed:[ZMContentHeader GetZMContentHeaderNibName] owner:self options:nil] objectAtIndex:0];
        self.contentHeader.zmContentHeaderDelegate = (id<ZMContentHeaderDelegate>)self;
    }
    
    self.contentHeader.frame = CGRectMake(self.cCategoryMenuVC.view.right, 68, self.view.width - (self.cCategoryMenuVC.view.right), 42);
    [self.view addSubview:self.contentHeader];
    
    //update view
    [self.contentHeader updateSortByButton];
    
}

- (void)setupViewsFrames
{
    //Setting CCategory Menu View Size
    self.cCategoryMenuVC.view.frame = CGRectMake(16, 74, 248, self.view.height - 74);
    //Setting ContentHeader  View Size
    self.contentHeader.frame = CGRectMake(self.cCategoryMenuVC.view.right, 69, (self.view.width - self.cCategoryMenuVC.view.right), 42);
    
    //Setting CLibrary View Size
    CGFloat width  = (self.view.width - (3 * 16 + self.cCategoryMenuVC.view.width)) + 10; //16 margin,
    CGFloat height = (self.view.height - (67 + 8 + self.contentHeader.height));
    self.cLibraryVC.view.frame = CGRectMake((self.cCategoryMenuVC.view.right + 14 ), (67 + self.contentHeader.height + 8), width,height);
}

#pragma CLibraryViewController Delegate Methods

- (void)cLibraryViewController:(CLibraryViewController*)cLibraryViewController openAsset:(Asset*)asset fromListOrGridAtIndex:(NSInteger)index ofType:(NSString*)type
{
    if (asset == nil)
    {
        return;
    }
    
    if ([asset title].length > 0)
    {
        [self validateAndShowAsset:asset];
    }
    /**
     * @Tracking
     * Default Action
     **/
    
    //        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    //
    //        if ([[ZMUserActions sharedInstance] getFiltersForTracking]) {
    //            [trackingOptions setObject:[[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
    //        }
    //
    //        [trackingOptions setObject:@"click-through" forKey:@"scfilteraction"];
    //        [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    //
    //        if ([selectedAsset title].length > 0) {
    //            [trackingOptions setObject:selectedAsset.title forKey:@"scsearchclickthroughfilename"];
    //        }
    
    //        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
    //                  withSubsection:@"filter"
    //                        withName:[NSString stringWithFormat:@"brand-%@-default", [ZMUserActions sharedInstance].franchiseName]
    //                     withOptions:trackingOptions];
    
    /**
     * @Tracking
     * Engagement
     **/
    //        trackingOptions = [[NSMutableDictionary alloc] init];
    //        [trackingOptions setObject:@"" forKey:@"sccollateralfeatures"];
    
    
    // Fix for ---Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: sccollateralname)
    //        if ([selectedAsset title].length > 0)
    //            [trackingOptions setObject:selectedAsset.title forKey:@"sccollateralname"];
    
    //        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
    //                  withSubsection:@"assetview"
    //                        withName:[NSString stringWithFormat:@"%@|%@",[ZMAssetViewController getAssetType:selectedAsset], selectedAsset.title]
    //                     withOptions:trackingOptions];
    
    
}

/**
 *  With the given asset validates if the assets is local or initiates the download
 *  @param selectedAsset
 */
- (void)validateAndShowAsset:(Asset *)selectedAsset {
    
    if(self.headerViewCompetitors.categoryPopOver)
    {
        [[self.headerViewCompetitors.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    if(self.headerViewCompetitors.sortMenuPopOver)
    {
        [[self.headerViewCompetitors.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    
    [AbbvieLogging logInfo:@"*****Intro validate Asset ********"];
    [self validateAssetBeforeOpen:selectedAsset completion:^(BOOL assetNeedsDownload) {
        if (assetNeedsDownload == YES) {
            //Restricting user from opening asset if offline download is in progress
            NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:selectedAsset.assetID];
            for(OfflineDownload * data in offlineDownloadArray) {
                if(data.assetID == selectedAsset.assetID) {
                    if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if(!self.isTapDownloadInitiated)
                            {
                                [self downloadSelectedAsset:selectedAsset];
                                [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                            }
                        });
                        //                        if(_apiManager.tappedAsset == 0) {
                        //                            _apiManager.tappedAsset = data.assetID;
                        //                        }
                        self.isDownloadingAsset = NO;
                    }
                    else {
                        if(!self.isTapDownloadInitiated)
                        {
                            [self downloadSelectedAsset:selectedAsset];
                            [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                        }
                    }
                }
            }
            if(offlineDownloadArray.count == 0) {
                if(!self.isTapDownloadInitiated)
                {
                    [self downloadSelectedAsset:selectedAsset];
                    [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                }
                //                _isDownloadignAsset = NO;
            }
        }else
        {
            if(self.currentSelectedAssetToSeeDetail)
            {
                //Return in case if open of any asset is in progress
                return;
            }
            else
            {
                self.currentSelectedAssetToSeeDetail = selectedAsset;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                    selectedAsset.isUpdateAvailable = NO;
                });
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [AbbvieLogging logInfo:@"------Asset needs opened ------"];
                    [self openAsset:selectedAsset];
                });
            }
        }
    } failure:^(NSString *messageFailure) {
        [self.mainDelegate showMessage:messageFailure whithType:ZMProntoMessagesTypeWarning];
    }];
}

-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
    //    [self trackGetAsset:asset];
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
    
    if (!self.isDownloadingAsset) {
        self.isDownloadingAsset = YES;
        // check if asset is protected
        if (![UIApplication sharedApplication].protectedDataAvailable) {
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
            /// This should occur during application:didFinishLaunchingWithOptions:
            //[ASFFileProtectionManager stopProtectingLocation:asset.path];
            
            // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
            NSError *error;
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];
            
            
            if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
            {
                attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
                if (!success)
                    [AbbvieLogging logInfo:@"Set ~/Documents attrsAssetPath NOT successfull"];
            }
            
            /// ....
            NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
            [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
        }
        
        // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        if([[NSFileManager defaultManager] fileExistsAtPath:asset.path]) {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"File Exists at %@", asset.path]];
            
        }
        
        if ((![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && assetType != ZMProntoFileTypesBrightcove) && (assetType != ZMProntoFileTypesWeblink))
        {
            if ([[ZMAbbvieAPI sharedAPIManager] isConnectionAvailable]) {
                //                _isDownloadignAsset = YES;
                completion(YES);
            }
            else {
                self.isDownloadingAsset = NO;
                failure(@"The internet connection is not available, please check your network connectivity");
            }
        }else{
            //Check if the asset size matches the metada info meaning the asset is correct
            if (asset.file_size.intValue == (int)fileSize) {
                //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
                self.isDownloadingAsset = NO;
                completion(NO);
            }else{
                completion(YES);
            }
        }
    } else {
        // PRONTO-16 - Opening an Asset in AirPlane Mode
        if (![[ZMAbbvieAPI sharedAPIManager] isConnectionAvailable]) {
            
            if(self.isDownloadingAsset == NO)
            {
                
                failure(@"The internet connection is not available, please check your network connectivity");
                
                self.isDownloadingAsset = NO;
            }
            
        }
        else
            failure(@"Download in progress. Please wait and then try again.");
    }
}

/**
 *  Open selected asset, the asset could be of type PDF-VIDEO-DOCUMENT,the assets of type PDF or document will be open through FastPDFKIT, the ones of type Video thorugh Brightcove
 *
 *  @param selectedAsset Asset to open
 */

- (void)openAsset:(Asset *)selectedAsset
{
    if([selectedAsset.type isEqualToString:[EventOnlyAsset TypeAttributeValueAsString]])
    {
        EventOnlyAsset* eoaAsset = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:[selectedAsset.assetID stringValue]];
        [self eventOnlyAssetHandler:nil openAsset:eoaAsset];
    }
    else
    {
        
        //If Selected path or Selected path is not present then asset can't be opened.
        if (selectedAsset == nil || selectedAsset.path == nil) { return ;}
        
        // PRONTO-15 - Popup Getting Displayed Over PDF
        if(headerViewCompetitors.categoryPopOver)
        {
            [[headerViewCompetitors.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        }
        
        // removing deprecated warnings
        if(headerViewCompetitors.sortMenuPopOver)
        {
            [[headerViewCompetitors.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        }
        
        self.shouldResetFranchises = NO;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mainDelegate hideMessage];
        });
        
        enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
        
        self.isDownloadingAsset = NO;
        
        /*!
         * ABT-51
         */
        //enum ZMProntoFileTypes types = [self getAsset:selectedAsset];
        /*NSString *activityType = @"Pronto Object";
        if (assetType == ZMProntoFileTypesPDF) {
            activityType = @"PDF";
        }
        else if (assetType == ZMProntoFileTypesVideo) {
            activityType = @"VIDEO";
        }
        else if (assetType == ZMProntoFileTypesBrightcove) {
            activityType = @"VIDEO";
        }
        else if (assetType == ZMProntoFileTypesDocument) {
            activityType = @"DOCUMENT";
        }
        else if (assetType == ZMProntoFileTypesEpub) {
            activityType = @"EPUB";
        }
        else if(assetType == ZMProntoFileTypesWeblink) // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        {
            activityType = @"WEBLINK";
        }*/
        
        //Tracking
//        [ASFActivityLog logActionForCurrentUser:@"viewed" withObjectName:[NSString stringWithFormat:@"%@",selectedAsset.title] ofType:[NSString stringWithFormat:@"%@",activityType] completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
//            if (!result){
//                //Ignore Errors
//            }
//        }];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UIViewController <ZMAssetViewControllerDelegate> *controller;
        
        MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
        
        if (assetType == ZMProntoFileTypesPDF) {
            if(document != nil)
            {
                [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
            }
            else{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                    self.currentSelectedAssetToSeeDetail = nil;
                });
            }
        }
        else if (assetType == ZMProntoFileTypesBrightcove) {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
            //        #warning From here proper controller needs to be sent
//            controller.competitors = self;
            controller.asset = selectedAsset;
            controller.userFranchise = headerViewCompetitors.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (assetType == ZMProntoFileTypesDocument) {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
            //        #warning From here proper controller needs to be sent
//            controller.competitors = self;
            controller.asset = selectedAsset;
            controller.userFranchise = headerViewCompetitors.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
            [self.navigationController pushViewController:controller animated:YES];
        }
        // PRONTO-25 Web View - Ability to provide a Web link as an asset type.
        //
        //
        else if(assetType == ZMProntoFileTypesWeblink)
        {
            // open the weblink URL
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
            //        #warning From here proper controller needs to be sent
//            controller.competitors = self;
            controller.asset = selectedAsset;
            controller.userFranchise = headerViewCompetitors.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        else if(assetType == ZMProntoFileTypesAudio)
        {
            [self stopAudio];
            assetForAudio = selectedAsset;
            if(!audioPlayerView)
            {
                audioPlayerView = (AudioPlayer*)[[[NSBundle mainBundle] loadNibNamed:@"AudioPlayerView" owner:self options:nil] firstObject];
            }
            [audioPlayerView initialSetupWithAssetVal:selectedAsset];
            if (self.cLibraryVC.cLibraryCollectionView.hidden)
            {
                NSPredicate * predicateForList = [NSPredicate predicateWithFormat:@"uid == %@",assetForAudio.assetID];
                NSArray * cellsForList = [[self.cLibraryVC.cLibraryTableView visibleCells]filteredArrayUsingPredicate:predicateForList];
                if (cellsForList.count > 0)
                {
                    audioCurrentCell = cellsForList.firstObject;
                    audioPlayerView.frame = CGRectMake(0, 0,audioCurrentCell.audioPlayerView.frame.size.width, audioCurrentCell.audioPlayerView.frame.size.height);
                    [audioPlayerView.layer setMasksToBounds:YES];
//                    [audioPlayerView initialSetupWithAssetVal:selectedAsset];
                    audioCurrentCell.audioPlayerView.hidden = NO;
//                    self.currentSelectedAssetToSeeDetail = nil;
                    [audioCurrentCell.audioPlayerView addSubview:audioPlayerView];
                }
            }
            else
            {
                NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@",assetForAudio.assetID];
                NSArray * cells = [[self.cLibraryVC.cLibraryCollectionView visibleCells]filteredArrayUsingPredicate:predicate];
                if (cells.count > 0) {
                    audioCurrentGridCell = cells.firstObject;
                    
                    audioPlayerView.frame = CGRectMake(0, 0,audioCurrentGridCell.audioPlayerView.frame.size.width, audioCurrentGridCell.audioPlayerView.frame.size.height);
                    [audioPlayerView.layer setMasksToBounds:YES];
//                    [audioPlayerView initialSetupWithAssetVal:selectedAsset];
                    audioCurrentGridCell.audioPlayerView.hidden = NO;
//                    self.currentSelectedAssetToSeeDetail = nil;
                    [audioCurrentGridCell.audioPlayerView addSubview:audioPlayerView];
                }
            }
            self.currentSelectedAssetToSeeDetail = nil;
            self.cLibraryVC.audioPlayerView = audioPlayerView;
            self.cLibraryVC.assetForAudio = assetForAudio;
        }
        else
        {
            // Fix - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** -[NSURL initFileURLWithPath:]: nil string parameter'
            if (assetType == ZMProntoFileTypesPDF) {
                
                if(selectedAsset.path)
                {
                    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
                    if(document != nil)
                    {
                        [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
                    }else{
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [self.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                            self.currentSelectedAssetToSeeDetail = nil;
                        });
                    }
                }
            }
        }
    }
}

- (void)stopAudio
{
    if(assetForAudio != nil)
    {
        [audioPlayerView stopAudio];
        
        if(audioCurrentCell)
        {
            audioCurrentCell.audioPlayerView.hidden = YES;
            audioCurrentCell = nil;
        }
        else if(audioCurrentGridCell)
        {
            audioCurrentGridCell.audioPlayerView.hidden = YES;
            audioCurrentGridCell = nil;
        }
        assetForAudio = nil;
        [audioPlayerView removeFromSuperview];
        self.cLibraryVC.audioPlayerView = nil;
        self.cLibraryVC.assetForAudio = nil;
    }
}

- (void)stopPlayingAudio
{
    [self stopAudio];
}

//Download of an asset if not found in coredata / new

- (void) downloadSelectedAsset:(Asset *)selected {
    self.isTapDownloadInitiated = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    });
    [AbbvieLogging logInfo:@"------Asset needs download ------"];
    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    if(![selected.file_mime isEqualToString:@"weblink"])
        [self downloadAsset:selected];
}

- (void)updateOfflineDownloadCoreData:(Asset *)asset status:(NSString*)status
{
    if([asset valueForKey:@"assetID"] != nil)
    {
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
        [initialDownloadAsset setObject:status forKey:@"status"];
        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
        
//        if (offlineData.count > 0) {
//            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0]];
//        }
//        [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset];
        
        if (offlineData.count > 0)
        {
            //if  insertation and deletetion has to be happen, it will happen one by one
            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
            }];
        }
        else
        {
            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
        }
    }
}

- (void)downloadAsset:(Asset *)selectedAsset{
    
    self.currentAssetDownload = selectedAsset;
    self.isDownloadingAsset = YES;
    [self setProgress:0.001000];
    
    [_apiManager downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        [self downloadAssetDidFinish:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"completed"];
        // New revamp changes - remove the update label once the asset is downloaded successfully.
        selectedAsset.isUpdateAvailable = NO;
        if(![selectedAsset.file_mime isEqualToString:@"audio"])
        {
            [self.cLibraryVC reloadTableViewOrCollectionView];
        }
    } onError:^(NSError *error) {
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAssetOnFailure:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"failed"];
    }];
}

// Download asset for print
- (void)downloadOneAsset:(Asset *)selectedAsset completion:(void (^)(BOOL assetDownloaded))completion
                 failure:(void (^)(NSString *assetDownloadFail))failure{
    
    self.currentAssetDownload = selectedAsset;
    self.isDownloadingAsset = YES;
    [self setProgress:0.001000];
    
    [self.apiManager downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        //[self downloadAssetDidFinish:selectedAsset];
//        [self.mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", selectedAsset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
        self.isDownloadingAsset = NO;
        completion(YES);
    } onError:^(NSError *error) {
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        self.isDownloadingAsset = NO;
        completion(NO);
    }];
}

- (void)setProgress:(float)progress
{
    if (self.cLibraryVC.cLibraryTableView.isHidden == YES) // Collection view is visible
    {
        //Progress for Grid Item
        ZMGridItem *currentDownloadItem;
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@",self.currentAssetDownload.assetID];
        NSArray * cells = [[self.cLibraryVC.cLibraryCollectionView visibleCells]filteredArrayUsingPredicate:predicate];
        if (cells.count > 0) {
            currentDownloadItem = cells.firstObject;
            currentDownloadItem.progress = progress;
            currentDownloadItem.loaderBackgroundView.hidden = NO;
            [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
            [currentDownloadItem.loaderBackgroundView setAlpha:0.5];
        }
        
        if (progress >= 1) {
            //        currentAssetDownload = nil;
            [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
            [currentDownloadItem.loaderBackgroundView setAlpha:1];
            currentDownloadItem.loaderBackgroundView.hidden = YES;
        }
    }
    else //Table view is visible
    {
        //Progress for list item
        FranchiseTableViewCell *currentCell;
        
        NSPredicate * predicateForList = [NSPredicate predicateWithFormat:@"uid == %@",self.currentAssetDownload.assetID];
        NSArray * cellsForList = [[self.cLibraryVC.cLibraryTableView visibleCells]filteredArrayUsingPredicate:predicateForList];
        if (cellsForList.count > 0)
        {
            currentCell = cellsForList.firstObject;
            currentCell.progress = progress;
            
            currentCell.loaderBackgroundView.hidden = NO;
            currentCell.loaderView.hidden = NO;
            
            [currentCell.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
            [currentCell.loaderBackgroundView setAlpha:0.5];
        }
        if (progress >= 1)
        {
            self.currentAssetDownload = nil;
            currentCell.loaderBackgroundView.bounds = currentCell.bounds;
            currentCell.loaderView.center = currentCell.center;
            
            [currentCell.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
            [currentCell.loaderBackgroundView setAlpha:1];
            
            currentCell.loaderBackgroundView.hidden = YES;
            currentCell.loaderView.hidden = YES;
        }
    }
}

#pragma mark - ZMAssetsLibrary Observer methods for asset downloading

- (void)downloadAssetDidFinish:(Asset *)asset {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate hideMessage];
    });
    
    self.isDownloadingAsset = NO;
    self.isTapDownloadInitiated = NO;
    
    [self.mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", asset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self validateAndShowAsset:asset];
    });
}

- (void)downloadAssetDidNotFinish:(Asset *)asset error:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.mainDelegate.currentMessage){
            [self.mainDelegate hideMessage];
        }
        
        self.isDownloadingAsset = NO;
        self.isTapDownloadInitiated = NO;
        
        NSString *message;
        enum ZMProntoMessages type = 1;
        
        // NSInteger errCode = ABS(error.code);
        
        if (error.code == 503) {
            type = ZMProntoMessagesTypeWarning;
            message = @"The server is currently under maintenance mode.";
        }
        
        else if (![_apiManager isConnectionAvailable]) {
            type = ZMProntoMessagesTypeWarning;
            message = @"The Internet connection is not available, please check your network connectivity.";
        }
        else if(error.code == 401) {
            //Restoring the user session
            [self.mainDelegate initPronto];
            return;
        }
        else if (error.code == 404) {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"Asset not found %@", asset.title];
        }
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        else if (error.code == 5303 || error.code == -5303)
        {
            message = @"Error while retreiving assets, please try again later";
        }
        else if (error.code == 1009 || error.code == -1009)
        {
            message = @"Please check the network connection";
        }
        else if (error.code == 1003 || error.code == -1003)
        {
            message = @"A server with the specified hostname could not be found";
        }
        else {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"There is a problem downloading %@", asset.title];
        }
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mainDelegate showMessage:message whithType:type withAction:^{
                [self.mainDelegate hideMessage];
                [self validateAndShowAsset:asset];
            }];
        });
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        
        
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        if ([message containsString:@"ERROR_CODE"])
        {
            // errorcode consists of digits
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",@"Please check the network connection"] forKey:@"scerrorcode"];
        }
        else
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",message] forKey:@"scerrorcode"];
        
        ZMGridItem *currentDownloadItem;
        for (ZMGridItem *cell in [self.cLibraryVC.cLibraryCollectionView visibleCells])
        {
            if(self.currentAssetDownload.assetID.longValue == cell.uid)
            {
                currentDownloadItem = cell;
                break;
            }
        }
        currentDownloadItem.progress = 1;
        self.currentAssetDownload = nil;
        
        /**
         * @Tracking
         * Download Did Not finished
         **/
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"assetview" withName:nil withOptions:trackingOptions];
    });
}

- (void)openFastPDFWithAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
//    controller.competitors = self;
    controller.asset = asset;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    //    [view addSubview:controller.view];
    
    [viewController.view addSubview:view];
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    //    controller.view.frame = view.bounds;
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
    
    //    [controller didMoveToParentViewController:viewController];
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor GetColorFromHexValueE0E6ED];
    [viewController.view addSubview:webview];
    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    [self hideMessage];
    controller.userFranchise = headerViewCompetitors.userFranchise.text;
    controller.viewMain = view;
    controller.webviewScreen = webview;
    controller.webviewScreen.hidden = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (void)hideMessage
{
    [self.mainDelegate hideMessage];
}

- (void)cLibraryViewController:(CLibraryViewController*)cLibraryViewController cell:(id)cell  configureExportPopOver:(UIButton*) sender
{    [self presentExportMenu:cell button:sender controller:cLibraryViewController];
}

- (void)presentExportMenu:(id)cell button:(UIButton*)sender controller:(CLibraryViewController*)cLibraryViewController
{
    [AbbvieLogging logInfo:@"Export button clicked on cell"];
    self.popOverPresentingCell = cell;
    
    CGRect frame = sender.frame;
    CGRect rect = CGRectZero;
    NSNumber * assetId = 0;
    selectedItem = sender.tag;
    rect =  [cell convertRect:cLibraryViewController.cLibraryTableView.frame fromView: self.view];
    frame.origin.x = 68;
    if ([cell isKindOfClass:[FranchiseTableViewCell class]])
    {
        self.currentlySelectedAsset = ((FranchiseTableViewCell*)cell).currentAsset;
    }
    else
    {
        self.currentlySelectedAsset = ((ZMGridItem*)cell).currentAsset;
    }
    
    self.exportMenu = (ExportMenu *)[[[NSBundle mainBundle] loadNibNamed:@"ExportMenu" owner:self options:nil] objectAtIndex:0];
    self.exportMenu.exportMenuDelegate = (id<ExportMenuDelegate>)self;
    self.library.exportMenuIsPresented = YES; // As this value is being checked to dismiss the view.
    self.exportMenu.competitors = self;
    
    // values in Export and Info menu based on the cell selection
    if ([cell isKindOfClass:[FranchiseTableViewCell class]])
        assetId = [NSNumber numberWithLong:((FranchiseTableViewCell*)cell).uid];
    else
        assetId = [NSNumber numberWithLong:((ZMGridItem*)cell).uid];
    
    NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
    if (assetArray.count > 0) {
        self.currentlySelectedAsset = assetArray[0];
    }
    
    [self.exportMenu selectingAssetWithDetails:self.currentlySelectedAsset withSender:sender];
    // self.exportMenu.parent = self;
    
    NSMutableArray * exportMutableArray =[[NSMutableArray alloc]init];
    // check and display Print Option
    if(self.currentlySelectedAsset.field_print.length > 0 && [self.currentlySelectedAsset.field_print isEqualToString:@"1"])
    {
        [exportMutableArray addObject:@"Print"];
    }
    [exportMutableArray addObject:@"Share"];
    // check and display Email to self Option
    if(self.currentlySelectedAsset.field_email_to_self.length > 0 && [self.currentlySelectedAsset.field_email_to_self isEqualToString:@"1"])
    {
        [exportMutableArray addObject:@"Email to self"];
    }
    [exportMutableArray addObject:@"Feedback"];
    
    self.exportMenu.exportArray = exportMutableArray;
    self.exportMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.exportMenu.preferredContentSize = CGSizeMake(178, [exportMutableArray count]*44);
    if (fabs(rect.origin.x) < 300 ) {
        self.exportMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomPopoverBackgroundView class];
    } else {
        frame.origin.x = 0;
        self.exportMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomInfoPopoverBackgroundView class];
    }
    [self presentViewController:self.exportMenu animated:YES completion:nil];
    
    // configure the Popover presentation controller
    _exportMenuPopOver = [self.exportMenu popoverPresentationController];
    _exportMenuPopOver.delegate = self;
    _exportMenuPopOver.sourceView = [sender superview];
    _exportMenuPopOver.sourceRect = frame;
    
    CGFloat deviceHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat value = (fabs(rect.origin.y) + ((exportMutableArray.count) * 44) );
    value =  value + 67+8+42+100; //conternt header height, top, margin
    if (deviceHeight > value)
    {
        _exportMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    else
    {
        _exportMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionDown;
    }
    
    int point = [Constant GetPointOnOrientation];
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown)
    {
        if(deviceHeight < value)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = value - deviceHeight;
        }
        if(fabs(rect.origin.y) > point)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 1;
        }
    }
    _exportMenuIsPresented = YES;
}

- (void)cLibraryViewController:(CLibraryViewController*)cLibraryViewController cell:(id)cell  configureInfoPopOver:(UIButton*) sender
{
    [self presentInfoMenu:cell button:sender controller:cLibraryViewController];
}

- (void)presentInfoMenu:(id)cell button:(UIButton*)sender controller:(CLibraryViewController*)cLibraryViewController
{
    //NSLog(@"Info button clicked on cell");
    self.popOverPresentingCell = cell;
    CGRect frame = sender.frame;
    CGRect rect = CGRectZero;
    selectedItem = sender.tag;
    rect =  [cell convertRect:cLibraryViewController.cLibraryTableView.frame fromView: self.view];
    frame.origin.x = 68;
    //    self.currentlySelectedAsset = cell.currentAsset;
    
    if ([cell isKindOfClass:[FranchiseTableViewCell class]])
    {
        self.currentlySelectedAsset = ((FranchiseTableViewCell*)cell).currentAsset;
    }
    else
    {
        self.currentlySelectedAsset = ((ZMGridItem*)cell).currentAsset;
    }
    
    if (!self.infoMenu)
    {
        self.infoMenu = (InfoMenu *)[[[NSBundle mainBundle] loadNibNamed:@"InfoMenu" owner:self options:nil] objectAtIndex:0];
//        self.infoMenu.library = self.library;
        self.library.infoMenuIsPresented = YES;
       
        NSNumber * assetId = 0;
        // values in Export and Info menu based on the cell selection
        if ([cell isKindOfClass:[FranchiseTableViewCell class]])
            assetId = [NSNumber numberWithLong:((FranchiseTableViewCell*)cell).uid];
        else
            assetId = [NSNumber numberWithLong:((ZMGridItem*)cell).uid];
        
        // selecting menu from cell
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
        if (assetArray.count > 0) {
            self.currentlySelectedAsset = assetArray[0];
        }
        
        // expiredDate
        NSNumber* expiredOn = self.currentlySelectedAsset.unpublish_on;
        NSTimeInterval timeInterval = [expiredOn doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd/MM/yyyy"];
        NSString* dateString = [df stringFromDate:date];
        
        // lastUpdated Date
        NSNumber* lastUpdated = self.currentlySelectedAsset.changed;
        NSTimeInterval lastTimeInterval = [lastUpdated doubleValue];
        NSDate *lastDate = [NSDate dateWithTimeIntervalSince1970:lastTimeInterval];
        NSString* lastDateString = [df stringFromDate:lastDate];
        
        // [self.infoMenu selectingAssetWithDetails:_currentSelectedAsset withSender:sender];
        if (self.cLibraryVC.cLibraryTableView.isHidden == NO && self.cLibraryVC.cLibraryCollectionView.isHidden == YES)
        {
            // Tableview is visible
            self.infoMenu.totalViewsCount = [NSString stringWithFormat:@"%d",self.currentlySelectedAsset.view_count.intValue];
        }
        else
        {
            // Grid View is visible
            self.infoMenu.totalViewsCount = [NSString stringWithFormat:@"%d",((ZMGridItem*)cell).totalViews];
        }
        self.infoMenu.expiredDate = dateString;
        self.infoMenu.lastUpdatedDate = lastDateString;
        self.infoMenu.medRegNo = self.currentlySelectedAsset.medRegNo;
    }
    
    NSMutableArray * infoMutableArray =[[NSMutableArray alloc]init];
    [infoMutableArray addObject:@"Number Of Views"];
    [infoMutableArray addObject:@"Med/Reg Number"];
    [infoMutableArray addObject:@"Last Updated"];
    [infoMutableArray addObject:@"Expiration Date"];
    self.infoMenu.infoArray = infoMutableArray;
    
    self.infoMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.infoMenu.popoverPresentationController.sourceRect = [sender frame];
    self.infoMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomInfoPopoverBackgroundView class];
    self.infoMenu.preferredContentSize = CGSizeMake(158, [infoMutableArray count]*44);
    [self presentViewController:self.infoMenu animated:YES completion:nil];
    
    // configure the Popover presentation controller
    self.infoMenuPopOver = [self.infoMenu popoverPresentationController];
    CGFloat deviceHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat value = (fabs(rect.origin.y) + ((infoMutableArray.count) * 44));
    value =  value + 67+8+42+100; //conternt header height, top, margin
    if (deviceHeight > value)
    {
        self.infoMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    else
    {
        self.infoMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionDown;
    }
    self.infoMenuPopOver.delegate = self;
    self.infoMenuPopOver.sourceView = [sender superview];
    self.infoMenuPopOver.sourceRect = sender.frame;
    
    int point = [Constant GetPointOnOrientation];
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown)
    {
        if(deviceHeight < value)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = value - deviceHeight;
        }
        if(fabs(rect.origin.y) > point)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 1;
        }
    }
    
    _infoMenuIsPresented = YES;
}

- (void)updateFavArrayID
{
    NSString *currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
    // New Revamping changes - fav assets disappear on deletion
    NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
    NSMutableArray *favArray = [[NSMutableArray alloc]init];
    for(FavFranchise *fav in favoritefranchise)
    {
        BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:fav.bucketName];
        if (isPresent  && fav.asset_ID != nil)
        {
            [favArray addObject:fav.asset_ID];
        }
    }
    
    // display the asset count on selecting Promoted asset as Fav asset
    if([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:ProntoTabOptionCompetitorsFavoriteAssets])
    {
        [self.contentHeader updateFranchiseLabelCount:(int)favArray.count currentTab:ProntoTabOptionCompetitor];
    }
    
    // check the empty array
    if(favArray.count > 0)
    {
        ZMProntoManager.sharedInstance.favArrayIds = favArray;
    }
    else
    {
        ZMProntoManager.sharedInstance.favArrayIds = @[];
    }
    self.cCategoryMenuVC.cCategoryViewModel.currentFilter.favoriteIdsArray = ZMProntoManager.sharedInstance.favArrayIds;
}

- (void)cLibraryViewController:(CLibraryViewController*)cLibraryViewController cell:(id)cell  configureFavButton:(UIButton*) sender
{
    FranchiseTableViewCell *listCell = nil;
    ZMGridItem *tempCell = nil;
    NSNumber * assetId = 0;
    
    // grid
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if ([showingGridView isEqualToString:@"YES"]) {
        tempCell = cell;
        assetId = [NSNumber numberWithLong:tempCell.uid];
    }
    
    // list
    else
    {
        listCell = cell;
        assetId = [NSNumber numberWithLong:listCell.uid];
    }
    
    
    
    NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
    if (assetArray.count > 0) {
        _currentSelectedAsset = assetArray[0];
    }
    //Revamp(1/12) - Adding into core data
    favIconTapped = YES;
    NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
    [favFranchiseObject setObject:assetId forKey:@"asset_ID"];
    [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionAsString] forKey:@"bucketName"];
    [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];
    
    NSString *currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
//    bool justAdded = false;
    
    // Revamp changes - for like function service call
    ZMEvent *ratingEvent = [[ZMEvent alloc] init];
    ratingEvent.assetId = _currentSelectedAsset.assetID.longValue;
    ratingEvent.eventType = ZMEventLike;
    NSString *rating = @"liked";
    NSInteger rate = 100;
    NSNumber *k = [NSNumber numberWithInt:1];
    NSNumber *totalVotes = [NSNumber numberWithInt:([k intValue] + [_currentSelectedAsset.votes intValue])];
    
    
    if ([sender isSelected])
    {
        [sender setImage:[UIImage imageNamed:@"favIcon"] forState:UIControlStateNormal];
        [sender setSelected:NO];
        
        NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:assetId];
        if (favfranchise.count > 0)
        {
            //check wheather it belongs to other tab as well,
            //if yes, then remove only bucket name and update to database
            // if no, then directly remove it
            FavFranchise *favFranchise = favfranchise[0];
            NSString *franchiseBucketName = favFranchise.bucketName;
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameFromBucket:franchiseBucketName byRemovingBucketName:currentBucketName];
            if (modifiedFranchiseBucketName == nil)
            {
                // It means that It belongs from one bucket and therefore delete it
                [_actions deleteFavFranchise:favfranchise[0] complete:^{
                    [self updateFavArrayID];
                } error:^(NSError *error) {
                }];
            }
            else
            {
                // It means that it belongs to other bucket as well and therefore
                //Remove it for current and keep it for others
                favFranchise.bucketName = modifiedFranchiseBucketName;
                [_actions updatePersistentStoreWithCurrentContext];
                
                [self updateFavArrayID];
                
                [favFranchiseObject setObject:modifiedFranchiseBucketName forKey:@"bucketName"];
            }
            
            // update the count
            if(_actions.assetsCount >= 1 && [[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:ProntoTabOptionCompetitorsFavoriteAssets])
            {
                _actions.assetsCount = _actions.assetsCount-1;
                [self.contentHeader updateFranchiseLabelCount:_actions.assetsCount currentTab:ProntoTabOptionCompetitor];
            }
        }
        else
        {
            [AbbvieLogging logInfo:@"favfranchise array is empty"];
        }
        
        //Revamp(26/12) - Handling heart bounce animation
        // Revamp changes - Like Action during unselect
        if(_currentSelectedAsset.votes.intValue > 0)
        {
            _currentSelectedAsset.votes = [NSNumber numberWithInt:([_currentSelectedAsset.votes intValue]-[k intValue])];
        }
        else
        {
            _currentSelectedAsset.votes = [NSNumber numberWithInt:0];
        }
        _currentSelectedAsset.user_vote = @"0";
        ratingEvent.eventType = ZMEventUnlike;
        rating = @"disliked";
        rate = 0;
        //        sender.tag = 100;
    }
    else
    {
        /// two condition, if asset is available for favorite, it means it is favorite for other bucket
        // if not then, it is not favourite already, it should be added directly
        NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:assetId];
        if (favfranchise.count > 0)
        {
            FavFranchise *favFranchise = favfranchise[0];
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
            favFranchise.bucketName = modifiedFranchiseBucketName;
            [_actions updatePersistentStoreWithCurrentContext];
        }
        else
        {
            [_actions insertNewFavouriteFranchise:favFranchiseObject complete:^() {
                NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
                NSMutableArray *favArray = [[NSMutableArray alloc]init];
                for(FavFranchise *fav in favoritefranchise)
                {
                    BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:fav.bucketName];
                    if (isPresent  && fav.asset_ID != nil)
                    {
                        [favArray addObject:fav.asset_ID];
                    }
                }
                
                if(favArray.count > 0)
                {
                    ZMProntoManager.sharedInstance.favArrayIds = favArray;
                }
                else
                {
                    ZMProntoManager.sharedInstance.favArrayIds = @[];
                }
                
                self.cCategoryMenuVC.cCategoryViewModel.currentFilter.favoriteIdsArray = ZMProntoManager.sharedInstance.favArrayIds;
                
                double delayInSeconds = 0.4;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self updateFavGridList];
                    [self loadCAssetLibrary];
                });
            }
                                            error:^(NSError *error)
             {
             }];
        }
        
        //End of revamp
        [sender setImage:[UIImage imageNamed:@"favIconSelect"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        //Revamp(26/12) - Handling heart bounce animation
//        justAdded = true;
        
        // Revamp changes - Like Action during selection
        if(_currentSelectedAsset.votes.intValue >0)
            _currentSelectedAsset.votes = totalVotes;
        else
            _currentSelectedAsset.votes = [NSNumber numberWithInt:0];
        
        _currentSelectedAsset.user_vote = @"100";
        //        sender.tag = 101;
        
    }
    
    // Revamp changes - like action - after the selection
    ratingEvent.eventValue = [NSString stringWithFormat:@"%ld", (long)rate];
    
    //Event created from bucket id
    ratingEvent.createdFromBucketId = [[Constant sharedConstant] getCurrentActiveTabOptionIDAsString].integerValue;
    //Saves the rating to the local DB
    [[ZMAssetsLibrary defaultLibrary] saveEvent:ratingEvent];
    
    [[ZMCDManager sharedInstance] updateAsset:_currentSelectedAsset success:^{
        //Send the events to the server
        [self updateAssetAndSendEventToServer];
        
        //Tracking
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                  withSubsection:@"assetview" withName:[NSString stringWithFormat:@"%@-rateit",[ZMAssetViewController getAssetType:_currentSelectedAsset]]
                     withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"sctierlevel":@"tier3", @"scrating":rating}]];
        
    } error:^(NSError * error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on ZMHeader %@",error.localizedDescription]];
    }];
    
    // bouncing animation of fav button
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.duration = .2;
    animation.repeatCount = 1;
    if ([showingGridView isEqualToString:@"YES"])
    {
        animation.fromValue = [NSValue valueWithCGPoint:tempCell.favButton.center];
        animation.toValue = [NSValue valueWithCGPoint:CGPointMake(tempCell.favButton.center.x, tempCell.favButton.center.y-10)];
        
    }
    else
    {
        animation.fromValue = [NSValue valueWithCGPoint:listCell.favButton.center];
        animation.toValue = [NSValue valueWithCGPoint:CGPointMake(listCell.favButton.center.x, listCell.favButton.center.y-10)];
    }
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    
    //Revamp(26/12) - Handling heart bounce animation
    if ([showingGridView isEqualToString:@"YES"])
    {
        [tempCell.favButton.layer addAnimation:animation forKey:@"position"];
    }
    else
    {
        [listCell.favButton.layer addAnimation:animation forKey:@"position"];
    }
    
        double delayInSeconds = 0.4;
//        double delayInSeconds = 0.6;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    if([self.cCategoryMenuVC.cCategoryViewModel.competitorsCategoryArray[1][@"selected"]  isEqual: @"1"])
    {
        NSMutableArray *catArray = [NSMutableArray array];
        NSString *favoriteAssetTagName  = [Constant GetFavoritesAssetTagName];
        for (NSDictionary *dict in self.cCategoryMenuVC.cCategoryViewModel.competitorsCategoryArray)
        {
            if ([dict[@"selected"] isEqualToString:@"1"])
            {
                [catArray addObject:dict[@"id"]];
            }
            if ([dict[@"name"] isEqualToString:favoriteAssetTagName])
            {
                [catArray addObject:dict[@"id"]];
            }
        }
        [ZMProntoManager sharedInstance].selectedCategories = catArray;
    }
    
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [self updateFavGridList];
        [self loadCAssetLibrary];
        
        if(![self.cCategoryMenuVC.cCategoryViewModel.competitorsCategoryArray[1][@"selected"]  isEqual: @"1"])
        {
            [self.cLibraryVC.cLibraryTableView reloadData];
        }
        
        if (self.cLibraryVC.cLibraryViewModel.cLibraryAssets.count <= 0)
        {
            [self showEmptyView];
        }
    });
    [self.cCategoryMenuVC reloadTableView];
    [self.cCategoryMenuVC showBouncingEffectOnFavouriteCountLabel];
}


- (void)markLiked:(Asset *)selectedAsset type:(NSInteger)type{
    
    //If selectedAsset == nil, There is nothing to track
    if (selectedAsset == nil || selectedAsset.assetID == nil) { return; }
    
    __block NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    NSString *currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
    NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:selectedAsset.assetID];
    
    [self.cLibraryVC.cLibraryViewModel.cLibraryAssets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        Asset *asset = obj;
        if ([asset isKindOfClass:[Asset class]]) {
            if (asset.assetID.longLongValue == selectedAsset.assetID.longValue){
                
                [self.cLibraryVC.cLibraryViewModel.cLibraryAssets replaceObjectAtIndex:idx withObject:selectedAsset];
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:0];
                [indexPaths addObject:indexPath];
                
            }
        }
    }];
    NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
    NSMutableArray *favArray = [[NSMutableArray alloc]init];
    NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
    
    if (favoritefranchise.count == 0 && type == ZMEventLike) {
        [favArray addObject: selectedAsset.assetID];
        [favFranchiseObject setObject: selectedAsset.assetID forKey:@"asset_ID"];
        [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionAsString] forKey:@"bucketName"];
        [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];
        
        if (favfranchise.count > 0)
        {
            FavFranchise *favFranchise = favfranchise[0];
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
            favFranchise.bucketName = modifiedFranchiseBucketName;
            [_actions updatePersistentStoreWithCurrentContext];
        }
        else
        {
            [_actions insertNewFavouriteFranchise:favFranchiseObject complete:^() {}
                                            error:^(NSError *error) {
                                            }];
        }
        
    } else {
        int count = 0;
        for(FavFranchise *fav in favoritefranchise) {
            if([fav.asset_ID longValue] !=  selectedAsset.assetID.longValue && type == ZMEventLike) {
                count += 1;
            }
            else if ([fav.asset_ID longValue] ==  selectedAsset.assetID.longValue) {
                break;
            }
        }
        if(count == favoritefranchise.count) {
            [favArray addObject: selectedAsset.assetID];
            [favFranchiseObject setObject:  selectedAsset.assetID forKey:@"asset_ID"];
            [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionAsString] forKey:@"bucketName"];
            [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];
            
            if (favfranchise.count > 0)
            {
                FavFranchise *favFranchise = favfranchise[0];
                NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
                favFranchise.bucketName = modifiedFranchiseBucketName;
                [_actions updatePersistentStoreWithCurrentContext];
            }
            else
            {
                [_actions insertNewFavouriteFranchise:favFranchiseObject complete:^() {}
                                                error:^(NSError *error) {
                                                }];
            }
        }
    }
    ZMProntoManager.sharedInstance.favArrayIds = favArray;
    
    for(FavFranchise *fav in favoritefranchise) {
        if([fav.asset_ID longValue] == selectedAsset.assetID.longValue && type == ZMEventUnlike) {
            NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById: selectedAsset.assetID];
            if (favfranchise.count > 0) {
                
                FavFranchise *favFranchise = favfranchise.firstObject ;
                NSString *franchiseBucketName = favFranchise.bucketName;
                NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameFromBucket:franchiseBucketName byRemovingBucketName:currentBucketName];
                if (modifiedFranchiseBucketName == nil)
                {
                    [_actions deleteFavFranchise:favfranchise.firstObject complete:^{
                    } error:^(NSError *error) {
                    }];
                }
                else
                {
                    favFranchise.bucketName = modifiedFranchiseBucketName;
                    [_actions updatePersistentStoreWithCurrentContext];
                }
                
                // update the count
                if(_actions.assetsCount >= 1 && ([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:ProntoTabOptionCompetitorsFavoriteAssets]))
                {
                    _actions.assetsCount = _actions.assetsCount-1;
                    [headerViewCompetitors addBreadCumb:[ZMProntoManager sharedInstance].franchisesToUpdateHeader total: _actions.assetsCount];
                }
            } else {
                [AbbvieLogging logInfo:@"favfranchise array is empty"];
            }
            
        }
    }
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"fav franchise:%@",[[ZMCDManager sharedInstance] getFavFranchise]]];
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.cCategoryMenuVC.cCategoryMenuTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation: UITableViewRowAnimationNone];
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if ([showingGridView isEqualToString:@"YES"]) {
        [self.cLibraryVC.cLibraryCollectionView reloadItemsAtIndexPaths:indexPaths];
        
    } else {
        [self.cLibraryVC.cLibraryTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation: UITableViewRowAnimationNone];
    }
    
    NSArray *favfranchiseReload = [[ZMCDManager sharedInstance] getFavFranchise];
    NSMutableArray *favArrayReload = [[NSMutableArray alloc]init];
    for(FavFranchise *fav in favfranchiseReload) {
        [favArrayReload addObject:fav.asset_ID];
    }
    self.cCategoryMenuVC.cCategoryViewModel.currentFilter.favoriteIdsArray = favArrayReload;
    [self loadCAssetLibrary];
    
}

- (void) updateAssetAndSendEventToServer {
    
    [[ZMCDManager sharedInstance] updateAsset: _currentSelectedAsset success:^{
        //Send the events to the server
        dispatch_async(dispatch_queue_create("Sending Events from Competitors", NULL), ^{
            [[ZMAbbvieAPI sharedAPIManager] uploadEvents:^{
                [AbbvieLogging logInfo:@">>> Events Synced"];
            } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with uploadEvents: %@", error]];
            }];
        });
        
    } error:^(NSError * error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on Competitors %@",error.localizedDescription]];
    }];
    
}

// Update Grid with fav assets
-(void) updateFavGridList
{
    //To avoid making the asset count to nil on fav tap after search
    for(Asset * val in [ZMProntoManager sharedInstance].assetsIdToFilter)
    {
        if(val)
        {
            [ZMProntoManager sharedInstance].assetsIdToFilter = @[];
            break;
        }
    }
    
    [[ZMCDManager sharedInstance] getFavAssets:0 limit:-1 success:^(NSArray* totalAssets) {

        NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
        
        if (_pageIndex == 0 ) {
            _cLibraryVC.cLibraryViewModel.cLibraryAssets = [[NSMutableArray alloc] init];
            
        }
        
        NSMutableArray *page = [[NSMutableArray alloc]initWithArray:totalAssets];
        
        if ([page count] > 0) {
            
            if ([_cLibraryVC.cLibraryViewModel.cLibraryAssets count] == _actions.assetsCount || _cLibraryVC.cLibraryCollectionView.contentOffset.y == 0 || [_cLibraryVC.cLibraryViewModel.cLibraryAssets count] == 0) {
                [_cLibraryVC.cLibraryViewModel.cLibraryAssets addObjectsFromArray:page];
                //                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //                    [self hideScrollSpinner];
                //
                //                });
                //
                //                _assetsInPage = assetsPage;
                //                _viewGrid = grid;
            }
            
            else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([showingGridView isEqualToString:@"YES"])
                    {
                        [_cLibraryVC.cLibraryCollectionView performBatchUpdates:^{
                            int resultsSize = (int)[_cLibraryVC.cLibraryViewModel.cLibraryAssets count]; //data is the previous array of data
                            [_cLibraryVC.cLibraryViewModel.cLibraryAssets addObjectsFromArray:page];
                            NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
                            for (int i = resultsSize; i < resultsSize + page.count; i++) {
                                [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                            }
                            [_cLibraryVC.cLibraryCollectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
                            
                        } completion:^(BOOL finished) {
                            //                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            //                                [self hideScrollSpinner];
                            //                                _viewGrid = grid;
                            //                            });
                        }];
                        
                    }
                    else
                    {
                        [_cLibraryVC.cLibraryTableView beginUpdates];
                        int resultsSize = (int)[_cLibraryVC.cLibraryViewModel.cLibraryAssets count];
                        [_cLibraryVC.cLibraryViewModel.cLibraryAssets addObjectsFromArray:page];
                        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
                        for (int i = resultsSize; i < resultsSize + page.count; i++) {
                            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        }
                        [_cLibraryVC.cLibraryTableView insertRowsAtIndexPaths: arrayWithIndexPaths withRowAnimation: UITableViewRowAnimationAutomatic];
                        [_cLibraryVC.cLibraryTableView endUpdates];
                    }
                });
                
                //                _assetsInPage = assetsPage;
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (([page count] > 0) || _pageIndex < 1) {
                
                //                if (page.count == 0) {
                //
                //                    [self displayEmpty:NO];
                //                }
                //                else {
                //                    [self hideEmpty];
                //                }
                if ([showingGridView isEqualToString:@"YES"]) {
                    [_cLibraryVC.cLibraryCollectionView reloadData];
                    [self.contentHeader updateFranchiseLabelCount:_cLibraryVC.cLibraryViewModel.cLibraryAssets.count currentTab:ProntoTabOptionCompetitor];
                    [ZMProntoManager sharedInstance].selectedCategories = @[];
                }
                else
                {
                    [_cLibraryVC.cLibraryTableView reloadData];
                    [self.contentHeader updateFranchiseLabelCount:_cLibraryVC.cLibraryViewModel.cLibraryAssets.count currentTab:ProntoTabOptionCompetitor];
                    [ZMProntoManager sharedInstance].selectedCategories = @[];
                }
                //[promotedGrid reloadData];
                [headerViewCompetitors folderButton].enabled = YES;
                
            }
            
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //                [self hideScrollSpinner];
            //
            //
            //            });
            
        });
        
    }];
    
}

-(void)loadCAssetLibrary
{
    Filter * filter = self.cCategoryMenuVC.cCategoryViewModel.currentFilter;
    
    if (filter.selectedCategories.count > 0)
    {
        NSInteger idVal = [[filter.selectedCategories lastObject] integerValue];
        if (idVal != 1)
        {
            filter.favoriteIdsArray = @[];
        }
    }
    
    [[ZMCDManager sharedInstance] getAssetsWithFilter:filter offSet:_pageIndex limit:_itemsPerPage andCompletionHandler:^(NSArray * assets)
     {
         NSMutableArray * filteredAssets = [assets mutableCopy];
         
         _actions.assetsCount = (int)filteredAssets.count;
         [self hideEmpty];
         self.cLibraryVC.cLibraryViewModel.cLibraryAssets = filteredAssets;
         [self.contentHeader updateFranchiseLabelCount:filteredAssets.count currentTab:ProntoTabOptionCompetitor];
         if(_actions.assetsCount == 0)
         {
             [self showEmptyView];
         }
         else
         {
             [empty setHidden:YES];
         }
         [self.cLibraryVC reloadTableViewOrCollectionView];
     }];
}

- (void)initiateFeedback:(NSString*) selectedAsset openedFrom:(NSString*) choice {
    //    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    feedback = (FeedbackView *)[[[NSBundle mainBundle] loadNibNamed:@"FeedbackViewController" owner:self options:nil] objectAtIndex:0];
    feedback.assetId = selectedAsset;
    // display feedback view on top of the view
    backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [feedback initWithParent:[UIApplication sharedApplication].keyWindow background:backgroundView openedFrom:choice];
    [[_exportMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        if(_exportMenuIsPresented) {
            NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
            if ([showingGridView isEqualToString:@"YES"]) {
                FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[self.cLibraryVC.cLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
                [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
                [cell.shareButton setSelected:NO];
            } else {
                ZMGridItem *cell = (ZMGridItem *)[self.cLibraryVC.cLibraryCollectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
                [cell.shareButton setSelected:NO];
            }
            [_exportMenu dismissViewControllerAnimated:NO completion:nil];
            _exportMenuIsPresented = NO;
        }
}

#pragma mark Category Menu Delegate Methods
- (void)didUpdatedDataSource
{
    [self loadCAssetLibrary];
}

- (void)exportMenu:(ExportMenu*)exportMenu didDeselectRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (_library.exportMenuIsPresented) {
        [[_library.exportMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    
    if (self.popOverPresentingCell != nil)
    {
        // Pop over were presented from list
        if ([self.popOverPresentingCell isKindOfClass:[FranchiseTableViewCell class]])
        {
            FranchiseTableViewCell *cell = (FranchiseTableViewCell*)self.popOverPresentingCell;
            [cell makeShareButtonSelected:NO];
            
        }
        else if ([self.popOverPresentingCell isKindOfClass:[ZMGridItem class]])
        {
            ZMGridItem *cell = (ZMGridItem*)self.popOverPresentingCell;
            [cell makeShareButtonSelected:NO];

        }
    }
}

#pragma mark - Popover delegate Method
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    if (self.infoMenuIsPresented == YES)
    {
        [self doSetupAfterDismissalOfInfoMenuView];
        [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
        _infoMenuIsPresented = NO;
    }
    if (self.exportMenuIsPresented == YES)
    {
        [self doSetupAfterDismissalOfExportView];
        [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
        _exportMenuIsPresented = NO;
    }
}

- (void)doSetupAfterDismissalOfExportView
{
    [self.exportMenu dismissViewControllerAnimated:NO completion:nil];
    
    if (self.popOverPresentingCell != nil)
    {
        // Pop over were presented from list
        if ([self.popOverPresentingCell isKindOfClass:[FranchiseTableViewCell class]])
        {
            FranchiseTableViewCell *cell = (FranchiseTableViewCell*)self.popOverPresentingCell;
            [cell makeShareButtonSelected:NO];
            
        }
        else if ([self.popOverPresentingCell isKindOfClass:[ZMGridItem class]])
        {
            ZMGridItem *cell = (ZMGridItem*)self.popOverPresentingCell;
            [cell makeShareButtonSelected:NO];
        }
    }
}

- (void)doSetupAfterDismissalOfInfoMenuView
{
    [self.infoMenu dismissViewControllerAnimated:NO completion:nil];
    
    if (self.popOverPresentingCell != nil)
    {
        // Pop over were presented from list
        if ([self.popOverPresentingCell isKindOfClass:[FranchiseTableViewCell class]])
        {
            FranchiseTableViewCell *cell = (FranchiseTableViewCell*)self.popOverPresentingCell;
            [cell makeInfoButtonSelected:NO];
            
        }
        else if ([self.popOverPresentingCell isKindOfClass:[ZMGridItem class]])
        {
            ZMGridItem *cell = (ZMGridItem*)self.popOverPresentingCell;
            [cell makeInfoButtonSelected:NO];
        }
    }
}

/**
 *  @private
 *  Mark the asset as read
 *  @param selectedAsset
 */
- (void)markAssetAsRead:(Asset *) selectedAsset
{
    ZMGridItem *cellView;
    for (ZMGridItem *cell in [self.cLibraryVC.cLibraryCollectionView visibleCells])
    {
        if (selectedAsset.assetID.longValue == cell.uid)
        {
            cellView = cell;
            break;
        }
    }
    cellView.totalViews = selectedAsset.view_count.intValue;
    cellView.read = YES;
}

#pragma mark - Notifications

- (void)registerNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:name object:nil];
}

- (void)recievedNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:kZMAssetViewControllerGoBackNotification])
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if ([[notification name] isEqualToString:kZMPDFDocumentDidCloseButtonPressedNotification])
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if ([[notification name] isEqualToString:kZMSortMenuDidSelectRowNotification])
    {
        [self sortMenuDidSelectItem];
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET"]) {
        Asset *asset = [notification.userInfo objectForKey:@"asset"];
        if(([ZMUserActions sharedInstance].countOfAssetDict == 0) && [ZMUserActions sharedInstance].currentSelectedTabIndex == 1)
        {
            [self validateAndShowAsset:asset];
            [ZMUserActions sharedInstance].countOfAssetDict += 1;
        }
    }
    else if ([[notification name] isEqualToString:@"CMS_BRING_ALL_DATA"]) {
        [[ZMProntoManager sharedInstance].search reset];
    }
    else if ([[notification name] isEqualToString:@"HIDE_LIBRARY_POPOVERS"]) {
        
        [_library.headerView hidePopovers];
        [_library resetPopOvers];
    }
    else if ([[notification name] isEqualToString:@"OPEN_TEMP_ASSET"]) {
//        _library.resestFranchises = NO;
//        [_library openTempAsset:[notification.userInfo objectForKey:@"asset_link"]];
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_FOUND"]) {
        _library.resestFranchises = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            Asset *asset = [notification.userInfo objectForKey:@"asset"];
            enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
            
            if (assetType != ZMProntoFileTypesBrightcove) {
                [self.mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                // asset.isUpdateAvailable = NO;
            }
            else
            {
                // for video file
                asset.isUpdateAvailable = NO;
            }
            
            [self validateAndShowAsset:asset];
        });
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_NOT_FOUND"]) {
        _library.resestFranchises = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mainDelegate showMessage:@"Asset not found" whithType:ZMProntoMessagesTypeWarning];
        });
    }
    else if ([[notification name] isEqualToString:UIApplicationWillEnterForegroundNotification])
    {
        // updaing the AssetV2 API. Doing after 3 seconds to make sure, it doesn't affect other changes.
        __weak typeof (self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //If app is not coming to foreground from
            if (weakSelf.actions.deepLinkUserInfo == nil || weakSelf.actions.globalSearchViewController == nil)
            {
                [weakSelf updateWithCMS];
            }
        });
    }
    else if ([[notification name] isEqualToString:kPreferenceListVCDidSavePreferencesNotification])
    {
        //On changing the preferences, bydefault select all category
        [self.cCategoryMenuVC.cCategoryViewModel prepareCategoryData];
    }
}

- (void)sortMenuDidSelectItem
{
    [self.cSortMenu dismissViewControllerAnimated:YES completion:NULL];
    [self loadCAssetLibrary];
    [self.cLibraryVC reloadTableViewOrCollectionView];
}

#pragma mark - ZMContentHeader Delegate Method

- (void)conentHeader:(ZMContentHeader*)contentHeader didSelectGridButton:(LibraryViewType)viewType
{
    //NSLog(@"didSelectGridButton");
    if (viewType == LibraryViewTypeList)
    {
        self.cLibraryVC.cLibraryTableView.hidden = NO;
        self.cLibraryVC.cLibraryCollectionView.hidden = YES;
    }
    else //LibraryViewTypeGrid
    {
        self.cLibraryVC.cLibraryTableView.hidden = YES;
        self.cLibraryVC.cLibraryCollectionView.hidden = NO;
    }
    [self.cLibraryVC reloadTableViewOrCollectionView];
}

- (void)conentHeader:(ZMContentHeader*)contentHeader didSelectRecentlyButton:(UIButton*)sender
{
    //NSLog(@"buttonname:%@",sender.titleLabel.text);
    self.cSortMenuButton = sender;
    
    if (self.cSortMenu == nil)
    {
        self.cSortMenu = (ZMSortMenu *)[[[NSBundle mainBundle] loadNibNamed:@"SortMenu" owner:self options:nil] objectAtIndex:0];
        self.cSortMenu.library = _library;
        self.cSortMenu.parent  = _library.headerView;
        self.cSortMenu.sortArray = [ZMUserActions sharedInstance].sorts;
    }
    
    // 'UIPopoverController' is deprecated so using Popover presentation controller
    self.cSortMenu.modalPresentationStyle  = UIModalPresentationPopover;
    self.cSortMenu.popoverPresentationController.sourceRect = sender.frame;
    //self.sortMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomPopoverBackgroundView class];
    //present it from here
    [self presentViewController:self.cSortMenu animated:YES completion:nil];
    
    // configure the Popover presentation controller
    self.cSortMenuPopOver = [self.cSortMenu popoverPresentationController];
    self.cSortMenuPopOver.permittedArrowDirections = 0;
    self.cSortMenuPopOver.delegate = self;
    self.cSortMenuPopOver.sourceView = sender.superview;
    
    self.cSortMenu.preferredContentSize = CGSizeMake(self.cSortMenuButton.frame.size.width - 2, [self.cSortMenu.sortArray count]*44);
    self.cSortMenuPopOver.sourceRect = CGRectMake(self.cSortMenuButton.frame.origin.x + (self.cSortMenuButton.frame.size.width/2) ,self.cSortMenuButton.frame.origin.y+104,1, 1);
    
    [self.cSortMenu.sortMenuTableView reloadData];
}

- (void)resetTheSourceRectValueOfCSortMenuView
{
    self.cSortMenu.popoverPresentationController.sourceRect = self.cSortMenuButton.frame;
    self.cSortMenuPopOver.sourceRect = CGRectMake(self.cSortMenuButton.frame.origin.x + (self.cSortMenuButton.frame.size.width/2) ,self.cSortMenuButton.frame.origin.y+104,1, 1);
}

-(void)header:(ZMHeader*)headerView editingChanged:(NSString*)searchtext {
    if (searchtext.length >= 3)
    {
        self.cCategoryMenuVC.cCategoryViewModel.currentFilter.searchTextString = searchtext;
        [ZMUserActions sharedInstance].currentFilter = self.cCategoryMenuVC.cCategoryViewModel.currentFilter;
        //        [ZMUserActions sharedInstance].enableSearch = YES;
        //        self.headerViewCompetitors.searchTextbox.enabled = YES;
        //[self loadSearchedAssets];
    }
    else
    {
        self.cCategoryMenuVC.cCategoryViewModel.currentFilter.searchTextString = searchtext;
        [ZMUserActions sharedInstance].enableSearch = YES;
        self.headerViewCompetitors.searchTextbox.enabled = YES;
        _apiManager._syncingCompletion = YES;
        
        [self.headerViewCompetitors hidingIndicator];
        if (searchtext.length == 0)
        {
            [self loadCAssetLibrary];
        }
    }
}

-(void)header:(ZMHeader*)headerView textFieldDidBeginEditing:(UITextField*)textField
{
    // Load the Global search view from here
    // Need to take care of observer, becuase same observer being used by tools and training
    // If not required remove it, Or when loading global search remove the assetlibrary observer
    // And when removing the view the view, start listening to assetlibrary observer
    //NSLog(@"Text field editing started - Present Global Search");
    [self loadGlobalSearchView];
}

-(void)header:(ZMHeader*)headerView isDismissingSearchPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    [self.globalSearcVC dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
}

- (void)loadGlobalSearchView
{
    if (self.globalSearcVC == nil)
    {
        self.globalSearcVC = [GlobalSearchVC GetGSLibraryViewController];
        self.globalSearcVC.gsVCDelegate = (id<GSViewControllerDelegate>)self;
    }
    
    self.headerViewCompetitors.delegate = nil;
    self.headerViewCompetitors.delegate = (id<headerActions>)self.globalSearcVC;
    self.globalSearcVC.parentHeader = self.headerViewCompetitors;
    [ZMUserActions sharedInstance].globalSearchViewController = self.globalSearcVC;
    [ZMUserActions sharedInstance].currentFilter = self.cCategoryMenuVC.cCategoryViewModel.currentFilter;
    [[ZMAssetsLibrary defaultLibrary] removeObserver:self];
    
    //Set frame
    self.globalSearcVC.view.frame = CGRectMake(0, 63,self.view.width, self.view.height - 63);
    
    [self addChildViewController:self.globalSearcVC];
    [self.view addSubview:self.globalSearcVC.view];
    [self.globalSearcVC didMoveToParentViewController:self];
    
    //Competitors Setup
    [self removeSelfFromListeningToNotificationCenter];
    
    // We need to listen to kPreferenceListVCDidSavePreferencesNotification notification
    [self registerNotification:kPreferenceListVCDidSavePreferencesNotification];
    
    [[AppUtilities sharedUtilities] shouldMakeAllProntoTabBarButtonItemSelectable:NO];
}

#pragma empty View

- (void)showEmptyView
{
    if (!empty)
    {
        empty = [[ZMEmpty alloc] initWithParent:self.cLibraryVC.view];
    }
    [empty updateMessage];
    
    NSString *message = @"No asset available for this selection.";
    
    if([self.cCategoryMenuVC.cCategoryViewModel.selectedLefteMenu isEqualToString:ProntoTabOptionCompetitorsFavoriteAssets])
    {
        message = @"You need to add a favorite asset first in order to see content.";
    }
    else if([self.cCategoryMenuVC.cCategoryViewModel.selectedLefteMenu isEqualToString:ProntoTabOptionCompetitorsAllAssets])
    {
        message = @"No asset available for this selection.";
    }
    
    empty.subtitle.text = message;
    empty.message.text = @"";

    [self.cLibraryVC.view addSubview:empty];
    [self.cLibraryVC.view  setBackgroundColor:[UIColor yellowColor]];
    [ZMEmpty transitionWithView:empty duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
    [empty setHidden:NO];
}

- (void)displayEmpty:(BOOL)isInAFolder{
    
    if (self.emptyView == nil)
    {
        //Pronto new revamp changes: To show message when thr is no assets
        if (self.cLibraryVC.cLibraryCollectionView.hidden == YES)
        {
            self.emptyView = [[ZMEmpty alloc] initWithParent: self.cLibraryVC.cLibraryTableView];
        }
        else
        {
            self.emptyView = [[ZMEmpty alloc] initWithParent:self.cLibraryVC.cLibraryCollectionView];
        }
    }
    [self.emptyView centerMessage];
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject: [[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
    [trackingOptions setObject:@"None" forKey:@"scfilteraction"];
    [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    
    /**
     * @Tracking
     * Default Action
     **/
    //    [ZMTracking trackSection:[ZMUserActions sharedInstance].section
    //              withSubsection:@"filter"
    //                    withName:[NSString stringWithFormat:@"brand-%@-none", [ZMUserActions sharedInstance].franchiseName]
    //                 withOptions:trackingOptions];
    
    if(self.cLibraryVC.cLibraryViewModel.cLibraryAssets.count == 0)
    {
        NSString *message = nil;
        if([self.cCategoryMenuVC.cCategoryViewModel.selectedLefteMenu isEqualToString:ProntoTabOptionCompetitorsFavoriteAssets])
        {
            message = @"You need to add a favorite asset first in order to see content.";
        }
        else if([self.cCategoryMenuVC.cCategoryViewModel.selectedLefteMenu isEqualToString:ProntoTabOptionCompetitorsAllAssets])
        {
            message = @"No asset available for this selection.";
        }
        else
        {
            message = @"";
        }
        
        if ([self.headerViewCompetitors.categoryButton.titleLabel.text isEqualToString:@"Category:  None Selected"] )
        {
            message = @"You need to select a category first in order to see content.";
        }
        
        self.emptyView.subtitle.text = message;
        self.emptyView.message.text = @"";
    }
    
    [ZMEmpty transitionWithView:self.emptyView duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
    [self.emptyView setHidden:NO];
}

- (void)hideEmpty
{
    [self.emptyView removeFromSuperview];
}

#pragma mark - ZMAsset Library observer method
- (void)libraryProgressStep:(NSString *)progressText progress:(float)progress forVal:(NSString*)progressFor
{
    //Progress for search will not be considered
    if ([progressFor isEqualToString:@"search"]) { return;}
    
    //NSLog(@"Search -> libraryProgressStep");
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        if([progressText isEqualToString:@"Synchronizing User Data"])
        {
            [ZMUserActions sharedInstance].enableSearch = NO;
        }
        else if([progressText isEqualToString:@"Getting Assets 100%"])
        {
            [ZMUserActions sharedInstance].enableSearch = YES;
        }
        else
        {
            [ZMUserActions sharedInstance].enableSearch = NO;
        }
        
        UIColor *color = [theme whiteFontColor];
        self.headerViewCompetitors.searchTextbox.backgroundColor = [UIColor clearColor];
        self.headerViewCompetitors.searchTextbox.font = [theme normalLato];
        
        if(_apiManager.isConnectionAvailable && !_apiManager._syncingCompletion)
        {
            self.headerViewCompetitors.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Loading Assets..." attributes:@{NSForegroundColorAttributeName: color}];
            self.headerViewCompetitors.searchTextbox.text = @"";
            //Revamp(15/12) - Search white
            [self.headerViewCompetitors.lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
            self.headerViewCompetitors.searchTextbox.textColor = color;
            [ZMUserActions sharedInstance].enableSearch = NO;
            self.headerViewCompetitors.searchTextbox.enabled = NO;
            [self.headerViewCompetitors hidingIndicator];
        }
        else
        {
            self.headerViewCompetitors.searchTextbox.enabled = YES;
            self.headerViewCompetitors.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color}];
            self.headerViewCompetitors.searchTextbox.textColor = [UIColor whiteColor];
            [ZMUserActions sharedInstance].enableSearch = YES;
        }
        //    // Do we need this method to be called from here
        if(self.headerViewCompetitors.searchTextbox.text.length >= 3 && (_apiManager.isConnectionAvailable || [ZMUserActions sharedInstance].isKeywordSaved))
        {
            [self loadSearchedAssets];
        }
        else
        {
            [self loadCAssetLibrary];
        }
        [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingAssets];
    });
}

- (void)updateWithCMS
{
    @synchronized(self)
    {
        if (_apiManager._syncingInProgress == NO && [AOFLoginManager sharedInstance].checkForAOFLogin == YES)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [_apiManager syncWithCMS];
            });
        }
    }
}

- (void)didRefreshControlSelected
{
    [self updateWithCMS];
    //[self loadTTAssetLibrary];
}

- (void)libraryFinishSync
{
    //NSLog(@"Search -> libraryFinishSync");
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingAssets];
        
        if(self.headerViewCompetitors.searchTextbox.text.length >= 3 && (_apiManager.isConnectionAvailable || [ZMUserActions sharedInstance].isKeywordSaved))
        {
            [self loadSearchedAssets];
        }
        else
        {
            [self loadCAssetLibrary];
        }
        
        [self.cLibraryVC  endRefreshControlRefreshing];
        [ZMUserActions sharedInstance].enableSearch = YES;
        self.headerViewCompetitors.searchTextbox.enabled = YES;
        _apiManager._syncingCompletion = YES;
        
        [self.cCategoryMenuVC updateCategoryMenuTableView];
        [self.headerViewCompetitors hidingIndicator];
        self.headerViewCompetitors.searchTextbox.text = [ZMProntoManager sharedInstance].search.getTitle;
        
        [self openAssetFromDeepLinkIfRequired];

    });
}

- (void)openAssetFromDeepLinkIfRequired
{
    if (self.actions.deepLinkUserInfo != nil)
    {
        // take page number as well
        NSString *assetId       = [self.actions.deepLinkUserInfo valueForKey:@"assetString"];
        NSInteger pageNumber    = [[self.actions.deepLinkUserInfo valueForKey:@"pageNumer"] integerValue];
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId];
        if (assetArray.count > 0) // It means asset available
        {
            Asset * asset = assetArray[0];
            //PRONTO-37 Deeplinking to a Page in a PDF
            [ZMUserActions sharedInstance].pageNo = pageNumber;
            [self validateAndShowAsset:asset];
        }
        else
        {
            [[EventsServiceManager sharedServiceManager] syncEventsAndOpenAssetForAssetId:assetId userFranchise:self.headerViewCompetitors.userFranchise.text andDelegate:(id <EventOnlyAssetHandlerDelegate>)self];
        }
        self.actions.deepLinkUserInfo = nil;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}

-(void) loadSearchedAssets
{
    /* In the advent of Global Search, Search from Tabs has been removed.
     Instead of removing it  from all places. disabling the implementation of search itself*/
}


/*!
 *  Hide the keyboard when other parts of the screens is touched
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
    [headerViewCompetitors setHeaderState];
}

#pragma mark - GSViewControllerDelegate Method
- (void)didRemovedFromSuperView
{
    //NSLog(@"Global Search Removed from Superview");
    [self addSelfToListenForNotificationCenter];
    [[ZMProntoManager sharedInstance].search setTitle:@""];
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
    self.headerViewCompetitors.delegate = (id<headerActions>)self;
    [ZMUserActions sharedInstance].globalSearchViewController = nil;
    self.globalSearcVC = nil;
    [self.cLibraryVC reloadTableViewOrCollectionView];
    [self.cCategoryMenuVC updateCCategoryMenuFavoriteCell];
    [[AppUtilities sharedUtilities] shouldMakeAllProntoTabBarButtonItemSelectable:YES];
    [[EventCoreDataManager sharedInstance] resetEventSearchedResults];
    [[EventCoreDataManager sharedInstance] resetEventOnlyAssets];
    [[EventCoreDataManager sharedInstance] resetAssets];
}

// same format but caseinsensitive
BOOL isAvailIn(NSArray* pageNums, NSString* stringToCheck)
{
    for (NSString* string in pageNums) {
        if ([string caseInsensitiveCompare:stringToCheck] == NSOrderedSame)
            return YES;
    }
    return NO;
}

- (void)resetGlobalSearchFromCompetitors
{
    [self.globalSearcVC dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
}

- (void)setExportToNormal:(BOOL) dismissExport
{
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if (self.exportMenuIsPresented) {
        if(dismissExport == YES)
        {
            [[self.exportMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
            self.exportMenuIsPresented = NO;
        }
        if ([showingGridView isEqualToString:@"YES"])
        {
            ZMGridItem *tempCell = (ZMGridItem *)[self.cLibraryVC.cLibraryCollectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (tempCell && tempCell.shareButton.isSelected)
            {
                [tempCell makeShareButtonSelected:NO];
            }
        }
        else
        {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[self.cLibraryVC.cLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.shareButton.isSelected)
            {
                [cell makeShareButtonSelected:NO];
            }
        }
        
    }
}
    
#pragma mark - Event Only Asset Open
- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler openAsset:(EventOnlyAsset*)selectedAsset
    {
        if (self.eventOnlyAssetHandler == nil)
        {
            self.eventOnlyAssetHandler = [[EventOnlyAssetHandler alloc]init];
            self.eventOnlyAssetHandler.delegate      = (id <EventOnlyAssetHandlerDelegate>)self;
        }
        
        //Asset Details accepts only Asset object and it is very tighly coupled with controller.
        //So, Instead of creating new asset detail controller, we are passing equivalent object of eventOnlyAsset.
        Asset *equivalentAssetForEventOnlyAsset = [selectedAsset getEquivalentAssetObject];
        
        if(self.headerViewCompetitors.categoryPopOver)
        {
            [[self.headerViewCompetitors.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        }
        if(self.headerViewCompetitors.sortMenuPopOver)
        {
            [[self.headerViewCompetitors.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        }
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.eoa_path]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.eventOnlyAssetHandler.mainDelegate hideMessage];
        });
        
        enum ZMProntoFileTypes assetType = [selectedAsset getFileTypeOfEventOnlyAsset];
        
        self.eventOnlyAssetHandler.isDownloadingAsset = NO;
        
        /*NSString *activityType = @"Pronto Object";
        if (assetType == ZMProntoFileTypesPDF) {
            activityType = @"PDF";
        }
        else if (assetType == ZMProntoFileTypesVideo) {
            activityType = @"VIDEO";
        }
        else if (assetType == ZMProntoFileTypesBrightcove) {
            activityType = @"VIDEO";
        }
        else if (assetType == ZMProntoFileTypesDocument) {
            activityType = @"DOCUMENT";
        }
        else if (assetType == ZMProntoFileTypesEpub) {
            activityType = @"EPUB";
        }
        else if(assetType == ZMProntoFileTypesWeblink)
        {
            activityType = @"WEBLINK";
        }*/
        
        //Tracking
        //    [ASFActivityLog logActionForCurrentUser:@"viewed" withObjectName:[NSString stringWithFormat:@"%@",selectedAsset.title] ofType:[NSString stringWithFormat:@"%@",activityType] completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
        //        if (!result){
        //            //Ignore Errors
        //        }
        //    }];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UIViewController <ZMAssetViewControllerDelegate> *controller;
        
        MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
        if (assetType == ZMProntoFileTypesPDF)
        {
            if(document != nil)
            {
                [self openFastPDFForEquivalentEventOnlyAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
            }
            else
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.eventOnlyAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                    self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
                });
            }
        }
        else if (assetType == ZMProntoFileTypesBrightcove)
        {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
            ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
//            controller.competitors = self;
            controller.asset = equivalentAssetForEventOnlyAsset;
            controller.userFranchise = self.headerViewCompetitors.userFranchise.text;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (assetType == ZMProntoFileTypesDocument)
        {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
            ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
//            controller.competitors = self;
            controller.asset = equivalentAssetForEventOnlyAsset;
            controller.userFranchise = self.headerViewCompetitors.userFranchise.text;
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        else if(assetType == ZMProntoFileTypesWeblink)
        {
            // open the weblink URL
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
            ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
//            controller.competitors = self;
            controller.asset = equivalentAssetForEventOnlyAsset;
            controller.userFranchise = self.headerViewCompetitors.userFranchise.text;
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        else
        {
            if (assetType == ZMProntoFileTypesPDF)
            {
                if(selectedAsset.eoa_path)
                {
                    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
                    if(document != nil)
                    {
                        [self openFastPDFForEquivalentEventOnlyAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
                    }
                    else
                    {
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [self.eventOnlyAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                            self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
                        });
                    }
                }
            }
        }
        
    }
    
- (void)openFastPDFForEquivalentEventOnlyAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
//    controller.competitors = self;
    controller.asset = asset;
    
    ((FastPDFViewController*)controller).isEventOnlyAsset = YES;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    //    [view addSubview:controller.view];
    
    [viewController.view addSubview:view];
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    //    controller.view.frame = view.bounds;
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
    
    //    [controller didMoveToParentViewController:viewController];
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor GetColorFromHexValueE0E6ED];
    [viewController.view addSubview:webview];
    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    if (self.eventOnlyAssetHandler != nil)
    {
        [self.eventOnlyAssetHandler hideMessage];
    }
    
    controller.userFranchise = headerViewCompetitors.userFranchise.text;
    controller.viewMain = view;
    controller.webviewScreen = webview;
    controller.webviewScreen.hidden = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

@end
