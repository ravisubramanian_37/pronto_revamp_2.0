//
//  CLibraryViewModel.h
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CLibraryViewModelDelegate <NSObject>
@end

@interface CLibraryViewModel : NSObject

@property (nonatomic, strong) NSMutableArray *cLibraryAssets;
@property(nonatomic, assign) id<CLibraryViewModelDelegate> cLibraryVMDelegate;

- (instancetype)initWithDelegate:(id<CLibraryViewModelDelegate>)delegate;
@end
