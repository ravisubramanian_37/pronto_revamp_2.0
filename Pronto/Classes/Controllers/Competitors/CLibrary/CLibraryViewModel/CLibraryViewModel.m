//
//  CLibraryViewModel.m
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "CLibraryViewModel.h"

@implementation CLibraryViewModel

- (instancetype)initWithDelegate:(id<CLibraryViewModelDelegate>)delegate
{
    if (self = [super init])
    {
        self.cLibraryVMDelegate    = delegate;
        self.cLibraryAssets        = [NSMutableArray array];
    }
    return self;
}


@end
