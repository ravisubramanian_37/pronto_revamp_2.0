//
//  CLibraryViewController.h
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pronto-Swift.h"

@class CLibraryViewModel;
@class CLibraryViewController;
@class Asset;
@class FranchiseTableViewCell;

@protocol CLibraryViewControllerDelegate<NSObject>

- (void)cLibraryViewController:(CLibraryViewController*)cLibraryViewController openAsset:(Asset*)asset fromListOrGridAtIndex:(NSInteger)index ofType:(NSString*)type;
- (void)cLibraryViewController:(CLibraryViewController*)cLibraryViewController cell:(id)cell  configureExportPopOver:(UIButton*) sender;
- (void)cLibraryViewController:(CLibraryViewController*)cLibraryViewController cell:(id)cell  configureInfoPopOver:(UIButton*) sender;
- (void)cLibraryViewController:(CLibraryViewController*)cLibraryViewController cell:(id)cell  configureFavButton:(UIButton*) sender;
- (void)didRefreshControlSelected;

@end

@interface CLibraryViewController : UIViewController
@property (nonatomic, strong) CLibraryViewModel *cLibraryViewModel;
@property (nonatomic, assign) id<CLibraryViewControllerDelegate>cLibraryVCDelegate;

@property (weak, nonatomic) IBOutlet UICollectionView *cLibraryCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *cLibraryTableView;

+ (CLibraryViewController*)GetCLibraryViewController;
- (void) reloadTableViewOrCollectionView;
- (void) endRefreshControlRefreshing;
- (void) changeTintColor;

@property (nonatomic, strong) AudioPlayer *audioPlayerView;
@property (nonatomic, strong) Asset *assetForAudio;

@end
