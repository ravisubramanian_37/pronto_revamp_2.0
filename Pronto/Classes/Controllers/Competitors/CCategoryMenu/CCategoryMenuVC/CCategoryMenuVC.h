//
//  CCategoryMenuVC.h
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CCategoryMenuViewModel;

@protocol CCategoryMenuVCDelegate<NSObject>
- (void)didUpdatedDataSource;
- (void)stopPlayingAudio;
@end

@interface CCategoryMenuVC : UIViewController
@property(nonatomic, strong) CCategoryMenuViewModel *cCategoryViewModel;
@property (nonatomic, assign) id<CCategoryMenuVCDelegate> cCategoryMenuVCDelegate;
@property (weak, nonatomic) IBOutlet UITableView *cCategoryMenuTableView;

+ (CCategoryMenuVC*)GetCCategoryMenuViewController;
- (void)updateCategoryMenuTableView;
- (void)reloadTableView;
- (void)updateCCategoryMenuFavoriteCell;
- (void)showBouncingEffectOnFavouriteCountLabel;
@end
