//
//  CCategoryMenuVC.m
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "CCategoryMenuVC.h"
#import "CCategoryMenuViewModel.h"
#import "CCategoryMenuCell.h"
#import "ZMUserActions.h"
#import "Pronto-Swift.h"
#import "Constant.h"

typedef NS_ENUM(NSInteger, UserSelection)
{
    AllCompetitorsAssets = 0,
    CompetitorFavoriteAssets,
    Other
};

static NSString * const CCategoryMenuStoryboardName = @"CCategoryMenu";
static NSString * const CCategoryMenuControllerIdentifier = @"CCategoryMenuVC";

@interface CCategoryMenuVC ()<CCategoryMenuViewModelDelegate>


@end

@implementation CCategoryMenuVC

+ (CCategoryMenuVC*)GetCCategoryMenuViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:CCategoryMenuStoryboardName bundle:nil];
    CCategoryMenuVC *cCategoryMenuVC = [sb instantiateViewControllerWithIdentifier:CCategoryMenuControllerIdentifier];
    return cCategoryMenuVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeData];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.cCategoryViewModel.competitorsCategoryArray == nil || self.cCategoryViewModel.competitorsCategoryArray.count <= 0)
    {
        [self.cCategoryViewModel prepareCategoryData];
    }
}

- (void)setupUI
{
    [self registerTableViewCell];
    self.cCategoryMenuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)initializeData
{
    self.cCategoryViewModel = [[CCategoryMenuViewModel alloc]initWithDelegate:(id<CCategoryMenuViewModelDelegate>)self];
}

- (void)registerTableViewCell
{
    [self.cCategoryMenuTableView registerNib:[UINib nibWithNibName:[CCategoryMenuCell GetCCategoryMenuCellNibName] bundle:nil]
                       forCellReuseIdentifier:[CCategoryMenuCell GetCCategoryMenuCellIdentifier]];
}

- (void)reloadTableView {
    [self.cCategoryMenuTableView reloadData];
}

# pragma mark -  TableView Data Source And Delegate Methods
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CCategoryMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:[CCategoryMenuCell GetCCategoryMenuCellIdentifier]];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:[CCategoryMenuCell GetCCategoryMenuCellNibName] owner:self options:nil];
        [tableView registerNib:[UINib nibWithNibName:[CCategoryMenuCell GetCCategoryMenuCellNibName] bundle:nil]
        forCellReuseIdentifier:[CCategoryMenuCell GetCCategoryMenuCellIdentifier]];
        cell = (CCategoryMenuCell*)[nib objectAtIndex:0];
    }
    
    // Customize cell // check for less than one
    if (self.cCategoryViewModel.competitorsCategoryArray != nil &&
        self.cCategoryViewModel.competitorsCategoryArray.count >= indexPath.row)
    {
        NSDictionary *category = nil;
        category = self.cCategoryViewModel.competitorsCategoryArray[indexPath.row];
        [cell prepareCellWithCategory:category atIndexPath:indexPath];
    }
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.cCategoryViewModel.competitorsCategoryArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat heightValue = [CCategoryMenuCell DefaultHeightOfCell];
    if(indexPath.row == 1)
    {
        heightValue = heightValue + [CCategoryMenuCell HeightOfCellTitle];
    }
    return heightValue;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CCategoryMenuCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == AllCompetitorsAssets)
    {
        [AbbvieLogging logInfo:@"All Assets selected"];
        [ZMUserActions sharedInstance].selectLeftMenu = ProntoTabOptionCompetitorsAllAssets;
        // All Asset cell can't be deselected unless select other cells.
        [self.cCategoryViewModel prepareCategoryDataWhenAllAssetSelected];
    }
    else if (indexPath.row == CompetitorFavoriteAssets)
    {
        [AbbvieLogging logInfo:@"Favorite Assets selected"];
        [ZMUserActions sharedInstance].selectLeftMenu = ProntoTabOptionCompetitorsFavoriteAssets;
        // If favorite gets selected all other cells will be deselected
        [self.cCategoryViewModel prepareFavoriteCategoryDataFor:cell.currentCategory];
    }
    else
    {
        [AbbvieLogging logInfo:@"Other Category selected"];
        //Other than all asset and favorite assets cell, Multiple selection allowed.
        [self.cCategoryViewModel prepareCategoryDataForCategory:cell.currentCategory];
    }
    /*  Instead of reloading single cell, loading table view
     It will update the previous cell as well as per data. Not a heavy task */
    
    [self.cCategoryMenuTableView reloadData];
    
}

- (void)updateCategoryMenuTableView
{
    [self.cCategoryMenuTableView reloadData];
}

# pragma mark - CCategoryMenuViewModelDelegate Methods
- (void)dataSourceUpdated
{
    [AbbvieLogging logInfo:@"Data source got updated"];
    // Notifying and Competitor Controller to update the Competitor Library View.
    [self.cCategoryMenuTableView reloadData];
    if (self.cCategoryMenuVCDelegate != nil &&
        [self.cCategoryMenuVCDelegate respondsToSelector:@selector(didUpdatedDataSource)])
    {
        [self.cCategoryMenuVCDelegate didUpdatedDataSource];
    }
    
    if (self.cCategoryMenuVCDelegate != nil &&
        [self.cCategoryMenuVCDelegate respondsToSelector:@selector(stopPlayingAudio)])
    {
        [self.cCategoryMenuVCDelegate stopPlayingAudio];
    }
}

- (void)updateCCategoryMenuFavoriteCell
{
    //2nd Row is Favorite always
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.cCategoryMenuTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)showBouncingEffectOnFavouriteCountLabel
{
    //2nd Row is Favorite always
    NSIndexPath *favouriteIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    CCategoryMenuCell *cell = [self.cCategoryMenuTableView cellForRowAtIndexPath:favouriteIndexPath];
    [cell showBouncingEffectOnFavouriteCountLabel];
}


@end
