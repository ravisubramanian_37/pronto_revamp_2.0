//
//  CCategoryMenuCell.h
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCategoryMenuCell : UITableViewCell

@property(nonatomic, strong)NSDictionary *currentCategory;

+ (NSString*)GetCCategoryMenuCellIdentifier;
+ (NSString*)GetCCategoryMenuCellNibName;
+ (CGFloat)DefaultHeightOfCell;
+ (CGFloat)HeightOfCellTitle;

- (void)prepareCellWithCategory:(NSDictionary*)category atIndexPath:(NSIndexPath*)indexPath;
- (void)selectDeselectCategoriesImageViewWithCategory:(NSDictionary*)category;
- (void)showBouncingEffectOnFavouriteCountLabel;
@end
