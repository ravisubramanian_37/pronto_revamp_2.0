//
//  CCategoryMenuCell.m
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "CCategoryMenuCell.h"
#import "ZMProntoTheme.h"
#import "ViewUtils.h"
#import "Pronto-Swift.h"
#import "ZMUserActions.h"
#import "UIImage+ImageDirectory.h"
#import "Constant.h"

@interface CCategoryMenuCell()

@property (weak, nonatomic) IBOutlet DividerView *bottomDividerView;
@property (weak, nonatomic) IBOutlet UIImageView *checkedImage;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *cntLabel;

@end

@implementation CCategoryMenuCell

+ (NSString*)GetCCategoryMenuCellIdentifier
{
    return @"CCategoryMenuCell";
}

+ (NSString*)GetCCategoryMenuCellNibName
{
    return @"CCategoryMenuCell";
}

+ (CGFloat)DefaultHeightOfCell
{
    return 43.5;
}

+ (CGFloat)HeightOfCellTitle
{
    return 15.0;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
    self.currentCategory = nil;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.currentCategory = nil;
    //Re-initialize the value again here.
}

- (void)setupUI
{
    self.mainLabel.font         = [[ZMProntoTheme sharedTheme] boldLato];
    self.mainLabel.textColor    = [UIColor redColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}
- (void)prepareCellWithCategory:(NSDictionary*)category atIndexPath:(NSIndexPath*)indexPath
{
    self.currentCategory = category;
    
    // Need to refactor
    self.mainLabel.textColor    = [UIColor colorWithRed:0.51 green:0.50 blue:0.49 alpha:1.0];
    self.mainLabel.font         = [[ZMProntoTheme sharedTheme] normalLato];
    
    //Divide in methods
    if([[ZMUserActions sharedInstance].categoryCompetitors count] > 1 && [[ZMUserActions sharedInstance].categoryCompetitors count] > indexPath.row)
    {
        if(indexPath.row <= [[ZMUserActions sharedInstance].categoryCompetitors count])
        {
            switch (indexPath.row) {
                case 1:
                {
                    
                    //Set the frame accordingly
                    self.cntLabel.backgroundColor   = [UIColor colorWithRed:0.00 green:0.51 blue:0.73 alpha:1.0];
                    self.cntLabel.textColor         = [UIColor whiteColor];
                    self.cntLabel.textAlignment     = NSTextAlignmentCenter;
                    self.cntLabel.font              =[UIFont fontWithName:@"Lato-Regular" size:12.0f];
                    self.cntLabel.layer.masksToBounds= YES;
                    self.cntLabel.layer.cornerRadius = 11.0;
                    self.cntLabel.hidden            = YES;
                    NSInteger favoriteCount         = [[ZMCDManager sharedInstance] getFavFranchise].count;
                    
                    if(favoriteCount > 0)
                    {
                        self.cntLabel.hidden = NO;
                        self.cntLabel.text= [ NSString stringWithFormat:@"%lu",(long)favoriteCount];
                    }
                    else
                    {
                        self.cntLabel.hidden = YES;
                    }
                    self.mainLabel.text = ProntoTabOptionCompetitorsFavoriteAssets;
                    self.mainLabel.font = [[ZMProntoTheme sharedTheme] boldLato];
                }
                    break;
                default:
                {
                    self.mainLabel.text = [category objectForKey:@"name"];
                    if(indexPath.row == 0 && [self.mainLabel.text isEqualToString:ProntoTabOptionCompetitorsAllAssets])
                    {
                        self.mainLabel.font = [[ZMProntoTheme sharedTheme] boldLato];
                    }
                    self.cntLabel.hidden = YES;
                }
                    break;
            }
        }
    }
    else
    {
        self.mainLabel.font = [[ZMProntoTheme sharedTheme] boldLato];
        if(indexPath.row == 0)
        {
            self.mainLabel.text = ProntoTabOptionCompetitorsAllAssets;
        }
        else if(indexPath.row == 1)
        {
            self.mainLabel.text = ProntoTabOptionCompetitorsFavoriteAssets;
        }
    }
    
    [self selectDeselectCategoriesImageViewWithCategory:category];
}

- (void)selectDeselectCategoriesImageViewWithCategory:(NSDictionary*)category
{
    self.currentCategory = category;
    
    if ([[category objectForKey:@"selected"] integerValue] == 1)
    {
        self.checkedImage.hidden = NO;
        [[ZMProntoTheme sharedTheme] tintArrow:self.checkedImage];
        self.checkedImage.image = [UIImage GetSelectedCheckListImage];
    }
    else
    {
        self.checkedImage.hidden = NO;
        self.checkedImage.image = [UIImage GetCheckListImage];
        [[ZMProntoTheme sharedTheme] tintArrow:self.checkedImage];
    }
}

- (void)showBouncingEffectOnFavouriteCountLabel
{
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         // moves label up 100 units in the y axis
                         self.cntLabel.transform = CGAffineTransformMakeTranslation(0, -10);
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.2
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              // move label back down to its original position
                                              self.cntLabel.transform = CGAffineTransformMakeTranslation(0,0);
                                          }
                                          completion:nil];
                     }];
    
}

@end
