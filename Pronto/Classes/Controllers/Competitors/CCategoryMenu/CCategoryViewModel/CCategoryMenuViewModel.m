//
//  CCategoryMenuViewModel.m
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "CCategoryMenuViewModel.h"
#import "Pronto-Swift.h"
#import "Categories.h"
#import <MagicalRecord/MagicalRecord.h>
#import "ZMUserActions.h"
#import "Filter.h"
#import "Constant.h"
#import "FavFranchiseAsset.h"

@implementation CCategoryMenuViewModel

- (instancetype)initWithDelegate:(id<CCategoryMenuViewModelDelegate>)delegate {
    if (self = [super init])
    {
        self.cCategorryVMDelegate      = delegate;
        self.competitorsCategoryArray  = [NSMutableArray array];
        self.currentFilter             = [[Filter alloc]init];
    }
    return self;
}

- (void)prepareCategoryData
{
   [[ZMCDManager sharedInstance]getCategories:^(NSArray * data) {

        [[ZMCDManager sharedInstance] getActiveCategoriesWithCategories:data
                                                  withCompletionHandler:^(NSArray *categoriesToFilter) {

             self.competitorsCategoryArray = [NSMutableArray arrayWithArray:categoriesToFilter];
             [ZMUserActions sharedInstance].categoryCompetitors = (NSMutableArray*)self.competitorsCategoryArray;
             [self prepareCategoryDataWhenAllAssetSelected];
             [AbbvieLogging logInfo:[NSString stringWithFormat:@"catDict:%@",[ZMUserActions sharedInstance].categoryCompetitors]];
         }];
    }];
}


- (void)prepareSelectedCategoryData {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *dict in self.competitorsCategoryArray)
    {
        if ([dict[@"selected"] isEqualToString:@"1"])
        {
            [tempArray addObject:dict[@"id"]];
        }
    }
    
    self.currentFilter.selectedCategories = tempArray;
}

- (void)prepareCategoryDataWhenAllAssetSelected
{
    NSString *allAssetTagName = [Constant GetAllAssetTagName];
    NSMutableArray *tempArray = [NSMutableArray array];
    NSMutableArray *tempIdsArray = [NSMutableArray array];
    self.selectedLefteMenu = ProntoTabOptionCompetitorsAllAssets;
    for (NSDictionary *dict in self.competitorsCategoryArray)
    {
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
        
        if ([dict[@"name"] isEqualToString:allAssetTagName])
        {
            tempDict[@"selected"] = @"1";
            [tempIdsArray addObject:dict[@"id"]];
        }
        else
        {
            tempDict[@"selected"] = @"0";
        }
        
        [tempArray addObject:tempDict];
    }
    
    self.competitorsCategoryArray         = tempArray;
    self.currentFilter.selectedCategories   = @[];
    // If all asset has been selected then favorite id will be empty
    self.currentFilter.favoriteIdsArray = @[];
    if (self.cCategorryVMDelegate != nil && [self.cCategorryVMDelegate respondsToSelector:@selector(dataSourceUpdated)])
    {
        [self.cCategorryVMDelegate dataSourceUpdated];
    }
}
- (void)prepareFavoriteCategoryDataFor:(NSDictionary*)category {
    if ([category[@"selected"] isEqualToString:@"1"]) //It meanse Favorite is already selected
    {
        //Then select all assets
        [self prepareCategoryDataWhenAllAssetSelected];
    }
    else
    {
        self.selectedLefteMenu = ProntoTabOptionCompetitorsFavoriteAssets;
        //Adding Categories
        NSMutableArray *catArray = [NSMutableArray array];
        NSString *allAssetTagName = [Constant GetAllAssetTagName];
        NSString *favoriteAssetTagName  = [Constant GetFavoritesAssetTagName];
        for (NSDictionary *dict in self.competitorsCategoryArray)
        {
            if ([dict[@"selected"] isEqualToString:allAssetTagName])
            {
                [catArray addObject:dict[@"id"]];
            }
            if ([dict[@"name"] isEqualToString:favoriteAssetTagName])
            {
                [catArray addObject:dict[@"id"]];
            }
        }
        self.currentFilter.selectedCategories = catArray;
        //Adding Favourite ids
//        NSMutableArray *favArray = [[NSMutableArray alloc]init];
//        for(FavFranchiseAsset *fav in favfranchise) {
//            [favArray addObject:fav.asset_ID];
//        }
        NSString *currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
        NSMutableArray *favArray = [[NSMutableArray alloc]init];

        NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchise];
        for(FavFranchise *fav in favfranchise)
        {
            BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:fav.bucketName];
            if (isPresent  && fav.asset_ID != nil)
            {
                [favArray addObject:fav.asset_ID];
            }
        }
        self.currentFilter.favoriteIdsArray = favArray;
        //For Selection
        NSMutableArray *tempArray       = [NSMutableArray array];
        for (NSDictionary *dict in self.competitorsCategoryArray)
        {
            NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
            if ([dict[@"name"] isEqualToString:favoriteAssetTagName])
            {
                tempDict[@"selected"] = @"1";
            }
            else
            {
                tempDict[@"selected"] = @"0";
            }
            [tempArray addObject:tempDict];
        }
        
        self.competitorsCategoryArray         = tempArray;
        // If Favorite asset has been selected then selectedCategories id will be empty
        //        self.currentFilter.selectedCategories   = @[];
        //        self.currentFilter.favoriteIdsArray = @[]; //Get all favorite id's from MCDManager
        if (self.cCategorryVMDelegate != nil && [self.cCategorryVMDelegate respondsToSelector:@selector(dataSourceUpdated)])
        {
            [self.cCategorryVMDelegate dataSourceUpdated];
        }
    }
}
- (void)prepareCategoryDataForCategory:(NSDictionary*)category {
    NSString *favoriteAssetTagName  = [Constant GetFavoritesAssetTagName];
    NSString *allAssetTagName       = [Constant GetAllAssetTagName];
    
    NSMutableArray *tempArray       = [NSMutableArray array];
    for (NSDictionary *dict in self.competitorsCategoryArray)
    {
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
        
        if ([dict[@"name"] isEqualToString:category[@"name"]])
        {
            if ([category[@"selected"] isEqualToString:@"1"])
            {
                tempDict[@"selected"] = @"0";
            }
            else
            {
                tempDict[@"selected"] = @"1";
            }
        }
        else if (([dict[@"name"] isEqualToString: allAssetTagName] ||
                  [dict[@"name"] isEqualToString: favoriteAssetTagName]))
        {
            // Restting the value of All assets and favorite assets dictionary value
            tempDict[@"selected"] = @"0";
        }
        
        [tempArray addObject:tempDict];
    }
    
    self.competitorsCategoryArray = tempArray;
    
    //Getting selected category name
    NSMutableArray *tempIdsArray    = [NSMutableArray array];
    for (NSDictionary *dict in self.competitorsCategoryArray)
    {
        /* Getting id's of selected category which would be other than
         All Assets and favorite asset */
        
        if (([dict[@"name"] isEqualToString: allAssetTagName] &&
             [dict[@"name"] isEqualToString: favoriteAssetTagName]) == NO)
        {
            if ([dict[@"selected"] isEqualToString:@"1"])
            {
                [tempIdsArray addObject:dict[@"id"]];
            }
        }
    }
    
    if (tempIdsArray.count > 0) // It means there is catogory selection
    {
        self.currentFilter.selectedCategories = tempIdsArray;
        
        if (self.cCategorryVMDelegate != nil && [self.cCategorryVMDelegate respondsToSelector:@selector(dataSourceUpdated)])
        {
            [self.cCategorryVMDelegate dataSourceUpdated];
        }
    }
    else
    {
        //selects the All categories.
        [self prepareCategoryDataWhenAllAssetSelected];
    }
}

@end
