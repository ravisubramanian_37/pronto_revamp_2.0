//
//  CCategoryMenuViewModel.h
//  Pronto
//
//  Created by cccuser on 27/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Filter;

@protocol CCategoryMenuViewModelDelegate <NSObject>
- (void)dataSourceUpdated;
@end

@interface CCategoryMenuViewModel : NSObject

@property (nonatomic, strong) NSArray *competitorsCategoryArray;
@property (nonatomic, strong) Filter *currentFilter;
@property (nonatomic, strong) NSString *selectedLefteMenu;

@property(nonatomic, assign) id<CCategoryMenuViewModelDelegate> cCategorryVMDelegate;

- (instancetype)initWithDelegate:(id<CCategoryMenuViewModelDelegate>)delegate;
- (void)prepareCategoryData;
- (void)prepareCategoryDataWhenAllAssetSelected;
- (void)prepareFavoriteCategoryDataFor:(NSDictionary*)category;
- (void)prepareCategoryDataForCategory:(NSDictionary*)category;

@end
