
//
//  ZMLibrary.m
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//
#import "ZMLibrary.h"
#import "ZMLogin.h"
#import "ZMProntoTheme.h"
#import "AppDelegate.h"
#import "ZMAssetsLibrary.h"
//Revamp(1/12) - Import FavFranchise
#import "FavFranchiseAsset.h"
//End of Revamp
#import "ZMCategory.h"
#import "ZMGridItem.h"
#import "ZMNotifications.h"
#import "ZMEmpty.h"
//Revamp(1/12) - Import magical Record
#import <MagicalRecord/MagicalRecord.h>
//End of Revamp
#import "Pronto-Swift.h"
#import "AssetDuration.h"
#import "Categories.h"
#import "Franchise.h"
#import "Brand.h"
#import "Asset.h"
#import "FeedbackView.h"
#import "FranchiseTableViewCell.h"
#import "ZMProntoPhase3.h"
#import "ProntoTabbarViewController.h"

#import "Constant.h"
#import "PreferencesListViewController.h"
#import "Preferences+CoreDataClass.h"
#import "Preferences+CoreDataProperties.h"
#import "AssetDownloadProvider.h"
#import "HelpViewController.h"
#import "GlobalSearchVC.h"
#import "ProntoUserDefaults.h"
#import "PreferencesVCViewModel.h"
#import "AppUtilities.h"
#import "NSMutableDictionary+Utilities.h"
#import "EventsViewController.h"
#import "EventCoreDataManager.h"
#import "ForceLogoutRefreshView.h"

#import "EventOnlyAssetHandler.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "EventAssetDownloadProvider.h"
#import "EventsServiceManager.h"

#import "UITableView+Utility.h"
#import "UICollectionView+Utility.h"

#import "ViewUtils.h"

#import "FastPDFViewController.h"
#import <FastPdfKit/FastPdfKit.h>

#import "UIColor+ColorDirectory.h"

//Revamp(7/12) - Definition for constants
const CGFloat kYPosition = 63;
CGFloat gridWidth = 232.0;
CGFloat interItemSpace = 0;
//End of Revamp

@interface ZMLibrary () {
    
//}<ZMBroseMenuDelegate>{
    __weak IBOutlet UIView *header;
    __weak IBOutlet UICollectionView *grid;
    __weak IBOutlet UIScrollView *libraryScrollView;
    __weak ZMGridItem* currentCell;
    __strong NSArray* visibleRows;
   // __strong NSMutableArray* promotedContentArray;
     __weak IBOutlet UIView *scrollSpinner;
    id <ZMProntoTheme> theme;
    ZMNotifications *notificationsPannel;
    ZMWalkthrough *walkthrough;
    ZMLaunchLoading *launchLoading;
    LoadingIndicatorNew *loadingIndicatorNew;
    FeedbackView *feedback;
    ZMEmpty *empty;
    //ZMBrowse *browse;
    Asset *currentAssetDownload;
    AppDelegate *mainDelegate;
    NSOperationQueue *myQueue;
    //For dynamic height of list view
    int heightToIncrease;
    long mostPopularAsset;
    BOOL libraryInitialized;
    BOOL firstTime;
    BOOL assetsAlreadyLoaded;
    BOOL needUserChecked;
    BOOL newUserLogged;
    BOOL showScrollSpinnerHidden;
    BOOL cellWithAction;
    BOOL promotedNoneMessageDisplayed;
    BOOL isFetchEventsCompleted;
    BOOL isSavePreferencesCompleted;
    //Revamp(14/12) bool to control bounce
    BOOL favIconTapped;
    UIRefreshControl *refreshControl;
    IBOutlet UILabel *loadingLabel;
    IBOutlet UIImageView *loadingIcon;
//    ZMBrowseMenu * browseMenu;
    UITapGestureRecognizer *touchOnView;
    UIView *backgroundView;
    //Revamp(1/12) - Object for Abbvie Api
    ZMAbbvieAPI *abbvieApi;
    //End of Revamp
    __weak IBOutlet UIView *contentHeader;
    __weak IBOutlet UIView *promotedContainerView;
    __weak IBOutlet UICollectionView *promotedGrid;
    __weak IBOutlet UIView *tableBackgroundView;
    
    __weak IBOutlet UIPageControl *promotedPageControl;
    __weak IBOutlet UITableView *listTableView;
    //Revamp(14/12) - Outlet for tile button in custom header
    __weak IBOutlet UIButton *listGridbutton;
    
    __weak IBOutlet UIActivityIndicatorView *promotedActivityIndicator;
    NSInteger selectedItem;
    NSInteger previousAssetCount;
    //Pronto new revamp changes: To show and hide popovers during device rotation
    NSMutableDictionary* promotedContentAssets;
    BOOL exportPopOverTappedonListView;
    BOOL exportPopOverTappedonPromotedGrid;
    BOOL exportPopOverTappedonGrid;
    
    BOOL infoPopOverTappedonListView;
    BOOL infoPopOverTappedonPromotedGrid;
    BOOL infoPopOverTappedonGrid;
    
    //to check if offline download is started
    BOOL offlineDownloadStarted;
    BOOL syncWithCMSActive;
    long presentCount;
    long totalPresentCount;
    
    //For Audio Player
    AudioPlayer *audioPlayerView;
    Asset *assetForAudio;
    FranchiseTableViewCell *audioCurrentCell;
    ZMGridItem *audioCurrentGridCell;
    ZMGridItem *audioCurrentPromotedGridCell;
    BOOL promotedTappedForAudio;
    
}
//@property (nonatomic,retain) ZMBrowseMenu * browseMenu;
@property (nonatomic,retain) UIRefreshControl *refreshControl;
//DB for Offline download coredata
@property(nonatomic, strong) NSArray * initialDownloadDbArray;
@property(nonatomic, strong) NSArray * favFranchiseArray;
@property(nonatomic, strong) NSMutableDictionary* promotedContentAssets;
@property(nonatomic, strong) NSMutableArray* promotedContentArray;
@property (nonatomic, assign) PreferencesListViewController *preferencesListViewController;
@property (nonatomic, assign) BOOL  isHomeOfficeFranchiseSelected;
@property (nonatomic, assign) BOOL  isShowingPreferencesView;

// if switchtotab bar is true and user is coming from deeplink of any 3 options.
@property (nonatomic, assign) ProntoTabBarItem switchToTabBarItem; // it can have value ranging from 0 to 3. 0 - franchise and 3 events

//@property (nonatomic, assign) BOOL isShowingIndicatorViewWithOverlay;
- (void) showScrollSpinner;

// Global Search View
@property (nonatomic, strong) GlobalSearchVC *globalSearcVC;
@property (nonatomic, strong) Asset *currentSelectedAssetToSeeDetail;
    
//Event Only Asset Handler
@property (nonatomic, strong) EventOnlyAssetHandler *eventOnlyAssetHandler;
@property (nonatomic, strong) ForceLogoutRefreshView *forceLogoutRefreshView;

@property (nonatomic, assign) BOOL isRefreshButtonTapped;
@property (nonatomic, assign) BOOL isPreferncesOrFranchiseBrandsNetworkRequestInProgress;

@property (nonatomic, strong) UIView* statusBar;
//@property (nonatomic, assign) BOOL multipleFranchisePromotedMessageDisplayed;

@property (nonatomic, strong) UIView *startUpView;
@end


static BOOL multipleFranchisePromotedMessageDisplayed;
@implementation ZMLibrary
@synthesize headerView,browseMenu,refreshControl,loadingLabel,franchiseCount,sortButton,promotedContentAssets,promotedContentArray;

# pragma mark - viewController


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInitialization];
    }
    return self;
}


- (void) customInitialization
{
    mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    myQueue = [[NSOperationQueue alloc] init];
//    browseMenu = [[ZMBrowseMenu alloc]init];
    _isBrowseMenuActive = NO;
    _libraryBackToFolderDetail = NO;
    _resestFranchises = YES;
    [ZMProntoManager sharedInstance].currentLibraryView = MainViewLibrary;
}

//Revamp(13/12) - funtion to be executed when notifications is tapped
- (void)showHideArrow:(BOOL) hide {
    if (!hide) {
        headerView.arrow.hidden = NO;
        [headerView.messagesButton setImage:[UIImage imageNamed:@"messageSelectIcon"] forState:UIControlStateNormal];
        [headerView showHideBadge:YES];
    }
    else {
        headerView.arrow.hidden = YES;
        [headerView.messagesButton setImage:[UIImage imageNamed:@"messagesIcon"] forState:UIControlStateNormal];
        [headerView showHideBadge:NO];
    }
}
//End of Revamp

- (void) configureListView {
    
    [self styleLibrary];
    [listTableView setDelegate: self];
    [listTableView setDataSource: self];
    listTableView.hidden = false;
    grid.hidden = true;
    listTableView.backgroundColor = [UIColor clearColor];
    listTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // adding the refresh control
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshAssets:) forControlEvents:UIControlEventValueChanged];
    [listTableView addSubview:refreshControl];
    
    /** If you want to have full selection list turn showResumeSelection to NO */
    headerView.showResumeSelection = YES;
    headerView.library = self;
}

//Instead of removing scroll spinner showing/hidding logic.
// Making it invisible to user and doing so will not cause any issue.
- (void)makingInvisibleScrollSpinnerToUser
{
    loadingLabel.textColor = [UIColor clearColor];
    loadingLabel.backgroundColor = [UIColor clearColor];
    scrollSpinner.backgroundColor = [UIColor clearColor];
    loadingIcon.image = nil;
    loadingIcon.backgroundColor = [UIColor clearColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self makingInvisibleScrollSpinnerToUser];
    [self hideScrollSpinner];
    [self configureAppereance];
    [self configurePromotedContentGrid];
    
    //Initialisations for Offline Download
    _initialDownloadDbArray = [[NSArray alloc]init];
    _favFranchiseArray = [[NSArray alloc]init];
    presentCount = 0;
    totalPresentCount = 0;

    self.franchiseList = [NSMutableArray array];
    self.franchiseList = [NSMutableArray arrayWithArray:[ZMUserActions sharedInstance].franchisesAndBrands];

    
    // init table view
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    _leftMenuTableView.delegate = self;
    _leftMenuTableView.dataSource = self;
    //_leftMenuTableView.backgroundColor = [UIColor whiteColor];
    _leftMenuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Off_Start"];
    
    //Set Global variables
    _isDownloadignAsset = NO;
    _itemsPerPage = 60;
    _pageIndex = 0;
    _actions = [ZMUserActions sharedInstance];
    _actions.section = @"library";
    _actions.sorts = [self getSortArray];
    _section = ProntoSectionTypeLibrary;
    
    [[ZMProntoManager sharedInstance].search reset];
    _apiManager = [ZMAbbvieAPI sharedAPIManager];
    
    //When the app runs the first time...
    firstTime = YES;
    favIconTapped = NO;
        
    /*!
     *@StartLogin
     */
    NSString* accessToken = [[ProntoUserDefaults userDefaults] getAOFAccessToken];
    if(accessToken.length <= 0 || accessToken == nil)
    {
        //Go to the login view if there is no a valid session
        [self displayLogin];
    }
    
    /**
     *  Notification system to handle lobrary syncronization and other tasks ***
     */
    [self registerNotification:@"CMS_BRING_ALL_DATA"];
    [self registerNotification:@"HIDE_LIBRARY_POPOVERS"];
    [self registerNotification:@"OPEN_TEMP_ASSET"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET_APP_CLOSED"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET_FOUND"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET_NOT_FOUND"];
    [self registerNotification:kPreferenceListVCWillShowNotification];
    [self registerNotification:kPreferenceListVCDidSavePreferencesNotification];
    [self registerNotification:kAOFLogoutSuccessNotification];
    //make it as preferences view controller
    [self registerNotification:kPreferenceListVCDismissNotification];
    [self registerNotification:kSignOutOnLaunchNotification];
    [self registerNotification:UIApplicationDidEnterBackgroundNotification];
    
    ZMProntoManager.sharedInstance.favArrayIds = @[];
    ZMProntoManager.sharedInstance.assetsIdToFilter = @[];
    ZMProntoManager.sharedInstance.selectedCategories = @[];
    
    // New revamp changes - tabbar
    _tabBar = [[ProntoTabbarViewController alloc]init];
    promotedTappedForAudio = false;
    
}


- (void)removeSelfFromListeningToNotificationCenter
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[ZMAssetsLibrary defaultLibrary] removeObserver:self];
    self.currentSelectedAssetToSeeDetail = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self stopAudio];
    multipleFranchisePromotedMessageDisplayed = YES;
    
}
-(void)removeStartingProntoView{
    
    if([self.view.subviews containsObject:_startUpView])
    [_startUpView removeFromSuperview];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isFetchEventsCompleted = NO;
    //Adding the Library as a valid observer for the ZMAssetsLibrary @see
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
    //It makes sure that only one time notification is observed.
    [self removeNotification:@"OPEN_SCHEMA_ASSET_APP_CLOSED"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET_APP_CLOSED"];
    [self registerNotification:kSignOutOnLaunchNotification];
    
     if([[ProntoUserDefaults singleton].isReadyToMakeServiceCalls isEqualToString:@"YES"]
        || ![_apiManager isConnectionAvailable]
        ){

         [self removeStartingProntoView];
            
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:@"Checking your login credentials.Please wait...."];
           
         
    if([[[NSUserDefaults standardUserDefaults] objectForKey:keyUserDefaultGridFranchise] isEqualToString:@"NO"] || ![[NSUserDefaults standardUserDefaults] objectForKey:keyUserDefaultGridFranchise] ) {
        [self configureListView];
    }
    else {
        [listGridbutton setImage:[UIImage imageNamed:@"listIcon"] forState:UIControlStateNormal];
    }
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW,3), dispatch_get_main_queue(), ^{
             [self fetchEvents];
         });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(self.isPromotedFirst)
        {
            [self displayPromotedContent:@""];
        }
    });
    [self getNotifications];
    //Revamp(1/12) - Assigning Tag
    [self headerUpdate:headerView];
    headerView.tabBarTag = 0;
    headerView.library = self;
    notificationsPannel.tabBarTag = SelectedTabFranchise;
    // New revamp changes - tabbar
    [_tabBar updateTabSelection:0];
    //End of Revamp
    self.navigationController.navigationBar.hidden = YES;
    if (launchLoading) {
        [launchLoading startAnimation];
    }
    
    [self checkSession];
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2)
        _leftMenuTableView.contentInset = UIEdgeInsetsMake(0, 0, 180, 0);
    else
        _leftMenuTableView.contentInset = UIEdgeInsetsMake(0, 0, 200, 0);
    
    [self adjustLeftTableView];
    
    NSArray* catList = [headerView categoryList];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"catList:%@",catList]];
    [_leftMenuTableView reloadData];

    // Pronto new revamp changes: To set leading space for sort button title
    sortButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    // End:
    
    if([headerView.userFranchise.text  isEqual: @"In House"] || [headerView.userFranchise.text  isEqual: @"Home Office"]) {
        promotedContainerView.hidden = true;
        [self setFrameOfPromotedGrid];
    }
         [self showPreferencesListViewIfRequired];
     }
    else{
        if(_startUpView == nil)
            _startUpView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.size.height+150)];
        _startUpView.backgroundColor = [UIColor blackColor];
        UIImageView *imageView = [UIImageView new];
        [imageView setImage:[UIImage imageNamed:@"prontoLogoBig"]];
        [_startUpView addSubview:imageView];
        [imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_startUpView.centerXAnchor constraintEqualToAnchor:imageView.centerXAnchor].active = YES;
        [imageView.topAnchor constraintLessThanOrEqualToAnchor:_startUpView.centerYAnchor constant:-165].active = YES;
        [imageView.heightAnchor constraintEqualToConstant:91].active = YES;
        [imageView.widthAnchor constraintEqualToConstant:264].active = YES;
        
        [self.view addSubview:_startUpView];

    }
    [ZMUserActions sharedInstance].currentSelectedTabIndex = 0;
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.actions.shouldSwitchToOtherTab == YES)
    {
        self.switchToTabBarItem = ProntoTabBarItemFranchise;
    
        
        if (self.switchToTabBarItem != ProntoTabBarItemFranchise &&
            self.isShowingIndicatorViewWithOverlay == NO)
        {
            [self switchToProntoTabbarItem:self.switchToTabBarItem];
        }
    }
}

- (void)showPreferencesListViewIfRequired
{
    if(!_actions.isLoggedoutFromHelp)
    {
        if ([ZMUserActions sharedInstance].isFieldUser == NO)
        {
            if ([ZMUserActions sharedInstance].shouldShowFranchiseListView == YES &&
                self.isShowingPreferencesView == NO && self.shouldLoadAllData == NO)
            {
                self.isShowingPreferencesView = YES;
                [ZMUserActions sharedInstance].shouldShowFranchiseListView = NO;
                PreferencesVCViewModel *preferencesViewModel = [[PreferencesVCViewModel alloc] init];
                
                if ([preferencesViewModel isAvailableLocalSavedPreferences] == YES)
                {
                    
                    if ([preferencesViewModel isFranchiseAndBrandsAvailable] == YES)
                    {
                        // DON'T SHOW PREFERENCES VIEW
                        [[NSNotificationCenter defaultCenter] postNotificationName:kPreferenceListVCDidSavePreferencesNotification
                                                                            object:nil];
                    }
                    else
                    {
                        self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress = YES;
//                        [preferencesViewModel initializeFranchiseAndBrandsWithCompletionHandler:^(BOOL success) {
//
//                            self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress = NO;
//
//                            if(self.isRefreshButtonTapped == YES)
//                            {
//                                [self makeForceRefreshCMSRequest];
//                                self.isRefreshButtonTapped = NO;
//                            }
//                            else
//                            {
//                                if (success)
//                                {
//                                    [[NSNotificationCenter defaultCenter] postNotificationName:kPreferenceListVCDidSavePreferencesNotification
//                                                                                        object:nil];
//                                }
//                                else
//                                {
//                                    // Needs to show saved preferences to the user to save preferences.
//                                    [self showPreferencesListViewController];
//                                }
//                            }
//                        }];
                    }
                }
                else
                {
                    __weak ZMLibrary *weakSelf = self;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        loadingIndicatorNew.loadingIndicatorLabel.text = @"Loading Preferences";
                        weakSelf.progressTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
                    });
                    
                    self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress = YES;
//                    [preferencesViewModel fetchPreferencesFromServerWithCompletionHandler:^(BOOL success) {
//                        
//                         //self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress = NO;
//                        
//                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                            loadingIndicatorNew.loadingIndicatorLabel.text = @"Refreshing Events";;
//                        });
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            
//                            if (success == YES)
//                            {
//                                // It means that saved preferences were at server but not locally.
//                                // updated local database from server. and not showing the preferences view
//                                if ([preferencesViewModel isAvailableLocalSavedPreferences] == YES)
//                                {
//                                    
//                                    //at server there were preference, but needs to sync the brand
//                                    self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress = YES;
//                                    [preferencesViewModel initializeFranchiseAndBrandsWithCompletionHandler:^(BOOL success) {
//                                        
//                                        self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress = NO;
//                                        
//                                        if(self.isRefreshButtonTapped == YES)
//                                        {
//                                            [self makeForceRefreshCMSRequest];
//                                            self.isRefreshButtonTapped = NO;
//                                        }
//                                        else
//                                        {
//                                            if (success)
//                                            {
//                                                [[NSNotificationCenter defaultCenter] postNotificationName:kPreferenceListVCDidSavePreferencesNotification
//                                                                                                    object:nil];
//                                            }
//                                            else
//                                            {
//                                                // Needs to show saved preferences to the user to save preferences.
//                                                [weakSelf showPreferencesListViewController];
//                                            }
//                                        }
//                                    }];
////                                    [[NSNotificationCenter defaultCenter] postNotificationName:kPreferenceListVCDidSavePreferencesNotification
////                                                                                        object:nil];
//                                }
//                                else
//                                {
//                                    // It means that saved preferences were not at server.
//                                    // Needs to show saved preferences to the user to save preferences.
//                                    [weakSelf showPreferencesListViewController];
//                                    
//                                    self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress = NO;
//                                }
//                            }
//                            else
//                            {
//                                // If prefernces fetching  fails, Needs to show preferences
//                                [weakSelf showPreferencesListViewController];
//                                
//                                self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress = NO;
//                            }
//                        });
//                    }];
                }
            }
            else
            {
                [AbbvieLogging logInfo:@" @objc"];
                // this scenario will happen for the home office users - whenever user is not saving the preferences
                self.isShowingPreferencesView = NO;
                [ZMUserActions sharedInstance].shouldShowFranchiseListView = NO;
                self.shouldLoadAllData = YES;
            }
        }
        else
        {
            self.isShowingPreferencesView = NO;
            self.shouldLoadAllData = YES;
            ZMProntoManager.sharedInstance.franchisesAndBrands = [self defaultFranchise:[self getFranchiseName]];
        }
    }
    else
    {
        if(_actions.isLoggedoutFromHelpOtherTab)
        {
            _actions.isLoggedoutFromHelpOtherTab = NO;
        }
        else
        {
            _actions.isLoggedoutFromHelp = NO;
        }
    }
}

- (void)viewDidLayoutSubviews
{

    if (!libraryInitialized)
    {
        if ([[ProntoUserDefaults userDefaults] getCMSLastSynchronization].length > 0)
        {
                [self backToDefault];
        }
    }
    if (!mostPopularAsset) {
        if([[ZMAssetsLibrary defaultLibrary] property:@"POPULAR_ASSET"]){
            mostPopularAsset = [[[ZMAssetsLibrary defaultLibrary] property:@"POPULAR_ASSET"] integerValue];
        }
    }
    if (mainDelegate.userHasChanged) {
        mainDelegate.userHasChanged = NO;
        [self loadLibraryEmpty];
    }
    
    
    //For Smoothness during rotation
    //Revamp - Hide the icon lables
    [self headerUpdate:headerView];
    //End of Revamp
    [notificationsPannel adjustNotifications];
    [notificationsPannel changeOfOrientation];
    [feedback adjustFrame:self.view];
    [self adjustLeftTableView];
    [self setFrameOfPromotedGrid];
    
    //Adjust loading indicator
    if(loadingIndicatorNew)
    {
        [loadingIndicatorNew adjustFrame];
        [mainDelegate adjustTabBarOverlay];
        [self.forceLogoutRefreshView adjustFrameWithRespectToSuperView];
    }
}

-(void) showWalkthroughOnInitialLogin
{
    BOOL isAppFreshlyInstalled = [[ProntoUserDefaults userDefaults] getIsAppFreshlyInstalled];
    if (isAppFreshlyInstalled == YES || self.actions.isVersionUpgraded == YES)
    {
        [self displayInitialWalkthroughWhenLogin];
        multipleFranchisePromotedMessageDisplayed = NO;
        [[ProntoUserDefaults userDefaults] setIsAppFreshlyInstalled:NO];
        self.actions.isVersionUpgraded = NO;
    }
}

- (void) showLoadingIndicatorForPromotedContent:(BOOL)show {
    if(show) {
        promotedActivityIndicator.hidden = false;
        promotedGrid.hidden = true;
        promotedPageControl.hidden = true;
        [promotedActivityIndicator startAnimating];
    }
    else {
        promotedActivityIndicator.hidden = true;
        promotedGrid.hidden = false;
        promotedPageControl.hidden = false;
        [promotedActivityIndicator stopAnimating];
    }
}

- (void) displayPromotedContent:(NSString *)triggeredFrom {
    
    // _promotedContentView shoud visible to Only for field user
    _promotedContentView = promotedContainerView;
    _promotedContentView.hidden = true;
    
    NSArray *selectedFranchiseArray = [self getAllFranchieseName];
    
    if(headerView.userFranchise.text.length > 0 || [self getFranchiseName].length > 0) {
        if(([headerView.userFranchise.text  isEqual: @"In House"] || [headerView.userFranchise.text  isEqual: @"Home Office"])  && (selectedFranchiseArray.count != 1)) {
            _promotedContentView.hidden = true;
            [promotedContentArray removeAllObjects];
            [promotedGrid reloadData];
            [AbbvieLogging logInfo:@"promotted hidden"];
            if((multipleFranchisePromotedMessageDisplayed == NO && self.isShowingIndicatorViewWithOverlay == NO)||(isFetchEventsCompleted && self.isShowingIndicatorViewWithOverlay == NO && ([[[NSUserDefaults standardUserDefaults]valueForKey:@"isFirstTime"]isEqualToString:@"YES"] || isSavePreferencesCompleted)))
            {
                [mainDelegate showMessage:kMultipleFranchiseForHomeOfficeMessage whithType:ZMProntoMessagesTypeWarning];
                multipleFranchisePromotedMessageDisplayed = YES;
                
                if(isFetchEventsCompleted && [[[NSUserDefaults standardUserDefaults]valueForKey:@"isFirstTime"]isEqualToString:@"YES"]){
                    [[NSUserDefaults standardUserDefaults]setValue:@"NO" forKey:@"isFirstTime"];
                    if(isSavePreferencesCompleted)
                        isSavePreferencesCompleted = NO;
                }
                else if(isSavePreferencesCompleted)
                {
                    isSavePreferencesCompleted = NO;
                }
            }
            
        } else {
            _promotedContentView.hidden = false;
        
            // New Revamping changes - Promoted Content Webservice Integration
//            NSArray *franchiseArray = [[NSArray alloc] init];
            NSArray *franchiseArray = nil;
            BOOL isHomeOfficeSingleFranchise = NO;
            if([[self getFranchiseName]  isEqual: @"In House"] || [[self getFranchiseName]  isEqual: @"Home Office"])
            {
                isHomeOfficeSingleFranchise = YES;
                franchiseArray = [[ZMCDManager sharedInstance] getFranchiseByName:selectedFranchiseArray.firstObject];
            }
            else
            {
                franchiseArray = [[ZMCDManager sharedInstance] getFranchiseByName:[self getFranchiseName]];
            }
            
            if (franchiseArray.count > 0) {
                Franchise * franchise = franchiseArray.firstObject;
                NSString* franchiseID = [NSString stringWithFormat:@"%@",franchise.tid];
            
                    dispatch_async(dispatch_get_main_queue(), ^{
                    if (promotedContentAssets.count > 0 && [ZMUserActions sharedInstance].isPromotedLoaded) {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"promotedContentAssets.count:%lu",(unsigned long)promotedContentAssets.count]];
                        if(![triggeredFrom isEqualToString:@"foreground_call"])
                            [self showLoadingIndicatorForPromotedContent:NO];
                        else
                            [self showLoadingIndicatorForPromotedContent:YES];
                    }
                    else
                    {
                        [self showLoadingIndicatorForPromotedContent:YES];
                    }
                       
                    if(isHomeOfficeSingleFranchise)
                    {
                        promotedActivityIndicator.hidden = false;
                        [promotedActivityIndicator startAnimating];
                    }
                    
                    // check the network availability
                    if(franchiseID.length > 0)
                    {
                        [self checkPromotedContent:franchiseID];
                    }

                    
                    
                });
            }
            else
            {
                _promotedContentView.hidden = true;
                promotedActivityIndicator.hidden = true;
                promotedGrid.hidden = true;
                promotedPageControl.hidden = true;
                
                NSArray * franchiseArray = [[ZMCDManager sharedInstance] getFranchiseByName:headerView.userFranchise.text];
                if (franchiseArray.count > 0) {
                    Franchise * franchise = franchiseArray.firstObject;
                    NSString* franchiseID = [NSString stringWithFormat:@"%@",franchise.tid];
                    [self performSelector:@selector(checkPromotedContent:) withObject:franchiseID afterDelay:0.1];
                }
            }
        }
        [self setFrameOfPromotedGrid];
        
    }

}

-(void) checkPromotedContent:(NSString*) franchiseID
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // check the network availability
        if(_apiManager.isConnectionAvailable)
        {
            [_apiManager getPromotedContent:franchiseID complete:^(NSDictionary *promotedAssets) {
                [self hideMessage];
                [AbbvieLogging logInfo:@"online promoted"];
                // display promoted content
                // Pronto New Revamp changes - offline promoted content
                if(promotedAssets != (NSDictionary*) [NSNull null]) {
                    _actions.promotedAssetsOffline = [promotedAssets mutableCopy];
                    // store the values
                    [[NSUserDefaults standardUserDefaults] setObject:_actions.promotedAssetsOffline forKey:@"promotedAssets"];
                    if (promotedAssets.count > 0){
                        
                        promotedActivityIndicator.hidden = true;
                        promotedGrid.hidden = false;
                        promotedPageControl.hidden = false;
                        promotedContentAssets = [NSMutableDictionary new];
                        [promotedAssets enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
                            promotedContentAssets[value] = key;
                        }];
                        int countTotal = 0;
                        int promotedCount = 0;
                        promotedContentArray = [[NSMutableArray alloc] init];
                        while(countTotal < promotedContentAssets.count) {
                            NSString *proAsset = [promotedContentAssets valueForKey:[NSString stringWithFormat:@"%ld",(long) promotedCount]];
                            
                            while (proAsset.length == 0) {
                                proAsset = [promotedContentAssets valueForKey:[NSString stringWithFormat:@"%ld",(long) promotedCount+1]];
                                promotedCount++;
                            }
                            if(proAsset.length != 0) {
                                promotedCount++;
                            }
                            NSArray * asset = [[ZMCDManager sharedInstance] getAssetArrayById:proAsset];
                            //Adding valid assets in promoted content
                            if(asset.count > 0) {
                                [promotedContentArray addObject:asset[0]];
                            }
                            countTotal++;
                        }
                        self.isPromotedFirst = YES;
                        [promotedGrid reloadData];
                        
                        if (grid.hidden) {
                            [listTableView reloadData];
                        } else {
                            [grid reloadData];
                        }
                    } else {
                        _promotedContentView.hidden = true;
                        promotedActivityIndicator.hidden = true;
                        promotedGrid.hidden = true;
                        promotedPageControl.hidden = true;
                        //Promoted content error message if not available
                        if(!promotedNoneMessageDisplayed) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [mainDelegate showMessage:@"There is no promoted content for this user" whithType:ZMProntoMessagesTypeWarning];
                            });
                            promotedNoneMessageDisplayed = true;
                        }
                        [self setFrameOfPromotedGrid];
                    }
                }
                else {
                    _promotedContentView.hidden = true;
                    promotedActivityIndicator.hidden = true;
                    promotedGrid.hidden = false;
                    promotedPageControl.hidden = false;
                    //Promoted content error message if not available
                    if(!promotedNoneMessageDisplayed) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [mainDelegate showMessage:@"There is no promoted content for this user" whithType:ZMProntoMessagesTypeWarning];
                        });
                        promotedNoneMessageDisplayed = true;
                    }
                    [self setFrameOfPromotedGrid];
                }
            } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@">>>> Error getting the assets %@", error]];
                _promotedContentView.hidden = true;
                promotedActivityIndicator.hidden = true;
                promotedGrid.hidden = false;
                promotedPageControl.hidden = false;
                [self setFrameOfPromotedGrid];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [mainDelegate showMessage:@"Promoted Content retrive Failed, please try after some time" whithType:ZMProntoMessagesTypeAlert];
                });
            }];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self offlinePromotedChanges];
            });
        }
    });
    
}

// Pronto Revamp - Offline changes for Promoted Content
-(void) offlinePromotedChanges
{
    [AbbvieLogging logInfo:@"offline promoted"];
    // remove the overlay
    //[mainDelegate showHideOverlay:YES];
    
    // offline changes
    promotedActivityIndicator.hidden = true;
    promotedGrid.hidden = false;
    promotedPageControl.hidden = false;
    // retreive data
    _actions.promotedAssetsOffline = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"promotedAssets"];
    
    if (_actions.promotedAssetsOffline.count > 0){
        promotedActivityIndicator.hidden = true;
        promotedGrid.hidden = false;
        promotedPageControl.hidden = false;
        promotedContentAssets = [NSMutableDictionary new];
        [_actions.promotedAssetsOffline enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
            promotedContentAssets[value] = key;
        }];
        int countTotal = 0;
        int promotedCount = 0;
        promotedContentArray = [[NSMutableArray alloc] init];
        while(countTotal < promotedContentAssets.count) {
            NSString *proAsset = [promotedContentAssets valueForKey:[NSString stringWithFormat:@"%ld",(long) promotedCount]];
            while (proAsset.length == 0) {
                proAsset = [promotedContentAssets valueForKey:[NSString stringWithFormat:@"%ld",(long) promotedCount+1]];
                promotedCount++;
            }
            if(proAsset.length != 0) {
                promotedCount++;
            }
            NSArray * asset = [[ZMCDManager sharedInstance] getAssetArrayById:proAsset];
            [promotedContentArray addObjectsFromArray:asset];
            countTotal++;
        }
        [promotedGrid reloadData];
        
        if (grid.hidden) {
            [listTableView reloadData];
        } else {
            [grid reloadData];
        }
    } else {
        _promotedContentView.hidden = true;
        promotedActivityIndicator.hidden = true;
        promotedGrid.hidden = false;
        promotedPageControl.hidden = false;
        [self setFrameOfPromotedGrid];
    }
}

- (void) setFrameOfPromotedGrid {
    if (_promotedContentView.hidden || promotedContainerView.hidden) {
        CGRect headerFrame = CGRectMake(contentHeader.frame.origin.x, kYPosition , contentHeader.frame.size.width, 40);
        CGFloat promotedHeight = [self view].frame.size.height - (kYPosition + headerFrame.size.height);
        CGRect gridFrame = CGRectMake(libraryScrollView.frame.origin.x, kYPosition + headerFrame.size.height , libraryScrollView.frame.size.width, promotedHeight);
        
        contentHeader.frame = headerFrame;
        _contentHeaderView = contentHeader;
        
        libraryScrollView.frame = gridFrame;
    } else {
        
        CGRect promotedContainerViewFrame = CGRectMake(promotedContainerView.frame.origin.x, promotedContainerView.frame.origin.y , promotedContainerView.frame.size.width, promotedContainerView.frame.size.height);
        
        CGRect headerFrame = CGRectMake(contentHeader.frame.origin.x, promotedContainerView.frame.size.height + promotedContainerView.frame.origin.y , contentHeader.frame.size.width, 40.0);
        
        CGFloat gridHeight = [self view].frame.size.height - (promotedContainerView.frame.size.height + headerFrame.size.height + promotedContainerViewFrame.origin.y);
        
        CGRect gridFrame = CGRectMake(libraryScrollView.frame.origin.x, (headerFrame.size.height + headerFrame.origin.y ) , libraryScrollView.frame.size.width, gridHeight);
        
        contentHeader.frame = headerFrame;
        _contentHeaderView = contentHeader;
        
        libraryScrollView.frame = gridFrame;
    }
}

-(void)checkSession{
    
    if ([AOFLoginManager sharedInstance].checkForAOFLogin) {
        
        if (!libraryInitialized) {
            
            NSString *storedUserId = [[ProntoUserDefaults userDefaults] getCurrentUserID];
            NSString *userId = [AOFLoginManager sharedInstance].upi;
            
            //The grid belongs to a master view and it is moved when the briefcase it's moved
            //Reposition of the scrollview (the grid) when the user logs into the application ***
            CGRect frame = grid.frame;
            frame.origin.x = 0;
            frame.origin.y = 0;
            frame.size.height = libraryScrollView.frame.size.height;
            grid.frame = frame;
            
            [self resetLabels];
            
            //Checks that was a synchronization and that is the same user
            if (([[ProntoUserDefaults userDefaults] getCMSLastSynchronization].length > 0) && [storedUserId isEqualToString:userId])
            {
                assetsAlreadyLoaded = YES;
                [self hideLaunchLoading];
                //To avoid loading of only 2 categories for franchise
                [self backToDefault];
                [ZMUserActions sharedInstance].enableSearch = YES;
                headerView.searchTextbox.enabled = YES;
                _apiManager._syncingCompletion = YES;
//                [ZMUserActions sharedInstance].shouldShowInitialWalkthrough = YES;
                
            }
            else
            {
                newUserLogged = YES;
                [self hideLaunchLoading];
                [AbbvieLogging logInfo:@"**Intro1"];
                [self hideEmpty];
                [self initiateLaunchLoading];
            }
            
            if ([[ZMAssetsLibrary defaultLibrary] property:@"POPULAR_ASSET"]) {
                mostPopularAsset = [[[ZMAssetsLibrary defaultLibrary] property:@"POPULAR_ASSET"] intValue];
            }
            //Revamp(4/12) - New Parm
            [self updateHeader:headerView];
            //End of Revamp
            libraryInitialized = YES;
        }
    }
    
    [self hideMessage];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([[ProntoUserDefaults singleton].isReadyToMakeServiceCalls isEqualToString:@"YES"])
        {
            [self displayPromotedContent:@""];
        
        }
    });
}

/**
 This methods is responsible for switching to tab, based on tab passed as param
 @param tabbarItem  this will become visible.
 */
- (void)switchToProntoTabbarItem:(ProntoTabBarItem)tabbarItem
{
    [ZMUserActions sharedInstance].shouldSwitchToOtherTab = NO;
    
    ProntoTabbarViewController *tabVC = [[AppUtilities sharedUtilities] getTabBarController];
    if (tabVC != nil)
    {
        if (tabbarItem == ProntoTabBarItemFranchise)
        {
            //current selected and to be selected tab is same. so, nothing needs to be done.
        }
        else if (tabbarItem == ProntoTabBarItemCompetitors)
        {
            [tabVC tabBarItemTapped:tabVC.competitorTabbarItem];
        }
        else if (tabbarItem == ProntoTabBarItemToolsTraining)
        {
             [tabVC tabBarItemTapped:tabVC.toolsTrainingTabbarItem];
        }
        else if (tabbarItem == ProntoTabBarItemEvents)
        {
            [tabVC tabBarItemTapped:tabVC.eventsTabbarItem];
            self.switchToTabBarItem = ProntoTabBarItemFranchise;
        }
    }
    else
    {
        //Let the current selection to be selected
    }
}

- (void) configurePromotedContentGrid {
    promotedGrid.alwaysBounceVertical = NO;
    promotedGrid.allowsSelection = YES;
    [promotedGrid setDelegate: self];
    [promotedGrid setDataSource: self];
    
    [promotedGrid registerNib:[UINib nibWithNibName:@"PromotedItem" bundle:nil] forCellWithReuseIdentifier:@"PromotedItem"];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [AbbvieLogging logInfo:@"Recieved memory warning"];
}

-(void) adjustLeftTableView {
    //Revamp(7/12) - Increase left menu height according to screen
    CGRect backgroundViewFrame = CGRectMake(tableBackgroundView.frame.origin.x, tableBackgroundView.frame.origin.y, tableBackgroundView.frame.size.width, self.view.bounds.size.height - (tableBackgroundView.frame.origin.y));
    tableBackgroundView.frame = backgroundViewFrame;
    
    CGRect tableFrame = CGRectMake(_leftMenuTableView.frame.origin.x, _leftMenuTableView.frame.origin.y, _leftMenuTableView.frame.size.width, self.view.bounds.size.height - (_leftMenuTableView.frame.origin.y));
    _leftMenuTableView.frame = tableFrame;
    //End of Revamp
}

- (void) willRotateToInterfaceOrientation: (UIInterfaceOrientation) toInterfaceOrientation duration: (NSTimeInterval) duration {
    [super willRotateToInterfaceOrientation: toInterfaceOrientation duration: duration];
    [walkthrough updateLayout];
    [headerView settingsHideOnRotate];
    [headerView searchHideOnRotate];
    //For fix of the list disappear issue
    visibleRows = [listTableView indexPathsForVisibleRows];
    
    CGSize contentSize = _leftMenuTableView.contentSize;
    contentSize.width = _leftMenuTableView.bounds.size.width;
    _leftMenuTableView.contentSize = contentSize;
    _leftMenuTableView.contentInset = UIEdgeInsetsMake(0, 0, 200, 0);
    [_leftMenuTableView reloadData];
    
    // Pronto new revamp changes: Hiding share button's pop over and deselecting its state image
    // Hides share button's pop over if it is opend at landscape/portrait mode
    if (_exportMenuIsPresented) {
        [[_exportMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        if (grid.hidden && exportPopOverTappedonListView) {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.shareButton.isSelected) {
                [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
                [cell.shareButton setSelected:NO];
            }
        }
        
        ZMGridItem *tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (tempCell && tempCell.shareButton.isSelected && exportPopOverTappedonGrid) {
            [tempCell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [tempCell.shareButton setSelected:NO];
        }
        ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (proCell && proCell.shareButton.isSelected && exportPopOverTappedonPromotedGrid) {
            [proCell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [proCell.shareButton setSelected:NO];
        }
    }
    // End
    
    if (_infoMenuIsPresented) {
        [[_infoMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        if (grid.hidden && infoPopOverTappedonListView) {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.infoButton.isSelected) {
                [cell.infoButton setImage:[UIImage imageNamed:@"infoIcon"] forState:UIControlStateNormal];
                [cell.infoButton setSelected:NO];
            }
        }
        ZMGridItem *tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (tempCell && tempCell.infoButton.isSelected) {
            [tempCell.infoButton setImage:[UIImage imageNamed:@"infoIcon"] forState:UIControlStateNormal];
            [tempCell.infoButton setSelected:NO];
        }
        ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (proCell && proCell.infoButton.isSelected) {
            [proCell.infoButton setImage:[UIImage imageNamed:@"infoIcon"] forState:UIControlStateNormal];
            [proCell.infoButton setSelected:NO];
        }
    }
    
//    [self.forceLogoutRefreshView adjustFrameWithRespectToSuperView];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    [headerView settingsShowOnRotate];
    [headerView searchShowOnRotate];
    [headerView adjustStatusBar];
    [self adjustStatusBar];
    //    [listTableView reloadData];
    //For fix of the list disappear issue
    if(visibleRows.count > 0)
        [listTableView scrollToRowAtIndexPath:[visibleRows objectAtIndex:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    if (_isBrowseMenuActive) {
        if (browseMenu) {
            [browseMenu updateLayoutView];
            [AbbvieLogging logInfo:@"Adjust browse Menu with "];
        }
    }
    
    NSArray *selectedFranchiseArray = [self getAllFranchieseName];
    // Pronto new revamp changes: To fix inter item space between promoted content grid items
    if(([headerView.userFranchise.text  isEqual: @"In House"] || [headerView.userFranchise.text  isEqual: @"Home Office"]) && (selectedFranchiseArray.count != 1)) {
        _promotedContentView.hidden = YES;
    }
    else
    {
        promotedPageControl.numberOfPages = 0;
        [promotedGrid reloadData];
        [promotedGrid setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
    // Pronto new revamp changes: Display share button's pop over if it is opend at landscape/portrait mode
    if (_exportMenuIsPresented) {
        if([ZMUserActions sharedInstance].export_infoMenuValOnOrientation == 0)
        {
            if (grid.hidden && exportPopOverTappedonListView) {
                FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (cell && !cell.shareButton.isSelected) {
                    [cell.shareButton setImage:[UIImage imageNamed:@"exportIconSelect"] forState:UIControlStateNormal];
                    [cell.shareButton setSelected:YES];
                    [self configureExportPopOver: cell.shareButton];
                }
            }
            if (exportPopOverTappedonGrid) {
                ZMGridItem *tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (tempCell && !tempCell.shareButton.isSelected) {
                    [tempCell.shareButton setImage:[UIImage imageNamed:@"exportIconSelect"] forState:UIControlStateSelected];
                    [tempCell.shareButton setSelected:YES];
                    [self configureExportPopOver: tempCell.shareButton];
                }
            }
            ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (proCell == nil && exportPopOverTappedonPromotedGrid) {
                proCell = (ZMGridItem*)[promotedGrid dequeueReusableCellWithReuseIdentifier:@"PromotedItem" forIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            }
            if (proCell && !proCell.shareButton.isSelected) {
                [promotedGrid reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedItem inSection:0]]];
                proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                [proCell.shareButton setImage:[UIImage imageNamed:@"exportIconSelect"] forState:UIControlStateNormal];
                [proCell.shareButton setSelected:YES];
                [self configureExportPopOver: proCell.shareButton];
            }
        }
        else
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
            _exportMenuIsPresented = NO;
        }
    }
    
    if (_infoMenuIsPresented) {
        if([ZMUserActions sharedInstance].export_infoMenuValOnOrientation == 0)
        {
            if (grid.hidden && infoPopOverTappedonListView) {
                FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (cell && !cell.infoButton.isSelected) {
                    [cell.infoButton setImage:[UIImage imageNamed:@"infoIconSelect"] forState:UIControlStateNormal];
                    [cell.infoButton setSelected:YES];
                    [self configureInfoPopOver: cell.infoButton];
                }
            }
            ZMGridItem *tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (tempCell && !tempCell.infoButton.isSelected) {
                [tempCell.infoButton setImage:[UIImage imageNamed:@"infoIconSelect"] forState:UIControlStateSelected];
                [tempCell.infoButton setSelected:YES];
                [self configureInfoPopOver: tempCell.infoButton];
            }
            ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (proCell == nil && infoPopOverTappedonPromotedGrid) {
                proCell = (ZMGridItem*)[promotedGrid dequeueReusableCellWithReuseIdentifier:@"PromotedItem" forIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            }
            if (proCell && !proCell.infoButton.isSelected) {
                [promotedGrid reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:selectedItem inSection:0]]];
                proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                [proCell.infoButton setImage:[UIImage imageNamed:@"infoIconSelect"] forState:UIControlStateSelected];
                [proCell.infoButton setSelected:YES];
                [self configureInfoPopOver: proCell.infoButton];
            }
        }
        else
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
            _infoMenuIsPresented = NO;
        }
    }
    
    if(assetForAudio != nil)
    {
        if (!grid.hidden)
        {
            [grid reloadData];
        }
    }
    
    [self.forceLogoutRefreshView adjustFrameWithRespectToSuperView];
}

//Revamp(1/12) - Updating the header func
- (void) headerUpdate: (ZMHeader*) headerToUpdateView {
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation)) {
        [headerToUpdateView.messagesButton setTitle:@" " forState:UIControlStateNormal];
        [headerToUpdateView.helpButton setTitle:@" " forState:UIControlStateNormal];
        [headerToUpdateView.profileButton setTitle:@" " forState:UIControlStateNormal];
        
    }
    else {
        [headerToUpdateView.messagesButton setTitle:@"Messages" forState:UIControlStateNormal];
        [headerToUpdateView.helpButton setTitle:@"Help" forState:UIControlStateNormal];
        [headerToUpdateView.profileButton setTitle:@"Profile" forState:UIControlStateNormal];
        
    }
    [headerToUpdateView adjustHeader];
}

-(void) leftMenuRefresh
{
    CGSize contentSize = _leftMenuTableView.contentSize;
    contentSize.width = _leftMenuTableView.bounds.size.width;
    _leftMenuTableView.contentSize = contentSize;
    _leftMenuTableView.contentInset = UIEdgeInsetsMake(0, 0, 200, 0);
    [_leftMenuTableView reloadData];
    [self adjustLeftTableView];
}




//End of Revamp

/**
 *  This method initialize the theme gives styling to the viewController
 */
- (void)styleLibrary {
    theme = [ZMProntoTheme sharedTheme];
    [theme styleMainView:self.view];
}

/**
 *  Adds the events to the Main viewController
 */
- (void)addEvents {
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPress.minimumPressDuration = .5;
    [grid addGestureRecognizer:longPress];
    
}


- (void)dismissIfShownKeyboard {
    [self.view endEditing:YES];
}


/**
 *  Long press handler
 *  @param gestureRecognizer gestureRecognizer event
 */
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    
    CGPoint point = [gestureRecognizer locationInView:grid];
    
    if (_section == ProntoSectionTypeBriefcaseFolderDetail || _section == ProntoSectionTypeBriefcase) {
        
        ZMGridItem *tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath:[grid indexPathForItemAtPoint:point]];
        
        if (currentCell.flipped && currentCell.uid != tempCell.uid) {
            [currentCell flip];
        }
    }
    
    currentCell = (ZMGridItem *)[grid cellForItemAtIndexPath:[grid indexPathForItemAtPoint:point]];
    
    if (_section == ProntoSectionTypeBriefcaseFolderDetail || _section == ProntoSectionTypeBriefcase) {
        
        _selectedIndexPath = [grid indexPathForItemAtPoint:point];
        
        if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
            return;
        }
        if (_selectedIndexPath != nil) {
            
            if (currentCell.type == ZMGridItemTypeAsset) {
                [currentCell flip];
            }
        }
    }
    
    if (currentCell.type == ZMGridItemTypeDefault || currentCell.type == ZMGridItemTypeAsset) {
        [currentCell highlight];
    }
}

- (void)defaultActions
{
    if (_actions)
    {
        [[ZMProntoManager sharedInstance].search reset];
    }
}

/*!
 *  Hide the keyboard when other parts of the screens is touched
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

- (void)hideKeyboard{
    [self.view endEditing:YES];
}

/*!
 * Re-setting the labels when a new session starts
 */
- (void)resetLabels {
    _pageIndex = 0;
    _actions.section = @"library";
    _actions.sorts = [self getSortArray];
    _section = ProntoSectionTypeLibrary;
    headerView.searchTextbox.text = @"";
    [[ZMProntoManager sharedInstance].search reset];
}

# pragma mark - ZMAssetsLibrary Calls
//Revamp(4/12) - New Parm
-(void) changeBadgeNumber:(int)badgeNumber header:(ZMHeader *) headerForBadge
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNumber];
        headerForBadge.badgeNumber = badgeNumber;
        if (badgeNumber < 0) {
            headerForBadge.badgeNumber = 0;
        }
    });
}
//End of Revamp

- (void)saveNotifications:(NSDictionary *) notifications {
    
    for (NSDictionary *item in notifications) {
        
        if ([item isKindOfClass:[NSDictionary class]]) {
            [self saveNotification:item];
        }
        else {
            
            if([item isEqual:@"changed"]){
                [[ZMAssetsLibrary defaultLibrary] setProperty:@"NOTIFICATIONS_LASTUPDATE" value:[notifications objectForKey:item]];
            }
            else if(![item isEqual:@"deleted"]){
                [self saveNotification:[notifications objectForKey:item]];
            }
            else {
                //Prepare to delete expired or deleted notifications
                NSArray *notificationsToRemove = (NSArray *)[notifications objectForKey:item];
                NSString *message;
                for (NSString *notificationId in notificationsToRemove){
                    [[ZMAssetsLibrary defaultLibrary] deleteNotification:[notificationId intValue]];
                    
                    if (notificationsPannel) {
                        message = @"One or more assets related to the listed messages are not available. The assets might be expired or deleted by an Administrator. Messages related will be removed from the Messages list.";
                    }
                }
                if (message && notificationsPannel != nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Messages"
                         message:message
                         delegate:self cancelButtonTitle:@"Ok"
                         otherButtonTitles:nil];
                         [alert show];*/
                        
                        // Alternative AlertView
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Messages"  message:message preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* okButton = [UIAlertAction
                                                   actionWithTitle:@"OK"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       //Handle OK button
                                                       
                                                   }];
                        
                        [alert addAction:okButton];
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }
            }
        }
    }
    
    _notificationsLoaded = YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (notificationsPannel != nil) {
            [notificationsPannel populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
            //Revamp(20/12) - To stop the unwanted close of the notification detail
            //[notificationsPannel closeDetail];
        }
    });
}


- (void)saveNotification:(NSDictionary *)not {
    ZMNotification *notification;
    ZMNotification *verifyNotification;
    verifyNotification = [[ZMAssetsLibrary defaultLibrary] selectNotification:[[not objectForKey:@"thread_id"] integerValue]];
    
    if (verifyNotification.notificationId != [[not objectForKey:@"thread_id"] integerValue]) {
        notification = [[ZMNotification alloc] init];
        notification.notificationId = [[not objectForKey:@"thread_id"] integerValue];
        notification.subject = [not objectForKey:@"subject"];
        notification.message = [not objectForKey:@"body"];
        notification.action = [not objectForKey:@"type"];
        notification.assetId = [[not objectForKey:@"asset"] integerValue];
        notification.date = [not objectForKey:@"last_updated"];
        notification.viewed = 0;
        
        if ([[not objectForKey:@"type"] isEqualToString:@"Url"]) {
            NSArray *urlInfo = [[not objectForKey:@"url"] objectForKey:@"und"];
            notification.url = [[urlInfo objectAtIndex:0] objectForKey:@"url"];
        }
        if (![[not objectForKey:@"app"] isEqualToString:@""]) {
            notification.url = [not objectForKey:@"app"];
        }
        [[ZMAssetsLibrary defaultLibrary] saveNotification:notification];
    }
}


- (void)geTotalOfNotifications {
    int total = [[ZMAssetsLibrary defaultLibrary] getTotalUnreadNotifications];
    
    [[ZMAssetsLibrary defaultLibrary] setProperty:@"TOTAL_NOTIFICATIONS" value:[NSString stringWithFormat:@"%d", total]];
    //Revamp(4/12) - New Parm
    [self changeBadgeNumber:total header:headerView];
    //End of Revamp
}


- (void)updateViewedNotification:(long)notificationId viewed:(long)viewed {
    //1 to viewed notification 0 to not viewed notification
    ZMNotification *notification = [[ZMNotification alloc] init];
    notification.notificationId = notificationId;
    notification.viewed = viewed;
    [[ZMAssetsLibrary defaultLibrary] updateViewedNotification:notification];
}

- (void)getNotifications{
    
//    [self loadNotification];
//
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//
//        [ getNotificationsWithDate:[[ZMAssetsLibrary defaultLibrary] property:@"NOTIFICATIONS_LASTUPDATE"] completeBlock:^(NSDictionary *notifications) {
//
//            //@Fix solving issues with quotes
//            NSArray *keys = [notifications allKeys];
//            NSMutableDictionary *formatedNotifications = [[NSMutableDictionary dictionaryWithDictionary:notifications] mutableCopy];
//
//            for (NSString *key in keys) {
//
//                if ([[formatedNotifications objectForKey:key] isKindOfClass:[NSDictionary class]]) {
//
//                    if ([[formatedNotifications objectForKey:key] objectForKey:@"subject"]) {
//
//                        NSMutableDictionary *notif = [[formatedNotifications objectForKey:key] mutableCopy];
//                        NSString *subject = [notif objectForKey:@"subject"];
//                        subject = [subject stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
//                        [notif setObject:subject forKey:@"subject"];
//                        [formatedNotifications setObject:notif forKey:key];
//                    }
//                }
//            }
//
//            [self saveNotifications:(NSDictionary *) formatedNotifications];
//            [self geTotalOfNotifications];
//
//        } error:^(NSError *error) {
//            [AbbvieLogging logError:@"Error getting the app notifications"];
//        }];
//
//    });
}

- (void)loadNotification{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (notificationsPannel) {
            [notificationsPannel populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
        }
    });
    
    int totalNotifications = 0;
    if ([[ZMAssetsLibrary defaultLibrary] property:@"TOTAL_NOTIFICATIONS"]) {
        totalNotifications = [[[ZMAssetsLibrary defaultLibrary] property:@"TOTAL_NOTIFICATIONS"] intValue];
    }
    
    //Revamp(4/12) - New Parm
    [self changeBadgeNumber:totalNotifications header:headerView];
    _notificationsLoaded = YES;
    //End of Revamp
}

//Setting frame for views
- (void) setFrame
{
    //self.globalSearcVC.view.frame = CGRectMake(0, 63,self.view.frame.size.width, self.view.frame.size.height - 63);
}

#pragma mark - Library Helpers

/**
 *  Configure global appereance to ViewController
 */

-(void)configureAppereance{
    
    //Configure Style to ViewController
    
    [self styleLibrary];
    
    //Configure Appereance CollectionView
    
    [libraryScrollView setDelegate:self];
    
    refreshControl = [[UIRefreshControl alloc] init];
    
    [refreshControl addTarget:self action:@selector(refreshAssets:) forControlEvents:UIControlEventValueChanged];
    
    
    grid.alwaysBounceVertical = YES;
    grid.allowsSelection = YES;
    [grid addSubview:refreshControl];
    [grid setDataSource:self];
    [grid setDelegate:self];
    [grid registerNib:[UINib nibWithNibName:@"AssetItem" bundle:nil] forCellWithReuseIdentifier:@"AssetItem"];
    [grid registerNib:[UINib nibWithNibName:@"FolderItem" bundle:nil] forCellWithReuseIdentifier:@"FolderItem"];
    listTableView.hidden = true;
    grid.hidden = false;
    //Set Gesture to CollectionView
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPress.minimumPressDuration = .5;
    [grid addGestureRecognizer:longPress];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [grid addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
    
    //Configure Appereance Header Menu
    
    headerView = (ZMHeader *)[[[NSBundle mainBundle] loadNibNamed:@"Header" owner:self options:nil] objectAtIndex:0];
    headerView.delegate = (id<headerActions>)self;
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Franchise:%@ acFR:%@",headerView.userFranchise.text,_actions.franchiseName]];
    [headerView initWithOptions:header];
    
    /** If you want to have full selection list turn showResumeSelection to NO */
    headerView.showResumeSelection = YES;
    headerView.library = self;
    headerView.sourceViewController = self;
}


/*!
 * New method to bring all the data
 */
- (void)bringLibraryData
{
    @synchronized(self)
    {
        if ([ZMUserActions sharedInstance].isFieldUser == NO) // It means user is Home Office.
        {
            if (self.shouldLoadAllData == NO)
            {
                return;
            }
        }
        if (!_apiManager._syncingInProgress) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showForceLogoutRefreshView:NO];
                    [self.forceLogoutRefreshView showErroView:NO];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        loadingIndicatorNew.loadingIndicatorLabel.text = @"Refreshing Events";
                        [loadingIndicatorNew.activityIndicator startAnimating];
                    });
                    
                });
                
                [self getNotifications];
                [mainDelegate validateUserStatus];
                self.headerView.isResetSelected = NO;
                
                if(!syncWithCMSActive)
                {
                    
                    _apiManager.libraryVC = self;
                    [_apiManager syncWithCMS];
                    syncWithCMSActive = YES;
                    //Send the events to the server
                    dispatch_async(dispatch_queue_create("Sending Events from ZMLibrary", NULL), ^{
                        [[ZMAbbvieAPI sharedAPIManager] uploadEvents:^{
                            [AbbvieLogging logInfo:@">>> Events Synced"];
                        } error:^(NSError *error) {
                            [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with uploadEvents: %@", error]];
                        }];
                    });
                    
                }
            });
        }
    }
}

- (void)hideMessage{
    [mainDelegate hideMessage];
}

/**
 *  Displays login if there is not a valid session
 */
- (void)displayLogin{

    //Revamp(11/12) - Update Status Bar colour
     dispatch_async(dispatch_get_main_queue(), ^{
    self.statusBar = [[UIView alloc]init];
    if (@available(iOS 13.0, *)) {
        self.statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
    }
    else
    {
        self.statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    }
    [theme styleHeader:self.statusBar];
    [theme styleHeader:headerView.UserInformation];
    //End of Revamp
   
            [[UIApplication sharedApplication].keyWindow addSubview:self.statusBar];
    
    //Hides the notification if they is openned
    
    if (notificationsPannel) {
        [notificationsPannel closePannel];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AOFLoadingController *loadingVC = (AOFLoadingController *)[storyboard instantiateViewControllerWithIdentifier:@"loadingController"];
    [self.navigationController pushViewController:loadingVC animated:NO];
    
    });
    [ZMUserActions sharedInstance].isPromotedLoaded = NO;
    
}


- (void)loadLibraryEmpty {
    
    [self hideMessage];
    
    if (libraryScrollView.contentOffset.x > 0) {
        [libraryScrollView scrollRectToVisible:CGRectMake(0, 0, libraryScrollView.frame.size.width, libraryScrollView.frame.size.height) animated:NO];
    }
    
    /** removing all items from grid */
    [self hideEmpty];
    
    self.assetsPage = [[NSMutableArray alloc] init];
    [self reloadAssets];
    
    headerView.folderButton.enabled = YES;
    _pageIndex = 0;
}

//Revamp(4/12) - New Parm
/**
 *  Displays notification if the pannel is displayed
 */
-(void) displayNotifications:(UIView*) viewToDisplayNotifications{
    
    if (!notificationsPannel.parent) {
        if (_notificationsLoaded) {
            
            notificationsPannel = (ZMNotifications *)[[[NSBundle mainBundle] loadNibNamed:@"Notifications" owner:self options:nil] objectAtIndex:0];
            //Revamp - Make message pannel appear above tabbar
            //            [mainDelegate showHideOverlayForTabBar:NO];
            [notificationsPannel initWithParent:viewToDisplayNotifications background:backgroundView];
            //End of Revamp
            notificationsPannel.tabBarTag = SelectedTabFranchise;
            notificationsPannel.library = self;
            [notificationsPannel populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
            
            //Revamp(13/12) - Show arrow delayed
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self showHideArrow:NO];
            });
            //End of Revamp
        }
    }
}

/**
 *  Displays the walkthrough component on main view
 */
- (void)displayWalkthrough:(UIView*) viewToDisplay {
//    UIViewController <HelpViewControllerDelegate> *controller;
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    controller = (HelpViewController*)[storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
//    controller.userFranchise = [self getFranchiseName];
//    controller.library = self;
//    [self.navigationController pushViewController:controller animated:NO];
}
//End of Revamp

- (void)displayInitialWalkthroughWhenLogin
{
    if(!walkthrough.superview){
        NSString *file = [[NSBundle mainBundle] pathForResource:@"WalkthroughData" ofType:@"plist"];
        NSArray *dict = [NSArray arrayWithContentsOfFile:file];
        walkthrough = [[ZMWalkthrough alloc]initWithParent:[UIApplication sharedApplication].keyWindow andData:dict];
        if (launchLoading) {
            [[UIApplication sharedApplication].keyWindow insertSubview:walkthrough belowSubview:launchLoading];
        }
        else {
            [[UIApplication sharedApplication].keyWindow addSubview:walkthrough];
        }
    }
    [ZMWalkthrough transitionWithView:walkthrough
                             duration:0.3
                              options:UIViewAnimationOptionTransitionCrossDissolve
                           animations:NULL
                           completion:NULL];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:walkthrough];
    [walkthrough goToPage:0];
    [walkthrough setHidden:NO];
}

/**
 *  Displays the loading indicator component on main view
 */
- (void)displayLaunchLoading
{
    self.isShowingIndicatorViewWithOverlay = YES;
    [ZMUserActions sharedInstance].selectLeftMenu = @"";
    [LoadingIndicatorNew transitionWithView:loadingIndicatorNew
                               duration:0.3
                                options:UIViewAnimationOptionTransitionCrossDissolve
                             animations:NULL
                             completion:NULL];
    [mainDelegate showHideOverlayForTabBar:NO];
    [loadingIndicatorNew setHidden:NO];
//    [self showForceLogoutRefreshView:YES];
}

//Hides the popup that indicates that assets are loading
- (void)hideLaunchLoading
{
    self.isShowingIndicatorViewWithOverlay = NO;
    
    [ZMUserActions sharedInstance].selectLeftMenu = @"";
    [LoadingIndicatorNew transitionWithView:loadingIndicatorNew
                               duration:0.3
                                options:UIViewAnimationOptionTransitionCrossDissolve
                             animations:nil
                             completion:^(BOOL finished) {
                                 loadingIndicatorNew.loadingIndicatorLabel.text = @"Refreshing Events";
                              
                                 // hiding the indicator
                                 [headerView hidingIndicator];
                             }];
    [mainDelegate showHideOverlayForTabBar:YES];
    [loadingIndicatorNew setHidden:YES];
    [self showForceLogoutRefreshView:NO];

}

-(void)header:(ZMHeader*)headerView textFieldDidBeginEditing:(UITextField*)textField
{
    // Load the Global search view from here
    // Need to take care of observer, becuase same observer being used by tools and training
    // If not required remove it, Or when loading global search remove the assetlibrary observer
    // And when removing the view the view, start listening to assetlibrary observer

    [AbbvieLogging logInfo:@"Text field editing started - Present Global Search"];
    [self loadGlobalSearchView];
}

-(void)header:(ZMHeader*)headerView isDismissingSearchPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    [self.globalSearcVC dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
}

- (void)loadGlobalSearchView
{
    if (self.globalSearcVC == nil)
    {
        self.globalSearcVC = [GlobalSearchVC GetGSLibraryViewController];
        self.globalSearcVC.gsVCDelegate = (id<GSViewControllerDelegate>)self;
    }
    
    self.headerView.delegate = nil;
    self.headerView.delegate = (id<headerActions>)self.globalSearcVC;
    self.globalSearcVC.parentHeader = self.headerView;
    _actions.globalSearchViewController = self.globalSearcVC;
    [[ZMAssetsLibrary defaultLibrary] removeObserver:self];
    
    //Set frame
    self.globalSearcVC.view.frame = CGRectMake(0, 63,self.view.frame.size.width, self.view.frame.size.height - 63);
    
    [self addChildViewController:self.globalSearcVC];
    [self.view addSubview:self.globalSearcVC.view];
    [self.globalSearcVC didMoveToParentViewController:self];
    
    [[AppUtilities sharedUtilities] shouldMakeAllProntoTabBarButtonItemSelectable:NO];
}


- (void)enableAssetLibraryObserver
{
//    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
}

-(void)getNavigationOfAppDelegate:(UIViewController*)viewController
{
    AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController* navigationController = (UINavigationController*)(mainDelegate.window.rootViewController);
    [navigationController pushViewController:viewController animated:YES];
}

#pragma mark - GSViewControllerDelegate Method
- (void)didRemovedFromSuperView
{
    [AbbvieLogging logInfo:@"Global Search Removed from Superview - LIBRARY"];
    [[ZMProntoManager sharedInstance].search setTitle:@""];
    [[ZMAssetsLibrary defaultLibrary] addObserver:self]; // verify
    self.headerView.delegate = nil;
    _actions.globalSearchViewController = nil;
    self.headerView.delegate = (id<headerActions>)self;
    self.globalSearcVC = nil;
    [[ZMProntoManager sharedInstance].search reset];
//    [self loadMyLibrary];
    if(grid.hidden)
    {
        [listTableView reloadData];
    }
    else
    {
        [grid reloadData];
    }
    [_leftMenuTableView reloadData];
    [[AppUtilities sharedUtilities] shouldMakeAllProntoTabBarButtonItemSelectable:YES];
    [[EventCoreDataManager sharedInstance] resetEventSearchedResults];
    [[EventCoreDataManager sharedInstance] resetEventOnlyAssets];
    [[EventCoreDataManager sharedInstance] resetAssets];
}

- (void)displayEmpty:(BOOL)isInAFolder{
    
    if (!empty) {
        //Pronto new revamp changes: To show message when thr is no assets
        if (grid.hidden) {
            empty = [[ZMEmpty alloc] initWithParent: listTableView];
        } else {
            empty = [[ZMEmpty alloc] initWithParent:grid];
        }
        // End
    }
    empty.message.hidden = YES;
    empty.subtitle.hidden = YES;
  
    [empty centerMessage];
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject: [[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
    [trackingOptions setObject:@"None" forKey:@"scfilteraction"];
    [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    
    /**
     * @Tracking
     * Default Action
     **/
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section
              withSubsection:@"filter"
                    withName:[NSString stringWithFormat:@"brand-%@-none", [ZMUserActions sharedInstance].franchiseName]
                 withOptions:trackingOptions];
    
        if(_actions.assetsCount == 0)
        {
            NSString *message = nil;
            
            if([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kFranchiseFavorites])
            {
                message = @"You need to add a favorite asset first in order to see content.";
            }
            else if([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kAllFranchiseAssets])
            {
                message = @"No asset available for this selection.";
            }
            else
            {
                if (([ZMUserActions sharedInstance].selectLeftMenu.length <= 0) &&
                    self.actions.categories.count == 2) // default selection
                {
                    NSDictionary *dict = self.actions.categories.firstObject;
                    if ([dict[@"selected"] isEqualToString:@"1"]) // All Franchise Selected
                    {
                        [ZMUserActions sharedInstance].selectLeftMenu = kAllFranchiseAssets;
                        message = @"No asset available for this selection.";
                    }
                    else //Franchise Favorites selected
                    {
                        [ZMUserActions sharedInstance].selectLeftMenu = kFranchiseFavorites;
                        message = @"You need to add a favorite asset first in order to see content.";
                    }
                }
                else
                {
                    message = @"";
                }
            }
            
            if(isBackendApp || headerView.searchTextbox.placeholder.length > 6)
            {
                [headerView showingIndicator];
                message = @"";
                
            }
            
            if ([headerView.categoryButton.titleLabel.text isEqualToString:@"Category:  None Selected"] )
            {
                message = @"You need to select a category first in order to see content.";
            }
            
            // Putting extra check
            if (([loadingIndicatorNew.loadingIndicatorLabel.text isEqualToString:@"Refreshing Events"] ||
                 [loadingIndicatorNew.loadingIndicatorLabel.text isEqualToString:@"Getting Assets"] ||
                 [loadingIndicatorNew.loadingIndicatorLabel.text isEqualToString:@"Synchronizing User Data"] ) &&
                _apiManager._syncingInProgress == YES)
            {
                message = @"";
            }
            empty.subtitle.text = message;
            empty.message.text = @"";
        }
        
        if (isInAFolder == NO)
        {
            [self.view insertSubview:empty belowSubview:scrollSpinner];
        }
        
    [ZMEmpty transitionWithView:empty duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:^(BOOL finish){
        empty.message.hidden = NO;
        empty.subtitle.hidden = NO;
    }];
    [empty setHidden:NO];
    
}

- (void)hideEmpty
{
    
    [empty removeFromSuperview];
}

/**
 *  Gets the name of the sales franchise
 *  @return
 */
- (NSString *)getFranchiseName
{
    NSString *franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
    //chnages for Warnings
//    NSString* nameFranchise = [[NSString alloc]init];
    NSString* nameFranchise = nil;
    
    _actions.franchiseId = @"";
    if ([franchiseId isEqualToString:@""]) {
        if ([franchiseId isEqualToString:@""]) {
            nameFranchise = @"In House";
        }else{
            nameFranchise = [AOFLoginManager sharedInstance].salesFranchise;
        }
        _actions.franchiseName = nameFranchise;
        if ([[ZMCDManager sharedInstance] getFranchiseById:franchiseId].count > 0) {
            Franchise * franchise = [[ZMCDManager sharedInstance] getFranchiseById:franchiseId].firstObject;
            _actions.franchiseId = franchise.franchiseID.stringValue;
        }
    }else{

        if ([[ZMCDManager sharedInstance] getFranchiseById:franchiseId].count > 0) {
            Franchise * franchise = [[ZMCDManager sharedInstance] getFranchiseById:franchiseId].firstObject;
            nameFranchise = franchise.name;
            _actions.franchiseName = nameFranchise;
            _actions.franchiseId = franchise.franchiseID.stringValue;
        }
        else
        {
            
            if([ZMUserActions sharedInstance].isLoginSuccess && _actions.assetsCount == 0)
            {
                [_apiManager synchronizeBrands:^{
                    [AbbvieLogging logInfo:@">>>> Finished sycronizing brands "];
                    if ([[ZMCDManager sharedInstance] getFranchiseById:franchiseId].count > 0) {
                        Franchise * franchise = [[ZMCDManager sharedInstance] getFranchiseById:franchiseId].firstObject;
                        _actions.franchiseId = franchise.franchiseID.stringValue;
                        _actions.franchiseName = franchise.name;
                        [ZMProntoManager sharedInstance].franchisesAndBrands = [self defaultFranchise:franchise.name];
                        
                    }
                    
                } error:^(NSError *error) {
                    _apiManager._syncingInProgress = NO;
                    _apiManager.lastServerCodeResponse = (int)error.code;
                    
                }];
            }
            
            return _actions.franchiseName;
        }
        
    }
    
    return _actions.franchiseName;
}



-(NSMutableArray *)checkUnCheckAll:(BOOL)selected excludingFranchiseName:(NSString *)franchiseName
{
    NSMutableArray *franchisesAndBrands = [[NSMutableArray alloc] init];
    NSMutableArray *brands;
    NSDictionary *brand;
    NSDictionary *franchiseNode;
    BOOL isValidSelectableFrachise;
    BOOL selectableValue;
    
    for (ZMAssetsGroup*franchise in [[ZMCDManager sharedInstance] getFranchises]) {
        brands = [[NSMutableArray alloc] init];
        
        if ([franchiseName isEqualToString:franchise.name]) {
            selectableValue = YES;
        }
        else {
            selectableValue = selected;
        }
        
        for (ZMAssetsGroup*bra in [self getBrandsWithFranchiseUid:franchise.uid andFranchiseName:franchiseName]) {
            
            brand = @{
                      @"id" : [NSString stringWithFormat:@"%ld", bra.uid],
                      @"name" : bra.name,
                      @"selected": [NSString stringWithFormat:@"%d", selectableValue]
                      };
            [brands addObject:brand];
        }
        isValidSelectableFrachise = (selectableValue)?[brands count]>0:selectableValue;
        
        if (isValidSelectableFrachise) {
            franchiseNode = @{
                              @"id" : [NSString stringWithFormat:@"%ld", franchise.uid],
                              @"name" : franchise.name,
                              @"selected": [NSString stringWithFormat:@"%d", isValidSelectableFrachise],
                              @"brands" : brands
                              };
            [franchisesAndBrands addObject:franchiseNode];
        }
    }
    
    return franchisesAndBrands;
}


/**
 *  Get the brands with franchise id and franchise name
 *  @param uid
 *  @param franchiseName
 *  @return
 */
-(NSArray *) getBrandsWithFranchiseUid:(long)uid andFranchiseName:(NSString *)franchiseName
{
    NSArray *searchedBrands;
    NSMutableArray *filters = [[NSMutableArray alloc] init];
    if(![franchiseName isEqualToString:@""])
    {
        NSArray *foundFranchises = [[ZMAssetsLibrary defaultLibrary] franchiseWithName:franchiseName];
        ZMAssetsGroup *brands;
        if([foundFranchises count] > 0)
        {
            for(ZMAssetsGroup *franchiseResult in foundFranchises)
            {
                if([franchiseResult.name isEqualToString:franchiseName])
                {
                    brands = [[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:@"Brands" uid:franchiseResult.uid corporate_id:0 related:nil];
                    [filters addObject:brands];
                } else {
                    brands = [[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:@"Brands" uid:-1 corporate_id:0 related:nil];
                    [filters addObject:brands];
                }
                searchedBrands = [self getBrands:brands withUID:uid];
            }
        }
        else
        {
            ZMAssetsGroup *brands = [[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:@"Brands" uid:-1 corporate_id:-1 related:nil];
            [filters addObject:brands];
            searchedBrands = [self getBrands:brands withUID:uid];
        }
        
    }
    return searchedBrands;
}

/**
 *  Gets the brands with the given filter and id
 *  @param filter filter description
 *  @param uid
 *  @return returns the found brands
 */
-(NSArray *) getBrands:(ZMAssetsGroup *) filter withUID:(long)uid
{
    NSArray *searchedFranchises;
    if((filter.uid > 0) || (filter.uid<0))
    {
        //searchedFranchises = [[[ZMAssetsLibrary defaultLibrary] groups] copy];
        searchedFranchises = [[ZMAssetsLibrary defaultLibrary] groupsWithRelatedUID:uid];
    } else {
        searchedFranchises = [[ZMAssetsLibrary defaultLibrary] groupsWithRelatedUID:0];
    }
    return searchedFranchises;
}


/**
 *  getFranchises
 *  @return array of franchises
 */
-(NSArray *) getFranchises
{
    ZMAssetsGroup *franchise = [[ZMAssetsGroup alloc] initWithProperties:[NSDictionary dictionary] name:@"Franchise" uid:0 corporate_id:0 related:nil];
    NSArray *searchedFranchises;
    
    if((franchise.uid > 0) || (franchise.uid<0)){
        searchedFranchises = [[ZMAssetsLibrary defaultLibrary] groups];
    }
    else {
        searchedFranchises = [[ZMAssetsLibrary defaultLibrary] groupsWithRelatedUID:0];
    }
    return searchedFranchises;
}


- (void)displayBrowse {
    
//    if (!browseMenu.menuIsDisplayed) {
//
//        NSString *franchiseName = [self getFranchiseName];
//
//        if ([franchiseName isEqualToString:@"In House"]) {
//            franchiseName = @"Home Office";
//        }
//        browseMenu = [ZMBrowseMenu initMenu];
//        browseMenu.library = self;
//        browseMenu.franchiseName = franchiseName;
//        browseMenu.delegate = self;
//        [browseMenu displayMenu:^{
//            _isBrowseMenuActive = YES;
//        }];
//        [self.view addSubview:browseMenu.view];
//    }
    
}

//Revamp(4/12) - New Parm
/**
 *  Updates the header information with the given logged user
 */
- (void)updateHeader: (ZMHeader*) headerToUpdateView{
    //AOFKit Implementation
    headerToUpdateView.userName.text = [NSString stringWithFormat:@"%@", [AOFLoginManager sharedInstance].displayName];
    
    headerToUpdateView.userFranchise.text = [NSString stringWithFormat:@"%@", [AOFLoginManager sharedInstance].salesFranchise];
    
    if([headerToUpdateView.userFranchise.text isEqualToString:@""])
        headerToUpdateView.userFranchise.text = @"In House";
    
//    UIApplication.sharedApplication.statusBarHidden = NO;
    //Revamp(1/12) - Change header color
    self.statusBar = [[UIView alloc]init];
    if (@available(iOS 13.0, *)) {
        self.statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
    }
    else
    {
        self.statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    }
    //Revamp(14/12)- check for login page
    NSString *firstTimeCheck = [[NSUserDefaults standardUserDefaults] objectForKey:@"First_Time_Check"];
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"First_Time_Check"] || firstTimeCheck.length <= 0) {
        if([headerToUpdateView.userFranchise.text  isEqual: @"In House"] || [headerToUpdateView.userFranchise.text  isEqual: @"Home Office"] ) {
       // if([franchiseName isEqual: @"In House"] || [franchiseName isEqual: @"Home Office"])
        //{
            [AbbvieLogging logInfo:@"is in house"];
            [ZMUserActions sharedInstance].isFieldUser = NO;
            [theme styleHOUser:self.statusBar];
            [theme styleHOUser:headerToUpdateView.UserInformation];
            
            //Revamp(7/12) - Update logo
            [headerToUpdateView updateProntoLogo];
            //End of Revamp
        } else {

            [AbbvieLogging logInfo:@"field user"];
            [ZMUserActions sharedInstance].isFieldUser = YES;
            if(![headerToUpdateView.userFranchise.text  isEqual: @""]) {
                [theme styleFGUser:self.statusBar franchise:headerToUpdateView.userFranchise.text colorCode:[ZMUserActions sharedInstance].franchiseColor];
                [theme styleFGUser:headerToUpdateView.UserInformation franchise:headerToUpdateView.userFranchise.text colorCode:[ZMUserActions sharedInstance].franchiseColor];
            }
            else {
                [theme styleHeader:self.statusBar];
                [theme styleHeader:headerToUpdateView.UserInformation];
            }
            //Revamp(7/12) - Update logo
            [headerToUpdateView updateProntoLogo];
            //End of Revamp
        }
    }
    else {
        [theme styleHeader:self.statusBar];
        [theme styleHeader:headerToUpdateView.UserInformation];
        [headerToUpdateView updateProntoLogo];
    }
    [[UIApplication sharedApplication].keyWindow addSubview:self.statusBar];
    //End of Revamp
    //AOFKit Implementation
    headerToUpdateView.displayName = [AOFLoginManager sharedInstance].displayName;
    
    if (headerToUpdateView.tabBarTag == 0) {
        [headerToUpdateView addBreadCumb:[ZMProntoManager sharedInstance].franchisesToUpdateHeader total: _actions.assetsCount];
    }
    
    [sortButton addTarget: headerToUpdateView
                   action: @selector(sort:)
         forControlEvents: UIControlEventTouchUpInside];
    
    
    if(![ZMUserActions sharedInstance].searchCompletion && headerToUpdateView.searchTextbox.text.length >=3 && [ZMUserActions sharedInstance].isServiceHitted)
    {
        
        [ZMUserActions sharedInstance].searchCompletion = NO;
        [headerToUpdateView showingIndicator];
        
    }
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"assets updated count:%d",_actions.assetsCount]];
    [self performSelector:@selector(checkCompletionStatus) withObject:nil afterDelay:3.0];
    
    // if the search has been completed and no results found
    if(_actions.assetsCount == 0 && headerToUpdateView.searchTextbox.text.length >= 3)
    {
        [AbbvieLogging logInfo:@"no record alert!"];
        [self performSelector:@selector(checkStatus) withObject:nil afterDelay:0.5];
    }
    
    
    // PRONTO-11 - Button Visibility not Consistent
    [self updateSortButton];
    [headerToUpdateView setHeaderState];
    
}

-(void)adjustStatusBar
{
    CGRect frame = self.statusBar.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    self.statusBar.frame = frame;
}

-(void) checkCompletionStatus
{
    [AbbvieLogging logInfo:@"completing"];
    
    
    if(([ZMUserActions sharedInstance].isFinalSearch && [ZMUserActions sharedInstance].searchCompletion) || ((_apiManager.totalPages == _apiManager._currentPage) && headerView.isShowingIndicator))
    {
        [headerView hidingIndicator];
        
    }
    else if(isBackendApp && headerView.isShowingIndicator && _apiManager._syncingCompletion)
    {
        [headerView hidingIndicator];
        isBackendApp = NO;
    }
    else
    {
        [self performSelector:@selector(removeLoader) withObject:nil afterDelay:10.0];
    }
    
    
    if(!_apiManager.isConnectionAvailable)
        [headerView hidingIndicator];
    
}


-(void) removeLoader
{
    [headerView hidingIndicator];
   // [mainDelegate showHideOverlay:YES];
    
}


-(void) checkStatus
{
    if([ZMUserActions sharedInstance].searchCompletion)
    {
        
        [self loadLibraryEmpty];
        [AbbvieLogging logInfo:@"search completion"];
        [self displayEmpty:NO];
        
        [ZMUserActions sharedInstance].searchCompletion = YES;
        _apiManager._syncingInProgress = NO;
        
        [headerView hidingIndicator];
        
    }
    
}


/**
 *  This method updates the label on the sort action acordingly to user selection
 */
- (void)updateSortButton {
    
    // New Search Implementation
    NSString *sortLabel = @"View Count";
    
    NSString* selectedValue = [ZMUserActions sharedInstance].selectedSortByMenu;
    NSString* gettingValue = @"";
    
    if([ZMProntoManager sharedInstance].sortBy.count > 0)
    {
        for (NSDictionary *sort in [ZMProntoManager sharedInstance].sortBy) {
            if ([[sort objectForKey:@"selected"] intValue] == 1) {
                sortLabel = [NSString stringWithFormat:@"Sort by: %@ ", [sort objectForKey:@"label"]];
                
                gettingValue = [sort objectForKey:@"label"];
            }
        }
        
    }
    else
    {
        for (NSDictionary *sort in _actions.sorts) {
            
            if ([[sort objectForKey:@"selected"] intValue] == 1) {
                sortLabel = [NSString stringWithFormat:@"%@ ", [sort objectForKey:@"label"]];
                
                gettingValue = [sort objectForKey:@"label"];
            }
        }
    }
    
    
    if(![selectedValue isEqualToString:gettingValue])
    {
        if(headerView.isResetSelected)
        {
            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
            headerView.isResetSelected = NO;
            
        }
        else if(headerView.isResetFranchiseSelected)
        {
            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
            headerView.isResetFranchiseSelected = NO;
            
        }
        else
        {
            sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
            headerView.isResetSelected = NO;
            
        }
    }
    else if([selectedValue isEqualToString:gettingValue])
    {
        sortLabel = [NSString stringWithFormat:@"%@ ", selectedValue];
        
    }
    else
    {
        sortLabel = [NSString stringWithFormat:@"%@ ", gettingValue];
        
    }
    
    [headerView formatButton:@"()" withText:sortLabel andButton:self.sortButton];
}

/**
 *  This method updates the label on the category action acordingly to user selection
 */
- (void)updateCategoryButton {
    
    NSMutableString *categoryLabel = [[NSMutableString alloc] init];
    [categoryLabel appendString:@"Category: "];
    
    NSArray * data = [ZMProntoManager sharedInstance].selectedCategoriesToUpdateHeader;
    NSArray *filtered = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(selected == %@)", @"1"]];
    
    if (data.count == 0) {
        categoryLabel = [[NSMutableString alloc] initWithString:@"Category:  All Selected"];
    }else{
        if (filtered.count == 1){
            //Changes for Warnings
//            NSDictionary * category = [[NSDictionary alloc]init];
//            category = filtered[0];
             NSDictionary * category = filtered[0];
            if ([[category valueForKey:@"name"] isEqualToString:@"All Franchise Assets"]) {
                if ([[category valueForKey:@"selected" ] isEqualToString:@"1"]) {
                    categoryLabel = [[NSMutableString alloc] initWithString:@"Category: All Selected"];
                }else{
                    categoryLabel = [[NSMutableString alloc] initWithString:@"Category: None Selected"];
                }
            }else{
                [categoryLabel appendString:[NSString stringWithFormat:@"%@ ", [category objectForKey:@"name"]]];
            }
        }else if (filtered.count > 0){
            categoryLabel = [[NSMutableString alloc] initWithString:@"Category: Multiple Selection"];
        }else{
            categoryLabel = [[NSMutableString alloc] initWithString:@"Category:  None Selected"];
        }
    }
    
    [headerView formatButton:@"(Category:)" withText:[NSString stringWithString:categoryLabel] andButton:headerView.categoryButton];
    
}

- (void)resetPopOvers{
    _resestFranchises = YES;
    [headerView resetHeader];
}

- (NSArray *)defaultFranchise:(NSString *)franchiseName {
    /**
     *  Logic added to segment the in house user
     */
    
    
    if ([franchiseName isEqualToString:@"In House"]) {
        franchiseName = @"Home Office";
    }

    NSArray * franchiseArray = [[ZMCDManager sharedInstance] getFranchiseByName:franchiseName];
    NSMutableArray * defaultFranchise = [[NSMutableArray alloc]init];
    if (franchiseArray.count > 0) {
        
        Franchise * franchise = franchiseArray.firstObject;
        if(franchise.field_franchise_color.length > 0)
        {
            [ZMUserActions sharedInstance].franchiseColor = franchise.field_franchise_color;
        }
        else
        {
            [ZMUserActions sharedInstance].franchiseColor = @"0d1c49";
            franchise.field_franchise_color = [ZMUserActions sharedInstance].franchiseColor;
        }
        
        for (Brand * brand in franchise.brands)
        {
            if (brand.brandID != nil)
            {
                [defaultFranchise addObject:brand.brandID];
            }
        }
        
        if (franchise.franchiseID != nil)
        {
            [defaultFranchise addObject:franchise.franchiseID];
        }
    }
    
    /**
     * @Filter
     * Default
     **/
    if ([franchiseArray count] > 0) {
        
        NSString *subcategory = @"default|";
        Franchise * franchise = franchiseArray.firstObject;
        // Get the Brands Array
        _brandsArray = [[NSMutableArray alloc] init];
        
        for (Brand *brand in franchise.brands) {
            
            if (brand.name != nil)
            {
                [_brandsArray addObject:brand.name];
                subcategory = [subcategory stringByAppendingFormat:@"%@,", brand.name];
            }
        }
        subcategory = [subcategory substringToIndex:[subcategory length]-1]; //Remove the last coma
        
        [ZMTracking trackSection:@"library"
                  withSubsection:@"filter"
                        withName:franchise.name
                     withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"scfilteraction":@"default",
                                                                                   @"scfiltercategory":@"default|franchise:brand",
                                                                                   @"scsubcategory":subcategory,
                                                                                   @"scsortaction":@"most recent",
                                                                                   @"scsortcategory":@"all",
                                                                                   @"scfiltercout":[NSString stringWithFormat:@"%d", _actions.assetsCount],
                                                                                   @"sctierlevel":@"tier2"}]];
    }
    
    // brandsArray count - only when there are more than 1 brand
    if(_brandsArray.count>1 && franchiseArray.count > 0 )
    {
        [ZMUserActions sharedInstance].brandsArray = _brandsArray;
        [ZMUserActions sharedInstance].brandsSelected = [NSMutableArray arrayWithCapacity:_brandsArray.count];
    }
    else
    {
        [ZMUserActions sharedInstance].brandsArray = @[];
    }
    
    return defaultFranchise;
}


/**
 *  Get franchises to update header, these franchises will getting with franchise by default
 *
 *  @param franchiseName franchise name
 *
 *  @return Array to franchises to update header
 */
- (NSArray*)franchiseToUpdateHeader:(NSString *)franchiseName {
    /**
     *  Logic added to segment the in house user
     */
    if ([franchiseName isEqualToString:@"In House"]) {
        franchiseName = @"Home Office";
    }
    
    NSMutableArray * franchisesTuUpdateHeader = [[NSMutableArray alloc]init];
    NSArray * franchiseArray = [[ZMCDManager sharedInstance] getFranchiseByName:franchiseName];
    
    if (franchiseArray.count > 0) {
        Franchise * franchise = franchiseArray.firstObject;
        NSMutableArray * brandsInsideFranchise = [[NSMutableArray alloc]init];
        
        for (Brand * brand in franchise.brands)
        {
            NSMutableDictionary * brandNode = [[NSMutableDictionary alloc]init];
            [brandNode setObjectIfValueAvailable:brand.brandID forKey:@"id"];
            [brandNode setObjectIfValueAvailable:brand.name forKey:@"name"];
            [brandNode setObjectIfValueAvailable:@"1" forKey:@"selected"];
            [brandsInsideFranchise addObject:brandNode];
        }
        
        NSMutableDictionary * franchiseNode = [[NSMutableDictionary alloc]init];
        [franchiseNode setObjectIfValueAvailable:franchise.franchiseID forKey:@"id"];
        [franchiseNode setObjectIfValueAvailable:franchise.name forKey:@"name"];
        [franchiseNode setObjectIfValueAvailable:@"1" forKey:@"selected"];
        [franchiseNode setObjectIfValueAvailable:brandsInsideFranchise forKey:@"brands"];
        
        [franchisesTuUpdateHeader addObject:franchiseNode];
    }
    return franchisesTuUpdateHeader;
    
}

//Sets all the default values
- (void)backToDefault {
    
    // PRONTO-31 - Optimization of Deep Linking Feature
    
    if( [ZMUserActions sharedInstance].checkURLSchema == NO)
    {
        
        _actions.sorts = [self getSortArray];
        _actions.franchisesAndBrands = [self defaultFranchise:[self getFranchiseName]];
        _actions.isDefault = YES;
        _actions.isAllCategorySelected = YES;
        _pageIndex = 0;
        
        if (_resestFranchises) {
            _actions.allFranchisesChecked = NO;
            
            /**
             *  When the app is runing at first time, the franchiseAndBrand Collection gets empty, so, we need fill it with the default franchise.
             */
            
            if ([[ZMProntoManager sharedInstance]getFranchiseAndBrandsCollectionCount] == 0 ) {
                [[ZMCDManager sharedInstance]setFranchiseAndBrandsCollection:[self getFranchiseName]];
            }
            
            [ZMProntoManager sharedInstance].franchisesAndBrands = [self defaultFranchise:[self getFranchiseName]];
            [ZMProntoManager sharedInstance].franchisesToUpdateHeader =  [self franchiseToUpdateHeader:[self getFranchiseName]];
            __weak typeof (self) weakSelf = self;
            [[ZMCDManager sharedInstance]getActiveCategories:^(NSArray * data) {
                [ZMProntoManager sharedInstance].categoriesToFilter = data;
                [weakSelf.leftMenuTableView reloadData];
                [weakSelf updateHeader:headerView];
                [weakSelf refreshLibrary];
                
            }];
        }else{
            [self updateHeader:headerView];
            [self refreshLibrary];
        }

    }
    else
    {
        
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
        {
            //Do checking here.
            [self updateHeader:headerView];
            [self updateSortButton];
            [self refreshLibrary];
            //
        }
        else
        {
            _actions.sorts = [self getSortArray];
            _actions.franchisesAndBrands = [self defaultFranchise:[self getFranchiseName]];
            _actions.isDefault = YES;
            _actions.isAllCategorySelected = YES;
            _pageIndex = 0;
            
            if (_resestFranchises) {
                _actions.allFranchisesChecked = NO;
                
                //
                // When the app is runing at first time, the franchiseAndBrand Collection gets empty, so, we need fill it with the default franchise.
                //
                
                if ([[ZMProntoManager sharedInstance]getFranchiseAndBrandsCollectionCount] == 0 ) {
                    [[ZMCDManager sharedInstance]setFranchiseAndBrandsCollection:[self getFranchiseName]];
                }
                
                [ZMProntoManager sharedInstance].franchisesAndBrands = [self defaultFranchise:[self getFranchiseName]];
                [ZMProntoManager sharedInstance].franchisesToUpdateHeader =  [self franchiseToUpdateHeader:[self getFranchiseName]];
                [[ZMCDManager sharedInstance]getActiveCategories:^(NSArray * data) {
                    [ZMProntoManager sharedInstance].categoriesToFilter = data;
                    
                    [self refreshLibrary];
                    [self updateSortButton];
                    [self updateHeader:headerView];
                }];
                
                
            }
            
            
        }
        
    }
    
    if([ZMProntoManager sharedInstance].categoriesToFilter.count>1)
    {
        [_leftMenuTableView reloadData];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        promotedNoneMessageDisplayed = false;
        if([[[ProntoUserDefaults singleton]isReadyToMakeServiceCalls]isEqualToString:@"YES"])
            [self displayPromotedContent:@""];
    });

    //Updating the View as per saved preferences.
    [self performSelector:@selector(updateViewOnBackToDefault) withObject:nil afterDelay:.3];
}

- (void)updateViewOnBackToDefault
{
    if(headerView.userFranchise.text.length > 0)
    {
        if([headerView.userFranchise.text  isEqual: @"In House"] ||
           [headerView.userFranchise.text  isEqual: @"Home Office"])
        {
            [self updateFranchieseViewFromPreferences:nil];
        }
    }
}

/**
 *  Check popular Asset into CMS, each time app is launched, get popular asset.
 *
 *  @param complete success completed
 */
- (void)getPopularAsset:(void(^)(void))complete {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [_apiManager getPopularAsset:^(NSDictionary *popularAsset) {
            
            if (popularAsset.count > 0) {
                
                for (NSString *asset in popularAsset) {
                    
                    if (![asset isEqualToString:@""]) {
                        mostPopularAsset = [asset intValue];
                        [[ZMAssetsLibrary defaultLibrary] setProperty:@"POPULAR_ASSET" value:[NSString stringWithFormat:@"%ld", mostPopularAsset]];
                        
                        if (popularAsset.count > 0){
                            
                            NSNumber * assetId = [NSNumber numberWithLong:mostPopularAsset];
                            NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
                            if (assetArray.count > 0) {
                                Asset *asset = assetArray[0];
                                asset.popular = [NSNumber numberWithBool:YES];
                                [[ZMCDManager sharedInstance]updateAsset:asset success:^{
                                    complete();
                                } error:^(NSError * error) {
                                    [AbbvieLogging logError:[NSString stringWithFormat:@"Error at updating most popular asset %@", error.localizedDescription]];
                                }];
                            }
                        }
                    }
                }
                complete();
            }else{
                complete();
            }
            
        } error:^(NSError *error) {
            [AbbvieLogging logError:@"Error getting the most popular asset"];
        }];
    });
}


/**
 *  @private
 *  Mark the asset as read
 *  @param selectedAsset
 */
- (void)markAssetAsRead:(Asset *) selectedAsset {
    
    
    ZMGridItem *cellView;
    for (ZMGridItem *cell in [grid visibleCells]) {
        if (selectedAsset.assetID.longValue == cell.uid) {
            cellView = cell;
            break;
        }
    }
    cellView.totalViews = selectedAsset.view_count.intValue;
    cellView.read = YES;
}

/*!
 * New public method to mark as liked or unliked an asset in real time
 */
- (void)markLiked:(Asset *)selectedAsset type:(NSInteger)type
{
    //If selectedAsset == nil, There is nothing to track
    if (selectedAsset == nil || selectedAsset.assetID == nil) { return; }
    
    NSString *currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
    NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:selectedAsset.assetID];
    
    [self.assetsPage enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        Asset *asset = obj;
        if ([asset isKindOfClass:[Asset class]])
        {
            if (asset.assetID.longLongValue == selectedAsset.assetID.longValue)
            {
                [self.assetsPage replaceObjectAtIndex:idx withObject:selectedAsset];
            }
        }
    }];
    NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
    NSMutableArray *favArray = [[NSMutableArray alloc]init];
    NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
    
    if (favoritefranchise.count == 0 && type == ZMEventLike) {
        [favArray addObject: selectedAsset.assetID];
        [favFranchiseObject setObject: selectedAsset.assetID forKey:@"asset_ID"];
        [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionAsString] forKey:@"bucketName"];
        [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];

        if (favfranchise.count > 0)
        {
            FavFranchise *favFranchise = favfranchise[0];
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
            favFranchise.bucketName = modifiedFranchiseBucketName;
            [_actions updatePersistentStoreWithCurrentContext];
        }
        else
        {
            [_actions insertNewFavouriteFranchise:favFranchiseObject complete:^() {}
                                            error:^(NSError *error) {
                                            }];
        }
        
    } else {
        int count = 0;
        for(FavFranchise *fav in favoritefranchise) {
            if([fav.asset_ID longValue] !=  selectedAsset.assetID.longValue && type == ZMEventLike) {
                count += 1;
            }
            else if ([fav.asset_ID longValue] ==  selectedAsset.assetID.longValue) {
                break;
            }
        }
        if(count == favoritefranchise.count) {
            [favArray addObject: selectedAsset.assetID];
            [favFranchiseObject setObject:  selectedAsset.assetID forKey:@"asset_ID"];
            [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionAsString] forKey:@"bucketName"];
//            [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];
            [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]
                                   forKey:@"bucketID"];
            
            if (favfranchise.count > 0)
            {
                FavFranchise *favFranchise = favfranchise[0];
                NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
                favFranchise.bucketName = modifiedFranchiseBucketName;
                [_actions updatePersistentStoreWithCurrentContext];
            }
            else
            {
                [_actions insertNewFavouriteFranchise:favFranchiseObject complete:^() {}
                                                error:^(NSError *error) {
                                                }];
            }
        }
    }
    ZMProntoManager.sharedInstance.favArrayIds = favArray;
    
    for(FavFranchise *fav in favoritefranchise) {
        if([fav.asset_ID longValue] == selectedAsset.assetID.longValue && type == ZMEventUnlike) {
            NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById: selectedAsset.assetID];
            if (favfranchise.count > 0) {
                
                FavFranchise *favFranchise = favfranchise.firstObject ;
                NSString *franchiseBucketName = favFranchise.bucketName;
                NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameFromBucket:franchiseBucketName byRemovingBucketName:currentBucketName];
                if (modifiedFranchiseBucketName == nil)
                {
                    [_actions deleteFavFranchise:favfranchise.firstObject complete:^{
                    } error:^(NSError *error) {
                    }];
                }
                else
                {
                    favFranchise.bucketName = modifiedFranchiseBucketName;
                    [_actions updatePersistentStoreWithCurrentContext];
                }
                
                // update the count
                if(_actions.assetsCount >= 1 && ([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kFranchiseFavorites]))
                {
                    _actions.assetsCount = _actions.assetsCount-1;
                     [headerView addBreadCumb:[ZMProntoManager sharedInstance].franchisesToUpdateHeader total: _actions.assetsCount];
                }
            } else {
                [AbbvieLogging logInfo:@"favfranchise array is empty"];
            }
           
        }
    }
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"fav franchise:%@",[[ZMCDManager sharedInstance] getFavFranchise]]];
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [_leftMenuTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation: UITableViewRowAnimationNone];
    if (grid.hidden)
    {
        [listTableView reloadDataWithCompletion:^{
            [self updateFavouriteAndLoadLibrary];
        }];
    }
    else
    {
        [grid reloadDataWithCompletion:^{
            [self updateFavouriteAndLoadLibrary];
        }];
    }
}

- (void)updateFavouriteAndLoadLibrary
{
    NSArray *favfranchiseReload = [[ZMCDManager sharedInstance] getFavFranchise];
    NSMutableArray *favArrayReload = [[NSMutableArray alloc]init];
    for(FavFranchise *fav in favfranchiseReload) {
        [favArrayReload addObject:fav.asset_ID];
    }
    ZMProntoManager.sharedInstance.favArrayIds = favArrayReload;
    ZMProntoManager.sharedInstance.assetsIdToFilter = favArrayReload;
    [self loadMyLibrary];
}

/**
 *  trackGetAsset: make traking to event
 *
 *  @param asset asset to track
 */
- (void)trackGetAsset:(Asset *) asset {
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    
    if (![asset.path isEqualToString:@""] || [[ZMAssetViewController getAssetType:asset] isEqualToString:@"video"]) {
        [trackingOptions setObject:[NSString stringWithFormat:@"newpanel:assetview:%@",[ZMAssetViewController getAssetType:asset]] forKey:@"prop32"];
        [trackingOptions setObject:[NSString stringWithFormat:@"newpanel:assetview:%@",[ZMAssetViewController getAssetType:asset]] forKey:@"eVar32"];
        [trackingOptions setObject:@"event52" forKey:@"events"];
        [ZMTracking trackSection:@"scscreenname" withSubsection:@"newpanel-assetview" withName:[NSString stringWithFormat:@"%ld-%@",asset.assetID.longValue,asset.title] withOptions:trackingOptions];
    }
}

//Download of an asset if not found in coredata / new

- (void) downloadSelectedAsset:(Asset *)selected {
    _isTapDownloadInitiated = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [mainDelegate showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    });
    [AbbvieLogging logInfo:@"------Asset needs download ------"];
    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    if(![selected.file_mime isEqualToString:@"weblink"])
        [self downloadAsset:selected];
}

/**
 *  With the given asset validates if the assets is local or initiates the download
 *  @param selectedAsset
 */
- (void)validateAsset:(Asset *)selectedAsset {
    
    // PRONTO-15 - Popup Getting Displayed Over PDF
    if(headerView.categoryPopOver)
        [[headerView.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    
    // removing deprecated warnings
    if(headerView.sortMenuPopOver)
        [[headerView.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    
    
    [AbbvieLogging logInfo:@"*****Intro validate Asset ********"];
    [self validateAssetBeforeOpen:selectedAsset completion:^(BOOL assetNeedsDownload) {
        if (assetNeedsDownload) {
            //Restricting user from opening asset if offline download is in progress
            NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:selectedAsset.assetID];
            for(OfflineDownload * data in offlineDownloadArray) {
                if(data.assetID == selectedAsset.assetID) {
                    if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                             [self.mainDelegate showMessage:@"Asset download in progress. Please wait and then try again." whithType:ZMProntoMessagesTypeWarning];
                            //                             if(![selectedAsset.file_mime isEqualToString:@"weblink"])
                            //                                 [self downloadAsset:selectedAsset];
                            if(!_isTapDownloadInitiated)
                            {
                                [self downloadSelectedAsset:selectedAsset];
                                [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                            }
                        });
//                        if(_apiManager.tappedAsset == 0) {
//                            _apiManager.tappedAsset = data.assetID;
//                        }
                        _isDownloadignAsset = NO;
                    }
                    else {
                        if(!_isTapDownloadInitiated)
                        {
                            [self downloadSelectedAsset:selectedAsset];
                            [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                        }
                    }
                }
            }
            if(offlineDownloadArray.count == 0) {
                if(!_isTapDownloadInitiated)
                {
                    [self downloadSelectedAsset:selectedAsset];
                    [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                }
//                _isDownloadignAsset = NO;
            }
        }else
        {
            if(self.currentSelectedAssetToSeeDetail)
            {
                //Return in case if open of any asset is in progress
                return;
            }
            else
            {
                self.currentSelectedAssetToSeeDetail = selectedAsset;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                    selectedAsset.isUpdateAvailable = NO;
                    // commenting this as the folders feature is not needed for revamp
                    // PRONTO-88 - Backlog issue - Asset selection not highlighting the Folder added
                    /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     ZMAbbvieAPI* apiManager = [ZMAbbvieAPI sharedAPIManager];
                     
                     dispatch_group_t syncGroup = dispatch_group_create();
                     
                     dispatch_group_enter(syncGroup);
                     [apiManager synchronizeFolders:^{
                     dispatch_group_leave(syncGroup);
                     } error:^(NSError *error) {
                     apiManager._syncingInProgress = NO;
                     apiManager.lastServerCodeResponse = (int)error.code;
                     [[ZMAssetsLibrary defaultLibrary] notifyError:[error localizedDescription]];
                     dispatch_group_leave(syncGroup);
                     }];
                     
                     
                     });*/
                    
                });
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [AbbvieLogging logInfo:@"------Asset needs opened ------"];
                    [self openAsset:selectedAsset];
                    promotedTappedForAudio = false;
                });
            }
        }
    } failure:^(NSString *messageFailure) {
        [mainDelegate showMessage:messageFailure whithType:ZMProntoMessagesTypeWarning];
    }];
}

/**
 *  validateAssetBeforeOpen : Check if the assets is local or initiates the download
 *
 *  @param asset      asset to validate
 *  @param completion block completion to indicate if asset is valid, these return indicator know if asset needs download or not
 *  @param failure    block failure completion to indicate something was wrong
 */

-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
    [self trackGetAsset:asset];
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
    
    if (!_isDownloadignAsset) {
        _isDownloadignAsset = YES;
        // check if asset is protected
        if (![UIApplication sharedApplication].protectedDataAvailable) {
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
            /// This should occur during application:didFinishLaunchingWithOptions:
            //[ASFFileProtectionManager stopProtectingLocation:asset.path];
            
            // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
            NSError *error;
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];
            
            
            if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
            {
                attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
                if (!success)
                    [AbbvieLogging logInfo:@"Set ~/Documents attrsAssetPath NOT successfull"];
            }
            
            /// ....
            NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
            [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
        }
        
        // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        if([[NSFileManager defaultManager] fileExistsAtPath:asset.path]) {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"File Exists at %@", asset.path]];
            
        }
        
        if ((![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && assetType != ZMProntoFileTypesBrightcove) && (assetType != ZMProntoFileTypesWeblink))
        {
            if ([_apiManager isConnectionAvailable]) {
                //                _isDownloadignAsset = YES;
                completion(YES);
            }
            else {
                _isDownloadignAsset = NO;
                failure(@"The internet connection is not available, please check your network connectivity");
            }
        }else{
            //Check if the asset size matches the metada info meaning the asset is correct
            if (asset.file_size.intValue == (int)fileSize) {
                //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
                _isDownloadignAsset = NO;
                completion(NO);
            }else{
                completion(YES);
            }
        }
    } else {
        // PRONTO-16 - Opening an Asset in AirPlane Mode
        if (![_apiManager isConnectionAvailable]) {
            
            if(_isDownloadignAsset == NO)
            {
                
                failure(@"The internet connection is not available, please check your network connectivity");
                
                _isDownloadignAsset = NO;
            }
            
        }
        else
            failure(@"Download in progress. Please wait and then try again.");
    }
}

/**
 *  Open selected asset, the asset could be of type PDF-VIDEO-DOCUMENT,the assets of type PDF or document will be open through FastPDFKIT, the ones of type Video thorugh Brightcove
 *
 *  @param selectedAsset Asset to open
 */

- (void)openAsset:(Asset *)selectedAsset
{
    if([selectedAsset.type isEqualToString:[EventOnlyAsset TypeAttributeValueAsString]])
    {
        EventOnlyAsset* eoaAsset = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:[selectedAsset.assetID stringValue]];
        [self eventOnlyAssetHandler:nil openAsset:eoaAsset];
    }
    else
    {
        //If Selected path or Selected path is not present then asset can't be opened.
        if (selectedAsset == nil || selectedAsset.path == nil) { return ;}
        
        // PRONTO-15 - Popup Getting Displayed Over PDF
        if(headerView.categoryPopOver)
        [[headerView.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        
        // removing deprecated warnings
        if(headerView.sortMenuPopOver)
        [[headerView.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        
        
        _resestFranchises = NO;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [mainDelegate hideMessage];
        });
        
        enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
        
        _isDownloadignAsset = NO;
        
        /*!
         * ABT-51
         */
        //enum ZMProntoFileTypes types = [self getAsset:selectedAsset];
        /*NSString *activityType = @"Pronto Object";
        if (assetType == ZMProntoFileTypesPDF) {
            activityType = @"PDF";
        }
        else if (assetType == ZMProntoFileTypesVideo) {
            activityType = @"VIDEO";
        }
        else if (assetType == ZMProntoFileTypesBrightcove) {
            activityType = @"VIDEO";
        }
        else if (assetType == ZMProntoFileTypesDocument) {
            activityType = @"DOCUMENT";
        }
        else if (assetType == ZMProntoFileTypesEpub) {
            activityType = @"EPUB";
        }
        else if(assetType == ZMProntoFileTypesWeblink) // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        {
            activityType = @"WEBLINK";
        }*/
        
        //Tracking
//        [ASFActivityLog logActionForCurrentUser:@"viewed" withObjectName:[NSString stringWithFormat:@"%@",selectedAsset.title] ofType:[NSString stringWithFormat:@"%@",activityType] completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
//            if (!result){
//                //Ignore Errors
//            }
//        }];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UIViewController <ZMAssetViewControllerDelegate> *controller;
        
        MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
        if (assetType == ZMProntoFileTypesPDF) {
            if(document != nil)
            {
                [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
            }
            else{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                    self.currentSelectedAssetToSeeDetail = nil;
                });
            }
        }
        else if (assetType == ZMProntoFileTypesBrightcove) {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
//            controller.library = self;
            controller.asset = selectedAsset;
            controller.userFranchise = headerView.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
            if(self.navigationController == nil)
            {
                [self getNavigationOfAppDelegate:controller];
            }
            else
            {
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        else if (assetType == ZMProntoFileTypesDocument) {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
//            controller.library = self;
            controller.asset = selectedAsset;
            controller.userFranchise = headerView.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
            if(self.navigationController == nil)
            {
                [self getNavigationOfAppDelegate:controller];
            }
            else
            {
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        // PRONTO-25 Web View - Ability to provide a Web link as an asset type.
        //
        //
        else if(assetType == ZMProntoFileTypesWeblink)
        {
            // open the weblink URL
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
//            controller.library = self;
            controller.asset = selectedAsset;
            controller.userFranchise = headerView.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
            if(self.navigationController == nil)
            {
                [self getNavigationOfAppDelegate:controller];
            }
            else
            {
                [self.navigationController pushViewController:controller animated:YES];
            }
            
        }
        else if(assetType == ZMProntoFileTypesAudio)
        {
            [self stopAudio];
            assetForAudio = selectedAsset;
            [AudioPlayer.sharedInstance setAssetWithAssetVal:selectedAsset];
            if(!audioPlayerView)
            {
                audioPlayerView = (AudioPlayer*)[[[NSBundle mainBundle] loadNibNamed:@"AudioPlayerView" owner:self options:nil] firstObject];
            }
            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@",assetForAudio.assetID];
            [audioPlayerView initialSetupWithAssetVal:selectedAsset];
            self.currentSelectedAssetToSeeDetail = nil;
            if(promotedTappedForAudio == true)
            {
                if(promotedGrid.hidden == false)
                {
                    NSArray * cells1 = [[promotedGrid visibleCells]filteredArrayUsingPredicate:predicate];
                    if (cells1.count > 0) {
                        audioCurrentPromotedGridCell = cells1.firstObject;
                        promotedTappedForAudio = false;
                        audioPlayerView.frame = CGRectMake(0, 0,audioCurrentPromotedGridCell.audioPlayerView.frame.size.width, audioCurrentPromotedGridCell.audioPlayerView.frame.size.height);
                        [audioPlayerView.layer setMasksToBounds:YES];
//                        [audioPlayerView initialSetupWithAssetVal:selectedAsset];
                        audioCurrentPromotedGridCell.audioPlayerView.hidden = NO;
                        
//                        self.currentSelectedAssetToSeeDetail = nil;
                        
                        [audioCurrentPromotedGridCell.audioPlayerView addSubview:audioPlayerView];
                        //                    return;
                    }
                }
            }
            else
            {
                if (grid.hidden)
                {
                    //                NSPredicate * predicateForList = [NSPredicate predicateWithFormat:@"uid == %@",assetForAudio.assetID];
                    NSArray * cellsForList = [[listTableView visibleCells]filteredArrayUsingPredicate:predicate];
                    if (cellsForList.count > 0)
                    {
                        audioCurrentCell = cellsForList.firstObject;
                        audioPlayerView.frame = CGRectMake(0, 0,audioCurrentCell.audioPlayerView.frame.size.width, audioCurrentCell.audioPlayerView.frame.size.height);
                        [audioPlayerView.layer setMasksToBounds:YES];
//                        [audioPlayerView initialSetupWithAssetVal:selectedAsset];
                        audioCurrentCell.audioPlayerView.hidden = NO;
//                        self.currentSelectedAssetToSeeDetail = nil;
                        [audioCurrentCell.audioPlayerView addSubview:audioPlayerView];
                    }
                }
                else
                {
                    NSArray * cells = [[grid visibleCells]filteredArrayUsingPredicate:predicate];
                    if (cells.count > 0) {
                        audioCurrentGridCell = cells.firstObject;
                        
                        audioPlayerView.frame = CGRectMake(0, 0,audioCurrentGridCell.audioPlayerView.frame.size.width, audioCurrentGridCell.audioPlayerView.frame.size.height);
                        [audioPlayerView.layer setMasksToBounds:YES];
//                        [audioPlayerView initialSetupWithAssetVal:selectedAsset];
                        audioCurrentGridCell.audioPlayerView.hidden = NO;
                        
//                        self.currentSelectedAssetToSeeDetail = nil;
                        
                        [audioCurrentGridCell.audioPlayerView addSubview:audioPlayerView];
                        //                    return;
                    }
                }
            }
        }
        else
        {
            // Fix - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** -[NSURL initFileURLWithPath:]: nil string parameter'
            if (assetType == ZMProntoFileTypesPDF) {
                
                if(selectedAsset.path)
                {
                    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
                    if(document != nil)
                    {
                        [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
                    }else{
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                            self.currentSelectedAssetToSeeDetail = nil;
                        });
                    }
                }
            }
        }
    }
}

- (void)stopAudio
{
    if(assetForAudio != nil)
    {
        [audioPlayerView stopAudio];
        
        if(audioCurrentCell)
        {
            audioCurrentCell.audioPlayerView.hidden = YES;
            audioCurrentCell = nil;
        }
        else if(audioCurrentGridCell)
        {
            audioCurrentGridCell.audioPlayerView.hidden = YES;
            audioCurrentGridCell = nil;
        }
        else if (audioCurrentPromotedGridCell)
        {
            audioCurrentPromotedGridCell.audioPlayerView.hidden = YES;
            audioCurrentPromotedGridCell = nil;
        }
        assetForAudio = nil;
    }
}

- (void)updateOfflineDownloadCoreData:(Asset *)asset status:(NSString*)status
{
    if([asset valueForKey:@"assetID"] != nil)
    {
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
        [initialDownloadAsset setObject:status forKey:@"status"];
        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
        
        if (offlineData.count > 0)
        {
            //if  insertation and deletetion has to be happen, it will happen one by one
            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
            }];
        }
        else
        {
            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
        }
    }
}

- (void)downloadAsset:(Asset *)selectedAsset{
    
    currentAssetDownload = selectedAsset;
    _isDownloadignAsset = YES;
    [self setProgress:0.001000];
    
    [_apiManager downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        [self downloadAssetDidFinish:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"completed"];
        // New revamp changes - remove the update label once the asset is downloaded successfully.
        selectedAsset.isUpdateAvailable = NO;
        [self reloadAssets];
    } onError:^(NSError *error) {
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAssetOnFailure:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"failed"];
    }];
}

- (void)openTempAsset:(NSURL *)url
{
    
    UIViewController <ZMAssetViewControllerDelegate> *controller;
    
    MFDocumentManager *doc = [[MFDocumentManager alloc] initWithFileUrl:url];
    
    //Creating dummy asset, because asset detail view controller expects asset object
    // to render Header and tool bar view. if dummy asset didn't get provided
    // header will not be shown in asset detail screen.
    // passing nil will cause the issue.
    NSArray *items       = [url.absoluteString componentsSeparatedByString:@"/"];
    NSString *assetTitle = (NSString*)items.lastObject;
    Asset * dummyAsset   = [Asset MR_createEntity];
    dummyAsset.assetID   = [NSNumber numberWithInteger:99999];
    dummyAsset.title     = assetTitle;
    
    [self openFastPDFWithAsset:dummyAsset pdfController:controller pdfDocument:doc];
}

- (void)openFastPDFWithAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
//    controller.library = self;
    controller.asset = asset;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    //    [view addSubview:controller.view];
    
    [viewController.view addSubview:view];
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    //    controller.view.frame = view.bounds;
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
    
    //    [controller didMoveToParentViewController:viewController];
    
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor whiteColor];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]; [webview addSubview:spinner];
    spinner.center = webview.center;
    spinner.color = [UIColor grayColor];
    [viewController.view addSubview:webview];
    [spinner startAnimating];

    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    [self hideMessage];
    controller.userFranchise = headerView.userFranchise.text;
    controller.viewMain = view;
    controller.webviewScreen = webview;
    controller.webviewScreen.hidden = NO;
    
    double delayInSeconds = 8.0; // set the time
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        controller.webviewScreen.hidden = YES;
        [spinner stopAnimating];
    });
    
    //To perform push if the navigation controller is nil
    if(self.navigationController == nil)
    {
        [self getNavigationOfAppDelegate:viewController];
    }
    else
    {
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
}

/**
 *  Returns the asset type according with the formats supported by pronto
 *  @param selectedAsset given asset
 *  @return type of the asset as ZMProntoFileTypes
 */

/**
 *  Load asset into library.
 */

- (void)loadLibrary {
    
    
    [self.view endEditing:YES];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@"tier1" forKey:@"sctierlevel"];
    [ZMTracking trackSection:@"library" withSubsection:@"home" withName:nil withOptions:dict];
    
    [self hideMessage];
    
    if (libraryScrollView.contentOffset.x > 0) {
        [libraryScrollView scrollRectToVisible:CGRectMake(0, 0, libraryScrollView.frame.size.width, libraryScrollView.frame.size.height) animated:YES];
    }
    
    /** removing all items from grid */
    [self hideEmpty];
    
    [grid setContentOffset:CGPointMake(0, 0) animated:YES];
    
    headerView.folderButton.enabled = YES;
    
    _pageIndex = 0;
    
    // PRONTO-8 - Performance Search
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self refreshLibrary];
    });
    
}

/**
 *  Load briefcase getting data from Coredata
 */

- (void)loadBriefcase {
    
    [self dismissView:nil];
    
    currentCell = nil;
    _selectedIndexPath = nil;

    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@"tier1" forKey:@"sctierlevel"];
    [ZMTracking trackSection:@"mybriefcase" withSubsection:@"home" withName:nil withOptions:dict];
    
    [self hideMessage];
    
    [self showScrollSpinner];
    
    // removing all items from grid
    [self hideEmpty];
    
    self.assetsPage = [[NSMutableArray alloc] init];
    
    [grid setContentOffset:CGPointMake(0, 0) animated:YES];
    
    headerView.searchTextbox.text = @"";
    [[ZMProntoManager sharedInstance].search setTitle:@""];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSMutableArray *foldersAndAssets = [[NSMutableArray alloc] init];
        int itemsCount = 0;
        
        //New implementation with CoreData DATABASE
        
        if (!_currentFolderId || _currentFolderId == 0) {
            
            foldersAndAssets = [[NSMutableArray alloc] initWithArray:[[ZMAssetsLibrary defaultLibrary] getAllFolders:FALSE]];
            headerView.folderButton.enabled = YES;
            itemsCount = (int)[foldersAndAssets count];
            
            NSArray * assetsOnFolder = [[ZMAssetsLibrary defaultLibrary] folderAssetsIds:_currentFolderId];
            if (assetsOnFolder.count == 0) {
                [ZMProntoManager sharedInstance].assetsIdToFilter = @[@0];
            }else{
                [ZMProntoManager sharedInstance].assetsIdToFilter = assetsOnFolder;
                
            }
            
            
        }
        else {
            
            headerView.folderButton.enabled = NO;
            
            NSArray * assetsOnFolder = [[ZMAssetsLibrary defaultLibrary] folderAssetsIds:_currentFolderId];
            if (assetsOnFolder.count == 0) {
                [ZMProntoManager sharedInstance].assetsIdToFilter = @[@0];
            }else{
                [ZMProntoManager sharedInstance].assetsIdToFilter = assetsOnFolder;
                
            }
            
        }
        
        if (!_currentFolderId) {
            _currentFolderId = 0;
        }
        
        // New Search Implementation -
        [[ZMCDManager sharedInstance]getAssets:_pageIndex limit:_itemsPerPage success:^(NSArray * assets) {
            
            if([assets count] == [ZMProntoManager sharedInstance].assetsIdToFilter.count)
                assets = assets;
            else if([[ZMProntoManager sharedInstance].assetsIdToFilter  isEqual: @[@0]])
                assets = assets;
            
            
            [foldersAndAssets addObjectsFromArray:assets];
            // assetsPage = folders + asset
            self.assetsPage = foldersAndAssets;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!_currentFolderId || _currentFolderId == 0) {
                    
                    // itemsCount = folders count
                    if (!(itemsCount == [self.assetsPage count]) && (!_currentFolderId || _currentFolderId == 0)) {
                        
                        if (_apiManager._syncingInProgress) {
                            [mainDelegate showMessage:@"Please wait for the assets to load." whithType:ZMProntoMessagesTypeWarning withTime:2];
                        }
                        else {
                            // To fix the issue of "No Search Results Found." on searching the keyword
                            if(![headerView.searchTextbox.text isEqualToString:@""] && (self.assetsPage.count - itemsCount == 0)&&  (headerView.isSearchAssets == NO))
                            {
                                [mainDelegate showMessage:@"No search results found." whithType:ZMProntoMessagesTypeWarning withTime:2];
                            }
                        }
                    }
                    
                }
                else {
                    if ([headerView.searchTextbox.text isEqualToString:@""]) {
                        headerView.searchTextbox.text = @"";
                    }
                }
                
                if (self.assetsPage.count == 0) {
                    [self displayEmpty:NO];
                }
                else {
                    [self hideEmpty];
                }
                
                [self reloadAssets];
                
                [self updateHeader:headerView];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self hideScrollSpinner];
                    
                });
            });
        }];
        
        
    });
}

- (void)loadBriefcasePages {
    
    if (!_pageIndex) {
        _pageIndex = 1;
    }
    
    [[ZMCDManager sharedInstance]getAssets:_pageIndex limit:_itemsPerPage success:^(NSArray * assets) {
        
        _pageIndex++;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSArray *assetsFromFolder = assets;
            
            if (!assets) {
                [self hideScrollSpinner];
            }else if ([assetsFromFolder count] > 0) {
                [self.assetsPage addObjectsFromArray:assetsFromFolder];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hideScrollSpinner];
                    [self reloadAssets];
                });
            }
            else {
                [self hideScrollSpinner];
            }
        });
    }];
}

-(void) loadSearchedAssets
{
    /* In the advent of Global Search, Search from Tabs has been removed.
     Instead of removing it  from all places. disabling the implementation of search itself*/
}

/**
 *  Get Asset from Coredata into My Library section, getting Data by index and limit
 */

-(void)loadMyLibrary{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"**** Page Index %d",_pageIndex]];
    
    [ZMProntoManager sharedInstance].assetsIdToFilter = @[];
    
    [[ZMCDManager sharedInstance] getAssets:0 limit:-1 success:^(NSArray * totalAssets) {
        NSMutableArray *toCount = [[NSMutableArray alloc]initWithArray:totalAssets];
        _actions.assetsCount = (int)toCount.count;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateHeader:headerView];
            // for hiding the loading indicators on refreshing library
            [self.headerView hidingIndicator];
            
            
        });
    }];
    
    [[ZMCDManager sharedInstance ] getAssets:_pageIndex limit:_itemsPerPage success:^(NSArray * assets) {
        
        if (_pageIndex == 0 ) {
            self.assetsPage = [[NSMutableArray alloc] init];
            
        }
        
        presentCount = assets.count;
        
        NSMutableArray *page = [[NSMutableArray alloc]initWithArray:assets];
        
        if ([page count] > 0) {
            
            if ([self.assetsPage count] == _actions.assetsCount || grid.contentOffset.y == 0 || [self.assetsPage count] == 0) {
                [self.assetsPage addObjectsFromArray:page];
                self.assetsPage = [_actions removeDuplicatesForAssets:self.assetsPage];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self hideScrollSpinner];
                });
                
                _assetsInPage = self.assetsPage;
                _viewGrid = grid;
            }
            
            else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (grid.hidden) {
                        [listTableView beginUpdates];
                        int resultsSize = (int)[self.assetsPage count]; //data is the previous array of data
                        [self.assetsPage addObjectsFromArray:page];
                        self.assetsPage = [_actions removeDuplicatesForAssets:self.assetsPage];
                        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
                        for (int i = resultsSize; i < resultsSize + page.count; i++) {
                            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        }
                        [listTableView insertRowsAtIndexPaths: arrayWithIndexPaths withRowAnimation: UITableViewRowAnimationAutomatic];
                        [listTableView endUpdates];
                    } else {
                        [grid performBatchUpdates:^{
                            int resultsSize = (int)[self.assetsPage count]; //data is the previous array of data
                            [self.assetsPage addObjectsFromArray:page];
                            self.assetsPage = [_actions removeDuplicatesForAssets:self.assetsPage];
                            NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
                            for (int i = resultsSize; i < self.assetsPage.count; i++) {
                                [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                            }
                            [grid insertItemsAtIndexPaths:arrayWithIndexPaths];
                            
                        } completion:^(BOOL finished) {
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [self hideScrollSpinner];
                                
                                
                            });
                        }];
                    }
                });
                
                _assetsInPage = self.assetsPage;
                _viewGrid = grid;
                
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (([page count] > 0) || _pageIndex < 1) {
                
                if (page.count == 0) {
                    
                    [self displayEmpty:NO];
                }
                else {
                    [self hideEmpty];
                }
                
                [self reloadAssets];
                
                [headerView folderButton].enabled = YES;
                
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self hideScrollSpinner];
            });
        });
    }];
}

- (NSArray *)getSelectedCategories {
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    int categoriesCount = 0;
    
    for (NSDictionary *category in [ZMProntoManager sharedInstance].categoriesToFilter) {
        if ([[category objectForKey:@"selected"] integerValue] == 1) {
            categoriesCount++;
            [newArray addObject:category];
        }
    }
    return [[NSArray alloc] initWithArray:newArray];
}

- (void)initiateLaunchLoading {
    //Loading indicator change
    [ZMUserActions sharedInstance].selectLeftMenu = @"";
    
    if (!launchLoading)
    {
        //        launchLoading = [[ZMLaunchLoading alloc] initWithParent:self.view];
        loadingIndicatorNew = [[LoadingIndicatorNew alloc] initWithParent:self.view];
        [self.view addSubview:loadingIndicatorNew];
        [self displayLaunchLoading];
    }
    else
    {
        [self displayLaunchLoading];
    }
}

//Handling export menu on print tap
- (void)initiatePrint:(BOOL) dismissExport {
    if(_exportMenuIsPresented) {
        if (grid.hidden) {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [cell.shareButton setSelected:NO];
        } else {
            ZMGridItem *cell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [cell.shareButton setSelected:NO];
        }
        ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (proCell && proCell.shareButton.isSelected && exportPopOverTappedonPromotedGrid) {
            [proCell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [proCell.shareButton setSelected:NO];
        }
        if(dismissExport) {
            [_exportMenu dismissViewControllerAnimated:NO completion:nil];
            _exportMenuIsPresented = NO;
        }
    }
}

-(void)setExportToNormal:(BOOL) dismissExport
{
    if(_exportMenuIsPresented) {
        if (grid.hidden) {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [cell.shareButton setSelected:NO];
        } else {
            ZMGridItem *cell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [cell.shareButton setSelected:NO];
        }
        ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (proCell && proCell.shareButton.isSelected && exportPopOverTappedonPromotedGrid) {
            [proCell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [proCell.shareButton setSelected:NO];
        }
        if(dismissExport) {
            [_exportMenu dismissViewControllerAnimated:NO completion:nil];
            _exportMenuIsPresented = NO;
        }
    }
}

- (void)initiateFeedback:(NSString*) selectedAsset openedFrom:(NSString*) choice {
    feedback = (FeedbackView *)[[[NSBundle mainBundle] loadNibNamed:@"FeedbackViewController" owner:self options:nil] objectAtIndex:0];
    feedback.assetId = selectedAsset;
    // display feedback view on top of the view
    backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [feedback initWithParent:[UIApplication sharedApplication].keyWindow background:backgroundView openedFrom:choice];
    if(_exportMenuIsPresented) {
        if (grid.hidden) {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [cell.shareButton setSelected:NO];
        } else {
            ZMGridItem *cell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [cell.shareButton setSelected:NO];
        }
        ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (proCell && proCell.shareButton.isSelected && exportPopOverTappedonPromotedGrid) {
            [proCell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [proCell.shareButton setSelected:NO];
        }
        [_exportMenu dismissViewControllerAnimated:NO completion:nil];
        _exportMenuIsPresented = NO;
    }
}

/**
 *  Reset view when swicth from Mybriefcase to My Library
 */
- (void)backToBriefcase {
    
    _section = ProntoSectionTypeBriefcase;
    [libraryScrollView scrollRectToVisible:CGRectMake(0, 0, libraryScrollView.frame.size.width, libraryScrollView.frame.size.height) animated:YES];
}


- (void)initLibraryAfterLogout
{
    [[ProntoUserDefaults userDefaults] setCMSLastSynchronization:@""];
    [[ProntoUserDefaults userDefaults] setCMSLastSynchronizationForEvents:@""];
    [[ProntoUserDefaults userDefaults] setCurrentUserID:@""];
    
    libraryInitialized = NO;
    firstTime = YES;
    //To avoid hiding of indicator when next login
    _previousProgress = 0;
    self.shouldLoadAllData = NO;
    syncWithCMSActive = NO;
    multipleFranchisePromotedMessageDisplayed = YES;
    //[self clearUserData];
}

- (void)dismissAllNotifications:(NSArray *)notifications {
    

}

- (IBAction)promotedContentChange:(id)sender {
    NSString * val = [NSString stringWithFormat:@"%li",(long)([promotedPageControl currentPage])];
    int intVal = [val intValue];
    
    //Promoted scroll fix
    int valToIncrease = 0;
    if (intVal != 0){
        valToIncrease = (interItemSpace-21);
    }
    [promotedGrid setContentOffset:CGPointMake((promotedGrid.frame.size.width * intVal)+(valToIncrease * intVal), promotedGrid.contentOffset.y) animated:YES];
    
}

#pragma mark - ScrollView Helpers

/**
 *  Show Scroll when the app is synch data from CMS
 */

- (void) showScrollSpinner {
    
    
    if (showScrollSpinnerHidden) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            showScrollSpinnerHidden = NO;
            
            [UIView animateWithDuration:0.5 animations:^{
                CGRect frame = scrollSpinner.frame;
                //Revamp - Move scroll spinner to the top
                frame.origin.y = self.view.frame.size.height - 55;
                frame.size.height = 55;
                //End of Revamp
                scrollSpinner.frame = frame;
            } completion:^(BOOL finished) {
                loadingLabel.hidden = NO;
                loadingIcon.hidden = NO;
            }];
        });
    }
}

- (void)hideScrollSpinner {
    
    if (!showScrollSpinnerHidden) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            showScrollSpinnerHidden = YES;
            [UIView animateWithDuration:0.5 animations:^{
                CGRect frame = scrollSpinner.frame;
                frame.origin.y = self.view.frame.size.height;// + frame.size.height;
                frame.size.height = 0;
                scrollSpinner.frame = frame;
            } completion:^(BOOL finished) {
                loadingLabel.text = @"Loading ...";
                loadingLabel.hidden = YES;
                loadingIcon.hidden = YES;
            }];
            
        });
    }
    
}

/**
 *  Function to increment number of pages to get data from CoreData Database
 */

- (void)loadNextPage {
    
    if (_section == ProntoSectionTypeLibrary) {
        _pageIndex++;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Page %d", _pageIndex]];
        [self refreshLibrary];
    }
}

- (void)verifySearch {
    
    
    [self.view endEditing:YES];
    _pageIndex = 0;
    
    // delay to display the assets
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if( headerView.searchTextbox.text.length >= 3 && (_apiManager.isConnectionAvailable || [ZMUserActions sharedInstance].isKeywordSaved))
        {
            [self loadSearchedAssets];
        }
        else
            [self loadMyLibrary];
        
    } );
    
    
    
}

/**
 *  Get Data from Coredata each time user chaged from Lybrary to My Briefcase views
 */

- (void)refreshLibrary {
    
    if(_section == ProntoSectionTypeBriefcase || _section == ProntoSectionTypeBriefcaseFolderDetail) {
        [self loadBriefcase];
    } else {
        
        // PRONTO-8 Search Performance - need to be checked
        [self.assetsPage removeAllObjects];
        dispatch_async(dispatch_get_main_queue(),^{
            
            [self loadMyLibrary];
        });
    }
}

/*
 * Helper, return the selected sorts
 */
- (NSMutableArray *)getSelectedSorts {
    
    NSMutableArray *selectedSorts = [[NSMutableArray alloc] init];
    
    for (id obj in _actions.sorts) {
        
        if ([[obj objectForKey:@"selected"] isEqualToString:@"1"]) {
            [selectedSorts addObject:obj];
        }
    }
    return selectedSorts;
}

/*
 * Return the single sort selection
 */
- (NSString *)getSelectedSort:(NSString*)sortParam {
    
    NSString *sortValue =[[NSString alloc] init];
    NSString *sortDirectionValue = [[NSString alloc] init];
    BOOL isViewCount = NO;
    
    for (id obj in _actions.sorts) {
        
        if ([[obj objectForKey:@"selected"] isEqualToString:@"1"]) {
            sortValue = [obj objectForKey:@"sort"];
            sortDirectionValue = [obj objectForKey:@"sortDir"];
            
            if ([sortValue isEqualToString:@"viewcount"]) {
                isViewCount = YES;
            }
        }
    }
    if (isViewCount) {
        sortValue = @"viewcount";
    }
    
    if ([sortParam isEqualToString:@"sort"]) {
        return sortValue;
    }
    else {
        return sortDirectionValue;
    }
}

#pragma mark - UICollectionView Helpers

- (void)initializeCollectionView {
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshAssets:) forControlEvents:UIControlEventValueChanged];
    
    grid.alwaysBounceVertical = YES;
    [grid addSubview:refreshControl];
    [grid setDataSource:self];
    [grid setDelegate:self];
    
    [libraryScrollView setDelegate:self];
}

- (void)resizeUICollectionView:(int)size {
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = libraryScrollView.frame;
        frame.origin.y = frame.origin.y + size;
        frame.size.height = frame.size.height + (size * -1);
        libraryScrollView.frame =  frame;
    }];
}


- (void)setSection:(enum ProntoSectionType)section {
    
    switch (section) {
        case ProntoSectionTypeLibrary:
            _actions.section = @"library";
            break;
        default:
            _actions.section = @"my briefcase";
            break;
    }
    
    if (_section == section) {
        return;
    }
    
    _section = section;
    
    switch (_section) {
            
        case ProntoSectionTypeBriefcase:
            [self resizeUICollectionView:-52];
            break;
            
        case ProntoSectionTypeLibrary:
            
        default:
            if (currentCell) {
                [currentCell flip];
                currentCell = nil;
            }
            [self resizeUICollectionView:52];
            break;
    }
}

- (void)refreshAssets:(id)sender
{
    if ([(UIRefreshControl *)sender respondsToSelector:@selector(endRefreshing)])
    {
        [(UIRefreshControl *)sender endRefreshing];
    }
    // should always refresh data
    _apiManager._syncingInProgress = NO;
    if (!_apiManager._syncingInProgress)
    {
        syncWithCMSActive = NO;
        [self bringLibraryData];
    }
}

# pragma mark - UIScrollView

/**
 *  scrollViewDidEndDecelerating Delegate to increment pageIndex to get Data from CoreData, pageIndex is used as indexOf parameter.
 *
 *  @param collectionView default collectionView
 */

- (void)scrollViewDidEndDecelerating:(UIScrollView *)collectionView {
    if (collectionView == promotedGrid) {
        // Pronto new revamp changes: calculating current page index of promoted content grid
        int value = (int)(collectionView.contentOffset.x) % (int)(collectionView.frame.size.width);
        NSInteger currentIndex = collectionView.contentOffset.x / collectionView.frame.size.width;
        if (value > 0) {
            currentIndex = currentIndex+1;
        }
        promotedPageControl.currentPage = currentIndex;
        
        
        CGFloat insets = 10.9 + 10.9;
        int visibleCellsCount = 1;
        for (int i = 1; i < 5; i++) {
            if (promotedGrid.bounds.size.width > ((i * gridWidth) + insets)) {
                visibleCellsCount = i;
            } else {
                break;
            }
        }
        
        if(promotedPageControl.numberOfPages > currentIndex+1){
            //Promoted scroll fix
            int valToIncrease = 0;
            if (currentIndex != 0){
                valToIncrease = (interItemSpace-21);
            }
            [promotedGrid setContentOffset:CGPointMake((promotedGrid.frame.size.width * currentIndex)+(valToIncrease * currentIndex), promotedGrid.contentOffset.y) animated:YES];
        }
        else if ((promotedContentArray.count != visibleCellsCount*promotedPageControl.numberOfPages) && (promotedPageControl.numberOfPages == currentIndex+1)) {
            //Promoted scroll fix
            int valToIncrease = 0;
            if (currentIndex != 0){
                valToIncrease = (interItemSpace-21);
            }
            [promotedGrid setContentOffset:CGPointMake((promotedGrid.frame.size.width * currentIndex)+(valToIncrease * currentIndex), promotedGrid.contentOffset.y) animated:YES];
        }
       
        
    }
    else
    {
        
        refreshControl.tintColor = [UIColor lightGrayColor];
        
        if([collectionView isEqual:grid] || [collectionView isEqual:listTableView]) {
            
            if (_section == ProntoSectionTypeLibrary && [[ZMProntoManager sharedInstance].search getTitle].length<3) {
                
                if (floor(collectionView.contentOffset.y) == collectionView.contentSize.height - collectionView.frame.size.height) {
                    
                    if(_pageIndex < 0)
                        _pageIndex = 0;
                    
                    _previousIndex = _pageIndex;
                    _pageIndex++;
                    int totalPages = floor(_actions.totalAssetsCount/_itemsPerPage);
                    
                    if (_pageIndex > totalPages) {
                        _pageIndex = totalPages;
                    }
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Page: %d %d", totalPages, _pageIndex]];
                    
                    if(presentCount != 0)
                    {
                        totalPresentCount += presentCount;
                        presentCount = 0;
                    }
                    
                    if ((_previousIndex != _pageIndex) && totalPresentCount != _actions.assetsCount) {
                        [self loadMyLibrary];
                        
                    }else{
                        [self hideScrollSpinner];
                    }
                }
            }
            else if(_section == ProntoSectionTypeLibrary && [[ZMProntoManager sharedInstance].search getTitle].length>=3)
            {
                if (collectionView.contentOffset.y == collectionView.contentSize.height - collectionView.frame.size.height) {
                    
                    _previousIndex = _pageIndex;
                    _pageIndex++;
   
                    int totalPages = floor(_actions.assetsCount/_itemsPerPage);
                    
                    if (_pageIndex > totalPages) {
                        _pageIndex = totalPages;
                    }
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Page: %d %d %d", totalPages, _pageIndex,_apiManager._currentPage]];
                    
                    [self reloadAssets];
                    [self hideScrollSpinner];
                }
            }
            else {
                
                if (collectionView.contentOffset.y == collectionView.contentSize.height - collectionView.frame.size.height) {
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Page %d", _pageIndex]];
                    [self showScrollSpinner];
                    [self loadBriefcasePages];
                }
            }
        }
    }
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    // check if this is promoted scroll
    if([promotedGrid indexPathsForVisibleSupplementaryElementsOfKind:UICollectionElementKindSectionHeader]){

    }
    
    cellWithAction = NO;
    int gridHeight = 90;
    
    if ([scrollView isEqual:libraryScrollView]) {
        if (scrollView.contentOffset.x == 0) {
            
            if(self.headerDetailFolder) {
                [self.headerDetailFolder removeFromSuperview];
                self.headerDetailFolder = nil;
                
            }
            _currentFolderId = 0;
        }
        else {
            _section = ProntoSectionTypeBriefcaseFolderDetail;
            gridHeight = gridHeight*-1;
        }
        
        if (!_libraryBackToFolderDetail) {
            self.assetsPage = [[NSMutableArray alloc] init];
            [self reloadAssets];
            
        }else{
            _libraryBackToFolderDetail = NO;
        }
        
        CGRect frame = grid.frame;
        frame.origin.x = scrollView.contentOffset.x;
        frame.size.height = frame.size.height+gridHeight;
        frame.origin.y = frame.origin.y-gridHeight;
        grid.frame = frame;
        
        if (_section == ProntoSectionTypeBriefcase || _section == ProntoSectionTypeBriefcaseFolderDetail) {
            _pageIndex = 0;
            [self loadBriefcase];
        }
        else {
            _pageIndex = 0;
            [self loadLibrary];
        }
    }
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //Pronto new revamp changes: Setting number of asset page count to display on tile to listview
    if (tableView == listTableView) {
        //Offline download on app stop and run
        float delay = 5.0;
        if([[self getFranchiseName] isEqual: @"In House"] || [[self getFranchiseName]  isEqual: @"Home Office"] ) {
            delay = 0.0;
        }
        [self performSelector:@selector(backgroundDownload) withObject:nil afterDelay:delay];
        return [self.assetsPage count];
    }
    //End
    return [ZMProntoManager sharedInstance].categoriesToFilter.count;
    // Return the number of rows in the section.
}

-(void) backgroundDownload
{
    if(([self.assetsPage count] > 0) && [[[NSUserDefaults standardUserDefaults] objectForKey:@"Off_Start"] isEqualToString:@"NO"] && self.isShowingIndicatorViewWithOverlay == NO && [_apiManager isConnectionAvailable]) {

                       [self offlineDownloadOfAssets];
                       previousAssetCount = [self.assetsPage count];

        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Off_Start"];
        
    }
    else if ((previousAssetCount != [self.assetsPage count]) && ([self.assetsPage count] > 0) && self.isShowingIndicatorViewWithOverlay == NO && [_apiManager isConnectionAvailable]) {

                        [self offlineDownloadOfAssets];

        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Off_Start"];
        
    }
}
//For adjusting label height according to content
-(CGRect) adjustLabelHeight: (UILabel *)label {
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    CGRect newFrame = label.frame;
    newFrame.size.height = size.height;
    return newFrame;
}

// New Revamping changes - leftMenuTableView updting with categories
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _leftMenuTableView) {
        
        static NSString *simpleTableIdentifier = @"CategoryMenuCell";
        CategoryMenuCell *cell = (CategoryMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryMenuCell" owner:self options:nil];
            [tableView registerNib:[UINib nibWithNibName:@"CategoryMenuCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
            cell = (CategoryMenuCell*)[nib objectAtIndex:0];
        }
        
        NSDictionary *category = nil;
        if([[ZMProntoManager sharedInstance].categoriesToFilter count] > indexPath.row)
        category = [[ZMProntoManager sharedInstance].categoriesToFilter objectAtIndex:indexPath.row];
        
//        else
//            return 0;
        
        self.actions.categories = [ZMProntoManager sharedInstance].categoriesToFilter;
        
        // Configure the cell...
        cell.mainLabel.textColor = [UIColor colorWithRed:0.51 green:0.50 blue:0.49 alpha:1.0];
        cell.mainLabel.font = [theme normalLato];
        
        if([[ZMProntoManager sharedInstance].categoriesToFilter count] > 1 && [[ZMProntoManager sharedInstance].categoriesToFilter count] > indexPath.row)
        {
            if(indexPath.row <= [[ZMProntoManager sharedInstance].categoriesToFilter count])
            {
                switch (indexPath.row) {
                    case 0:
                    {
                        cell.mainLabel.text = @"All Franchise Assets";
                        cell.mainLabel.font = [theme boldLato];
                    }
                        break;
                    case 1:
                    {
                        cell.cntLabel.backgroundColor= [UIColor colorWithRed:0.00 green:0.51 blue:0.73 alpha:1.0];
                        cell.cntLabel.textColor = [UIColor whiteColor];
                        cell.cntLabel.textAlignment = NSTextAlignmentCenter;
                        cell.cntLabel.font=[UIFont fontWithName:@"Lato-Regular" size:12.0f];
                        cell.cntLabel.layer.masksToBounds = YES;
                        cell.cntLabel.layer.cornerRadius = 11.0;
                        // [cell.contentView addSubview:cell.cntLabel];
                        cell.cntLabel.hidden = YES;
                        // bouncing animation for fav franchise count
                        if([[ZMCDManager sharedInstance] getFavFranchise].count > 0)
                        {
                            cell.cntLabel.hidden = NO;
                            cell.cntLabel.text= [ NSString stringWithFormat:@"%lu",(unsigned long)[[ZMCDManager sharedInstance] getFavFranchise].count];
                            if (favIconTapped == YES){
                                favIconTapped = NO;
                                
                                NSString *activeTabBucketName = [Constant GetCurrentActiveTabOptionAsString];
                                NSString *bucketNameForPromotedContent = [self getBucketIdAsStringForPromotedContentAsset:self.currentSelectedAsset];
                                //if selected asset  belongs to other tab, don't do the animation.
                                if ([[activeTabBucketName lowercaseString] isEqualToString:[bucketNameForPromotedContent lowercaseString]])
                                {
                                    [UIView animateWithDuration:0.2
                                                          delay:0
                                                        options:UIViewAnimationOptionCurveEaseIn
                                                     animations:^{
                                                         // moves label up 100 units in the y axis
                                                         cell.cntLabel.transform = CGAffineTransformMakeTranslation(0, -10);
                                                     }
                                                     completion:^(BOOL finished) {
                                                         [UIView animateWithDuration:0.2
                                                                               delay:0
                                                                             options:UIViewAnimationOptionCurveEaseOut
                                                                          animations:^{
                                                                              // move label back down to its original position
                                                                              cell.cntLabel.transform = CGAffineTransformMakeTranslation(0,0);
                                                                          }
                                                                          completion:nil];
                                                     }];
                                }
                            }
                        }
                        else
                        {
                            cell.cntLabel.hidden = YES;
                        }
                        cell.mainLabel.text = @"My Franchise Favorites";
                        cell.mainLabel.font = [theme boldLato];
                        
                    }
                        break;
                        
                    default:
                    {
                        if([[ZMUserActions sharedInstance].brandsArray containsObject:[category objectForKey:@"name"]])
                        {
                            // brands display
                            cell.mainLabel.textColor = [UIColor colorWithRed:0.00 green:0.51 blue:0.73 alpha:1.0];
                            cell.mainLabel.font = [theme normalLato];
                            
                        }
//                        else if(indexPath.row == 0 && [cell.mainLabel.text isEqualToString:@"All Franchise Assets"])
//                        {
//                            // fav assets
//                            cell.mainLabel.font = [theme boldLato];
//                        }

                        cell.mainLabel.text = [category objectForKey:@"name"];
                        cell.cntLabel.hidden = YES;
                }
                break;
                }
            }
        }
        else
        {
            cell.mainLabel.font = [theme boldLato];
            
            if(indexPath.row == 0)
                cell.mainLabel.text = @"All Franchise Assets";
            else if(indexPath.row == 1)
            {
                cell.mainLabel.text = @"My Franchise Favorites";
                
            }
        }
        
        if(indexPath.row > 1)
            cell.cntLabel.hidden = YES;
        //Revamp(26/12) - To make main label appear in center
        [cell.mainLabel sizeToFit];
        if ([[category objectForKey:@"selected"] integerValue] == 1)
        {
            cell.checkedImage.hidden = NO;
            [theme tintArrow:cell.checkedImage];
            //Revamp(26/12) - To make main label appear in center
            cell.checkedImage.image = [UIImage imageNamed:@"checkListSelect"];
            //Avoid left table menu crash
            long indexpathCountVal = indexPath.row;
            if([ZMProntoManager sharedInstance].categoriesToFilter.count < indexpathCountVal)
                [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            
        } else {
            cell.checkedImage.hidden = NO;
            cell.checkedImage.image = [UIImage imageNamed:@"checkList"];
            [theme tintArrow:cell.checkedImage];
            //Revamp(26/12) - To make main label appear in center
            
        }
        //Revamp(26/12) - To make main label appear in center
        cell.mainLabel.center = cell.checkedImage.center;
        CGRect frame = cell.mainLabel.frame;
        frame.origin.x = 45;
        frame.size.width = 170;
        frame.origin.y -= 1;
        cell.mainLabel.frame = frame;
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    } else {
        // Pronto new revamping changes
        heightToIncrease = 0;
        static NSString *simpleTableIdentifier = @"FranchiseTableViewCell";
        FranchiseTableViewCell *cell = (FranchiseTableViewCell *) [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        //Hide loader background
        [cell.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
        [cell.loaderBackgroundView setAlpha:1];
        cell.loaderBackgroundView.hidden = YES;
        cell.audioPlayerView.hidden = YES;
        cell.loaderView.hidden = YES;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FranchiseTableViewCell" owner:self options:nil];
            
            [tableView registerNib:[UINib nibWithNibName:@"FranchiseTableViewCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
            
            cell = (FranchiseTableViewCell*)[nib objectAtIndex:0];
            
        }
        if ([self.assetsPage count] > 0 && indexPath.row < [self.assetsPage count]) {
            Asset * asset = [self.assetsPage objectAtIndex:indexPath.row];
            cell.labelAssetName.text = asset.title;
            
            //Adjust and increase label height
            cell.labelAssetName.frame = [self adjustLabelHeight:cell.labelAssetName];
            if(cell.labelAssetName.frame.size.height > 21) {
                heightToIncrease = heightToIncrease + cell.labelAssetName.frame.size.height - 21;
            }
            
            CGRect frame = cell.labelAssetType.frame;
            frame.origin.y = cell.labelAssetName.frame.origin.y + cell.labelAssetName.frame.size.height + 5;
            cell.labelAssetType.frame = frame;
            cell.labelAssetType.text = [asset.field_description stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            cell.labelAssetType.frame = [self adjustLabelHeight:cell.labelAssetType];
            if(cell.labelAssetType.frame.size.height > 21) {
                heightToIncrease = heightToIncrease + cell.labelAssetType.frame.size.height - 21;
            }
            
            cell.isNew = asset.is_new;
           // cell.isUpdateAvailable = asset.isUpdateAvailable;
            cell.uid = asset.assetID.longValue;
            [cell.shareButton  addTarget:self  action:@selector(exportIconClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.favButton  addTarget:self  action:@selector(favIconClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.infoButton  addTarget:self  action:@selector(infoIconClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.shareButton.tag = indexPath.row;
            cell.favButton.tag = indexPath.row;
            cell.infoButton.tag = indexPath.row;
            
            NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchise];
            [cell.favButton setImage:[UIImage imageNamed:@"favIcon"] forState:UIControlStateSelected];
            [cell.favButton setSelected:NO];
            for(FavFranchise *fav in favfranchise) {
                if(cell.uid == [fav.asset_ID longValue]) {
                    [cell.favButton setImage:[UIImage imageNamed:@"favIconSelect"] forState:UIControlStateSelected];
                    [cell.favButton setSelected:YES];
                    break;
                } else {
                    [cell.favButton setImage:[UIImage imageNamed:@"favIcon"] forState:UIControlStateSelected];
                    [cell.favButton setSelected:NO];
                }
            }
            if(asset.assetID == assetForAudio.assetID && (audioCurrentPromotedGridCell == nil))
            {
                cell.audioPlayerView.hidden = NO;
                [audioPlayerView removeFromSuperview];
                audioPlayerView.frame = CGRectMake(0, 0,cell.audioPlayerView.frame.size.width, cell.audioPlayerView.frame.size.height);
                [cell.audioPlayerView addSubview:audioPlayerView];
            }
        }
        //set loader background for full
        CGRect fram = cell.loaderBackgroundView.frame;
        fram.size.width = cell.frame.size.width;
        fram.size.height = cell.frame.size.height;
        cell.loaderBackgroundView.frame = fram;
        cell.loaderView.center = cell.loaderBackgroundView.center;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Pronto new revamp chnages: To open asset from listview
    if (tableView == listTableView) {
        [tableView deselectRowAtIndexPath:indexPath animated: YES];

        [self openAssetFromListOrGrid: indexPath.row type:@"List"];
        return;
    }
    
    // End
    [ZMUserActions sharedInstance].isCategoriesSelected = YES;
    _categoryMenu = (ZMCategoryMenu *)[[[NSBundle mainBundle] loadNibNamed:@"CategoryMenu" owner:self options:nil] objectAtIndex:0];
    
    _categoryMenu.categoriesArray = [[NSMutableArray alloc] initWithArray:self.actions.categories];

    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Categories count %lu in did select",(unsigned long)self.actions.categories.count]];
    _categoryMenu.library = self;
    _categoryMenu.categoryMenuTableView = _leftMenuTableView;
    
    //[_leftMenuTableView deselectRowAtIndexPath:indexPath animated:YES];
    CategoryMenuCell *cell = (CategoryMenuCell*)[tableView cellForRowAtIndexPath:indexPath];
    [_categoryMenu checkStatusCell:cell indexPath:indexPath];
    [_categoryMenu setAnalyticsCategory];
    
    [self stopAudio];
    
    //Fix for rows hiding on category swap
    if(grid.hidden == YES) {
        NSIndexPath* top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
        [listTableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    else {
        if([self.assetsPage count] > 0)
        {
            NSIndexPath* top = [NSIndexPath indexPathForRow:0 inSection:0];
            [grid scrollToItemAtIndexPath:top atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
        }
    }

    if(indexPath.row == 1)
    { // Favorite is selected
        NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchise];
        NSMutableArray *favArray = [[NSMutableArray alloc]init];
        for(FavFranchise *fav in favfranchise) {
            [favArray addObject:fav.asset_ID];
        }
        
        ZMProntoManager.sharedInstance.favArrayIds = favArray;
        ZMProntoManager.sharedInstance.assetsIdToFilter = favArray;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Assets to Filter: %@",favArray]];
        
        //To stop the promoted content refresh on opening fav
        //[promotedGrid setContentOffset:CGPointMake(0, 0) animated:YES];
        [self setFrameOfPromotedGrid];
        
        
        if(ZMProntoManager.sharedInstance.assetsIdToFilter.count > 0 && headerView.searchTextbox.text.length == 0)
            [self updateFavGridList];
        else
            return;
        
    }
    else // Favorite is not selected
    {
        // then reset the value of selected favorite
        ZMProntoManager.sharedInstance.favArrayIds = @[];
        ZMProntoManager.sharedInstance.assetsIdToFilter = @[];
    }
}


// Update Grid with fav assets
-(void) updateFavGridList
{
    [[ZMCDManager sharedInstance] getFavAssets:0 limit:-1 success:^(NSArray* totalAssets) {
    
        if (_pageIndex == 0 ) {
            self.assetsPage = [[NSMutableArray alloc] init];

        }
        
        NSMutableArray *page = [[NSMutableArray alloc]initWithArray:totalAssets];
           
        if ([page count] > 0) {
            
            if ([self.assetsPage count] == _actions.assetsCount || grid.contentOffset.y == 0 || [self.assetsPage count] == 0) {
                [self.assetsPage addObjectsFromArray:page];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self hideScrollSpinner];
                    
                });
                
                _assetsInPage = self.assetsPage;
                _viewGrid = grid;
            }
            
            else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (grid.hidden) {
                        [listTableView beginUpdates];
                        int resultsSize = (int)[self.assetsPage count];
                        [self.assetsPage addObjectsFromArray:page];
                        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
                        for (int i = resultsSize; i < resultsSize + page.count; i++) {
                            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        }
                        [listTableView insertRowsAtIndexPaths: arrayWithIndexPaths withRowAnimation: UITableViewRowAnimationAutomatic];
                        [listTableView endUpdates];
                    } else {
                        [grid performBatchUpdates:^{
                            int resultsSize = (int)[self.assetsPage count]; //data is the previous array of data
                            [self.assetsPage addObjectsFromArray:page];
                            NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
                            for (int i = resultsSize; i < resultsSize + page.count; i++) {
                                [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                            }
                            [grid insertItemsAtIndexPaths:arrayWithIndexPaths];
                            
                        } completion:^(BOOL finished) {
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [self hideScrollSpinner];
                                _viewGrid = grid;
                            });
                        }];
                    }
                });
                
                _assetsInPage = self.assetsPage;
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (([page count] > 0) || _pageIndex < 1) {
                
                if (page.count == 0) {
                    
                    [self displayEmpty:NO];
                }
                else {
                    [self hideEmpty];
                }
                if (grid.hidden) {
                    [listTableView reloadData];
                } else {
                    [grid reloadData];
                }
                //[promotedGrid reloadData];
                [headerView folderButton].enabled = YES;
                
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self hideScrollSpinner];
                
                
            });
            
        });
        
    }];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Revamp(26/12) - To adjust left table view cell height;
    if (tableView == listTableView) {
        //Adjust row height dynamically
        return 96 + heightToIncrease;

    } else {
       
        return 44;//height > 44 ? (height+10):44;
    }
}


#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == grid) {
        //Offline download on app stop and run
        if(([self.assetsPage count] > 0) && [[[NSUserDefaults standardUserDefaults] objectForKey:@"Off_Start"] isEqualToString:@"NO"] && [_apiManager isConnectionAvailable]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [self offlineDownloadOfAssets];
            });
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Off_Start"];
            
        }
        else if ((previousAssetCount != [self.assetsPage count]) && ([self.assetsPage count] > 0) && [_apiManager isConnectionAvailable]) {
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [self offlineDownloadOfAssets];
         });
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Off_Start"];
            
        }
        previousAssetCount = [self.assetsPage count];
        return [self.assetsPage count];
        
        
    } else {
        //To avoid unwanted gaps in promoted
        return promotedContentArray.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // display promoted content
    if(collectionView == promotedGrid)
    {
        if(promotedContentArray.count > 0 && (promotedContentArray.count >= indexPath.item + 1)) {
            Asset *assetVal = [promotedContentArray objectAtIndex:indexPath.item];//[promotedContentAssets valueForKey:[NSString stringWithFormat:@"%ld",(long) indexPath.item]];
            // fix for displaying promoted content
            
            NSString * proAsset = [assetVal.assetID stringValue];

        NSFetchRequest * fetchrequest = [Asset MR_requestAll];
        NSArray *importedAssets = [Asset MR_executeFetchRequest:fetchrequest];
        for (Asset* asset in importedAssets) {
            if ([[[asset assetID] stringValue] isEqual: proAsset]) {
                self.cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PromotedItem" forIndexPath:indexPath];
                
                //Hide loader background
                [self.cell.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
                [self.cell.loaderBackgroundView setAlpha:1];
                self.cell.loaderBackgroundView.hidden = YES;
                self.cell.audioPlayerView.hidden = YES;
                
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"at indexpath %ld = %@", (long)indexPath.item, proAsset]];
                self.cell.uid = asset.assetID.longValue;
                self.cell.type = ZMGridItemTypeAsset;
                self.cell.layer.shouldRasterize = YES;
                self.cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
                [asset setField_promote:YES];
                
                self.cell.nameLabel.text = asset.title;
                
                // PRONTO-63 - User can see how many items are there in the Folder
                if(![self.cell.assetCount.text isEqualToString:@""])
                    [self.cell.countLabel setTitle:self.cell.assetCount.text forState:UIControlStateDisabled];
                else
                    self.cell.countLabel.hidden = YES;
                self.cell.isVideo = [[asset getAssetType] isEqualToString:@"Video"];
                self.cell.assetType = [asset getAssetType];
                // removing thumbnail path
                self.cell.thumbnailPath = asset.field_thumbnail;
                
                // PRONTO-1 - Likes not Working
                self.cell.reviews = asset.votes.intValue;
                self.cell.totalViews = asset.view_count.intValue;
                self.cell.totalDuration = asset.field_length;
                self.cell.isPopularDocument = NO;
                
                // New revamping changes - icons in a single asset
                self.cell.shareButton.tag = indexPath.item;
                self.cell.favButton.tag = indexPath.item;
                self.cell.infoButton.tag = indexPath.item;
                
                [self.cell.shareButton  addTarget:self  action:@selector(exportIconClicked:) forControlEvents:UIControlEventTouchUpInside];
                [self.cell.favButton  addTarget:self  action:@selector(favIconClicked:) forControlEvents:UIControlEventTouchUpInside];
                [self.cell.infoButton  addTarget:self  action:@selector(infoIconClicked:) forControlEvents:UIControlEventTouchUpInside];
                //[self.cell.downloadButton addTarget:self  action:@selector(downloadOneAsset:) forControlEvents:UIControlEventTouchUpInside];
                // New revamping changes - Status of asset
                self.cell.isUpdateAvailable = asset.isUpdateAvailable;
                self.cell.isNew = asset.is_new;
                
                if (![asset.path isEqualToString:@""] || [asset.path isEqualToString:@"BrigthcoveUpdated"]) {
                    self.cell.read = YES;
                }else{
                    self.cell.read = NO;
                }
                
                if (mostPopularAsset == asset.assetID.longValue)
                {
                    self.cell.isPopularDocument = YES;
                }
                //Revamp(4/12) - Keep fav icon pressed for the assets tapped before
                [_cell.favButton setImage:[UIImage imageNamed:@"favIcon"] forState:UIControlStateSelected];
                [_cell.favButton setSelected:NO];
               
                NSString *currentBucketName = @"";
                if([ZMUserActions sharedInstance].globalSearchViewController)
                {
                    currentBucketName = [[ZMUserActions sharedInstance].globalSearchViewController getBuckeNameFromSelectedTab];
                }
                else
                {
                    currentBucketName = [self getBucketIdAsStringForPromotedContentAsset:asset];
                }
                
                NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:asset.assetID];
                if (favfranchise.count > 0)
                {
                    FavFranchise *favFranchise = favfranchise[0];
                    BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:favFranchise.bucketName];
                    if (isPresent == YES)
                    {
                        [_cell.favButton setImage:[UIImage imageNamed:@"favIconSelect"] forState:UIControlStateSelected];
                        [_cell.favButton setSelected:YES];
                    }
                }
                
                if(asset.assetID == assetForAudio.assetID && audioCurrentPromotedGridCell != nil)
                {
                    self.cell.audioPlayerView.hidden = NO;
                    [audioPlayerView removeFromSuperview];
                    audioPlayerView.frame = CGRectMake(0, 0,self.cell.audioPlayerView.frame.size.width, self.cell.audioPlayerView.frame.size.height);
                    [self.cell.audioPlayerView addSubview:audioPlayerView];
                }
                
                self.cell.delegate = self;
            }
        }
    }
        if (!self.cell) {
            self.cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PromotedItem" forIndexPath:indexPath];
        }
        
        [self.cell setNeedsDisplay];
        
        // self.cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AssetItem"  forIndexPath:indexPath];
        // Configure the cell …
        self.cell.layer.shouldRasterize = YES;
        self.cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
        return self.cell;
    }
    else
    {
        if ([self.assetsPage count] > 0 && indexPath.row < [self.assetsPage count]) {
            
            Asset * asset = [self.assetsPage objectAtIndex:indexPath.row];
            self.cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AssetItem" forIndexPath:indexPath];
            
            //Hide loader background
            [self.cell.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
            [self.cell.loaderBackgroundView setAlpha:1];
            self.cell.loaderBackgroundView.hidden = YES;
            self.cell.audioPlayerView.hidden = YES;
            
            self.cell.uid = asset.assetID.longValue;
            self.cell.type = ZMGridItemTypeAsset;
            
            self.cell.layer.shouldRasterize = YES;
            self.cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
            
            self.cell.nameLabel.text = asset.title;
            
            // PRONTO-63 - User can see how many items are there in the Folder
            
            self.cell.countLabel.hidden = YES;
            self.cell.isVideo = [[asset getAssetType] isEqualToString:@"Video"];
            self.cell.assetType = [asset getAssetType];
            // removing thumbnail path
            self.cell.thumbnailPath = asset.field_thumbnail;
            
            // PRONTO-1 - Likes not Working
            self.cell.reviews = asset.votes.intValue;
            self.cell.totalViews = asset.view_count.intValue;
            self.cell.totalDuration = asset.field_length;
            self.cell.isPopularDocument = NO;
            
            // New revamping changes - icons in a single asset
            self.cell.shareButton.tag = indexPath.item;
            self.cell.favButton.tag = indexPath.item;
            self.cell.infoButton.tag = indexPath.item;
            
            [self.cell.shareButton  addTarget:self  action:@selector(exportIconClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self.cell.favButton  addTarget:self  action:@selector(favIconClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self.cell.infoButton  addTarget:self  action:@selector(infoIconClicked:) forControlEvents:UIControlEventTouchUpInside];
            // New revamping changes - Status of asset
            self.cell.isUpdateAvailable = asset.isUpdateAvailable;
            self.cell.isNew = asset.is_new;
            
            if (![asset.path isEqualToString:@""] || [asset.path isEqualToString:@"BrigthcoveUpdated"]) {
                self.cell.read = YES;
            }else{
                self.cell.read = NO;
            }
            
            if (mostPopularAsset == asset.assetID.longValue)
            {
                self.cell.isPopularDocument = YES;
            }
            //Revamp(4/12) - Keep fav icon pressed for the assets tapped before
            NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchise];
            [_cell.favButton setImage:[UIImage imageNamed:@"favIcon"] forState:UIControlStateSelected];
            [_cell.favButton setSelected:NO];
            
            for(FavFranchise *fav in favfranchise) {
                
                if(_cell.uid == [fav.asset_ID longValue]) {
                    [_cell.favButton setImage:[UIImage imageNamed:@"favIconSelect"] forState:UIControlStateSelected];
                    [_cell.favButton setSelected:YES];
                    break;
                } else {
                    [_cell.favButton setImage:[UIImage imageNamed:@"favIcon"] forState:UIControlStateSelected];
                    [_cell.favButton setSelected:NO];
                    
                }
            }
            //End of Revamp
            if(asset.assetID == assetForAudio.assetID && (audioCurrentPromotedGridCell == nil))
            {
                self.cell.audioPlayerView.hidden = NO;
                [audioPlayerView removeFromSuperview];
                audioPlayerView.frame = CGRectMake(0, 0,self.cell.audioPlayerView.frame.size.width, self.cell.audioPlayerView.frame.size.height);
                [self.cell.audioPlayerView addSubview:audioPlayerView];
            }
            
            self.cell.delegate = self;
        }
        
    }
    
    if (!self.cell) {
        self.cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AssetItem" forIndexPath:indexPath];
    }
    self.cell.layer.shouldRasterize = YES;
    self.cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return self.cell;
    
    
}

//Pronto new revamp chnages: To open asset from listview/grid view
- (void) openAssetFromListOrGrid:(NSInteger)row type:(NSString*)selectedType  {
    // Fix for -- Terminating app due to uncaught exception 'NSRangeException', reason: '*** -[__NSArrayM objectAtIndex:]: index 17 beyond bounds [0 .. 12]
    if (([self.assetsPage count] > 0 && row < [self.assetsPage count]) || [selectedType isEqualToString:@"Promoted"]) {
        
        Asset * selectedAsset;
        if([selectedType isEqualToString:@"Promoted"]){
            selectedAsset = [promotedContentArray objectAtIndex:row];
        }
        else {
            selectedAsset = [self.assetsPage objectAtIndex:row];
        }
        
        /**
         * @Tracking
         * Default Action
         **/
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        
        if ([[ZMUserActions sharedInstance] getFiltersForTracking]) {
            [trackingOptions setObject:[[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
        }
        
        [trackingOptions setObject:@"click-through" forKey:@"scfilteraction"];
        [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
        
        if ([selectedAsset title].length > 0) {
            [trackingOptions setObject:selectedAsset.title forKey:@"scsearchclickthroughfilename"];
        }
        
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                  withSubsection:@"filter"
                        withName:[NSString stringWithFormat:@"brand-%@-default", [ZMUserActions sharedInstance].franchiseName]
                     withOptions:trackingOptions];
        
        /**
         * @Tracking
         * Engagement
         **/
        trackingOptions = [[NSMutableDictionary alloc] init];
        [trackingOptions setObject:@"" forKey:@"sccollateralfeatures"];
        
        
        // Fix for ---Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: sccollateralname)
        if ([selectedAsset title].length > 0)
            [trackingOptions setObject:selectedAsset.title forKey:@"sccollateralname"];
        
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                  withSubsection:@"assetview"
                        withName:[NSString stringWithFormat:@"%@|%@",[ZMAssetViewController getAssetType:selectedAsset], selectedAsset.title]
                     withOptions:trackingOptions];
        if ([selectedAsset title].length > 0)
            [self validateAsset:selectedAsset];
    }
}
// End

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // Pronto new revamp changes: Disable all actions for promoted content grid item due to sample data
//    Asset *assetSelected = [self.assetsPage objectAtIndex:indexPath.row];
    if (collectionView == promotedGrid) {
        ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:indexPath.item inSection:0]];
    
//        if([assetSelected.file_mime isEqualToString:@"audio"])
//        {
//            [self makeAllAudioPlayerCellsNil];
//            audioCurrentPromotedGridCell = proCell;
//        }
        if (proCell.type == ZMGridItemTypeAsset) {
            [self openAssetFromListOrGrid: indexPath.item type:@"Promoted"];
        }
        promotedTappedForAudio = true;
    }
    // End: Pronto new revamp changes:
    
    else {
        if ([collectionView cellForItemAtIndexPath:indexPath]) {
            
            ZMGridItem *cell = (ZMGridItem*)[collectionView cellForItemAtIndexPath:indexPath];

            if (cell.type == ZMGridItemTypeAsset) {
                [self openAssetFromListOrGrid: indexPath.item type:@"Grid"];
            }
            else {
                
                // folder action
            }
        }
    }
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(gridWidth, 160);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 12, 0, 15);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (collectionView == promotedGrid) {
        return [self getInterItemSpace];
    }
    return 15;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

#pragma mark - ZMGridItem Delegate
- (NSInteger) getPageCount {
    return 0;
}

// Pronto new revamp changes: calculating promoted content grid items Interspace
- (CGFloat) getInterItemSpace {
    CGFloat insets = 10.9 + 10.9; // left inset + right inset
    int visibleCellsCount = 1;
    for (int i = 1; i < 5; i++) {
        if (promotedGrid.bounds.size.width > ((i * gridWidth) + insets)) {
            visibleCellsCount = i;
        } else {
            break;
        }
    }
    
    long pageNo = promotedPageControl.currentPage;
    promotedPageControl.numberOfPages = 0;
    //To avoid unwanted gaps in promoted
    if (promotedContentArray.count != 0 ) {
        if (promotedContentArray.count >= visibleCellsCount) {
            int pageValue = promotedContentArray.count % visibleCellsCount;
            NSInteger numberOfPages = promotedContentArray.count / visibleCellsCount;
            if (pageValue > 0) {
                numberOfPages += 1;
            }
            promotedPageControl.numberOfPages = numberOfPages;
            promotedPageControl.currentPage = pageNo;
        }
    }
    CGFloat space = promotedGrid.bounds.size.width - ((visibleCellsCount * gridWidth) + insets) ;
    interItemSpace = ceil(space/(visibleCellsCount - 1))+1;
    
    [promotedGrid setContentOffset:CGPointMake((promotedGrid.frame.size.width * promotedPageControl.currentPage), promotedGrid.contentOffset.y) animated:YES];
    
    return interItemSpace;
}

- (void)cellClosedOnEvent {
    currentCell = nil;
    _selectedIndexPath = nil;
}

- (void)deleteComplete:(NSString *)folderName gridItem:(ZMGridItem *)gridItem {
    
    [grid performBatchUpdates:^{
        
        int count = 0;
        NSIndexPath *indexPath = [grid indexPathForCell:gridItem];
        ZMFolder *currentFolder;
        
        for (id currentItem in self.assetsPage) {
            
            if ([currentItem isKindOfClass:[ZMFolder class]]) {
                currentFolder = (ZMFolder *)currentItem;
                
                if (currentFolder.fldr_id == gridItem.uid) {
                    
                    [self.assetsPage removeObjectAtIndex:count];
                    break;
                }
            }
            count++;
        }
        [grid deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        
    } completion:^(BOOL finished) {
        if (self.assetsPage.count == 0) {
            [self displayEmpty:NO];
        }
        else {
            [self hideEmpty];
        }
    }];
}

- (void)deleteAssetFromFolder:(Asset *)asset {
    
    [grid performBatchUpdates:^{
        int count = 0;
        NSIndexPath *indexPath;
        
        for (id currentItem in self.assetsPage) {
            if ([currentItem isKindOfClass:[Asset class]]) {
                
                Asset * currentAsset = currentItem;
                
                if (currentAsset.assetID.longValue == asset.assetID.longValue) {
                    [self.assetsPage removeObjectAtIndex:count];
                    indexPath =[NSIndexPath indexPathForRow:count inSection:0];
                    break;
                }
            }
            count++;
        }
        
        if (indexPath != nil)
        {
            NSArray* indexArray = [NSArray arrayWithObject:indexPath];
            if(indexArray.count > 0)
            {
                [grid deleteItemsAtIndexPaths:indexArray];
            }
        }
    } completion:^(BOOL finished) {
        
    }];
    
    if (_currentFolderId != 0) {
        
        
        
    }
    else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [[ZMAssetsLibrary defaultLibrary] removeAssetToFolder:asset folder:nil];
            [[ZMAssetsLibrary defaultLibrary] deleteAssetFromFolderAssets:asset.assetID.longValue folderId:0];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _pageIndex = 0;
                currentCell = nil;
                
            });
            
        });
    }
}

- (void)updateFolderName:(NSString *)folderName folderId:(long)folderId {
    
    BOOL isDuplicated = [[ZMAssetsLibrary defaultLibrary] duplicatedFolderName:folderName];
    
    if (!isDuplicated && ![folderName isEqualToString:@""]) {
        ZMFolder *folder = [[ZMAssetsLibrary defaultLibrary] folderWithId:folderId];
        folder.fldr_name = folderName;
        [[ZMAssetsLibrary defaultLibrary] saveFolder:folder withDetails:FALSE];
        
        /**
         * @Tracking
         * Update Folder Name
         **/
        [ZMTracking trackSection:_actions.section withSubsection:@"folder" withName:[NSString stringWithFormat:@"addfolder-%@",folderName] withOptions:
         [[NSMutableDictionary alloc] initWithDictionary:@{
                                                           @"scbriefcaseaction": @"update",
                                                           @"sccollateralname" : folderName }]];
        
        
    }
    else {
        NSString *message = @"Error, duplicated folder name, please change it.";
        
        if ([folderName isEqualToString:@""]) {
            message = @"Error, invalid folder name, please change it.";
        }
        [mainDelegate showMessage:message whithType:ZMProntoMessagesTypeWarning withTime:2];
    }
}

//Offline download main function
-(void) offlineDownloadOfAssets {
    ///For Offline Download main

    __strong NSMutableArray* assetsPageDuplicate;
    assetsPageDuplicate = [[NSMutableArray alloc]init];
    [assetsPageDuplicate addObjectsFromArray:self.assetsPage];
    // is it in background
    dispatch_async(dispatch_get_main_queue(), ^{
        _initialDownloadDbArray = [[ZMCDManager sharedInstance]getOfflineDownloadArray];
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        //If coredata not present
        if (_initialDownloadDbArray.count != 0) {

            //Add assets which are newly added
            for (Asset * asset in assetsPageDuplicate) {
                bool dataPresent = NO;
                for(OfflineDownload * data in _initialDownloadDbArray) {
                    if ([[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"]) {
                        if(asset.assetID == data.assetID) {
                            dataPresent = YES;
                            break;
                        }
                    }
                    else {
                        dataPresent = YES;
                        break;
                    }
                }
                if(!dataPresent) {
                    [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                    [initialDownloadAsset setObject:@"in_progress" forKey:@"status"];
                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                    
                    //Downloading the Asset added new
                    NSNumber * bucketId = [_actions getBucketID:asset];
                    if(asset != nil &&
                       [[bucketId stringValue] isEqualToString:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]])
                    {
//                            [_apiManager downloadAssetFor:asset];
                        [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
                    }
                    else if(asset != nil && bucketId == nil)
                    {
                        [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
                    }
                }
            }
           
            //Download the data which is in progress
            for(OfflineDownload * data in _initialDownloadDbArray) {
                if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"] || [data.status isEqualToString:@"failed"]) {
                    
                    //Download initiate for failed assets
                    if([data.status isEqualToString:@"completed_partialy"] || [data.status isEqualToString:@"failed"]) {
                        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:data.assetID ];
                        [initialDownloadAsset setObject:data.assetID forKey:@"assetID"];
                        [initialDownloadAsset setObject:@"in_progress" forKey:@"status"];

                        if (offlineData.count > 0)
                        {
                            //if  insertation and deletetion has to be happen, it will happen one by one
                            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                            }];
                        }
                        else
                        {
                            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                        }
                    }
                    
                    NSArray * assetVal = [[ZMCDManager sharedInstance]getAssetArrayById:data.assetID.stringValue];
                    if (assetVal.count > 0) {
                        Asset * assetCache = assetVal[0];
//                            [_apiManager downloadAssetFor:assetCache];
                        NSNumber * bucketId = [_actions getBucketID:assetCache];
                        if(assetCache != nil && [[bucketId stringValue] isEqualToString:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]])
                        {
                            [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:assetCache];
                        }
                        else if(assetCache != nil && bucketId == nil)
                        {
                            [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:assetCache];
                        }
                       
                    }
                }
            }
//                        }
        }
        //If coredata not present or first download
        else {
            NSMutableArray * assetArrayToDownload = [[NSMutableArray alloc]init];
            for (Asset * asset in assetsPageDuplicate) {
                if ([[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"]) {
                    [assetArrayToDownload addObject:asset];
                }
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset:%@ uri:%@",asset.assetID,asset.uri_full]];
            }
            [_apiManager offlineDownload:assetArrayToDownload];
            for(Asset * asset in assetArrayToDownload) {
                [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
            }
      
        }

});
}

#pragma mark - Assets Library Methods

- (void)statsChanged:(NSDictionary *)assets{
}

- (void)setProgress:(float)progress {
    
    //Progress for Grid Item
    ZMGridItem *currentDownloadItem;
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@",currentAssetDownload.assetID];
    NSArray * cells = [[grid visibleCells]filteredArrayUsingPredicate:predicate];
    if (cells.count > 0) {
        currentDownloadItem = cells.firstObject;
        currentDownloadItem.progress = progress;
        currentDownloadItem.loaderBackgroundView.hidden = NO;
        [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
        [currentDownloadItem.loaderBackgroundView setAlpha:0.5];
    }
    
    NSArray * cells1 = [[promotedGrid visibleCells]filteredArrayUsingPredicate:predicate];
    if (cells1.count > 0) {
        currentDownloadItem = cells1.firstObject;
        currentDownloadItem.progress = progress;
        currentDownloadItem.loaderBackgroundView.hidden = NO;
        [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
        [currentDownloadItem.loaderBackgroundView setAlpha:0.5];
    }
    
    if (progress >= 1) {
//        currentAssetDownload = nil;
        [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
        [currentDownloadItem.loaderBackgroundView setAlpha:1];
        currentDownloadItem.loaderBackgroundView.hidden = YES;
    }
    
    //Progress for list item
    FranchiseTableViewCell *currentCell;
    
    NSPredicate * predicateForList = [NSPredicate predicateWithFormat:@"uid == %@",currentAssetDownload.assetID];
    NSArray * cellsForList = [[listTableView visibleCells]filteredArrayUsingPredicate:predicateForList];
    if (cellsForList.count > 0) {
        currentCell = cellsForList.firstObject;
        currentCell.progress = progress;
        
        currentCell.loaderBackgroundView.hidden = NO;
        currentCell.loaderView.hidden = NO;
        
        [currentCell.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
        [currentCell.loaderBackgroundView setAlpha:0.5];
    }
    if (progress >= 1) {
        currentAssetDownload = nil;
        
        currentCell.loaderBackgroundView.bounds = currentCell.bounds;
        currentCell.loaderView.center = currentCell.center;
        
        [currentCell.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
        [currentCell.loaderBackgroundView setAlpha:1];
        
        currentCell.loaderBackgroundView.hidden = YES;
        currentCell.loaderView.hidden = YES;
    }
}

- (void)groupDidUpdate:(ZMAssetsGroup *)group
{
    [[ProntoUserDefaults userDefaults] setCMSLastSynchronization:@""];
}

- (void)groupDidDelete:(ZMAssetsGroup *)group {
    [self backToDefault];
}

- (void)assetDidUpdate:(ZMAsset *)asset {
    
}

- (void)libraryStartSync {
    [AbbvieLogging logInfo:@"library start sync...."];
    
   // [mainDelegate showHideOverlay:NO];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Off_Start"];
    _pageIndex = 0;
   // [self displayPromotedContent];
}

- (void)libraryDidFinishWithError:(NSString *)error {
    
    if ([_apiManager isConnectionAvailable]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kAssetV2CompletionNotification object:nil];

        NSString *message = error;
        
        if (_apiManager.lastServerCodeResponse == 503) {
            message = @"The server is currently under maintenance mode.";
        }else if (_apiManager.lastServerCodeResponse == 401 ||
                  _apiManager.lastServerCodeResponse == -1001 ||
                  _apiManager.lastServerCodeResponse == -1005 ||
                  _apiManager.lastServerCodeResponse == -1011 ||
                  _apiManager.lastServerCodeResponse == -1004 ||
                  _apiManager.lastServerCodeResponse == 3840) {
            
            message = @"Server connection is not available at the moment, please try again later";
        } // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        else if ( _apiManager.lastServerCodeResponse == 5303 || _apiManager.lastServerCodeResponse == -5303 )
        {
            message = @"Error while retreiving assets, please try again later";
        }
        else if ( _apiManager.lastServerCodeResponse == 1009 || _apiManager.lastServerCodeResponse == -1009)
        {
            message = @"Please check the network connection";
        }
        else if ( _apiManager.lastServerCodeResponse == 1003 || _apiManager.lastServerCodeResponse == -1003 )
        {
            message = @"A server with the specified hostname could not be found";
        }
        else if ( _apiManager.lastServerCodeResponse == 303 || _apiManager.lastServerCodeResponse == 305 || _apiManager.lastServerCodeResponse == 107 || _apiManager.lastServerCodeResponse == 0) {
            
            if (_apiManager.lastServerCodeResponse == 303){
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error %d", _apiManager.lastServerCodeResponse]];
            }
            
            //Restoring the user session
            [refreshControl endRefreshing];
            [mainDelegate initPronto];
            
            return;
        }
        else if (_apiManager.lastServerCodeResponse == 206) { //This means the sync process has stopped due the OS
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self refreshAssets:refreshControl];
            });
            
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [mainDelegate showMessage:message whithType:ZMProntoMessagesTypeReload withAction:^{
                [mainDelegate hideMessage];
                [self refreshAssets:refreshControl];
            }];
            
            /**
             * @Tracking
             * Download Did Not finished
             **/
            NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
            
            // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
            if ([message containsString:@"ERROR_CODE"])
            {
                // errorcode consists of the digits
                [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",@"Please check the network connection"] forKey:@"scerrorcode"];
            }
            else
                [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",message] forKey:@"scerrorcode"];
            [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"home" withName:nil withOptions:trackingOptions];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kAssetV2CompletionNotification object:nil];
            
        });
    }

    else
    {
        // error message for poor network connection
        [mainDelegate showMessage:@"Please check the network connection" whithType:ZMProntoMessagesTypeReload];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@">>>> SyncCMS calling: %s", __PRETTY_FUNCTION__]];
        
        if (self.isShowingIndicatorViewWithOverlay == YES)
        {
            [self showForceLogoutRefreshView:YES];
            [self.forceLogoutRefreshView showErroView:YES];
            // make visible forceLogoutRefreshView here and don't show forceLogoutRefreshView from begining
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                loadingIndicatorNew.loadingIndicatorLabel.text = @"";
                [loadingIndicatorNew.activityIndicator stopAnimating];
            });
        }
        else
        {
            [self hideLaunchLoading];
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingAssets];
        }
        
        [self hideScrollSpinner];

        if (refreshControl) {
            [refreshControl endRefreshing];
        }
        [self showWalkthroughOnInitialLogin];
    });
}



- (void)libraryDidFinishWithError {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(self.assetsPage.count > 0 && self.isShowingIndicatorViewWithOverlay == YES && [_apiManager isConnectionAvailable])
        {
            [self offlineDownloadOfAssets];
        }
        
        [self hideLaunchLoading];
        [self hideScrollSpinner];
        
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
        
        if (refreshControl) {
            [refreshControl endRefreshing];
        }
        [self showWalkthroughOnInitialLogin];
    });
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kAssetV2CompletionNotification object:nil];
}

- (void)librarySyncInProgress{
    
    
}

- (void)libraryFinishSync {
    [AbbvieLogging logInfo:@"syncWithCMS started -> Finish"];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if(self.assetsPage.count > 0 && self.isShowingIndicatorViewWithOverlay == YES && [_apiManager isConnectionAvailable])
            {
                [self offlineDownloadOfAssets];
            }
            
            [self hideScrollSpinner];
            [self hideLaunchLoading];
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingAssets];
            
            if (self.actions.shouldSwitchToOtherTab == YES &&
                self.actions.deepLinkUserInfo == nil &&
                (self.switchToTabBarItem != ProntoTabBarItemFranchise)) //deeplink option disabled. //showin. == no
            {
                [self switchToProntoTabbarItem:self.switchToTabBarItem];
            }
            [self showWalkthroughOnInitialLogin];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                isFetchEventsCompleted = YES;
            [self displayPromotedContent:@""];
            });
        });
        
        if (refreshControl) {
            [refreshControl endRefreshing];
        }
        
        if (!assetsAlreadyLoaded) {
            [self backToDefault]; //Para que todo el GUI y los flitros esten nulos
            assetsAlreadyLoaded = YES;
        }
     
        [ZMUserActions sharedInstance].enableSearch = YES;
        headerView.searchTextbox.enabled = YES;
        _apiManager._syncingCompletion = YES;
        
        [self updateHeader:headerView];
        [AbbvieLogging logInfo:@"refresh finishes.."];
        
        // Pronto-New Reavamp - hide the overlay
        //[mainDelegate showHideOverlay:YES];
        //[self fetchEvents];
        //[self displayPromotedContent];
        [_leftMenuTableView reloadData];
        [headerView hidingIndicator];
        headerView.searchTextbox.text = [ZMProntoManager sharedInstance].search.getTitle;
        
        if([ZMProntoManager sharedInstance].categoriesToFilter.count>1)
        {
            [_leftMenuTableView reloadData];
        }
        
        [self openAssetFromDeepLinkIfRequired];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kAssetV2CompletionNotification object:nil];
        
        _apiManager.libraryVC = nil;
    });
}


- (void)openAssetFromDeepLinkIfRequired
{
    if (self.actions.deepLinkUserInfo != nil)
    {
        // take page number as well
        NSString *assetId       = [self.actions.deepLinkUserInfo valueForKey:@"assetString"];
        NSInteger pageNumber    = [[self.actions.deepLinkUserInfo valueForKey:@"pageNumer"] integerValue];
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId];
        if (assetArray.count > 0) // It means asset available
        {
            Asset * asset = assetArray[0];
            //PRONTO-37 Deeplinking to a Page in a PDF
            [ZMUserActions sharedInstance].pageNo = pageNumber;
            [self validateAsset:asset];
        }
        else
        {
            //Sync for EOA
            [[EventsServiceManager sharedServiceManager] syncEventsAndOpenAssetForAssetId:assetId userFranchise:self.headerView.userFranchise.text andDelegate:(id <EventOnlyAssetHandlerDelegate>)self];
        }
        self.actions.deepLinkUserInfo = nil;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [AbbvieLogging logInfo:@"Lib  libraryFinishSync  from deepLinkUserInfo block"];
    }
}

- (void)libraryProgressStep:(NSString *)progressText progress:(float)progress forVal:(NSString*)progressFor
{
    //Progress for search will not be considered
    if ([progressFor isEqualToString:@"search"]) { return;}
    
    [ZMUserActions sharedInstance].selectLeftMenu = @"";
    // Revamp -to resolve thread issue
    dispatch_async(dispatch_get_main_queue(), ^(void){
    // hide the indicator while loading
    [headerView hidingIndicator];
    //set label for loading indicator
    if(progress == 0) {
        if(!(_previousProgress == 100)) {
            _previousProgress = progress;
            loadingIndicatorNew.activityIndicator.hidden = NO;
            loadingIndicatorNew.bgImage.hidden = NO;
            [loadingIndicatorNew.activityIndicator startAnimating];
        }

    }
    else {
        [loadingIndicatorNew.activityIndicator stopAnimating];
        loadingIndicatorNew.activityIndicator.hidden = YES;
        loadingIndicatorNew.bgImage.hidden = YES;
        float count = _previousProgress;
        while(count < progress) {
            [loadingIndicatorNew setProgress:count/100];
            count += 1;
        }
        _previousProgress = progress;
        [loadingIndicatorNew setProgress:progress/100];
    }
    
    if([progressText isEqualToString:@"Synchronizing User Data"])
    {
        if([ZMUserActions sharedInstance].isFieldUser == YES)
            self.progressTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
        [ZMUserActions sharedInstance].enableSearch = NO;
    }
    else if([progressText isEqualToString:@"Getting Assets 100%"])
    {
        [ZMUserActions sharedInstance].enableSearch = YES;
         self.checkUser = [NSTimer scheduledTimerWithTimeInterval:3.0  target:self selector:@selector(checkUserPreferences) userInfo:nil repeats:NO];
    }
    else
        [ZMUserActions sharedInstance].enableSearch = NO;
    
    
    UIColor *color = [theme whiteFontColor];
    headerView.searchTextbox.backgroundColor = [UIColor clearColor];
    headerView.searchTextbox.font = [theme normalLato];
    
    if(_apiManager.isConnectionAvailable && !_apiManager._syncingCompletion)
    {
        headerView.searchTextbox.enabled = NO;
        headerView.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Loading Assets..." attributes:@{NSForegroundColorAttributeName: color}];
        headerView.searchTextbox.text = @"";
        //Revamp(15/12) - Search white
        [headerView.lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
        headerView.searchTextbox.textColor = color;
        [ZMUserActions sharedInstance].enableSearch = NO;
        [headerView hidingIndicator];
    }
    else
    {
        headerView.searchTextbox.enabled = YES;
        headerView.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color}];
        headerView.searchTextbox.textColor = [UIColor whiteColor];
        [ZMUserActions sharedInstance].enableSearch = YES;
    }
    });
    
    
    [self refreshLibrary];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //Loading indicator label val set
        NSString * labelVal = @"";
        if([progressText containsString:@"Getting Assets"]) {
            labelVal = @"Getting Assets";
        }
        else {
            labelVal = progressText;
        }
        loadingIndicatorNew.loadingIndicatorLabel.text = labelVal;
        
        if (refreshControl) {
            [refreshControl endRefreshing];
        }
        
        if ([launchLoading isHidden]) {
            loadingLabel.text = progressText;
            [self showScrollSpinner];
        }
        // ABT-243 library will show assets when progress will be greater than 30 %
        //Avoid setting default for search
        if (progress > 2.0 && ![progressFor isEqualToString:@"search"]){
            [self backToDefault];
        }
        
        //If Already Indicator is not being shown, Then need to show
        //bottom indicator view
        if (self.isShowingIndicatorViewWithOverlay == NO && (loadingIndicatorNew.isHidden == YES || loadingIndicatorNew == nil))
        {
            //show bottom bar indicator view
            [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingAssets];
        }
    });
}

// For Loading Indicator with % of loading
- (void)updateUI:(NSTimer *)timer
{
    static int count =0; count++;
    
    if (count <=10)
    {
        _previousProgress = count*10;
        _timerProgress = (float)count/10.0f;

        [loadingIndicatorNew setProgress:_timerProgress];
        
        if(_previousProgress == 100)
        {
            [self.progressTimer invalidate];
            self.progressTimer = nil;
            count = 0;
            
        }
    }
    else
    {
        [self.progressTimer invalidate];
        self.progressTimer = nil;
        count=0;
        [loadingIndicatorNew.activityIndicator stopAnimating];
        loadingIndicatorNew.activityIndicator.hidden = YES;
        loadingIndicatorNew.bgImage.hidden = YES;
        
    }
}

- (void)progressFinished {
    
    if (mostPopularAsset > 0) {
        
        NSNumber * assetId = [NSNumber numberWithLong:mostPopularAsset];
        NSDate * date = [NSDate date];
        NSNumber *timeStampObj = [NSNumber numberWithDouble: date.timeIntervalSince1970];
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
        
        if (assetArray.count > 0) {
            Asset *asset = assetArray[0];
            asset.changed = timeStampObj;
            
            [[ZMCDManager sharedInstance]updateAsset:asset success:^{
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Succesfully updated Most Popular Asset with Id %@",assetId.stringValue]];
            } error:^(NSError * error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@"Error at updating most popular asset %@", error.localizedDescription]];
            }];
        }
    }
    
    [self hideScrollSpinner];
    [self hideLaunchLoading];
    [self showWalkthroughOnInitialLogin];
}

- (NSMutableArray *)getSortArray{
    
    NSMutableArray *sort = [[NSMutableArray alloc] init];
    
    [sort addObject:@{@"id":@"1", @"sort":@"dateAdded", @"label":@"Recently Added", @"selected":@"1", @"sortDir":@"DESC"}];
    [sort addObject:@{@"id":@"5", @"sort":@"viewcount", @"label" : @"Most Viewed", @"selected" : @"0", @"sortDir" : @"DESC"}];
    [sort addObject:@{@"id":@"2", @"sort":@"dateUpdated", @"label" : @"Recently Updated", @"selected" : @"0", @"sortDir":@"DESC"}];
    
    return sort;
}

- (ZMAsset *)getLibraryFilters {
    
    ZMAsset *assetFilter = [ZMAsset alloc];
    [assetFilter setState:0];
    ZMAssetsGroup *filter;
    ZMAssetsGroup *group;
    NSMutableArray *groups = [[NSMutableArray alloc] init];
    NSMutableArray *franchiseArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *franchise in _actions.franchisesAndBrands) {
        
        filter = [[ZMAssetsLibrary defaultLibrary] groupByName:[franchise objectForKey:@"name"]];
        
        for (NSDictionary *brands in [franchise objectForKey:@"brands"]) {
            group = [[ZMAssetsGroup alloc] init];
            
            if ([brands objectForKey:@"id"]) {
                group.uid = [[brands objectForKey:@"id"] integerValue];
            }
            
            if ([brands objectForKey:@"name"]) {
                group.name = [brands objectForKey:@"name"];
            }
            
            if(filter){
                group.related = @[filter];
            }
            [groups addObject:group];
        }
        
        [assetFilter setGroups:groups];
        if (filter) {
            [franchiseArray addObject:filter];
        }
    }
    assetFilter.franchises = [NSArray arrayWithArray:franchiseArray];
    
    if (!_actions.isAllCategorySelected) {
        
        NSMutableArray *cats = [[NSMutableArray alloc] init];
        
        for (NSDictionary *category in _actions.categories) {
            
            if([[category objectForKey:@"selected"] integerValue] == 1) {
                
                ZMCategory *categoryObject = [[ZMCategory alloc] init];
                categoryObject.categoryId = [[category objectForKey:@"id"] intValue];
                categoryObject.categoryName = [category objectForKey:@"name"];
                // Revamp - added for bucket selection
                categoryObject.bucketName = [category objectForKey:@"bucketname"];
                categoryObject.bucketId = [[category objectForKey:@"bucketid"] intValue];
                [cats addObject:categoryObject];
            }
        }
        if ([cats count] > 0) {
            assetFilter.categories = cats;
        }
    }
    return assetFilter;
}

#pragma mark - User Trackings

- (NSString *)getSelectedSort {
    
    NSString *selectedSort = @"";
    
    for (NSDictionary *sort in /*_actions.sorts*/ [ZMProntoManager sharedInstance].sortBy) {
        
        if ([[sort objectForKey:@"selected"] isEqualToString:@"1"]) {
            selectedSort = [sort objectForKey:@"label"];
        }
    }
    
    return [selectedSort lowercaseString];
}

- (NSString *)getCategoriesForTracking{
    
    NSString *categoriesFilters = @"";
    NSArray *selectedCats = [self getSelectedCategories];
    NSPredicate * predicateCategories = [NSPredicate predicateWithFormat:@"name == %@ AND selected == %@", @"All Franchise Assets", @"1"];
    NSArray * selectedAllCategories = [selectedCats filteredArrayUsingPredicate:predicateCategories];
    
    if (selectedAllCategories.count == 1) {
        categoriesFilters = @"All Franchise Assets";
    }
    else {
        if ([selectedCats count] == 0) {
            categoriesFilters = @"no selection";
        }
        else {
            for (NSDictionary *category in selectedCats) {
                categoriesFilters = [categoriesFilters stringByAppendingFormat:@"%@:", [category objectForKey:@"name"]];
            }
            categoriesFilters = [categoriesFilters substringToIndex:[categoriesFilters length]-1]; //Remove the extra semi colon
        }
    }
    
    return categoriesFilters;
}

#pragma mark - Notifications

- (void)registerNotification:(NSString *)name {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:name object:nil];
}

- (void)removeNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:name object:nil];
}

- (void)recievedNotification:(NSNotification *) notification{
    //
    if ([[notification name] isEqualToString:@"CMS_BRING_ALL_DATA"]) {
        [self defaultActions];
        [self bringLibraryData];
    }
    else if ([[notification name] isEqualToString:@"HIDE_LIBRARY_POPOVERS"]) {
        
        [self.headerView hidePopovers];
        [self resetPopOvers];
    }
    else if ([[notification name] isEqualToString:@"OPEN_TEMP_ASSET"]) {
        _resestFranchises = NO;
        [self openTempAsset:[notification.userInfo objectForKey:@"asset_link"]];
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET"]) {
        
        _resestFranchises = NO;
        Asset *asset = [notification.userInfo objectForKey:@"asset"];

        if([ZMUserActions sharedInstance].countOfAssetDict == 0)
        {
            [ZMUserActions sharedInstance].countOfAssetDict += 1;
            AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSArray *vcs = ((UINavigationController*)(mainDelegate.window.rootViewController)).viewControllers;
            ProntoTabbarViewController *tabBar = vcs.firstObject;
            
            if([ZMUserActions sharedInstance].globalSearchViewController)
            {
                [[ZMUserActions sharedInstance].globalSearchViewController validateAndShowAsset:asset];
            }
            else
            {
                if ([tabBar.currentController isKindOfClass:[ZMLibrary class]])
                {
                    [self validateAsset:asset];
                }

                else if ([tabBar.currentController isKindOfClass:[ToolsTraining class]])
                {
                    [((ToolsTraining*)tabBar.currentController) validateAndShowAsset:asset];
                }
                else if ([tabBar.currentController isKindOfClass:[EventsViewController class]])
                {
                    [((EventsViewController*)tabBar.currentController) validateAndShowAsset:asset];
                }
            }
        }
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_APP_CLOSED"]) {
        _resestFranchises = NO;
        
        //Once listen, Next time it should listen only after view will apper.
        NSString * idAsset = [notification.userInfo objectForKey:@"assetString"];
        
        //PRONTO-37 Deeplinking to a Page in a PDF
        // same format but caseinsensitive
        NSInteger pageNumber = 0;
        NSArray* deeplinkPage = [idAsset componentsSeparatedByString:@"&"];
        NSArray* deeplinkPageNo = [idAsset componentsSeparatedByString:@"="];
        NSString* stringToCheck = [NSString stringWithFormat:@"%@%@",@"pageNum=",[deeplinkPageNo lastObject]];
        if(deeplinkPage.count >1 )
        {
            if( isContainedIn(deeplinkPage,[deeplinkPage lastObject]))
            {
                idAsset = [idAsset stringByReplacingOccurrencesOfString:[deeplinkPage lastObject] withString:stringToCheck];
                NSArray* pageNums = [idAsset componentsSeparatedByString:@"&pageNum="];
                
                if (pageNums.count > 1) {
                    pageNumber = [[pageNums lastObject] integerValue];
                    idAsset = [pageNums firstObject];
                }
            }
        }
        
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:idAsset];
        if (assetArray.count > 0)
        {
            Asset * asset = assetArray[0];
            if ([asset.type isEqualToString:[EventOnlyAsset TypeAttributeValueAsString]])
            {
                EventOnlyAsset* eoaAssetValue = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:[asset.assetID stringValue]];
                [self openAssetDetailForDeeplinkEventOnlyAsset:eoaAssetValue];
            }
            else
            {
                //PRONTO-37 Deeplinking to a Page in a PDF
                [ZMUserActions sharedInstance].pageNo = pageNumber;
                [self openAssetDetailForDeeplinkAsset:asset];
                
            }
            // If asset found, No need to keep the reference of deepLinkUserInfo
            self.actions.deepLinkUserInfo = nil;
        }
        else
        {
            EventOnlyAsset* eoaAsset = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:idAsset];
            if(eoaAsset != nil)
            {
                [self openAssetDetailForDeeplinkEventOnlyAsset:eoaAsset];
                self.actions.deepLinkUserInfo = nil;
            }
            else
            {
                NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:notification.userInfo];
                [temp setValue:[NSNumber numberWithInteger:pageNumber] forKey:@"pageNumer"];
                self.actions.deepLinkUserInfo = temp;
                
                // Overwriting below properties to force start syncCMS call.
                self.shouldLoadAllData = YES;
                _apiManager._syncingInProgress = NO;
                syncWithCMSActive = NO;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:@"Getting Events"];
                [self bringLibraryData];
                [AbbvieLogging logInfo:@"Deep link -> Asset Not found"];
            }
        }
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_FOUND"]) {
        _resestFranchises = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            Asset *asset = [notification.userInfo objectForKey:@"asset"];
            enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
            
            if (assetType != ZMProntoFileTypesBrightcove) {
                [mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
               // asset.isUpdateAvailable = NO;
            }
            else
            {
                // for video file
                asset.isUpdateAvailable = NO;
            }
            
            [self validateAsset:asset];
        });
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_NOT_FOUND"]) {
        _resestFranchises = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [mainDelegate showMessage:@"Asset not found" whithType:ZMProntoMessagesTypeWarning];
        });
    }
    else if ([[notification name] isEqualToString:kAOFLogoutSuccessNotification]){
        _apiManager._syncingInProgress= NO;
        [self hideLaunchLoading];
        [self displayLogin];
    }
    else if ([[notification name] isEqualToString:kPreferenceListVCWillShowNotification])
    {
        [self loadPrefencesListViewController];
    }
    else if ([[notification name] isEqualToString:kPreferenceListVCDidSavePreferencesNotification])
    {
        self.isShowingPreferencesView   = NO;
        self.shouldLoadAllData          = YES;
        
        
        [self updateFranchieseViewFromPreferences:notification];
        multipleFranchisePromotedMessageDisplayed = NO;
        isSavePreferencesCompleted = YES;
    }
    else if ([[notification name] isEqualToString:kPreferenceListVCDismissNotification])
    {
        self.isShowingPreferencesView   = NO;
        self.shouldLoadAllData          = YES;
        [self bringLibraryData];
    }
    else if ([[notification name] isEqualToString:kSignOutOnLaunchNotification])
    {
        //Once listen, Next time it should listen only after view will apper.
        [self removeNotification:kSignOutOnLaunchNotification];
        [self signOutOnLaunchNotification:notification];
    }
    else if ([[notification name] isEqualToString:UIApplicationDidEnterBackgroundNotification])
    {
        //When app goes to background and come to foreground it should
        // refresh the local database i.e. sync with cms
        if (self.actions.currentSelectedTabIndex == SelectedTabFranchise && self.actions.globalSearchViewController == nil)
        {
            syncWithCMSActive = NO;
            _apiManager._syncingInProgress = NO;
        }
    }
}

// Test it for three scenario
// if a asset is open from deeplink url and it is not available in core database, it should sync local database and then it should open the asset
// Deep link from asset detail
// Deep link from browser
- (void)openAssetDetailForDeeplinkAsset:(Asset*)asset
{
    AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *vcs = ((UINavigationController*)(mainDelegate.window.rootViewController)).viewControllers;
    ProntoTabbarViewController *tabBar = vcs.firstObject;
    
    if([ZMUserActions sharedInstance].globalSearchViewController)
    {
        [[ZMUserActions sharedInstance].globalSearchViewController validateAndShowAsset:asset];
    }
    else
    {
        if ([tabBar.currentController isKindOfClass:[ZMLibrary class]])
        {
            [self validateAsset:asset];
        }

        else if ([tabBar.currentController isKindOfClass:[ToolsTraining class]])
        {
            [((ToolsTraining*)tabBar.currentController) validateAndShowAsset:asset];
        }
        else if ([tabBar.currentController isKindOfClass:[EventsViewController class]])
        {
            [((EventsViewController*)tabBar.currentController) validateAndShowAsset:asset];
        }
    }
}

- (void)openAssetDetailForDeeplinkEventOnlyAsset:(EventOnlyAsset*)eventOnlyAsset
{
    AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *vcs = ((UINavigationController*)(mainDelegate.window.rootViewController)).viewControllers;
    ProntoTabbarViewController *tabBar = vcs.firstObject;
    
    if([ZMUserActions sharedInstance].globalSearchViewController)
    {
        [[EventsServiceManager sharedServiceManager] validateAndShowEventOnlyAsset:eventOnlyAsset withFranchise:[ZMUserActions sharedInstance].globalSearchViewController.parentHeader.userFranchise.text andDelegate:(id <EventOnlyAssetHandlerDelegate>)[ZMUserActions sharedInstance].globalSearchViewController];
    }
    else
    {
        if ([tabBar.currentController isKindOfClass:[ZMLibrary class]])
        {
            [[EventsServiceManager sharedServiceManager] validateAndShowEventOnlyAsset:eventOnlyAsset withFranchise:self.headerView.userFranchise.text andDelegate:(id <EventOnlyAssetHandlerDelegate>)self];
        }

        else if ([tabBar.currentController isKindOfClass:[ToolsTraining class]])
        {
            [[EventsServiceManager sharedServiceManager] validateAndShowEventOnlyAsset:eventOnlyAsset withFranchise:((ToolsTraining*)tabBar.currentController).headerViewTools.userFranchise.text andDelegate:(id <EventOnlyAssetHandlerDelegate>)((ToolsTraining*)tabBar.currentController)];
        }
        else if ([tabBar.currentController isKindOfClass:[EventsViewController class]])
        {

        }
    }
}


- (void)showPreferencesListViewController
{
    if ([AOFLoginManager sharedInstance].checkForAOFLogin == YES)
    {
        self.preferencesListViewController = [PreferencesListViewController presentPreferencesViewControllerFromController:self];
    }
}

- (void) updateFranchieseViewFromPreferences:(NSNotification*)notification
{
    [self.franchiseList removeAllObjects];
    self.isHomeOfficeFranchiseSelected = NO;
    // Resetting the pagination value
    _pageIndex = 0;

    NSArray *franchiese = [self getAllFranchieseName];
    NSMutableArray *franchiseNode = [NSMutableArray array];
    NSMutableArray *brandsArray = [NSMutableArray array];
    
    if (franchiese != nil & franchiese.count > 0) // There is Franchiese
    {
        for (NSString *franchieseLabel in franchiese)
        {
            NSArray * franchiseArray = [[ZMCDManager sharedInstance] getFranchiseByName:franchieseLabel];
            if (franchiseArray.count > 0)
            {
                Franchise * franchise = franchiseArray.firstObject;
                if (franchise.franchiseID != nil)
                {
                    [self.franchiseList addObject:franchise.franchiseID];
                }
                NSMutableArray *tempBrandArray = [NSMutableArray array];
                for (Brand * brand in franchise.brands)
                {
                    if (brand.brandID != nil)
                    {
                        [self.franchiseList addObject:brand.brandID];
                    }
                    if (brand.name != nil)
                    {
                        [tempBrandArray addObject:brand.name];
                    }
                }
                
                if (tempBrandArray.count > 1)
                {
                    // If brand count is more than one, Then Include brand name
                    [brandsArray addObjectsFromArray:tempBrandArray];
                }
            }
            
            [franchiseNode addObjectsFromArray:[self franchiseToUpdateHeader:franchieseLabel]];
        }
        
        [ZMUserActions sharedInstance].brandsArray = brandsArray;
        [ZMProntoManager sharedInstance].franchisesAndBrands = self.franchiseList;
        [ZMProntoManager sharedInstance].franchisesToUpdateHeader =  franchiseNode;
    }
    else
    {
        [ZMProntoManager sharedInstance].franchisesAndBrands        = @[];
        [ZMProntoManager sharedInstance].franchisesToUpdateHeader   = @[];
        [ZMUserActions sharedInstance].brandsArray                  = @[];
    }
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"franchiseList count -> :%lu",(unsigned long)self.franchiseList.count]];
    
    [[ZMUserActions sharedInstance] setFranchisesAndBrands:[ZMProntoManager sharedInstance].franchisesAndBrands];
    [[ZMCDManager sharedInstance]getActiveCategories:^(NSArray * data) {
        [ZMProntoManager sharedInstance].categoriesToFilter = data;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Categories count %lu ",(unsigned long)data.count]];
        [self updateSortButton];
        [ZMProntoManager sharedInstance].selectedCategories = @[];
        [self.leftMenuTableView reloadData];
        
        if (notification != nil)
        {
            [self bringLibraryData];            
        }
        
        if (self.franchiseList.count <= 0)
        {
            [ZMUserActions sharedInstance].selectLeftMenu  = kAllFranchiseAssets;
            [self displayEmpty:NO];
        }
        [self performSelector:@selector(refreshLibrary) withObject:nil afterDelay:0.1];
        
        // if Global search controller is available
        //update Global search result on the basis of preferences changes
        if (self.actions.globalSearchViewController != nil)
        {
            [self.actions.globalSearchViewController reloadGlobalSearchResult];
        }
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([[[ProntoUserDefaults singleton]isReadyToMakeServiceCalls]isEqualToString:@"YES"])
        [self displayPromotedContent:@""];
    });
}



- (void)savePreferencesOnServerWithFranchieseAndNodes:(NSArray*)franchiesAndBrands
{
    [_apiManager savePreferences:franchiesAndBrands complete:^(NSDictionary *preferencesSavedStatus)
     {
         [AbbvieLogging logInfo:[NSString stringWithFormat:@"Saving preferences happen successfully:%@",preferencesSavedStatus]];
     } error:^(NSError *error)
     {
         [AbbvieLogging logError:[NSString stringWithFormat:@">>>> Error getting the while saving preferences %@", error]];
     }];
}

- (NSArray*)getAllFranchieseName
{
    NSString *userName = [AOFLoginManager sharedInstance].displayName;
    Preferences *preference = [Preferences MR_findFirstByAttribute:@"userName"
                                                         withValue:userName];
    return preference.franchieses;
}


// same format but caseinsensitive
BOOL isContainedIn(NSArray* pageNums, NSString* stringToCheck)
{
    for (NSString* string in pageNums) {
        if ([string caseInsensitiveCompare:stringToCheck] == NSOrderedSame)
            return YES;
    }
    return NO;
}

#pragma mark - ZMAssetsLibrary Observer methods for asset downloading

- (void)downloadAssetDidFinish:(Asset *)asset {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [mainDelegate hideMessage];
    });
    
    _isDownloadignAsset = NO;
    _isTapDownloadInitiated = NO;
    [mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", asset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self validateAsset:asset];
    });
}

- (void)downloadAssetDidNotFinish:(Asset *)asset error:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (mainDelegate.currentMessage){
            [mainDelegate hideMessage];
        }
        
        _isDownloadignAsset = NO;
        _isTapDownloadInitiated = NO;
        
        NSString *message;
        enum ZMProntoMessages type = 1;
        
        // NSInteger errCode = ABS(error.code);
        
        if (error.code == 503) {
            type = ZMProntoMessagesTypeWarning;
            message = @"The server is currently under maintenance mode.";
        }
        
        else if (![_apiManager isConnectionAvailable]) {
            type = ZMProntoMessagesTypeWarning;
            message = @"The Internet connection is not available, please check your network connectivity.";
        }
        else if(error.code == 401) {
            //Restoring the user session
            [mainDelegate initPronto];
            return;
        }
        else if (error.code == 404) {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"Asset not found %@", asset.title];
        }
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        else if (error.code == 5303 || error.code == -5303)
        {
            message = @"Error while retreiving assets, please try again later";
        }
        else if (error.code == 1009 || error.code == -1009)
        {
            message = @"Please check the network connection";
        }
        else if (error.code == 1003 || error.code == -1003)
        {
            message = @"A server with the specified hostname could not be found";
        }
        else {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"There is a problem downloading %@", asset.title];
        }
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [mainDelegate showMessage:message whithType:type withAction:^{
                [mainDelegate hideMessage];
                [self validateAsset:asset];
            }];
        });
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        
        
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        if ([message containsString:@"ERROR_CODE"])
        {
            // errorcode consists of digits
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",@"Please check the network connection"] forKey:@"scerrorcode"];
        }
        else
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",message] forKey:@"scerrorcode"];
        
        ZMGridItem *currentDownloadItem;
        for (ZMGridItem *cell in [grid visibleCells]) {
            
            if(currentAssetDownload.assetID.longValue == cell.uid) {
                currentDownloadItem = cell;
                break;
            }
        }
        currentDownloadItem.progress = 1;
        currentAssetDownload = nil;
        
        /**
         * @Tracking
         * Download Did Not finished
         **/
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"assetview" withName:nil withOptions:trackingOptions];
    });
}

- (void)assetsChanged {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        switch (_section) {
            case ProntoSectionTypeLibrary:
            {
                _pageIndex = 0;
                if (!newUserLogged) {
                    [self refreshLibrary];
                }
                else {
                    newUserLogged = NO;
                    [self backToDefault];
                }
                
               // [self getPopularAsset:^{}];
                break;
            }
            case ProntoSectionTypeBriefcase:
            default:
                [self loadBriefcase];
                break;
        }
    });
}





-(void) researchKeyword:(NSString*) keyword
{
    headerView.searchTextbox.text = keyword;

    if(keyword.length >= 3 && _apiManager._syncingCompletion)
    {
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_group_t group = dispatch_group_create();
        
        // Add a task to the group
        dispatch_group_async(group, queue, ^{
            
            [self bringLibraryData];
        });
        
        // Add a task to the group
        dispatch_group_async(group, queue, ^{
            
            [self performSelector:@selector(loadSearchedAssets) withObject:nil afterDelay:1.0];
        });
        
        // Add another task to the group
        dispatch_group_async(group, queue, ^{
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self performSelector:@selector(refreshTable) withObject:nil];
            });
        });
        
    }
    // [self loadSearchedAssets];
    
    headerView.searchTextbox.text = keyword;
    [[ZMProntoManager sharedInstance].search setTitle:keyword];
    [headerView hidingIndicator];
    
}

-(void) refreshTable
{
    if([ZMUserActions sharedInstance].assetsCount > 0)
    {
        [AbbvieLogging logInfo:@"refreshing table.."];
        [self hideEmpty];
        [self reloadAssets];
        [headerView hidingIndicator];
        
    }
}

- (IBAction)dismissView:(id)sender
{
    //removing overlay on button click
    [self hideScrollSpinner];
    
    UIView* removeView;
    
    while((removeView = [mainDelegate.window.rootViewController.view viewWithTag:5001]) != nil)
    {
        
        for(UIView *v in [mainDelegate.window.rootViewController.view subviews])
        {
            if([v viewWithTag:5002] || [v viewWithTag:5003] || [v viewWithTag:5004])
                [v removeFromSuperview];
        }
        
        [removeView removeFromSuperview];
    }
    
}

// New Revamping - Export Icon
- (IBAction)exportIconClicked:(UIButton*)sender
{
    // Pronto new revamp changes: setting selected/deselected share button's image
    if (sender.isSelected) {
        [sender setImage:[UIImage imageNamed:@"exportIcon"] forState: UIControlStateNormal];
        [sender setSelected:NO];
    } else {
        [sender setImage:[UIImage imageNamed:@"exportIconSelect"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        selectedItem = sender.tag;
        [self configureExportPopOver: sender];
    }
    //End
}

// Pronto new revamp changes: configuring export/share pop over
- (void) configureExportPopOver:(UIButton*) sender {
    FranchiseTableViewCell *listCell = nil;
    ZMGridItem *tempCell = nil;
    NSNumber * assetId = 0;
    CGRect frame = sender.frame;
    CGRect rect = CGRectZero;
    
    if ([sender.superview.superview.superview.superview.superview isEqual: promotedGrid]) {
        tempCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
        rect =  [tempCell convertRect: [promotedGrid frame] fromView: self.view];
        exportPopOverTappedonPromotedGrid = YES;
        exportPopOverTappedonListView = NO;
        exportPopOverTappedonGrid = NO;
    }
    if ([sender.superview.superview.superview.superview.superview isEqual: grid]) {
        tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
        rect =  [tempCell convertRect: [grid frame] fromView: self.view];
        exportPopOverTappedonGrid = YES;
        exportPopOverTappedonListView = NO;
        exportPopOverTappedonPromotedGrid = NO;
    }
    assetId = [NSNumber numberWithLong:tempCell.uid];
    frame.origin.x = 68;
    
    if (grid.hidden && ![sender.superview.superview.superview.superview.superview isEqual: promotedGrid]){
        //[sender.superview.superview.superview.superview isEqual: listTableView]) {
        listCell = (FranchiseTableViewCell *)[listTableView cellForRowAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
        assetId = [NSNumber numberWithLong:listCell.uid];
        frame.origin.x = 60;
        exportPopOverTappedonGrid = NO;
        exportPopOverTappedonListView = YES;
        exportPopOverTappedonPromotedGrid = NO;
        rect =  [listCell convertRect: [listTableView frame] fromView: self.view];
    }
    NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
    if (assetArray.count > 0) {
        _currentSelectedAsset = assetArray[0];
    }
    self.exportMenu = (ExportMenu *)[[[NSBundle mainBundle] loadNibNamed:@"ExportMenu" owner:self options:nil] objectAtIndex:0];
    self.exportMenu.library = self;
    [self.exportMenu selectingAssetWithDetails:_currentSelectedAsset withSender:sender];
    // self.exportMenu.parent = self;
    
    NSMutableArray * exportMutableArray =[[NSMutableArray alloc]init];
    // check and display Print Option
    if(_currentSelectedAsset.field_print.length > 0 && [_currentSelectedAsset.field_print isEqualToString:@"1"])
    {
        [exportMutableArray addObject:@"Print"];
    }
    [exportMutableArray addObject:@"Share"];
    // check and display Email to self Option
    if(_currentSelectedAsset.field_email_to_self.length > 0 && [_currentSelectedAsset.field_email_to_self isEqualToString:@"1"])
    {
        [exportMutableArray addObject:@"Email to self"];
    }
    [exportMutableArray addObject:@"Feedback"];
    
    self.exportMenu.exportArray = exportMutableArray;
    self.exportMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.exportMenu.preferredContentSize = CGSizeMake(178, [exportMutableArray count]*44);
    if (fabs(rect.origin.x) < 300 ) {
        self.exportMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomPopoverBackgroundView class];
    } else {
        frame.origin.x = 0;
        self.exportMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomInfoPopoverBackgroundView class];
    }
    [self presentViewController:self.exportMenu animated:YES completion:nil];
    
    // configure the Popover presentation controller
    _exportMenuPopOver = [self.exportMenu popoverPresentationController];
    _exportMenuPopOver.delegate = self;
    _exportMenuPopOver.sourceView = [sender superview];
    _exportMenuPopOver.sourceRect = frame;
    
    CGFloat deviceHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat value = (fabs(rect.origin.y) + ((exportMutableArray.count) * 44) ) + promotedContainerView.frame.size.height;
    if (deviceHeight > value) {
        _exportMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
    } else {
        _exportMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionDown;
    }
    
    int point = [Constant GetPointOnOrientation];
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown)
    {
        if(deviceHeight < value)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = value - deviceHeight;
        }
        if(fabs(rect.origin.y) > point)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 1;
        }
    }
    
    _exportMenuIsPresented = YES;
}
// End
- (void) updateAssetAndSendEventToServer {
    
    [[ZMCDManager sharedInstance] updateAsset: _currentSelectedAsset success:^{
        //Send the events to the server
        dispatch_async(dispatch_queue_create("Sending Events from ZMLibrary", NULL), ^{
            [[ZMAbbvieAPI sharedAPIManager] uploadEvents:^{
                [AbbvieLogging logInfo:@">>> Events Synced"];
            } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with uploadEvents: %@", error]];
            }];
        });
        
    } error:^(NSError * error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on ZMLibrary %@",error.localizedDescription]];
    }];
    
}

- (void)updateFavArrayID
{
    NSString *currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
    NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
    NSMutableArray *favArray = [[NSMutableArray alloc]init];
    for(FavFranchise *fav in favoritefranchise)
    {
        BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:fav.bucketName];
        if (isPresent  && fav.asset_ID != nil)
        {
            [favArray addObject:fav.asset_ID];
        }
    }
    
    // display the asset count on selecting Promoted asset as Fav asset
    if([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kFranchiseFavorites])
    {
        [headerView addBreadCumb:[ZMProntoManager sharedInstance].franchisesToUpdateHeader total: (int)favArray.count];
    }
    
    // check the empty array
    if(favArray.count > 0)
    {
        ZMProntoManager.sharedInstance.favArrayIds = favArray;
    }
    else
    {
        ZMProntoManager.sharedInstance.favArrayIds = @[];
    }
}

// New Revamping - Favorite Icon
- (IBAction)favIconClicked:(UIButton*)sender
{
    FranchiseTableViewCell *listCell = nil;
    ZMGridItem *tempCell = nil;
    NSNumber * assetId = 0;
    BOOL isPromotedContent = NO;
    
    // promoted content
    if ([sender.superview.superview.superview.superview.superview isEqual: promotedGrid])
    {
        tempCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
        isPromotedContent = YES;
    }
    // grid
    if ([sender.superview.superview.superview.superview.superview isEqual: grid]) {
        tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
    }
    assetId = [NSNumber numberWithLong:tempCell.uid];
    // list
    if (grid.hidden && !listTableView.hidden && ![sender.superview.superview.superview.superview.superview isEqual: promotedGrid])
    {
        listCell = (FranchiseTableViewCell *)[listTableView cellForRowAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
        assetId = [NSNumber numberWithLong:listCell.uid];
    }
    NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
    if (assetArray.count > 0) {
        _currentSelectedAsset = assetArray[0];
    }
    
    favIconTapped = YES;
    
    NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
    [favFranchiseObject setObject:assetId forKey:@"asset_ID"];
    [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionAsString] forKey:@"bucketName"];
//    [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];
    [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]
                           forKey:@"bucketID"];


    NSString *currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
    if (isPromotedContent == YES)
    {
        //Promoted content can be only favorites for on tab.
        currentBucketName = [self getBucketIdAsStringForPromotedContentAsset:self.currentSelectedAsset];
        [favFranchiseObject setObject:currentBucketName forKey:@"bucketName"];
    }
    
//    bool justAdded = false;
    
    // Revamp changes - for like function service call
    ZMEvent *ratingEvent = [[ZMEvent alloc] init];
    ratingEvent.assetId = _currentSelectedAsset.assetID.longValue;
    ratingEvent.eventType = ZMEventLike;
    NSString *rating = @"liked";
    NSInteger rate = 100;
    NSNumber *k = [NSNumber numberWithInt:1];
    NSNumber *totalVotes = [NSNumber numberWithInt:([k intValue] + [_currentSelectedAsset.votes intValue])];
    
    //Send the events to the server
    [self updateAssetAndSendEventToServer];
    
    if ([sender isSelected])
    {
        [sender setImage:[UIImage imageNamed:@"favIcon"] forState:UIControlStateNormal];
        [sender setSelected:NO];
        
        NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:assetId];
        if (favfranchise.count > 0)
        {
            //check wheather it belongs to other tab as well,
            //if yes, then remove only bucket name and update to database
            // if no, then directly remove it
            FavFranchise *favFranchise = favfranchise[0];
            NSString *franchiseBucketName = favFranchise.bucketName;
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameFromBucket:franchiseBucketName byRemovingBucketName:currentBucketName];
            if (modifiedFranchiseBucketName == nil)
            {
                // It means that It belongs from one bucket and therefore delete it
                [_actions deleteFavFranchise:favfranchise[0] complete:^{
                    [self updateFavArrayID];
                } error:^(NSError *error) {
                }];
            }
            else
            {
                // It means that it belongs to other bucket as well and therefore
                //Remove it for current and keep it for others
                favFranchise.bucketName = modifiedFranchiseBucketName;
                [_actions updatePersistentStoreWithCurrentContext];
                [self updateFavArrayID];
                [favFranchiseObject setObject:modifiedFranchiseBucketName forKey:@"bucketName"];
            }
            
            // update the count
            if(_actions.assetsCount >= 1 && [[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kFranchiseFavorites])
            {
                _actions.assetsCount = _actions.assetsCount-1;
                [headerView addBreadCumb:[ZMProntoManager sharedInstance].franchisesToUpdateHeader total: _actions.assetsCount];
            }
        }
        else
        {
            [AbbvieLogging logInfo:@"favfranchise array is empty"];
        }
        
        //Revamp(26/12) - Handling heart bounce animation
        // Revamp changes - Like Action during unselect
        if(_currentSelectedAsset.votes.intValue > 0)
        {
            _currentSelectedAsset.votes = [NSNumber numberWithInt:([_currentSelectedAsset.votes intValue]-[k intValue])];
        }
        else
        {
            _currentSelectedAsset.votes = [NSNumber numberWithInt:0];
        }
        _currentSelectedAsset.user_vote = @"0";
        ratingEvent.eventType = ZMEventUnlike;
        rating = @"disliked";
        rate = 0;
        //        sender.tag = 100;
    }
    else
    {
        /// two condition, if asset is available for favorite, it means it is favorite for other bucket
        // if not then, it is not favourite already, it should be added directly
        NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:assetId];
        if (favfranchise.count > 0)
        {
            FavFranchise *favFranchise = favfranchise[0];
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
            favFranchise.bucketName = modifiedFranchiseBucketName;
            [_actions updatePersistentStoreWithCurrentContext];
        }
        else
        {
            [_actions insertNewFavouriteFranchise:favFranchiseObject complete:^() {
                NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
                NSMutableArray *favArray = [[NSMutableArray alloc]init];
                for(FavFranchise *fav in favoritefranchise)
                {
                    BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:fav.bucketName];
                    if (isPresent  && fav.asset_ID != nil)
                    {
                        [favArray addObject:fav.asset_ID];
                    }
                }
                
                // display the asset count on selecting Promoted asset as Fav asset
                if([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kFranchiseFavorites])
                {
                    [headerView addBreadCumb:[ZMProntoManager sharedInstance].franchisesToUpdateHeader total: (int)favArray.count];
                }
                
                if(favArray.count > 0)
                {
                    ZMProntoManager.sharedInstance.favArrayIds = favArray;
                }
                else
                {
                    ZMProntoManager.sharedInstance.favArrayIds = @[];
                }
            
                double delayInSeconds = 0.6;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self updateFavGridList];
                });
            }
            error:^(NSError *error)
             {
             }];
        }
        
        //End of revamp
        [sender setImage:[UIImage imageNamed:@"favIconSelect"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        //Revamp(26/12) - Handling heart bounce animation
//        justAdded = true;
        
        // Revamp changes - Like Action during selection
        if(_currentSelectedAsset.votes.intValue >0)
            _currentSelectedAsset.votes = totalVotes;
        else
            _currentSelectedAsset.votes = [NSNumber numberWithInt:0];
        
        _currentSelectedAsset.user_vote = @"100";
        //        sender.tag = 101;
        
    }
    
    // // Revamp changes - like action - after the selection
    ratingEvent.eventValue = [NSString stringWithFormat:@"%ld", (long)rate];
    //Event created from bucket id
    if (isPromotedContent == YES)
    {
        ratingEvent.createdFromBucketId = [self getBucketIdForPromotedContentAsset:self.currentSelectedAsset];
    }
    else
    {
        ratingEvent.createdFromBucketId = [[Constant sharedConstant] getCurrentActiveTabOptionIDAsString].integerValue;
    }
    //Saves the rating to the local DB
    [[ZMAssetsLibrary defaultLibrary] saveEvent:ratingEvent];

    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    [[ZMCDManager sharedInstance] updateAsset:_currentSelectedAsset success:^{
        
        
        //Tracking
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                  withSubsection:@"assetview" withName:[NSString stringWithFormat:@"%@-rateit",[ZMAssetViewController getAssetType:_currentSelectedAsset]]
                     withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"sctierlevel":@"tier3", @"scrating":rating}]];
        
    } error:^(NSError * error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on ZMHeader %@",error.localizedDescription]];
    }];
    
    // bouncing animation of fav button
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.duration = .2;
    animation.repeatCount = 1;
    if (grid.hidden && listCell && ![sender.superview.superview.superview.superview.superview isEqual: promotedGrid]) {
        animation.fromValue = [NSValue valueWithCGPoint:listCell.favButton.center];
        animation.toValue = [NSValue valueWithCGPoint:CGPointMake(listCell.favButton.center.x, listCell.favButton.center.y-10)];
//        [listCell.favButton.layer addAnimation:animation forKey:@"position"];
    }
    if ([sender.superview.superview.superview.superview.superview isEqual: promotedGrid]) {
        animation.fromValue = [NSValue valueWithCGPoint:tempCell.favButton.center];
        animation.toValue = [NSValue valueWithCGPoint:CGPointMake(tempCell.favButton.center.x, tempCell.favButton.center.y-10)];
        
        //Revamp(26/12) - Handling heart bounce animation
        //[tempCell.favButton.layer addAnimation:animation forKey:@"position"];
    }
    if ([sender.superview.superview.superview.superview.superview isEqual: grid]) {
        animation.fromValue = [NSValue valueWithCGPoint:tempCell.favButton.center];
        animation.toValue = [NSValue valueWithCGPoint:CGPointMake(tempCell.favButton.center.x, tempCell.favButton.center.y-10)];
        //Revamp(26/12) - Handling heart bounce animation
        //[tempCell.favButton.layer addAnimation:animation forKey:@"position"];
    }
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    
    //Revamp(26/12) - Handling heart bounce animation
    if (grid.hidden) {
        [tempCell.favButton.layer addAnimation:animation forKey:@"position"];
        if(![sender.superview.superview.superview.superview.superview isEqual: promotedGrid]){
            [listCell.favButton.layer addAnimation:animation forKey:@"position"];
        }
    } else {
        [tempCell.favButton.layer addAnimation:animation forKey:@"position"];
    }
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    //if (![sender isSelected] && justAdded == false) {
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self updateFavGridList];
        [listTableView reloadData];
        if (isPromotedContent == NO)
        {
            [promotedGrid reloadData];
        }
    });
    //}
    
    [_leftMenuTableView reloadData];
    // [self updateAssetAndSendEventToServer];
    // [promotedGrid reloadData];
}

/**
 for promoted content, The promoted content asset bucket id will be considered in below priority
    a. if the assets belongs from all three bucket -> franchise bucket id will be considered
    b. if assets belongs from Competitor and Tools Training then competitors will be considered
    c. otherwise Tools trainig bucket id will be considered.
 */
- (NSInteger)getBucketIdForPromotedContentAsset:(Asset*)asset
{
    NSInteger bucketId =  [Constant sharedConstant].ProntoTabOptionFranchiseID.integerValue;
    NSArray *bucketIds = [Constant getStringsValuesSeperatedByUnderscoreFromString:asset.assetBucketId];
    
    if ([bucketIds containsObject:[Constant sharedConstant].ProntoTabOptionFranchiseID] == NO)
    {
        if (([bucketIds containsObject:[Constant sharedConstant].ProntoTabOptionCompetitorID] == YES))
        {
            bucketId =  [Constant sharedConstant].ProntoTabOptionCompetitorID.integerValue;
        }
        else
        {
            bucketId =  [Constant sharedConstant].ProntoTabOptionToolsAndTrainingID.integerValue;
        }
    }
    
    return bucketId;
}

- (NSString*)getBucketIdAsStringForPromotedContentAsset:(Asset*)asset
{
    NSString *bucketName =  ProntoTabOptionFranchise;
    NSArray *bucketIds = [Constant getStringsValuesSeperatedByUnderscoreFromString:asset.assetBucketId];
    
    if ([bucketIds containsObject:[Constant sharedConstant].ProntoTabOptionFranchiseID] == NO)
    {
        if (([bucketIds containsObject:[Constant sharedConstant].ProntoTabOptionCompetitorID] == YES))
        {
            bucketName =  ProntoTabOptionCompetitor;
        }
        else
        {
            bucketName =  ProntoTabOptionToolsAndTraining;
        }
    }
    
    return bucketName;
}

- (void) configureInfoPopOver:(UIButton*) sender {
    FranchiseTableViewCell *listCell = nil;
    ZMGridItem *tempCell = nil;
    NSNumber * assetId = 0;
    CGRect rect = CGRectZero;
    
    if ([sender.superview.superview.superview.superview.superview isEqual: promotedGrid]) {
        tempCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
        rect =  [tempCell convertRect: [promotedGrid frame] fromView: self.view];
        infoPopOverTappedonPromotedGrid = YES;
        infoPopOverTappedonListView = NO;
        infoPopOverTappedonGrid = NO;
    }
    if ([sender.superview.superview.superview.superview.superview isEqual: grid]) {
        tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
        rect =  [tempCell convertRect: [grid frame] fromView: self.view];
        infoPopOverTappedonGrid = YES;
        infoPopOverTappedonListView = NO;
        infoPopOverTappedonPromotedGrid = NO;
    }
    assetId = [NSNumber numberWithLong:tempCell.uid];
    
    if (grid.hidden && ![sender.superview.superview.superview.superview.superview isEqual: promotedGrid])
        //[sender.superview.superview.superview.superview isEqual: listTableView])
    {
        listCell = (FranchiseTableViewCell *)[listTableView cellForRowAtIndexPath: [NSIndexPath indexPathForItem:sender.tag inSection:0]];
        assetId = [NSNumber numberWithLong:listCell.uid];
        infoPopOverTappedonGrid = NO;
        infoPopOverTappedonListView = YES;
        infoPopOverTappedonPromotedGrid = NO;
        rect =  [listCell convertRect: [listTableView frame] fromView: self.view];
    }
    
    if (!self.infoMenu) {
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
        if (assetArray.count > 0) {
            _currentSelectedAsset = assetArray[0];
        }
        
        self.infoMenu = (InfoMenu *)[[[NSBundle mainBundle] loadNibNamed:@"InfoMenu" owner:self options:nil] objectAtIndex:0];
//        self.infoMenu.library = self;
        
        // expiredDate
        NSNumber* expiredOn = _currentSelectedAsset.unpublish_on;
        NSTimeInterval timeInterval = [expiredOn doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd/MM/yyyy"];
        NSString* dateString = [df stringFromDate:date];
        
        // lastUpdated Date
        
        NSNumber* lastUpdated = _currentSelectedAsset.changed;
        NSTimeInterval lastTimeInterval = [lastUpdated doubleValue];
        NSDate *lastDate = [NSDate dateWithTimeIntervalSince1970:lastTimeInterval];
        //NSDateFormatter *df = [[NSDateFormatter alloc] init];
        // [df setDateFormat:@"dd/MM/yyyy"];
        NSString* lastDateString = [df stringFromDate:lastDate];
        
        // [self.infoMenu selectingAssetWithDetails:_currentSelectedAsset withSender:sender];
        if (grid.hidden && !listTableView.hidden)
            self.infoMenu.totalViewsCount = [NSString stringWithFormat:@"%d",_currentSelectedAsset.view_count.intValue];
        else
            self.infoMenu.totalViewsCount = [NSString stringWithFormat:@"%d",tempCell.totalViews];
        self.infoMenu.expiredDate = dateString;
        self.infoMenu.lastUpdatedDate = lastDateString;
        self.infoMenu.medRegNo = _currentSelectedAsset.medRegNo;
    }
    
    NSMutableArray * infoMutableArray =[[NSMutableArray alloc]init];
    [infoMutableArray addObject:@"Number Of Views"];
    [infoMutableArray addObject:@"Med/Reg Number"];
    [infoMutableArray addObject:@"Last Updated"];
    [infoMutableArray addObject:@"Expiration Date"];
    
    self.infoMenu.infoArray = infoMutableArray;
    
    self.infoMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.infoMenu.popoverPresentationController.sourceRect = [sender frame];
    self.infoMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomInfoPopoverBackgroundView class];
    self.infoMenu.preferredContentSize = CGSizeMake(158, [infoMutableArray count]*44);
    [self presentViewController:self.infoMenu animated:YES completion:nil];
    
    // configure the Popover presentation controller
    self.infoMenuPopOver = [self.infoMenu popoverPresentationController];
    
    CGFloat deviceHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat value = (fabs(rect.origin.y) + ((infoMutableArray.count) * 44) ) + promotedContainerView.frame.size.height;
    if (deviceHeight > value) {
        self.infoMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
    } else {
        self.infoMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionDown;
    }
    
    if ([sender.superview.superview.superview.superview.superview isEqual: promotedGrid]) {
        self.infoMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    
    self.infoMenuPopOver.delegate = self;
    self.infoMenuPopOver.sourceView = [sender superview];
    
    self.infoMenuPopOver.sourceRect = sender.frame;
    
    int point = [Constant GetPointOnOrientation];
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown)
    {
        if(deviceHeight < value)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = value - deviceHeight;
        }
        if(fabs(rect.origin.y) > point)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 1;
        }
    }
    _infoMenuIsPresented = YES;
}

// New Revamping - Info Icon
- (IBAction)infoIconClicked:(UIButton*)sender
{
    // Pronto new revamp changes: setting selected/deselected info button's image
    if (sender.isSelected) {
        [sender setImage:[UIImage imageNamed:@"infoIcon"] forState: UIControlStateNormal];
        [sender setSelected:NO];
    } else {
        [sender setImage:[UIImage imageNamed:@"infoIconSelect"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        selectedItem = sender.tag;
        [self configureInfoPopOver: sender];
    }
    //End
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    if(_exportMenuIsPresented) {
        if (grid.hidden) {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.shareButton.isSelected) {
                [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
                [cell.shareButton setSelected:NO];
            }
        }
        
        ZMGridItem *tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (tempCell && tempCell.shareButton.isSelected) {
            [tempCell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [tempCell.shareButton setSelected:NO];
        }
        ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (proCell && proCell.shareButton.isSelected) {
            [proCell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
            [proCell.shareButton setSelected:NO];
        }
        exportPopOverTappedonGrid = NO;
        exportPopOverTappedonPromotedGrid = NO;
        exportPopOverTappedonListView = NO;
        
        _exportMenuIsPresented = NO;
        [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
    }
    
    if(_infoMenuIsPresented) {
        if (grid.hidden) {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[listTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.infoButton.isSelected) {
                [cell.infoButton setImage:[UIImage imageNamed:@"infoIcon"] forState:UIControlStateNormal];
                [cell.infoButton setSelected:NO];
            }
        }
        ZMGridItem *tempCell = (ZMGridItem *)[grid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (tempCell && tempCell.infoButton.isSelected) {
            [tempCell.infoButton setImage:[UIImage imageNamed:@"infoIcon"] forState:UIControlStateNormal];
            [tempCell.infoButton setSelected:NO];
        }
        ZMGridItem *proCell = (ZMGridItem *)[promotedGrid cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
        if (proCell && proCell.infoButton.isSelected) {
            [proCell.infoButton setImage:[UIImage imageNamed:@"infoIcon"] forState:UIControlStateNormal];
            [proCell.infoButton setSelected:NO];
        }
        infoPopOverTappedonGrid = NO;
        infoPopOverTappedonPromotedGrid = NO;
        infoPopOverTappedonListView = NO;
        _infoMenuIsPresented = NO;
        [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
    }
    
    
    
}

// New Revamping - Download selected asset
- (void)downloadOneAsset:(Asset *)selectedAsset completion:(void (^)(BOOL assetDownloaded))completion
                 failure:(void (^)(NSString *assetDownloadFail))failure {
    
    currentAssetDownload = selectedAsset;
    _isDownloadignAsset = YES;
    [self setProgress:0.001000];
    
    [_apiManager downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        //[self downloadAssetDidFinish:selectedAsset];
//        [mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", selectedAsset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
        _isDownloadignAsset = NO;
        completion(YES);
    } onError:^(NSError *error) {
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        _isDownloadignAsset = NO;
        completion(NO);
    }];
}

// Pronto new revamping changes
- (IBAction)viewChangesButtonTapped:(id)sender {
    if (grid.hidden) {
        grid.hidden = false;
        listTableView.hidden = true;
        [listGridbutton setImage:[UIImage imageNamed:@"listIcon"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:keyUserDefaultGridFranchise];
    } else {
        grid.hidden = true;
        listTableView.hidden = false;
        listTableView.delegate = self;
        listTableView.dataSource = self;
        [listGridbutton setImage:[UIImage imageNamed:@"thumbnailIcon"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:keyUserDefaultGridFranchise];
    }
    [self reloadAssets];
}

// Pronto new revamping changes: reloading assests
- (void) reloadAssets {
    if (grid.hidden) {
        [listTableView reloadData];
    } else {
        [grid reloadData];
    }
}

// To fetch the fav assets from CMS
-(void) fetchEvents
{
    
    [self removeStartingProntoView];
    
    dispatch_async(dispatch_queue_create("Fetching Fav Assets", NULL), ^{
        [[ZMAbbvieAPI sharedAPIManager] fetchEvents:^(NSDictionary*FavAssets){

            [AbbvieLogging logInfo:[NSString stringWithFormat:@" Fav fetched:%@",FavAssets]];
            if(FavAssets.count > 0)
            {
                NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchiseWithoutFilter];
                for(NSNumber *ID in FavAssets) {
                    bool isPresent = false;
                    for(FavFranchise *fav in favoritefranchise) {
                        if([ID doubleValue] == [fav.asset_ID doubleValue]) {
                            isPresent = true;
                            break;
                        }
                    }
                    if(FavAssets.count == 1 && [ID intValue] == 1)
                    {
                        isPresent = true;
                    }
                    if(!isPresent)
                    {
                        NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
                        [favFranchiseObject setObject:ID forKey:@"asset_ID"];
                         //Changes for Removing Warnings
//                        NSMutableDictionary * bucketDictionary = [[NSMutableDictionary alloc]init];
//                        bucketDictionary = [FavAssets objectForKey:ID];
                        
                        //Changes for Removing Warnings
                        NSString *bucketNames = [self getBucketNamesForFavAssetItBelongsTo:[FavAssets objectForKey:ID]];
                        [favFranchiseObject setObject:bucketNames forKey:@"bucketName"] ;
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Fav inserting -> %@",favFranchiseObject]];
                        [_actions insertNewFavouriteFranchise:favFranchiseObject complete:^{
                        } error:^(NSError *error) {
                        }];
                    }
                }
                
                [_leftMenuTableView reloadData];
                [self displayPromotedContent:@""];
            }
        } error:^(NSError *error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with Fetching Fav Assets: %@", error]];
        }];
    });
}

-(NSString*)getBucketIDsForFavAssetItBelongsTo:(NSArray*)values
{
    NSString *bucketIDs = nil;
    NSString *defaultBucketId = [[Constant sharedConstant] getCurrentActiveTabOptionIDAsString];
    
    if (values != nil && [values isKindOfClass:[NSArray class]])
    {
        for (NSDictionary *dict in values)
        {
            NSString *bucketIdValue = [dict valueForKey:@"bucket_id"];
            if ([bucketIdValue isKindOfClass:[NSNumber class]])
            {
                bucketIdValue = [NSString stringWithFormat:@"%@", bucketIdValue];
            }
            if ([bucketIdValue isEqualToString:@""])
            {
                // then assign default value
                bucketIdValue = defaultBucketId;
                if (bucketIDs == nil)
                {
                    bucketIDs = bucketIdValue;
                }
                else
                {
                    bucketIDs = [NSString stringWithFormat:@"%@_%@",bucketIDs,bucketIdValue];
                }
            }
            else
            {
                if (bucketIDs == nil)
                {
                    bucketIDs = bucketIdValue;
                }
                else
                {
                    bucketIDs = [NSString stringWithFormat:@"%@_%@",bucketIDs,bucketIdValue];
                }
            }
        }
    }
    
    return bucketIDs;
}

-(NSString*)getBucketNamesForFavAssetItBelongsTo:(NSArray*)values
{
    NSString *bucketNames = nil;
    NSString *defaultBucketName = [Constant GetCurrentActiveTabOptionAsString];
    
    if (values != nil && [values isKindOfClass:[NSArray class]])
    {
        for (NSDictionary *dict in values)
        {
            NSString *bucketNameValue = [dict valueForKey:@"bucket_name"];
            if ([bucketNameValue isEqualToString:@""])
            {
                // then assign default value
                bucketNameValue = defaultBucketName;
                if (bucketNames == nil)
                {
                    bucketNames = bucketNameValue;
                }
                else
                {
                    bucketNames = [NSString stringWithFormat:@"%@_%@",bucketNames,bucketNameValue];
                }
            }
            else
            {
                if (bucketNames == nil)
                {
                    bucketNames = bucketNameValue;
                }
                else
                {
                    bucketNames = [NSString stringWithFormat:@"%@_%@",bucketNames,bucketNameValue];
                }
            }
        }
    }
    
    // if there is mismatch in datasource assign defaultBucketName
    if (bucketNames == nil)
    {
        bucketNames = defaultBucketName;
    }
    return bucketNames;
}

-(void) changeTintColor
{
    refreshControl.tintColor = [UIColor clearColor];
}

- (void)loadPrefencesListViewController
{
    [self.headerView hidePopovers];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self.preferencesListViewController = [PreferencesListViewController presentPreferencesViewControllerFromController:self];
        
        if (self.isHomeOfficeFranchiseSelected)
        {
            // MARK Home Office Franchise Selected.
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.preferencesListViewController markSelectedToHomeOfficeFranchiese];
            });
        }
    });
}

// Display User Preferences
-(void) checkUserPreferences
{
    if(headerView.userFranchise.text.length > 0) {
        if([headerView.userFranchise.text  isEqual: @"In House"] || [headerView.userFranchise.text  isEqual: @"Home Office"]) {
            [AbbvieLogging logInfo:@"home office user need the modal"];

            [mainDelegate showHideOverlay:YES];
            [self.checkUser invalidate];
            self.checkUser = nil;
        }
    }
}

// Clear the user data while logout
-(void) clearUserData
{
    headerView.userFranchise.text = @"";
    headerView.userName.text = @"";
    [self loadLibraryEmpty];
    _actions.assetsCount = 0;
    [ZMUserActions sharedInstance].franchiseColor = @"#00000";
    [self.promotedContentArray removeAllObjects];
    [self.promotedContentAssets removeAllObjects];
    promotedPageControl.currentPage = 0;
    promotedPageControl.hidden = YES;
    self.shouldLoadAllData = NO;
    syncWithCMSActive = NO;
    [ZMUserActions sharedInstance].shouldShowFranchiseListView = YES;
    [ZMUserActions sharedInstance].shoudRefreshEventData = YES;
    [ZMUserActions sharedInstance].currentSelectedCell = nil;
    [[ProntoUserDefaults userDefaults] setPreviousAsset:nil];
    [notificationsPannel hideNotificationPannel];
    [self.progressTimer invalidate];
    self.progressTimer = nil;
    [[AssetDownloadProvider sharedAssetDownloadProvider] clearOnLogout];
//    [ZMUserActions sharedInstance].shouldShowInitialWalkthrough = NO;
    [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] clearOnLogout];
    [[ZMUserActions sharedInstance] resetUserActionSharedInstanceValuesOnLogout];
}

- (void)resetGlobalSearchFromLibrary
{
    [self.globalSearcVC dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
}

- (ZMLibrary*)getCurrentVisibleLibraryInstance
{
    AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *vcs = ((UINavigationController*)(mainDelegate.window.rootViewController)).viewControllers;
    ProntoTabbarViewController *tabVC = vcs.firstObject;
    if ([tabVC isKindOfClass:[ProntoTabbarViewController class]])
    {
        return tabVC.library;
    }
    return nil;
}

#pragma mark - SignOutLaunch Methods
- (void)signOutOnLaunchNotification:(NSNotification*)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
    AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *vcs = ((UINavigationController*)(mainDelegate.window.rootViewController)).viewControllers;
    ProntoTabbarViewController *tabVC = vcs.firstObject;
    if ([tabVC isKindOfClass:[ProntoTabbarViewController class]])
    {
        if (vcs != nil && vcs.count > 1 &&
            [vcs.lastObject isKindOfClass:[UIViewController class]])
        {
            //If user has logged out from asset detail page or Help screen.
            // First poping the asset detail page or Help screen and coming back to
            //Root view controller.
            if([vcs.lastObject isKindOfClass:[HelpViewController class]])
            {
                [ZMUserActions sharedInstance].isLoggedoutFromHelp = YES;
                HelpViewController *helpVC = (HelpViewController*)vcs.lastObject;
                [helpVC pauseVideoPlayerResetTheValue];
            }
            
            if ([vcs.lastObject isKindOfClass:[ZMLogin class]] == NO)
            {
                [[NSUserDefaults standardUserDefaults]setInteger:3 forKey:@"waitTime"];
                [((UINavigationController*)(mainDelegate.window.rootViewController)) popToRootViewControllerAnimated:NO];
                
//                [((UINavigationController*)(mainDelegate.window.rootViewController)) popViewControllerAnimated:NO];
            }
        }
        [self removeGlobalSearchViewControllerFromTabBarHierarchy:tabVC];
        if (tabVC.library == tabVC.currentController)
        {
            //Resetting the datasource to show empty screen.
            [tabVC.library.assetsPage addObjectsFromArray:@[]];
            [ZMProntoManager sharedInstance].categoriesToFilter = @[];
            tabVC.library.actions.categories = @[];
            [self performSelector:@selector(doCleanUpAfterLogout:) withObject:tabVC.library afterDelay:0.2];
        }
        else
        {
            [tabVC makeLibraryViewControllerVisible];
            [self performSelector:@selector(doCleanUpAfterLogout:) withObject:tabVC.library afterDelay:0.2];
            if([vcs.lastObject isKindOfClass:[HelpViewController class]])
            {
                [ZMUserActions sharedInstance].isLoggedoutFromHelpOtherTab = YES;
            }
        }
    }
    });
}

- (void)removeGlobalSearchViewControllerFromTabBarHierarchy:(ProntoTabbarViewController*)tabBar
{
    NSArray *childControllers = tabBar.currentController.childViewControllers;
    BOOL isGlobalSearchPresent = NO;
    for (UIViewController *vc in [childControllers reverseObjectEnumerator])
    {
        if ([vc isKindOfClass:[GlobalSearchVC class]])
        {
            isGlobalSearchPresent = YES;
            break;
        }
    }
    
    if (isGlobalSearchPresent == YES)
    {
        if ([tabBar.currentController isKindOfClass:[ZMLibrary class]])
        {
            [((ZMLibrary*)tabBar.currentController) resetGlobalSearchFromLibrary];
        }

        else if ([tabBar.currentController isKindOfClass:[ToolsTraining class]])
        {
            [((ToolsTraining*)tabBar.currentController) resetGlobalSearchFromToolsTraining];
        }
    }
}

- (void)doCleanUpAfterLogout:(ZMLibrary*)library
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([AOFLoginManager sharedInstance].checkForAOFLogin)
        {
            [[AOFLoginManager sharedInstance] AOFKitLogout];
        }
    });
    [ZMUserActions sharedInstance].shouldShowFranchiseListView    = YES;
    [ZMUserActions sharedInstance].shoudRefreshEventData = YES;
    [ZMUserActions sharedInstance].currentSelectedTabIndex = 0;
    [ZMUserActions sharedInstance].isLoggedoutFromHelp = NO;
    [ZMProntoManager sharedInstance].categoriesToFilter = @[]; // left menu will have no data
    
    library.actions.categories = @[];
    [self dismissViewControllerAnimated:YES completion:NULL];
    [library clearUserData];
    
    self.actions.shouldSwitchToOtherTab = YES;
    
    //ViewedAsset
    [[ProntoUserDefaults userDefaults] setViewedAssetFetchedFromCMSValue:NO];
    [[ProntoUserDefaults userDefaults] setViewedEventOnlyAssetIds:@[]];
    [[ProntoUserDefaults userDefaults] setViewedNormalAssetIds:@[]];
    [[ProntoUserDefaults  userDefaults] setEventV3RequestMadeFromCMSValue:NO];
    
    self.isShowingPreferencesView = NO;
}
    
#pragma mark - Event Only Asset Open
- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler openAsset:(EventOnlyAsset*)selectedAsset
    {
        if (self.eventOnlyAssetHandler == nil)
        {
            self.eventOnlyAssetHandler = [[EventOnlyAssetHandler alloc]init];
            self.eventOnlyAssetHandler.delegate      = (id <EventOnlyAssetHandlerDelegate>)self;
        }
        
        //Asset Details accepts only Asset object and it is very tighly coupled with controller.
        //So, Instead of creating new asset detail controller, we are passing equivalent object of eventOnlyAsset.
        Asset *equivalentAssetForEventOnlyAsset = [selectedAsset getEquivalentAssetObject];
        
        if(self.headerView.categoryPopOver)
        {
            [[self.headerView.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        }
        if(self.headerView.sortMenuPopOver)
        {
            [[self.headerView.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        }
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.eoa_path]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.eventOnlyAssetHandler.mainDelegate hideMessage];
        });
        
        enum ZMProntoFileTypes assetType = [selectedAsset getFileTypeOfEventOnlyAsset];
        
        self.eventOnlyAssetHandler.isDownloadingAsset = NO;
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UIViewController <ZMAssetViewControllerDelegate> *controller;
        
        MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
        if (assetType == ZMProntoFileTypesPDF)
        {
            if(document != nil)
            {
                [self openFastPDFForEquivalentEventOnlyAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
            }
            else
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.eventOnlyAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                    self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
                });
            }
        }
        else if (assetType == ZMProntoFileTypesBrightcove)
        {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
            ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
//            controller.library = self;
            controller.asset = equivalentAssetForEventOnlyAsset;
            controller.userFranchise = self.headerView.userFranchise.text;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (assetType == ZMProntoFileTypesDocument)
        {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
            ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
//            controller.library = self;
            controller.asset = equivalentAssetForEventOnlyAsset;
            controller.userFranchise = self.headerView.userFranchise.text;
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        else if(assetType == ZMProntoFileTypesWeblink)
        {
            // open the weblink URL
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
            ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
//            controller.library = self;
            controller.asset = equivalentAssetForEventOnlyAsset;
            controller.userFranchise = self.headerView.userFranchise.text;
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        else
        {
            if (assetType == ZMProntoFileTypesPDF)
            {
                if(selectedAsset.eoa_path)
                {
                    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
                    if(document != nil)
                    {
                        [self openFastPDFForEquivalentEventOnlyAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
                    }
                    else
                    {
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [self.eventOnlyAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                            self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
                        });
                    }
                }
            }
        }
        
    }

- (void)openFastPDFForEquivalentEventOnlyAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
//    controller.library = self;
    controller.asset = asset;
    
    ((FastPDFViewController*)controller).isEventOnlyAsset = YES;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    //    [view addSubview:controller.view];
    
    [viewController.view addSubview:view];
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    //    controller.view.frame = view.bounds;
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
    
    //    [controller didMoveToParentViewController:viewController];
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor GetColorFromHexValueE0E6ED];
    [viewController.view addSubview:webview];
    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    if (self.eventOnlyAssetHandler != nil)
    {
        [self.eventOnlyAssetHandler hideMessage];
    }
    
    controller.userFranchise = self.headerView.userFranchise.text;
    controller.viewMain = view;
    controller.webviewScreen = webview;
    controller.webviewScreen.hidden = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (void)refreshLibraryDataFromCMS
{
        // Overwriting below properties to force start syncCMS call.
        self.shouldLoadAllData = YES;
        _apiManager._syncingInProgress = NO;
        syncWithCMSActive = NO;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self bringLibraryData];
}
    
#pragma mark - Force Logout refresh View initialization  and Delegate Methods

- (void)showForceLogoutRefreshView:(BOOL)show
{
    if (show == YES)
    {
        if (self.forceLogoutRefreshView == nil)
        {
            self.forceLogoutRefreshView = [ForceLogoutRefreshView GetForceLogoutRefreshView];
        }
        self.forceLogoutRefreshView.delegate = (id<ForceLogoutRefreshViewDelegate>)self;
        [self.view addSubview:self.forceLogoutRefreshView];
        [self.forceLogoutRefreshView adjustFrameWithRespectToSuperView];
        
    }
    else
    {
        [self.forceLogoutRefreshView removeFromSuperview];
        self.forceLogoutRefreshView = nil;
    }
    
    self.isRefreshButtonTapped = NO;
}

- (void)forceLogoutRefreshView:(ForceLogoutRefreshView*)forceLogoutRefreshView didTapRefreshButton:(UIButton*)refreshButton
{
    if(self.isPreferncesOrFranchiseBrandsNetworkRequestInProgress == YES)
    {
        self.isRefreshButtonTapped = YES;
        return;
    }
    
    [self showForceLogoutRefreshView:NO];
    [self.forceLogoutRefreshView showErroView:NO];
    if([_apiManager isConnectionAvailable])
    {
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingAssets];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        loadingIndicatorNew.loadingIndicatorLabel.text = @"Refreshing Events";
        [loadingIndicatorNew.activityIndicator startAnimating];
    });
    
    [self makeForceRefreshCMSRequest];
}

- (void)makeForceRefreshCMSRequest
{
    // Overwriting below properties to force start syncCMS call.
    self.shouldLoadAllData = YES;
    _apiManager._syncingInProgress = NO;
    syncWithCMSActive = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self bringLibraryData];
}


- (void)forceLogoutRefreshView:(ForceLogoutRefreshView*)forceLogoutRefreshView didTapLogoutButton:(UIButton*)logoutButton
{
    self.isShowingPreferencesView = NO;
    self.shouldLoadAllData = NO;

    [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingAssets];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSignOutOnLaunchNotification object:nil];
}

@end

