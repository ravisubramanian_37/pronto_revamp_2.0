//
//  ZMLogin.h
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMProntoViewController.h"

@interface ZMLogin : ZMProntoViewController
    /**
     * Closes the Login ViewController if there is a valid session
     **/
    -(void) close;
@end
