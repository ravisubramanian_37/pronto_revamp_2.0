//
//  SessionsView.h
//  CalendarDetailPOC
//
//  Created by Praveen Kumar on 21/08/18.
//  Copyright © 2018 Praveen Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SessionsView;
@class AssetView;
@class SessionsCell;

@class Event;
@class EventDuration;



@protocol EventsSessionsViewDelegate<NSObject>

- (void)sessionsView:(SessionsView*)sessionView didSelectCell:(SessionsCell*)cell withAssetView:(AssetView*)assetViewVal;

@end

@interface SessionsView : UIView

@property (weak, nonatomic) IBOutlet UITableView *sessionsTableView;

+ (SessionsView*)GetSessionsView;
+ (SessionsView*)GetSessionsViewFrame:(CGRect)frame;
+ (SessionsView*)GetSessionsViewFrame:(CGRect)frame withDurations:(NSArray*)durations;

-(void) reloadData;
-(void) scrollTableToTop;

@property (nonatomic, weak) id <EventsSessionsViewDelegate> delegate;

@property (nonatomic, strong) Event *selectedEvent;
@property (nonatomic, strong) NSArray *durations;


@end
