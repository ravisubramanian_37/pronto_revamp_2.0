//
//  SessionsView.m
//  CalendarDetailPOC
//
//  Created by Praveen Kumar on 21/08/18.
//  Copyright © 2018 Praveen Kumar. All rights reserved.
//

#import "SessionsView.h"
#import "SessionsCell.h"
#import "AssetView.h"


#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"


@interface SessionsView()
@property(nonatomic, assign) CGFloat heightOfTableContent;

@end

@implementation SessionsView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
    
    self.sessionsTableView.dataSource = (id<UITableViewDataSource>)self;
    self.sessionsTableView.delegate   = (id<UITableViewDelegate>)self;
}

+ (SessionsView*)GetSessionsView
{
    return (SessionsView*)[[[NSBundle mainBundle] loadNibNamed:@"SessionsView" owner:self options:nil] lastObject];
}

+ (SessionsView*)GetSessionsViewFrame:(CGRect)frame
{
    SessionsView *sessionsView = [self GetSessionsView];
    sessionsView.frame = frame;
    
    return sessionsView;
}

+ (SessionsView*)GetSessionsViewFrame:(CGRect)frame withDurations:(NSArray*)durations
{
    SessionsView *sessionsView = [self GetSessionsView];
    sessionsView.frame      = frame;
    sessionsView.durations  = durations;
    
    return sessionsView;
}

- (void)setDurations:(NSArray *)durations
{
    _durations = durations;
    [self reloadData];
}

-(void) scrollTableToTop
{
    NSIndexPath *topItem = [NSIndexPath indexPathForItem:0 inSection:0];
    [self.sessionsTableView scrollToRowAtIndexPath:topItem atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)setupUI
{
    //Registering the cell with table view
    [self.sessionsTableView registerNib:[UINib nibWithNibName:[SessionsCell NibName] bundle:nil] forCellReuseIdentifier:[SessionsCell CellIdentifier]];
    
    self.heightOfTableContent = 0;
    self.backgroundColor = [UIColor clearColor];
}

-(void) reloadData
{
    self.heightOfTableContent = 0;
    [self.sessionsTableView reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource and UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger returnVal = self.durations.count;
    NSInteger heightOfTable = [self calculateHeightOfTable];
    NSInteger overallHeight = self.sessionsTableView.frame.size.height;
    if(overallHeight > heightOfTable)
    {
        returnVal += 1;
    }
    return returnVal;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SessionsCell *cell = [tableView dequeueReusableCellWithIdentifier:[SessionsCell CellIdentifier]];
    cell.delegate = (id<EventsSessionsCellDelegate>)self;
    if (self.durations.count > 0 && indexPath.row < self.durations.count)
    {
        [cell prepareCellWithEventDuration:self.durations[indexPath.row]];
    }
    else
    {
        [cell shouldShowEmptyCell:YES];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SessionsCell *cell = [tableView dequeueReusableCellWithIdentifier:[SessionsCell CellIdentifier]];
    
    CGFloat heightOfCell = 0.0;
    
    if(indexPath.row < self.durations.count)
    {
        heightOfCell = [cell getHeightOfCellForDuration:self.durations[indexPath.row]];
    }
 
    //For displaying the last empty cell
    self.heightOfTableContent += heightOfCell;
    if (self.durations.count > 0 && indexPath.row == self.durations.count)
    {
        CGFloat sessionViewHeightValue = self.sessionsTableView.frame.size.height;
        if(sessionViewHeightValue > self.heightOfTableContent)
        {
            return (sessionViewHeightValue - self.heightOfTableContent);
        }
        else
        {
            return 0;
        }
    }

    return heightOfCell;
}

- (NSInteger) calculateHeightOfTable
{
    NSInteger height = 0;
    for(int i=0;i<self.durations.count;i++)
    {
        EventDuration *eventDuration = self.durations[i];
        NSInteger count = [eventDuration totalNumberOfAssetAvailableForEventDuration];
        if (count > 0)
        {
            height += ((AssetView.DefaultHeight * count) + 1);
        }
    }
    
    height += SessionsCell.DefaultHeightOfCell * self.durations.count;
    return height;
}


#pragma mark --
#pragma mark EventsSessionsCell Delegate Methods

- (void)sessionCell:(SessionsCell*)cell didSelectAssetView:(AssetView*)assetView
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(sessionsView:didSelectCell:withAssetView:)])
    {
        [self.delegate sessionsView:self didSelectCell:cell withAssetView:assetView];
    }
}

@end
