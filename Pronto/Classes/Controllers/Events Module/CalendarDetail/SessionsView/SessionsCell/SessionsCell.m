//
//  SessionsCell.m
//  CalendarDetailPOC
//
//  Created by Praveen Kumar on 21/08/18.
//  Copyright © 2018 Praveen Kumar. All rights reserved.
//

#import "SessionsCell.h"
#import "AssetView.h"
#import "UIImage+ImageDirectory.h"
#import "UIFont+FontDirectory.h"
#import "UIColor+ColorDirectory.h"
#import "ViewUtils.h"
#import "Constant.h"

#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"

#define DefaultHeightOfAssetViewContainer 25.0
#define DefaultHeightOfLocation 20.0
#define MarginBetweenTitles  8.0
#define SessionDescriptionLabelHeight 17.0
@interface SessionsCell()

@property (nonatomic, strong) NSMutableArray *assetButtons;

@end

@implementation SessionsCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
    [self shouldShowEmptyCell:NO];
    self.assetButtons   = nil;
    self.eventDuration  = nil;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self shouldShowEmptyCell:NO];
    [self removePreviousViewFromAssetContainerViewIfAny];
    self.assetButtons   = nil;
    self.eventDuration  = nil;
}

- (void)removePreviousViewFromAssetContainerViewIfAny
{
    for (UIView  *view in self.assetsContainerView.subviews)
    {
        if ([view isKindOfClass:[AssetView class]])
        {
            [view removeFromSuperview];
        }
    }
}

+ (NSString*)CellIdentifier
{
    return @"SessionsCell";
}

+ (NSString*)NibName
{
    return @"SessionsCell";
}

+ (CGFloat)DefaultHeightOfCell
{
    return 50.0;
}


- (void)setupUI
{
    self.eventEndTime.hidden = true;
    self.timeOfSession.font = [UIFont FontLatoRegularSize11];
    self.sessionHeadding.font = [UIFont FontLatoRegularSize16];
    self.sessionLocation.font = [UIFont FontLatoRegularSize12];
    self.assetContainerTitle.font = [UIFont FontLatoRegularSize14];
    
    self.timeOfSession.textColor = [UIColor GetColorFromHexValue2d2926];
    self.sessionHeadding.textColor = [UIColor GetColorFromHexValue2d2926];
    self.sessionLocation.textColor = [UIColor GetColorFromHexValue2d2926];
    self.assetContainerTitle.textColor = [UIColor GetColorFromHexValue2d2926];
    
    self.roundPointer.image = [UIImage GetCalendarDetailTimeIcon];
    self.locationIcon.image = [UIImage GetCalendarDetailLocationIcon];
    
}

- (void)prepareCellWithEventDuration:(EventDuration*)eventDuration
{
    [self removePreviousViewFromAssetContainerViewIfAny];
    
    self.eventDuration = eventDuration;
    
    self.timeOfSession.text      = [eventDuration getFormattedStartAndEndTimeString];
    self.sessionHeadding.text    = [eventDuration getEventDurationTitle];
    self.sessionLocation.text    = [eventDuration getSessionLocation];
    self.sessionDescription.text = [eventDuration getSessionDescription];
    if(self.sessionLocation.text.length <= 0)
    {
        self.locationIconHeightConstraint.constant = 0;
        self.locationTopConstraint.constant = 0;
    }
    else
    {
        self.locationIconHeightConstraint.constant = DefaultHeightOfLocation;
        self.locationTopConstraint.constant = MarginBetweenTitles;
    }
    
    NSArray *allAssets =  [eventDuration totalAssetForEventDuration];
    
    if (allAssets != nil && allAssets.count > 0)
    {
        NSInteger assetsAssociated = allAssets.count;

        self.assetsContainerView.hidden = NO;
        
        CGRect frame = self.assetsContainerView.frame;
        frame.size.height = self.assetContainerTitle.frame.size.height * (assetsAssociated + 1);
        self.assetsContainerView.frame = frame;
        
        if(assetsAssociated > 1)
        {
            self.assetContainerTitle.text = NSLocalizedString(kAssetsAssociated, kAssetsAssociated);
        }
        else
        {
            self.assetContainerTitle.text = NSLocalizedString(kAssetAssociated, kAssetAssociated);
        }
        
        for(int count=0; count < assetsAssociated; count++)
        {
            CGRect frame = CGRectMake(0, (AssetView.DefaultHeight * (count+1)), self.assetsContainerView.frame.size.width, AssetView.DefaultHeight);
            
            AssetView *assetView = [AssetView GetAssetViewWithFrame:frame andAsset:allAssets[count]];
            assetView.button.tag = count;
            assetView.delegate = (id<EventsAssetViewDelegate>)self;;
            [self.assetsContainerView addSubview:assetView];
            
            [self.assetButtons addObject:assetView];
        }
        self.assetsContainerView.hidden = NO;
        
    }
    else
    {
        self.assetsContainerView.hidden = YES;
    }
}

- (void)shouldShowEmptyCell:(BOOL)show
{
    self.timeOfSession.hidden   = show;
    self.sessionHeadding.hidden = show;
    self.sessionLocation.hidden = show;
    self.sessionDescription.hidden  = show;
    self.assetsContainerView.hidden = show;
    self.assetContainerTitle.hidden = show;
    self.roundPointer.hidden        = show;
    self.locationIcon.hidden        = show;
    self.bottomSeparator.hidden     = show;
}

- (CGFloat)getHeightOfCellForDuration:(EventDuration*)duration
{
    //Getting default height
    CGFloat totalHeight = [SessionsCell DefaultHeightOfCell];
    
    CGFloat heightToIncrease = 0;
    
    //Increasing height for the Assets and EOA if present
    CGFloat totalAssetAssociated  = [duration totalNumberOfAssetAvailableForEventDuration];
    if (totalAssetAssociated > 0)
    {
        heightToIncrease += DefaultHeightOfAssetViewContainer;
        heightToIncrease += (AssetView.DefaultHeight * totalAssetAssociated);
    }
    
    //Increasing height for the location if present
    if([duration getSessionLocation].length > 0)
    {
        heightToIncrease = heightToIncrease + DefaultHeightOfLocation + MarginBetweenTitles;
    }
    
    //Increasing height for the Session description if present
    NSString* sessionDescription = [duration getSessionDescription];
    if(sessionDescription.length > 0)
    {
        CGFloat sessionDescriptionLabelHeight = [self adjustLabelHeight:self.sessionDescription withText:sessionDescription];
        if(sessionDescriptionLabelHeight > self.sessionDescription.height)
        {
            heightToIncrease = heightToIncrease + sessionDescriptionLabelHeight + MarginBetweenTitles;
        }
        else
        {
            heightToIncrease = heightToIncrease + SessionDescriptionLabelHeight + MarginBetweenTitles;
        }
    }
    
    //Finalizing total height
    totalHeight += heightToIncrease;
    
    return totalHeight + 30;
}

//For adjusting label height according to content
-(CGFloat) adjustLabelHeight: (UILabel *)label withText:(NSString*)content
{
    
    CGSize constraint = CGSizeMake([[UIScreen mainScreen] bounds].size.width - 130, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [content boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    CGRect newFrame = label.frame;
    newFrame.size.height = size.height;
    return size.height;
}

#pragma mark --
#pragma mark EventsAssetView Delegate Methods

- (void)assetView:(AssetView*)view  didTapAssetButton:(UIButton*)sender
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(sessionCell:didSelectAssetView:)])
    {
        [self.delegate sessionCell:self didSelectAssetView:view];
    }
}


@end
