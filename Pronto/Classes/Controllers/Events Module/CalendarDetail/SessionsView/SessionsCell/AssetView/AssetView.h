//
//  AssetButton.h
//  CalendarDetailPOC
//
//  Created by Praveen Kumar on 22/08/18.
//  Copyright © 2018 Praveen Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AssetView;

@protocol EventsAssetViewDelegate<NSObject>

- (void)assetView:(AssetView*)view  didTapAssetButton:(UIButton*)sender;

@end

@interface AssetView : UIView

+ (AssetView*)GetAssetView;
+ (AssetView*)GetAssetViewFrame:(CGRect)frame;
/**
 it can accept asset of type either of Asset Or Event_Only_Asset object
 */
+ (AssetView*)GetAssetViewWithFrame:(CGRect)frame andAsset:(NSObject*)asset;
+ (CGFloat)DefaultHeight;

@property (weak, nonatomic) IBOutlet UILabel *documentTitle;
@property (weak, nonatomic) IBOutlet UIImageView *documentImage;
@property (weak, nonatomic) IBOutlet UIImageView *readUnreadImage;
@property (weak, nonatomic) IBOutlet UIButton *button;

@property (nonatomic, weak) id <EventsAssetViewDelegate> delegate;
@property (nonatomic, strong) NSObject *associatedAsset;
@end
