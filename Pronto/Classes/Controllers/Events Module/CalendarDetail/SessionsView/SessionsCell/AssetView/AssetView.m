//
//  AssetButton.m
//  CalendarDetailPOC
//
//  Created by Praveen Kumar on 22/08/18.
//  Copyright © 2018 Praveen Kumar. All rights reserved.
//

#import "AssetView.h"
#import "UIImage+ImageDirectory.h"
#import "UIFont+FontDirectory.h"
#import "UIColor+ColorDirectory.h"
#import "ViewUtils.h"

#import "Asset.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"


#define AssetView_Margin 250

@implementation AssetView

+ (AssetView*)GetAssetView
{
    return (AssetView*)[[[NSBundle mainBundle] loadNibNamed:@"AssetView" owner:self options:nil] lastObject];
}

+ (AssetView*)GetAssetViewFrame:(CGRect)frame
{
    AssetView *assetView = [self GetAssetView];
    NSString * docTitle = @"Document";
    assetView.documentImage.image = [UIImage GetCalendarDetailPDFIcon];
    assetView.readUnreadImage.image = [UIImage GetCalendarDetailReadIcon];
    assetView.documentTitle.text = docTitle;
    assetView.documentTitle.textColor = [UIColor GetColorFromHexValue0081B9];
    assetView.documentTitle.font = [UIFont FontLatoRegularSize13];
    
    //Setting extended width
    float widthIs =
    [assetView.documentTitle.text
     boundingRectWithSize:assetView.documentTitle.frame.size
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{ NSFontAttributeName:assetView.documentTitle.font }
     context:nil]
    .size.width;
    
    widthIs += 6;
    
    if(widthIs > ([UIScreen mainScreen].bounds.size.width - AssetView_Margin))
    {
        widthIs = ([UIScreen mainScreen].bounds.size.width - AssetView_Margin);
        
    }
    
    //Increasing frame Width
    frame.size.width = widthIs + (assetView.documentImage.frame.size.width * 2) + (8 * 2);
    assetView.documentTitle.width = widthIs;
    assetView.frame = frame;
    
    return assetView;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self prepareAssetViewWhenAssetIsView:NO];
}

- (void)prepareAssetViewWhenAssetIsView:(BOOL)isViewed
{
    if (isViewed == YES)
    {
        self.documentTitle.textColor   = [UIColor GetColorFromHexValue0e398e];
        self.documentImage.image       = [UIImage GetCalendarDetailPDFIconSelected];
        self.readUnreadImage.image     = [UIImage GetCalendarDetailReadIcon];
        self.readUnreadImage.hidden    = NO;
    }
    else
    {
        self.documentTitle.textColor   = [UIColor GetColorFromHexValue0081B9];
        self.documentTitle.font        = [UIFont FontLatoRegularSize13];
        self.documentImage.image       = [UIImage GetCalendarDetailPDFIcon];
        self.readUnreadImage.hidden    = YES;
    }
}

+ (AssetView*)GetAssetViewWithFrame:(CGRect)frame andAsset:(NSObject*)asset
{
    AssetView *assetView = [self GetAssetView];
    assetView.associatedAsset = asset;
    
    NSString *assetTitle = @"";
    BOOL isAssetViewed = NO;

    if ([asset isKindOfClass:[Asset class]])
    {
        assetTitle = ((Asset*)asset).title;
        isAssetViewed = ((Asset*)asset).isViewed.boolValue;
    }
    else
    {
        assetTitle = ((EventOnlyAsset*)asset).eoa_title;
        isAssetViewed = ((EventOnlyAsset*)asset).isViewed.boolValue;
    }
    assetView.documentTitle.text = assetTitle;
    
    [assetView prepareAssetViewWhenAssetIsView:isAssetViewed];
    
    //Setting extended width
    float widthIs =
    [assetView.documentTitle.text
     boundingRectWithSize:assetView.documentTitle.frame.size
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{ NSFontAttributeName:assetView.documentTitle.font }
     context:nil]
    .size.width;
    
    widthIs += 6;
    
    if(widthIs > ([UIScreen mainScreen].bounds.size.width - AssetView_Margin))
    {
        widthIs = ([UIScreen mainScreen].bounds.size.width - AssetView_Margin);
        
    }
    
    //Increasing frame Width
    frame.size.width = widthIs + (assetView.documentImage.frame.size.width * 2) + (8 * 2);
    assetView.documentTitle.width = widthIs;
    assetView.frame = frame;
    
    return assetView;

}

+ (CGFloat)DefaultHeight
{
    return 25.0;
}

- (IBAction)buttonAction:(id)sender
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(assetView:didTapAssetButton:)])
    {
        [self.delegate assetView:self didTapAssetButton:sender];
    }
}

@end
