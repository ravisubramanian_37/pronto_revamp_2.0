//
//  SessionsCell.h
//  CalendarDetailPOC
//
//  Created by Praveen Kumar on 21/08/18.
//  Copyright © 2018 Praveen Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SessionsCell;
@class AssetView;
@class EventDuration;

@protocol EventsSessionsCellDelegate<NSObject>

- (void)sessionCell:(SessionsCell*)cell didSelectAssetView:(AssetView*)assetView;

@end

@interface SessionsCell : UITableViewCell

+ (NSString*)CellIdentifier;
+ (NSString*)NibName;
+ (CGFloat)DefaultHeightOfCell;

@property (weak, nonatomic) IBOutlet UILabel *timeOfSession;
@property (weak, nonatomic) IBOutlet UILabel *sessionHeadding;
@property (weak, nonatomic) IBOutlet UILabel *sessionLocation;
@property (weak, nonatomic) IBOutlet UIView *assetsContainerView;
@property (weak, nonatomic) IBOutlet UILabel *assetContainerTitle;
@property (weak, nonatomic) IBOutlet UIImageView *roundPointer;
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationIconHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *sessionDescription;
@property (nonatomic, strong) EventDuration *eventDuration;
@property (weak, nonatomic) IBOutlet UILabel *eventEndTime;
@property (weak, nonatomic) IBOutlet UIImageView *eventendTimeImage;

@property (nonatomic, weak) id <EventsSessionsCellDelegate> delegate;

- (void)shouldShowEmptyCell:(BOOL)show;
- (void)prepareCellWithEventDuration:(EventDuration*)eventDuration;
- (CGFloat)getHeightOfCellForDuration:(EventDuration*)duration;

@end
