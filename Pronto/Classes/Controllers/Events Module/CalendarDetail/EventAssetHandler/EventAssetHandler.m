//
//  EventAssetHandler.m
//  Pronto
//
//  Created by m-666346 on 07/09/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventAssetHandler.h"
#import "EventCoreDataManager.h"
#import "Pronto-Swift.h"
#import "ZMAssetsLibrary.h"
#import "Asset.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "Constant.h"
#import "AssetDownloadProvider.h"
#import "AppDelegate.h"
#import "ZMAssetViewController.h"
#import "ZMDocument.h"
#import "ZMUserActions.h"
#import "ZMTracking.h"

@interface EventAssetHandler()

@end

@implementation EventAssetHandler

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        [self initializeData];
    }
    return self;
}

- (void)initializeData
{
    self.mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.actions      = [ZMUserActions sharedInstance];
    
    //It will provide all the events during asset download.
    [[ZMAssetsLibrary defaultLibrary] addObserver:self]; // add all methods related this
}

- (void)validateAndShowAsset:(Asset*)selectedAsset
{
    //if there is no asset or asset title or Asset id available.. don't do anything. Exit.
    if(selectedAsset == nil || selectedAsset.title.length <= 0 || selectedAsset.assetID == nil) { return ;}

    [self validateAssetBeforeOpen:selectedAsset completion:^(BOOL assetNeedsDownload)
     {
         if (assetNeedsDownload == YES)
         {
             //Restricting user from opening asset if offline download is in progress
             NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:selectedAsset.assetID];
             for(OfflineDownload * data in offlineDownloadArray)
             {
                 if(data.assetID == selectedAsset.assetID)
                 {
                     if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"])
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             if(!self.isTapDownloadInitiated)
                             {
                                 [self downloadSelectedAsset:selectedAsset];
                                 [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                             }
                         });
                        
                         self.isDownloadingAsset = NO;
                     }
                     else
                     {
                         if(!self.isTapDownloadInitiated)
                         {
                             [self downloadSelectedAsset:selectedAsset];
                             [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                         }
                     }
                 }
             }
             if(offlineDownloadArray.count == 0)
             {
                 if(!self.isTapDownloadInitiated)
                 {
                     [self downloadSelectedAsset:selectedAsset];
                     [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                 }
             }
         }
         else
         {
           self.currentSelectedAssetToSeeDetail = nil;
             if(self.currentSelectedAssetToSeeDetail)
             {
                 //Return in case if open of any asset is in progress
                 return;
             }
             else
             {
                 self.currentSelectedAssetToSeeDetail = selectedAsset;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                     selectedAsset.isUpdateAvailable = NO;
                 });
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [AbbvieLogging logInfo:@"------Asset needs opened ------"];
                     [self openAsset:selectedAsset];
                 });
             }
         }
     } failure:^(NSString *messageFailure) {
         [self.mainDelegate showMessage:messageFailure whithType:ZMProntoMessagesTypeWarning];
     }];

}

//Validate Assets before open
-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {

    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
  
  if (assetType == ZMProntoFileTypesBrightcove)
      {
          UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
          if (topController.presentedViewController) {
              topController = topController.presentedViewController;
               }
          else{
              UINavigationController *nav = (UINavigationController *)topController;
              topController = nav.topViewController;
              
              UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
              UIViewController <ZMAssetViewControllerDelegate> *controller;
              controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
              controller.asset = asset;
              controller.videoId = asset.uri_full;
              [topController.navigationController pushViewController:controller animated:NO];
          }
      }
      else if (!self.isDownloadingAsset)
    {
        self.isDownloadingAsset = YES;
        // check if asset is protected
        if (![UIApplication sharedApplication].protectedDataAvailable)
        {
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
            /// This should occur during application:didFinishLaunchingWithOptions:

            // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
            NSError *error;
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];

            if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
            {
                attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
                if (!success)
                    [AbbvieLogging logInfo:@"Set ~/Documents attrsAssetPath NOT successfull"];
            }

            /// ....
            NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
            [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
        }

        // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        if([[NSFileManager defaultManager] fileExistsAtPath:asset.path])
        {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"File Exists at %@", asset.path]];

        }

        if ((![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && assetType != ZMProntoFileTypesBrightcove) && (assetType != ZMProntoFileTypesWeblink))
        {
            if ([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
            {
                completion(YES);
            }
            else
            {
                self.isDownloadingAsset = NO;
                failure(@"The internet connection is not available, please check your network connectivity");
            }
        }
        else
        {
            //Check if the asset size matches the metada info meaning the asset is correct
            if (asset.file_size.intValue == (int)fileSize)
            {
                //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
                self.isDownloadingAsset = NO;
                completion(NO);
            }
            else
            {
                completion(YES);
            }
        }
    }
    else
    {
        if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
        {
            if(self.isDownloadingAsset == NO)
            {
                failure(@"The internet connection is not available, please check your network connectivity");
                self.isDownloadingAsset = NO;
            }
        }
        else
        {
            failure(@"Download in progress. Please wait and then try again.");
        }
    }
}
// Download selected Asset
- (void) downloadSelectedAsset:(Asset *)selected
{
    self.isTapDownloadInitiated = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    });
    [AbbvieLogging logInfo:@"------Asset needs download ------"];
    //Web View - Ability to provide a Web link as an asset type.
    if(![selected.file_mime isEqualToString:@"weblink"])
    {
        [self downloadAsset:selected];
    }
}

//Open Selected Asset
- (void)openAsset:(Asset *)selectedAsset
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventAssetHandler:openAsset:)])
    {
        selectedAsset.isViewed = [NSNumber numberWithBool:YES];
        [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:NULL];
        
        [self.delegate eventAssetHandler:self openAsset:selectedAsset];
    }
}

// Download Assets
- (void)downloadAsset:(Asset *)selectedAsset{
    
    self.currentAssetDownload = selectedAsset;
    self.isDownloadingAsset = YES;
    
   
    [self setProgress:0.001000];
    
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventAssetHandler:assetDownloadStatus:)])
    {
        [self.delegate eventAssetHandler:self assetDownloadStatus:AssetDownloadStatusStarted];
    }
    
    [AssetsServiceClass.sharedAPIManager downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        
        if (self.delegate != nil &&
            [self.delegate respondsToSelector:@selector(eventAssetHandler:assetDownloadStatus:)])
        {
            [self.delegate eventAssetHandler:self assetDownloadStatus:AssetDownloadStatusCompleted];
        }
        
        [self downloadAssetDidFinish:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"completed"];
        selectedAsset.isUpdateAvailable = NO;
    } onError:^(NSError *error) {
        
        if (self.delegate != nil &&
            [self.delegate respondsToSelector:@selector(eventAssetHandler:assetDownloadStatus:)])
        {
            [self.delegate eventAssetHandler:self assetDownloadStatus:AssetDownloadStatusFailed];
        }
        
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAssetOnFailure:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"failed"];
    }];
}

- (void)setProgress:(CGFloat)progress
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventAssetHandler:setProgress:)])
    {
        [self.delegate eventAssetHandler:self setProgress:progress];
    }
}

- (void)hideMessage
{
    [self.mainDelegate hideMessage];
}

//Asset Download completed
- (void)downloadAssetDidFinish:(Asset *)asset
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate hideMessage];
    });
    
    self.isDownloadingAsset = NO;
    self.isTapDownloadInitiated = NO;
    
    [self.mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", asset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self validateAndShowAsset:asset];
    });
}

//To Update Coredata with offlineDownload
- (void)updateOfflineDownloadCoreData:(Asset *)asset status:(NSString*)status
{
    if([asset valueForKey:@"assetID"] != nil)
    {
        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
        if (offlineData.count > 0)
        {
            //update the status of object, if current status and status to be updated is not same
            OfflineDownload *offlineAsset = (OfflineDownload*)[offlineData lastObject];
            if ([offlineAsset.status isEqualToString:status] == NO)
            {
                offlineAsset.status = status;
                [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:NULL];
            }
        }
        else
        {
            NSMutableDictionary * initialDownloadAsset = [NSMutableDictionary dictionary];
            [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
            [initialDownloadAsset setObject:status forKey:@"status"];
            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
        }
    }
}

//To check Asset Download status
- (void)downloadAssetDidNotFinish:(Asset *)asset error:(NSError *)error
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.mainDelegate.currentMessage)
        {
            [self.mainDelegate hideMessage];
        }
        
        self.isDownloadingAsset = NO;
        self.isTapDownloadInitiated = NO;
        
        NSString *message;
        enum ZMProntoMessages type = 1;
        if (error.code == 503)
        {
            type = ZMProntoMessagesTypeWarning;
            message = @"The server is currently under maintenance mode.";
        }
        
        else if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
        {
            type = ZMProntoMessagesTypeWarning;
            message = @"The Internet connection is not available, please check your network connectivity.";
        }
        else if(error.code == 401)
        {
            [self.mainDelegate initPronto];
            return;
        }
        else if (error.code == 404)
        {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"Asset not found %@", asset.title];
        }
        else if (error.code == 5303 || error.code == -5303)
        {
            message = @"Error while retreiving assets, please try again later";
        }
        else if (error.code == 1009 || error.code == -1009)
        {
            message = @"Please check the network connection";
        }
        else if (error.code == 1003 || error.code == -1003)
        {
            message = @"A server with the specified hostname could not be found";
        }
        else
        {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"There is a problem downloading %@", asset.title];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mainDelegate showMessage:message whithType:type withAction:^{
                [self.mainDelegate hideMessage];
                [self validateAndShowAsset:asset];
            }];
        });
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        if ([message containsString:@"ERROR_CODE"])
        {
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",@"Please check the network connection"] forKey:@"scerrorcode"];
        }
        else
        {
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",message] forKey:@"scerrorcode"];
        }
        
        self.currentAssetDownload = nil;
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"assetview" withName:nil withOptions:trackingOptions];
    });
}

@end

