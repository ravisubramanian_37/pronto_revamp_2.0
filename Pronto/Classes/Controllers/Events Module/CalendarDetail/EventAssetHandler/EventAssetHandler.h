//
//  EventAssetHandler.h
//  Pronto
//
//  Created by m-666346 on 07/09/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "Pronto-Swift.h"
#import "ZMAssetsLibrary.h"
#import "Asset.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "Constant.h"
#import "AssetDownloadProvider.h"
#import "AppDelegate.h"
#import "ZMAssetViewController.h"
#import "ZMDocument.h"
#import "ZMUserActions.h"
#import "ZMTracking.h"


@class Asset;
@class EventAssetHandler;

@protocol EventAssetHandlerDelegate<NSObject>

- (void)eventAssetHandler:(EventAssetHandler*)eventAssetHandler pushViewController:(UIViewController*)viewController;
- (void)eventAssetHandler:(EventAssetHandler*)eventAssetHandler setProgress:(CGFloat)progress;
- (void)eventAssetHandler:(EventAssetHandler*)eventAssetHandler openAsset:(Asset*)selectedAsset;

- (void)eventAssetHandler:(EventAssetHandler*)eventAssetHandler assetDownloadStatus:(AssetDownloadStatus)status;

//Data Source
- (UIViewController*) getPresentViewControllerForEventAssetHandler:(EventAssetHandler*)eventAssetHandler;
@end

/**This class will all download of asset and Notifying to concern object after download happen succesfully.
 if There is multple asset selected for validating and preparing. All selected asset will be passed to single object of this class.
 It will handle download of all asset.
 */
@interface EventAssetHandler : NSObject
@property (nonatomic, strong) NSString *userFranchise; //Must provide
@property (nonatomic, weak) id <EventAssetHandlerDelegate> delegate;

@property (nonatomic, assign) BOOL isTapDownloadInitiated; // It could be Tap download has been initiated.
@property (nonatomic, assign) BOOL isDownloadingAsset;
@property (nonatomic, strong) Asset *currentSelectedAssetToSeeDetail;
@property (nonatomic, strong) Asset *currentAssetDownload;

@property (nonatomic, weak) AppDelegate *mainDelegate;
@property (nonatomic, weak) ZMAbbvieAPI *apiManager;
@property (strong, nonatomic) ZMUserActions *actions;

@property (nonatomic, strong) UIViewController <ZMAssetViewControllerDelegate> *controller;
@property (nonatomic, strong) UIViewController * viewController;

- (void)hideMessage;

- (void)validateAndShowAsset:(Asset*)selectedAsset;

- (void)updateOfflineDownloadCoreData:(Asset *)asset status:(NSString*)status;

@end
