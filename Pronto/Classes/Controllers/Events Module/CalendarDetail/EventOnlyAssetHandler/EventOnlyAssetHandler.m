//
//  EventAssetHandler.m
//  Pronto
//
//  Created by m-666346 on 07/09/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventOnlyAssetHandler.h"
#import "EventCoreDataManager.h"
#import "Pronto-Swift.h"
#import "ZMAssetsLibrary.h"
#import "Asset.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "ZMAssetViewController.h"
#import "ZMDocument.h"
#import "ZMUserActions.h"
#import "ZMTracking.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"
#import "EventAssetDownloadProvider.h"

@interface EventOnlyAssetHandler()

@end

@implementation EventOnlyAssetHandler

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        [self initializeData];
    }
    return self;
}

- (void)initializeData
{
    self.mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.actions      = [ZMUserActions sharedInstance];
    
    //It will provide all the events during asset download.
    [[ZMAssetsLibrary defaultLibrary] addObserver:self]; // add all methods related this
}

- (void)validateAndShowEventOnlyAsset:(EventOnlyAsset*)selectedAsset
{
    //if there is no asset or asset title or Asset id available.. don't do anything. Exit.
    if(selectedAsset == nil || selectedAsset.eoa_title.length <= 0 || selectedAsset.eoa_id == nil) { return ;}

    [self validateAssetBeforeOpen:selectedAsset completion:^(BOOL assetNeedsDownload)
     {
         if (assetNeedsDownload == YES)
         {
             NSNumber *eoa_assetId = [NSNumber numberWithInteger:[selectedAsset.eoa_id integerValue]];
             //Restricting user from opening asset if offline download is in progress
             NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:eoa_assetId];
             for(OfflineDownload *data in offlineDownloadArray)
             {
                 if(data.assetID.longLongValue == eoa_assetId.longLongValue)
                 {
                     if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"])
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             if(!self.isTapDownloadInitiated)
                             {
                                 [self downloadSelectedAsset:selectedAsset];
                                 [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] handleTapToDownloadScenarioForEventOnlyAsset:selectedAsset];
                             }
                         });
                         self.isDownloadingAsset = NO;
                     }
                     else
                     {
                         if(!self.isTapDownloadInitiated)
                         {
                             [self downloadSelectedAsset:selectedAsset];
                             [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] handleTapToDownloadScenarioForEventOnlyAsset:selectedAsset];
                         }
                     }
                 }
             }
             if(offlineDownloadArray.count == 0)
             {
                 if(!self.isTapDownloadInitiated)
                 {
                     [self downloadSelectedAsset:selectedAsset];
                     [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] handleTapToDownloadScenarioForEventOnlyAsset:selectedAsset];
                 }
             }
         }
         else
         {
             if(self.currentSelectedAssetToSeeDetail)
             {
                 //Return in case if open of any asset is in progress
                 return;
             }
             else
             {
                 self.currentSelectedAssetToSeeDetail = selectedAsset;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                 });
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [AbbvieLogging logInfo:@"------Asset needs opened ------"];
                     [self openAsset:selectedAsset];
                 });
             }
         }
     } failure:^(NSString *messageFailure) {
         [self.mainDelegate showMessage:messageFailure whithType:ZMProntoMessagesTypeWarning];
     }];

}

-(void)validateAssetBeforeOpen:(EventOnlyAsset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {

    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.eoa_path error:nil] fileSize];
    enum ZMProntoFileTypes assetType = [asset getFileTypeOfEventOnlyAsset];

    if (!self.isDownloadingAsset)
    {
        self.isDownloadingAsset = YES;
        // check if asset is protected
        if (![UIApplication sharedApplication].protectedDataAvailable)
        {
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
            /// This should occur during application:didFinishLaunchingWithOptions:

            // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
            NSError *error;
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.eoa_path error:&error];

            if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
            {
                attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.eoa_path error:&error];
                if (!success)
                    [AbbvieLogging logInfo:@"Set ~/Documents attrsAssetPath NOT successfull"];
            }

            /// ....
            NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
            [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.eoa_path error:nil];
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
        }

        // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        if([[NSFileManager defaultManager] fileExistsAtPath:asset.eoa_path])
        {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"File Exists at %@", asset.eoa_path]];

        }

        if ((![[NSFileManager defaultManager] fileExistsAtPath:asset.eoa_path] && assetType != ZMProntoFileTypesBrightcove) && (assetType != ZMProntoFileTypesWeblink))
        {
            if ([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
            {
                completion(YES);
            }
            else
            {
                self.isDownloadingAsset = NO;
                failure(@"The internet connection is not available, please check your network connectivity");
            }
        }
        else
        {
            //Check if the asset size matches the metada info meaning the asset is correct
            if (asset.eoa_file_size.intValue == (int)fileSize)
            {
                //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
                self.isDownloadingAsset = NO;
                completion(NO);
            }
            else
            {
                completion(YES);
            }
        }
    }
    else
    {
        if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
        {
            if(self.isDownloadingAsset == NO)
            {
                failure(@"The internet connection is not available, please check your network connectivity");
                self.isDownloadingAsset = NO;
            }
        }
        else
        {
            failure(@"Download in progress. Please wait and then try again.");
        }
    }
}


- (void) downloadSelectedAsset:(EventOnlyAsset *)eventOnlyAsset
{
    self.isTapDownloadInitiated = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    });
    [AbbvieLogging logInfo:@"------Asset needs download ------"];
    //Web View - Ability to provide a Web link as an asset type.
    if(![eventOnlyAsset.eoa_file_mime isEqualToString:@"weblink"])
    {
        [self downloadAsset:eventOnlyAsset];
    }
}

- (void)openAsset:(EventOnlyAsset *)selectedAsset
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventOnlyAssetHandler:openAsset:)])
    {
        selectedAsset.isViewed = [NSNumber numberWithBool:YES];
        [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:NULL];
        
        [self.delegate eventOnlyAssetHandler:self openAsset:selectedAsset];
    }
}


- (void)downloadAsset:(EventOnlyAsset *)evenOnlyAsset
{
    //downloadEventOnlyAsset
    self.currentAssetDownload = evenOnlyAsset;
    self.isDownloadingAsset = YES;
    
    [self setProgress:0.001000];
    
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventOnlyAssetHandler:assetDownloadStatus:)])
    {
        [self.delegate eventOnlyAssetHandler:self assetDownloadStatus:AssetDownloadStatusStarted];
    }
    
    [[EventsServiceClass sharedAPIManager] downloadEventOnlyAsset:evenOnlyAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        
        if (self.delegate != nil &&
            [self.delegate respondsToSelector:@selector(eventOnlyAssetHandler:assetDownloadStatus:)])
        {
            [self.delegate eventOnlyAssetHandler:self assetDownloadStatus:AssetDownloadStatusCompleted];
        }
        
        [self downloadEventOnlyAssetDidFinish:evenOnlyAsset];
        [self updateOfflineDownloadCoreData:evenOnlyAsset status:@"completed"];
        
    } onError:^(NSError *error) {
        
        if (self.delegate != nil &&
            [self.delegate respondsToSelector:@selector(eventOnlyAssetHandler:assetDownloadStatus:)])
        {
            [self.delegate eventOnlyAssetHandler:self assetDownloadStatus:AssetDownloadStatusFailed];
        }
        
        [self setProgress:2];
        [self downloadEventOnlyAssetDidNotFinish:evenOnlyAsset error:error];
        [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] handleTapToDownloadScenarioForEventOnlyAssetOnFailure:evenOnlyAsset];
        [self updateOfflineDownloadCoreData:evenOnlyAsset status:@"failed"];
    }];
}

- (void)setProgress:(CGFloat)progress
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventOnlyAssetHandler:setProgress:)])
    {
        [self.delegate eventOnlyAssetHandler:self setProgress:progress];
    }
}

- (void)hideMessage
{
    [self.mainDelegate hideMessage];
}

- (void)downloadEventOnlyAssetDidFinish:(EventOnlyAsset* )asset
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate hideMessage];
    });
    
    self.isDownloadingAsset = NO;
    self.isTapDownloadInitiated = NO;
    
    [self.mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", asset.eoa_title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self validateAndShowEventOnlyAsset:asset];
    });
}

- (void)updateOfflineDownloadCoreData:(EventOnlyAsset *)eventOnlyAsset status:(NSString*)status
{
    if(eventOnlyAsset.eoa_id != nil)
    {
        NSNumber *eventOnlyAssetId = [NSNumber numberWithInteger:[eventOnlyAsset.eoa_id integerValue]];
        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:eventOnlyAssetId];
        
        if (offlineData.count > 0)
        {
            //update the status of object, if current status and status to be updated is not same
            OfflineDownload *offlineAsset = (OfflineDownload*)[offlineData lastObject];
            if ([offlineAsset.status isEqualToString:status] == NO)
            {
                offlineAsset.status = status;
                [[EventCoreDataManager sharedInstance] saveCurrentContextWithCompletionHandler:NULL];
            }
        }
        else
        {
            //Insert the object
            NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
            if (eventOnlyAssetId != nil)
            {
                [initialDownloadAsset setObject:eventOnlyAssetId forKey:@"assetID"];
            }
            [initialDownloadAsset setObject:status forKey:@"status"];
            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
        }
    }
}

- (void)downloadEventOnlyAssetDidNotFinish:(EventOnlyAsset *)eventOnlyAsset error:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.mainDelegate  hideMessage];
        
        self.isDownloadingAsset = NO;
        self.isTapDownloadInitiated = NO;
        
        NSString *message;
        enum ZMProntoMessages type = 1;
        if (error.code == 503)
        {
            type = ZMProntoMessagesTypeWarning;
            message = @"The server is currently under maintenance mode.";
        }
        
        else if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
        {
            type = ZMProntoMessagesTypeWarning;
            message = @"The Internet connection is not available, please check your network connectivity.";
        }
        else if(error.code == 401)
        {
            [self.mainDelegate initPronto];
            return;
        }
        else if (error.code == 404)
        {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"Asset not found %@", eventOnlyAsset.eoa_title];
        }
        else if (error.code == 5303 || error.code == -5303)
        {
            message = @"Error while retreiving assets, please try again later";
        }
        else if (error.code == 1009 || error.code == -1009)
        {
            message = @"Please check the network connection";
        }
        else if (error.code == 1003 || error.code == -1003)
        {
            message = @"A server with the specified hostname could not be found";
        }
        else
        {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"There is a problem downloading %@", eventOnlyAsset.eoa_title];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mainDelegate showMessage:message whithType:type withAction:^{
                [self.mainDelegate hideMessage];
                [self validateAndShowEventOnlyAsset:eventOnlyAsset];
            }];
        });
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        if ([message containsString:@"ERROR_CODE"])
        {
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",@"Please check the network connection"] forKey:@"scerrorcode"];
        }
        else
        {
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",message] forKey:@"scerrorcode"];
        }
        
        self.currentAssetDownload = nil;
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"assetview" withName:nil withOptions:trackingOptions];
    });
}

@end

