//
//  EventOnlyAssetHandler.h
//  Pronto
//
//  Created by m-666346 on 19/09/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "Pronto-Swift.h"
#import "ZMAssetsLibrary.h"
#import "Asset.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "Constant.h"
#import "AssetDownloadProvider.h"
#import "AppDelegate.h"
#import "ZMAssetViewController.h"
#import "ZMDocument.h"
#import "ZMUserActions.h"
//#import "ZMPDFDocument.h"
#import "ZMTracking.h"
//#import "ToolsTraining.h"


@class EventOnlyAsset ;
@class EventOnlyAssetHandler;

@protocol EventOnlyAssetHandlerDelegate<NSObject>

- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler pushViewController:(UIViewController*)viewController;
- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler setProgress:(CGFloat)progress;
- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler openAsset:(EventOnlyAsset*)selectedAsset;


- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler assetDownloadStatus:(AssetDownloadStatus)status;

//Data Source
- (UIViewController*) getPresentViewControllerForEventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler;
@end

/**This class will all download of asset and Notifying to concern object after download happen succesfully.
 if There is multple asset selected for validating and preparing. All selected asset will be passed to single object of this class.
 It will handle download of all asset.
 */
@interface EventOnlyAssetHandler : NSObject
@property (nonatomic, strong) NSString *userFranchise; //Must provide
@property (nonatomic, weak) id <EventOnlyAssetHandlerDelegate> delegate;

@property (nonatomic, assign) BOOL isTapDownloadInitiated; // It could be Tap download has been initiated.
@property (nonatomic, assign) BOOL isDownloadingAsset;
@property (nonatomic, strong) EventOnlyAsset *currentSelectedAssetToSeeDetail;
@property (nonatomic, strong) EventOnlyAsset *currentAssetDownload;

@property (nonatomic, weak) AppDelegate *mainDelegate;
@property (nonatomic, weak) ZMAbbvieAPI *apiManager;
@property (strong, nonatomic) ZMUserActions *actions;

@property (nonatomic, strong) UIViewController <ZMAssetViewControllerDelegate> *controller;
@property (nonatomic, strong) UIViewController * viewController;

- (void)hideMessage;

- (void)validateAndShowEventOnlyAsset:(EventOnlyAsset*)selectedAsset;

- (void)updateOfflineDownloadCoreData:(EventOnlyAsset *)eventOnlyAsset status:(NSString*)status; 

@end
