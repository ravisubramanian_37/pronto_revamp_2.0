//
//  WeekViewCell.m
//  Pronto
//
//  Created by m-666346 on 27/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "WeekViewCell.h"
#import "ViewUtils.h"
#import "NSDate+DateDirectory.h"
#import "UIFont+FontDirectory.h"
#import "UIColor+ColorDirectory.h"


@interface WeekViewCell()

@property (nonatomic,strong) NSDate *date;
@property (nonatomic,strong) NSCalendar *calendar;
@property (nonatomic,assign,readwrite) NSUInteger weekday;

 
@end

@implementation WeekViewCell

+ (NSString*)CellIdentifier
{
    return @"WeekViewCell";
}

+ (NSString*)NibName
{
    return @"WeekViewCell";
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setDate:(NSDate *)date andCalendar:(NSCalendar *)calendar
{
    self.date       = date;
    self.calendar   = calendar;
    
    //Logic: if param date is less than current date, disabling useInteraction, and Reducing alpha value
    NSDate *utcDate = [NSDate GetDateUTCTimeZoneFromLocalTimeZoneDate:date];

    BOOL isPastDate = [NSDate isUTCDate1:utcDate lessThanDate:[NSDate date] forCalendar:self.calendar];
    if (isPastDate || ([NSDate isDateWeekend:date]))
    {
        [self setEnabled:NO];
    }
    else
    {
        [self setEnabled:YES];
    }
    
    if ([self.calendar isDate:utcDate inSameDayAsDate:[NSDate date]])
    {
        [self prepareCellForCurrentDate:YES];
    }
    else
    {
        [self prepareCellForCurrentDate:NO];
    }
}

- (void)setupUI
{
    self.currentDateIndicativeDotView.hidden    = YES;
    self.date = nil;
    self.bottomDividerView.alpha = 0.0;
    self.bottomDividerView.backgroundColor = [UIColor GetColorFromHexValue00809f];
    [self prepareDotView];
}

- (void)setEnabled:(BOOL)enabled
{
    _enabled = enabled;
    
    if (enabled == YES)
    {
        self.dateLabel.textColor = [UIColor GetColorFromHexValue417BAE];
        self.dateLabel.font      = [UIFont FontLatoRegularSize20];
        
        self.weekNameLabel.textColor = [UIColor GetColorFromHexValue2d2926];
        self.weekNameLabel.font      = [UIFont FontLatoRegularSize11];
    }
    else
    {
        self.dateLabel.textColor = [UIColor GetColorFromHexValue417BAEWith30PercentOpacity];
        self.dateLabel.font      = [UIFont FontLatoRegularSize20];
        
        self.weekNameLabel.textColor = [UIColor GetColorFromHexValue2d2926With30PercentOpacity];
        self.weekNameLabel.font      = [UIFont FontLatoRegularSize11];
    }
   
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.currentDateIndicativeDotView.hidden    = YES;
    [self setCellSelected:NO];
    self.date = nil;
    self.enabled = NO;
    self.userInteractionEnabled = YES;
    self.alpha = 1.0;
}

- (void)prepareDotView
{
    self.currentDateIndicativeDotView.layer.cornerRadius = self.currentDateIndicativeDotView.width / 2.0;
    self.currentDateIndicativeDotView.backgroundColor = [UIColor GetColorFromHexValue1AC9A8];
    self.currentDateIndicativeDotView.clipsToBounds   = YES;
    self.currentDateIndicativeDotView.hidden          = YES;
}

- (void)prepareCellForCurrentDate:(BOOL)isCurrentDate
{
    self.currentDateIndicativeDotView.hidden = !isCurrentDate;
}

/// New changes
- (void)prepareCellForDate:(NSDate*)date
{
    //if it is past date, disable the user interaction and make alpha reduced by 0.75;
    // set date

    self.date = date;
    NSString *dateString = [NSString stringWithFormat:@"%.2ld", (long)[NSDate GetDateInUTCFromDate:date]];
    NSString *weekDay    = [[NSDate GetWeekdayInUTCFormateForDate:date] uppercaseString];
    
    self.dateLabel.text     = dateString;
    self.weekNameLabel.text = weekDay;

}

- (void)setCellSelected:(BOOL)selected
{
    if (selected == YES)
    {
        self.bottomDividerView.alpha = 1.0;
    }
    else
    {
        self.bottomDividerView.alpha = 0.0;
    }
}

- (void)setCellSelected:(BOOL)selected andShowAnimationFromLeftSide:(BOOL)leftSide
{
    if (selected == YES)
    {
        self.bottomDividerView.alpha = 0.0;
        CGFloat xPositionOfBottomDividerView = self.bottomDividerView.left;
        if (leftSide == YES)
        {
            self.bottomDividerView.left = -(self.bottomDividerView.width + 25);
        }
        else
        {
             self.bottomDividerView.left = self.bottomDividerView.width + 25;
        }
        
        [UIView animateWithDuration:0.33 animations:^{ 
            self.bottomDividerView.alpha = 1.0;
            self.bottomDividerView.left = xPositionOfBottomDividerView;
        } completion:^(BOOL finished) {
        }];
    }
    else
    {
        self.bottomDividerView.alpha = 0.0;
    }
}


@end
