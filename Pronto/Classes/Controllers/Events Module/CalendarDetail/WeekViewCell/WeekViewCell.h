//
//  WeekViewCell.h
//  Pronto
//
//  Created by m-666346 on 27/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WeekViewCell;

@protocol WeekViewCellDelegate <NSObject>

- (void)cell:(WeekViewCell*)cell didSelectItemAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface WeekViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIView *WeekCellContainerView;
@property (nonatomic, weak) IBOutlet UILabel *weekNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UIView *currentDateIndicativeDotView;
@property (nonatomic, weak) IBOutlet UIView *bottomDividerView;

@property (nonatomic,strong,readonly) NSDate *date;
@property (nonatomic,strong, readonly) NSCalendar *calendar;
@property (nonatomic,assign,getter = isEnabled) BOOL enabled;

@property (nonatomic, weak) id<WeekViewCellDelegate> delegate;


//This is method is responsible for setting default of cell related to calendar;
- (void)setDate:(NSDate *)date andCalendar:(NSCalendar *)calendar;

- (void)prepareCellForDate:(NSDate*)date;

+ (NSString*)CellIdentifier;
+ (NSString*)NibName;
- (void)setCellSelected:(BOOL)selected;
- (void)setCellSelected:(BOOL)selected andShowAnimationFromLeftSide:(BOOL)leftSide;

@end
