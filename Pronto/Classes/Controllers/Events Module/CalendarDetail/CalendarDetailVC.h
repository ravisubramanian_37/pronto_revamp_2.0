//
//  CalendarDetailVC.h
//  Pronto
//
//  Created by Praveen Kumar on 22/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.

#import <UIKit/UIKit.h>
#import "Asset.h"
//#import "ZMHeader.h"

@class EventsGlobalSearchViewModel;
@class Event;

@interface CalendarDetailVC : UIViewController


@property (weak, nonatomic) IBOutlet UIView *mainHeaderView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *overallView;
@property (weak, nonatomic) IBOutlet UIView *dateHeaderView;
@property (weak, nonatomic) IBOutlet UIView *calendarDetailContainer;
@property (weak, nonatomic) IBOutlet UIView *dateHeader;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *dateHeaderMainView;

@property (nonatomic, strong) Event *event;
@property (nonatomic, strong) NSDate *selectedDate;

//@property(nonatomic, strong) EventsGlobalSearchViewModel *eventsGSViewModel;
@property (nonatomic, assign) BOOL isComingFromGlobalSearchResult;
+(CalendarDetailVC*)GetCalendarDetailsScreen;
- (void)openAudioAsset:(Asset *)selectedAsset;

@end
