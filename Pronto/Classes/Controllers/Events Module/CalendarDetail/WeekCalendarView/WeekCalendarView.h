//
//  WeekCalendarView.h
//  HorizontalScrolling
//
//  Created by m-666346 on 31/08/18.
//  Copyright © 2018 m-666346. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WeekCalendarView;

@protocol WeekCalendarViewDelegate <NSObject>
// Go to next page
// take help from other library and mncalendar view.
- (void)weekCalendarView:(WeekCalendarView*)weekCalendarView didSelectDate:(NSDate*)date;
- (void)weekCalendarView:(WeekCalendarView*)weekCalendarView  scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
//
@end
@interface WeekCalendarView : UIView

@property (nonatomic, weak) IBOutlet UICollectionView *weekCalendarCollectionView;
@property (nonatomic, assign) NSInteger currentPage; //Pagination
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, weak) id <WeekCalendarViewDelegate> delegate;

@property (nonatomic, strong) NSArray *sessionUniqueDateKeys;

- (void)reload;
- (void)reloadWithCompletionHandler:(void (^)(void))completionHandler;
- (void)adjustViewFrame:(CGRect)referenceFrame;
//if
- (void)showWeekCalendarViewWithPageIndex:(NSInteger)pageIndex;

//Selected cell;
// Have methods for getting for date from cell
// Have methods for getting index path from date etc.

- (NSIndexPath*)getIndexPathForDate:(NSDate*)date;
- (BOOL)isCellVisibleAtIndexPath:(NSIndexPath*)indexPath;
- (NSInteger)getExpectedPageOfWeekForDate:(NSDate*)date;
- (void)setCellSelected:(BOOL)selected atIndexPath:(NSIndexPath*)indexPath;
- (void)setCellSelected:(BOOL)selected atIndexPath:(NSIndexPath*)indexPath andShowAnimationFromLeftSide:(BOOL)leftSide;
- (BOOL)haveReachedUpperLimitOfWeekCalendarForDate:(NSDate*)date;
- (BOOL)haveReachedLowerLimitOfWeekCalendarForDate:(NSDate*)date;

- (BOOL)shouldAllowSelectionForDate:(NSDate*)date;

+ (WeekCalendarView*)GetWeekCalendarView;
+ (WeekCalendarView*)GetWeekCalendarViewWithFrame:(CGRect)frame;

@end
