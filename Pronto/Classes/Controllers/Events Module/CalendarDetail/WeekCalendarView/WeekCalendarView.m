//
//  WeekCalendarView.m
//  HorizontalScrolling
//
//  Created by m-666346 on 31/08/18.
//  Copyright © 2018 m-666346. All rights reserved.
//

#import "WeekCalendarView.h"
#import "WeekViewCell.h"
#import "ViewUtils.h"
#import "NSDate+DateDirectory.h"
#import "UICollectionView+Utility.h"
#import "AppUtilities.h"
#import "Pronto-Swift.h"


@interface WeekCalendarView()<UIScrollViewDelegate, UICollectionViewDelegate>

@property (nonatomic, strong) NSArray *dates;
@property (nonatomic, weak) IBOutlet UIView *weekCalendarContainerView;
@property (nonatomic, strong) NSCalendar *calendar;
@end

@implementation WeekCalendarView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setupUI
{
    // Set background colour of container view as cell background.
    // It required because there is very small spacing left in between two cells.
   // self.weekCalendarContainerView.backgroundColor = [UIColor somecolour];
    
    [self.weekCalendarCollectionView registerNib:[UINib nibWithNibName:[WeekViewCell CellIdentifier] bundle:nil] forCellWithReuseIdentifier:[WeekViewCell NibName]];
    self.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    
    self.weekCalendarCollectionView.pagingEnabled = YES;
}

+ (WeekCalendarView*)GetWeekCalendarView
{
    return (WeekCalendarView*)[[[NSBundle mainBundle] loadNibNamed:@"WeekCalendarView" owner:self options:nil] lastObject];
}


+ (WeekCalendarView*)GetWeekCalendarViewWithFrame:(CGRect)frame
{
    WeekCalendarView *weekCalendarView = [self GetWeekCalendarView];
    weekCalendarView.frame = frame;
    // explicit frame set of  view
    CGFloat xMargin = 40;
    if([[AppUtilities sharedUtilities] isLandscape] == NO)
    {
        xMargin = 50;
    }
    weekCalendarView.weekCalendarCollectionView.frame = CGRectMake(xMargin, 0, weekCalendarView.width - xMargin,
                                                                   weekCalendarView.height);
    return weekCalendarView;
}

- (void)adjustViewFrame:(CGRect)referenceFrame
{
    CGFloat xMargin = 40;
    CGRect frame = referenceFrame;
    if([[AppUtilities sharedUtilities] isLandscape] == NO)
    {
        xMargin = 50;
    }
    frame.origin.x = xMargin;
    frame.size.width = frame.size.width - xMargin;
    self.weekCalendarCollectionView.frame = frame;
}

#pragma mark - Implementation
- (void)reload
{
    if (self.dates == nil)
    {
        NSDate *nearestSunday   = [NSDate GetNearestPreviousSundayFromDate:self.startDate];
        NSDate *nearestSaturday = [NSDate GetNearestNextSaturdayFromDate:self.endDate];
        self.dates = [NSDate GetAllDatesInUTCLiesBetween:nearestSunday andEndDate:nearestSaturday];
    }
    [self.weekCalendarCollectionView reloadData]; // reload with completion.
}

- (void)reloadWithCompletionHandler:(void (^)(void))completionHandler
{
    if (self.dates == nil)
    {
        NSDate *nearestSunday   = [NSDate GetNearestPreviousSundayFromDate:self.startDate];
        NSDate *nearestSaturday = [NSDate GetNearestNextSaturdayFromDate:self.endDate];
        self.dates = [NSDate GetAllDatesInUTCLiesBetween:nearestSunday andEndDate:nearestSaturday];
    }
    
    [self.weekCalendarCollectionView reloadDataWithCompletion:^{
        
        if (completionHandler != nil)
        {
            completionHandler();
        }
    }];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  self.dates.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WeekViewCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:[WeekViewCell CellIdentifier]
                                              forIndexPath:indexPath];
    if (self.dates != nil && self.dates.count >= indexPath.row)
    {
        NSDate *date  = self.dates[indexPath.row];
        //Configure cell for default behaviour
        [cell setDate:date andCalendar:self.calendar];

        //Configuring cell custom behviour
        [cell prepareCellForDate:date];
        
        if ([NSDate isDate1:self.selectedDate equalToDate:date])
        {
            [cell setCellSelected:YES];
        }
        else
        {
            [cell setCellSelected:NO];
        }
        
            [cell setEnabled:NO];

        
        [self.sessionUniqueDateKeys enumerateObjectsUsingBlock:^(NSDate *sessionDate , NSUInteger idx, BOOL *stop) {
            
            //Enable only the cells having the sessions in it
            if ([NSDate isDate1:sessionDate equalToDate:date])
            {
                [cell setEnabled:YES];
                *stop = YES;
            }
        }];
    }
   
    return cell;
}

- (BOOL)shouldDisableCellForDate:(NSDate*)date
{
    return ![self shouldAllowSelectionForDate:date];
}

- (BOOL)shouldAllowSelectionForDate:(NSDate*)date
{
    if ([NSDate isUTCDate1:date lessThanDate:self.startDate forCalendar:self.calendar])
    {
        return NO;
    }
    
    if ([NSDate isDate1:date greaterThanDate:self.endDate])
    {
        return NO;
    }
    
    return YES;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.weekCalendarCollectionView.width / 7;
    return CGSizeMake(width, 64);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Cell selected at indexPath %@",indexPath]];
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(weekCalendarView:didSelectDate:)])
    {
        WeekViewCell *cell = (WeekViewCell*)[self.weekCalendarCollectionView cellForItemAtIndexPath:indexPath];
        [self.delegate weekCalendarView:self didSelectDate:cell.date];
    }
}


#pragma mark - Scrollview delegate Method
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger currentIndex = self.weekCalendarCollectionView.contentOffset.x / self.weekCalendarCollectionView.frame.size.width;
    [self makeBottomBarSelectedForPageIndex:currentIndex];
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Current index %ld", (long)currentIndex]];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger currentIndex = self.weekCalendarCollectionView.contentOffset.x / self.weekCalendarCollectionView.frame.size.width;
    self.currentPage = currentIndex;
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Current index %ld", (long)currentIndex]];
}

- (void)makeBottomBarSelectedForPageIndex:(NSInteger)pageIndex
{
    pageIndex = pageIndex*7;
    
    for (NSInteger i=0; i<7; i++)
    {
        if(pageIndex <= self.dates.count - 1 )
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageIndex inSection:0];
            WeekViewCell *cell = (WeekViewCell*)[self.weekCalendarCollectionView cellForItemAtIndexPath:indexPath];
            
            NSDate *date  = self.dates[pageIndex];
            
            if ([NSDate isDate1:self.selectedDate equalToDate:date])
            {
                [cell setCellSelected:YES];
            }
            else
            {
                [cell setCellSelected:NO];
            }
        }
        pageIndex += 1;
    }
    
    
}

- (void)showWeekCalendarViewWithPageIndex:(NSInteger)pageIndex
{
    
    //show current page --- current page and pageIndex is same
    //self.weekCalendarView.contentOffset.x = self.weekCalendarView.width * pageIndex;
    CGPoint contentOffset = self.weekCalendarCollectionView.contentOffset;
    contentOffset.x       = self.weekCalendarCollectionView.width * pageIndex;
    self.currentPage      = pageIndex;
    
    [self.weekCalendarCollectionView setContentOffset:contentOffset animated:YES];
}

- (NSIndexPath*)getIndexPathForDate:(NSDate*)date
{
    NSInteger index = [self indexPositionOfDate:date]; //scroll to position left
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    return indexPath;
}

- (BOOL)isCellVisibleAtIndexPath:(NSIndexPath*)indexPath
{
    NSInteger currentIndex = self.weekCalendarCollectionView.contentOffset.x / self.weekCalendarCollectionView.width;
    
    if(indexPath.row >= currentIndex*7)
    {
        WeekViewCell *cell = (WeekViewCell*)[self.weekCalendarCollectionView cellForItemAtIndexPath:indexPath];
        
        NSArray *visibleCells = [self.weekCalendarCollectionView visibleCells];
        if ([visibleCells containsObject:cell])
        {
            return YES;
        }
        return NO;
    }
    else
    {
        return NO;
    }
}

- (NSInteger)indexPositionOfDate:(NSDate*)date
{
    __block NSInteger index = 0;
    
    [self.dates enumerateObjectsUsingBlock:^(NSDate *anotherDate  , NSUInteger idx, BOOL *stop) {
        
        if ([NSDate isDate1:date equalToDate:anotherDate])
        {
            *stop = YES; // CHECK DOES IT STOPS ENUMERATION
            index = idx;
        }
    }];
    
    return index;
}

- (NSInteger)getExpectedPageOfWeekForDate:(NSDate*)date
{
    //test this behaviour
    NSInteger indexPosition = [self indexPositionOfDate:date];
    NSInteger page = indexPosition / 7;
    
    if (indexPosition % 7 == 0)
    {
//        page -= 1;
    }
    return page ;
}

- (void)setCellSelected:(BOOL)selected atIndexPath:(NSIndexPath*)indexPath
{
    WeekViewCell *cell = (WeekViewCell*)[self.weekCalendarCollectionView cellForItemAtIndexPath:indexPath];
    if (cell != nil)
    {
        [cell setCellSelected:selected];
    }
}

- (void)setCellSelected:(BOOL)selected atIndexPath:(NSIndexPath*)indexPath andShowAnimationFromLeftSide:(BOOL)leftSide
{
    WeekViewCell *cell = (WeekViewCell*)[self.weekCalendarCollectionView cellForItemAtIndexPath:indexPath];
    if (cell != nil)
    {
        [cell setCellSelected:selected andShowAnimationFromLeftSide:leftSide];
    }
}

- (BOOL)haveReachedUpperLimitOfWeekCalendarForDate:(NSDate*)date
{
    NSInteger index = [self indexPositionOfDate:date];
    //Adding for Sunday
    index += 1;
    if (index == self.dates.count - 1)
    {
        return YES;
    }
    return NO;
}

- (BOOL)haveReachedLowerLimitOfWeekCalendarForDate:(NSDate*)date
{
    NSInteger index = [self indexPositionOfDate:date];
    //Adding for Sunday
    index -= 1;
    if (index == 0)
    {
        return YES;
    }
    return NO;
}

- (void)setSelectedDate:(NSDate *)selectedDate
{
    _selectedDate = selectedDate;
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Selected date %@", selectedDate]];
}

@end
