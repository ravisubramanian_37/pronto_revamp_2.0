//
//  CalendarDetailVC.m
//  Pronto
//
//  Created by Praveen Kumar on 22/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.

#import "CalendarDetailVC.h"
#import "Pronto-Swift.h"
#import "SessionsView.h"
//#import "EventsFakeTabbarView.h"
//#import "ZMHeader.h"
#import "ZMUserActions.h"
#import "ZMProntoTheme.h"
#import "UIFont+FontDirectory.h"
#import "UIColor+ColorDirectory.h"
#import "AppDelegate.h"
//#import "ProntoTabbarViewController.h"
#import "EventsCalendarHeaderView.h"
#import "HelpViewController.h"
#import "ZMNotifications.h"
#import "UIImage+ImageDirectory.h"
#import "FeedbackView.h"
#import "WeekCalendarView.h"
#import "NSDate+DateDirectory.h"
#import "EventListViewController.h"
#import "Asset+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import "EventAssetHandler.h"
#import "NSDate+DateDirectory.h"
#import "AppUtilities.h"
#import "AssetView.h"
#import "EventOnlyAssetHandler.h"
#import "SessionsCell.h"

#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventsViewController.h"
#import "ZMAssetViewController.h"
//#import "ZMPDFDocument.h"

#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "EventCoreDataManager.h"
#import "ProntoOverlayView.h"

//#import "GlobalSearchVC.h"
#import "AppUtilities.h"
//#import "EventsGlobalSearchViewModel.h"
#import "Filter.h"
#import "Asset.h"
//#import "ZMSearchMenu.h"
//#import "ZMLibrary.h"

#import "ProntoUserDefaults.h"

#import "EventAssetDownloadProvider.h"

#import "FastPDFViewController.h"
#import <FastPdfKit/FastPdfKit.h>

typedef void(^OperationStatusCompletion)(BOOL success);

@interface CalendarDetailVC ()<WeekCalendarViewDelegate>
@property(nonatomic,strong) SessionsView *sessionsView;
@property (strong, nonatomic) ZMHeader *headerViewEventsDetail;
@property (nonatomic, strong) id <ZMProntoTheme> theme;
//@property (nonatomic, strong) EventsFakeTabbarView *eventsFakeTabbarView;
@property (nonatomic, strong) EventsCalendarHeaderView *eventsCalendarHeaderView;
@property (nonatomic, strong) ZMNotifications *notificationsPannel;
@property (nonatomic, strong) FeedbackView *feedbackView;
@property (nonatomic, strong) WeekCalendarView *weekCalendarView;
@property (nonatomic, strong)  UIView *backgroundView;

//SwipeGesture
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGesture;
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGesture;

@property (nonatomic, strong) EventAssetHandler *eventAssetHandler;
@property (nonatomic, strong) EventOnlyAssetHandler *eventOnlyAssetHandler;

@property (nonatomic, assign, getter=isNetworkRequestInProgress) BOOL networkRequestInProgress;

/**
 It will hold the unique dates of all sessions belonging to one event
 */
@property (nonatomic, strong) NSArray *sessionUniqueDateKeys;
@property (nonatomic, weak) ZMAbbvieAPI *apiManager;

// Global Search View
//@property (nonatomic, strong) GlobalSearchVC *globalSearcVC;

@property (nonatomic, strong)  ProntoOverlayView *prontoOverlayView;

@property (nonatomic, strong) NSDictionary *dictionaryToPrint;

@property (nonatomic, assign) BOOL isAssetDownloadInProgress;
@property (nonatomic, strong) Asset *currentSelectedAssetToSeeDetail;
@end

@implementation CalendarDetailVC
AudioPlayer *audioPlayerView;
Asset *assetForAudio;
BOOL _isDownloadignAsset;
- (void)createSwipeGestures
{
    if (self.rightSwipeGesture == nil)
    {
        self.rightSwipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeGesture:)];
        self.rightSwipeGesture.direction = UISwipeGestureRecognizerDirectionRight; // finger moves from left to right
        [self.calendarDetailContainer addGestureRecognizer:self.rightSwipeGesture];
    }
    
    if (self.leftSwipeGesture == nil)
    {
        self.leftSwipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeGesture:)];
        self.leftSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft; // finger moves from right to left
        [self.calendarDetailContainer addGestureRecognizer:self.leftSwipeGesture];
    }
}

- (void)handleSwipeGesture:(UISwipeGestureRecognizer*)swipeGesture
{
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight)
    {
        // finger moves from left to right
        [AbbvieLogging logInfo:@"right recognizer"];

        NSDate *currentSelecteDate = self.weekCalendarView.selectedDate;
        
        NSInteger currentIndexOfSelectedDate = 0;
        NSInteger indexOfPreviousDate = -1;
        
        if(![self.sessionUniqueDateKeys containsObject:currentSelecteDate])
        {
            NSMutableArray *arrayOfDatesLessThanSelectedDate = [NSMutableArray array];
            for(NSDate* date in self.sessionUniqueDateKeys)
            {
                NSComparisonResult result = [currentSelecteDate compare:date]; // comparing two dates
                
                if(result == NSOrderedDescending)
                {
                    [arrayOfDatesLessThanSelectedDate addObject:date];
                }
            }
            if(arrayOfDatesLessThanSelectedDate.count > 0)
            {
                currentSelecteDate = [arrayOfDatesLessThanSelectedDate lastObject];
                currentIndexOfSelectedDate = [self.sessionUniqueDateKeys indexOfObject:currentSelecteDate];
                indexOfPreviousDate = currentIndexOfSelectedDate;
            }
        }
        else
        {
            currentIndexOfSelectedDate = [self.sessionUniqueDateKeys indexOfObject:currentSelecteDate];
            indexOfPreviousDate = currentIndexOfSelectedDate - 1;
        }
        
        if (indexOfPreviousDate >= 0)
        {
            NSDate *previousDate = self.sessionUniqueDateKeys[indexOfPreviousDate];
            NSIndexPath *indexPathForSelectedDate = [self.weekCalendarView getIndexPathForDate:previousDate];
            if ([self.weekCalendarView isCellVisibleAtIndexPath:indexPathForSelectedDate] == NO)
            {
                //declare one method for weekself.
                NSIndexPath *unhighlightCellAtIndexPath = [self.weekCalendarView getIndexPathForDate:currentSelecteDate];
                [self.weekCalendarView setCellSelected:NO atIndexPath:unhighlightCellAtIndexPath];
                
                self.weekCalendarView.selectedDate = previousDate;
                NSInteger pageIndex = [self.weekCalendarView getExpectedPageOfWeekForDate:previousDate];
                [self.weekCalendarView showWeekCalendarViewWithPageIndex:pageIndex];
                //Changing page index with animation, so it takes fraction of second. and therefore using delay
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.weekCalendarView setCellSelected:YES atIndexPath:indexPathForSelectedDate];
                });
            }
            else
            {
                NSIndexPath *previousSelectedIndexPath = [self.weekCalendarView getIndexPathForDate:self.weekCalendarView.selectedDate];
                [self.weekCalendarView setCellSelected:NO atIndexPath:previousSelectedIndexPath];
                [self.weekCalendarView setCellSelected:YES atIndexPath:indexPathForSelectedDate andShowAnimationFromLeftSide:NO];
            }
            self.weekCalendarView.selectedDate = previousDate;
            NSArray *durations = [self.event getSortedDurationForDateString:previousDate];
            if (durations.count > 0)
            {
                self.sessionsView.durations = durations;
                [self.sessionsView scrollTableToTop];
                [self refreshCalendarDetail:self.weekCalendarView.selectedDate];
                [self initiateOfflineDownloadForDate:self.weekCalendarView.selectedDate];
            }
        }
        else
        {
            //No selection/swipe possible
            UIAlertController *alertController = [self getAlertControllerWithMessage:@"There are no prior sessions available"];
            [self presentViewController:alertController animated:YES completion:NULL];
        }
    }
    else if (swipeGesture.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        // finger moves from right to left
        [AbbvieLogging logInfo:@"Left recognizer"];
        /*
         Actual Logic: Get the next date where need to move according to datasource.
         if: This date is in visible range.
         direct mark selected that date and on completion load the session detail.
         Else:change the page and mark selected date which we got from data sou
         ]rce.
         */
        
        // if it weekend and go to next page
        //else get cell for indexpath
        // from the data source we can ask for next data, we can directly to that date.
        // if it reaches to end of week view
        NSDate *currentSelecteDate = self.weekCalendarView.selectedDate;
        
        NSInteger currentIndexOfSelectedDate = 0;
        NSInteger indexOfNextDate = self.sessionUniqueDateKeys.count;
        if(![self.sessionUniqueDateKeys containsObject:currentSelecteDate])
        {
            NSMutableArray *arrayOfDatesGreaterThanSelectedDate = [NSMutableArray array];
            for(NSDate* date in self.sessionUniqueDateKeys)
            {
                NSComparisonResult result = [currentSelecteDate compare:date]; // comparing two dates
                
                if(result == NSOrderedAscending)
                {
                    [arrayOfDatesGreaterThanSelectedDate addObject:date];
                }
            }
            if(arrayOfDatesGreaterThanSelectedDate.count > 0)
            {
                currentSelecteDate = [arrayOfDatesGreaterThanSelectedDate firstObject];
                currentIndexOfSelectedDate = [self.sessionUniqueDateKeys indexOfObject:currentSelecteDate];
                indexOfNextDate = currentIndexOfSelectedDate;
            }
        }
        else
        {
            currentIndexOfSelectedDate = [self.sessionUniqueDateKeys indexOfObject:currentSelecteDate];
            indexOfNextDate = currentIndexOfSelectedDate + 1;
        }
        
        if (indexOfNextDate < self.sessionUniqueDateKeys.count)
        {
            //There could be selection
            NSDate *nextDate = self.sessionUniqueDateKeys[indexOfNextDate];
        
            NSIndexPath *indexPathForSelectedDate = [self.weekCalendarView getIndexPathForDate:nextDate];
            if ([self.weekCalendarView isCellVisibleAtIndexPath:indexPathForSelectedDate] == NO)
            {
                //declare one method for weekself.
                NSIndexPath *unhighlightCellAtIndexPath = [self.weekCalendarView getIndexPathForDate:currentSelecteDate];
                [self.weekCalendarView setCellSelected:NO atIndexPath:unhighlightCellAtIndexPath];
                
                NSInteger pageIndex = [self.weekCalendarView getExpectedPageOfWeekForDate:nextDate];
                [self.weekCalendarView showWeekCalendarViewWithPageIndex:pageIndex];
                //Changing page index with animation, so it takes fraction of second. and therefore using delay
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [self.weekCalendarView setCellSelected:YES atIndexPath:indexPathForSelectedDate];
                });
            }
            else
            {
                NSIndexPath *previousSelectedIndexPath = [self.weekCalendarView getIndexPathForDate:self.weekCalendarView.selectedDate];
                [self.weekCalendarView setCellSelected:NO atIndexPath:previousSelectedIndexPath];
                [self.weekCalendarView setCellSelected:YES atIndexPath:indexPathForSelectedDate andShowAnimationFromLeftSide:YES];
            }
            self.weekCalendarView.selectedDate = nextDate;
            [self refreshCalendarDetail:self.weekCalendarView.selectedDate];
            
            NSArray *durations = [self.event getSortedDurationForDateString:nextDate];
            if (durations.count > 0)
            {
                self.sessionsView.durations = durations;
                [self.sessionsView scrollTableToTop];
                [self refreshCalendarDetail:self.weekCalendarView.selectedDate];
                [self initiateOfflineDownloadForDate:self.weekCalendarView.selectedDate];
            }
        }
        else
        {
            //No selection/swipe possible
            UIAlertController *alertController = [self getAlertControllerWithMessage:@"There are no future sessions available"];
            [self presentViewController:alertController animated:YES completion:NULL];
        }
    }
}


- (void)loadWeekCalendarView
{

    /*NSDate *date2 = [NSDate findEndDateFromStartDate:date1 byAddingDay:40];
    
    self.weekCalendarView.startDate = date1;
    self.weekCalendarView.endDate   = date2; */
    
    
    if (self.weekCalendarView == nil)
    {
        self.weekCalendarView = [WeekCalendarView GetWeekCalendarViewWithFrame:self.dateHeaderView.bounds];
        self.weekCalendarView.delegate = self;
        
        self.weekCalendarView.startDate = [self.event eventStartDate];
        self.weekCalendarView.endDate   = [self.event eventEndDate];
        
        self.weekCalendarView.selectedDate = self.selectedDate;
        
        [self.dateHeaderView addSubview:self.weekCalendarView];
        
        self.weekCalendarView.sessionUniqueDateKeys = self.sessionUniqueDateKeys;
        
        [self.weekCalendarView reloadWithCompletionHandler:^{
            NSIndexPath *indexPathForSelectedDate = [self.weekCalendarView getIndexPathForDate:self.weekCalendarView.selectedDate];
            if ([self.weekCalendarView isCellVisibleAtIndexPath:indexPathForSelectedDate] == NO)
            {
                NSInteger pageIndex = [self.weekCalendarView getExpectedPageOfWeekForDate:self.weekCalendarView.selectedDate];
                [self.weekCalendarView showWeekCalendarViewWithPageIndex:pageIndex];
            }
            
            [self refreshCalendarDetail:self.weekCalendarView.selectedDate];
        }];
    }
}

- (void)addSelfToListenForNotificationCenter
{
    [self registerNotification:kZMAssetViewControllerGoBackNotification];
    [self registerNotification:kZMPDFDocumentDidCloseButtonPressedNotification];
    [self registerNotification:kPreferenceListVCDidSavePreferencesNotification];
    [self registerNotification:kAssetV2CompletionNotification];
    [self registerNotification:UIApplicationWillEnterForegroundNotification];
}

#pragma mark - Notifications

- (void)removeNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:name object:nil];
}

- (void)registerNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:name object:nil];
}

- (void)recievedNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:kZMAssetViewControllerGoBackNotification] || [[notification name] isEqualToString:kZMPDFDocumentDidCloseButtonPressedNotification])
    {
        //if user has come from global search flow and then opening asset
        // poping of view will be taken care from global search
        if([ZMUserActions sharedInstance].globalSearchViewController == NO)
        {
            NSArray *array = [self.navigationController viewControllers];
            
            for(UIViewController *controller in array)
            {
                if([controller isKindOfClass:[CalendarDetailVC class]])
                {
                    [self.navigationController popToViewController:controller animated:YES];
                    return;
                }
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if ([[notification name] isEqualToString:UIApplicationWillEnterForegroundNotification])
    {
        //Refreshing calendar detail when user coming from background to foreground.

        [self makeServiceRequestsAndUpdateUIOnCompletion];
    }
    else if ([[notification name] isEqualToString:kAssetV2CompletionNotification])
    {
        [self refreshCalendarDetail:self.weekCalendarView.selectedDate]; //Verify the selected date
        [self refreshLocalDBFromAssetV2:NO]; //Resetting other flags.
    }
    
    
    // Resetting Event Asset Handler
    self.eventAssetHandler = nil;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //Tabbar tag reset
    [self setupUI];
    [self loadEventsCalendarHeaderView];
    [self loadWeekCalendarView];
    [self prepareSessionViewWithData];
//    [self loadEventsFakeTabbarView];
    
    self.headerViewEventsDetail.tabBarTag = -1;
    self.headerViewEventsDetail.calendarDetailVC = self;
//    self.eventsGSViewModel = [[EventsGlobalSearchViewModel alloc]initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)self];
    
    if (self.isComingFromGlobalSearchResult == YES)
    {
        //Resetting the flag value. don't need to do cms call
        self.isComingFromGlobalSearchResult = NO;
    }
    else
    {
        //TODO: check for redesign - Saranya
        //Rest of cases we will make network request.
        [self makeServiceRequestsAndUpdateUIOnCompletion];
    }
    [self initiateOfflineDownloadForDate:self.selectedDate];
    
    [self createSwipeGestures];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addSelfToListenForNotificationCenter];
    [self.headerViewEventsDetail prepareHeaderView];
    self.sessionUniqueDateKeys = [self.event getUniqueDatesOfAllSessionSortedAccordingToStartTime];
    
//    if([self.headerViewEventsDetail.userFranchise.text  isEqual: @"In House"] || [self.headerViewEventsDetail.userFranchise.text  isEqual: @"Home Office"])
//    {
//        [self.theme styleHOUser:self.headerViewEventsDetail.UserInformation];
//    } else
//    {
//        [self.theme styleFGUser:self.headerViewEventsDetail.UserInformation franchise:self.headerViewEventsDetail.userFranchise.text colorCode:[ZMUserActions sharedInstance].franchiseColor];
//    }
    
    // Profile Icon
//    [self.headerViewEventsDetail.profileButton addTarget:self.headerViewEventsDetail
//                                                  action:@selector(profileSelected:)
//                                        forControlEvents: UIControlEventTouchUpInside];
    
    [self reloadAllData];
    [self.navigationController setNavigationBarHidden:YES];
    
//    [self getNotifications];
//    [self geTotalOfNotifications];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.eventAssetHandler.currentSelectedAssetToSeeDetail = nil;
    self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeNotification:UIApplicationWillEnterForegroundNotification];
    [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] pauseOfflineDownload:NO];
    [[AssetDownloadProvider sharedAssetDownloadProvider] pauseOfflineDownload:NO];
    
    //For safer size, if there is any bottom indicatore remove it.
    AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [mainDelegate hideMessage];
    
    ((ZMAbbvieAPI*)[ZMAbbvieAPI sharedAPIManager])._syncingCompletion = YES;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
//    [self.notificationsPannel adjustNotifications];
//    [self.notificationsPannel changeOfOrientation];
//    [self.headerViewEventsDetail prepareHeaderView];
//    [self createSwipeGestures];
//    [self.feedbackView adjustFrame:self.view];
}

//Capture the rotation of screens
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
//    [self.eventsFakeTabbarView setTabBarItemFrame];
    [self.headerViewEventsDetail settingsShowOnRotate];
    [self.headerViewEventsDetail searchShowOnRotate];
    [self.headerViewEventsDetail adjustStatusBar];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.headerViewEventsDetail settingsHideOnRotate];
    [self.headerViewEventsDetail searchHideOnRotate];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self reloadAllData];
    [self.prontoOverlayView adjustProntoOverlay];
}

-(void) reloadAllData
{
    NSInteger widthValue = 0;
    if([[AppUtilities sharedUtilities] isLandscape] == YES)
    {
        if(self.view.frame.size.width > self.view.frame.size.height)
        {
            widthValue = self.view.frame.size.width;
        }
        else
        {
            widthValue = self.view.frame.size.height;
        }
    }
    else
    {
        if(self.view.frame.size.width < self.view.frame.size.height)
        {
            widthValue = self.view.frame.size.width;
        }
        else
        {
            widthValue = self.view.frame.size.height;
        }
    }
    self.dateHeaderView.frame = CGRectMake(self.dateHeaderView.frame.origin.x, self.dateHeaderView.frame.origin.y, widthValue, self.dateHeaderView.frame.size.height);
    [self.weekCalendarView adjustViewFrame:self.dateHeaderView.bounds];
    self.weekCalendarView.sessionUniqueDateKeys = self.sessionUniqueDateKeys;
    [self.weekCalendarView reloadWithCompletionHandler:^{
        // do any other setup.
        //if left swipe currentPage -1
        // if right swip current page + 1
        [self.weekCalendarView showWeekCalendarViewWithPageIndex:self.weekCalendarView.currentPage];
    }];
    
//    [self.eventsFakeTabbarView setTabBarItemFrame];
    [self.sessionsView reloadData];
}

+(CalendarDetailVC*)GetCalendarDetailsScreen
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"CalendarDetailScene" bundle:nil];
    CalendarDetailVC *calendarDetailScreenVC = [sb instantiateViewControllerWithIdentifier:@"CalendarDetailVC"];
    return calendarDetailScreenVC;
}

- (void)loadEventsCalendarHeaderView
{
    if (self.eventsCalendarHeaderView == nil)
    {
        CGRect frame = self.headerView.bounds;
        self.eventsCalendarHeaderView = [EventsCalendarHeaderView GetEventsCalendarHeaderViewWithFrame:frame];
        self.eventsCalendarHeaderView.delegate = (id <EventsCalendarHeaderViewDelegate>)self;
    }
    [self.headerView addSubview:self.eventsCalendarHeaderView];
    
    //Setup values and adjust header for calendar detail screen
    [self.eventsCalendarHeaderView prepareHeaderViewWithData:self.event];
    [self.eventsCalendarHeaderView prepareHeaderViewForSessionScreen];
}

//- (void)loadEventsFakeTabbarView
//{
//    if (self.eventsFakeTabbarView == nil)
//    {
//        CGRect frame = [[UIScreen mainScreen] bounds];
//        frame.origin.y = frame.size.height - [EventsFakeTabbarView GetEventsFakeTabbarViewHeight];
//        frame.size.height = [EventsFakeTabbarView GetEventsFakeTabbarViewHeight];
//        self.eventsFakeTabbarView = [EventsFakeTabbarView GetEventsFakeTabbarViewWithFrames:frame];
//        self.eventsFakeTabbarView.delegate = (id <EventsFakeTabbarViewDelegate>)self;
//    }
//
//    [self.view addSubview:self.eventsFakeTabbarView];
//    [self.eventsFakeTabbarView setTabBarItemFrame];
//}

- (void)setupUI
{
    self.overallView.backgroundColor = [UIColor GetColorFromHexValuef2f2f2];
    self.dateHeaderView.backgroundColor = [UIColor GetColorFromHexValue0082ba];
    self.dateHeaderMainView.backgroundColor = [UIColor GetColorFromHexValueD9E3EE];
    self.dateLabel.font = [UIFont FontLatoRegularSize12];

    [self setupHeader];
    [self addSelfToListenForNotificationCenter];
}

- (void)setupHeader
{
//    self.headerViewEventsDetail = (ZMHeader *)[[[NSBundle mainBundle] loadNibNamed:@"Header" owner:self options:nil] objectAtIndex:0];
//    CGRect frame = self.mainHeaderView.frame;
//    frame.size.width = self.view.frame.size.width;
//    self.mainHeaderView.frame = frame;
//    [self.headerViewEventsDetail initWithOptions:self.mainHeaderView];
//    self.headerViewEventsDetail.showResumeSelection = YES;
//    self.headerViewEventsDetail.delegate = (id<headerActions>)self;
//    [self.headerViewEventsDetail prepareHeaderView];
//    self.headerViewEventsDetail.sourceViewController = self;
    
    //Header View
    HomeMainHeaderView *mainHeaderView = [[[NSBundle mainBundle] loadNibNamed:RedesignConstants.mainViewHeaderNib owner:nil options:nil] objectAtIndex:0];
    [self.mainHeaderView addSubview:mainHeaderView];
    [mainHeaderView setThemes];
    self.view.backgroundColor = [RedesignThemesClass sharedInstance].defaultThemeforUser;

    mainHeaderView.frame = CGRectMake(0, 0, self.mainHeaderView.frame.size.width, 60);
    
}

- (void)prepareSessionViewWithData
{
    if (self.sessionsView == nil)
    {
        self.sessionsView = [SessionsView GetSessionsViewFrame:self.calendarDetailContainer.bounds];
    }
    self.sessionsView.delegate = (id<EventsSessionsViewDelegate>)self;
    [self.calendarDetailContainer addSubview:self.sessionsView];
    self.sessionsView.durations = [self.event getSortedDurationForDateString:self.selectedDate];
    
}

-(void) refreshCalendarDetail:(NSDate*)date
{
    self.dateLabel.text = [NSDate GetDateForCalendatDetailTableHeader:date];
    [self.sessionsView reloadData];
}

- (void)makeEventDetailServiceRequestWithCMS:(OperationStatusCompletion)completionHandler
{
    if (self.isNetworkRequestInProgress == YES) { return; }
    
    self.networkRequestInProgress = YES;
    [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingEventDetails];
    //TODO:Update one with new service in Events Service class
    [[EventsServiceClass sharedAPIManager] getEventDetails:self.event.eventId complete:^(NSDictionary *sessionDetails) {
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"sessionsFetched%@:", sessionDetails]];
        
       
        if (sessionDetails != nil &&
            [sessionDetails isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *sessionDetail = [[NSDictionary alloc] initWithObjectsAndKeys:sessionDetails, self.event.eventId, nil];
//            NSDictionary *sessionDetail = (NSDictionary*)[sessionDetails valueForKey:self.event.eventId];
            if (sessionDetail != nil && [sessionDetail isKindOfClass:[NSDictionary class]] && sessionDetail.count <=0 ) {
                
                //It means that event has been deleted from CMS
                //go back to landing screen with updated list
                
                [[EventCoreDataManager sharedInstance] removeEvent:self.event withCompletionHandler:^(BOOL success) {
                    
                    if (success == YES)
                    {
                        self.event = nil;
                        completionHandler(NO);
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                                 message:EventHasBeenDeletedMessage
                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAlertAction = [UIAlertAction actionWithTitle:AlertOKTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:kEventDeletedFromCMSNotification object:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                
                                [self.navigationController popToRootViewControllerAnimated:YES];
                                
                            });
                        }];
                        [alertController addAction:okAlertAction];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }];
            }
            else
            {
                [[EventCoreDataManager sharedInstance] saveEventDetail:sessionDetails withCompletionHandler:^(BOOL success, Event *event) {
                    
                    if (success == YES)
                    {
                        //self.event = event;
                        if (completionHandler != nil)
                        {
                            completionHandler(YES);
                        }
                    }
                    else
                    {
                        if (completionHandler != nil)
                        {
                            completionHandler(NO);
                        }
                    }
                }];
                
                [self prepareSessionViewWithData];
                [self.sessionsView reloadData];
                
            }
        }
        self.networkRequestInProgress = NO;
        //[[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
    } error:^(NSError *error) {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"getSessions error:%@", error]];
        self.networkRequestInProgress = NO;
       // [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
        completionHandler(NO);
    }];
}

- (void) initiateOfflineDownloadForDate:(NSDate*) date
{
    NSArray* durationsArray = [self.event getSortedDurationForDateString:date];
    if (durationsArray.count > 0)
    {
        [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] initiateOfflineDownloadForDurations:durationsArray];
    }
}

#pragma mark --
#pragma mark EventsCalendarHeaderView Delegate Methods

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapPrintButton:(UIButton*)sener
{
    [self populateData];
}

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapBackButton:(UIButton*)sener
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapRefreshButton:(UIButton*)sender
{
    [self makeServiceRequestsAndUpdateUIOnCompletion];
}

- (void)makeServiceRequestsAndUpdateUIOnCompletion
{
    __weak CalendarDetailVC *weakSelf = self;
    
    [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] pauseOfflineDownload:YES];
    [[AssetDownloadProvider sharedAssetDownloadProvider] pauseOfflineDownload:YES];
    [self makeEventDetailServiceRequestWithCMS:^(BOOL success) {
        
        if (success == YES)
        {
            NSArray *durations = [weakSelf.event getSortedDurationForDateString:self.weekCalendarView.selectedDate];
            self.sessionUniqueDateKeys = [self.event getUniqueDatesOfAllSessionSortedAccordingToStartTime];
            //Reload current selected date
            self.weekCalendarView.sessionUniqueDateKeys = self.sessionUniqueDateKeys;
            
            [weakSelf.weekCalendarView reloadWithCompletionHandler:^{
                weakSelf.sessionsView.durations = durations;
                [weakSelf refreshCalendarDetail:weakSelf.weekCalendarView.selectedDate];
            }];
            
            if (durations.count < 1)
            {
                UIAlertController *alertController = [self getAlertControllerWithMessage:@"No session available for selected date. Please swipe right/left to go to previous or future sessions"];
                [weakSelf presentViewController:alertController animated:YES completion:NULL];
            }
            
            // Refreshing Header view.
            [weakSelf.eventsCalendarHeaderView prepareHeaderViewWithData:weakSelf.event];
            
            
            //Also refreshing AssetV2 call
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [weakSelf refreshLocalDBFromAssetV2:YES];
            });
        }
        else
        {
            [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] pauseOfflineDownload:NO];
            [[AssetDownloadProvider sharedAssetDownloadProvider] pauseOfflineDownload:NO];
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingEventDetails];
        }
    }];
}


#pragma mark --

#pragma mark ProntoHeaderView Delegate Methods




- (BOOL)shouldAllowSelectionForDate:(NSDate*)date
{
    if ([NSDate isPastDate:date] == YES ||
        [NSDate isDate1:date lessThanDate:[self.event eventStartDate]])
    {
        return NO;
    }
    
    if ([NSDate isDate1:date greaterThanDate:[self.event eventEndDate]])
    {
        return NO;
    }
    
    return YES;
}


- (void)weekCalendarView:(WeekCalendarView*)weekCalendarView didSelectDate:(NSDate*)date
{
    //if ([self.weekCalendarView shouldAllowSelectionForDate:date] == NO) { return; }
    
    NSArray *durations = [self.event getSortedDurationForDateString:date];
    if (durations.count > 0)
    {
        //Selected date in week view;
        self.weekCalendarView.selectedDate = date;
        self.weekCalendarView.sessionUniqueDateKeys = self.sessionUniqueDateKeys;
        [self.weekCalendarView reloadWithCompletionHandler:^{
            self.sessionsView.durations = durations;
            [self.sessionsView scrollTableToTop];
            [self refreshCalendarDetail:self.weekCalendarView.selectedDate];
            [self initiateOfflineDownloadForDate:self.weekCalendarView.selectedDate];
        }];
    }
    else
    {
        UIAlertController *alertController = [self getAlertControllerWithMessage:@"No session available for selected date. Please swipe right/left to go to previous or future sessions"];
        [self presentViewController:alertController animated:YES completion:NULL];
    }
   
}


- (void)weekCalendarView:(WeekCalendarView*)weekCalendarView  scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}

   
#pragma mark --
#pragma mark EventsPrint Delegate Methods

//Populating print data
-(void) populateData
{
    [self showLoadingIndicator:YES];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self printPDF:[self constructPrintData] assetTitle:@"Print Data"];
    });
}

-(void) showLoadingIndicator:(BOOL)show
{
    if(show == YES)
    {
        self.prontoOverlayView = [ProntoOverlayView PrepareProntoOverlayWithView:self.view];
        [self.prontoOverlayView showProntoOverlayViewWithMessage:ProntoMessagePreparingDataToPrint];
    }
    else
    {
        [self.prontoOverlayView removeProntoOverlay];
    }
}

//Print preview the constructed PDF
- (void)printPDF:(NSData*)pdfData assetTitle: (NSString*) title {
    UIPrintInteractionController *printer=[UIPrintInteractionController sharedPrintController];
    UIPrintInfo *info = [UIPrintInfo printInfo];
    info.orientation = UIPrintInfoOrientationPortrait;
    info.outputType = UIPrintInfoOutputGeneral;
    info.jobName= [NSString stringWithFormat:@"%@",title];
    info.duplex=UIPrintInfoDuplexLongEdge;
    printer.printInfo = info;
//    printer.showsPageRange=YES;
    printer.printingItem=pdfData;
    
    UIPrintInteractionCompletionHandler completionHandler =
    ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        if (!completed && error)
        {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"FAILED! error = %@",[error localizedDescription]]];
        }
        else if (!completed && !error)
        {
            [self.sessionsView reloadData];
        }
    };
    [self showLoadingIndicator:NO];
    [printer presentAnimated:YES completionHandler:completionHandler];
}

- (NSMutableData*)constructPrintData
{
    NSDictionary *data = [[AppUtilities sharedUtilities] renderPrintDictionaryForEvent:self.event];
    
    NSInteger kMargin = 20;
    NSInteger kDefaultPageWidth = [UIScreen mainScreen].bounds.size.width;
    NSInteger kDefaultPageHeight = [UIScreen mainScreen].bounds.size.height;
    NSInteger kColumnMargin = 2;
    
    // Create the PDF context using the default page size.
    // This default is spelled out in the iOS documentation for UIGraphicsBeginPDFContextToFile
    CGRect rct = {{0.0 , 0.0 } , {kDefaultPageWidth , kDefaultPageHeight}};
    NSMutableData *pdfData = [[NSMutableData alloc]init];
    UIGraphicsBeginPDFContextToData(pdfData, rct, nil);
    //    UIGraphicsBeginPDFContextToFile(pdfFilePath, CGRectZero, nil);
        
    // maximum height and width of the content on the page, byt taking margins into account.
    CGFloat maxWidth = kDefaultPageWidth - kMargin * 2;
    CGFloat maxHeight = kDefaultPageHeight - kMargin * 2;
    
    // Max Width of table
    CGFloat tableSingleMaxWidth = maxWidth;
    
    CGFloat tableDualMaxWidth = maxWidth / 2;
    
    // create the fonts and set its attributes
    UIFont* eventTitleFont = [UIFont FontLatoRegularSize16];
    
    UIFont* eventTitleSubheddingFont = [UIFont FontLatoRegularSize14];
    
    UIFont* tableTitleFont = [UIFont FontLatoBoldSize14];
    
    UIFont* tableDescriptionBoldFont = [UIFont FontLatoBoldSize10];
    
    UIFont* tableDescriptionFont = [UIFont FontLatoRegularSize10];
    
    NSMutableParagraphStyle *textStyleCenter = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyleCenter.lineBreakMode = NSLineBreakByWordWrapping;
    textStyleCenter.alignment = NSTextAlignmentCenter;
    
    NSMutableParagraphStyle *textStyleLeft = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyleLeft.lineBreakMode = NSLineBreakByWordWrapping;
    textStyleLeft.alignment = NSTextAlignmentLeft;
    
    NSDictionary *eventTitleAttributes = @{NSFontAttributeName:eventTitleFont,NSParagraphStyleAttributeName:textStyleCenter};
    NSDictionary *eventTitleSubheddingAtttributes = @{NSFontAttributeName:eventTitleSubheddingFont,NSParagraphStyleAttributeName:textStyleCenter};
    NSDictionary *tableTitleAtttributes = @{NSFontAttributeName:tableTitleFont,NSParagraphStyleAttributeName:textStyleCenter};
    
    NSDictionary *tableDescriptionBoldAtttributes = @{NSFontAttributeName:tableDescriptionBoldFont,NSParagraphStyleAttributeName:textStyleLeft};
    NSDictionary *tableDescriptionAtttributes = @{NSFontAttributeName:tableDescriptionFont,NSParagraphStyleAttributeName:textStyleLeft};
    
    CGFloat currentPageY = 0;
    
    //Initialize PDF and set initial Margin
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, kDefaultPageWidth, kDefaultPageHeight), nil);
    currentPageY = kMargin;
    int xOrigin = (int)kMargin;
    
    // draw the Event title name at the top of the page.
    NSString* eventTitle = [data objectForKey:@"EventTitle"];
    
    CGRect eventTitleSize = [eventTitle boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:eventTitleAttributes context:nil];
    
    [eventTitle drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleAttributes];
    
    currentPageY = currentPageY + floor(eventTitleSize.size.height + 5);
    
    // draw the Event sub hedding title name at the top of the page.
    NSString* eventLocation = [data objectForKey:@"EventLocation"];
    
    CGRect eventLocationSize = [eventLocation boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:eventTitleSubheddingAtttributes context:nil];
    
    if(eventLocation.length > 0)
    {
        [eventLocation drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleSubheddingAtttributes];
        
        currentPageY = currentPageY + floor(eventLocationSize.size.height + 5);
    }
    
    NSDate* startDate = [NSDate GetDateSameAsStringFromDateString:[data objectForKey:@"EventStartDate"]];
    NSDate* endDate = [NSDate GetDateSameAsStringFromDateString:[data objectForKey:@"EventEndDate"]];
    
    NSString* eventStartEndDate = [NSString stringWithFormat:@"%@ to %@",
                                   [NSDate getFormattedDateStringForGlobalSearchFromDate:startDate],[NSDate getFormattedDateStringForGlobalSearchFromDate:endDate]];
    
    CGRect eventStartEndDateSize = [eventStartEndDate boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:eventTitleSubheddingAtttributes context:nil];
    
    [eventStartEndDate drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleSubheddingAtttributes];
    
    currentPageY = currentPageY + floor(eventStartEndDateSize.size.height + eventTitleSize.size.height);
    
    NSString* timezone = [data objectForKey:@"EventTimeZone"];
    
    NSArray* durationDates = [data objectForKey:@"DurationDates"];
    
    NSArray* durations = [data objectForKey:@"Durations"];
    
    for (NSDate* date in durationDates)
    {
        NSString* sessionDate = [NSDate getFormattedDateStringForGlobalSearchFromDate:date];
        [sessionDate drawInRect:CGRectMake(kMargin, currentPageY, tableSingleMaxWidth, MAXFLOAT) withAttributes:tableTitleAtttributes];
        CGRect sessionDateSize = [sessionDate boundingRectWithSize:CGSizeMake(tableSingleMaxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:tableTitleAtttributes context:nil];
        [self drawTableAt:CGPointMake(xOrigin, currentPageY) withRowHeight:sessionDateSize.size.height andColumnWidth:tableSingleMaxWidth andRowCount:1 andColumnCount:1];
        currentPageY = currentPageY + floor(sessionDateSize.size.height);
        
        for (EventDuration* duration in durations)
        {
            NSDate *durationDate = [NSDate GetDateSameAsStringFromDateString:duration.date];
            if([durationDate isEqualToDate:date])
            {
                Session* session = (Session*)duration.session;
                
                NSString* eventDate = [NSString stringWithFormat:@"%@ - %@   %@",duration.startTime,duration.endTime,timezone];
                
                NSString* eventDescription = @"";
                if((session.location.length > 0) && (session.sessionDescription.length > 0))
                {
                    eventDescription = [NSString stringWithFormat:@"%@\r%@%@\r%@%@",session.title,PrintConstantLocation,session.location,PrintConstantDescription,session.sessionDescription];
                }
                else if (session.location.length > 0)
                {
                    eventDescription = [NSString stringWithFormat:@"%@\r%@%@",session.title,PrintConstantLocation,session.location];
                }
                else if (session.sessionDescription.length > 0)
                {
                    eventDescription = [NSString stringWithFormat:@"%@\r%@%@",session.title,PrintConstantDescription,session.sessionDescription];
                }
                else
                {
                    eventDescription = [NSString stringWithFormat:@"%@",session.title];
                }
                
                NSMutableAttributedString * attributedEventDescriptionString= [[NSMutableAttributedString alloc] initWithString:eventDescription];
                NSRange range = [eventDescription rangeOfString:eventDescription];
                [attributedEventDescriptionString addAttributes:tableDescriptionAtttributes range:range];
                NSRange range1 = [eventDescription rangeOfString:PrintConstantLocation];
                [attributedEventDescriptionString addAttributes:tableDescriptionBoldAtttributes range:range1];
                NSRange range2 = [eventDescription rangeOfString:PrintConstantDescription];
                [attributedEventDescriptionString addAttributes:tableDescriptionBoldAtttributes range:range2];
                
                
                
                // before we render any text to the PDF, we need to measure it, so we'll know where to render the
                // next line.
                
                CGRect eventDateSize = [eventDate boundingRectWithSize:CGSizeMake((tableDualMaxWidth * 0.5), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:tableDescriptionAtttributes context:nil];
                
                CGRect eventDescriptionSize = [attributedEventDescriptionString boundingRectWithSize:CGSizeMake((tableDualMaxWidth * 1.5), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                
                CGFloat maxVal = MAX(eventDateSize.size.height, eventDescriptionSize.size.height);
                
                if (maxVal + currentPageY > maxHeight) {
                    // create a new page and reset the current page's Y value
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, kDefaultPageWidth, kDefaultPageHeight), nil);
                    currentPageY = kMargin;
                    
                    //Adding header in all pages
                    [eventTitle drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleAttributes];
                    currentPageY = currentPageY + floor(eventTitleSize.size.height + 5);
                    
                    if(eventLocation.length > 0)
                    {
                        [eventLocation drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleSubheddingAtttributes];
                        currentPageY = currentPageY + floor(eventLocationSize.size.height + 5);
                    }
                    
                    [eventStartEndDate drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleSubheddingAtttributes];
                    currentPageY = currentPageY + floor(eventStartEndDateSize.size.height + eventTitleSize.size.height);
                    
                    [sessionDate drawInRect:CGRectMake(kMargin, currentPageY, tableSingleMaxWidth, MAXFLOAT) withAttributes:tableTitleAtttributes];
                    
                    [self drawTableAt:CGPointMake(xOrigin, currentPageY) withRowHeight:sessionDateSize.size.height andColumnWidth:tableSingleMaxWidth andRowCount:1 andColumnCount:1];
                    
                    currentPageY = currentPageY + floor(sessionDateSize.size.height);
                }
                
                // render the text
                [eventDate drawInRect:CGRectMake(kMargin, currentPageY, (tableDualMaxWidth * 0.5), maxHeight) withAttributes:tableDescriptionAtttributes];
                
                [self drawTableAt:CGPointMake(xOrigin, currentPageY) withRowHeight:maxVal andColumnWidth:(tableDualMaxWidth * 0.5) andRowCount:1 andColumnCount:1];
                
                [attributedEventDescriptionString drawInRect:CGRectMake(kMargin + (tableDualMaxWidth * 0.5) + kColumnMargin, currentPageY, (tableDualMaxWidth * 1.5), maxHeight)];
                
                [self drawTableAt:CGPointMake(xOrigin, currentPageY) withRowHeight:maxVal andColumnWidth:(tableDualMaxWidth * 2) andRowCount:1 andColumnCount:1];
                
                currentPageY = currentPageY + floor(maxVal);
            }
        }
    }
    
    // end and save the PDF.
    UIGraphicsEndPDFContext();
    return pdfData;
}

-(void)drawTableAt:(CGPoint)origin
     withRowHeight:(int)rowHeight
    andColumnWidth:(int)columnWidth
       andRowCount:(int)numberOfRows
    andColumnCount:(int)numberOfColumns
{
    for (int i = 0; i <= numberOfRows; i++) {
        int newOrigin = origin.y + (rowHeight*i);
        CGPoint from = CGPointMake(origin.x, newOrigin);
        CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
        [self drawLineFromPoint:from toPoint:to];
    }
    
    for (int i = 0; i <= numberOfColumns; i++) {
        int newOrigin = origin.x + (columnWidth*i);
        CGPoint from = CGPointMake(newOrigin, origin.y);
        CGPoint to = CGPointMake(newOrigin, origin.y +(numberOfRows*rowHeight));
        [self drawLineFromPoint:from toPoint:to];
    }
}

-(void)drawLineFromPoint:(CGPoint)from toPoint:(CGPoint)to
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.3);
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGFloat components[] = {0.0, 0.0, 0.0, 0.4};
    CGColorRef color = CGColorCreate(colorspace, components);
    
    CGContextSetStrokeColorWithColor(context, color);
    CGContextMoveToPoint(context, from.x, from.y);
    CGContextAddLineToPoint(context, to.x, to.y);
    
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
}

-(NSMutableArray*) getSessionsDictionary
{
    NSMutableArray *arrayToReturn = [NSMutableArray array];
    
    NSDictionary *sessions = self.dictionaryToPrint[@"sessions"];
    
    if  (sessions != nil && [sessions isKindOfClass:[NSDictionary class]] && sessions.count > 0)
    {
        NSArray *allSessionIds = [sessions allKeys];
        for (NSString *sessionId in allSessionIds)
        {
            NSDictionary *singleSession = sessions[sessionId];
            if  (singleSession != nil &&
                 [singleSession isKindOfClass:[NSDictionary class]] &&
                 singleSession.count > 0)
            {
                NSArray *durations = singleSession[@"duration"];
                if (durations != nil &&
                    [durations isKindOfClass:[NSArray class]] &&
                    durations.count > 0)
                {
                    for (NSDictionary *duration in durations)
                    {
                        NSString *dateToMatch = duration[@"session_date"];
                        
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"yyyy-MM-dd"];
                        NSString *dateToCompare = [dateFormat stringFromDate:self.weekCalendarView.selectedDate];
                        
                        if ([dateToMatch isEqualToString:dateToCompare])
                        {
                            [arrayToReturn addObject:singleSession];
                        }
                    }
                }
            }
        }
    }
    
    return arrayToReturn;
}

-(void)pdfDocument:(FastPDFViewController*)pdfDocumentView didTapLastViewedForAsset:(NSObject*)asset
{
    [self checkAndOpenAsset:asset];
}

-(void)assetViewController:(ZMAssetViewController*)assetVCView didTapLastViewedButton:(UIButton*)sender forAsset:(NSObject*)asset
{
    [self checkAndOpenAsset:asset];
}

-(void) checkAndOpenAsset:(NSObject*)asset
{
    if([asset isKindOfClass:[Asset class]])
    {
        Asset *assetVal = (Asset*)asset;
        if([assetVal.type isEqualToString:[EventOnlyAsset TypeAttributeValueAsString]])
        {
            EventOnlyAsset* eoaAsset = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:[assetVal.assetID stringValue]];
            [self eventOnlyAssetHandler:nil openAsset:eoaAsset];
        }
        else
        {
            [self eventAssetHandler:nil openAsset:assetVal];
        }
    }
}

#pragma mark --
#pragma mark EventsSessionsView Delegate Methods

- (void)sessionsView:(SessionsView*)sessionView didSelectCell:(SessionsCell*)cell withAssetView:(AssetView*)assetViewVal
{
    NSString *type       = @"";
    NSString *assetId    = @"";
    NSString *eventId    = self.event.eventId;
    NSString *sessionId  = cell.eventDuration.session.sessionId;
    BOOL isAlreadyViewedAsset = NO;
    
    if ([assetViewVal.associatedAsset isKindOfClass:[Asset class]])
    {
        if (self.eventAssetHandler == nil)
        {
            self.eventAssetHandler = [[EventAssetHandler alloc]init];
            self.eventAssetHandler.delegate      = (id <EventAssetHandlerDelegate>)self;
        }
        self.eventAssetHandler.userFranchise = self.headerViewEventsDetail.userFranchise.text;
        
        //Setting delay for save view will not collide with download asset
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.eventAssetHandler validateAndShowAsset:(Asset*)assetViewVal.associatedAsset];
            
        });
        
        //related to saveViewed
        assetId = [((Asset*)assetViewVal.associatedAsset).assetID stringValue];
        type    = keyAssetString;
        isAlreadyViewedAsset = ((Asset*)assetViewVal.associatedAsset).isViewed.boolValue;
    }
    else
    {
        if (self.eventOnlyAssetHandler == nil)
        {
            self.eventOnlyAssetHandler = [[EventOnlyAssetHandler alloc]init];
            self.eventOnlyAssetHandler.delegate      = (id <EventOnlyAssetHandlerDelegate>)self;
        }
        self.eventOnlyAssetHandler.userFranchise = self.headerViewEventsDetail.userFranchise.text;
        
        //Setting delay for save view will not collide with download asset
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.eventOnlyAssetHandler validateAndShowEventOnlyAsset:(EventOnlyAsset*)assetViewVal.associatedAsset];
        });

        
        //related to saveViewed
        assetId = ((EventOnlyAsset*)assetViewVal.associatedAsset).eoa_id;
        type    = [EventOnlyAsset TypeAttributeValueAsString];
        isAlreadyViewedAsset = ((EventOnlyAsset*)assetViewVal.associatedAsset).isViewed.boolValue;
    }
    
    if (isAlreadyViewedAsset == NO)
    {
        // saveViewed for assets
            [[EventsServiceClass sharedAPIManager] saveViewed:eventId session:sessionId asset:assetId type:type complete:^(NSDictionary *savedAssetDetails){
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"savedAssetDetails:%@",savedAssetDetails]];
        } error:^(NSError *error) {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"error:%@",error]];
        }];
    }
}

#pragma mark - EventAssetHandlerDelegate

- (void)eventAssetHandler:(EventAssetHandler*)eventAssetHandler pushViewController:(UIViewController*)viewController
{
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler assetDownloadStatus:(AssetDownloadStatus)status
{
    if (status == AssetDownloadStatusStarted)
    {
        self.isAssetDownloadInProgress = YES;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset download in progress = YES"]];
    }
    else
    {
        self.isAssetDownloadInProgress = NO;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset download in progress = NO"]];
    }
}


- (void)eventAssetHandler:(EventAssetHandler*)eventAssetHandler assetDownloadStatus:(AssetDownloadStatus)status
{
    if (status == AssetDownloadStatusStarted)
    {
        self.isAssetDownloadInProgress = YES;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset download in progress = YES"]];
    }
    else
    {
        self.isAssetDownloadInProgress = NO;
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset download in progress = NO"]];
    }
}

- (void)eventAssetHandler:(EventAssetHandler*)eventAssetHandler setProgress:(CGFloat)progress
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Progress: %f",progress]];
}

- (UIViewController*) getPresentViewControllerForEventAssetHandler:(EventAssetHandler*)eventAssetHandler
{
    return self;
}

- (void)eventAssetHandler:(EventAssetHandler*)eventAssetHandler openAsset:(Asset*)selectedAsset
{
    if(self.headerViewEventsDetail.categoryPopOver)
    {
         [[self.headerViewEventsDetail.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    if(self.headerViewEventsDetail.sortMenuPopOver)
    {
        [[self.headerViewEventsDetail.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.eventAssetHandler.mainDelegate hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
    
    self.eventAssetHandler.isDownloadingAsset = NO;
 
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController <ZMAssetViewControllerDelegate> *controller;
    
    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
    if (assetType == ZMProntoFileTypesPDF) {
        if(document != nil)
        {
            [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.eventAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                self.eventAssetHandler.currentSelectedAssetToSeeDetail = nil;
            });
        }
    }
    else if (assetType == ZMProntoFileTypesBrightcove)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
        controller.calendarDetailVC = self;
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.asset = selectedAsset;
        controller.videoId = selectedAsset.uri_full;
        controller.userFranchise = self.headerViewEventsDetail.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (assetType == ZMProntoFileTypesDocument)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.calendarDetailVC = self;
        controller.asset = selectedAsset;
        controller.userFranchise = self.headerViewEventsDetail.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    else if(assetType == ZMProntoFileTypesWeblink)
    {
        // open the weblink URL
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.calendarDetailVC = self;
        controller.asset = selectedAsset;
        controller.userFranchise = self.headerViewEventsDetail.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if(assetType == ZMProntoFileTypesAudio)
    {

    } else
    {
        if (assetType == ZMProntoFileTypesPDF)
        {
            if(selectedAsset.path)
            {                
                MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
                if(document != nil)
                {
                    [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
                }
                else
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.eventAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                        self.eventAssetHandler.currentSelectedAssetToSeeDetail = nil;
                    });
                }
            }
        }
    }
    
    if (assetType != ZMProntoFileTypesPDF)
    {
        ((ZMAssetViewController*)controller).delegate = (id<ZMAssetViewControllerDelegate>)self;
    }

}

- (void)openFastPDFWithAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    controller.calendarDetailVC = self;
    controller.asset = asset;
    
    
    ((FastPDFViewController*)controller).fastPDFDocumentDelegate = (id<ZMAssetViewControllerDelegate>)self;
    
    ((FastPDFViewController*)controller).isEventOnlyAsset = YES;
    
  UIViewController *viewController = [[UIViewController alloc] init];
  viewController.view.frame = CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, viewController.view.frame.size.height);
  [viewController addChildViewController:controller];
  
  UIView *view = [[UIView alloc] init];
  view.frame = viewController.view.bounds;
  view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, view.frame.size.height);
  
  [viewController.view addSubview:view];
  viewController.view.backgroundColor = [RedesignThemesClass sharedInstance].defaultThemeforUser;
  
  controller.view.frame = viewController.view.bounds;
  [viewController.view addSubview:controller.view];
  
  CGRect frame = controller.view.frame;
  frame.origin.y = 104;
  frame.size.height = frame.size.height - frame.origin.y;
  controller.view.frame = frame;
  
  CGRect frame1 = view.frame;
  frame1.origin.y = 0;
  frame1.size.height = 104;
  view.frame = frame1;
  
  UIView *webview = [[UIView alloc] init];
  webview.frame = viewController.view.bounds;
  webview.backgroundColor = [UIColor whiteColor];
  UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]; [webview addSubview:spinner];
  spinner.center = webview.center;
  spinner.color = [UIColor grayColor];
  [viewController.view addSubview:webview];
  [spinner startAnimating];
  UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(viewController.view.center.x - 48, viewController.view.center.y + 24, UIScreen.mainScreen.bounds.size.width, 24)];
  labelView.backgroundColor = [UIColor whiteColor];
  labelView.text = @"Please wait...";
  [labelView setFont:[UIFont boldSystemFontOfSize:16.0]];
  labelView.textColor = [UIColor blackColor];
  [viewController.view addSubview:labelView];

  //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
  [[RedesignErrorHandling sharedInstance] hideMessage];
  //    controller.userFranchise = headerView.userFranchise.text;
  
  controller.userFranchise = self.headerViewEventsDetail.userFranchise.text;
  controller.viewMain = view;
  controller.viewMain.frame = CGRectMake(controller.viewMain.frame.origin.x, controller.viewMain.frame.origin.y, UIScreen.mainScreen.bounds.size.width, controller.viewMain.frame.size.height);
  controller.webviewScreen = webview;
  controller.webviewScreen.frame = CGRectMake(controller.webviewScreen.frame.origin.x, controller.webviewScreen.frame.origin.y, UIScreen.mainScreen.bounds.size.width, controller.webviewScreen.frame.size.height);
  controller.webviewScreen.hidden = NO;
  
  double delayInSeconds = 2.5; // set the time
  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    controller.webviewScreen.hidden = YES;
    [spinner stopAnimating];
    [labelView removeFromSuperview];
  });
  
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)openAudioAsset:(Asset *)selectedAsset{
    
    [ProntoRedesignSingletonClass sharedInstance].audioAssetView = nil;
    [ProntoRedesignSingletonClass sharedInstance].audioAsset = nil;
    
    //If Selected path or Selected path is not present then asset can't be opened.
    if (selectedAsset == nil || selectedAsset.path == nil) { return ;}
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RedesignErrorHandling sharedInstance] hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
    
    _isDownloadignAsset = NO;
        
    if(assetType == ZMProntoFileTypesAudio){
        [self stopAudio];
        assetForAudio = selectedAsset;
        [AudioPlayer.sharedInstance setAssetWithAssetVal:selectedAsset];
        if(!audioPlayerView)
        {
            audioPlayerView = (AudioPlayer*)[[[NSBundle mainBundle] loadNibNamed:@"AudioPlayerView" owner:self options:nil] firstObject];
        }
        [audioPlayerView initialSetupWithAssetVal:selectedAsset];
        audioPlayerView.frame = CGRectMake(0, 30, self.view.frame.size.width, self.view.frame.size.height - 30);
        [self.view addSubview: audioPlayerView];
        self.currentSelectedAssetToSeeDetail = nil;
    }

}

- (void)stopAudio
{
    if(assetForAudio != nil)
    {
        [audioPlayerView stopAudio];
        assetForAudio = nil;
    }
}

#pragma mark - EventOnlyAssetHandlerDelegate starts
- (void)eventOnlyAssetHandler:(EventAssetHandler*)eventAssetHandler pushViewController:(UIViewController*)viewController
{
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)eventOnlyAssetHandler:(EventAssetHandler*)eventAssetHandler setProgress:(CGFloat)progress
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Progress: %f",progress]];
}

- (UIViewController*) getPresentViewControllerForEventOnlyAssetHandler:(EventAssetHandler*)eventAssetHandler
{
    return self;
}

- (void)eventOnlyAssetHandler:(EventAssetHandler*)eventAssetHandler openAsset:(EventOnlyAsset*)selectedAsset
{
    //Asset Details accepts only Asset object and it is very tighly coupled with controller.
    //So, Instead of creating new asset detail controller, we are passing equivalent object of eventOnlyAsset.
    Asset *equivalentAssetForEventOnlyAsset = [selectedAsset getEquivalentAssetObject];
    
    if(self.headerViewEventsDetail.categoryPopOver)
    {
        [[self.headerViewEventsDetail.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    if(self.headerViewEventsDetail.sortMenuPopOver)
    {
        [[self.headerViewEventsDetail.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.eoa_path]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.eventOnlyAssetHandler.mainDelegate hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [selectedAsset getFileTypeOfEventOnlyAsset];
    
    self.eventAssetHandler.isDownloadingAsset = NO;
    
 
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController <ZMAssetViewControllerDelegate> *controller;
    
    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
    if (assetType == ZMProntoFileTypesPDF)
    {
        if(document != nil)
        {
            [self openFastPDFWithAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.eventAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
            });
        }
    }
    else if (assetType == ZMProntoFileTypesBrightcove)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
        controller.calendarDetailVC = self;
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.asset = equivalentAssetForEventOnlyAsset;
        controller.userFranchise = self.headerViewEventsDetail.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (assetType == ZMProntoFileTypesDocument)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.calendarDetailVC = self;
        controller.asset = equivalentAssetForEventOnlyAsset;
        controller.userFranchise = self.headerViewEventsDetail.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    else if(assetType == ZMProntoFileTypesWeblink)
    {
        // open the weblink URL
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.calendarDetailVC = self;
        controller.asset = equivalentAssetForEventOnlyAsset;
        controller.userFranchise = self.headerViewEventsDetail.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else
    {
        if (assetType == ZMProntoFileTypesPDF)
        {
            if(selectedAsset.eoa_path)
            {
                MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
                if(document != nil)
                {
                    [self openFastPDFWithAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
                }
                else
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.eventAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                        self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
                    });
                }
            }
        }
    }
    
    if (assetType != ZMProntoFileTypesPDF)
    {
        ((ZMAssetViewController*)controller).delegate = (id<ZMAssetViewControllerDelegate>)self;
    }
    
}

#pragma mark - EventOnlyAssetHandlerDelegate Ends

- (UIAlertController*)getAlertControllerWithMessage:(NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    return alertController;
}


#pragma mark - GSViewControllerDelegate Method
- (void)didRemovedFromSuperView
{
    //NSLog(@"Global Search Removed from Superview");
    [self addSelfToListenForNotificationCenter];
    [[ZMProntoManager sharedInstance].search setTitle:@""];
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
    self.headerViewEventsDetail.delegate = (id<headerActions>)self;
    [ZMUserActions sharedInstance].globalSearchViewController = nil;

    [[EventCoreDataManager sharedInstance] resetEventSearchedResults];
    [[EventCoreDataManager sharedInstance] resetEventOnlyAssets];
    [[EventCoreDataManager sharedInstance] resetAssets];
}

- (void)removeSelfFromListeningToNotificationCenter
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Refresh AssetV2 Service
- (void)refreshLocalDBFromAssetV2:(BOOL)refresh
{
    [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingAssets];
    
    [[ZMAbbvieAPI sharedAPIManager] syncAssets:^{
        //if showing "Loading Assets" in header
        ((ZMAbbvieAPI*)[ZMAbbvieAPI sharedAPIManager])._syncingCompletion = YES;
        [self.headerViewEventsDetail enableSearchIconWithDefaultValues];
        
        [self refreshCalendarDetail:self.weekCalendarView.selectedDate]; //Verify the selected date
        [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] pauseOfflineDownload:NO];
        [[AssetDownloadProvider sharedAssetDownloadProvider] pauseOfflineDownload:NO];
        
        if (self.isAssetDownloadInProgress == YES)
        {
            AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [mainDelegate showMessage:ProntoMessageDownloadingAsset whithType:ZMProntoMessagesTypeSuccess];
        }
        else
        {
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingAssets];
        }
    } progress:^(float progressValue) {
        // Not logging progress
    } error:^(NSError *error) {
        [[EventAssetDownloadProvider sharedEventAssetDownloadProvider] pauseOfflineDownload:NO];
        [[AssetDownloadProvider sharedAssetDownloadProvider] pauseOfflineDownload:NO];
        
        if (self.isAssetDownloadInProgress == YES)
        {
            AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [mainDelegate showMessage:ProntoMessageDownloadingAsset whithType:ZMProntoMessagesTypeSuccess];
        }
        else
        {
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageDownloadingAsset];
        }
    }];
}

@end
