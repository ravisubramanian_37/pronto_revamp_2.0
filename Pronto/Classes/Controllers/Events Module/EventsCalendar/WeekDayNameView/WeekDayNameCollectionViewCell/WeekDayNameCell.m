//
//  WeekDayNameCell.m
//  Pronto
//
//  Created by m-666346 on 09/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "WeekDayNameCell.h"
#import "UIColor+ColorDirectory.h"
#import "UIFont+FontDirectory.h"
#import "NSDate+DateDirectory.h"

@interface WeekDayNameCell()
@end


@implementation WeekDayNameCell

+ (NSString*)cellIdentifier
{
    return @"WeekDayNameCell";
}

+ (NSString*)nibName
{
    return @"WeekDayNameCell";
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
    [self markDayNameLabelAsCurrentDate:NO];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self setupUI];
    [self markDayNameLabelAsCurrentDate:NO];
}

- (void)createBorderAroundCellView
{
    self.layer.borderWidth = 0.75;
    self.layer.borderColor = [[UIColor GetColorFromHexValueEEF3F7] CGColor];
    self.clipsToBounds = YES;
}

- (void)setupUI
{
    self.backgroundColor = [UIColor clearColor];
    self.dayNameLabel.font  = [UIFont FontLatoRegularSize14];
    self.dayNameLabel.textColor = [UIColor GetColorFromHexValue2d2926];
}

- (void)markDayNameLabelAsCurrentDate:(BOOL)mark
{
    if (mark == YES)
    {
        self.dayNameLabel.textColor = [UIColor GetColorFromHexValue1AC9A8];
    }
    else
    {
        self.dayNameLabel.textColor = [UIColor GetColorFromHexValue2d2926];
    }
}

@end
