//
//  WeekDayNameCell.h
//  Pronto
//
//  Created by m-666346 on 09/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeekDayNameCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *dayNameLabel;
@property (nonatomic, weak) IBOutlet UIView  *weekDayNameContainerView;

+ (NSString*)cellIdentifier;
+ (NSString*)nibName;
- (void)markDayNameLabelAsCurrentDate:(BOOL)mark;

@end
