//
//  WeekDayNameView.m
//  Pronto
//
//  Created by m-666346 on 09/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "WeekDayNameView.h"
#import "WeekDayNameCell.h"
#import "ViewUtils.h"
#import "NSDate+DateDirectory.h"



@interface WeekDayNameView()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray *weekDayNames;
@property (nonatomic, strong) NSString *weekDayName;


@end

@implementation WeekDayNameView

+ (WeekDayNameView*)GetWeekDayNameView
{
    return (WeekDayNameView*)[[[NSBundle mainBundle] loadNibNamed:[WeekDayNameView NibName]
                                                            owner:self
                                                          options:nil] lastObject];
}

+ (NSString*)NibName
{
    return @"WeekDayNameView";
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initializeDatasource];
    [self setupUI];
}

- (void)reloadData
{
    [self.weekDayNameCollectionView reloadData];
}

- (void)initializeDatasource
{
    self.weekDayName = [NSDate GetWeekdayForDate:[NSDate date]];
    self.weekDayNames = @[@"SUN",@"MON",@"TUE",@"WED",@"THU",@"FRI",@"SAT"];
}


- (void)setupUI
{
    self.backgroundColor = [UIColor whiteColor];
    
    [self.weekDayNameCollectionView registerNib:[UINib nibWithNibName:[WeekDayNameCell nibName] bundle:nil]
                     forCellWithReuseIdentifier: [WeekDayNameCell cellIdentifier]];
}

#pragma mark - Collection View Data Soure Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  self.weekDayNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WeekDayNameCell *cell = (WeekDayNameCell*)[self.weekDayNameCollectionView dequeueReusableCellWithReuseIdentifier:[WeekDayNameCell cellIdentifier]
                                                                                                        forIndexPath:indexPath];
    
    if (self.weekDayNames != nil &&
        indexPath.row <= self.weekDayNames.count)
    {
        NSString *weekDayName = self.weekDayNames[indexPath.row];
        cell.dayNameLabel.text = weekDayName;
        
        /*
        Uncomment this code if you need to highlight current date in view.
        if ([[self.weekDayName uppercaseString] isEqualToString:weekDayName])
        {
            [cell markDayNameLabelAsCurrentDate:YES];
        }
        else
        {
            [cell markDayNameLabelAsCurrentDate:NO];
        }
        */
    }

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.weekDayNameCollectionView.width / 7.0;
    return CGSizeMake(width, 40);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

@end
