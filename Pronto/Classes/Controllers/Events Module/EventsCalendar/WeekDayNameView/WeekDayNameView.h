//
//  WeekDayNameView.h
//  Pronto
//
//  Created by m-666346 on 09/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

// It will contain collection view to show Day Name Sun to Sat

@interface WeekDayNameView : UIView

@property (nonatomic, weak) IBOutlet UICollectionView *weekDayNameCollectionView;

+ (WeekDayNameView*)GetWeekDayNameView;
- (void)reloadData;
@end
