//
//  EventsCalendarDetailsScreen.m
//  Pronto
//
//  Created by m-666346 on 09/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventsCalendarDetailsScreenVC.h"
#import "EventListViewController.h"
//#import "ZMHeader.h"
#import "ZMUserActions.h"
#import "MonthsCalendarViewController.h"
#import "WeekDayNameView.h"
#import "EventsCalendarHeaderView.h"
#import "ViewUtils.h"
#import "UIColor+ColorDirectory.h"
//#import "EventsFakeTabbarView.h"
#import "AppDelegate.h"
//#import "ProntoTabbarViewController.h"
#import "UIFont+FontDirectory.h"
#import "Pronto-Swift.h"
#import "CalendarDetailVC.h"
#import "HelpViewController.h"
#import "ZMNotifications.h"
#import "UIImage+ImageDirectory.h"
#import "FeedbackView.h"
#import "NSDate+DateDirectory.h"
#import "AppUtilities.h"

#import "EventCoreDataManager.h"
#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"
#import "EventAssetDownloadProvider.h"
#import "ProntoOverlayView.h"

//#import "GlobalSearchVC.h"
#import "AppUtilities.h"
#import "EventsGlobalSearchViewModel.h"
#import "Filter.h"
//#import "ZMSearchMenu.h"
#import "EventCoreDataManager.h"
//#import "ZMLibrary.h"
#import "ProntoUserDefaults.h"

typedef void(^OperationStatusCompletion)(BOOL success);


@interface EventsCalendarDetailsScreenVC ()<MonthsCalendarDelegate>{
    HomeMainHeaderView *mainHeaderView;
    
}
@property (nonatomic, weak) IBOutlet UIView *header;
@property (nonatomic, weak) IBOutlet UIView *calendarHeaderView;
@property (nonatomic, weak) IBOutlet UIView *calendarContainerView;
@property (nonatomic, weak) IBOutlet UIView *calendarView;
@property (nonatomic, weak) IBOutlet UIView *weekDayNameContainerView;

//@property (strong, nonatomic) ZMHeader *headerViewEventsDetail;
@property (nonatomic, strong) id <ZMProntoTheme> theme;
@property (nonatomic, strong) MonthsCalendarViewController *monthCalendarViewController;
@property (nonatomic, strong) WeekDayNameView *weekDayNameView;
@property (nonatomic, strong) EventsCalendarHeaderView *eventsCalendarHeaderView;
//@property (nonatomic, strong) EventsFakeTabbarView *eventsFakeTabbarView;

@property (nonatomic, strong)  ZMNotifications *notificationsPannel;
@property (nonatomic, strong)  FeedbackView *feedbackView;
@property (nonatomic, strong)  UIView *backgroundView;
@property (nonatomic, assign, getter=isNetworkRequestInProgress) BOOL networkRequestInProgress;
@property (nonatomic, strong)  ProntoOverlayView *prontoOverlayView;

@property (nonatomic, weak) ZMAbbvieAPI *apiManager;

// Global Search View
@property (nonatomic, strong) GlobalSearchVC *globalSearcVC;

@end

@implementation EventsCalendarDetailsScreenVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
    [self loadEventsCalendarHeaderView];
    //Tabbar reset
//    self.headerViewEventsDetail.tabBarTag = -1;
    [self loadWeekDayNameView];
    [self loadMonthsCalendarView];
    // Have place holder view. as other's
//    [self loadEventsFakeTabbarView];
    //SelectedTab reset
//    [ZMUserActions sharedInstance].currentSelectedTabIndex = -1;
    [self setupViewsFrames];
    
    self.eventsGSViewModel = [[EventsGlobalSearchViewModel alloc]initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)self];
    //TODO:check for redesign implementation
    [self makeEventsDetailRequestAndUpdateCalendarView];
}

- (void)loadEventsCalendarHeaderView
{
    if (self.eventsCalendarHeaderView == nil)
    {
        CGRect frame = self.calendarHeaderView.bounds;
        self.eventsCalendarHeaderView = [EventsCalendarHeaderView GetEventsCalendarHeaderViewWithFrame:frame];
        self.eventsCalendarHeaderView.delegate = (id <EventsCalendarHeaderViewDelegate>)self;
    }
    [self.calendarHeaderView addSubview:self.eventsCalendarHeaderView];
    
    [self.eventsCalendarHeaderView prepareHeaderViewWithData:self.selectedEvent];
}

- (void)loadWeekDayNameView
{
    if (self.weekDayNameView == nil)
    {
        self.weekDayNameView = [WeekDayNameView GetWeekDayNameView];
        self.weekDayNameView.frame = self.weekDayNameContainerView.bounds;
    }
    
    [self.weekDayNameContainerView addSubview:self.weekDayNameView];
    [self.weekDayNameView reloadData];
}

- (void)loadMonthsCalendarView
{
    if (self.monthCalendarViewController == nil)
    {
        CGRect frame        = self.calendarView.bounds;
        frame.origin.y      = self.weekDayNameContainerView.height;
        frame.size.height   = self.calendarView.height - ( self.weekDayNameContainerView.height) + 50; // make it constant
//        frame.size.height   = self.calendarView.height - self.weekDayNameContainerView.height;
        self.monthCalendarViewController = [[MonthsCalendarViewController alloc]initWithFrame:frame
                                                                                     andEvent:self.selectedEvent];
        self.monthCalendarViewController.delegate = self;
    }
    
    //Frame change is not required. As it is being loaded to placeholder view.
    [self addChildViewController:self.monthCalendarViewController];
    [self.calendarView addSubview:self.monthCalendarViewController.view];
    [self.monthCalendarViewController didMoveToParentViewController:self];
}



- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addSelfToListenForNotificationCenter];

    [self setupViewsFrames];
    [self.monthCalendarViewController.calendarView reloadDataWithCompletionHandler:nil];

    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.monthCalendarViewController makeCellsVisibleContainingSession];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeNotification:UIApplicationWillEnterForegroundNotification];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.notificationsPannel adjustNotifications];
    [self.notificationsPannel changeOfOrientation];
    [self.feedbackView adjustFrame:self.view];
}

//Capture the rotation of screens
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{

    [self.weekDayNameView reloadData];

}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

    [mainHeaderView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];

}

- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

    [self.prontoOverlayView adjustProntoOverlay];
}

+(EventsCalendarDetailsScreenVC*)GetEventsCalendarDetailsScreen
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"EventsDetailsScene" bundle:nil];
    EventsCalendarDetailsScreenVC *eventsCalendarDetailScreenVC = [sb instantiateViewControllerWithIdentifier:@"EventsCalendarDetailsScreenVC"];
    return eventsCalendarDetailScreenVC;
}

- (void)setupUI
{
    self.calendarContainerView.backgroundColor = [UIColor GetColorFromHexValuef2f2f2];
    [self setupHeader];
}

- (void)setupHeader
{

    //Header View
    mainHeaderView = [[[NSBundle mainBundle] loadNibNamed:RedesignConstants.mainViewHeaderNib owner:nil options:nil] objectAtIndex:0];
    [self.header addSubview:mainHeaderView];
    [mainHeaderView setThemes];
    self.view.backgroundColor = [RedesignThemesClass sharedInstance].defaultThemeforUser;

    mainHeaderView.frame = CGRectMake(0, 0, self.view.frame.size.width, 60);

}


- (void)setupViewsFrames
{
    //Week day view update
    self.weekDayNameView.frame = self.weekDayNameContainerView.bounds;
    [self.weekDayNameView reloadData];
}

- (void)makeEventDetailServiceRequestWithCMS:(OperationStatusCompletion)completionHandler
{
    if (self.isNetworkRequestInProgress == YES) { return; }
    
    self.networkRequestInProgress = YES;
    [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingEventDetails];
   
        [[EventsServiceClass sharedAPIManager] getEventDetails:self.selectedEvent.eventId complete:^(NSDictionary *sessionDetails) {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"sessionsFetched%@:",sessionDetails]];
        
        if (sessionDetails != nil &&
            [sessionDetails isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *sessionDetail = [[NSDictionary alloc] initWithObjectsAndKeys:sessionDetails, self.selectedEvent.eventId, nil];

            if (sessionDetail != nil && [sessionDetail isKindOfClass:[NSDictionary class]] && sessionDetail.count <=0 )
            {
                //It means that event has been deleted from CMS
                //go back to landing screen with updated list
                
                [[EventCoreDataManager sharedInstance] removeEvent:self.selectedEvent withCompletionHandler:^(BOOL success) {
                    
                    if (success == YES)
                    {
                        self.selectedEvent = nil;
                        completionHandler(NO);
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                                 message:EventHasBeenDeletedMessage
                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAlertAction = [UIAlertAction actionWithTitle:AlertOKTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:kEventDeletedFromCMSNotification object:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                
                                [self.navigationController popToRootViewControllerAnimated:YES];
                                
                            });
                        }];
                        [alertController addAction:okAlertAction];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }];
            }
            else
            {
                [[EventCoreDataManager sharedInstance] saveEventDetail:sessionDetail withCompletionHandler:^(BOOL success, Event *event) {
                    
                    if (success == YES)
                    {
                        if (completionHandler != nil)
                        {
                            completionHandler(YES);
                        }
                    }
                    else
                    {
                        completionHandler(NO);
                    }
                }];
            }
            

        }
        self.networkRequestInProgress = NO;
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];

    } error:^(NSError *error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"getSessions error:%@", error]];
        self.networkRequestInProgress = NO;
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
        completionHandler(NO);
    }];
}

#pragma mark --
#pragma mark EventsCalendarHeaderView Delegate Methods

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapPrintButton:(UIButton*)sener
{
    [self populateData];
}

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapBackButton:(UIButton*)sener
{
    [ZMUserActions sharedInstance].currentSelectedTabIndex = 3;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapRefreshButton:(UIButton*)sender
{
    
    [self makeEventsDetailRequestAndUpdateCalendarView];
}

- (void)makeEventsDetailRequestAndUpdateCalendarView
{
    __weak EventsCalendarDetailsScreenVC *weakSelf = self;
    
    [self makeEventDetailServiceRequestWithCMS:^(BOOL success) {
        
        if (success == YES)
        {
            [weakSelf.monthCalendarViewController.calendarView reloadDataWithCompletionHandler:nil];
            // Refreshing Header view.
            [weakSelf.eventsCalendarHeaderView prepareHeaderViewWithData:self.selectedEvent];
        }
    }];
}

- (void)updateLocalDatabaseForEventDetails
{
    [self eventCalendarHeaderView:nil didTapRefreshButton:nil];
}


#pragma mark --
#pragma mark Print Operation Methods
//Populating text data
-(void) populateData
{
    [self showLoadingIndicator:YES];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self printPDF:[self constructPrintData] assetTitle:@"Print Data"];
    });
}

-(void) showLoadingIndicator:(BOOL)show
{
    if(show == YES)
    {
        self.prontoOverlayView = [ProntoOverlayView PrepareProntoOverlayWithView:self.view];
        [self.prontoOverlayView showProntoOverlayViewWithMessage:ProntoMessagePreparingDataToPrint];
    }
    else
    {
        [self.prontoOverlayView removeProntoOverlay];
    }
}

- (NSMutableData*)constructPrintData
{
    NSDictionary *data = [[AppUtilities sharedUtilities] renderPrintDictionaryForEvent:self.selectedEvent];
    
    NSInteger kMargin = 20;
    NSInteger kDefaultPageWidth = [UIScreen mainScreen].bounds.size.width;
    NSInteger kDefaultPageHeight = [UIScreen mainScreen].bounds.size.height;
    NSInteger kColumnMargin = 2;
    
    // Create the PDF context using the default page size.
    // This default is spelled out in the iOS documentation for UIGraphicsBeginPDFContextToFile
    CGRect rct = {{0.0 , 0.0 } , {kDefaultPageWidth , kDefaultPageHeight}};
    NSMutableData *pdfData = [[NSMutableData alloc]init];
    UIGraphicsBeginPDFContextToData(pdfData, rct, nil);
//    UIGraphicsBeginPDFContextToFile(pdfFilePath, CGRectZero, nil);
    
    // maximum height and width of the content on the page, byt taking margins into account.
    CGFloat maxWidth = kDefaultPageWidth - kMargin * 2;
    CGFloat maxHeight = kDefaultPageHeight - kMargin * 2;
    
    // Max Width of table
    CGFloat tableSingleMaxWidth = maxWidth;
    
    CGFloat tableDualMaxWidth = maxWidth / 2;
    
    // create the fonts and set its attributes
    UIFont* eventTitleFont = [UIFont FontLatoRegularSize16];
    
    UIFont* eventTitleSubheddingFont = [UIFont FontLatoRegularSize14];
    
    UIFont* tableTitleFont = [UIFont FontLatoBoldSize14];
    
    UIFont* tableDescriptionBoldFont = [UIFont FontLatoBoldSize10];
    
    UIFont* tableDescriptionFont = [UIFont FontLatoRegularSize10];
    
    NSMutableParagraphStyle *textStyleCenter = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyleCenter.lineBreakMode = NSLineBreakByWordWrapping;
    textStyleCenter.alignment = NSTextAlignmentCenter;
    
    NSMutableParagraphStyle *textStyleLeft = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyleLeft.lineBreakMode = NSLineBreakByWordWrapping;
    textStyleLeft.alignment = NSTextAlignmentLeft;
    
    NSDictionary *eventTitleAttributes = @{NSFontAttributeName:eventTitleFont,NSParagraphStyleAttributeName:textStyleCenter};
    NSDictionary *eventTitleSubheddingAtttributes = @{NSFontAttributeName:eventTitleSubheddingFont,NSParagraphStyleAttributeName:textStyleCenter};
    NSDictionary *tableTitleAtttributes = @{NSFontAttributeName:tableTitleFont,NSParagraphStyleAttributeName:textStyleCenter};
    
    NSDictionary *tableDescriptionBoldAtttributes = @{NSFontAttributeName:tableDescriptionBoldFont,NSParagraphStyleAttributeName:textStyleLeft};
    NSDictionary *tableDescriptionAtttributes = @{NSFontAttributeName:tableDescriptionFont,NSParagraphStyleAttributeName:textStyleLeft};
    
    CGFloat currentPageY = 0;
    
    //Initialize PDF and set initial Margin
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, kDefaultPageWidth, kDefaultPageHeight), nil);
    currentPageY = kMargin;
    NSInteger xOrigin = (NSInteger)kMargin;
    
    // draw the Event title name at the top of the page.
    NSString* eventTitle = [data objectForKey:@"EventTitle"];
    
    CGRect eventTitleSize = [eventTitle boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:eventTitleAttributes context:nil];
    
    [eventTitle drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleAttributes];
    
    currentPageY = currentPageY + floor(eventTitleSize.size.height + 5);
    
    // draw the Event sub hedding title name at the top of the page.
    NSString* eventLocation = [data objectForKey:@"EventLocation"];
    
    CGRect eventLocationSize = [eventLocation boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:eventTitleSubheddingAtttributes context:nil];
    
    if(eventLocation.length > 0)
    {
        [eventLocation drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleSubheddingAtttributes];
        
        currentPageY = currentPageY + floor(eventLocationSize.size.height + 5);
    }
    
    NSDate* startDate = [NSDate GetDateSameAsStringFromDateString:[data objectForKey:@"EventStartDate"]];
    NSDate* endDate = [NSDate GetDateSameAsStringFromDateString:[data objectForKey:@"EventEndDate"]];
    
    NSString* eventStartEndDate = [NSString stringWithFormat:@"%@ to %@",
                               [NSDate getFormattedDateStringForGlobalSearchFromDate:startDate],[NSDate getFormattedDateStringForGlobalSearchFromDate:endDate]];
    
    CGRect eventStartEndDateSize = [eventStartEndDate boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:eventTitleSubheddingAtttributes context:nil];
    
    [eventStartEndDate drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleSubheddingAtttributes];
    
    currentPageY = currentPageY + floor(eventStartEndDateSize.size.height + eventTitleSize.size.height);
    
    NSString* timezone = [data objectForKey:@"EventTimeZone"];
    
    NSArray* durationDates = [data objectForKey:@"DurationDates"];
    
    NSArray* durations = [data objectForKey:@"Durations"];
    
    for (NSDate* date in durationDates)
    {
        NSString* sessionDate = [NSDate getFormattedDateStringForGlobalSearchFromDate:date];
        [sessionDate drawInRect:CGRectMake(kMargin, currentPageY, tableSingleMaxWidth, MAXFLOAT) withAttributes:tableTitleAtttributes];
        CGRect sessionDateSize = [sessionDate boundingRectWithSize:CGSizeMake(tableSingleMaxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:tableTitleAtttributes context:nil];
        [self drawTableAt:CGPointMake(xOrigin, currentPageY) withRowHeight:sessionDateSize.size.height andColumnWidth:tableSingleMaxWidth andRowCount:1 andColumnCount:1];
        currentPageY = currentPageY + floor(sessionDateSize.size.height);
        
        for (EventDuration* duration in durations)
        {
            NSDate *durationDate = [NSDate GetDateSameAsStringFromDateString:duration.date];
            if([durationDate isEqualToDate:date])
            {
                Session* session = (Session*)duration.session;
                
                NSString* eventDate = [NSString stringWithFormat:@"%@ - %@   %@",duration.startTime,duration.endTime,timezone];
                
                NSString* eventDescription = @"";
                if((session.location.length > 0) && (session.sessionDescription.length > 0))
                {
                eventDescription = [NSString stringWithFormat:@"%@\r%@%@\r%@%@",session.title,PrintConstantLocation,session.location,PrintConstantDescription,session.sessionDescription];
                }
                else if (session.location.length > 0)
                {
                    eventDescription = [NSString stringWithFormat:@"%@\r%@%@",session.title,PrintConstantLocation,session.location];
                }
                else if (session.sessionDescription.length > 0)
                {
                    eventDescription = [NSString stringWithFormat:@"%@\r%@%@",session.title,PrintConstantDescription,session.sessionDescription];
                }
                else
                {
                    eventDescription = [NSString stringWithFormat:@"%@",session.title];
                }
                
                NSMutableAttributedString * attributedEventDescriptionString= [[NSMutableAttributedString alloc] initWithString:eventDescription];
                NSRange range = [eventDescription rangeOfString:eventDescription];
                [attributedEventDescriptionString addAttributes:tableDescriptionAtttributes range:range];
                NSRange range1 = [eventDescription rangeOfString:PrintConstantLocation];
                [attributedEventDescriptionString addAttributes:tableDescriptionBoldAtttributes range:range1];
                NSRange range2 = [eventDescription rangeOfString:PrintConstantDescription];
                [attributedEventDescriptionString addAttributes:tableDescriptionBoldAtttributes range:range2];
                
                
                
                // before we render any text to the PDF, we need to measure it, so we'll know where to render the
                // next line.
                
                CGRect eventDateSize = [eventDate boundingRectWithSize:CGSizeMake((tableDualMaxWidth * 0.5), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:tableDescriptionAtttributes context:nil];
                
                CGRect eventDescriptionSize = [attributedEventDescriptionString boundingRectWithSize:CGSizeMake((tableDualMaxWidth * 1.5), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                
                CGFloat maxVal = MAX(eventDateSize.size.height, eventDescriptionSize.size.height);
                
                if (maxVal + currentPageY > maxHeight) {
                    // create a new page and reset the current page's Y value
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, kDefaultPageWidth, kDefaultPageHeight), nil);
                    currentPageY = kMargin;
                    
                    //Adding header in all pages
                    [eventTitle drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleAttributes];
                    currentPageY = currentPageY + floor(eventTitleSize.size.height + 5);
                    
                    if(eventLocation.length > 0)
                    {
                        [eventLocation drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleSubheddingAtttributes];
                        currentPageY = currentPageY + floor(eventLocationSize.size.height + 5);
                    }
                    
                    [eventStartEndDate drawInRect:CGRectMake(kMargin, currentPageY, maxWidth, MAXFLOAT) withAttributes:eventTitleSubheddingAtttributes];
                    currentPageY = currentPageY + floor(eventStartEndDateSize.size.height + eventTitleSize.size.height);
                    
                    [sessionDate drawInRect:CGRectMake(kMargin, currentPageY, tableSingleMaxWidth, MAXFLOAT) withAttributes:tableTitleAtttributes];
                    
                    [self drawTableAt:CGPointMake(xOrigin, currentPageY) withRowHeight:sessionDateSize.size.height andColumnWidth:tableSingleMaxWidth andRowCount:1 andColumnCount:1];
                    
                    currentPageY = currentPageY + floor(sessionDateSize.size.height);
                }
                
                // render the text
                [eventDate drawInRect:CGRectMake(kMargin, currentPageY, (tableDualMaxWidth * 0.5), maxHeight) withAttributes:tableDescriptionAtttributes];
                
                [self drawTableAt:CGPointMake(xOrigin, currentPageY) withRowHeight:maxVal andColumnWidth:(tableDualMaxWidth * 0.5) andRowCount:1 andColumnCount:1];
                
                [attributedEventDescriptionString drawInRect:CGRectMake(kMargin + (tableDualMaxWidth * 0.5) + kColumnMargin, currentPageY, (tableDualMaxWidth * 1.5), maxHeight)];
                
                [self drawTableAt:CGPointMake(xOrigin, currentPageY) withRowHeight:maxVal andColumnWidth:(tableDualMaxWidth * 2) andRowCount:1 andColumnCount:1];
                
                currentPageY = currentPageY + floor(maxVal);
            }
        }
    }
    
    // end and save the PDF.
    UIGraphicsEndPDFContext();
    return pdfData;
}

-(void)drawTableAt:(CGPoint)origin
     withRowHeight:(int)rowHeight
    andColumnWidth:(int)columnWidth
       andRowCount:(int)numberOfRows
    andColumnCount:(int)numberOfColumns
{
    for (int i = 0; i <= numberOfRows; i++) {
        int newOrigin = origin.y + (rowHeight*i);
        CGPoint from = CGPointMake(origin.x, newOrigin);
        CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
        [self drawLineFromPoint:from toPoint:to];
    }
    
    for (int i = 0; i <= numberOfColumns; i++) {
        int newOrigin = origin.x + (columnWidth*i);
        CGPoint from = CGPointMake(newOrigin, origin.y);
        CGPoint to = CGPointMake(newOrigin, origin.y +(numberOfRows*rowHeight));
        [self drawLineFromPoint:from toPoint:to];
    }
}

-(void)drawLineFromPoint:(CGPoint)from toPoint:(CGPoint)to
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.3);
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGFloat components[] = {0.0, 0.0, 0.0, 0.4};
    CGColorRef color = CGColorCreate(colorspace, components);
    
    CGContextSetStrokeColorWithColor(context, color);
    CGContextMoveToPoint(context, from.x, from.y);
    CGContextAddLineToPoint(context, to.x, to.y);
    
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
}

//Print preview the constructed PDF
- (void)printPDF:(NSData*)pdfData assetTitle: (NSString*) title {
    UIPrintInteractionController *printer=[UIPrintInteractionController sharedPrintController];
    UIPrintInfo *info = [UIPrintInfo printInfo];
    info.orientation = UIPrintInfoOrientationPortrait;
    info.outputType = UIPrintInfoOutputGeneral;
    info.jobName= [NSString stringWithFormat:@"%@",title];
    info.duplex=UIPrintInfoDuplexLongEdge;
    printer.printInfo = info;
//    printer.showsPageRange=YES;
    printer.printingItem=pdfData;
    
    UIPrintInteractionCompletionHandler completionHandler =
    ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        if (!completed && error)
        {
            [AbbvieLogging logError:[NSString stringWithFormat:@"FAILED! error = %@",[error localizedDescription]]];
        }
        else if (!completed && !error)
        {
            [self printCancelTapped];
        }
    };
    [self showLoadingIndicator:NO];
    [printer presentAnimated:YES completionHandler:completionHandler];
}

- (void) printCancelTapped
{
    [AbbvieLogging logInfo:@"Cancel/Print Tapped"];
    [self.monthCalendarViewController makeCellsVisibleContainingSession];
}

#pragma mark - Months Calendar Delegate Method
- (void)monthsCalendarView:(MonthsCalendarViewController*)monthsCalendarView didSelectDate:(NSDate*)date
{
    //TODO:Uncomment this Saranya
    CalendarDetailVC *calendarDetailScreenVC = [CalendarDetailVC GetCalendarDetailsScreen];
    calendarDetailScreenVC.event = self.selectedEvent;
    calendarDetailScreenVC.selectedDate  = date;

    [self.navigationController pushViewController:calendarDetailScreenVC animated:YES];
}



#pragma mark - ZMHeader delegate methods for global search

-(void)header:(ZMHeader*)headerView editingChanged:(NSString*)searchtext {
    if (searchtext.length >= 3)
    {
        self.eventsGSViewModel.currentFilter.searchTextString = searchtext;
        [ZMUserActions sharedInstance].currentFilter = self.eventsGSViewModel.currentFilter;
    }
    else
    {
        self.eventsGSViewModel.currentFilter.searchTextString = searchtext;
        [ZMUserActions sharedInstance].enableSearch = YES;

        
    }
}


#pragma mark - GSViewControllerDelegate Method
- (void)didRemovedFromSuperView
{
    //NSLog(@"Global Search Removed from Superview");
    [self addSelfToListenForNotificationCenter];
    [[ZMProntoManager sharedInstance].search setTitle:@""];
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
    [ZMUserActions sharedInstance].globalSearchViewController = nil;
    self.globalSearcVC = nil;

    [[EventCoreDataManager sharedInstance] resetEventSearchedResults];
    [[EventCoreDataManager sharedInstance] resetEventOnlyAssets];
    [[EventCoreDataManager sharedInstance] resetAssets];
}


- (void)addSelfToListenForNotificationCenter
{
    [self registerNotification:kPreferenceListVCDidSavePreferencesNotification];
    [self registerNotification:UIApplicationWillEnterForegroundNotification];
}

- (void)removeNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:name object:nil];
}

- (void)registerNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:name object:nil];
}

- (void)removeSelfFromListeningToNotificationCenter
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)recievedNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:kPreferenceListVCDidSavePreferencesNotification])
    {
        //On changing the preferences, populate the preferences accordingly.
//        [self.eventListViewController initializeDataSource];
    }
    else if ([[notification name] isEqualToString:UIApplicationWillEnterForegroundNotification])
    {
        //Refreshing event details when user coming from background to foreground.
        [self makeEventsDetailRequestAndUpdateCalendarView];
    }
}

@end
