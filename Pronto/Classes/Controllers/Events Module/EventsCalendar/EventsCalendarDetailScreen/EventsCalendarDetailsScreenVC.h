//
//  EventsCalendarDetailsScreen.h
//  Pronto
//
//  Created by m-666346 on 09/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMHeader.h"

@class EventsGlobalSearchViewModel;
@class Event;


@interface EventsCalendarDetailsScreenVC : UIViewController

+(EventsCalendarDetailsScreenVC*)GetEventsCalendarDetailsScreen;

@property (nonatomic, strong) Event *selectedEvent;
@property(nonatomic, strong) EventsGlobalSearchViewModel *eventsGSViewModel;

- (void)updateLocalDatabaseForEventDetails;

@end
