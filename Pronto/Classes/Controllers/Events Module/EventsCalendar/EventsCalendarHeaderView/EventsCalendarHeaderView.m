//
//  EventsCalendarHeaderView.m
//  Pronto
//
//  Created by m-666346 on 14/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventsCalendarHeaderView.h"
#import "ViewUtils.h"
#import "UIColor+ColorDirectory.h"
#import "UIFont+FontDirectory.h"
#import "UIImage+ImageDirectory.h"
#import "EventListViewController.h"
#import "NSDate+DateDirectory.h"
#import "Pronto-Swift.h"

#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"



@implementation EventsCalendarHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setupUI
{
    self.eventsCalendarHeaderContainerView.backgroundColor = [UIColor whiteColor];
    self.datePrintContainerView.backgroundColor = [UIColor whiteColor];
    self.printOptionContainerView.backgroundColor = [UIColor clearColor];
    self.closeOptionContainerView.backgroundColor = [UIColor clearColor];
    self.refreshOptionContainerView.backgroundColor = [UIColor clearColor];
    
    [self.backButton setImage:[UIImage GetGrayBackIconImage] forState:UIControlStateNormal];

    
    self.closeTitleLabel.textColor = [UIColor GetColorFromHexValue0082ba];
    self.closeTitleLabel.font      = [UIFont FontLatoRegularSize14];
    self.closeImageView.image      = [UIImage GetBlueCloseIconImage];
    
//    self.refreshTitleLabel.textColor = [UIColor GetColorFromHexValue0082ba];
    self.refreshTitleLabel.textColor = [[RedesignThemesClass sharedInstance]hexStringToUIColorWithHex:RedesignThemesClass.sharedInstance.royalBlueCode];
    self.refreshTitleLabel.font    = [UIFont FontLatoRegularSize14];
    self.refreshImageView.image    = [UIImage GetRefreshImage];

    self.middleDividerView.backgroundColor = [UIColor GetColorFromHexValue0082ba];
    
    self.printLabel.textColor = [[RedesignThemesClass sharedInstance]hexStringToUIColorWithHex:RedesignThemesClass.sharedInstance.royalBlueCode];
//    self.printLabel.textColor   = [UIColor GetColorFromHexValue0082ba];
    self.printLabel.font        = [UIFont FontLatoRegularSize14];
    self.printImageView.image   = [UIImage GetPrintAssetDetailIconImage];

    self.eventsPlannerNameLabel.textColor   = [UIColor GetColorFromHexValueaba9a8];
    self.eventsPlannerNameLabel.font        = [UIFont  FontLatoRegularSize12];
    
    self.eventPlannerLabel.textColor    = [UIColor GetColorFromHexValue2d2926];
    self.eventPlannerLabel.font         = [UIFont FontLatoBoldSize12];
    
    self.eventsTitleValueLabel.textColor  = [UIColor GetColorFromHexValueaba9a8];
    self.eventsTitleValueLabel.font       = [UIFont FontLatoRegularSize12];
    
    self.eventsTitleLabel.textColor = [UIColor GetColorFromHexValue2d2926];
    self.eventsTitleLabel.font      = [UIFont FontLatoBoldSize12];
    
    self.eventsFranchiseNameLabel.textColor = [UIColor GetColorFromHexValue2d2926];
    self.eventsFranchiseNameLabel.font      = [UIFont FontLatoRegularSize17];
    
    self.eventsOccoranceTimeLabel.textColor = [UIColor GetColorFromHexValueaba9a8];
    self.eventsOccoranceTimeLabel.font      = [UIFont FontLatoRegularSize14];
    
}

- (void)prepareHeaderViewWithData:(Event*)event
{
    NSString *planner = [event getEventPlanner];
    
    if (planner == nil ||  (planner != nil && planner.length <= 0) )
    {
        self.eventsPlannerNameLabel.hidden = YES;
        self.eventPlannerLabel.hidden = YES;
    }
    else
    {
        self.eventsPlannerNameLabel.text   = planner;
        
        self.eventsPlannerNameLabel.hidden = NO;
        self.eventPlannerLabel.hidden = NO;
    }
    self.eventsFranchiseNameLabel.text   = event.title;
    self.eventsTitleValueLabel.text      =  [event getFormattedDateString];
    self.eventsOccoranceTimeLabel.text   = [event getEventOccuranceDateString];
    
    [self setupRefreshPrintOptionFrameForEvent:event];
}

- (void)setupRefreshPrintOptionFrameForEvent:(Event*)event
{
    if (event.print.boolValue == YES) // Need to show Refresh and option view
    {
        self.printOptionContainerView.left = self.datePrintContainerView.width - (self.printOptionContainerView.width );
        self.refreshOptionContainerView.left = self.printOptionContainerView.left - (self.refreshOptionContainerView.width );
        
        self.printOptionContainerView.hidden = NO;
        self.refreshOptionContainerView.hidden = NO;
    }
    else // Need to show only Refresh
    {
        self.refreshOptionContainerView.left = self.datePrintContainerView.width - (self.refreshOptionContainerView.width);
        
        self.printOptionContainerView.hidden = YES;
        self.refreshOptionContainerView.hidden = NO;
    }
}

- (void)prepareHeaderViewForSessionScreen
{
  
    self.closeOptionContainerView.hidden = YES;
    
}

- (IBAction)backButtonAction:(UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(eventCalendarHeaderView:didTapBackButton:)])
    {
        [self.delegate eventCalendarHeaderView:self didTapBackButton:sender];
    }
}

- (IBAction)printOptionButtonAction:(UIButton *)sender
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventCalendarHeaderView:didTapPrintButton:)])
    {
        [self.delegate eventCalendarHeaderView:self didTapPrintButton:sender];
    }
}

- (IBAction)closeButtonAction:(UIButton *)sender
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventCalendarHeaderView:didTapCloseButton:)])
    {
        [self.delegate eventCalendarHeaderView:self didTapCloseButton:sender];
    }
}

- (IBAction)refreshButtonAction:(id)sender
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventCalendarHeaderView:didTapRefreshButton:)])
    {
        [self.delegate eventCalendarHeaderView:self didTapRefreshButton:sender];
    }
}


+(EventsCalendarHeaderView*)GetEventsCalendarHeaderView
{
    return (EventsCalendarHeaderView*)[[[NSBundle mainBundle] loadNibNamed:@"EventsCalendarHeaderView" owner:self options:nil] lastObject];
}


+(EventsCalendarHeaderView*)GetEventsCalendarHeaderViewWithFrame:(CGRect)frame
{
    EventsCalendarHeaderView *headerView = [self GetEventsCalendarHeaderView];
    headerView.frame = frame;
    return headerView;
}

@end
