//
//  EventsCalendarHeaderView.h
//  Pronto
//
//  Created by m-666346 on 14/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EventsCalendarHeaderView;
@class Event;

@protocol EventsCalendarHeaderViewDelegate<NSObject>

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapPrintButton:(UIButton*)sener;

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapBackButton:(UIButton*)sener;

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapCloseButton:(UIButton*)sener;

- (void)eventCalendarHeaderView:(EventsCalendarHeaderView*)view  didTapRefreshButton:(UIButton*)sender;
@end


@interface EventsCalendarHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIView *eventsCalendarHeaderContainerView;
@property (weak, nonatomic) IBOutlet UIView *datePrintContainerView;
@property (weak, nonatomic) IBOutlet UIView *closeOptionContainerView;
@property (weak, nonatomic) IBOutlet UIView *printOptionContainerView;
@property (weak, nonatomic) IBOutlet UIView *refreshOptionContainerView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *printImageView;
@property (weak, nonatomic) IBOutlet UILabel *printLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventsOccoranceTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *middleDividerView;
@property (weak, nonatomic) IBOutlet UILabel *eventsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventsTitleValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventPlannerLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventsPlannerNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *closeImageView;
@property (weak, nonatomic) IBOutlet UILabel *closeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventsFranchiseNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *refreshImageView;
@property (weak, nonatomic) IBOutlet UILabel *refreshTitleLabel;


@property (nonatomic, weak) id <EventsCalendarHeaderViewDelegate> delegate;


+(EventsCalendarHeaderView*)GetEventsCalendarHeaderView;
+(EventsCalendarHeaderView*)GetEventsCalendarHeaderViewWithFrame:(CGRect)frame;

- (void)prepareHeaderViewWithData:(Event*)event;

- (void)prepareHeaderViewForSessionScreen;

@end
