//
//  EventsPrintOverlayView.m
//  Pronto
//
//  Created by Praveen Kumar on 16/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "ProntoOverlayView.h"
#import "UIFont+FontDirectory.h"
#import "UIColor+ColorDirectory.h"

#define indicatorViewHeight 100.0
#define indicatorViewWidth 190.0

@implementation ProntoOverlayView

+ (ProntoOverlayView*)GetProntoOverlayView
{
    return (ProntoOverlayView*)[[[NSBundle mainBundle] loadNibNamed:@"ProntoOverlayView" owner:self options:nil] lastObject];
}

+ (ProntoOverlayView*)PrepareProntoOverlayWithView:(UIView*) parentView
{
    ProntoOverlayView *prontoOverlayView = [self GetProntoOverlayView];
    
    prontoOverlayView.bgView = [[UIView alloc]init];
    prontoOverlayView.bgView.frame = parentView.bounds;
    [prontoOverlayView.bgView setBackgroundColor:[UIColor GetTransparentOverlayColor]];
    [parentView addSubview:prontoOverlayView.bgView];
    prontoOverlayView.center = prontoOverlayView.bgView.center;
    [parentView addSubview:prontoOverlayView];
    
    return prontoOverlayView;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.overlayMessageLabel.font = [UIFont FontLatoRegularSize16];
    self.overlayMessageLabel.textColor = [UIColor GetColorFromHexValueFFFFFF];
    self.overlayActivityIndicator.color = [UIColor GetColorFromHexValueFFFFFF];
    self.overlayActivityIndicator.transform = CGAffineTransformMakeScale(2.5, 2.5);
}

-(void) showProntoOverlayViewWithMessage:(NSString*)message
{
    self.overlayMessageLabel.text = message;
    [self.overlayActivityIndicator startAnimating];
}

-(void) removeProntoOverlay
{
    [self.overlayActivityIndicator stopAnimating];
    [self.bgView removeFromSuperview];
    [self removeFromSuperview];
}

-(void) adjustProntoOverlay
{
    CGRect frame = self.bgView.frame;
    frame.size.width = self.bgView.frame.size.height;
    frame.size.height = self.bgView.frame.size.width;
    self.bgView.frame = frame;
    
    CGRect indicatorViewFrame = self.frame;
    indicatorViewFrame.size.width = indicatorViewWidth;
    indicatorViewFrame.size.height = indicatorViewHeight;
    self.frame = indicatorViewFrame;
    self.center = self.bgView.center;
}

@end
