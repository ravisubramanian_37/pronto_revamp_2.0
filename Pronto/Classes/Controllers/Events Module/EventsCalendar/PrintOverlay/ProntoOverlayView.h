//
//  EventsPrintOverlayView.h
//  Pronto
//
//  Created by Praveen Kumar on 16/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProntoOverlayView : UIView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *overlayActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *overlayMessageLabel;
@property (strong, nonatomic) UIView *bgView;

-(void) showProntoOverlayViewWithMessage:(NSString*) message;
-(void) adjustProntoOverlay;
-(void) removeProntoOverlay;

+ (ProntoOverlayView*)GetProntoOverlayView;
+ (ProntoOverlayView*)PrepareProntoOverlayWithView:(UIView*) parentView;

@end
