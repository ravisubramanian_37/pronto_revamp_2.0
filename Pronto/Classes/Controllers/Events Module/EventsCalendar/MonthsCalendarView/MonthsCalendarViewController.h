//
//  MonthsCalendarViewController.h
//  Pronto
//
//  Created by m-666346 on 09/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNCalendarView.h"
@class Event;
@class MonthsCalendarViewController;


@protocol MonthsCalendarDelegate<NSObject>

- (void)monthsCalendarView:(MonthsCalendarViewController*)monthsCalendarView didSelectDate:(NSDate*)date;

@end

@interface MonthsCalendarViewController : UIViewController

- (instancetype) initWithFrame:(CGRect)frame;
- (instancetype) initWithFrame:(CGRect)frame andEvent:(Event*)event;

- (void)makeCellsVisibleContainingSession;

//We are passing event object
@property (nonatomic, strong) Event *event;
@property (nonatomic, strong) MNCalendarView *calendarView;
@property (nonatomic, weak) id <MonthsCalendarDelegate>delegate;

@end
