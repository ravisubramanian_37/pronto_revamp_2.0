//
//  MonthsCalendarViewController.m
//  Pronto
//
//  Created by m-666346 on 09/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "MonthsCalendarViewController.h"
#import "MNCalendarView.h"
#import "EventListViewController.h"
#import "NSDate+DateDirectory.h"
#import "MonthCustomView.h"

#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"

#import "EventDuration+CoreDataProperties.h"
#import "EventDuration+CoreDataClass.h"
#import "Pronto-Swift.h"


@interface MonthsCalendarViewController ()<MNCalendarViewDelegate, UIScrollViewDelegate>

@property(nonatomic,strong) NSCalendar     *calendar;
@property(nonatomic,strong) NSDate         *currentDate;
@property(nonatomic, assign) BOOL didCalendarViewScrolled;

@end

@implementation MonthsCalendarViewController

//Logic: cell containig session will be made visible, until user scrolls the calendar explicitly
/**
 It is responsible for making cell visible which has session.
 */

- (void)makeCellsVisibleContainingSession
{
    if (self.didCalendarViewScrolled == YES) { return ;}
    
    EventDuration *eventDuration = (EventDuration*)[[self.event sortEventDurationAccordingToStartTime] firstObject];
    NSDate *eventDurationStartDate = [eventDuration eventDate];
    
    if (eventDurationStartDate == nil ||
        [NSDate isDate1:eventDurationStartDate greaterThanDate:[self.event eventEndDate]])
    {
        // If first session date is greater than that of dates shown in calendar, we will not scroll up.
        // This is not valid use case.
        return;
    }
    
    NSInteger eventDurationStartDayDate = [NSDate GetDateInUTCFromDate:eventDurationStartDate];
    NSInteger sectionForEventDurationStartDateMonth = [NSDate GetMonthNameFromDate:eventDurationStartDate];
    
    NSDate *eventStartDate = [self.event eventStartDate];
    NSInteger sectionForEventStartDateMonth = [NSDate GetMonthNameFromDate:eventStartDate];
    NSInteger section = 0;
    
    if ([NSDate isDate1:eventStartDate fallsInSameYearAsDate2:eventDurationStartDate])
    {
        section =  sectionForEventDurationStartDateMonth - sectionForEventStartDateMonth;
    }
    else
    {
        section = (12 - sectionForEventStartDateMonth) + sectionForEventDurationStartDateMonth;
    }
    
    if ([self.calendarView isValidSection:section] == NO)
    {
        //If section is not valid, don't scroll the calendar.
        section = 0;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:eventDurationStartDayDate  inSection:section];
    
    [self.calendarView.collectionView scrollToItemAtIndexPath:indexPath
                                             atScrollPosition:UICollectionViewScrollPositionTop
                                                     animated:YES];
    //Force scrolling will not be considered.
    self.didCalendarViewScrolled = NO;
}



- (void)calendarView:(MNCalendarView *)calendarView scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    self.didCalendarViewScrolled = YES;
}

- (instancetype) initWithFrame:(CGRect)frame andEvent:(Event*)event;
{
    if (self = [super init])
    {
        self.event      = event;
        // viewDidLoad gets called whenever the frame changed. So setting properties before frame change.
        self.view.frame = frame;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super init])
    {
        self.view.frame = frame;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareCalendarView];
}

- (void) prepareCalendarView
{
    self.calendar =  [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.currentDate = [NSDate date];
    
    self.calendarView = [[MNCalendarView alloc] initWithFrame:self.view.bounds];
    self.calendarView.event = self.event;
    
    self.calendarView.calendar = self.calendar;
    self.calendarView.selectedDate = [NSDate date]; 
    
    self.calendarView.fromDate = [NSDate GetDateInLocalTimeZoneFromUTCDateExactAsUTCDate:[self.event eventStartDate]];
    self.calendarView.toDate   = [NSDate GetDateInLocalTimeZoneFromUTCDateExactAsUTCDate:[self.event eventEndDate]];
    
    //[self prepareSessionDatasource];
    self.calendarView.currentDate = [NSDate date];
    self.calendarView.delegate = self;
    self.calendarView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.calendarView.backgroundColor = UIColor.whiteColor;
    
    [self.view addSubview:self.calendarView];
    
    [self.calendarView reloadDataWithCompletionHandler:^{
        
        [self makeCellsVisibleContainingSession];

    }];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.calendarView.collectionView.collectionViewLayout invalidateLayout];
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.calendarView reloadDataWithCompletionHandler:^{
       
        [self makeCellsVisibleContainingSession];
       
    }];
}

#pragma mark - MNCalendarViewDelegate

- (void)calendarView:(MNCalendarView *)calendarView didSelectDate:(NSDate *)date
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"date selected: %@", date]];
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(monthsCalendarView:didSelectDate:
                                                    )])
    {
        [self.delegate monthsCalendarView:self didSelectDate:date];
    }
}

- (BOOL)calendarView:(MNCalendarView *)calendarView shouldSelectDate:(NSDate *)date
{

    
    return YES;
}

@end
