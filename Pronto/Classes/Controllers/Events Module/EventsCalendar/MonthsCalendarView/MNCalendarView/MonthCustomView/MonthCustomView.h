//
//  MonthCustomView.h
//  DemoWithCalendarLib
//
//  Created by m-666346 on 06/08/18.
//  Copyright © 2018 m-666346. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MGCEventView.h"

//@class CustomData;
@class MonthCustomView;

@protocol MonthCustomViewDelegate<NSObject>

- (void)monthCustomView:(MonthCustomView*)monthCustomView didSelectDate:(NSDate*)date;

@end


@interface MonthCustomView : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *eventsCountLabel;
@property (nonatomic, weak) IBOutlet UIView  *eventsCountDateContainerView;
@property (nonatomic, weak) IBOutlet UIView  *monthCustomCellContainerView;
@property (nonatomic, weak) IBOutlet UIView  *singleDayEventContainerView;
@property (nonatomic, weak) id<MonthCustomViewDelegate>delegate;


+ (NSString*)ViewIdentifier;
+ (NSString*)NibName;
+ (MonthCustomView*)GetMonthCustomView;

- (void)setDate:(NSDate *)date
          month:(NSDate *)month
       calendar:(NSCalendar *)calendar;

- (void)prepareWithDayEventsWithDurations:(NSArray*)durations;
- (void)prepareCellForCurrentDate;

//property from other lib
@property(nonatomic,strong,readonly) NSDate *date;
@property(nonatomic,strong,readonly) NSDate *month;
@property(nonatomic,strong) NSCalendar *calendar;
@property(nonatomic,assign,getter = isEnabled) BOOL enabled;


@end
