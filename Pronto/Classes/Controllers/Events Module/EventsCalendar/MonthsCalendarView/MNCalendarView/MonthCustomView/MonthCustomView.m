//
//  MonthCustomView.m
//  DemoWithCalendarLib
//
//  Created by m-666346 on 06/08/18.
//  Copyright © 2018 m-666346. All rights reserved.
//

#import "MonthCustomView.h"
#import "UIColor+ColorDirectory.h"
#import "SingleDayEventView.h"
#import "ViewUtils.h"
#import "UIFont+FontDirectory.h"
#import "EventListViewController.h"
#import "NSDate+DateDirectory.h"


//#import "CustomData.h"


@interface MonthCustomView ()

@property(nonatomic,strong,readwrite) NSDate *date;
@property(nonatomic,strong,readwrite) NSDate *month;
@property(nonatomic,assign,readwrite) NSUInteger weekday;
@property(nonatomic,strong) SingleDayEventView *singleEventView;

@end


@implementation MonthCustomView

- (void)setDate:(NSDate *)date
          month:(NSDate *)month
       calendar:(NSCalendar *)calendar {
    
    self.date     = date;
    self.month    = month;
    self.calendar = calendar;
    
    NSDateComponents *components =

    [self.calendar components:NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday
                     fromDate:self.date];
    
    NSDateComponents *monthComponents =
    [self.calendar components:NSCalendarUnitMonth 
                     fromDate:self.month];
    
    self.weekday = components.weekday;
    self.dateLabel.text = [NSString stringWithFormat:@"%ld", (long)components.day];
//    BOOL isWeekEnd = [self.calendar isDateInWeekend:self.date];
    if (monthComponents.month == components.month)
    {
        self.dateLabel.hidden = NO;
        [self setEnabled:YES];
    }
    else
    {
        [self setEnabled:NO];
        //If Date belongs to different month hide the date label
        self.dateLabel.hidden = YES;
    }
    
    [self setNeedsDisplay];
}

- (void)setEnabled:(BOOL)enabled
{
    _enabled = enabled;
    [self prepareCellViewForHighlighted:enabled];
}


+ (NSString*)ViewIdentifier
{
    return @"ViewIdentifier";
}

+ (NSString*)NibName
{
    return @"MonthCustomView";
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.singleEventView removeFromSuperview];
    self.singleEventView = nil;
    self.dateLabel.hidden = NO;
    self.enabled = NO;
    [self setupUI];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self prepareCellViewForHighlighted:NO];
    self.eventsCountLabel.textColor = [UIColor GetColorFromHexValue0082ba];
    self.dateLabel.hidden = NO;
    self.enabled = NO;
    [self.singleEventView removeFromSuperview];
    self.singleEventView = nil;
}

- (void)setupUI
{
    self.eventsCountLabel.textColor = [UIColor GetColorFromHexValue0082ba];
    self.eventsCountLabel.font      = [UIFont FontLatoRegularSize12];
    self.dateLabel.font             = [UIFont FontLatoRegularSize13];
}

- (void)prepareCellViewForHighlighted:(BOOL)highlighted
{
    if (highlighted == YES)
    {
        self.monthCustomCellContainerView.backgroundColor = [UIColor whiteColor];
        self.dateLabel.backgroundColor      = [UIColor GetColorFromHexValue417BAF];
        self.dateLabel.textColor            = [UIColor GetColorFromHexValueFFFFFF];
        
        self.eventsCountLabel.hidden        = NO;
    }
    else
    {
        self.monthCustomCellContainerView.backgroundColor = [UIColor GetColorFromHexValueE6EDF4]; 
        self.dateLabel.backgroundColor      = [UIColor GetColorFromHexValueDAE4EF];
        self.dateLabel.textColor            = [UIColor GetColorFromHexValueaba9a8];
        
        self.eventsCountLabel.hidden        = YES;
    }
}

- (void)prepareCellForCurrentDate
{
    self.dateLabel.backgroundColor  = [UIColor GetColorFromHexValue1AC9A8];
    self.eventsCountLabel.textColor = [UIColor GetColorFromHexValue1AC9A8];
}

+ (MonthCustomView*)GetMonthCustomView
{
    return (MonthCustomView*)[[[NSBundle mainBundle] loadNibNamed:@"MonthCustomView" owner:self options:nil] lastObject];
}

- (void)prepareWithDayEventsWithDurations:(NSArray*)durations
{
    
    
    //prepare table view from dayEvent is next task
    if (durations != nil)
    {
        if (self.singleEventView == nil)
        {
            self.singleEventView = [SingleDayEventView GetSingleDayEventViewFrame:self.singleDayEventContainerView.bounds];
            self.singleEventView.delegate = (id<SingleDayEventViewDelegate>)self;
        }
        [self.singleDayEventContainerView addSubview:self.singleEventView];
        self.singleEventView.durations = durations;
    
        //Else whatever value we have assigned is fine;
    }
}

- (NSString*)getFormattedStringFromDate:(NSDate*)date
{
    
    // create date directory and every manipulation there.
    

    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: date];
    NSInteger day = dateComponents.day;
    return [NSString stringWithFormat:@"%ld",(long)day];
}

#pragma mark - Single Day EventView Delegate Method

- (void)didSelectView:(SingleDayEventView*)singleDayEventView
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(monthCustomView:didSelectDate:)])
    {
        NSDate *date = [NSDate GetDateInLocalTimeZoneFromUTCDate:self.date];
        //Add an hour if it is in daylight saving for Canadian time zone
        if(![[NSDate GetDateStringFromFormaterWithUTC:date] isEqualToString:[NSDate GetDateStringInFormateDDMMYYYYFromDate:self.date]])
        {
            date = [NSDate GetDateInLocalTimeZoneFromUTCDateForDayLightSavings:date];
        }
        [self.delegate monthCustomView:self didSelectDate:date];
    }
}

@end
