//
//  MNCalendarView.m
//  MNCalendarView
//
//  Created by Min Kim on 7/23/13.
//  Copyright (c) 2013 min. All rights reserved.
//

#import "MNCalendarView.h"
#import "MNCalendarViewLayout.h"
#import "MNCalendarViewDayCell.h"
#import "MNCalendarViewWeekdayCell.h"
#import "MNCalendarHeaderView.h"
#import "MNFastDateEnumeration.h"
#import "NSDate+MNAdditions.h"
#import "MonthCustomView.h"
#import "NSDate+DateDirectory.h"
#import "EventListViewController.h"
#import "UICollectionView+Utility.h"
#import "Constant.h"

#import "Event+CoreDataProperties.h"
#import "Event+CoreDataClass.h"


@interface MNCalendarView() <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>

@property(nonatomic,strong,readwrite) UICollectionView *collectionView;
@property(nonatomic,strong,readwrite) UICollectionViewFlowLayout *layout;

@property(nonatomic,strong,readwrite) NSArray *monthDates;
@property(nonatomic,strong,readwrite) NSArray *weekdaySymbols;
@property(nonatomic,assign,readwrite) NSUInteger daysInWeek;

@property(nonatomic,strong,readwrite) NSDateFormatter *monthFormatter;

- (NSDate *)firstVisibleDateOfMonth:(NSDate *)date;
- (NSDate *)lastVisibleDateOfMonth:(NSDate *)date;

- (BOOL)dateEnabled:(NSDate *)date;
- (BOOL)canSelectItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)applyConstraints;

@end

@implementation MNCalendarView

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(calendarView:scrollViewWillEndDragging:withVelocity:targetContentOffset:)])
    {
        [self.delegate calendarView:self scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)commonInit
{
    //  self.calendar   = NSCalendar.currentCalendar;
    self.calendar   = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  self.fromDate   = [NSDate.date mn_beginningOfDay:self.calendar];
  self.toDate     = [self.fromDate dateByAddingTimeInterval:MN_YEAR * 4];
  self.daysInWeek = 7;
  
  self.headerViewClass  = MNCalendarHeaderView.class;
  self.weekdayCellClass = MNCalendarViewWeekdayCell.class;
  self.dayCellClass     = MNCalendarViewDayCell.class;
  
  _separatorColor = [UIColor colorWithRed:.85f green:.85f blue:.85f alpha:1.f];
  
  [self addSubview:self.collectionView];
  [self applyConstraints];
//  [self reloadData];
  [self reloadDataWithCompletionHandler:nil];
}

- (id)initWithFrame:(CGRect)frame {
  if (self = [super initWithFrame:frame]) {
    [self commonInit];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder: aDecoder];
  if ( self ) {
    [self commonInit];
  }
  
  return self;
}

- (UICollectionView *)collectionView {
  if (nil == _collectionView) {
    MNCalendarViewLayout *layout = [[MNCalendarViewLayout alloc] init];

    _collectionView =
      [[UICollectionView alloc] initWithFrame:CGRectZero
                         collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor colorWithRed:.96f green:.96f blue:.96f alpha:1.f];
    _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    
    [self registerUICollectionViewClasses];
  }
  return _collectionView;
}

- (void)setSeparatorColor:(UIColor *)separatorColor {
  _separatorColor = separatorColor;
}

- (void)setCalendar:(NSCalendar *)calendar {
  _calendar = calendar;
  
  self.monthFormatter = [[NSDateFormatter alloc] init];
  self.monthFormatter.calendar = calendar;
  [self.monthFormatter setDateFormat:@"MMMM yyyy"];
}

- (void)setSelectedDate:(NSDate *)selectedDate {
  _selectedDate = [selectedDate mn_beginningOfDay:self.calendar];
}

- (void)reloadDataWithCompletionHandler:(completionHandler)completion
{
    NSMutableArray *monthDates = @[].mutableCopy;
    MNFastDateEnumeration *enumeration =
    [[MNFastDateEnumeration alloc] initWithFromDate:[self.fromDate mn_firstDateOfMonth:self.calendar]
                                             toDate:[self.toDate mn_firstDateOfMonth:self.calendar]
                                           calendar:self.calendar
                                               unit:NSCalendarUnitMonth];
    
    for (NSDate *date in enumeration) {
        [monthDates addObject:date];
    }
    
    self.monthDates = monthDates;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.calendar = self.calendar;
    
    self.weekdaySymbols = [NSArray array];
    
    [self.collectionView reloadDataWithCompletion:^{
        
        if (completion != nil)
        {
            completion();
        }
    }];
}


- (void)registerUICollectionViewClasses
{
    [self.collectionView registerNib:[UINib nibWithNibName:[MonthCustomView NibName] bundle:nil]
      forCellWithReuseIdentifier:[MonthCustomView ViewIdentifier]];
    
    [self.collectionView registerClass:self.headerViewClass
        forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
               withReuseIdentifier:MNCalendarHeaderViewIdentifier];
    
}


- (NSDate *)firstVisibleDateOfMonth:(NSDate *)date {
  date = [date mn_firstDateOfMonth:self.calendar];

    
  NSDateComponents *components =
    [self.calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday
                fromDate:date];

  return
    [[date mn_dateWithDay:-((components.weekday - 1) % self.daysInWeek) calendar:self.calendar] dateByAddingTimeInterval:MN_DAY];
}

- (NSDate *)lastVisibleDateOfMonth:(NSDate *)date {
  date = [date mn_lastDateOfMonth:self.calendar];

  
  NSDateComponents *components =
    [self.calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday
                     fromDate:date];

  return
    [date mn_dateWithDay:components.day + (self.daysInWeek - 1) - ((components.weekday - 1) % self.daysInWeek)
                calendar:self.calendar];
}




- (void)applyConstraints {
  NSDictionary *views = @{@"collectionView" : self.collectionView};
  [self addConstraints:
   [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|"
                                           options:0
                                           metrics:nil
                                             views:views]];
  
  [self addConstraints:
   [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|"
                                           options:0
                                           metrics:nil
                                             views:views]
   ];
}

- (BOOL)dateEnabled:(NSDate *)date {
  if (self.delegate && [self.delegate respondsToSelector:@selector(calendarView:shouldSelectDate:)]) {
    return [self.delegate calendarView:self shouldSelectDate:date];
  }
  return YES;
}

- (BOOL)canSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    return YES;
}

#pragma mark - UICollectionViewDataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    MNCalendarHeaderView *headerView =
    [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                       withReuseIdentifier:MNCalendarHeaderViewIdentifier
                                              forIndexPath:indexPath];
    
    headerView.backgroundColor = self.collectionView.backgroundColor;
    headerView.titleLabel.text = [self.monthFormatter stringFromDate:self.monthDates[indexPath.section]];

    return headerView;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.monthDates.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSDate *monthDate = self.monthDates[section];
    
    NSDateComponents *components =
    [self.calendar components:NSCalendarUnitDay
                     fromDate:[self firstVisibleDateOfMonth:monthDate]
                       toDate:[self lastVisibleDateOfMonth:monthDate]
                      options:0];
    
    return  components.day+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MonthCustomView *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:[MonthCustomView ViewIdentifier]
                                              forIndexPath:indexPath];
    
    cell.delegate = (id<MonthCustomViewDelegate>)self;
    
    NSDate *monthDate = self.monthDates[indexPath.section];
    NSDate *firstDateInMonth = [self firstVisibleDateOfMonth:monthDate];
    
    NSUInteger day = indexPath.item;
    
    NSDateComponents *components =
    [self.calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                     fromDate:firstDateInMonth];
    components.day += day;
    
    NSDate *date = [self.calendar dateFromComponents:components];
    [cell setDate:date
            month:monthDate
         calendar:self.calendar];
    
    if (cell.enabled) // prepare cell only if it is enabled and have data
    {
        NSDate *timeWithLocalTimeZone = [NSDate GetDateInLocalTimeZoneFromUTCDate:cell.date];
        NSArray *durations = [self.event getSortedDurationForDateString:timeWithLocalTimeZone];
        
        if (durations.count > 0)
        {
            [cell prepareWithDayEventsWithDurations:durations];
            NSString *singularOrPluralEvents = durations.count == 1 ? @"Event" : @"Events";
            cell.eventsCountLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)durations.count, singularOrPluralEvents];
            cell.dateLabel.text = [NSString stringWithFormat:@"%0.2ld", (long)[NSDate GetDateFromDate:cell.date]];
        }
        else
        {
          [cell setEnabled:NO];
        }
    }
    
    if (cell.enabled)
    {
        [cell setEnabled:[self dateEnabled:date]];
    }
    
    if (self.selectedDate && cell.enabled)
    {
        [cell setSelected:[date isEqualToDate:self.selectedDate]];
    }
    
    if ([self.calendar isDate:self.currentDate inSameDayAsDate:cell.date])
    {
        [cell prepareCellForCurrentDate];
    }
    
    return cell;
}


#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
  return [self canSelectItemAtIndexPath:indexPath];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  return [self canSelectItemAtIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(calendarView:didSelectDate:)])
    {
        MonthCustomView *cell = (MonthCustomView*)[collectionView cellForItemAtIndexPath:indexPath];
        if (cell != nil && cell.date != nil && cell.enabled == YES)
        {
            NSDate *date = [NSDate GetDateInLocalTimeZoneFromUTCDate:cell.date];
            [self.delegate calendarView:self didSelectDate:date];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width      = self.bounds.size.width;
    CGFloat itemWidth  = roundf(width / self.daysInWeek);
    
    NSUInteger weekday = indexPath.item % self.daysInWeek;
    
    if (weekday == self.daysInWeek - 1) {
        itemWidth = width - (itemWidth * (self.daysInWeek - 1));
    }
    
    return CGSizeMake(itemWidth, 220);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (BOOL)isValidSection:(NSInteger)section
{
    if (section < self.monthDates.count) {
        return YES;
    }
    return NO;
}

#pragma mark - Month Custom View Delegate Method

- (void)monthCustomView:(MonthCustomView*)monthCustomView didSelectDate:(NSDate*)date
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(calendarView:didSelectDate:)])
    {
        [self.delegate calendarView:self didSelectDate:date];
    }
    
}

@end
