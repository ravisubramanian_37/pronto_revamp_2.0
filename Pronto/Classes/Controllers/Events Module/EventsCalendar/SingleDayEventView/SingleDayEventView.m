//
//  SingleDayEventView.m
//  Pronto
//
//  Created by m-666346 on 10/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "SingleDayEventView.h"
#import "SingleDayEventCell.h"
#import "EventListViewController.h"

@interface SingleDayEventView()

@property(nonatomic, strong) NSMutableArray *dayEventDataSource;

@end

@implementation SingleDayEventView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
    
    self.singleDayEventTableView.dataSource = (id<UITableViewDataSource>)self;
    self.singleDayEventTableView.delegate   = (id<UITableViewDelegate>)self;
    
    // No touch events needs to be handled for this view.
    // All touch event handled by parent view. In this case collection view.
    //self.userInteractionEnabled = NO;
}

+ (SingleDayEventView*)GetSingleDayEventView
{
    return (SingleDayEventView*)[[[NSBundle mainBundle] loadNibNamed:@"SingleDayEventView" owner:self options:nil] lastObject];
}

+ (SingleDayEventView*)GetSingleDayEventViewFrame:(CGRect)frame
{
    SingleDayEventView *singleDayEventView = [self GetSingleDayEventView];
    singleDayEventView.frame = frame;
    return singleDayEventView;
}

- (void)setupUI
{
    //Registering the cell with table view
    [self.singleDayEventTableView registerNib:[UINib nibWithNibName:[SingleDayEventCell NibName] bundle:nil] forCellReuseIdentifier:[SingleDayEventCell CellIdentifier]];

    self.backgroundColor = [UIColor clearColor];
}

- (void)setDurations:(NSArray *)durations
{
    _durations = durations;
    [self.singleDayEventTableView reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource and UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.durations.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SingleDayEventCell *cell = [tableView dequeueReusableCellWithIdentifier:[SingleDayEventCell CellIdentifier]];
    if (self.durations.count > 0 && indexPath.row <= self.durations.count)
    {
        [cell prepareCellEventDuration:self.durations[indexPath.row]];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(didSelectView:)])
    {
        [self.delegate didSelectView:self];
    }
}

@end
