//
//  SingleDayEventCell.m
//  Pronto
//
//  Created by m-666346 on 10/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "SingleDayEventCell.h"
#import "UIColor+ColorDirectory.h"
#import "UIFont+FontDirectory.h"

#import "Event+CoreDataProperties.h"
#import "EventDuration+CoreDataProperties.h"


@implementation SingleDayEventCell

+ (NSString*)CellIdentifier
{
    return @"SingleDayEventCell";
}

+ (NSString*)NibName
{
    return @"SingleDayEventCell";
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setupUI
{
    self.eventTimeLabel.font        = [UIFont FontLatoRegularSize11];
    self.eventTimeLabel.textColor   = [UIColor GetColorFromHexValue817f7d];
    
    self.eventDescriptionLabel.font      = [UIFont FontLatoRegularSize12];
    self.eventDescriptionLabel.textColor = [UIColor GetColorFromHexValue2d2926];
}

- (void)prepareCellWithEventTime:(NSString*)eventTime
{
    self.eventTimeLabel.text = eventTime;
}

//- (void)prepareCellWithDictionary:(NSDictionary*)dict
//{
//    self.eventTimeLabel.text = dict[@"time"];
//    self.eventDescriptionLabel.text = dict[@"description"];
//}

- (void)prepareCellEventDuration:(EventDuration*)eventDuration
{
        self.eventTimeLabel.text = [eventDuration getFormattedStartAndEndTimeString];
        self.eventDescriptionLabel.text = [eventDuration getEventDurationTitle];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}


@end
