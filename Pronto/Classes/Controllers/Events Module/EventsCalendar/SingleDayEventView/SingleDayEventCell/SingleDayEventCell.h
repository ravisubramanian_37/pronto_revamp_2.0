//
//  SingleDayEventCell.h
//  Pronto
//
//  Created by m-666346 on 10/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EventDuration;

@interface SingleDayEventCell : UITableViewCell

+ (NSString*)CellIdentifier;
+ (NSString*)NibName;

@property (nonatomic, weak) IBOutlet UIView *singleDayEventCellContainerView;
@property (nonatomic, weak) IBOutlet UILabel *eventTimeLabel;
@property (nonatomic, weak) IBOutlet UILabel *eventDescriptionLabel;

- (void)prepareCellWithEventTime:(NSString*)eventTime;
//- (void)prepareCellWithDictionary:(NSDictionary*)dict;

- (void)prepareCellEventDuration:(EventDuration*)eventDuration;
@end
