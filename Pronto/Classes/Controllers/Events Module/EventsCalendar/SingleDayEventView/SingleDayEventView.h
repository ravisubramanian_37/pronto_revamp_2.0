//
//  SingleDayEventView.h
//  Pronto
//
//  Created by m-666346 on 10/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SingleDayEventView;

@protocol SingleDayEventViewDelegate<NSObject>

- (void)didSelectView:(SingleDayEventView*)singleDayEventView;

@end

@interface SingleDayEventView : UIView

@property (nonatomic, weak) IBOutlet UITableView *singleDayEventTableView;
@property (nonatomic, weak) id <SingleDayEventViewDelegate>delegate;

@property (nonatomic, strong) NSArray *durations;

+ (SingleDayEventView*)GetSingleDayEventView;
+ (SingleDayEventView*)GetSingleDayEventViewFrame:(CGRect)frame;



@end
