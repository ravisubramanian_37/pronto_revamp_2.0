//
//  EventAssetDownloadProvider.h
//  Pronto
//
//  Created by Praveen Kumar on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Event;
@class EventOnlyAsset;

@interface EventAssetDownloadProvider : NSObject

+ (id)sharedEventAssetDownloadProvider;

- (void)submitRequestToDownloadEventOnlyAsset:(EventOnlyAsset*) asset;

- (void)clearOnLogout;

- (void)handleTapToDownloadScenarioForEventOnlyAsset:(EventOnlyAsset*)asset;

- (void)handleTapToDownloadScenarioForEventOnlyAssetOnFailure:(EventOnlyAsset*)asset;

-(void)initiateOfflineDownloadForEvent:(Event*)event;

- (void)pauseOfflineDownload:(BOOL)pause;

-(void)initiateOfflineDownloadForDurations:(NSArray*)durations;

@end
