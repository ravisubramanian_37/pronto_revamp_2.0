//
//  EventAssetDownloadProvider.m
//  Pronto
//
//  Created by Praveen Kumar on 24/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventAssetDownloadProvider.h"
#import "Constant.h"
//#import "ZMAbbvieAPI.h"
#import "ZMUserActions.h"
#import "EventAssetHandler.h"
#import "EventOnlyAssetHandler.h"
#import "AssetDownloadProvider.h"
#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"
#import "OfflineDownload+CoreDataClass.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "EventDuration+CoreDataClass.h"
#import "Pronto-Swift.h"

@class EventDuration;
@class Session;

#define MAX_NNUMBER_ASSET_DOWNLOAD_SIMULATANIOUSLY 2
@interface EventAssetDownloadProvider()

@property(nonatomic, strong)NSMutableArray * assetDownloadInProgress;
@property(nonatomic, strong)NSMutableArray * totalSubmitionAssetDownload;

@property (nonatomic, strong) EventAssetHandler *eventAssetHandler;
@property (nonatomic, strong) EventOnlyAssetHandler *eventOnlyAssetHandler;

@property(nonatomic, strong)NSMutableArray * constantAssetArray;

@property (nonatomic, assign) BOOL isPauseOfflineDownloadEnabled;
@end

@implementation EventAssetDownloadProvider

- (void)pauseOfflineDownload:(BOOL)pause
{
    self.isPauseOfflineDownloadEnabled = pause;
    
    if (self.isPauseOfflineDownloadEnabled == NO)
    {
        
        if(self.totalSubmitionAssetDownload.count > 0)
        {
            EventOnlyAsset* newAsset = [self.totalSubmitionAssetDownload firstObject];
            
            if(newAsset.eoa_id != nil && newAsset != nil)
            {
                [self.assetDownloadInProgress addObject:newAsset];
                [self.totalSubmitionAssetDownload removeObject:newAsset];
                
                [[EventsServiceClass sharedAPIManager] downloadEventOnlyAsset:newAsset];
                
                [AbbvieLogging logInfo:@"eoa -- Downloading start from pause"];
            }
        }
    }
    else
    {
        [AbbvieLogging logInfo:@"eoa -- pausing offline download"];
    }
}

+ (id)sharedEventAssetDownloadProvider
{
    static EventAssetDownloadProvider *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
        manager.assetDownloadInProgress       = [NSMutableArray array];
        manager.totalSubmitionAssetDownload   = [NSMutableArray array];
        manager.isPauseOfflineDownloadEnabled = NO;
        [[NSNotificationCenter defaultCenter] addObserver:manager selector:@selector(recievedNotification:) name:kEventOnlyAssetDownloadProviderCompletionNotification object:nil];
    });
    
    return manager;
}

-(void)submitRequestToDownloadEventOnlyAsset:(EventOnlyAsset*) asset
{
    BOOL isPresent = NO;
    for(EventOnlyAsset *checkAsset in self.totalSubmitionAssetDownload)
    {
        if([checkAsset.eoa_id isEqualToString:asset.eoa_id])
        {
            isPresent = YES;
            break;
        }
    }
    if(!isPresent && [[[asset.eoa_uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"])
    {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Adding EOA Asset into provider: %@ name is : %@",asset.eoa_id,asset.eoa_title]];
        
        [self.totalSubmitionAssetDownload addObject:asset];
        if(self.assetDownloadInProgress.count < MAX_NNUMBER_ASSET_DOWNLOAD_SIMULATANIOUSLY)
        {
            if (self.isPauseOfflineDownloadEnabled == NO)
            {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Initial EOA Download started for: %@ name is : %@",asset.eoa_id,asset.eoa_title]];
                [self.assetDownloadInProgress addObject:asset];
                [self.totalSubmitionAssetDownload removeObject:asset];
                
                [[EventsServiceClass sharedAPIManager] downloadEventOnlyAsset:asset];
                [AbbvieLogging logInfo:@"eoa -- Downloading start from submitRequest"];
            }
        }
    }
}

- (void)recievedNotification:(NSNotification *)notification
{
    NSDictionary* userInfo = notification.userInfo;
    EventOnlyAsset* asset = (EventOnlyAsset*)userInfo[@"asset"];
    if([self.assetDownloadInProgress containsObject:asset])
    {
        [self.assetDownloadInProgress removeObject:asset];
        if(self.totalSubmitionAssetDownload.count > 0)
        {
            EventOnlyAsset* newAsset = [self.totalSubmitionAssetDownload firstObject];
            
            if(newAsset.eoa_id != nil && newAsset != nil)
            {
                if (self.isPauseOfflineDownloadEnabled == NO)
                {
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Download EOA started for: %@ name is : %@",newAsset.eoa_id,newAsset.eoa_title]];
                    
                    [self.assetDownloadInProgress addObject:newAsset];
                    [self.totalSubmitionAssetDownload removeObject:newAsset];
                    [[EventsServiceClass sharedAPIManager] downloadEventOnlyAsset:newAsset];
                    
                    [AbbvieLogging logInfo:@"eoa -- Downloading start from Notification"];
                }
            }
        }
    }
}

- (void)clearOnLogout
{
    [self.assetDownloadInProgress removeAllObjects];
    [self.totalSubmitionAssetDownload removeAllObjects];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)handleTapToDownloadScenarioForEventOnlyAsset:(EventOnlyAsset*)asset
{
    if([self.totalSubmitionAssetDownload containsObject:asset] == YES)
    {
        [self.totalSubmitionAssetDownload removeObject:asset];
    }
}

- (void)handleTapToDownloadScenarioForEventOnlyAssetOnFailure:(EventOnlyAsset*)asset
{
    if([self.totalSubmitionAssetDownload containsObject:asset] == NO)
    {
        [self.totalSubmitionAssetDownload addObject:asset];
    }
}

-(void)initiateOfflineDownloadForEvent:(Event*)event
{
    self.isPauseOfflineDownloadEnabled = NO;
    
    NSSet *mgdObject = event.eventDurations;
    NSMutableArray *assetArray = [NSMutableArray array];
    self.constantAssetArray = [NSMutableArray array];
    for(EventDuration *duration in mgdObject)
    {
        NSArray *allAssets =  [duration totalAssetForEventDuration];
        for(NSObject *asset in allAssets)
        {
            [assetArray addObject:asset];
            if ([asset isKindOfClass:[Asset class]])
            {
                Asset *assetVal = (Asset*)asset;
                if(![self.constantAssetArray containsObject:assetVal.assetID])
                {
                    [self.constantAssetArray addObject:assetVal.assetID];
                }
            }
        }
    }
    assetArray = [self arrayByEliminatingDuplicatesWithoutOrder:assetArray];
    
    [self offlineDownloadForAssetArray:assetArray];
}

-(void)initiateOfflineDownloadForDurations:(NSArray*)durations
{
    self.isPauseOfflineDownloadEnabled = NO;
    
    NSMutableArray *assetArray = [NSMutableArray array];
    self.constantAssetArray = [NSMutableArray array];
    for(EventDuration *duration in durations)
    {
        NSArray *allAssets =  [duration totalAssetForEventDuration];
        for(NSObject *asset in allAssets)
        {
            [assetArray addObject:asset];
            if ([asset isKindOfClass:[Asset class]])
            {
                Asset *assetVal = (Asset*)asset;
                if(![self.constantAssetArray containsObject:assetVal.assetID])
                {
                    [self.constantAssetArray addObject:assetVal.assetID];
                }
            }
        }
    }
    assetArray = [self arrayByEliminatingDuplicatesWithoutOrder:assetArray];
    
    [self offlineDownloadForAssetArray:assetArray];
}

- (NSMutableArray *)arrayByEliminatingDuplicatesWithoutOrder:(NSMutableArray *)array
{
    
    NSArray *uniqueObjects = [[NSSet setWithArray:[array copy]] allObjects];
    
    NSMutableArray* newArray  = [uniqueObjects mutableCopy];
    return newArray;
    
}

//Offline download main function
-(void)offlineDownloadForAssetArray:(NSArray*)assetArray
{
    __strong NSMutableArray* assetsPageDuplicate;
    assetsPageDuplicate = [[NSMutableArray alloc]init];
    [assetsPageDuplicate addObjectsFromArray:assetArray];
    // is it in background
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *initialDownloadDbArray = [[ZMCDManager sharedInstance]getOfflineDownloadArray];
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        
        //If coredata not present
        if (initialDownloadDbArray.count != 0)
        {
            //Download the data which is in progress
            for(OfflineDownload *data in initialDownloadDbArray)
            {
                if(![data.status isEqualToString:keyCompleted])
                {
                    NSArray * assetVal = [[ZMCDManager sharedInstance]getAssetArrayById:data.assetID.stringValue];
                    
                    //Download initiate for failed assets
                    if([data.status isEqualToString:keyCompleted_partialy] || [data.status isEqualToString:keyFailed])
                    {
                        if(assetVal.count > 0 && [[assetVal lastObject] isKindOfClass:[Asset class]])
                        {
                            Asset *asset = (Asset*)[assetVal lastObject];
                            if (self.eventAssetHandler == nil)
                            {
                                self.eventAssetHandler = [[EventAssetHandler alloc]init];
                                self.eventAssetHandler.delegate      = (id <EventAssetHandlerDelegate>)self;
                            }
                            [self.eventAssetHandler updateOfflineDownloadCoreData:asset status:keyIn_progress];
                        }
                    }
                    
                    if (assetVal.count > 0 && [[assetVal lastObject] isKindOfClass:[Asset class]])
                    {
                        Asset *asset = (Asset*)[assetVal lastObject];
                        if(asset != nil && asset.assetID != nil && [self.constantAssetArray containsObject:asset.assetID])
                        {
                            [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
                        }
                    }
                }
            }
            
            //Add assets which are newly added
            for (NSObject *obj in assetsPageDuplicate)
            {
                if([obj isKindOfClass:[EventOnlyAsset class]])
                {
                    EventOnlyAsset *asset = (EventOnlyAsset*)obj;

                    OfflineDownload *data = [[[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[NSNumber numberWithInteger:[asset.eoa_id integerValue]]] lastObject];
                    if ([[[asset.eoa_uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"])
                    {
                        if(data == nil)
                        {
                            [initialDownloadAsset setObject:[NSNumber numberWithInteger:[asset.eoa_id integerValue]] forKey:@"assetID"];
                            [initialDownloadAsset setObject:keyIn_progress forKey:@"status"];
                            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                            
                            //Downloading the Asset added new
                            if(asset != nil)
                            {
                                [self submitRequestToDownloadEventOnlyAsset:asset];
                            }
                        }
                        else
                        {
                            if(![data.status isEqualToString:keyCompleted])
                            {
                                if (self.eventAssetHandler == nil)
                                {
                                    self.eventAssetHandler = [[EventAssetHandler alloc]init];
                                    self.eventAssetHandler.delegate      = (id <EventAssetHandlerDelegate>)self;
                                }
                                if(asset != nil && asset.eoa_id != nil)
                                {
                                    [self.eventOnlyAssetHandler updateOfflineDownloadCoreData:asset status:keyIn_progress];
                                    [self submitRequestToDownloadEventOnlyAsset:asset];
                                }
                            }
                        }
                    }
                    else
                    {
                        [AbbvieLogging logInfo:[NSString stringWithFormat:@"EOA not having URL: %@ and URL: %@",asset.eoa_title,asset.eoa_uri_full]];
                    }
                }
                else if ([obj isKindOfClass:[Asset class]])
                {
                    Asset *asset = (Asset*)obj;
                    OfflineDownload *data = [[[ZMCDManager sharedInstance]getOfflineDownloadArrayById:asset.assetID] lastObject];
                    if ([[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"])
                    {
                        if((asset.assetID != data.assetID) || data == nil)
                        {
                            [initialDownloadAsset setObject:asset.assetID forKey:@"assetID"];
                            [initialDownloadAsset setObject:keyIn_progress forKey:@"status"];
                            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                            
                            //Downloading the Asset added new
                            if(asset != nil)
                            {
                                [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
                            }
                        }
                    }
                }
            }
        }
        //If coredata not present or first download
        else
        {
            for (NSObject *obj in assetsPageDuplicate)
            {
                if([obj isKindOfClass:[EventOnlyAsset class]])
                {
                    EventOnlyAsset *asset = (EventOnlyAsset*)obj;
                    if ([[[asset.eoa_uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"])
                    {
                        [self submitRequestToDownloadEventOnlyAsset:asset];
                    }
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset:%@ uri:%@",asset.eoa_id,asset.eoa_uri_full]];
                }
                else if([obj isKindOfClass:[Asset class]])
                {
                    Asset *asset = (Asset*)obj;
                    if ([[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"]) {
                        [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
                    }
                    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset:%@ uri:%@",asset.assetID,asset.uri_full]];
                }
            }
        }
    });
    
}
@end
