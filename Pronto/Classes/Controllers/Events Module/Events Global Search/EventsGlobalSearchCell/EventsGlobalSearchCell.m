//
//  EventsGlobalSearchCell.m
//  Pronto
//
//  Created by Praveen Kumar on 05/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventsGlobalSearchCell.h"
#import "UIFont+FontDirectory.h"
#import "UIColor+ColorDirectory.h"
#import "UIImage+ImageDirectory.h"
#import "NSDate+DateDirectory.h"
#import "EventCoreDataManager.h"
#import "AssetView.h"
#import "ViewUtils.h"
#import "Constant.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"

#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"

/*
 will have one view which will have label for event only asset
 then we will add as many as asset view as many EOA is there.
 We will create Widthlayout constraint and keep changing.
 
 a. we will hide the container view if it not  session for EOA
 b. we will show container with asset view.
 */

#define DefaultHeightOfAssetViewContainer 25.0
#define DefaultHeightOfLocation 18.0
#define MarginBetweenAssetView  5.0
#define DefaultYPositionOfAssetViewContainer 85.0
@interface EventsGlobalSearchCell()

@property (nonatomic, strong) NSMutableArray *assetButtons;

@end


@implementation EventsGlobalSearchCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setupUI];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self shouldShowAllLabels:YES];
    self.sessionOrEoa   = nil;
    self.eventDuration  = nil;
    self.assetViewContainer.height = DefaultHeightOfAssetViewContainer;
    [self.assetButtons removeAllObjects];
    
    
    self.assetViewContainer.hidden = YES;
    [self removeAssetViewsFromAssetContainerView];
}

- (void)removeAssetViewsFromAssetContainerView
{
    for (UIView *view in self.assetViewContainer.subviews)
    {
        if ([view isKindOfClass:[AssetView class]])
        {
            [view removeFromSuperview];
        }
    }
}

+ (NSString*)CellIdentifier
{
    return @"EventsGlobalSearchCell";
}

+ (NSString*)NibName
{
    return @"EventsGlobalSearchCell";
}

+ (CGFloat)DefaultHeightOfCell
{
    return 90.0;
}

- (void)setupUI
{
    self.date_timeLabel.font = [UIFont FontLatoRegularSize11];
    self.titleLabel.font = [UIFont FontLatoRegularSize16];
    self.locationLabel.font = [UIFont FontLatoRegularSize12];
    self.eventOnlyAssetLabel.font = [UIFont FontLatoRegularSize14];
    
    self.date_timeLabel.textColor = [UIColor GetColorFromHexValue999897];
    self.titleLabel.textColor = [UIColor GetColorFromHexValue2d2926];
    self.eventOnlyAssetLabel.textColor = [UIColor GetColorFromHexValue2d2926];
    self.locationLabel.textColor = [UIColor GetColorFromHexValue605E5C];
    
    self.locationImage.image = [UIImage GetCalendarDetailLocationIcon];
    
}

- (void)prepareCellWithEvent:(Event*)event  andOtherValues:(NSObject*)sessionOrEoa
{
    [self shouldShowAllLabels:YES];
    self.sessionOrEoa = sessionOrEoa;
    
    if ([sessionOrEoa isKindOfClass:[EventOnlyAsset class]])
    {
        [self shouldPrepareCellForEventOnlyAsset:YES];
        
        EventOnlyAsset *eventOnlyAsset = (EventOnlyAsset*)sessionOrEoa;
        
        self.date_timeLabel.text    = eventOnlyAsset.eoa_title;
        self.titleLabel.text        = @"Event only asset description";
        
        self.locationLabel.hidden  = YES;
        self.locationImage.hidden  = YES;
    }
    else if ([sessionOrEoa isKindOfClass:[Session class]])
    {
        [self shouldPrepareCellForEventOnlyAsset:NO];
        
        Session *session = (Session*)sessionOrEoa;
        NSArray *eventDurations = [session getEventDurationForCurrentSession];
        if (eventDurations != nil && eventDurations.count > 0)
        {
            // SHOWING FIRST ONE
            EventDuration *eventDuration = (EventDuration*)[eventDurations lastObject];
            NSDate *sessionDate = eventDuration.eventDate;
            NSString *dateString = [NSDate getFormattedDateStringForGlobalSearchFromDate:sessionDate];
            NSString *weekDay    = [NSDate GetWeekdayInEEEEFormateForDate:sessionDate];
            
            NSString *dateTitle  = [NSString stringWithFormat:@"%@, %@ | %@ - %@ %@", weekDay, dateString,eventDuration.startTime, eventDuration.endTime, eventDuration.parentEvent.eventTimezone];
            
            self.date_timeLabel.text = dateTitle;
            self.titleLabel.text     = session.title;
            self.locationLabel.text  = [session getLocationName];
            
            CGRect frame = self.assetViewContainer.frame;
            frame.origin.y = DefaultYPositionOfAssetViewContainer;
            if(self.locationLabel.text.length <= 0)
            {
                self.locationImage.height = 0;
        
                frame.origin.y = frame.origin.y - DefaultHeightOfAssetViewContainer;
            }
            else
            {
                self.locationImage.height = DefaultHeightOfLocation;
            }
            self.assetViewContainer.frame = frame;
            
            self.eventDuration       = eventDuration;
            
            NSArray *eoasFoundInGlobalSearch = [[EventCoreDataManager sharedInstance] getAllEOAsFoundInGlobalSearchForSession: session];
            NSArray *assetsFoundInGlobalSearch = [[EventCoreDataManager sharedInstance] getAllAssetsFoundInGlobalSearchForSession: session];
            
            NSMutableArray *totalAssetsInGlobalSearch = [NSMutableArray arrayWithArray:eoasFoundInGlobalSearch];
            [totalAssetsInGlobalSearch addObjectsFromArray:assetsFoundInGlobalSearch];
            
            if (totalAssetsInGlobalSearch != nil &&
                totalAssetsInGlobalSearch.count > 0)
            {
                if(totalAssetsInGlobalSearch.count > 1)
                {
                    self.eventOnlyAssetLabel.text = NSLocalizedString(kAssetsAssociated, kAssetsAssociated);
                }
                else
                {
                    self.eventOnlyAssetLabel.text = NSLocalizedString(kAssetAssociated, kAssetAssociated);
                }
                
                for(NSInteger count=0; count < totalAssetsInGlobalSearch.count; count++)
                {
                    CGRect frame = CGRectMake(0, (AssetView.DefaultHeight * (count+1) + MarginBetweenAssetView), self.assetViewContainer.frame.size.width, AssetView.DefaultHeight);
                    
                    AssetView *assetView = [AssetView GetAssetViewWithFrame:frame andAsset:totalAssetsInGlobalSearch[count]];
                    assetView.button.tag = count;
                    assetView.delegate = (id<EventsAssetViewDelegate>)self;;
                    [self.assetViewContainer addSubview:assetView];
                    
                    [self.assetButtons addObject:assetView];
                }
                self.assetViewContainer.height = DefaultHeightOfAssetViewContainer + (AssetView.DefaultHeight * totalAssetsInGlobalSearch.count);
                self.assetViewContainer.hidden = NO;
            }
            else
            {
                self.assetViewContainer.hidden = YES;
            }
        }
    }
}


- (void)shouldShowAllLabels:(BOOL)show
{
    if (show)
    {
        self.date_timeLabel.hidden = NO;
        self.titleLabel.hidden = NO;
        self.locationLabel.hidden = NO;
        self.locationImage.hidden = NO;
    }
    else
    {
        self.date_timeLabel.hidden = YES;
        self.titleLabel.hidden = YES;
        self.locationLabel.hidden = YES;
        self.locationImage.hidden = YES;
    }
}

- (void)shouldPrepareCellForEventOnlyAsset:(BOOL)prepareForEoa
{
    if (prepareForEoa == YES)
    {
        self.date_timeLabel.font      = [UIFont FontLatoRegularSize16];
        self.date_timeLabel.textColor = [UIColor GetColorFromHexValue2d2926];
        
        self.titleLabel.font      = [UIFont FontLatoRegularSize12];
        self.titleLabel.textColor = [UIColor GetColorFromHexValue605E5C];
    }
    else
    {
        self.date_timeLabel.font = [UIFont FontLatoRegularSize11];
        self.titleLabel.font = [UIFont FontLatoRegularSize16];
        self.locationLabel.font = [UIFont FontLatoRegularSize12];
        
        self.date_timeLabel.textColor = [UIColor GetColorFromHexValue999897];
        self.titleLabel.textColor = [UIColor GetColorFromHexValue2d2926];
        self.locationLabel.textColor = [UIColor GetColorFromHexValue605E5C];
        
        self.locationImage.image = [UIImage GetCalendarDetailLocationIcon];
    }
}

- (CGFloat)getHeightOfCellForSession:(Session*)session
{
    CGFloat totalHeight = [EventsGlobalSearchCell DefaultHeightOfCell];
    
    CGFloat heightToIncrease = 0;
    
    NSArray *eoasFoundInGlobalSearch = [[EventCoreDataManager sharedInstance] getAllEOAsFoundInGlobalSearchForSession: session];
    NSArray *assetsFoundInGlobalSearch = [[EventCoreDataManager sharedInstance] getAllAssetsFoundInGlobalSearchForSession: session];
    
    NSMutableArray *totalAssetsInGlobalSearch = [NSMutableArray arrayWithArray:eoasFoundInGlobalSearch];
    [totalAssetsInGlobalSearch addObjectsFromArray:assetsFoundInGlobalSearch];
    
    if (totalAssetsInGlobalSearch != nil &&
        totalAssetsInGlobalSearch.count > 0)
    {
        heightToIncrease = heightToIncrease + DefaultHeightOfAssetViewContainer;
        heightToIncrease = heightToIncrease + [AssetView DefaultHeight] * totalAssetsInGlobalSearch.count;
    }
    
    //For Location
    if([session getLocationName].length > 0)
    {
        heightToIncrease = heightToIncrease + DefaultHeightOfLocation;
    }
    else
    {
        heightToIncrease = heightToIncrease - MarginBetweenAssetView;
    }
    
    totalHeight += heightToIncrease;
    
    return totalHeight;
}

- (IBAction)cellSelectionAction:(UIButton*)sender
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(didSelectCell:)])
    {
        [self.delegate didSelectCell:self];
    }
}

#pragma mark - EventsAssetViewDelegate
- (void)assetView:(AssetView*)view  didTapAssetButton:(UIButton*)sender
{    
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(cell:didSelectAssetView:)])
    {
        [self.delegate cell:self didSelectAssetView:view];
    }
}

@end
