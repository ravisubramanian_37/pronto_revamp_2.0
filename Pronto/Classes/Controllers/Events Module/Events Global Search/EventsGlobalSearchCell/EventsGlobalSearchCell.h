//
//  EventsGlobalSearchCell.h
//  Pronto
//
//  Created by Praveen Kumar on 05/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EventsGlobalSearchCell;
@class Event;
@class EventDuration;
@class Session;
@class EventsGlobalSearchCell;
@class AssetView;


@protocol EventsGSCellDelegate<NSObject>

- (void)didSelectCell:(EventsGlobalSearchCell*)cell;
- (void)cell:(EventsGlobalSearchCell*)cell didSelectAssetView:(AssetView*)assetView;

@end

@interface EventsGlobalSearchCell : UITableViewCell

+ (NSString*)CellIdentifier;
+ (NSString*)NibName;
+ (CGFloat)DefaultHeightOfCell;

@property (weak, nonatomic) IBOutlet UILabel *date_timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImage;
@property (weak, nonatomic) IBOutlet UIView *bottomSeperator;
@property (weak, nonatomic) IBOutlet UIView *assetViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *eventOnlyAssetLabel;


@property (nonatomic, strong) NSObject *sessionOrEoa;
@property (nonatomic, strong) EventDuration *eventDuration;

- (void)prepareCellWithEvent:(Event*)event andOtherValues:(NSObject*)sessionOrEoa;
- (CGFloat)getHeightOfCellForSession:(Session*)session;

@property (nonatomic, weak) id <EventsGSCellDelegate> delegate;

- (IBAction)cellSelectionAction:(UIButton*)sender;

@end
