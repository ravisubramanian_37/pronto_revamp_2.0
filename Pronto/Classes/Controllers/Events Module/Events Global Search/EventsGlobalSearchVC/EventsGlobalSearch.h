//
//  EventsGlobalSearch.h
//  Pronto
//
//  Created by Praveen Kumar on 25/09/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsGlobalSearchViewModel.h"

@class EventsGlobalSearch;
@class EventDuration;
@class EventOnlyAsset;
@class Asset;

@protocol EventsGSDelegate<NSObject>

- (void)eventsGlobalSearchViewController:(EventsGlobalSearch*)eventsGlobalSearchVC didSelectEOA:(EventOnlyAsset*)eventOnlyAsset;
- (void)eventsGlobalSearchViewController:(EventsGlobalSearch*)eventsGlobalSearchVC didSelectAsset:(Asset*)asset;
- (void)eventsGlobalSearchViewController:(EventsGlobalSearch*)eventsGlobalSearchVC didSelectItem:(NSObject*)sessionOrEoa andEventDuration:(EventDuration*)eventDuration;


@end

@interface EventsGlobalSearch : UIViewController

+ (EventsGlobalSearch*)GetEventsGSViewController;

- (void)reloadTableView;

@property (nonatomic, weak) id <EventsGSDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *eventsGSTableView;
@property (nonatomic, strong) NSArray *dataSourceFromGlobalSearchViewModal;
@property (nonatomic, strong) EventsGlobalSearchViewModel *eventsGSViewModel;



@end
