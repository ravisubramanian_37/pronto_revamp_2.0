//
//  EventsGlobalSearch.m
//  Pronto
//
//  Created by Praveen Kumar on 25/09/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventsGlobalSearch.h"
#import "EventsGlobalSearchCell.h"
#import "UIColor+ColorDirectory.h"
#import "UIImage+ImageDirectory.h"
#import "UIFont+FontDirectory.h"
#import "Pronto-Swift.h"
#import "EventGlobalSearchHeaderView.h"
#import "AssetView.h"
#import "Asset.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

static NSString * const EventsGSStoryboardName_Identifier = @"EventsGlobalSearch";

@interface EventsGlobalSearch ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation EventsGlobalSearch

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupUI];
}

+ (EventsGlobalSearch*)GetEventsGSViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:EventsGSStoryboardName_Identifier bundle:nil];
    EventsGlobalSearch *evenstGSVC = [sb instantiateViewControllerWithIdentifier:EventsGSStoryboardName_Identifier];
    return evenstGSVC;
}

-(void) setupUI
{
    self.eventsGSTableView.dataSource = (id<UITableViewDataSource>)self;
    self.eventsGSTableView.delegate   = (id<UITableViewDelegate>)self;
    self.eventsGSTableView.backgroundColor = [UIColor clearColor];
    [self.eventsGSTableView registerNib:[UINib nibWithNibName:[EventsGlobalSearchCell NibName] bundle:nil] forCellReuseIdentifier:[EventsGlobalSearchCell CellIdentifier]];
    self.eventsGSViewModel = [[EventsGlobalSearchViewModel alloc]initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)self];
    [self.eventsGSViewModel prepareEventGlobalResultsWithGlobalSearchResult:(NSMutableArray*)[self.dataSourceFromGlobalSearchViewModal mutableCopy]];
}

- (void)reloadTableView
{
    [self.eventsGSViewModel prepareEventGlobalResultsWithGlobalSearchResult:(NSMutableArray*)[self.dataSourceFromGlobalSearchViewModal mutableCopy]];
    [self.eventsGSTableView reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource and UITableViewDelegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.eventsGSViewModel.eventsGlobalSearchDataSource.count;
}

//Setting up header with the expected UI in form of .xib file
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    EventGlobalSearchHeaderView *eventGlobalSearchHeaderView = [EventGlobalSearchHeaderView GetEventGlobalSearchHeaderView];
    eventGlobalSearchHeaderView.delegate = (id<EventGlobalSearchHeaderViewDelegate>)self;
    
    [eventGlobalSearchHeaderView prepareHeaderViewWithData:self.eventsGSViewModel.eventsGlobalSearchDataSource andSection:section];
    
    //Showing Arrow imange
     NSString *key = [self.eventsGSViewModel getKeyFromDataSourceForSection:section forData:self.eventsGSViewModel.eventsGlobalSearchDataSource];
    
    if([self.eventsGSViewModel.eventsActiveArray containsObject:key])
    {
        [eventGlobalSearchHeaderView shouldShowArrowImageFacingDownwards:YES];
    }
    else
    {
        [eventGlobalSearchHeaderView shouldShowArrowImageFacingDownwards:NO];
    }
    
    return eventGlobalSearchHeaderView;
}

//Shrink and Expand Table View on header selection
- (void)eventGlobalSearchHeaderView:(EventGlobalSearchHeaderView*)eventGlobalSearchHeaderView didTapHeader:(UIButton*)sender
{
    NSInteger section = sender.tag;
    NSArray *arrayForSection = [self.eventsGSViewModel getValueFromDataSourceForSection:section forData:self.eventsGSViewModel.eventsGlobalSearchDataSource];
    
    if (arrayForSection != nil && arrayForSection.count > 0)
    {
        NSMutableArray * indexPathArray = [NSMutableArray array];
        //Chnages for Warnings
        NSIndexPath * indexPath = nil;
        
        NSString *key = [self.eventsGSViewModel getKeyFromDataSourceForSection:section forData:self.eventsGSViewModel.eventsGlobalSearchDataSource];
        
        for(int i=0; i<arrayForSection.count; i++)
        {
            indexPath = [NSIndexPath indexPathForItem:i inSection:section];
            [indexPathArray addObject:indexPath];
        }
        
        if(![self.eventsGSViewModel.eventsActiveArray containsObject:key])
        {
            [self.eventsGSViewModel.eventsActiveArray addObject:key];
            [self.eventsGSTableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationFade];
            [eventGlobalSearchHeaderView shouldShowArrowImageFacingDownwards:YES];
        }
        else
        {
            [self.eventsGSViewModel.eventsActiveArray removeObject:key];
            [self.eventsGSTableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationFade];
            [eventGlobalSearchHeaderView shouldShowArrowImageFacingDownwards:NO];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *value = [self.eventsGSViewModel getValueFromDataSourceForSection:section forData:self.eventsGSViewModel.eventsGlobalSearchDataSource];
    
    NSString *key = [self.eventsGSViewModel getKeyFromDataSourceForSection:section forData:self.eventsGSViewModel.eventsGlobalSearchDataSource];
    
    if(![self.eventsGSViewModel.eventsActiveArray containsObject:key])
    {
        return 0;
    }
    return value.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventsGlobalSearchCell *cell = (EventsGlobalSearchCell*)[tableView dequeueReusableCellWithIdentifier:[EventsGlobalSearchCell CellIdentifier]];
    
    NSArray *values = [self.eventsGSViewModel getValueFromDataSourceForSection:indexPath.section forData:self.eventsGSViewModel.eventsGlobalSearchDataSource];
    
    CGFloat heightOfCell = 0.0;
    
    if (values.count > 0 && indexPath.row <= values.count)
    {
        Session *session = values[indexPath.row];
        if (session != nil &&
            [session isKindOfClass:[Session class]])
        {
            heightOfCell =  [cell getHeightOfCellForSession:session];
        }
    }
    
    return heightOfCell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventsGlobalSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:[EventsGlobalSearchCell CellIdentifier]];
    cell.delegate = (id<EventsGSCellDelegate>)self;
    
    NSArray *values = [self.eventsGSViewModel getValueFromDataSourceForSection:indexPath.section forData:self.eventsGSViewModel.eventsGlobalSearchDataSource];
    
    if (values.count > 0 && indexPath.row <= values.count)
    {
        [cell prepareCellWithEvent:nil andOtherValues:values[indexPath.row]];
        cell.delegate        = (id<EventsGSCellDelegate>)self;
    }

    return cell;
}

- (void)didSelectCell:(EventsGlobalSearchCell*)cell
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventsGlobalSearchViewController:didSelectItem: andEventDuration:)])
    {
        [self.delegate eventsGlobalSearchViewController:self didSelectItem:cell.sessionOrEoa andEventDuration:cell.eventDuration];
    }
}

- (void)cell:(EventsGlobalSearchCell*)cell didSelectAssetView:(AssetView*)assetView
{
    if (self.delegate != nil &&
        [self.delegate respondsToSelector:@selector(eventsGlobalSearchViewController:didSelectEOA:)]
        && [assetView.associatedAsset isKindOfClass:[EventOnlyAsset class]])
    {
        [self.delegate eventsGlobalSearchViewController:self didSelectEOA:(EventOnlyAsset*)assetView.associatedAsset];
    }
    else if (self.delegate != nil &&
             [self.delegate respondsToSelector:@selector(eventsGlobalSearchViewController:didSelectAsset:)]
             && [assetView.associatedAsset isKindOfClass:[Asset class]])
    {
        [self.delegate eventsGlobalSearchViewController:self didSelectAsset:(Asset*)assetView.associatedAsset];
    }
}

@end
