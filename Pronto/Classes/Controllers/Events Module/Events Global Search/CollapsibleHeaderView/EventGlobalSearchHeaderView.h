//
//  EventGlobalSearchHeaderView.h
//  Pronto
//
//  Created by Praveen Kumar on 11/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EventGlobalSearchHeaderView;

@protocol EventGlobalSearchHeaderViewDelegate <NSObject>

-(void)eventGlobalSearchHeaderView:(EventGlobalSearchHeaderView*)eventGlobalSearchHeaderView didTapHeader:(UIButton*)sender;

@end

@interface EventGlobalSearchHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *bottomBorderView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;
@property (weak, nonatomic) IBOutlet UIButton *headerButton;

+ (NSString*)GetEventGlobalSearchHeaderNibName;
+ (EventGlobalSearchHeaderView*)GetEventGlobalSearchHeaderView;

- (void) shouldShowArrowImageFacingDownwards:(BOOL) show;
- (void) prepareHeaderViewWithData:(NSMutableArray*)data andSection:(NSInteger)section;

@property (nonatomic, assign) id <EventGlobalSearchHeaderViewDelegate> delegate;

@end
