//
//  EventGlobalSearchHeaderView.m
//  Pronto
//
//  Created by Praveen Kumar on 11/10/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventGlobalSearchHeaderView.h"
#import "UIColor+ColorDirectory.h"
#import "UIImage+ImageDirectory.h"
#import "UIFont+FontDirectory.h"
#import "EventsGlobalSearchViewModel.h"

@interface EventGlobalSearchHeaderView ()

@property (nonatomic, strong) EventsGlobalSearchViewModel *eventsGSViewModel;

@end

@implementation EventGlobalSearchHeaderView

+ (NSString*)GetEventGlobalSearchHeaderNibName
{
    return @"EventGlobalSearchHeaderView";
}

+ (EventGlobalSearchHeaderView*)GetEventGlobalSearchHeaderView
{
    return (EventGlobalSearchHeaderView*)[[[NSBundle mainBundle] loadNibNamed:[self GetEventGlobalSearchHeaderNibName] owner:self options:nil] lastObject];
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (IBAction)buttonAction:(id)sender {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(eventGlobalSearchHeaderView:didTapHeader:)])
    {
        [self.delegate eventGlobalSearchHeaderView:self didTapHeader:sender];
    }
}

- (void) setupUI
{
    self.headerView.backgroundColor = [UIColor GetColorFromHexValueFFFFFF];
    self.bottomBorderView.backgroundColor = [UIColor GetColorFromHexValueDCE4EE];
    [self.arrowImage setImage:[UIImage GetArrowDownIcon]];
    self.titleLabel.font = [UIFont FontLatoRegularSize16];
    self.titleLabel.textColor = [UIColor GetColorFromHexValue000000];
    self.eventsGSViewModel = [[EventsGlobalSearchViewModel alloc]initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)self];
}

- (void)prepareHeaderViewWithData:(NSMutableArray*)data andSection:(NSInteger)section
{
    NSString *key = [self.eventsGSViewModel getKeyFromDataSourceForSection:section forData:data];
    NSArray *values = [self.eventsGSViewModel getValueFromDataSourceForSection:section forData:data];
    
    if (values != nil && values.count > 0)
    {
        self.titleLabel.text = [NSString stringWithFormat:@"%@ (%02lu)", key, (unsigned long)values.count];
    }
    else
    {
        self.titleLabel.text = [NSString stringWithFormat:@"%@ ", key];
    }
    
    NSRange range = [self.titleLabel.text rangeOfString:[NSString stringWithFormat:@"%@ ",key]];
    NSMutableAttributedString *attText = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text];
    [attText setAttributes:@{NSFontAttributeName:[UIFont FontLatoBoldSize16]}
                     range:range];
    self.titleLabel.attributedText = attText;
    self.headerButton.tag = section;
}

- (void) shouldShowArrowImageFacingDownwards:(BOOL) show
{
    if(show == YES)
    {
        [self.arrowImage setImage:[UIImage GetArrowDownIcon]];
    }
    else
    {
        [self.arrowImage setImage:[UIImage GetArrowRightIcon]];
    }
}

@end
