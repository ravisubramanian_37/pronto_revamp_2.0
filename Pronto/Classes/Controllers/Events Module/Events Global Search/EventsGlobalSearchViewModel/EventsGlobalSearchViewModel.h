//
//  EventsGlobalSearchViewModel.h
//  Pronto
//
//  Created by Praveen Kumar on 25/09/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Filter;

@protocol EventsGlobalSearchViewModelDelegate <NSObject>

@end

@interface EventsGlobalSearchViewModel : NSObject

- (void)prepareDataSourceWithAssets:(NSArray*)assets;

/**
    It holds the grouped events/Session/EventOnlyAsset according to their Event/parent event/Franchise.
    It is array which contains dictionary and dictionary contains one key and arrays values.
 */
@property (nonatomic, strong) NSMutableArray *eventsGlobalSearchDataSource;

/**
    It contains title of Events/Franchise for which group of exist.
 */
@property (nonatomic, strong) NSMutableArray *eventsActiveArray;
@property(nonatomic, assign) id<EventsGlobalSearchViewModelDelegate> EventsGlobalSearchVMDelegate;
@property (nonatomic, strong) Filter *currentFilter;

-(NSString*)getKeyFromDataSourceForSection:(NSInteger)section forData:(NSMutableArray*)data;
-(NSArray*)getValueFromDataSourceForSection:(NSInteger)section forData:(NSMutableArray*)data;
- (NSInteger)getCountForEventsGlobalSearchHeader;

- (instancetype)initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)delegate;
- (instancetype)initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)delegate andSearchResults:(NSMutableArray*)searchResults;

- (void)prepareEventGlobalResultsWithGlobalSearchResult:(NSMutableArray*)searchResults;

@end
