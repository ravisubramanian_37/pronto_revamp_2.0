//
//  EventsGlobalSearchViewModel.m
//  Pronto
//
//  Created by Praveen Kumar on 25/09/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventsGlobalSearchViewModel.h"
#import "Filter.h"
#import "Constant.h"
#import "Asset.h"
#import "EventCoreDataManager.h"
#import "NSDate+DateDirectory.h"

#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"



@implementation EventsGlobalSearchViewModel


/*
 Requirement:
    Event will not be selectable, Session and EOA will be selectable.
    Session and EOA will be grouped under same Events.
 */

/*
 
 Logic: whenever we will find any EOA and we will keep it in
 one array in event core data manager class.
 
 We will get all those session from which EOA belongs and
 add it to our Data structure.
 While showing session, we will check that if session EOA belongs
 to core data manager class we will add it assest view in cell.
 
 */

- (void)prepareEventGlobalResultsWithGlobalSearchResult:(NSMutableArray*)searchResults
{
    [self.eventsGlobalSearchDataSource removeAllObjects];
    [[EventCoreDataManager sharedInstance] resetEventOnlyAssets];
    [[EventCoreDataManager sharedInstance] resetAssets];
    
    NSMutableArray *tempArray = [NSMutableArray array];
    
    NSMutableDictionary *groupingOfSearchResults = [NSMutableDictionary dictionary];
    NSMutableArray *tempEventOnlyAssets = [NSMutableArray array];
    NSMutableArray *tempAssets = [NSMutableArray array];
    
    for (NSObject *item in searchResults)
    {
        [tempArray removeAllObjects];
        
        if ([item isKindOfClass:[Event class]])
        {
            Event *event = (Event*)item;
            if ([[groupingOfSearchResults allKeys] containsObject:event.title] == NO)
            {
                if(event.title != nil)
                {
                    [groupingOfSearchResults setObject:@[] forKey:event.title];
                }
            }
        }
        else if ([item isKindOfClass:[Session class]])
        {
            Session *session   = (Session*)item;
            NSArray *events    = [session getAllEventsOfCurrentSession];
            //Session will be shown in all the events it belong to
            for (Event *event in events)
            {
                if ([[groupingOfSearchResults allKeys] containsObject:event.title])
                {
                    [tempArray addObjectsFromArray:groupingOfSearchResults[event.title]];
                }
                
                //adding session
                [tempArray addObject:session];
               
                //Keeping unique session
                NSSet *uniqueEvent = [NSSet setWithArray:tempArray];
                [tempArray removeAllObjects];
                [tempArray addObjectsFromArray:[uniqueEvent allObjects]];
                
                NSArray *eventsWithTitle = [NSArray arrayWithArray:tempArray];
                if(event.title != nil)
                {
                    [groupingOfSearchResults setObject:eventsWithTitle forKey:event.title];
                }
                //Clearing all the session added for the event
                [tempArray removeAllObjects];
            }
        }
        else if ([item isKindOfClass:[EventOnlyAsset class]])
        {
            
            EventOnlyAsset *eventOnlyAsset = (EventOnlyAsset*)item;
            
            for (Session *session in [eventOnlyAsset.eoa_sessions allObjects])
            {
                NSArray *events    = [session getAllEventsOfCurrentSession];
                //Session will be shown in all the events it belong to
                for (Event *event in events)
                {
                    if ([[groupingOfSearchResults allKeys] containsObject:event.title])
                    {
                        [tempArray addObjectsFromArray:groupingOfSearchResults[event.title]];
                    }
                
                    //adding session
                    [tempArray addObject:session];
                    
                    //Keeping unique session
                    NSSet *uniqueEvent = [NSSet setWithArray:tempArray];
                    [tempArray removeAllObjects];
                    [tempArray addObjectsFromArray:[uniqueEvent allObjects]];
                    
                    NSArray *eventsWithTitle = [NSArray arrayWithArray:tempArray];
                    if(event.title != nil)
                    {
                        [groupingOfSearchResults setObject:eventsWithTitle forKey:event.title];
                    }
                    //Clearing all the EOA added for the event
                    [tempArray removeAllObjects];
                }
            }
            
            //This reference is being kept for searching result
            [tempEventOnlyAssets addObject:eventOnlyAsset];
            
        }
        
        else if ([item isKindOfClass:[Asset class]])
        {
            
            Asset *asset = (Asset*)item;
            //This reference is being kept for searching result
            [tempAssets addObject:asset];
        }
    }
    
    //Sorting inner sessions objects.
    NSDictionary *sortedSearchedResults = [self sortEventGlobalSearchResult:groupingOfSearchResults];
    
    //Sorting keys.
    NSMutableArray *sortedKey = (NSMutableArray*)[[sortedSearchedResults allKeys] mutableCopy];
    [sortedKey sortUsingComparator:^NSComparisonResult(NSString *key1, NSString *key2) {
        
        NSComparisonResult result = [key1 compare:key2 options:NSNumericSearch | NSCaseInsensitiveSearch];
        
        if (result == -1)
        {
            return NSOrderedAscending;
        }
        else if (result == 1)
        {
            return NSOrderedDescending;
        }
        return NSOrderedSame;
        
    }];
    
    //Sorting can be done on title and inner array
    NSMutableArray *orderedDataStructure = [NSMutableArray array];
    //Creating ordered Group of search of results.
    for (NSString *key in sortedKey)
    {
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:sortedSearchedResults[key],key, nil];
        [orderedDataStructure addObject:dict];
    }
    
    //Setting actual data structures.
    self.eventsGlobalSearchDataSource = orderedDataStructure;
    [[EventCoreDataManager sharedInstance] setEventOnlyAssetsFromGlobalSearch:tempEventOnlyAssets];
    [[EventCoreDataManager sharedInstance] setAssetsFromGlobalSearch:tempAssets];
}

- (NSDictionary*)sortEventGlobalSearchResult:(NSDictionary*)searchResult
{
    NSMutableDictionary *sortedSearchResults = [NSMutableDictionary dictionary];
    for (NSString *key in searchResult)
    {
        NSMutableArray *tempSortedArray = [NSMutableArray arrayWithArray:searchResult[key]];
        
        [tempSortedArray sortUsingComparator:^NSComparisonResult(Session *session1, Session *session2) {
            
            NSComparisonResult result = [session1.title compare:session2.title options:NSNumericSearch | NSCaseInsensitiveSearch];
            
            if (result == -1)
            {
                return NSOrderedAscending;
            }
            else if (result == 1)
            {
                return NSOrderedDescending;
            }
            return NSOrderedSame;
            
        }];
        
        [sortedSearchResults setObject:tempSortedArray forKey:key];
    }
    
    return sortedSearchResults;
}

- (instancetype)initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)delegate andSearchResults:(NSMutableArray*)searchResults
{
    if (self = [super init])
    {
        self.EventsGlobalSearchVMDelegate  = delegate;
        self.currentFilter = [[Filter alloc]init];
    }
    return self;
}

- (instancetype)initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)delegate
{
    if (self = [super init])
    {
        self.EventsGlobalSearchVMDelegate      = delegate;
        [self initializeEventsGlobalSearchDataSource];
        self.currentFilter             = [[Filter alloc]init];
    }
    return self;
}

- (void)initializeEventsGlobalSearchDataSource
{
    self.eventsGlobalSearchDataSource = [NSMutableArray array];
    self.eventsActiveArray = [NSMutableArray array];
}

- (void)prepareDataSourceWithAssets:(NSArray*)assets
{
    //Prepare the values from the asset array obtained for events
    
    //remove when deleting mock data and get the values from the asset array
    for(int i=0;i<assets.count;i++)
    {
        NSArray *eventObjects = [assets[i] allKeys];
        if(eventObjects != nil && eventObjects.count > 0)
        {
            [self.eventsActiveArray addObject:[eventObjects lastObject]];
        }
    }
}

-(NSString*)getKeyFromDataSourceForSection:(NSInteger)section forData:(NSMutableArray*)data
{
    NSArray *eventObjects = [data[section] allKeys];
    if(eventObjects != nil && eventObjects.count > 0)
    {
        return [eventObjects lastObject];
    }
    return @"";
}

-(NSArray*)getValueFromDataSourceForSection:(NSInteger)section forData:(NSMutableArray*)data
{
    NSArray *sectionObjects = [data[section] allValues];
    if(sectionObjects != nil && sectionObjects.count > 0)
    {
        return [sectionObjects lastObject];
    }
    return @[];
}

- (NSInteger)getCountForEventsGlobalSearchHeader
{
    NSInteger count = 0;
    
    for (NSDictionary *dict in self.eventsGlobalSearchDataSource)
    {
        NSString *key  = [[dict allKeys] lastObject];
        NSArray *values = dict[key];
        if (values.count > 0)
        {
            count+= values.count;
        }
        else
        {
            count++;
        }
    }

    return count;
}


@end
