//
//  EventListViewController.h
//  Pronto
//
//  Created by m-666346 on 27/07/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EventListViewModel;
@class EventListViewController;
@class EventsViewController;
@class Event;

@protocol EventListViewControllerDelegate<NSObject>

- (void)eventListViewController:(EventListViewController*)viewController didSelectIndex:(NSInteger)inex;

@end

@interface EventListViewController : UIViewController
@property (nonatomic, strong) EventListViewModel *eventListViewModel;
@property (nonatomic, weak) id <EventListViewControllerDelegate> delegate;

// It has been created to store the current selected events from the list
@property (nonatomic, weak) EventsViewController *parentVC;

@property (nonatomic, strong) Event *selectedEvent;

+ (EventListViewController*)GetEventListViewController;

/*
 Refreshing Events local database in these many scenario. belows
 a. app is launched
 b. refresh button is tapped.
 c. on app logout an login and come to events tab
 d. if local database is empty
 f. app goes to background and comes to foreground. -> TBD. Not implemented yet.
 */
- (void)didTapRefreshButton;
- (void)showActivityIndicatorView:(BOOL)show;
- (void)updateViewOnPreferenceChanges;
- (void)showViewWhenDataSourceIsEmpty:(BOOL)show;
- (void)updateDatasourceFromLocalDB;

@end
