//
//  EventListViewController.m
//  Pronto
//
//  Created by m-666346 on 27/07/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventListViewController.h"
#import "Pronto-Swift.h"
#import "ViewUtils.h"
#import "iCarousel.h"
#import "EventsCardView.h"
#import "AppUtilities.h"
#import "EventListViewModel.h"
#import "ZMEmpty.h"
#import "UIColor+ColorDirectory.h"
#import "EventsCalendarDetailsScreenVC.h"
#import "NSDate+DateDirectory.h"
#import "Franchise.h"
#import "ZMUserActions.h"
#import "ZMAbbvieAPI.h"
#import "ViewUtils.h"
#import "EventsViewController.h"
#import "Constant.h"
#import "ViewUtils.h"
#import "EventsViewController.h"
#import "Constant.h"
#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"
#import "ProntoUserDefaults.h"


static NSUInteger kCardViewWidth    = 464;
static NSUInteger kCardViewHeight   = 464;

@interface EventListViewController ()

@property (nonatomic, strong) IBOutlet iCarousel *cardView;
@property (nonatomic, strong) ZMEmpty *emptyView;
@property (strong, nonatomic) UIView *indicatorView;
@property (strong, nonatomic) UIActivityIndicatorView * activityIndicatorView;



@end

@implementation EventListViewController

+ (EventListViewController*)GetEventListViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"EventListScene"
                                                 bundle:nil];
    EventListViewController *eventListVC = [sb instantiateViewControllerWithIdentifier:@"EventListViewController"];
    return eventListVC;
}

- (void)setupUI
{
    self.cardView.type = iCarouselTypeCustom;
    self.cardView.clipsToBounds = YES;
    
    self.cardView.dataSource = (id<iCarouselDataSource>)self;
    self.cardView.delegate   = (id<iCarouselDelegate>)self;
    
    self.view.backgroundColor       = [UIColor GetColorFromHexValuef2f2f2];
    self.cardView.backgroundColor   = [UIColor GetColorFromHexValuef2f2f2];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
    [self setupViewsFrames];
    
    self.eventListViewModel = [[EventListViewModel alloc]init];
    self.eventListViewModel.parentViewController = self;
    [self.eventListViewModel initializeDatasource];
    
    //Eventlist should get refreshed every time view did load happens
    [ZMUserActions sharedInstance].shoudRefreshEventData = YES;
    
}

- (void)setupViewsFrames
{
    //Setting up Card views
    [self setupCardViewFrame];
    
    //Setting activity indicator view
    [self updateActivityIndicatorCenter];
}

- (void)setupCardViewFrame
{
    CGFloat xAxis = (self.view.width - kCardViewWidth) / 6.0; //
    self.cardView.frame = CGRectMake(xAxis, ((self.view.height - kCardViewHeight) / 2), self.view.width - (2 * xAxis), kCardViewHeight);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setupViewsFrames];
    [self.cardView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupViewsFrames];
    [self.cardView reloadData];
    
  if ([HomeViewModel.sharedInstance.flowType  isEqual: @"events"]) {
    if ([self.eventListViewModel isDataSourceEmpty] == YES) // It means there is event but for current franchise selection, No events
    {
      [self.eventListViewModel.parentViewController showViewWhenDataSourceIsEmpty:YES];
    }
    else
    {
      [self.eventListViewModel.parentViewController showViewWhenDataSourceIsEmpty:NO];
    }
  } else {
    if ([ZMUserActions sharedInstance].shoudRefreshEventData == YES)
    {
      [ZMUserActions sharedInstance].shoudRefreshEventData = NO;
      
      [self.eventListViewModel makeEventServiceRequestWithCMS:NULL];
    }
  }
    
    BOOL fetchViewedAssetNotCompleted = [[ProntoUserDefaults userDefaults] hasViewedAssetFetchedFromCMS];
    if (fetchViewedAssetNotCompleted == NO)
    {
        [[ProntoUserDefaults userDefaults] setViewedAssetFetchedFromCMSValue:YES];
        [self.eventListViewModel fetchAndSaveViewedAssetsWithCompletionHandler:^(BOOL success) {
            if (success == NO)
            {
                 [[ProntoUserDefaults userDefaults] setViewedAssetFetchedFromCMSValue:NO];
            }
        }];
    }
}

- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self setupViewsFrames];
    
    [self.cardView reloadData];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{

}


#pragma mark -
#pragma mark Public Methods

/*
 Requirement:
 Field User : Event will be listed according to their priority and if some event having same priority,   then those will  again sorted according their title.
 Home Office: Event will be listed according to their start date and if some event having same start date then those will  again sorted according their title.
 */

- (void)updateViewOnPreferenceChanges
{
    [self.eventListViewModel updateDataSourceOnPreferenceChanges];
}

- (void)showViewWhenDataSourceIsEmpty:(BOOL)show
{
    if (show == YES)
    {
        self.cardView.hidden = YES;
        [self displayEmpty];
    }
    else
    {
        [self hideEmpty];
        self.cardView.hidden = NO;
        [self.cardView reloadData];
    }
}

#pragma mark -
#pragma mark iCarousel methods


- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return [self.eventListViewModel.eventListDataSource count];
    return ProntoRedesignSingletonClass.sharedInstance.eventsAllArray.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    EventsCardView *cardView = nil;
    cardView = [EventsCardView GetEventsCardView];
    cardView.frame = CGRectMake(0, 0,kCardViewWidth, kCardViewHeight);
    cardView.contentMode     = UIViewContentModeCenter;
    cardView.backgroundColor = [UIColor redColor];
    [cardView.layer setMasksToBounds:YES];
  if (ProntoRedesignSingletonClass.sharedInstance.eventsAllArray.count > 0) {
    [cardView prepareCardViewFromEvent:ProntoRedesignSingletonClass.sharedInstance.eventsAllArray[index]];
  }
  //  [cardView prepareCardViewFromEvent:self.eventListViewModel.eventListDataSource[index]];

    [cardView createCornerAroundView];
    
    return cardView;
}

- (CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //place for customization.

    
    //Changes for Warnings
    CGFloat offsetFactor = [[AppUtilities sharedUtilities] isLandscape] == YES ? 220.0f: 200; //220,200
     CGFloat zFactor = 400;
    CGFloat  normalFactor = 0;
    CGFloat  shrinkFactor = 100.0;
    CGFloat f =  sqrt(offset * offset + 1)-1;
    
    transform = CATransform3DTranslate(transform, offset * offsetFactor, f * normalFactor, f * (-zFactor));
    transform = CATransform3DScale(transform, 1 / (f / shrinkFactor + 1.0), 1 / (f / shrinkFactor + 1.0), 1.0 );
    
    return transform;
}

- (BOOL)carousel:(iCarousel *)carousel shouldSelectItemAtIndex:(NSInteger)index
{
    return true;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    //if (self.eventListViewModel.networkRequestInProgress == YES) { return ;}
    
    if (carousel.currentItemIndex == index)
    {
        //Code updated - read event from eventsAllArray
        self.selectedEvent = ProntoRedesignSingletonClass.sharedInstance.eventsAllArray[index];
        if ([self.selectedEvent doesHaveEventDetails] == YES)
        {
            
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(eventListViewController:didSelectIndex:)])
            {
                [self.delegate eventListViewController:self didSelectIndex:index];
            }
        }
        else
        {
            [self.eventListViewModel makeEventDetailServiceRequestWithCMS:^(BOOL success) {
                
                if (success == YES)
                {
                    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(eventListViewController:didSelectIndex:)])
                    {
                        [self.delegate eventListViewController:self didSelectIndex:index];
                    }
                }
            }];
        }
    }
}


- (NSInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 0;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.1;
    }
    else if (option == iCarouselOptionWrap)
    {
        return 1.0;
    }
    return value;
}

#pragma empty View

- (void)displayEmpty
{
    if ([self.eventListViewModel isDataSourceEmpty] == NO) { return; }
    if (self.emptyView == nil)
    {
        self.emptyView = [[ZMEmpty alloc] initWithEventScreenAsParent:self.view];
    }
        
    if (self.eventListViewModel.errorMessage != nil &&
        self.eventListViewModel.errorMessage.length > 0)
    {
        
        self.emptyView.subtitle.text = self.eventListViewModel.errorMessage;
    }
    else
    {
        self.emptyView.subtitle.text = RedesignConstants.noEventsContentInDetailScreen;
    }
    self.emptyView.message.text = @"";
    [self.view addSubview:self.emptyView];
    
    [ZMEmpty transitionWithView:self.emptyView duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
    [self.emptyView setHidden:NO];
}

- (void)hideEmpty
{
    [self.emptyView removeFromSuperview];
    self.emptyView = nil;
    self.eventListViewModel.errorMessage = nil;
}

#pragma mark - Activity Indicator View


- (void)showActivityIndicatorView:(BOOL)show
{
    if (show == YES)
    {
        if(self.indicatorView != nil)
        {
            [self.indicatorView removeFromSuperview];
        }
        
        
        self.indicatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        self.indicatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.indicatorView.clipsToBounds = YES;
        self.indicatorView.layer.cornerRadius = 10.0;
        
        // indicator
        self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        CGRect frame2 = CGRectMake(0, 0, self.indicatorView.bounds.size.width, self.indicatorView.bounds.size.height);
        self.activityIndicatorView.frame = frame2;
        
        [self.indicatorView addSubview:self.activityIndicatorView];
        [self.activityIndicatorView startAnimating];
        [self.view addSubview:self.indicatorView];
        self.indicatorView.center = CGPointMake(self.view.center.x, self.view.center.y - 50);
    }
    else
    {
        [AbbvieLogging logInfo:@"Events -> Hiding Indicatorview "];
        
        [self.activityIndicatorView stopAnimating];
        [self.activityIndicatorView removeFromSuperview];
        [self.indicatorView removeFromSuperview];
        self.activityIndicatorView  = nil;
        self.indicatorView = nil;
        
        self.activityIndicatorView.hidden = YES;
        self.indicatorView.hidden = YES;
        NSArray *viewsToRemove = [self.indicatorView subviews];
        for (UIView *view in viewsToRemove)
        {
            [view removeFromSuperview];
        }
    }
    
}

- (void)updateActivityIndicatorCenter
{
    self.indicatorView.center = CGPointMake(self.view.center.x, self.view.center.y - 50);
}

- (void)didTapRefreshButton
{
    [self.eventListViewModel makeEventServiceRequestWithCMS:NULL];
}

- (void)updateDatasourceFromLocalDB
{
    [self.eventListViewModel updateDataSourceFromLocalDB];
}

@end



