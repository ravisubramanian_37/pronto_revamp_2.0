//
//  EventListViewModel.h
//  Pronto
//
//  Created by m-666346 on 03/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@class EventListViewController;
@class Constant;



@interface EventListViewModel : NSObject

- (BOOL)isDataSourceEmpty;
- (void)initializeDatasource;
- (void)updateDataSourceOnPreferenceChanges;
- (void)updateDataSourceFromLocalDB;
- (void)makeEventServiceRequestWithCMS:(OperationStatusCompletion)completionHandler;
- (void)makeEventDetailServiceRequestWithCMS:(OperationStatusCompletion)completionHandler;
- (NSArray*) getSortedEventListAccordingToUserForList:(NSArray*)unsortedList;

//This will fetch viewed asset and saved it user preferences.
- (void)fetchAndSaveViewedAssetsWithCompletionHandler:(OperationStatusCompletion)completionHandler;

//if isdatasourceEmpty-> initiate the a
@property (nonatomic, strong) NSString *errorMessage;
@property (nonatomic, strong) NSMutableArray *eventListDataSource;
@property (nonatomic, weak) EventListViewController *parentViewController;
@property (nonatomic, assign, getter=isNetworkRequestInProgress) BOOL networkRequestInProgress;

@end
