//
//  EventListViewModel.m
//  Pronto
//
//  Created by m-666346 on 03/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventListViewModel.h"
#import "Pronto-Swift.h"
#import "Franchise.h"
#import "EventListViewController.h"
#import "ZMHeader.h"
#import "EventsViewController.h"
#import "EventCoreDataManager.h"
#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"
#import "NSDate+DateDirectory.h"
#import "ProntoUserDefaults.h"
#import "Constant.h"
#import "AppUtilities.h"


typedef void(^NetworkReuestWithCMSCompletion)(BOOL success);

@implementation EventListViewModel

- (void)updateDataSourceFromLocalDB
{
  if (ProntoRedesignSingletonClass.sharedInstance.eventsAllArray.count > 0) {
    //Sort the event list
//    ProntoRedesignSingletonClass.sharedInstance.eventsAllArray = [[self getSortedEventListAccordingToUserForList:ProntoRedesignSingletonClass.sharedInstance.eventsAllArray] mutableCopy];
    NSLog(@"EVEnts order %@", ProntoRedesignSingletonClass.sharedInstance.eventsAllArray);
   //update view
    [self.parentViewController showViewWhenDataSourceIsEmpty:NO];
  } else {
    [self.parentViewController showViewWhenDataSourceIsEmpty:YES];
  }
}

- (void)updateDataSourceOnPreferenceChanges
{
    [self updateDataSourceFromLocalDB];
}

- (void)initializeDatasource
{
    if (self.isNetworkRequestInProgress == YES) { return; }
    //Removing old values if there any.
    [self.eventListDataSource removeAllObjects];
    
    if ([[EventCoreDataManager sharedInstance] isEventEmptyInLocalDatabase] == YES)
    {
        if (self.parentViewController != nil)
        {
            [self.parentViewController showActivityIndicatorView:YES];
        }
        
        //Gettings Events
        self.networkRequestInProgress = YES;
        //TODO:Saranya replace one with newly created service in eventsServiceclass
        [[EventsServiceClass sharedAPIManager]getEvents:^(NSDictionary *eventsFetched) {
            
            if (eventsFetched != nil &&
                eventsFetched.count > 0 &&
                [eventsFetched isKindOfClass:[NSDictionary class]])
            {
                //success
                [[EventCoreDataManager sharedInstance] saveEvents:eventsFetched withCompletionHandler:^(BOOL success, NSArray *events) {
                    
                    if (success)
                    {
                        // Events saved to local database succesfully. Get All the saved Events
                        self.eventListDataSource = [[[EventCoreDataManager sharedInstance] getEvents] mutableCopy];
                        
//                        self.eventListDataSource = [[self getSortedEventListAccordingToUserForList:self.eventListDataSource] mutableCopy];
                        
                    }
                    else
                    {
                        //If failed to save events in local database. update the value with in-memory events and newly created one.
                        self.eventListDataSource = [[[EventCoreDataManager sharedInstance] getEvents] mutableCopy];
//                        self.eventListDataSource = [[self getSortedEventListAccordingToUserForList:self.eventListDataSource] mutableCopy];
                        
                        [self.eventListDataSource  addObjectsFromArray:events];
                        
                    }
                    
                    // Hiding activity indicator
                    if (self.parentViewController != nil)
                    {
                        [self.parentViewController showActivityIndicatorView:NO];
                    }
                    
                    // Updating UI
                    if ([self isDataSourceEmpty] == YES) // It means there is event but for current franchise selection, No events
                    {
                        [self.parentViewController showViewWhenDataSourceIsEmpty:YES];
                    }
                    else
                    {
                        [self.parentViewController showViewWhenDataSourceIsEmpty:NO];
                    }
                    
                }];
            }
            else
            {
                //if last update is zero and events count is zero. it means no events available at this moment.
                NSString   *lastUpdate = [[ProntoUserDefaults userDefaults] getCMSLastSynchronizationForEvents];
                if (lastUpdate.integerValue <= 0 &&
                    [[eventsFetched objectForKey:@"total"] integerValue] <= 0)
                {
                    self.errorMessage = @"No events found.";
                }
                
                [self.parentViewController showViewWhenDataSourceIsEmpty:YES];
            }
            
            self.networkRequestInProgress = NO;
        } error:^(NSError *error) {
            
            [AbbvieLogging logError:[NSString stringWithFormat:@"getEvents error:%@", error]];
            self.errorMessage = error.localizedDescription;
            
            if (self.parentViewController != nil)
            {
                [self.parentViewController showActivityIndicatorView:NO];
            }
            
            self.networkRequestInProgress = NO;
        }];
    }
    else
    {
        //updating values from local database.
        self.eventListDataSource = [[[EventCoreDataManager sharedInstance] getEvents] mutableCopy];
//        self.eventListDataSource = [[self getSortedEventListAccordingToUserForList:self.eventListDataSource] mutableCopy];
        if ([self isDataSourceEmpty] == YES) // It means there is event but for current franchise selection, No events
        {
            [self.parentViewController showViewWhenDataSourceIsEmpty:YES];
        }
        else
        {
             [self.parentViewController showViewWhenDataSourceIsEmpty:NO];
        }
        self.networkRequestInProgress = NO;
    }
}

-(void)fetchAndSaveViewedAssetsWithCompletionHandler:(OperationStatusCompletion)completionHandler
{
    // fetchViewed
    //TODO:Replace with new service in EventsServiceClaas
   [[EventsServiceClass sharedAPIManager]fetchViewed:^(NSDictionary *assetDetailsFetched) {
        
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"assetDetailsFetched:%@",assetDetailsFetched]];
        NSMutableArray *normalAsset  = [NSMutableArray array];
        NSMutableArray *eoaAsset     = [NSMutableArray array];
        
        if (assetDetailsFetched != nil &&
            [assetDetailsFetched isKindOfClass:[NSArray class]] &&
            assetDetailsFetched.count > 0)
        {
            for (NSDictionary *dict in assetDetailsFetched)
            {
                if ([dict[@"asset_type"] isEqualToString:keyAssetString] &&
                    [normalAsset containsObject:dict[@"asset_id"]] == NO)
                {
                    [normalAsset addObject:dict[@"asset_id"]];
                }
                else // event only asset
                {
                    if ([eoaAsset containsObject:dict[@"asset_id"]] == NO)
                    {
                        [eoaAsset addObject:dict[@"asset_id"]];
                    }
                }
            }
        }
        
        [[ProntoUserDefaults userDefaults] setViewedNormalAssetIds:normalAsset];
        [[ProntoUserDefaults userDefaults] setViewedEventOnlyAssetIds:eoaAsset];
        completionHandler (YES);
    }
    error:^(NSError *error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"%@ %@",ProntoRedesignAPIConstants.fetchViewedAssetErrorMsg, error]];
        completionHandler (NO);
    }];
    
}

- (void)makeEventServiceRequestWithCMS:(OperationStatusCompletion)completionHandler
{
    if (self.isNetworkRequestInProgress == YES) { return; }
    
    self.networkRequestInProgress = YES;
    [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingEvents];
    [[EventsServiceClass sharedAPIManager]getEvents:^(NSDictionary *eventsFetched) {
   // [[ZMAbbvieAPI sharedAPIManager] getEvents:^(NSDictionary *eventsFetched) {
        
        if (eventsFetched != nil &&
            eventsFetched.count > 0 &&
            [eventsFetched isKindOfClass:[NSDictionary class]])
        {
            //success
            [[EventCoreDataManager sharedInstance] saveEvents:eventsFetched withCompletionHandler:^(BOOL success, NSArray *events) {
                
                if (success)
                {
                    // Events saved to local database succesfully. Get All the saved Events
                    self.eventListDataSource = [[[EventCoreDataManager sharedInstance] getEvents] mutableCopy];
                    
//                    self.eventListDataSource = [[self getSortedEventListAccordingToUserForList:self.eventListDataSource] mutableCopy];
                    
                }
                else
                {
                    //If failed to save events in local database. update the value with in-memory events and newly created one.
                    self.eventListDataSource = [[[EventCoreDataManager sharedInstance] getEvents] mutableCopy];
//                    self.eventListDataSource = [[self getSortedEventListAccordingToUserForList:self.eventListDataSource] mutableCopy];
                    
                    [self.eventListDataSource  addObjectsFromArray:events];
                }
                
                // Updating UI
                if ([self isDataSourceEmpty] == YES) // It means there is event but for current franchise selection, No events
                {
                    [self.parentViewController showViewWhenDataSourceIsEmpty:YES];
                }
                else
                {
                    [self.parentViewController showViewWhenDataSourceIsEmpty:NO];
                }
                [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage: nil];
            }];
        }
        else
        {
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
        }
        self.networkRequestInProgress = NO;
    } error:^(NSError *error) {
        
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
        self.networkRequestInProgress = NO;
    }];
}

- (void)makeEventDetailServiceRequestWithCMS:(OperationStatusCompletion)completionHandler
{
    if (self.isNetworkRequestInProgress == YES) { return; }

    self.networkRequestInProgress = YES;
    [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingEventDetails];
    
    //TODO:Update one with new service in Events Service class

    [[EventsServiceClass sharedAPIManager] getEventDetails:self.parentViewController.selectedEvent.eventId complete:^(NSDictionary *sessionDetails) {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"sessionsFetched%@:",sessionDetails]];
        if (sessionDetails != nil &&
            [sessionDetails isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *sessionDetail = [[NSDictionary alloc] initWithObjectsAndKeys:sessionDetails, self.parentViewController.selectedEvent.eventId, nil];
           // NSDictionary *sessionDetail = (NSDictionary*)[sessionDetails valueForKey:self.parentViewController.selectedEvent.eventId];
            if (sessionDetail != nil && [sessionDetail isKindOfClass:[NSDictionary class]] && sessionDetail.count <=0 )
            {
                //It means that event has been deleted from CMS
                //go back to landing screen with updated list
                
                [[EventCoreDataManager sharedInstance] removeEvent:self.parentViewController.selectedEvent withCompletionHandler:^(BOOL success) {
                    
                    if (success == YES)
                    {
                        if (completionHandler != nil)
                        {
                            completionHandler(NO);
                        }
                        self.parentViewController.selectedEvent = nil;
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                                 message:EventHasBeenDeletedMessage
                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAlertAction = [UIAlertAction actionWithTitle:AlertOKTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:kEventDeletedFromCMSNotification object:nil];
                            
                        }];
                        [alertController addAction:okAlertAction];
                        
                        [self.parentViewController presentViewController:alertController animated:YES completion:nil];
                        
                    }
                    else
                    {
                        if (completionHandler != nil)
                        {
                            completionHandler(NO);
                        }
                    }
                }];
            }
            else
            {
                [[EventCoreDataManager sharedInstance] saveEventDetail:sessionDetail withCompletionHandler:^(BOOL success, Event *event) {
                    
                    if (success == YES)
                    {
                        if (completionHandler != nil)
                        {
                            completionHandler(YES);
                        }
                    }
                    else
                    {
                        completionHandler(NO);
                    }
                }];
                
            }
        }
        
        self.networkRequestInProgress = NO;
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
    } error:^(NSError *error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"getSessions error:%@", error]];
        self.networkRequestInProgress = NO;
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
         completionHandler(NO);
    }];
}

- (BOOL)isDataSourceEmpty
{
    if (ProntoRedesignSingletonClass.sharedInstance.eventsAllArray.count > 0)
    {
        return NO;
    }
    return YES;
}

/*
 Requirement:
 Field User : Event will be listed according to their priority and if some event having same priority,   then those will  again sorted according their title.
 Home Office: Event will be listed according to their start date and if some event having same start date then those will  again sorted according their title.
 */
//- (NSArray*) getSortedEventListAccordingToUserForList:(NSArray*)unsortedList
//{
////    if (unsortedList.count <= 0) { return unsortedList;}
////
////    NSMutableArray *temp = [NSMutableArray arrayWithArray:unsortedList];
////
////    if ([ZMUserActions sharedInstance].isFieldUser == YES)
////    {
////        [temp sortUsingComparator:^NSComparisonResult(Event *obj1, Event *obj2) {
////
////            NSInteger priority1 = [obj1 getPriority];
////            NSInteger priority2 = [obj2 getPriority];
////
////            //priority and title
////            if (priority1 > priority2)
////            {
////                return NSOrderedDescending ;
////            }
////            else if (priority1  == priority2)
////            {
////                NSComparisonResult result = [obj1.title compare:obj2.title options:NSNumericSearch | NSCaseInsensitiveSearch];
////
////                if (result == -1)
////                {
////                    return NSOrderedAscending;
////                }
////                else if (result == 1)
////                {
////                    return NSOrderedDescending;
////                }
////                return NSOrderedSame;
////            }
////            return NSOrderedAscending;
////        }];
////    }
////    else
////    {
////        [temp sortUsingComparator:^NSComparisonResult(Event *obj1, Event *obj2) {
////
////            if (obj1.startDate == nil || obj2.startDate == nil) {
////                return NSOrderedDescending;
////            }
////
////            //start date and title
////            NSDate *startDate1 = [NSDate GetDateSameAsStringFromDateString:obj1.startDate] ;
////            NSDate *startDate2 = [NSDate GetDateSameAsStringFromDateString:obj2.startDate] ;
////
////            if ([NSDate isDate1:startDate1 greaterThanDate:startDate2] )
////            {
////                return NSOrderedDescending ;
////            }
////            else if ([NSDate isDate1:startDate1 equalToDate:startDate2])
////            {
////                NSComparisonResult result = [obj1.title compare:obj2.title options:NSNumericSearch | NSCaseInsensitiveSearch];
////
////                if (result == -1)
////                {
////                    return NSOrderedAscending;
////                }
////                else if (result == 1)
////                {
////                    return NSOrderedDescending;
////                }
////                return NSOrderedSame;
////            }
////            return NSOrderedAscending;
////        }];
////      return NSOrderedSame;
////    }
//
//    return [NSArray arrayWithArray:temp];
//
//}


@end
