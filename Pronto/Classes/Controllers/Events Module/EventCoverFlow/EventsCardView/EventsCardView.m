//
//  EventsCardView.m
//  Pronto
//
//  Created by m-666346 on 30/07/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventsCardView.h"
#import "UIColor+ColorDirectory.h"
#import "Pronto-Swift.h"
#import "UIFont+FontDirectory.h"
#import "UIColor+ColorDirectory.h"
#import "UIImage+ImageDirectory.h"
#import "NSDate+DateDirectory.h"
//#import "ZMAbbvieAPI.h"
#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"


@interface EventsCardView()

@end

@implementation EventsCardView

+ (EventsCardView*)GetEventsCardView
{
    return (EventsCardView*)[[[NSBundle mainBundle] loadNibNamed:@"EvensCardView"
                                          owner:self options:nil] firstObject];
}

+ (EventsCardView*)GetEventsCardViewWithFrame:(CGRect)frame
{
    EventsCardView *cardView = (EventsCardView*)[[[NSBundle mainBundle] loadNibNamed:@"EvensCardView"
                                                                               owner:self
                                                                             options:nil] firstObject];
    cardView.frame = frame;
    cardView.cardContainerView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    return cardView;
}


- (void)createCornerAroundView
{
    self.layer.cornerRadius = 8.0f;
    self.layer.borderColor  = [[UIColor GetColorFromHexValue7d91b4] CGColor];
    self.layer.borderWidth  = 1.0;
    self.clipsToBounds      = YES;
}


- (void)prepareCardViewFromEvent:(Event*)event
{
    NSString *location = [event getEventLocation];
    if (location == nil ||  (location != nil && location.length <= 0) )
    {
        self.eventLocationView.hidden = YES;
    }
    else
    {
        self.eventsLocationNameLabel.text   = location;
        self.eventLocationView.hidden = NO;
    }
    
    self.eventTitleLabel.text           = event.title;
    self.eventsDateRangeLabel.text      = [event getFormattedDateString];
    self.cardFooterView.backgroundColor = [UIColor GetColorFromHexString:[event getFooterColor]];
    self.eventSyncedImageView.hidden    = !event.isUpdateAvailable.boolValue;
    
    [self prepareCoverFlowImageForEvent:event];
}

- (void)prepareCoverFlowImageForEvent:(Event*)event
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[[EventsServiceClass sharedAPIManager] getCoverflowImagePath:event.imageURL]])
    {
        self.cardBGImageView.image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:[[EventsServiceClass sharedAPIManager] getCoverflowImagePath:event.imageURL]]];
    }
    else
    {
        self.cardBGImageView.backgroundColor = [UIColor GetColorFromHexString:[event getFooterColor]];
        //Downloads the image and caches it
        [[EventsServiceClass sharedAPIManager] downloadCoverflowImage:event.imageURL complete:^(UIImage *image, NSString *p)
         {
             if (![p isEqualToString:event.imageURL])
             {
                 image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:[[EventsServiceClass sharedAPIManager]getCoverflowImagePath:event.imageURL]]];
             }
             
             if(image != nil)
             {
                 self.cardBGImageView.image = image;
                 self.cardBGImageView.backgroundColor = [UIColor clearColor];
             }
             
         } fromCache:YES];
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setupUI
{
    self.eventTitleLabel.font           = [UIFont FontLatoRegularSize24];
    self.eventsLocationNameLabel.font   = [UIFont FontLatoRegularSize14];
    self.eventsDateRangeLabel.font      = [UIFont FontLatoRegularSize14];
    
    self.eventTitleLabel.textColor           = [UIColor GetColorFromHexValueFFFFFF];
    self.eventsLocationNameLabel.textColor   = [UIColor GetColorFromHexValueFFFFFF];
    self.eventsDateRangeLabel.textColor      = [UIColor GetColorFromHexValueFFFFFF];
    
    self.eventSyncedImageView.image          = [UIImage GetEventDownloadIconImage];
    self.eventLocationImageView.image        = [UIImage GetLocationIconImage];
    self.eventsDateImageView.image           = [UIImage GetCalendarIconImage];

}

@end
