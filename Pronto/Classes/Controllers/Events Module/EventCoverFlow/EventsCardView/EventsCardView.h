//
//  EventsCardView.h
//  Pronto
//
//  Created by m-666346 on 30/07/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventListViewController.h"


@class Event;

@interface EventsCardView : UIView

@property (nonatomic, weak) IBOutlet UIImageView *cardBGImageView;
@property (nonatomic, weak) IBOutlet UIView *cardContainerView;
@property (weak, nonatomic) IBOutlet UIView *cardFooterView;
@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *eventSyncedImageView;
@property (weak, nonatomic) IBOutlet UIView *eventLocationView;
@property (weak, nonatomic) IBOutlet UIView *eventsDateRangeView;
@property (weak, nonatomic) IBOutlet UIImageView *eventLocationImageView;

@property (weak, nonatomic) IBOutlet UILabel *eventsLocationNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *eventsDateImageView;
@property (weak, nonatomic) IBOutlet UILabel *eventsDateRangeLabel;


+ (EventsCardView*)GetEventsCardView;
+ (EventsCardView*)GetEventsCardViewWithFrame:(CGRect)frame;
- (void)createCornerAroundView;
- (void)prepareCardViewFromEvent:(Event*)event;

@end
