////
//  EventsViewController.m
//  Pronto
//
//  Created by cccuser on 27/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import "EventsViewController.h"
#import "ZMNotifications.h"
#import "ZMProntoTheme.h"
#import "Pronto-Swift.h"
#import "Franchise.h"
//#import "ProntoTabbarViewController.h"
#import "EventListViewController.h"
#import "ViewUtils.h"
#import "Constant.h"
#import "FeedbackView.h"
#import "HelpViewController.h"
#import "UIImage+ImageDirectory.h"
#import "AssetDownloadProvider.h"
#import "NSMutableDictionary+Utilities.h"
#import "EventListViewModel.h"
#import "EventsCalendarDetailsScreenVC.h"
#import "UIFont+FontDirectory.h"
#import "UIColor+ColorDirectory.h"
//#import "GlobalSearchVC.h"
#import "AppUtilities.h"
#import "EventsGlobalSearchViewModel.h"
#import "Filter.h"
#import "EventCoreDataManager.h"

#import "Event+CoreDataProperties.h"
#import "EventOnlyAssetHandler.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"
#import "EventsServiceManager.h"
#import "FastPDFViewController.h"
#import <FastPdfKit/FastPdfKit.h>

//https://ch1hub.cognizant.com/sites/Sc1994/CICDKOCProfiles/BaseGrid/tool.html

@interface EventsViewController ()
{
    id <ZMProntoTheme> theme;
    __weak IBOutlet UIView *header;
    ZMWalkthrough *walkthrough;
    ZMLaunchLoading *launchLoading;
    UIView *backView;
    BOOL notificationsLoaded;
    HomeMainHeaderView *mainHeaderView;
}

@property (nonatomic, strong) EventListViewController *eventListViewController;
@property (nonatomic, weak) AppDelegate *mainDelegate;
//@property (nonatomic, weak) ZMAbbvieAPI *apiManager;
//@property (nonatomic, strong)  FeedbackView *feedbackView;
@property (nonatomic, strong)  UIView *backgroundView;
@property (nonatomic, strong)  UIView *refreshButtonView;
@property (nonatomic, strong)  ZMNotifications *notificationsPannel;
@property (nonatomic, assign) BOOL isTapDownloadInitiated;
@property (nonatomic, assign) BOOL isDownloadingAsset;
@property (nonatomic, strong) Asset *currentSelectedAssetToSeeDetail;
@property (nonatomic, strong) Asset *currentAssetDownload;
@property (nonatomic, assign) BOOL shouldResetFranchises;

// Global Search View
//@property (nonatomic, strong) GlobalSearchVC *globalSearcVC;

//Event Only Asset Handler
@property (nonatomic, strong) EventOnlyAssetHandler *eventOnlyAssetHandler;

//To check if we are navigating to asset detail page from current controller
@property (nonatomic, assign) BOOL isNavigatingToAssetDetailPage;
@end

@interface EventsViewController () <EventsResultsHistoryProtocol>

@end

@implementation EventsViewController
//@synthesize headerViewEvents;
static EventsViewController *sharedInstance = nil;


+ (EventsViewController *)sharedInstance{
    
    if(sharedInstance == nil){
        sharedInstance = [[super allocWithZone:NULL] init];
        [sharedInstance initializeData];
    }
    return sharedInstance;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeData];
    
//    self.eventsGSViewModel = [[EventsGlobalSearchViewModel alloc]initWithDelegate:(id<EventsGlobalSearchViewModelDelegate>)self];
    
    
    [self loadEventListViewController];
    [self setupViews];
    [self addRefreshButtonView];
    
//    [self setupViewsFrames];
    //[self addRefreshButtonView];
    self.isNavigatingToAssetDetailPage = NO;
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
    [self addSelfToListenForNotificationCenter];
    
    [self setupViewsFrames];
    [backView addSubview:self.refreshButtonView];
    [self.refreshButtonView setHidden:NO];
    [backView setHidden:NO];
    
}
-(void)setupViews
{
    //Header View
    mainHeaderView = [[[NSBundle mainBundle] loadNibNamed:RedesignConstants.mainViewHeaderNib owner:nil options:nil] objectAtIndex:0];
    mainHeaderView.frame = CGRectMake(0, 60, self.view.frame.size.width, 60);
    mainHeaderView.eventsDelegate = self;
    mainHeaderView.flowType = @"events";
    [self.view addSubview:mainHeaderView];
    [mainHeaderView setThemes];
    self.view.backgroundColor = [RedesignThemesClass sharedInstance].defaultThemeforUser;

    mainHeaderView.frame = CGRectMake(0, 15, self.view.frame.size.width, 70);
    
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 85, self.view.frame.size.width, 50)];
    backView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:backView];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(5, 10, 25, 25);
    [backButton setImage:[UIImage imageNamed:@"left_arrow_gray"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnClicked) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:backButton];
    
    UILabel *backLabel = [[UILabel alloc] initWithFrame:CGRectMake(backButton.frame.origin.x + backButton.frame.size.width , 10, 50, 25)];
    [backLabel setText:@"Home"];
    backLabel.font = [UIFont boldSystemFontOfSize:16];
    backLabel.textColor = [[RedesignThemesClass sharedInstance]hexStringToUIColorWithHex:RedesignThemesClass.sharedInstance.royalBlueCode];
    [backView addSubview:backLabel];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnClicked)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [backLabel addGestureRecognizer:tapGestureRecognizer];
    backLabel.userInteractionEnabled = YES;
}

-(void)btnClicked{
    [[RedesignErrorHandling sharedInstance]hideMessage];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setupViewsFrames];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [backView setHidden:YES];
   
   // [backView removeFromSuperview];
    self.currentSelectedAssetToSeeDetail = nil;
    
    //We want to remove all notification but want to keep listing to kEventDeletedFromCMSNotification
    [self removeSelfFromListeningToNotificationCenter];
    [self registerNotification:kEventDeletedFromCMSNotification];
    if(self.isNavigatingToAssetDetailPage == YES)
    {
        [self registerNotification:kZMPDFDocumentDidCloseButtonPressedNotification];
        [self registerNotification:kZMAssetViewControllerGoBackNotification];
    }
    self.isNavigatingToAssetDetailPage = NO;
    mainHeaderView.searchBarHomeScreen.text = @"";
}

- (void)loadEventListViewController
{
    if (self.eventListViewController == nil)
    {
        self.eventListViewController = [EventListViewController GetEventListViewController];
    }
    self.eventListViewController.parentVC = self;
    self.eventListViewController.view.frame = CGRectMake(0, 135,self.view.width, self.view.height - 288);
    self.eventListViewController.delegate = (id <EventListViewControllerDelegate>)self;
    
    [self addChildViewController:self.eventListViewController];
    [self.view addSubview:self.eventListViewController.view];
    [self.eventListViewController didMoveToParentViewController:self];
}

- (void)addRefreshButtonView
{
    self.refreshButtonView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    CGRect frame = self.refreshButtonView.frame;
    frame.origin.x = 0;
    frame.size.width = 100;
    frame.size.height = 40;
    frame.origin.y = 5;
    self.refreshButtonView.frame = frame;
    
    UIImageView *refreshImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,(self.refreshButtonView.frame.size.height/2 - 6.5),15,15)];
    refreshImage.contentMode = UIViewContentModeScaleAspectFill;
    [refreshImage setImage:[UIImage imageNamed:@"refresh"]];
    [self.refreshButtonView addSubview:refreshImage];
    
    UILabel *refreshLabel = [[UILabel alloc]initWithFrame:CGRectMake(refreshImage.frame.size.width + 8,0,self.refreshButtonView.frame.size.width - refreshImage.frame.size.width,self.refreshButtonView.frame.size.height)];
    refreshLabel.text = @"Refresh";
    refreshLabel.font = [UIFont boldSystemFontOfSize:16]; //[UIFont FontLatoRegularSize17];
    refreshLabel.textColor = [[RedesignThemesClass sharedInstance]hexStringToUIColorWithHex:RedesignThemesClass.sharedInstance.royalBlueCode];
    [self.refreshButtonView addSubview:refreshLabel];
    
    UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    refreshButton.frame = CGRectMake(0,0,self.refreshButtonView.frame.size.width,self.refreshButtonView.frame.size.height);
    [refreshButton addTarget:self action:@selector(refreshButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.refreshButtonView addSubview:refreshButton];
    
    [self.refreshButtonView setHidden:YES];
    
    //return self.refreshButtonView;
}

- (IBAction)refreshButtonTapped:(UIButton *)sender
{
    [self.eventListViewController didTapRefreshButton];
}

- (void)setupViewsFrames
{
    //Setting EventList View controller
    self.eventListViewController.view.frame = CGRectMake(0, 63,self.view.width, self.view.height - 63);
    [self updateRefreshButtonFrame];
}


- (void)initializeData
{
    self.mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    self.apiManager   = [ZMAbbvieAPI sharedAPIManager];
    self.actions      = [ZMUserActions sharedInstance];
    [self addSelfToListenForNotificationCenter];
}

#pragma mark - Notifications

- (void)addSelfToListenForNotificationCenter
{
    [self registerNotification:kPreferenceListVCDidSavePreferencesNotification];
    [self registerNotification:kEventDeletedFromCMSNotification];
    [self registerNotification:kZMPDFDocumentDidCloseButtonPressedNotification];
    [self registerNotification:kZMAssetViewControllerGoBackNotification];
    [self registerNotification:UIApplicationWillEnterForegroundNotification];

}

- (void)removeSelfFromListeningToNotificationCenter
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)registerNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:name object:nil];
}

- (void)recievedNotification:(NSNotification *)notification
{
   if ([[notification name] isEqualToString:kPreferenceListVCDidSavePreferencesNotification])
    {
        //On changing the preferences, populate the preferences accordingly.
        [self.eventListViewController updateViewOnPreferenceChanges];
    }
    else if ([[notification name] isEqualToString:UIApplicationWillEnterForegroundNotification])
    {
        //Refreshing event list when user coming from background to foreground.
        [self.eventListViewController didTapRefreshButton];
    }
    else if ([[notification name] isEqualToString:kEventDeletedFromCMSNotification])
    {
        //Event has been deleted from CMS, update data source from local db.
        [self.eventListViewController updateDatasourceFromLocalDB];
        [ZMUserActions sharedInstance].currentSelectedTabIndex = 3;
    }
    else if ([[notification name] isEqualToString:kZMPDFDocumentDidCloseButtonPressedNotification] || [[notification name] isEqualToString:kZMAssetViewControllerGoBackNotification])
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)viewDidLayoutSubviews
{
    CGRect frame = self.comingSoonLabel.frame;
    frame.origin.x = (self.view.frame.size.width/2) - (frame.size.width/2);
    frame.origin.y = (self.view.frame.size.height/2) - (frame.size.height/2);
    self.comingSoonLabel.frame = frame;
    
    [self.notificationsPannel adjustNotifications];
    [self.notificationsPannel changeOfOrientation];
}

-(void) updateRefreshButtonFrame
{
    CGRect frame = self.refreshButtonView.frame;
    frame.origin.x = self.view.frame.size.width - (frame.size.width + 20);
    self.refreshButtonView.frame = frame;
    
    [backView setFrame:CGRectMake(0, 85, self.view.frame.size.width, 50)];
    [mainHeaderView setFrame:CGRectMake(0, 20, self.view.frame.size.width, 60)];
}

- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self setupViewsFrames];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

}

//Capture the rotation of screens
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {

}

#pragma mark
#pragma mark - Open Asset Related Methods
/**
 *  With the given asset validates if the assets is local or initiates the download
 *  @param selectedAsset
 */
- (void)validateAndShowAsset:(Asset *)selectedAsset
{
    [AbbvieLogging logInfo:@"*****Intro validate Asset ********"];
    [self validateAssetBeforeOpen:selectedAsset completion:^(BOOL assetNeedsDownload)
     {
         if (assetNeedsDownload == YES)
         {
             //Restricting user from opening asset if offline download is in progress
             NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:selectedAsset.assetID];
             for(OfflineDownload * data in offlineDownloadArray)
             {
                 if(data.assetID == selectedAsset.assetID)
                 {
                     if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"])
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             if(!self.isTapDownloadInitiated)
                             {
                                 [self downloadSelectedAsset:selectedAsset];
                                 [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                             }
                         });
                         
                         self.isDownloadingAsset = NO;
                     }
                     else
                     {
                         if(!self.isTapDownloadInitiated)
                         {
                             [self downloadSelectedAsset:selectedAsset];
                             [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                         }
                     }
                 }
             }
             if(offlineDownloadArray.count == 0)
             {
                 if(!self.isTapDownloadInitiated)
                 {
                     [self downloadSelectedAsset:selectedAsset];
                     [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                 }
             }
         }
         else
         {
             if(self.currentSelectedAssetToSeeDetail)
             {
                 //Return in case if open of any asset is in progress
                 return;
             }
             else
             {
                 self.currentSelectedAssetToSeeDetail = selectedAsset;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                     selectedAsset.isUpdateAvailable = NO;
                 });
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [AbbvieLogging logInfo:@"------Asset needs opened ------"];
                     [self openAsset:selectedAsset];
                 });
             }
         }
     } failure:^(NSString *messageFailure) {
         [self.mainDelegate showMessage:messageFailure whithType:ZMProntoMessagesTypeWarning];
     }];
}

-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
    
    if (!self.isDownloadingAsset)
    {
        self.isDownloadingAsset = YES;
        // check if asset is protected
        if (![UIApplication sharedApplication].protectedDataAvailable)
        {
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
            /// This should occur during application:didFinishLaunchingWithOptions:
            //[ASFFileProtectionManager stopProtectingLocation:asset.path];
            
            // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
            NSError *error;
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];
            
            if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
            {
                attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
                if (!success)
                    [AbbvieLogging logInfo:@"Set ~/Documents attrsAssetPath NOT successfull"];
            }
            
            /// ....
            NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
            [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
        }
        
        // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        if([[NSFileManager defaultManager] fileExistsAtPath:asset.path])
        {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"File Exists at %@", asset.path]];

        }
        
        if ((![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && assetType != ZMProntoFileTypesBrightcove) && (assetType != ZMProntoFileTypesWeblink))
        {
            if ([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
            {
                //                _isDownloadignAsset = YES;
                completion(YES);
            }
            else
            {
                self.isDownloadingAsset = NO;
                failure(@"The internet connection is not available, please check your network connectivity");
            }
        }
        else
        {
            //Check if the asset size matches the metada info meaning the asset is correct
            //            NSLog(@"** size Asset %d size %d", asset.file_size.intValue, (int)fileSize);
            if (asset.file_size.intValue == (int)fileSize)
            {
                //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
                self.isDownloadingAsset = NO;
                completion(NO);
            }
            else
            {
                completion(YES);
            }
        }
    }
    else
    {
        if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
        {
            if(self.isDownloadingAsset == NO)
            {
                failure(@"The internet connection is not available, please check your network connectivity");
                self.isDownloadingAsset = NO;
            }
        }
        else
        {
            failure(@"Download in progress. Please wait and then try again.");
        }
    }
}

//Download of an asset if not found in coredata / new

- (void) downloadSelectedAsset:(Asset *)selected
{
    self.isTapDownloadInitiated = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    });
    [AbbvieLogging logInfo:@"------Asset needs download ------"];
    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    if(![selected.file_mime isEqualToString:@"weblink"])
    {
        [self downloadAsset:selected];
    }
}

- (void)downloadAsset:(Asset *)selectedAsset
{
    self.currentAssetDownload = selectedAsset;
    self.isDownloadingAsset = YES;
   // [self setProgress:0.001000];
    
    [[AssetsServiceClass sharedAPIManager] downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
//        [self setProgress:progress];
    } completion:^{
        [self downloadAssetDidFinish:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"completed"];
        // New revamp changes - remove the update label once the asset is downloaded successfully.
        selectedAsset.isUpdateAvailable = NO;
    } onError:^(NSError *error) {
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAssetOnFailure:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"failed"];
    }];
}


/**
 *  Open selected asset, the asset could be of type PDF-VIDEO-DOCUMENT,the assets of type PDF or document will be open through FastPDFKIT, the ones of type Video thorugh Brightcove
 *
 *  @param selectedAsset Asset to open
 */

- (void)openAsset:(Asset *)selectedAsset
{
    self.isNavigatingToAssetDetailPage = YES;
    // PRONTO-15 - Popup Getting Displayed Over PDF
//    if(self.headerViewEvents.categoryPopOver)
//        [[self.headerViewEvents.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
//
//    // removing deprecated warnings
//    if(self.headerViewEvents.sortMenuPopOver)
//        [[self.headerViewEvents.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    
    
    self.shouldResetFranchises = NO;
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
    
    self.isDownloadingAsset = NO;

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController <ZMAssetViewControllerDelegate> *controller;
    
    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
    if (assetType == ZMProntoFileTypesPDF) {
        if(document != nil)
        {
            [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                self.currentSelectedAssetToSeeDetail = nil;
            });
        }
    }
    else if (assetType == ZMProntoFileTypesBrightcove)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
        controller.eventsViewController = self;
        controller.asset = selectedAsset;
//        controller.userFranchise = self.headerViewEvents.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (assetType == ZMProntoFileTypesDocument)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
        controller.eventsViewController = self;
        controller.asset = selectedAsset;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if(assetType == ZMProntoFileTypesWeblink)
    {
        //NSLog(@"opening weblink..");
        // open the weblink URL
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
        controller.eventsViewController = self;
        controller.asset = selectedAsset;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else
    {
        // Fix - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** -[NSURL initFileURLWithPath:]: nil string parameter'
        if (assetType == ZMProntoFileTypesPDF)
        {
            if(selectedAsset.path)
            {
                MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
                if(document != nil)
                {
                    [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
                }
                else
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                        self.currentSelectedAssetToSeeDetail = nil;
                    });
                }
            }
        }
    }
}

#pragma mark - ZMHeader delegate methods for global search

-(void)header:(ZMHeader*)headerView editingChanged:(NSString*)searchtext {
    if (searchtext.length >= 3)
    {
        self.eventsGSViewModel.currentFilter.searchTextString = searchtext;
        [ZMUserActions sharedInstance].currentFilter = self.eventsGSViewModel.currentFilter;
    }
    else
    {
        self.eventsGSViewModel.currentFilter.searchTextString = searchtext;
        [ZMUserActions sharedInstance].enableSearch = YES;
//        self.headerViewEvents.searchTextbox.enabled = YES;
//        _apiManager._syncingCompletion = YES;
        
//        [self.headerViewEvents hidingIndicator];
        if (searchtext.length == 0)
        {
//            [self loadCAssetLibrary];
        }
    }
}

//-(void)header:(ZMHeader*)headerView isDismissingSearchPopover:(UIPopoverPresentationController *)popoverPresentationController
//{
//    [self.globalSearcVC dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
//}

//-(void)header:(ZMHeader*)headerView textFieldDidBeginEditing:(UITextField*)textField
//{
    // Load the Global search view from here
    // Need to take care of observer, becuase same observer being used by tools and training
    // If not required remove it, Or when loading global search remove the assetlibrary observer
    // And when removing the view the view, start listening to assetlibrary observer
    //NSLog(@"Text field editing started - Present Global Search");
//    [self loadGlobalSearchView];
//}

//- (void)loadGlobalSearchView
//{
//    if (self.globalSearcVC == nil)
//    {
//        self.globalSearcVC = [GlobalSearchVC GetGSLibraryViewController];
//        self.globalSearcVC.gsVCDelegate = (id<GSViewControllerDelegate>)self;
//    }
//
//    [ZMUserActions sharedInstance].globalSearchViewController = self.globalSearcVC;
//    [ZMUserActions sharedInstance].currentFilter = self.eventsGSViewModel.currentFilter;
//    [[ZMAssetsLibrary defaultLibrary] removeObserver:self];
//
//    //Set frame
//    self.globalSearcVC.view.frame = CGRectMake(0, 63,self.view.width, self.view.height - 63);
//
//    [self addChildViewController:self.globalSearcVC];
//    [self.view addSubview:self.globalSearcVC.view];
//    [self.globalSearcVC didMoveToParentViewController:self];
//
//    //Events Setup
//    [self removeSelfFromListeningToNotificationCenter];
//
//    // We need to listen to kPreferenceListVCDidSavePreferencesNotification notification
//    [self registerNotification:kPreferenceListVCDidSavePreferencesNotification];
//
//    [[AppUtilities sharedUtilities] shouldMakeAllProntoTabBarButtonItemSelectable:NO];
//}

#pragma mark - GSViewControllerDelegate Method
- (void)didRemovedFromSuperView
{
    [self addSelfToListenForNotificationCenter];
    [[ZMProntoManager sharedInstance].search setTitle:@""];
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
//    [ZMUserActions sharedInstance].globalSearchViewController = nil;
//    self.globalSearcVC = nil;
//    [[AppUtilities sharedUtilities] shouldMakeAllProntoTabBarButtonItemSelectable:YES];
    [[EventCoreDataManager sharedInstance] resetEventSearchedResults];
    [[EventCoreDataManager sharedInstance] resetEventOnlyAssets];
    [[EventCoreDataManager sharedInstance] resetAssets];
}

//- (void)resetGlobalSearchFromEvents
//{
//    [self.globalSearcVC dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
//}


#pragma mark - ZMAssetsLibrary Observer methods for asset downloading

- (void)downloadAssetDidFinish:(Asset *)asset {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate hideMessage];
    });
    
    self.isDownloadingAsset = NO;
    self.isTapDownloadInitiated = NO;
    
    [self.mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", asset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self validateAndShowAsset:asset];
    });
}


- (void)downloadAssetDidNotFinish:(Asset *)asset error:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.mainDelegate.currentMessage)
        {
            [self.mainDelegate hideMessage];
        }
        
        self.isDownloadingAsset = NO;
        self.isTapDownloadInitiated = NO;
        
        NSString *message;
        enum ZMProntoMessages type = 1;
        // NSInteger errCode = ABS(error.code);
        if (error.code == 503)
        {
            type = ZMProntoMessagesTypeWarning;
            message = @"The server is currently under maintenance mode.";
        }
        
        else if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
        {
            type = ZMProntoMessagesTypeWarning;
            message = @"The Internet connection is not available, please check your network connectivity.";
        }
        else if(error.code == 401)
        {
            //Restoring the user session
            [self.mainDelegate initPronto];
            return;
        }
        else if (error.code == 404)
        {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"Asset not found %@", asset.title];
        }
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        else if (error.code == 5303 || error.code == -5303)
        {
            message = @"Error while retreiving assets, please try again later";
        }
        else if (error.code == 1009 || error.code == -1009)
        {
            message = @"Please check the network connection";
        }
        else if (error.code == 1003 || error.code == -1003)
        {
            message = @"A server with the specified hostname could not be found";
        }
        else
        {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"There is a problem downloading %@", asset.title];
        }
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mainDelegate showMessage:message whithType:type withAction:^{
                [self.mainDelegate hideMessage];
                [self validateAndShowAsset:asset];
            }];
        });
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        if ([message containsString:@"ERROR_CODE"])
        {
            // errorcode consists of digits
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",@"Please check the network connection"] forKey:@"scerrorcode"];
        }
        else
        {
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",message] forKey:@"scerrorcode"];
        }
        
        self.currentAssetDownload = nil;
        
        /**
         * @Tracking
         * Download Did Not finished
         **/
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"assetview" withName:nil withOptions:trackingOptions];
    });
}

- (void)updateOfflineDownloadCoreData:(Asset *)asset status:(NSString*)status
{
    if([asset valueForKey:@"assetID"] != nil)
    {
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
        [initialDownloadAsset setObject:status forKey:@"status"];
        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
        
        if (offlineData.count > 0)
        {
            //if  insertation and deletetion has to be happen, it will happen one by one
            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
            }];
        }
        else
        {
            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
        }
    }
}

- (void)openFastPDFWithAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
    controller.eventsViewController = self;
    controller.asset = asset;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    
    [viewController.view addSubview:view];
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
        
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor GetColorFromHexValueE0E6ED];
    [viewController.view addSubview:webview];
    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    [self hideMessage];
    controller.viewMain = view;
    controller.webviewScreen = webview;
    controller.webviewScreen.hidden = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (void)hideMessage
{
    [self.mainDelegate hideMessage];
}

#pragma mark --
#pragma mark EventListViewControllerDelegate Delegate

- (void)eventListViewController:(EventListViewController*)viewController didSelectIndex:(NSInteger)inex
{
    EventsCalendarDetailsScreenVC *eventsCalendarDetailScreenVC = [EventsCalendarDetailsScreenVC GetEventsCalendarDetailsScreen];
    eventsCalendarDetailScreenVC.selectedEvent = viewController.selectedEvent;
    
    [self.navigationController pushViewController:eventsCalendarDetailScreenVC animated:YES];
}

#pragma mark - ZMAsset Library observer method
- (void)libraryFinishSync
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self openAssetFromDeepLinkIfRequired];
    });
}

- (void)openAssetFromDeepLinkIfRequired
{
    if (self.actions.deepLinkUserInfo != nil)
    {
        // take page number as well
        NSString *assetId       = [self.actions.deepLinkUserInfo valueForKey:@"assetString"];
        NSInteger pageNumber    = [[self.actions.deepLinkUserInfo  valueForKey:@"pageNumer"] integerValue];
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId];
        if (assetArray.count > 0) // It means asset available
        {
            Asset * asset = assetArray[0];
            //PRONTO-37 Deeplinking to a Page in a PDF
            [ZMUserActions sharedInstance].pageNo = pageNumber;
            [self validateAndShowAsset:asset];
        }
        else
        {
            //Nothing to do
        }
        self.actions.deepLinkUserInfo = nil;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}

#pragma mark - Event Only Asset Open
- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler openAsset:(EventOnlyAsset*)selectedAsset
{
    self.isNavigatingToAssetDetailPage = YES;
    if (self.eventOnlyAssetHandler == nil)
    {
        self.eventOnlyAssetHandler = [[EventOnlyAssetHandler alloc]init];
        self.eventOnlyAssetHandler.delegate      = (id <EventOnlyAssetHandlerDelegate>)self;
    }
    
    //Asset Details accepts only Asset object and it is very tighly coupled with controller.
    //So, Instead of creating new asset detail controller, we are passing equivalent object of eventOnlyAsset.
    Asset *equivalentAssetForEventOnlyAsset = [selectedAsset getEquivalentAssetObject];
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.eoa_path]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.eventOnlyAssetHandler.mainDelegate hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [selectedAsset getFileTypeOfEventOnlyAsset];
    
    self.eventOnlyAssetHandler.isDownloadingAsset = NO;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController <ZMAssetViewControllerDelegate> *controller;
    
    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
    if (assetType == ZMProntoFileTypesPDF)
    {
        if(document != nil)
        {
            [self openFastPDFForEquivalentEventOnlyAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.eventOnlyAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
            });
        }
    }
    else if (assetType == ZMProntoFileTypesBrightcove)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.eventsViewController = self;
        controller.asset = equivalentAssetForEventOnlyAsset;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (assetType == ZMProntoFileTypesDocument)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.eventsViewController = self;
        controller.asset = equivalentAssetForEventOnlyAsset;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    else if(assetType == ZMProntoFileTypesWeblink)
    {
        // open the weblink URL
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.eventsViewController = self;
        controller.asset = equivalentAssetForEventOnlyAsset;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else
    {
        if (assetType == ZMProntoFileTypesPDF)
        {
            if(selectedAsset.eoa_path)
            {
                MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
                if(document != nil)
                {
                    [self openFastPDFForEquivalentEventOnlyAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
                }
                else
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.eventOnlyAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                        self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
                    });
                }
            }
        }
    }
    
}

- (void)openFastPDFForEquivalentEventOnlyAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
    controller.eventsViewController = self;
    controller.asset = asset;
    
    ((FastPDFViewController*)controller).isEventOnlyAsset = YES;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    
    [viewController.view addSubview:view];
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
    
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor GetColorFromHexValueE0E6ED];
    [viewController.view addSubview:webview];
    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    if (self.eventOnlyAssetHandler != nil)
    {
        [self.eventOnlyAssetHandler hideMessage];
    }
    
    controller.viewMain = view;
    controller.webviewScreen = webview;
    controller.webviewScreen.hidden = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (void)addSearchListViewWithXAxis:(CGFloat)xAxis yAxis:(CGFloat)yAxis width:(CGFloat)width height:(NSInteger)height {
  [[SearchHistoryViewController instance] addSearchListViewWithXAxis:xAxis + 6 yAxis: 66 width:width height:height mainStackView:self.view];
}

- (void)dismissView {
  [[SearchHistoryViewController instance] dismissView];
}

@end
