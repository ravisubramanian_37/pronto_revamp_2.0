//
//  EventsViewController.h
//  Pronto
//
//  Created by cccuser on 27/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMHeader.h"
#import "ZMUserActions.h"
#import "ZMAbbvieAPI.h"
#import "ZMProntoViewController.h"
//#import "TabBarController.h"
//#import "ZMLibrary.h"
//#import "ProntoTabbarViewController.h"

@class EventsGlobalSearchViewModel;

@interface EventsViewController : ZMProntoViewController <UICollectionViewDelegateFlowLayout, UIScrollViewDelegate,UIPopoverPresentationControllerDelegate> {//ZMGridItemDelegate
}

+ (EventsViewController *)sharedInstance;

- (void)resetGlobalSearchFromEvents;
- (void)openAsset:(Asset *)selectedAsset;
- (void)validateAndShowAsset:(Asset *)selectedAsset;
-(void)dismissView;

@property (weak, nonatomic) IBOutlet UILabel *comingSoonLabel;
@property (strong, nonatomic) ZMUserActions *actions;
@property(nonatomic, strong) EventsGlobalSearchViewModel *eventsGSViewModel;

@end
