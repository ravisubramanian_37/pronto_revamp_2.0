//
//  EventsFakeTabbarViewController.h
//  Pronto
//
//  Created by m-666346 on 16/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EventsFakeTabbarView;


@protocol EventsFakeTabbarViewDelegate <NSObject>

- (void)eventsFakeTabbarView:(EventsFakeTabbarView*)view didTapButton:(UIButton*)sender;
@end

/**
   This class has been created to provide the same look and feel of ProntoTabbarViewController.
    As we are having linear hierarchy in our design, Having Tabbar inside the hierarchy will affect
    the navigation. So, We are creating this class to provide the same look and feel of tabbar.
    It is not tabbar.
*/
@interface EventsFakeTabbarView : UIView

@property (nonatomic, weak) id <EventsFakeTabbarViewDelegate> delegate;

+ (EventsFakeTabbarView*)GetEventsFakeTabbarView;
+ (EventsFakeTabbarView*)GetEventsFakeTabbarViewWithFrames:(CGRect)frame;
+ (CGFloat)GetEventsFakeTabbarViewHeight;

- (void) setTabBarItemFrame;
- (IBAction)tabBarItemTapped:(UIButton*)sender;

- (void)makeAllTabBarItemSelectable:(BOOL)selectable;

@end
