//
//  EventsFakeTabbarViewController.m
//  Pronto
//
//  Created by m-666346 on 16/08/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "EventsFakeTabbarView.h"
#import "ProntoTabbarViewController.h"
#import "AppDelegate.h"
#import "UIColor+ColorDirectory.h"


const CGFloat EventFakeTabBarItemWidth      = 104;
const CGFloat EventFakeNumberOfTabBarItems  = 4;
const CGFloat EventFakeTabBarItemHeight     = 54;


@interface EventsFakeTabbarView ()

@property (strong, nonatomic) IBOutlet UIView *tabBarView;
@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIButton *franchiseTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *competitorTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *toolsTrainingTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *eventsTabbarItem;

@property (nonatomic, assign) NSInteger currentSelectedIndex;

@end

@implementation EventsFakeTabbarView

+ (EventsFakeTabbarView*)GetEventsFakeTabbarView
{
    return (EventsFakeTabbarView*)[[[NSBundle mainBundle]loadNibNamed:@"EventsFakeTabbarView"
                                                                owner:self
                                                              options:nil] lastObject];
}

+ (EventsFakeTabbarView*)GetEventsFakeTabbarViewWithFrames:(CGRect)frame
{
    EventsFakeTabbarView *fakeTabbarView = [self GetEventsFakeTabbarView];
    fakeTabbarView.frame = frame;
    return fakeTabbarView;
}

+ (CGFloat)GetEventsFakeTabbarViewHeight
{
    return EventFakeTabBarItemHeight;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setTabBarItemFrame];
    [self prepareFakeTabbarView];
}

-(void)viewDidLayoutSubviews
{
    [self setTabBarItemFrame];
}

- (void)prepareFakeTabbarView
{
    [self.franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseUnselect"] forState: UIControlStateNormal];
    [self.competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compUnselect"] forState: UIControlStateNormal];
    [self.toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsUnselect"] forState: UIControlStateNormal];
    [self.eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventSelect"] forState: UIControlStateNormal];
    self.franchiseTabbarItem.backgroundColor = [UIColor clearColor];
    self.competitorTabbarItem.backgroundColor = [UIColor clearColor];
    self.toolsTrainingTabbarItem.backgroundColor = [UIColor clearColor];
    self.eventsTabbarItem.backgroundColor = [UIColor GetColorFromHexValue071D49];
}

- (IBAction)tabBarItemTapped:(UIButton*)sender
{
    
    if (self.delegate != nil)
    {
        [self.delegate eventsFakeTabbarView:self didTapButton:sender];
    }
}

- (void) setTabBarItemFrame
{
    CGFloat xOrigin =  ([UIScreen mainScreen].bounds.size.width - (EventFakeTabBarItemWidth * EventFakeNumberOfTabBarItems)) / 2;
    self.franchiseTabbarItem.frame = CGRectMake(xOrigin, 0, EventFakeTabBarItemWidth, EventFakeTabBarItemHeight);
    self.competitorTabbarItem.frame = CGRectMake(xOrigin + EventFakeTabBarItemWidth, 0, EventFakeTabBarItemWidth, EventFakeTabBarItemHeight);
    self.toolsTrainingTabbarItem.frame = CGRectMake(xOrigin + (EventFakeTabBarItemWidth * 2), 0, EventFakeTabBarItemWidth, EventFakeTabBarItemHeight);
    self.eventsTabbarItem.frame = CGRectMake(xOrigin + (EventFakeTabBarItemWidth * 3), 0, EventFakeTabBarItemWidth, EventFakeTabBarItemHeight);
}

- (void)makeAllTabBarItemSelectable:(BOOL)selectable
{
    self.franchiseTabbarItem.userInteractionEnabled     = selectable;
    self.competitorTabbarItem.userInteractionEnabled    = selectable;
    self.toolsTrainingTabbarItem.userInteractionEnabled = selectable;
    self.eventsTabbarItem.userInteractionEnabled        = selectable;
}

@end
