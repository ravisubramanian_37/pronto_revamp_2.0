//
//  EventsServiceManager.h
//  Pronto
//
//  Created by Praveen Kumar on 24/01/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@class EventOnlyAssetHandler;
@class EventOnlyAsset;

@interface EventsServiceManager : NSObject

+ (id)sharedServiceManager;
-(void)syncEventsAndOpenAssetForAssetId:(NSString*)assetId userFranchise:(NSString*)franchise andDelegate:(id)delegate;
-(void)validateAndShowEventOnlyAsset:(EventOnlyAsset*)eventOnlyAsset withFranchise:(NSString*)franchise andDelegate:(id)delegate;

@property (nonatomic, weak) AppDelegate *mainDelegate;
//Event Only Asset Handler
@property (nonatomic, strong) EventOnlyAssetHandler *eventOnlyAssetHandler;

@end
