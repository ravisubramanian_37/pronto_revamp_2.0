//
//  EventsServiceManager.m
//  Pronto
//
//  Created by Praveen Kumar on 24/01/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

#import "EventsServiceManager.h"
#import "ZMAbbvieAPI.h"
#import "AppUtilities.h"
#import "EventCoreDataManager.h"
#import "EventOnlyAssetHandler.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

@implementation EventsServiceManager

+ (id)sharedServiceManager
{
    @synchronized(self)
    {
        static EventsServiceManager *sharedInstance = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            sharedInstance = [[self alloc] init];
        });
        return sharedInstance;
    }
}

-(void) syncEventsAndOpenAssetForAssetId:(NSString*)assetId userFranchise:(NSString*)franchise andDelegate:(id)delegate
{
    self.mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingAllEvents];
    
    [[EventsServiceClass sharedAPIManager] syncAllEvents:^(NSDictionary *allEvents) {
        
        [[EventCoreDataManager sharedInstance] saveAllEvents:allEvents withCompletionHandler:^(BOOL success, NSArray *events) {
            
            EventOnlyAsset* eoaAsset = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:assetId];
            if(eoaAsset != nil)
            {
                [self validateAndShowEventOnlyAsset:eoaAsset withFranchise:franchise andDelegate:delegate];
            }
            else
            {
                // After the syncing the local database, we don't found asset, showing message
                [self.mainDelegate showMessage:ProntoMessageAssetNotFound whithType:ZMProntoMessagesTypeWarning];
            }
        }];
        
    } error:^(NSError *error) {
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:@"Getting Events"];
    }];
}

- (void)validateAndShowEventOnlyAsset:(EventOnlyAsset*)eventOnlyAsset withFranchise:(NSString*)franchise andDelegate:(id)delegate
{    
    self.eventOnlyAssetHandler = [[EventOnlyAssetHandler alloc]init];
    self.eventOnlyAssetHandler.delegate      = delegate;
    
    self.eventOnlyAssetHandler.userFranchise = franchise;
    self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
    
    //Setting delay for save view will not collide with download asset
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.eventOnlyAssetHandler validateAndShowEventOnlyAsset:eventOnlyAsset];
    });
}

@end
