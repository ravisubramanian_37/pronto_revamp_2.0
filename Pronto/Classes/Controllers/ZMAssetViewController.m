//
//  ZMAssetViewController.m
//  Pronto
//
//  Created by Sebastian Romero on 2/21/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMAssetViewController.h"
#import "ZMAssetsLibrary.h"
#import "AppDelegate.h"
//#import "ZMLibrary.h"
//#import "Competitors.h"
//#import "ToolsTraining.h"
#import "FastPDFKitToolbar.h"
#import "Pronto-Swift.h"
#import "Constant.h"
//#import "GlobalSearchVC.h"
#import "EventsViewController.h"
#import "ProntoUserDefaults.h"
#import "FeedbackView.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"
#import "AssetsServiceClass.h"
#import "CalendarDetailVC.h"

@interface ZMAssetViewController ()
{
    __weak IBOutlet UIButton *tmpBack;
    FastPDFKitToolbar *toolBar;
}

//@property (nonatomic, strong) FeedbackView *feedbackView;
@property (nonatomic, strong)  UIView *backgroundView;
@property (nonatomic, strong) UIView* statusBar;

@end

@implementation ZMAssetViewController
@synthesize userFranchise,viewMain,webviewScreen;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Initializes the theme
    self.theme = [ZMProntoTheme sharedTheme];
    [self.theme styleMainView:self.view];
    [self populatePrevious_CurrentAssetTitle];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

            HomeMainHeaderView *mainHeaderView = [[[NSBundle mainBundle] loadNibNamed:RedesignConstants.mainViewHeaderNib owner:nil options:nil] objectAtIndex:0];
            [self.headerView addSubview:mainHeaderView];
            [mainHeaderView setThemes];
            [self.headerContainer addSubview:mainHeaderView];

            [mainHeaderView configureHeaderForAssetPage];
            mainHeaderView.translatesAutoresizingMaskIntoConstraints = NO;
          [mainHeaderView pinEdgesTo:self.headerContainer];


}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:RedesignConstants.settingsOptionForAsset];

}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self headerAdjust];
}

- (void)viewDidLayoutSubviews
{

}

- (void)adjustHeaderForEventOnlyAsset
{
 
}

- (void)adjustHeaderForOnlyAsset
{
    self.statusBar = [[UIView alloc]init];
    if (@available(iOS 13.0, *)) {
        self.statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
    }
    else
    {
        self.statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    }

    [[UIApplication sharedApplication].keyWindow addSubview:self.statusBar];
    //Adjust Detail Bar
    CGRect assetDetailFrame = _assetDetailBar.frame;
    assetDetailFrame.size.width = _headerView.frame.size.width;
    _assetDetailBar.frame = assetDetailFrame;
    
    CGRect closeFrame = _closeButton.frame;
    closeFrame.origin.x = _headerView.frame.size.width - _closeButton.frame.size.width - 10;
    _closeButton.frame = closeFrame;
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2) {
        [_favouriteButton setTitle:@" " forState:UIControlStateNormal];
        [_shareButton setTitle:@" " forState:UIControlStateNormal];
        [_favouriteButton setFrame:CGRectMake(_favouriteButton.frame.origin.x, _favouriteButton.frame.origin.y, 40, 40)];
        [_shareButton setFrame:CGRectMake(_shareButton.frame.origin.x, _shareButton.frame.origin.y, 40, 40)];
    }
    else {
        [_favouriteButton setTitle:@"Add to Favorites" forState:UIControlStateNormal];
        [_shareButton setTitle:@"Share" forState:UIControlStateNormal];
        [_favouriteButton setFrame:CGRectMake(_favouriteButton.frame.origin.x, _favouriteButton.frame.origin.y, 183, 40)];
        [_shareButton setFrame:CGRectMake(_shareButton.frame.origin.x, _shareButton.frame.origin.y, 85, 40)];
    }
    _favouriteButton.titleLabel.textColor = [[RedesignThemesClass sharedInstance] hexStringToUIColorWithHex:[RedesignThemesClass sharedInstance].royalBlueCode];
    _shareButton.titleLabel.textColor = [[RedesignThemesClass sharedInstance] hexStringToUIColorWithHex:[RedesignThemesClass sharedInstance].royalBlueCode];
    [_shareButton setFrame:CGRectMake(_favouriteButton.frame.origin.x + _favouriteButton.frame.size.width + 10, _shareButton.frame.origin.y, _shareButton.frame.size.width, _shareButton.frame.size.height)];
    
    //Adjust Description Bar
    CGRect assetDescriptionFrame = _assetDescriptionBar.frame;
    assetDescriptionFrame.size.width = _headerView.frame.size.width;
    _assetDescriptionBar.frame = assetDescriptionFrame;
    
    CGRect previousDescriptionFrame = _previousAssetContainer.frame;
    previousDescriptionFrame.origin.x = _headerView.frame.size.width - _previousAssetContainer.frame.size.width;
    _previousAssetContainer.frame = previousDescriptionFrame;
    
    _previousAssetButton.frame = previousDescriptionFrame;
    
    [_previousAssetTitle setFrame:CGRectMake(_previousAssetTitle.frame.origin.x , _previousAssetTitle.frame.origin.y, 300, _previousAssetTitle.frame.size.height)];
    
  
    if (_asset.assetID != nil)
    {
        NSArray *favfranchisee = [[ZMCDManager sharedInstance] getFavFranchiseById:_asset.assetID];
        if (favfranchisee.count > 0)
        {

                [_favouriteButton setImage:[UIImage imageNamed:RedesignConstants.favoSelected] forState:UIControlStateNormal];
                [_favouriteButton setSelected: YES];

        }
        else
        {
            [_favouriteButton setImage:[UIImage imageNamed:RedesignConstants.favoUnSelected] forState:UIControlStateNormal];
            [_favouriteButton setSelected: NO];
        }
    }
}

-(void)adjustStatusBar
{
    CGRect frame = self.statusBar.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    self.statusBar.frame = frame;
}

-(void) headerAdjust
{
    
    if (self.isEventOnlyAsset == YES)
    {
        [self adjustHeaderForEventOnlyAsset];
    }
    else
    {
        [self adjustHeaderForOnlyAsset];
    }
}

- (void) showHideHeader:(BOOL)hide
{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender
{
    [[ProntoUserDefaults userDefaults] setPreviousAsset:self.asset];

    
        int count = (int)self.navigationController.viewControllers.count;
        for(int i= (count - 1);i>=0;i--){
            if([[ProntoRedesignSingletonClass sharedInstance]isFavouritesChosen]){
                if([self.navigationController.viewControllers[i] isKindOfClass:[MyFavouritesDetailsViewController class]]){
                  [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.notificationforListAndGridChange object:self userInfo:nil];
                  [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.favouritesRemovalNotification object:self userInfo:nil];
                    [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                    break;
                }else if([self.navigationController.viewControllers[i] isKindOfClass:[HomeViewController class]]){
                    [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                }
            }else{
                if([self.navigationController.viewControllers[i] isKindOfClass:[CategoriesViewController class]]){
//                  [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.favouritesAssetUpdationNotification object:self userInfo:nil];
                    [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                    break;
                }else if([self.navigationController.viewControllers[i] isKindOfClass:[CalendarDetailVC class]]){
                    [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                                   break;
                }else if([self.navigationController.viewControllers[i] isKindOfClass:[SearchResultsViewController class]]){
                    [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                                   break;
                }else if([self.navigationController.viewControllers[i] isKindOfClass:[HomeViewController class]]){
                    [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                }
            }
        }

}


- (void)setAsset:(Asset *)asset {
    if (asset) {
        _asset = asset;
        _asset.view_count= [NSNumber numberWithInt:_asset.view_count.intValue+1];
        _asset.path = RedesignConstants.brightCoveText;
        
        [ZMUserActions sharedInstance].countOfAssetDict = 0;
        
        [[ZMCDManager sharedInstance]updateAsset:_asset success:^{
            
            [[ZMAbbvieAPI sharedAPIManager]saveViewedAsset:asset.assetID type:asset.type complete:^(NSDictionary *savedAssetDetails){
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"savedAssetDetails:%@",savedAssetDetails]];}
                error:^(NSError *error) {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"error:%@",error]];
            } ];
            
            [self setViewEvent];
        } error:^(NSError * error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on ZMAssetViewController %@", error.localizedDescription]];
        }];

    }
}

- (void)setViewEvent {
    if (_asset) {
        ZMEvent *plus1view = [[ZMEvent alloc] init];
        plus1view.assetId = _asset.assetID.longValue;
        plus1view.eventType = ZMEventView;
        plus1view.eventValue = [NSString stringWithFormat:@"%d",1];
        [[ZMAssetsLibrary defaultLibrary] saveEvent:plus1view];
//        [_library markAssetAsRead:_asset];
        [[CategoryViewModel sharedInstance]markAssetAsReadWithSelectedAsset:_asset];
        [self assetControllerDelegateSendEvent];
    }
}

- (void)addActivityIndicator {
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = self.view.center;
    spinner.color = [UIColor blueColor];
    [self.view addSubview:spinner];
    self.assetDetailBar.userInteractionEnabled = NO;
    [spinner startAnimating];

    
    double delayInSeconds = 8.0; // set the time
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [spinner stopAnimating];
            self.assetDetailBar.userInteractionEnabled = YES;
        });
}

- (IBAction)favouriteAction:(id)sender {
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed:@"Favo_unselected"] forState:UIControlStateNormal];
        [sender setSelected:NO];
        
        if(_asset){
            [[MyFavouritesViewModel sharedInstance] deleteAssetFromFavouritesWithAsset:_asset];
            [self addActivityIndicator];
        }
    }
    else
    {
        [sender setImage:[UIImage imageNamed:@"Favo_selected"] forState:UIControlStateNormal];
        [sender setSelected:YES];
       
        if(_asset){
            [[MyFavouritesViewModel sharedInstance] createFavObjectWithSelectedAsset:_asset];
            [self addActivityIndicator];
        }
    }
}

- (IBAction)shareAction:(id)sender {
    // Email to self button is pressed - showing mail client
    NSString* emailId = [AOFLoginManager sharedInstance].mail ;
    
//    if ([MFMailComposeViewController canSendMail])
//    {
//        if(emailId == nil || [emailId isEqual: [NSNull null]])
//        {
//            [self sendEmail:@""];
//        }
//        else{
          [self sendEmail:@""];
//            [self sendEmail:@""];
//        }
//    }
//    else
//    {
//        NSString* msg = @"Your device cannot send email";
//
//        UIAlertController* confirm = [UIAlertController alertControllerWithTitle:@""  message:msg  preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* okButton = [UIAlertAction
//                                   actionWithTitle:@"OK"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
//
//                                   }];
//
//        [confirm addAction:okButton];
//        [self presentViewController:confirm animated:YES completion:nil];
//
//    }
}

//Navigating to previous asset
- (IBAction)previousButtonAction:(id)sender
{
  [ProntoAssetHandlingClass sharedInstance].selectedGridCell = nil;
    NSDictionary* previousAsset = [[ProntoUserDefaults userDefaults] getPreviousAsset];

  if (previousAsset != nil && previousAsset.count > 0)
  {
    if(([previousAsset objectForKey:@"id"] != nil) &&
       [[previousAsset objectForKey:@"id"] integerValue] == [self.asset.assetID integerValue])
    {
      return;
    }
    NSNumber * idVal = [previousAsset objectForKey:@"id"];
    NSString * assetType = [previousAsset objectForKey:@"file_mime"];
//    Asset * savedAsset = [[ZMProntoManager sharedInstance]retrieveAssetWithAssetID: idVal];
    [[ProntoUserDefaults userDefaults] setPreviousAsset:_asset];
    NSArray * savedAsset = [[ZMCDManager sharedInstance]getDownloadedAsset:idVal.stringValue];
    if (savedAsset.count > 0 && [assetType isEqualToString: @"application/pdf"]) {
      [[ProntoAssetHandlingClass sharedInstance]openAsset:savedAsset[0]];
    } else {
      NSArray * assetVal = [[ZMCDManager sharedInstance]getAssetArrayById:idVal.stringValue];
      if(assetVal.count > 0) {
        [[ProntoAssetHandlingClass sharedInstance]openAsset:assetVal[0]];
      }
    }
  }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {

}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {

    [self adjustStatusBar];

}

// send mail on selecting "Share" by showing mail client
-(void) sendEmail:(NSString*)emailId
{
    NSString* mailSubject = [NSString stringWithFormat:@"%@ is Sharing a Pronto Asset",[AOFLoginManager sharedInstance].givenName];
    
    NSString* wcUser = [NSString stringWithFormat:@"<div>Hello ,</div>\n"];
    NSString* deeplink = [NSString stringWithFormat:@"abbviepronto://%@",_asset.assetID];
    NSString* strHtmlBody = [NSString stringWithFormat:
                             @"\nPlease click <b><u><a href=\"%@\">here</a></u></b> to open the asset '%@' in Pronto.You will not be able to open the asset unless you have the Pronto app on your iPad.",
                             deeplink,_asset.title];
    NSMutableString *body = [NSMutableString string];
    [body appendString:wcUser];
    [body appendString:@"<div><br/></div>"];
    [body appendString:strHtmlBody];
    [body appendString:@"<div><br/></div>"];
    [body appendString:@"<div>Thanks</div>"];
  
  if ([MFMailComposeViewController canSendMail]) {
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    [mail setSubject:mailSubject];
    
    [mail setMessageBody:body isHTML:YES];
    [mail setToRecipients:@[emailId]];
    mail.delegate = self;
    [self presentViewController:mail animated:YES completion:NULL];
  } else {
    [RedesignConstants sendByURLTo:emailId subject:mailSubject body:body isHtml:YES];
  }
}

// --------------------------------
//
// Function: didFinishWithResult
// Description: Used to return the result of the mail sent
//
// —————————————————------------------

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString* msg = nil;
    
    switch (result)
    {
        case MFMailComposeResultSent:
            if([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
                msg = RedesignConstants.mailSentSuccess;
            else
                msg = RedesignConstants.mailConnectionError;
            break;
        case MFMailComposeResultFailed:
            msg = RedesignConstants.mailSentError;
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    // show the message at the bottom of the screen
    AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(msg != nil)
    {
        [mainDelegate showMessage:msg whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    }
}

- (void)assetControllerDelegateSendEvent {
    
    if (_asset) {
        dispatch_async(dispatch_queue_create("Sending Events", NULL), ^{
            [[AssetsServiceClass sharedAPIManager] uploadEvents:^{
                [AbbvieLogging logInfo:@">>> Events Synced"];
            } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with uploadEvents: %@", error]];
            }];
        });
    }
}

- (void)populatePrevious_CurrentAssetTitle
{
    //Display present and previous asset
    NSDictionary* previousAsset = [[ProntoUserDefaults userDefaults] getPreviousAsset];
    if (previousAsset != nil && previousAsset.count > 0)
    {
        if([[previousAsset objectForKey:@"id"] integerValue] == [self.asset.assetID integerValue])
        {
            self.previousAssetTitle.text = PreviousAssetEmptyString;
        }
        else
        {
            self.previousAssetTitle.text = [previousAsset objectForKey:@"name"];
        }
    }
    else
    {
        self.previousAssetTitle.text = PreviousAssetEmptyString;
    }
    
    self.assetTitle.text = self.asset.title;
}

+ (NSString *)getAssetType:(Asset *)asset{
    
    NSString *extension = [asset.uri_full pathExtension];
   // NSRange range = [asset.uri_full rangeOfString:@"mp4|mov|avi|m4v|brightcove" options:NSRegularExpressionSearch];
    // PRONTO-25 - Web View - Ability to provide a Web link as an asset type.
    BOOL isMimeType = ([asset.file_mime isEqualToString:@"weblink"]);
    BOOL isVideo = [asset.file_mime isEqualToString:@"video/brightcove"];

//    if (range.location != NSNotFound) {
    if(isVideo){
        extension = @"video";
    }
    // PRONTO-25
    else if(isMimeType)
         extension = @"weblink";
    
    return extension;
}




#pragma mark ZMAssetViewControllerDelegate Methods

-(void)assetViewController:(ZMAssetViewController*)assetVCView didTapLastViewedButton:(UIButton*)sender forAsset:(NSObject*)asset{
    
}
    
-(void)pdfDocument:(FastPDFViewController*)pdfDocumentView didTapLastViewedForAsset:(NSObject*)asset{
    
}


@end

