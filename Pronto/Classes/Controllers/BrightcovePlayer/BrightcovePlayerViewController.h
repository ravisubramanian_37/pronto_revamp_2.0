//
//  BrightcovePlayerViewController.h
//  Pronto
//
//  Created by m-666346 on 22/06/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrightcovePlayerViewController : UIViewController
@property (nonatomic, strong) NSString *videoID;

/**
 *  property to manage and show duration of video
 */
@property (nonatomic, assign) long long duration;

// Provides ready to play instance variable
+ (BrightcovePlayerViewController*)GetBrightcovePlayerViewController;

// Player Helper Methods
- (void)play;
- (void)resetVideo;
- (void)pause;
- (void)resume;

@end
