//
//  PreferencesVCViewModel.m
//  Pronto
//
//  Created by m-666346 on 14/05/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "PreferencesVCViewModel.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Preferences+CoreDataClass.h"
#import "Preferences+CoreDataProperties.h"
#import "ZMAbbvieAPI.h"
#import "Franchise+CoreDataProperties.h"
#import "Pronto-Swift.h"
#import "Brand+CoreDataProperties.h"
#import "ZMUserActions.h"

#import "InitialLoadServiceClass.h"


@implementation PreferencesVCViewModel

- (BOOL)isAvailableLocalSavedPreferences
{
    NSString *userName = [AOFLoginManager sharedInstance].displayName;
//    Preferences *preference = [Preferences MR_findFirstByAttribute:@"userName"
//                                                         withValue:userName];
//    if (preference != nil && preference.franchieses != nil && preference.franchieses.count > 0)
//    {
//        return YES;
//    }
//    return NO;
    
    NSFetchRequest * fetchrequestPreferences = [Preferences MR_requestAll];
    NSArray *importedPreferences = [Preferences MR_executeFetchRequest:fetchrequestPreferences];
    if(importedPreferences.count >0){
    Preferences *preferences = [importedPreferences objectAtIndex:0];
    if([[preferences valueForKey:@"userName"] isEqualToString:userName]){
//        if (preferences.franchieses != nil && preferences.franchieses.count > 0){
//                return YES;
//        }
        if([[preferences valueForKey:@"savedFranchise"] boolValue]){
            return YES;
        }
    }
    }
    return NO;
}

//- (void) fetchPreferencesFromServerWithCompletionHandler:(void(^)(BOOL success))completionHandler
//{
//    [[InitialLoadServiceClass sharedAPIManager] fetchPrefernces:^(NSArray *preferencesFetched)
//     {
//         [AbbvieLogging logInfo:[NSString stringWithFormat:@"preferencesFetched:%@",preferencesFetched]];
//         NSMutableArray *savedPreferences = [NSMutableArray array];
//
//         for (NSDictionary *dict in preferencesFetched)
//         {
//             if (dict[@"name"] != nil)
//             {
//                 [savedPreferences addObject:dict[@"name"]];
//             }
//         }
//         // what if saved preferences is zero
//
//         [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//
//             NSString *userName = [AOFLoginManager sharedInstance].displayName;
//             Preferences *preference = [Preferences MR_findFirstByAttribute:[Preferences GetUserNameAsString]
//                                                                  withValue:userName inContext:localContext];
//             if (preference == nil)
//             {
//                 //There is no Entry of preference for this user
//                 preference = [Preferences MR_createEntityInContext:localContext];
//                 preference.userName     = userName;
//                 preference.franchieses  = savedPreferences;
//             }
//             else
//             {
//                 preference.franchieses = savedPreferences;
//             }
//         } completion:^(BOOL success, NSError *error) {
//
//             if (completionHandler != nil)
//             {
//                 completionHandler(YES);
//             }
//
//         }];
//
//     } error:^(NSError *error) {
//         completionHandler(NO);
//     }];
//
//}

+ (NSArray*)getFranchisesAndBrands
{
    NSArray *franchiese = [PreferencesVCViewModel getAllFranchieseName];
    NSMutableArray *franchisesAndBrands = [NSMutableArray array];
    
    if (franchiese != nil & franchiese.count > 0) // There is Franchiese
    {
        for (NSString *franchieseLabel in franchiese)
        {
            NSArray * franchiseArray = [[ZMCDManager sharedInstance] getFranchiseByName:franchieseLabel];
            if (franchiseArray.count > 0)
            {
                Franchise * franchise = franchiseArray.firstObject;
                if (franchise.franchiseID != nil)
                {
                    [franchisesAndBrands addObject:franchise.franchiseID];
                }
//                for (Brand * brand in franchise.brands)
//                {
//                    if (brand.brandID != nil)
//                    {
//                        [franchisesAndBrands addObject:brand.brandID];
//                    }
//                }
            }
        }
    }
    ZMProntoManager.sharedInstance.franchisesAndBrands = franchisesAndBrands;
    return franchisesAndBrands;
}

+ (NSArray*)getAllFranchieseName
{
    NSString *userName = [AOFLoginManager sharedInstance].displayName;
    Preferences *preference = [Preferences MR_findFirstByAttribute:@"userName"
                                                         withValue:userName];
    return preference.franchieses;
}

//(void (^)(NSArray *franchiseArray, NSArray* brandsArray))success
//- (void)initializeFranchiseAndBrandsWithCompletionHandler:(void (^)(BOOL success))success
//{
//    if([ZMUserActions sharedInstance].isLoginSuccess)
//    {
//        [[ZMAbbvieAPI sharedAPIManager] synchronizeBrands:^{
//
//            [AbbvieLogging logInfo:@">>>> Finished sycronizing brands "];
//            NSString *franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//            [AbbvieLogging logInfo:[NSString stringWithFormat:@"-- franachise id %@", [[ZMCDManager sharedInstance] getFranchiseById:franchiseId]]];
//            if ([[ZMCDManager sharedInstance] getFranchiseById:franchiseId].count > 0) {
//                Franchise * franchise = [[ZMCDManager sharedInstance] getFranchiseById:franchiseId].firstObject;
//                [ZMUserActions sharedInstance].franchiseId = franchise.franchiseID.stringValue;
//                [ZMUserActions sharedInstance].franchiseName = franchise.name;
//            }
//            success(YES);
//
//        } error:^(NSError *error) {
//            success(NO);
//        }];
//    }
//}

- (BOOL)isFranchiseAndBrandsAvailable
{
    //if saved preferences is available and franchise and franchise and brands not avialble don't consider
    // the value of saved preferences;
    NSArray *franchiseAndBrands = [PreferencesVCViewModel getFranchisesAndBrands];
    
    if (franchiseAndBrands != nil && franchiseAndBrands.count > 0)
    {
        return YES;
    }
    return NO;
}

@end
