//
//  PreferencesVCViewModel.h
//  Pronto
//
//  Created by m-666346 on 14/05/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PreferencesVCViewModel : NSObject

- (BOOL)isAvailableLocalSavedPreferences;
//- (void) fetchPreferencesFromServerWithCompletionHandler:(void(^)(BOOL success))completionHandler;
+ (NSArray*)getFranchisesAndBrands;
- (BOOL)isFranchiseAndBrandsAvailable;
//- (void)initializeFranchiseAndBrandsWithCompletionHandler:(void (^)(BOOL success))success;
@end
