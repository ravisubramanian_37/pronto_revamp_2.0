//
//  PreferencesListVCViewController.m
//  Pronto
//
//  Created by m-666346 on 19/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "PreferencesListViewController.h"
#import "PreferenceListCell.h"
#import "FadeTransitionAnimator.h"
#import "UIColor+ColorDirectory.h"
#import "Pronto-Swift.h"
#import "Franchise.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Preferences+CoreDataClass.h"
#import "Preferences+CoreDataProperties.h"
#import "Constant.h"
#import "ZMProntoPhase3.h"
#import "ZMLibrary.h"
#import "ZMUserActions.h"
#import "ZMAbbvieAPI.h"

#import "InitialLoadServiceClass.h"


NSString * const kDefaultFranchieseHomeOffice = @"Home Office";

@interface PreferencesListViewController () <UIViewControllerTransitioningDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *preferenceListBGView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UICollectionView *preferenceListCollectionView;
@property (weak, nonatomic) IBOutlet UILabel  *selectFranchieseLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectAllButton;
@property (weak, nonatomic) IBOutlet UIButton *savePreferencesButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@property (weak, nonatomic) IBOutlet UIView *errorMessageContainerView;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;
@property (weak, nonatomic) IBOutlet UILabel  *errorMessageLabel;


@property (nonatomic,strong) NSArray *preferencesList;
@property (nonatomic, strong) NSMutableArray *selectedPreferencesList;
@property (nonatomic, strong) NSMutableArray *selectedList;
@property (nonatomic, assign) BOOL isAllFranchiesSelected;
@property (nonatomic, assign) BOOL isLoadingData; // Make it false at appropiate places


@end

@implementation PreferencesListViewController
+ (id)sharedInstance {
    
    @synchronized(self)
    {
        static PreferencesListViewController *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}

- (void)setupUI
{
    [self setupPreferenceListViewControllerFrame];
    [self.selectAllButton setTitleColor:[UIColor GetColorFromHexValue36a2c6] forState:UIControlStateNormal];
    [self.savePreferencesButton setTitleColor:[UIColor GetColorFromHexValue36a2c6] forState:UIControlStateNormal];
    self.preferenceListBGView.backgroundColor   = [UIColor whiteColor];
    self.containerView.layer.cornerRadius       = 10.0;
    self.selectFranchieseLabel.textColor        = [UIColor GetColorFromHexValue2d2926];
    
    // ERROR message UI set up
    [self setupErrorMessageUI];

    [self registerCollectionViewCell];
    
    //NSArray *savedFranchiese = [self getAllSavedFranchieseName];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *savedFranchiese = [userDefaults objectForKey:@"selectedprefList"];
    
    if (savedFranchiese != nil && savedFranchiese.count > 0 && savedFranchiese.count >= self.preferencesList.count )
    {
        [self.selectAllButton setTitle:NSLocalizedString(@"Unselect All", @"Unselect All") forState:UIControlStateNormal];
        self.isAllFranchiesSelected = YES;
    }else if (savedFranchiese.count == 0){
        [self fetchPreferences];
    }
}

- (void)setupErrorMessageUI
{
    self.errorMessageLabel.text = NSLocalizedString(@"Oops, Something went wrong, Please try again.", "");
    [self.retryButton setTitle:NSLocalizedString(@"Try again", "") forState:UIControlStateNormal];
    self.errorMessageContainerView.hidden = YES;
    self.errorMessageLabel.textColor      = [UIColor GetColorFromHexValue2d2926];
    [self.retryButton setTitleColor:[UIColor GetColorFromHexValue36a2c6] forState:UIControlStateNormal];
    self.retryButton.layer.cornerRadius = 6.0f;
    self.retryButton.layer.borderColor  = [UIColor GetColorFromHexValue36a2c6].CGColor;
    self.retryButton.layer.borderWidth  = 1.0f;
}

// Create view model for franchise list and
// Do code refefatoring it as soon as possible


- (void)startAnimatingIndicatorView:(BOOL)yesOrNo
{
    if (yesOrNo == YES)
    {
        if ([self.activityIndicatorView isAnimating] == NO)
        {
            [self.activityIndicatorView startAnimating];
        }
    }
    else
    {
        if ([self.activityIndicatorView isAnimating] == YES)
        {
            [self.activityIndicatorView stopAnimating];
        }
    }
}

- (void)disableUserInteractionOfButtons:(BOOL)disable
{
    self.selectAllButton.userInteractionEnabled         = !disable;
    self.savePreferencesButton.userInteractionEnabled   = !disable;
    self.preferenceListCollectionView.userInteractionEnabled = !disable;
}

/**
  It is responsible of fetching the Franchiese and brands from the server.
 */
//- (void)syncFranchiseAndBrands
//{
////    if([ZMUserActions sharedInstance].isLoginSuccess)
////    {
//        [[InitialLoadServiceClass sharedAPIManager] synchronizeBrands:^{
//
//            [AbbvieLogging logInfo:@">>>> Finished sycronizing brands "];
//            NSString *franchiseId = [AOFLoginManager sharedInstance].salesFranchiseID;
//            [AbbvieLogging logInfo:[NSString stringWithFormat:@"-- franachise id %@", [[ZMCDManager sharedInstance] getFranchiseById:franchiseId]]];
//            if ([[ZMCDManager sharedInstance] getFranchiseById:franchiseId].count > 0) {
//                Franchise * franchise = [[ZMCDManager sharedInstance] getFranchiseById:franchiseId].firstObject;
//                [ZMUserActions sharedInstance].franchiseId = franchise.franchiseID.stringValue;
//                [ZMUserActions sharedInstance].franchiseName = franchise.name;
//            }
//            [self initializeFranchiseList];
//            [self disableUserInteractionOfButtons:NO];
//            [self startAnimatingIndicatorView:NO];
//            [self.preferenceListCollectionView reloadData];
//
//            //initialize saved preferences
//            [self fetchPreferences];
//        } error:^(NSError *error) {
//
//            [self syncFranchiseAndBrandsFinishedWithError:error];
//        }];
////    }
//}
/**
 * Handle the scenario, when there is error in syncFranchiseAndBrands service call
 */
- (void)syncFranchiseAndBrandsFinishedWithError:(NSError*)error
{
    [self startAnimatingIndicatorView:NO];
    self.isLoadingData = NO;
    self.preferenceListCollectionView.hidden = YES;
    self.errorMessageContainerView.hidden = NO;
}

-(void) fetchPreferences
{
  _preferenceListCollectionView.hidden = YES;
    [self startAnimatingIndicatorView:YES];
    [self disableUserInteractionOfButtons:YES];
    [[InitialLoadServiceClass sharedAPIManager] fetchPrefernces:^(NSArray *preferencesFetched)
     {
         [AbbvieLogging logInfo:[NSString stringWithFormat:@"preferencesFetched:%@",preferencesFetched]];
         NSMutableArray *savedPreferences = [NSMutableArray array];
         NSMutableArray *selectedPreferences = [NSMutableArray array];
         NSMutableArray *savedtest = [NSMutableArray array];
         
        if(![[NSUserDefaults standardUserDefaults]boolForKey:RedesignConstants.isLoadingFirstTime])
        {
            NSLog(@"loading 1st time");
            for (NSDictionary *dict in preferencesFetched)
            {
                if ([dict valueForKey:@"name"] != nil && [dict valueForKey:@"tid"] != nil)
                {
                    [savedPreferences addObject:[dict valueForKey:@"name"]];
                    [selectedPreferences addObject:[dict valueForKey:@"tid"]];
                    NSDictionary *testDict = @{ @"name" : [dict valueForKey:@"name"], @"tid" : [dict valueForKey:@"tid"]};
                    [savedtest addObject:testDict];
                }
            }
            
        }
        else
        {
            NSLog(@"Not loading 1st time");
            for (NSDictionary *dict in preferencesFetched)
            {
   //             if ([dict valueForKey:@"name"] != nil && [[dict valueForKey:@"field_active"] boolValue] == YES)
                if ([dict valueForKey:@"name"] != nil && [dict valueForKey:@"tid"] != nil )
                {
                    if ( [[dict valueForKey:@"field_active"] boolValue] == YES )
                    {
                        [savedPreferences addObject:[dict valueForKey:@"name"]];
                    }
                    [selectedPreferences addObject:[dict valueForKey:@"tid"]];
                    NSDictionary *testDict = @{ @"name" : [dict valueForKey:@"name"], @"tid" : [dict valueForKey:@"tid"]};
                    [savedtest addObject:testDict];
                }
                
            }
        }
             NSArray *franchieseList = [[ZMCDManager sharedInstance] getFranchises];
             NSArray *franchiseNameList = [self getAllFranchiseNameFromFranchises:franchieseList];
             BOOL isSameBoth = [self isOldValue:franchiseNameList sameAsNewValue:savedPreferences];
             if (isSameBoth == YES && savedPreferences.count >= franchieseList.count)
             {
                 [self.selectAllButton setTitle:NSLocalizedString(@"Unselect All", @"Unselect All") forState:UIControlStateNormal];
                 self.isAllFranchiesSelected = YES;
             }
             else
             {
                 [self.selectAllButton setTitle:NSLocalizedString(@"Select All", @"Select All") forState:UIControlStateNormal];
                  self.isAllFranchiesSelected = NO;
             }
             [self initializeFranchiseList];
             self.selectedPreferencesList = savedPreferences;
             self.selectedList = savedtest;
              [[NSUserDefaults standardUserDefaults] setObject:self.selectedPreferencesList forKey:@"selectedprefList"];
             [[NSUserDefaults standardUserDefaults] setObject:savedtest forKey:@"SavedPreferencesList"];
             [[NSUserDefaults standardUserDefaults] setObject:self.selectedPreferencesList forKey:@"totalprefList"];
             [self startAnimatingIndicatorView:NO];
             [self disableUserInteractionOfButtons:NO];
        _preferenceListCollectionView.hidden = NO;
             [self.preferenceListCollectionView reloadData];
             
//         }];
         
     } error:^(NSError *error) {
         [AbbvieLogging logError:[NSString stringWithFormat:@"error fetching prefrences:%@",error]];
         [self startAnimatingIndicatorView:NO];
         [self disableUserInteractionOfButtons:NO];
     }];
    
}

-(void) fetchPreferencesInBackground
{
    [[InitialLoadServiceClass sharedAPIManager] fetchPrefernces:^(NSArray *preferencesFetched)
     {
      [AbbvieLogging logInfo:[NSString stringWithFormat:@"preferencesFetched:%@",preferencesFetched]];
      NSMutableArray *savedPreferences = [NSMutableArray array];
      NSMutableArray *selectedPreferences = [NSMutableArray array];
      NSMutableArray *savedtest = [NSMutableArray array];
      NSLog(@"Preferences loaded");
      for (NSDictionary *dict in preferencesFetched)
      {
        //             if ([dict valueForKey:@"name"] != nil && [[dict valueForKey:@"field_active"] boolValue] == YES)
        if ([dict valueForKey:@"name"] != nil && [dict valueForKey:@"tid"] != nil )
        {
          if ( [[dict valueForKey:@"field_active"] boolValue] == YES )
          {
            [savedPreferences addObject:[dict valueForKey:@"name"]];
          }
          [selectedPreferences addObject:[dict valueForKey:@"tid"]];
          NSDictionary *testDict = @{ @"name" : [dict valueForKey:@"name"], @"tid" : [dict valueForKey:@"tid"]};
          [savedtest addObject:testDict];
        }
        
      }
      self.selectedList = savedtest;
      [[NSUserDefaults standardUserDefaults] setObject:savedtest forKey:@"SavedPreferencesList"];
      NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
      NSArray *savedFranchiese = [userDefaults objectForKey:@"selectedprefList"];
      NSLog(@"saved preferences %@", savedFranchiese);
      [self.preferenceListCollectionView reloadData];
    } error:^(NSError *error) {
         [AbbvieLogging logError:[NSString stringWithFormat:@"error fetching prefrences:%@",error]];
     }];
}


//-(void) fetchPreferencesFromServerWithCompletionHandler:(void(^)(BOOL success))completionHandler
//{
//    [[ZMAbbvieAPI sharedAPIManager] fetchPrefernces:^(NSArray *preferencesFetched)
//     {
//         [AbbvieLogging logInfo:[NSString stringWithFormat:@"preferencesFetched:%@",preferencesFetched]];
//         NSMutableArray *savedPreferences = [NSMutableArray array];
//
//         for (NSDictionary *dict in preferencesFetched)
//         {
//             if (dict[@"name"] != nil)
//             {
//                 [savedPreferences addObject:dict[@"name"]];
//             }
//         }
//         // what if saved preferences is zero
//
//         [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//
//             NSString *userName = [AOFLoginManager sharedInstance].displayName;
//             Preferences *preference = [Preferences MR_findFirstByAttribute:[Preferences GetUserNameAsString]
//                                                                  withValue:userName inContext:localContext];
//             if (preference == nil)
//             {
//                 //There is no Entry of preference for this user
//                 preference = [Preferences MR_createEntityInContext:localContext];
//                 preference.userName     = userName;
//                 preference.franchieses  = savedPreferences;
//             }
//             else
//             {
//                 preference.franchieses = savedPreferences;
//             }
//         } completion:^(BOOL success, NSError *error) {
//
//             if (completionHandler != nil)
//             {
//                 completionHandler(YES);
//             }
//
//         }];
//
//     } error:^(NSError *error) {
//        completionHandler(NO);
//     }];
//
//}

- (NSArray*)getAllFranchiseNameFromFranchises:(NSArray*)franchises
{
    NSMutableArray *franchisesName= [NSMutableArray array];
    for (Franchise *franchise in franchises)
    {
        [franchisesName addObject:franchise.name];
    }
    return franchisesName;
}
- (void)initializeFranchiseList
{
    NSArray *franchieseList = [[ZMCDManager sharedInstance] getFranchises];
    if (franchieseList != nil && franchieseList.count > 0)
    {
        NSMutableArray *tmp = [NSMutableArray array];
        for (Franchise *franchiese in franchieseList)
        {
            if ([franchiese isKindOfClass:[Franchise class]])
            {
                //Remove duplicate names, If any
                if ([tmp containsObject:franchiese.name] == NO)
                {
                    [tmp addObject:franchiese.name];
                }
            }
        }
        
        self.preferencesList = [NSArray arrayWithArray:tmp];
    }
}

- (void)initializeSavedFranchiseList
{
    self.selectedPreferencesList = [NSMutableArray array];
    NSArray *franchiese = [self getAllSavedFranchieseName];
    
    if (franchiese != nil && franchiese.count > 0)
    {
        self.selectedPreferencesList = [franchiese mutableCopy];
    }
    else
    {
        [AbbvieLogging logInfo:@"There is no saved franchise in database"];
//        [self startAnimatingIndicatorView:YES];
//        self.isLoadingData = YES;
//        [self fetchPreferences];
    }
}

- (void)initializeDataStructures
{
    self.isAllFranchiesSelected  = NO;
    self.selectedPreferencesList = [NSMutableArray array];
    self.preferencesList         = [NSMutableArray array];
    
    NSArray *franchieseList = [[ZMCDManager sharedInstance] getFranchises];
    if (franchieseList != nil && franchieseList.count > 0)
    {
        // initialize  data structure from database
        [self initializeFranchiseList];
        [self initializeSavedFranchiseList];
    }
    else
    {
        // initialize  data structure from server
      //  [self startAnimatingIndicatorView:YES];
//        [self disableUserInteractionOfButtons:YES];
        self.isLoadingData = YES;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *totalFranchieseList = [userDefaults objectForKey:@"totalprefList"];
        NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
        totalFranchieseList = [totalFranchieseList sortedArrayUsingDescriptors:@[sd]];
        self.preferencesList = totalFranchieseList;
      NSArray *savedFranchieseList = [userDefaults objectForKey:@"selectedprefList"];
      self.selectedPreferencesList = [savedFranchieseList mutableCopy];
    }
}
- (NSArray*)getAllSavedFranchieseName
{
    NSString *userName = [AOFLoginManager sharedInstance].displayName;
    Preferences *preference = [Preferences MR_findFirstByAttribute:@"userName"
                                                         withValue:userName];
    if(preference.franchieses.count == 0){
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *savedFranchiese = [userDefaults objectForKey:@"selectedprefList"];
        return savedFranchiese;
       // return [ProntoRedesignSingletonClass sharedInstance].savedPreferences;
    }else{
        return [preference.franchieses allObjects];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // don't dismiss view until data loading completes Or select atleast on franchise
//    if (self.isLoadingData == YES || self.selectedPreferencesList.count <= 0) {
//        return ;
//    }
    
//    if (self.selectedPreferencesList.count <= 0) { // for now
//        return ;
//    }
    
    CGPoint location = [touches.anyObject locationInView:self.preferenceListBGView];
    BOOL touchedInsideContainerView = CGRectContainsPoint(self.containerView.frame, location);
    if (touchedInsideContainerView == NO && [ProntoRedesignSingletonClass sharedInstance].callPreferencesApi == true)
    {
        [ProntoRedesignSingletonClass sharedInstance].callPreferencesApi = false;
        [self dismissViewControllerAnimated:YES completion:NULL];
        [[NSNotificationCenter defaultCenter] postNotificationName:kPreferenceListVCDismissNotification
                                                            object:nil];
      if(![[NSUserDefaults standardUserDefaults] boolForKey:@"changepreference"]) {
        [[HomeViewModel sharedInstance]performBackgroundDownload];
        [[HomeViewModel sharedInstance]loadHomeServiceCalls];
      } else{
        [[HomeViewController sharedInstance]loadActualView];
      }
    } else {
      [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeDataStructures];
    [self setupUI];
}

- (void)registerCollectionViewCell
{
    [self.preferenceListCollectionView registerNib:[UINib nibWithNibName:[PreferenceListCell nibName] bundle:nil]
                        forCellWithReuseIdentifier:[PreferenceListCell cellIdentifier]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [UIView animateWithDuration:0.33 animations:^{
        self.preferenceListBGView.backgroundColor = [ZMProntoPhase3 homeOfficeBG];
    }];
}

- (void)setupPreferenceListViewControllerFrame
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    self.view.frame = frame;
    self.preferenceListBGView.frame = frame;
}

- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self setupPreferenceListViewControllerFrame];
    [self.view layoutSubviews];
}

- (IBAction)selectAllButtonAction:(UIButton *)sender
{
    if (self.isAllFranchiesSelected == YES)
    {
        [self.selectAllButton setTitle:NSLocalizedString(@"Select All", @"Select All") forState:UIControlStateNormal];
        [self.selectedPreferencesList removeAllObjects];
    }
    else
    {
        [self.selectAllButton setTitle:NSLocalizedString(@"Unselect All", @"Select All") forState:UIControlStateNormal];
        //All Item selected, remove previous item to save from duplication entry.
        [self.selectedPreferencesList removeAllObjects];
        [self.selectedPreferencesList addObjectsFromArray:self.preferencesList];
    }
    self.isAllFranchiesSelected = !self.isAllFranchiesSelected;
    [self.preferenceListCollectionView reloadData];
}

- (IBAction)retryAgainButtonAction:(UIButton *)sender
{
    //Hide Error Message View
    self.errorMessageContainerView.hidden       = YES;
    self.preferenceListCollectionView.hidden    = NO;
    
//   // do sync franchise and Brands call
    [self initializeDataStructures];
}

- (BOOL)isOldValue:(NSArray*)oldValue sameAsNewValue:(NSArray*)newValue
{
    if (oldValue.count == newValue.count)
    {
        BOOL matchFound = NO;
        for (NSString *singleObjectFromOld in oldValue)
        {
            matchFound = NO;
            for (NSString *singleObjectFromNew in newValue)
            {
                if ([singleObjectFromOld.lowercaseString isEqualToString:singleObjectFromNew.lowercaseString])
                {
                    matchFound = YES;
                    break;
                }
                else
                {
                    matchFound = NO;
                }
            }
        }
        return matchFound;
    }
    return NO;
}
- (IBAction)savePreferencesButtonAction:(UIButton *)sender
{
    [AbbvieLogging logInfo:@"Asset Preference -> savePreferences view dismissed"];
    //If user has not selected  franchise, Stay on the screen
    //Force user to select atleast one franchise
    if (self.selectedPreferencesList.count <= 0)  { return; }
    
    [self startAnimatingIndicatorView:YES];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        NSString *userName = [AOFLoginManager sharedInstance].displayName;
//        Preferences *preference = [Preferences MR_findFirstByAttribute:[Preferences GetUserNameAsString]
//                                                             withValue:userName inContext:localContext];
        NSFetchRequest * fetchrequestPreferences = [Preferences MR_requestAll];
        NSArray *importedPreferences = [Preferences MR_executeFetchRequest:fetchrequestPreferences];
        [ProntoRedesignSingletonClass sharedInstance].savedPreferences = self.selectedPreferencesList;
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:self.selectedPreferencesList forKey:@"selectedprefList"];
        [userDefaults synchronize];
        
        if(importedPreferences.count >0){
        Preferences *preference = [importedPreferences objectAtIndex:0];
            if (preference == nil && ![[preference valueForKey:@"userName"] isEqualToString:userName]){
            //There is no Entry of preference for this user
                if([[preference valueForKey:@"userName"] isEqualToString:userName]){
                    if(self.selectedPreferencesList != nil) {
                        preference.savedFranchise = YES;
                    }
                }else{
                    preference = [Preferences MR_createEntityInContext:localContext];
                    preference.userName     = userName;
//                preference.franchieses  = self.selectedPreferencesList;
                
                    if(self.selectedPreferencesList != nil) {
                
//                    NSMutableSet *set= [NSMutableSet new];
//                    for (int i=0;i<[self.selectedPreferencesList count];i++){
//
//                        [set addObject:self.selectedPreferencesList[i]];
//                    }
//                    [preference setFranchieses:set];
                    
                        preference.savedFranchise = YES;
                    }
                    
                }
            }else{
                
                if([[preference valueForKey:@"userName"] isEqualToString:userName]){
                    if(self.selectedPreferencesList != nil) {
                        preference.savedFranchise = YES;
                    }
                }
//                BOOL isSameBoth = [self isOldValue:preference.savedFranchise sameAsNewValue:self.selectedPreferencesList];
//                if (isSameBoth == NO){
////                    preference.franchieses = self.selectedPreferencesList;
//
//                }
            }
        }else{
            Preferences *preference = [Preferences MR_createEntityInContext:localContext];
            preference.userName     = userName;
//                preference.franchieses  = self.selectedPreferencesList;
            
            if(self.selectedPreferencesList != nil && self.selectedPreferencesList.count > 0) {
            
                preference.savedFranchise = YES;
//                NSMutableSet *set= [NSMutableSet new];
                
//                NSFetchRequest * fetchrequestFranchise = [Franchise MR_requestAll];
//                NSArray *importedFranchise = [Franchise MR_executeFetchRequest:fetchrequestFranchise];
//
//                    for(Franchise *cat in importedFranchise){
//                        for(int i=0;i<self.selectedPreferencesList.count;i++){
//                            if([[self.selectedPreferencesList objectAtIndex:i]isEqualToString:[cat valueForKey:@"name"]]){
//                                [set addObject:cat];
//                                    break;
//                                }
//                            }
//                        }
//                NSSet *set1 = [NSSet setWithSet:set];
                //                [preference setFranchieses:set1];
//                [preference setFranchieses:set1];
//                preference.franchieses = set1;
            }else{
                preference.savedFranchise = NO;
            }
//                NSSet *set1 = [NSSet setWithSet:set];
//                [preference setFranchieses:set1];
                
        }
    } completion:^(BOOL success, NSError *error)
    {
//        if (success == YES)
//        {
            [self savePreferencesOnServer];
//        }
//        else{
//            [self startAnimatingIndicatorView:NO];
//             [self dismissViewControllerAnimated:YES completion:NULL];
//        }
//        [[NSNotificationCenter defaultCenter] postNotificationName:kPreferenceListVCDidSavePreferencesNotification
//                                                            object:nil];
    }];
}

// service call for saving preferences
- (void)savePreferencesOnServer
{

    NSArray *franchiese = self.selectedPreferencesList;
    NSArray *list = [[NSUserDefaults standardUserDefaults] objectForKey:@"SavedPreferencesList"];
    NSMutableArray *finalList = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in list)
    {
        if ([franchiese containsObject:[dict objectForKey:@"name"]])
        {
            [finalList addObject:dict];
        }
    }
 
    NSMutableArray *franchiseNode = [NSMutableArray array];
    
    if (finalList != nil & finalList.count > 0) // There is Franchiese
    {
        for (NSDictionary *dict in finalList) {
            
            [franchiseNode addObject:[dict objectForKey:@"tid"]];
        }
        if (finalList != nil && finalList.count == 1)
        {
            NSString * tidvalue = [[[finalList objectAtIndex:0] objectForKey:@"tid"] stringValue];
            [AOFLoginManager sharedInstance].baseFranchiseID = tidvalue;
        }
        else
        {
            [AOFLoginManager sharedInstance].baseFranchiseID = @"";
        }
        [[NSUserDefaults standardUserDefaults]setValue:[AOFLoginManager sharedInstance].baseFranchiseID forKey: RedesignConstants.baseFranchiseID];
    }
    else
    {
        [franchiseNode addObjectsFromArray:@[]];
    }

    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Saving Preferences %@", franchiseNode]];
    
    [[InitialLoadServiceClass sharedAPIManager] savePreferences:franchiseNode complete:^(NSDictionary *preferencesSavedStatus)
     {
        [self startAnimatingIndicatorView:NO];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:RedesignConstants.isPreferenceRequired];
      [ProntoRedesignSingletonClass sharedInstance].savedCategoryIDs = [NSMutableArray new];
//        UserDefaults.standard.set(true, forKey: RedesignConstants.isPreferenceRequired)
//        [[NSNotificationCenter defaultCenter] postNotificationName:kPreferenceListVCDidSavePreferencesNotification object:nil];
        [self dismissViewControllerAnimated:YES completion:^{
          [HomeViewModel sharedInstance].flowType = _flowType;
            if(![[NSUserDefaults standardUserDefaults] boolForKey:@"changepreference"]) {
              [[HomeViewModel sharedInstance]performBackgroundDownload];
              [[HomeViewModel sharedInstance]loadHomeServiceCalls];
            } else {
              [[HomeViewController sharedInstance]loadActualView];
            }
        }];
         [AbbvieLogging logInfo:[NSString stringWithFormat:@"Saving preferences happened successfully:%@",preferencesSavedStatus]];
     } error:^(NSError *error)
     {
         [self startAnimatingIndicatorView:NO];
         [AbbvieLogging logError:[NSString stringWithFormat:@">>>> Error getting the while saving preferences %@", error]];
     }];
}

#pragma mark - Collection View Data Soure Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  self.preferencesList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PreferenceListCell *cell = (PreferenceListCell*)[collectionView dequeueReusableCellWithReuseIdentifier:[PreferenceListCell cellIdentifier]
                                                                                              forIndexPath:indexPath];
    [self prepareCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)prepareCell:(PreferenceListCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    NSInteger index = [self getIndexForItemAtIndexPath:indexPath];
    NSString *franchieseName = @"";
    if (index <= self.preferencesList.count)
    {
        franchieseName = self.preferencesList[index - 1];
        [cell prepareCellWithFranchieseName:franchieseName];
    }
    
    [self shouldHighlightImageOfCell:cell atIndexPath:indexPath];
}
- (NSInteger)getIndexForItemAtIndexPath:(NSIndexPath*)indexPath
{
    NSInteger index = (indexPath.section * 3) + indexPath.row;
    return index + 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *plist = [self.selectedPreferencesList mutableCopy];
    PreferenceListCell *cell  = (PreferenceListCell*)[self.preferenceListCollectionView cellForItemAtIndexPath:indexPath];
    NSString *franchieseLabel = cell.franchieseNameLabel.text;
    
    if ([self.selectedPreferencesList containsObject:franchieseLabel])
    {
       // [self.selectedPreferencesList removeObject:franchieseLabel];
        [plist removeObject:franchieseLabel];
    }
    else
    {
       // [self.selectedPreferencesList addObject:franchieseLabel];
        [plist addObject:franchieseLabel];
    }
    self.selectedPreferencesList = [plist mutableCopy];
    NSArray *franchieseList = [[ZMCDManager sharedInstance] getFranchises];
    if (self.selectedPreferencesList.count >= franchieseList.count)
    {
        [self.selectAllButton setTitle:NSLocalizedString(@"Unselect All", @"Unselect All") forState:UIControlStateNormal];
        self.isAllFranchiesSelected = YES;
    }
    else
    {
        [self.selectAllButton setTitle:NSLocalizedString(@"Select All", @"Select All") forState:UIControlStateNormal];
        self.isAllFranchiesSelected = NO;
    }
    
    [self shouldHighlightImageOfCell:cell atIndexPath:indexPath];
}

- (void)shouldHighlightImageOfCell:(PreferenceListCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    NSString *franchieseLabel = cell.franchieseNameLabel.text;
    if ([self.selectedPreferencesList containsObject:franchieseLabel])
    {
        [cell setPreferenceSelectionImageViewSelected:YES];
    }
    else
    {
        [cell setPreferenceSelectionImageViewSelected:NO];
    }
    if (self.selectedPreferencesList.count == 0)
    {
        self.savePreferencesButton.userInteractionEnabled = false;
        [self.savePreferencesButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    else
    {
        self.savePreferencesButton.userInteractionEnabled = true;
        [self.savePreferencesButton setTitleColor:[UIColor GetColorFromHexValue36a2c6] forState:UIControlStateNormal];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(218, 40);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - Ends Here
+ (PreferencesListViewController*)GetPreferencesViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PreferencesListViewController* preferencesVC = (PreferencesListViewController*)[sb instantiateViewControllerWithIdentifier:@"PreferencesListViewController"];
    preferencesVC.transitioningDelegate  = preferencesVC;
    preferencesVC.modalPresentationStyle = UIModalPresentationCustom;
    return preferencesVC;
}

- (void)showPreferencesViewControllerFromController:(UIViewController*)fromController
{
    self.fromViewController = fromController;
    [self.fromViewController presentViewController:self animated:YES completion:nil];
}


+ (PreferencesListViewController*)presentPreferencesViewControllerFromController:(UIViewController*)fromController flowType:(NSString *)flowType
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PreferencesListViewController* preferencesVC = (PreferencesListViewController*)[sb instantiateViewControllerWithIdentifier:@"PreferencesListViewController"];
    preferencesVC.transitioningDelegate  = preferencesVC;
    preferencesVC.modalPresentationStyle = UIModalPresentationCustom;
    // display on top of any view
    UIWindow *windows = [[UIApplication sharedApplication].delegate window];
    UIViewController *vc = windows.rootViewController;
    preferencesVC.fromViewController     = fromController;
    preferencesVC.flowType = flowType;
    //fromController;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [vc presentViewController:preferencesVC animated:YES completion:nil];
    });
    return preferencesVC;
}

- (void)presentContextMenuViewController
{
    [self.fromViewController presentViewController:self animated:NO completion:nil];
}

#pragma mark- Transition Animator Methods
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    FadeTransitionAnimator *animator = [[FadeTransitionAnimator alloc] init];
    animator.presenting = YES;
    animator.duration = 0.11f;
    return animator;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    FadeTransitionAnimator *animator = [[FadeTransitionAnimator alloc] init];
    animator.duration = 0.11f;
    return animator;
}

- (void)markSelectedToHomeOfficeFranchiese
{
    //[self.selectedPreferencesList addObject:kDefaultFranchieseHomeOffice];
    [self.preferenceListCollectionView reloadData];
}
+ (BOOL)isSavedPreferencesAvailable
{
    NSString *userName = [AOFLoginManager sharedInstance].displayName;
    Preferences *preference = [Preferences MR_findFirstByAttribute:@"userName"
                                                         withValue:userName];
    NSArray *savedFranchiese = [preference.franchieses allObjects];
    if (savedFranchiese.count > 0)
    {
        return YES;
    }
    return NO;
}
@end
