//
//  PreferenceListCell.m
//  Pronto
//
//  Created by m-666346 on 19/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "PreferenceListCell.h"
#import "UIImage+ImageDirectory.h"
#import "UIColor+ColorDirectory.h"


@interface PreferenceListCell()
@property (weak, nonatomic) IBOutlet UIImageView *preferenceSelectionImageView;

@end

@implementation PreferenceListCell

+(NSString*)nibName
{
    return @"PreferenceListCell";
}

+(NSString*)cellIdentifier
{
    return @"PreferenceListCellIdentifier";
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.franchieseNameLabel.textColor = [UIColor GetColorFromHexValue817f7d];
    self.preferenceSelectionImageView.image = [UIImage GetCheckListImage];
}

- (void)prepareForReuse
{
    [self setPreferenceSelectionImageViewSelected:NO];
}

- (void)setPreferenceSelectionImageViewSelected:(BOOL)selected
{
    if (selected == YES)
    {
        self.preferenceSelectionImageView.image = [UIImage GetSelectedCheckListImage];
    }
    else
    {
        self.preferenceSelectionImageView.image = [UIImage GetCheckListImage];
    }
}
- (void)prepareCellWithFranchieseName:(NSString*)franchieseName
{
    self.franchieseNameLabel.text = franchieseName;
}

@end
