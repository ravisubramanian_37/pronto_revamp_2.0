//
//  PreferenceListCell.h
//  Pronto
//
//  Created by m-666346 on 19/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PreferenceListCell;
@protocol PreferenceListCellDelegate
-(void)didTapCell:(PreferenceListCell*)cell;
@end


@interface PreferenceListCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *franchieseNameLabel;
@property (nonatomic, assign)  id<PreferenceListCellDelegate> delegate;

+(NSString*)nibName;
+(NSString*)cellIdentifier;

- (void)prepareCellWithFranchieseName:(NSString*)franchieseName;
- (void)setPreferenceSelectionImageViewSelected:(BOOL)selected;

@end
