//
//  PreferencesListVCViewController.h
//  Pronto
//
//  Created by m-666346 on 19/02/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PreferencesListViewController : UIViewController
@property (nonatomic, weak) UIViewController *fromViewController;
+ (id)sharedInstance;
+ (PreferencesListViewController*)presentPreferencesViewControllerFromController:(UIViewController*)fromController flowType: (NSString*)flowType;
+ (PreferencesListViewController*)GetPreferencesViewController;
- (void)showPreferencesViewControllerFromController:(UIViewController*)fromController;
- (void)markSelectedToHomeOfficeFranchiese;
- (void)fetchPreferencesInBackground;
//User should do V2 call or sync call or Library bring all data call only when saved preferences is available,else no
+ (BOOL)isSavedPreferencesAvailable;
@property(nonatomic, strong) NSString *flowType;
@end
