//
//  ZMLibrary.h
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMGridItem.h"
#import "ZMUserActions.h"
#import "ZMProntoViewController.h"
#import "ZMAssetsLibrary.h"
#import "ZMWalkthrough.h"
#import "ZMLaunchLoading.h"
#import "LoadingIndicatorNew.h"
#import "ZMTracking.h"
#import "ZMHeader.h"
#import "ZMAbbvieAPI.h"
#import "ZMDocument.h"
//#import "ZMPDFDocument.h"
#import "ZMFolderHeader.h"
#import "ZMCategoryMenu.h"
#import "ExportMenu.h"
#import "InfoMenu.h"
#import "ProntoTabbarViewController.h"

@interface ZMLibrary : ZMProntoViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, ZMGridItemDelegate,UITableViewDelegate , UITableViewDataSource,UIPopoverPresentationControllerDelegate>{
    
    ZMAbbvieAPI *_apiManager;
    BOOL _isDownloadignAsset;
    BOOL _isTapDownloadInitiated;
    
    NSIndexPath *_selectedIndexPath;
    BOOL isBackendApp;
  
    
}
@property (nonatomic, assign) BOOL  shouldLoadAllData;

/**
 *  Enumerates the sections types
 */
enum ProntoSectionType
{
    ProntoSectionTypeLibrary = 0,
    ProntoSectionTypeBriefcase = 1,
    ProntoSectionTypeBriefcaseFolderDetail = 2,
    ProntoSectionTypeCompetitors = 3,
    ProntoSectionTypeToolsTraining = 4,
    ProntoSectionTypeEvents = 5
};


/**
 *  Enumerates the download type
 */
enum ZMProntoDownloadStatus
{
    ZMProntoDownloadStatusNotDownloaded = 0,
    ZMProntoDownloadStatusLocal = 1,
    ZMProntoDownloadInProgress = 2
};


/**
 * Displays the login viewController if there is not a valid session initiated
 **/
-(void) displayLogin;

//Revamp(4/12) - Adding Parameter to display notifications
/**
 *  Displays the notification pannel on view
 */
-(void) displayNotifications:(UIView*) viewToDisplayNotifications;
//End of Revamp

//Revamp(4/22)-Add parameter for walkthrough
/**
 *  Displays the walkthrough component on main view
 */
-(void) displayWalkthrough:(UIView*) viewToDisplay;
//End of Revamp

/**
 *  Get notifications from server
 */
-(void) getNotifications;

//Revamp(4/12) - Adding Parameter to display change badge
/**
 *  Changes the badge number, for both application icon and header badge
 *  @param badgeNumber
 */
-(void) changeBadgeNumber:(int)badgeNumber header:(ZMHeader *) headerForBadge;
//End of Revamp

-(void) displayBrowse;
//-(void) hideBrowse;

- (void)displayEmpty:(BOOL)isInAFolder;
- (void)loadLibraryEmpty;

//Revamp(4/12) - parameter for updation of header
/**
 *  Updates the header within the user actions
 */
- (void)updateHeader:(ZMHeader*) headerToUpdateView;

- (void) headerUpdate: (ZMHeader*) headerToUpdateView;
//End of Revamp
/**
 *  Refresh library to update latest changes on Assets and Folders
 */
- (void)refreshLibrary;
/**
 *  Each time user performed some search the Scroll to containt data is scrolled to index zero
 */
- (void)verifySearch;
/**
 *  Reload data with the default values (Franchise and Brands)
 */
- (void)backToDefault;
/**
 *  Load Library When finished the Scroll on latest page
 */
- (void)loadLibrary;
/**
 *  Load My Briefcase view
 */
- (void)loadBriefcase;
/**
 *  Reset Lybrary view when user swithed since MyBriefcase To MyLibrary
 */
- (void)backToBriefcase;

//For opening the selected asset
- (void)openAsset:(Asset *)selectedAsset;

/**
 *  Reset date synch after user logout of App
 */
- (void)initLibraryAfterLogout;
- (void)dismissAllNotifications:(NSArray *)notifications;
- (void)saveNotifications:(NSDictionary *) notifications;
- (void)geTotalOfNotifications;
- (void)resetPopOvers;
- (void)openTempAsset:(NSURL *)url;
//Revamp(13/12) - Show arrow below the message button
- (void)showHideArrow:(BOOL) hide;

- (NSArray *)defaultFranchise:(NSString *)franchiseName;
- (NSArray *)getSelectedCategories;
- (NSString *)getFranchiseName;


/**
 *  ZMAsset with the setters
 */
- (ZMAsset *)getLibraryFilters;

- (void)markLiked:(Asset *)selectedAsset type:(NSInteger)type;
- (void)markAssetAsRead:(Asset *)selectedAsset;

/**
 *  Gets the asset downlod or open depending of its state
 *  @param selectedAsset
 *  @return a boolean with if the asset will be opened or no
 */
- (void)validateAsset:(Asset *) selectedAsset;

- (void) showScrollSpinner;

/**
 * User Trackings
 */
- (NSString *)getSelectedSort;
- (NSString *)getCategoriesForTracking;

/**
 *  current section on library
 */
@property (nonatomic) enum ProntoSectionType section;

/**
 *  Describes the actions of the user on library
 */
@property (strong, nonatomic) ZMUserActions *actions;
@property (strong, nonatomic) ZMAssetsLibrary *libraryData;
@property (strong, nonatomic) ZMHeader *headerView;
@property (strong, nonatomic) ZMFolderHeader *headerDetailFolder;
@property (nonatomic) long currentFolderId;
@property (nonatomic) int itemsPerPage;
@property (nonatomic) int pageIndex;
@property (nonatomic) int previousIndex;
//Know the previous progress of main loading indicator
@property (nonatomic) float previousProgress;
@property (nonatomic) BOOL needRefresh;
@property (nonatomic) BOOL isBrowseMenuActive;
@property (nonatomic) BOOL libraryBackToFolderDetail;
@property (nonatomic) BOOL resestFranchises;
//Revamp(4/12) - Adding notifications loaded in h file
@property (nonatomic) BOOL notificationsLoaded;
//End of Revamp
// New Search Implementation
@property (strong, nonatomic) NSMutableArray *assetsInPage;
@property (strong, nonatomic) UICollectionView * viewGrid;
@property (strong, nonatomic) UIActivityIndicatorView * activityView;
@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) UILabel *loadingLabel;
@property (nonatomic) BOOL selectedSortBy;

// New Revamping
@property (strong, nonatomic) IBOutlet UITableView *leftMenuTableView;
@property (strong, nonatomic) ZMCategoryMenu* categoryMenu;
@property (nonatomic,strong) NSMutableArray *categoriesList;
@property (nonatomic,strong) UIView* franchiseView;
@property (strong, nonatomic) UILabel *franchiseCount;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
// Export Menu
@property (nonatomic, weak) ExportMenu *exportMenu;
@property (nonatomic) BOOL exportMenuIsPresented;
@property(nonatomic, strong) UIPopoverPresentationController* exportMenuPopOver;

@property (strong, nonatomic) Asset *currentSelectedAsset;
@property (strong, nonatomic) ZMGridItem *cell;
// Info Menu
@property (nonatomic, weak) InfoMenu *infoMenu;
@property (nonatomic, weak) UIPopoverPresentationController *infoMenuPopOver;
@property (nonatomic) BOOL infoMenuIsPresented;

@property (strong, nonatomic) UIView *promotedContentView;
@property (strong, nonatomic) UIView *contentHeaderView;
@property (strong, nonatomic) UICollectionView * promotedViewGrid;
@property (strong, nonatomic) ProntoTabbarViewController *tabBar;
@property (nonatomic) BOOL isPromotedFirst;
@property (strong,nonatomic) NSMutableArray* brandsArray;
@property (strong,nonatomic) NSTimer* checkUser;
@property (strong,nonatomic) NSMutableArray* assetsPage;

@property (nonatomic, strong) NSTimer *progressTimer;
@property (nonatomic) float  timerProgress;
@property (nonatomic, strong) NSMutableArray * franchiseList;
@property (nonatomic, assign) BOOL isShowingIndicatorViewWithOverlay;

// New Revamp Changes - Feedback details
- (void)initiateFeedback:(NSString*) selectedAsset openedFrom:(NSString*) choice;
//To adjust height according to label content
-(CGRect) adjustLabelHeight: (UILabel *)label;
-(void) loadSearchedAssets ;
- (void)loadNextPage;
- (void)initializeCollectionView ;
-(void)loadMyLibrary;
- (IBAction)btnClicked:(id)sender;
- (IBAction)dismissView:(id)sender;
- (void)updateSortButton;
- (void)updateCategoryButton;

- (IBAction)exportIconClicked:(id)sender;
- (IBAction)favIconClicked:(id)sender;
- (IBAction)infoIconClicked:(id)sender;
-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure;
- (void)downloadAsset:(Asset *)selectedAsset;
- (void)downloadOneAsset:(Asset *)selectedAsset completion:(void (^)(BOOL assetDownloaded))completion
                 failure:(void (^)(NSString *assetDownloadFail))failure;
- (IBAction)viewChangesButtonTapped:(id)sender;
- (void)hideScrollSpinner;
- (NSArray*)franchiseToUpdateHeader:(NSString *)franchiseName ;
- (void) displayPromotedContent:(NSString *)triggeredFrom;
- (void) showLoadingIndicatorForPromotedContent:(BOOL)show;
- (void) offlinePromotedChanges;
-(void) changeTintColor;
- (void)bringLibraryData;
-(void) checkUserPreferences;
-(void) clearUserData;
- (NSArray*)getAllFranchieseName;
-(void) researchKeyword:(NSString*) keyword;
-(void) checkCompletionStatus;
- (void)resetGlobalSearchFromLibrary;
-(void)setExportToNormal:(BOOL) dismissExport;
- (void)refreshLibraryDataFromCMS;

@end

