//
//  HelpViewController.m
//  Pronto
//
//  Created by cccuser on 24/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "HelpViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "BrightcovePlayerViewController.h"
#import "ViewUtils.h"
#import "FeedbackView.h"
#import "EventsViewController.h"
#import "Pronto-Swift.h"


static NSString * const kVidoeID = @"5803237506001";

@interface HelpViewController ()

@property (nonatomic, strong) BrightcovePlayerViewController *brightcovePlayer;
@property (nonatomic, strong)  UIView *backgroundView;
@property (nonatomic, strong) HomeMainHeaderView *headerView;
@end

@implementation HelpViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.theme = [ZMProntoTheme sharedTheme];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUpView];
    [self intializeAndPlayVideo];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _headerView.helpOptions = NO;
    _headerView.helpViewController = nil;
     [_headerView configureHeaderForHelpWithHeaderView: _headerView];
}
-(void)setUpView{

    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    _headerView = [[[NSBundle mainBundle] loadNibNamed:RedesignConstants.mainViewHeaderNib owner:self options:nil] objectAtIndex:0];
    [_headerContainer addSubview:_headerView];
    [_headerView setThemes];
    _headerView.helpOptions = YES;
    _headerView.helpViewController = self;
    [_headerView configureHeaderForHelpWithHeaderView:_headerView];
    _headerContainer.frame = _headerView.frame;
}
- (void)intializeAndPlayVideo
{
    if (self.brightcovePlayer == nil)
    {
        self.brightcovePlayer = [BrightcovePlayerViewController GetBrightcovePlayerViewController];
        self.brightcovePlayer.videoID = self.videoId;
    }
    
    [self addChildViewController:self.brightcovePlayer];

    [self.mainView addSubview:self.brightcovePlayer.view];
    self.brightcovePlayer.view.frame = CGRectMake(0, 0, self.mainView.bounds.size.width, self.mainView.bounds.size.height + 100);
    [self.brightcovePlayer didMoveToParentViewController:self];
    
    [self.brightcovePlayer play];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.brightcovePlayer = nil;
    
}

- (void)pauseVideoPlayerResetTheValue
{
    if (self.brightcovePlayer != nil)
    {
        [self.brightcovePlayer pause];
        
        [self.brightcovePlayer willMoveToParentViewController:nil];
        [self.brightcovePlayer.view removeFromSuperview];
        [self.brightcovePlayer removeFromParentViewController];
        self.brightcovePlayer = nil;
    }
}

- (IBAction)closeAction:(id)sender
{
    [self pauseVideoPlayerResetTheValue];
    if (_ispresented)
    {
        [self dismissViewControllerAnimated:true completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }

}
@end
