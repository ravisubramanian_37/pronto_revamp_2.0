//
//  HelpViewController.h
//  Pronto
//
//  Created by cccuser on 24/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMUserActions.h"
#import "ZMHeader.h"
#import "Constant.h"
#import "ZMProntoTheme.h"

@interface HelpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *headerContainer;
@property (weak, nonatomic) IBOutlet UIView *toolbarView;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (nonatomic) BOOL ispresented;
@property (strong,nonatomic) NSString * userFranchise;
@property (strong,nonatomic) NSString * videoId;
@property (weak, nonatomic) id <ZMProntoTheme> theme;
- (void)pauseVideoPlayerResetTheValue;

@end
