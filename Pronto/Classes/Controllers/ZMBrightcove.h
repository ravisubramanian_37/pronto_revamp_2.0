//
//  ZMBrightcove.h
//  Pronto
//
//  Created by Sebastian Romero on 2/21/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMAssetViewController.h"
#import <UIKit/UIKit.h>
// for AVAudioPlayer
#import <AVFoundation/AVFoundation.h>

@interface ZMBrightcove : ZMAssetViewController <UIGestureRecognizerDelegate>
{
    // for AVAudioPlayer
    AVAudioPlayer *_audioPlayer;
    IBOutlet UISlider					*volumeSlider;
    
}

/**
 *  property to manage and show duration of video
 */
@property (nonatomic, assign) long long duration;

// for AVAudioPlayer
@property (nonatomic, retain)	UISlider		*volumeSlider;
/**
 *  Reset video start-play
 */
-(void) resetVideo;

// for AVAudioPlayer
- (IBAction)volumeSliderMoved:(UISlider*)sender;

@end
