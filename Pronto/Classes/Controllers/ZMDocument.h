//
//  ZMDocument.h
//  Pronto
//
//  Created by Sebastian Romero on 2/21/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMAssetViewController.h"
#import <WebKit/WebKit.h>

@interface ZMDocument : ZMAssetViewController<WKNavigationDelegate,WKUIDelegate,UIWebViewDelegate>

@end
