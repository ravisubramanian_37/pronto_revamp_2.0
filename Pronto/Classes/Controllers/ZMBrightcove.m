//
//  ZMBrightcove.m
//  Pronto
//
//  Created by Sebastian Romero on 2/21/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//

#import "ZMBrightcove.h"
#import "ZMUserActions.h"
#import "ZMTracking.h"
#import <AVKit/AVKit.h>
#import "ZMLibrary.h"
#import "Pronto-Swift.h"

@import BrightcovePlayerSDK;
@import BrightcovePlayerUI;
@import BrightcoveSidecarSubtitles;

// PRONTO-26 Update Brightcove Player
static NSString * const kViewControllerCatalogToken = @"JIPAZdyRpSRxPk1LDlpUrNfZ5UV0DpmxFVeAuZB3EtaZhsxzNSNSAA..";
static NSString * const kViewControllerPlaybackServicePolicyKey =@"BCpkADawqM3c8kqsObBQXAlnrJz1x1b1eyOahI3tBF08wPnzukXRaIhFIGpvbAQJKYDdtW-gVTa-HUUmpLIi1QpPgp_OgOFyu2R8R2YxsnjRevxZcgzwDsyuVGy18njMfWQXSCzedKzGwWib";
static NSString * const kVPlayBackServiceAccountId = @"1029485116001";




@interface ZMBrightcove () <BCOVPlaybackControllerDelegate, BCOVPUIPlayerViewDelegate  , headerActions>{
    __weak IBOutlet UIView *mainView;
    UIView *videoControls;
    UIAlertController *message;
}
@property (nonatomic, strong) id<BCOVPlaybackController> playbackController;
@property (nonatomic, strong) AVPlayerViewController *avpvc;
@property int totalTimeVideo;

@property (nonatomic, strong) BCOVCatalogService *catalogService;
// PRONTO-26 Update Brightcove Player
@property (nonatomic, strong) BCOVPlaybackService *service;
@property (nonatomic, weak) BCOVPUIPlayerView *playerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *assetTitleWidthConstraint;


@end

@implementation ZMBrightcove
// for AVAudioPlayer
@synthesize volumeSlider;

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self setup];
    }
    return self;
}

/**
 *  Set up Brightcove controller
 */

- (void)setup
{
    _avpvc = [[AVPlayerViewController alloc] init];
    
    // PRONTO-26 Update Brightcove Player
    BCOVPlayerSDKManager *manager = [BCOVPlayerSDKManager sharedManager];
    
    // using the https policy
    BCOVBasicSessionProviderOptions *options = [[BCOVBasicSessionProviderOptions alloc] init];
    options.sourceSelectionPolicy = [BCOVBasicSourceSelectionPolicy sourceSelectionHLSWithScheme:kBCOVSourceURLSchemeHTTPS];
    id<BCOVPlaybackSessionProvider> provider = [manager createBasicSessionProviderWithOptions:options];
    // remove the grey bar which is not functional - [manager defaultControlsViewStrategy]
    _playbackController = [manager createPlaybackControllerWithSessionProvider:provider viewStrategy:nil];
    
    // sidecar subtitles for cc
      _playbackController = [manager createSidecarSubtitlesPlaybackControllerWithViewStrategy:nil];

    _playbackController.delegate = self;
    _playbackController.autoAdvance = YES;
    _playbackController.autoPlay = YES;
    
   // _catalogService = [[BCOVCatalogService alloc] initWithToken:kViewControllerCatalogToken];
    _service = [[BCOVPlaybackService alloc] initWithAccountId:kVPlayBackServiceAccountId
                                                    policyKey:kViewControllerPlaybackServicePolicyKey];
}
- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    _assetTitleWidthConstraint.constant = self.view.frame.size.width/2;
    [self.view updateConstraints];
    [self.assetDescriptionBar reloadInputViews];
}
- (void)viewDidLoad{
    
    [super  viewDidLoad];
    
    [self addGestures];
    [self addNotifications];
    
    // PRONTO-26 Update Brightcove Player
    //Custom style
    
    // Standard play/pause button
    BCOVPUILayoutView *playbackLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagButtonPlayback width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    // Standard jump back button
    BCOVPUILayoutView *jumpBackButtonLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagButtonJumpBack width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    // Current time indicator
    BCOVPUILayoutView *currentTimeLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagLabelCurrentTime width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    // Time separator - typically the '/' character
    BCOVPUILayoutView *timeSeparatorLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagLabelTimeSeparator width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
     // Video duration label
    BCOVPUILayoutView *durationLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagLabelDuration width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    // Slider bar used for scrubbing through the video
    // The elasticity is set to 1 so that it can resize to fill available space
    BCOVPUILayoutView *progressLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagSliderProgress width:kBCOVPUILayoutUseDefaultValue elasticity:1.0];
    // Closed caption button
    // This button is initially hidden ('removed'), and will be shown
    // if closed captions or audio tracks are available.
    BCOVPUILayoutView *closedCaptionLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagButtonClosedCaption width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    
    
    
    // The full-screen button
    //BCOVPUILayoutView *screenModeLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagButtonScreenMode width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    
    // AirPlay button
    // This button is initially hidden ('removed'), and will be shown
    // if AirPlay devices are available.
    BCOVPUILayoutView *externalRouteLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagViewExternalRoute width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    // Empty view - used as a spacer
    BCOVPUILayoutView *spacerLayoutView1 = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagViewEmpty width:1.0 elasticity:0.5];
    BCOVPUILayoutView *spacerLayoutView2 = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagViewEmpty width:1.0 elasticity:0.5];
    
    // Volume Control
    
    BCOVPUILayoutView *spacerLayoutView3 = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagViewEmpty width:9.0 elasticity:0.0];
    // Empty view - used as a spacer
    BCOVPUILayoutView *spacerLayoutVolView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagViewEmpty width:180.0 elasticity:0.0];
    // Empty view - will have a custom UIImageView added as a subview
    BCOVPUILayoutView *logoLayoutView1 = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagViewEmpty width:180.0 elasticity:0.0];
    BCOVPUILayoutView *spacerLayoutView4 = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagViewEmpty width:18.0 elasticity:0.0];
    
    
    // Put UIImages inside our logo layout views.
    {
        // Create logo image inside an image view for display in control bar.
        UIImageView *volumeDownView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"volumedown"]];
        volumeDownView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        volumeDownView.contentMode = UIViewContentModeScaleAspectFit;
        volumeDownView.frame = spacerLayoutView3.frame;
        
        // Add image view to our empty layout view.
        [spacerLayoutView3 addSubview:volumeDownView];
    }
    
    {
        // Create logo image inside an image view for display in control bar.
        UIImageView *volumeUpView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"volumeup"]];
        //volumeUpView.frame = CGRectMake(0, 0, 18, 20);
        volumeUpView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        volumeUpView.contentMode = UIViewContentModeScaleAspectFit;
        volumeUpView.frame = spacerLayoutView4.frame;
        
        // Add image view to our empty layout view.
        [spacerLayoutView4 addSubview:volumeUpView];
    }
    
    // Volume Slider
    
    {
        
        UISlider *slider = [[UISlider alloc] initWithFrame:logoLayoutView1.frame];
        [slider addTarget:self.avpvc.player action:@selector(volumeSliderMoved:) forControlEvents:UIControlEventValueChanged];
        [slider setBackgroundColor:[UIColor clearColor]];
        slider.minimumValue = 0.0;
        slider.maximumValue = 1.0;
        slider.continuous = YES;
        slider.value = 0.5;
        [spacerLayoutVolView addSubview:slider];
    }
    
    
    // Hide closed caption and AirPlay controls until explicily needed.
    closedCaptionLayoutView.removed = YES;
    externalRouteLayoutView.removed = YES;
    
    
    NSArray *standardLayoutLine1 = @[ playbackLayoutView,
                                      jumpBackButtonLayoutView,
                                      currentTimeLayoutView,
                                      timeSeparatorLayoutView,
                                      durationLayoutView,
                                      progressLayoutView,
                                      spacerLayoutView1,
                                      spacerLayoutView2,
                                      closedCaptionLayoutView,
                                      spacerLayoutView3,
                                      spacerLayoutVolView,
                                      
                                      spacerLayoutView4,
                                      //currentTimeLayoutView,
                                      externalRouteLayoutView
                                      ];
    
    NSArray *standardLayoutLines = @[ standardLayoutLine1 ];
    
    NSArray *compactLayoutLine1 = @[ currentTimeLayoutView,
                                     progressLayoutView,
                                     durationLayoutView ];
    
    NSArray *compactLayoutLine2 = @[ playbackLayoutView,
                                     jumpBackButtonLayoutView,
                                     spacerLayoutView1,
                                     closedCaptionLayoutView,
                                     //currentTimeLayoutView,
                                     externalRouteLayoutView
                                     //logoLayoutView2
                                     ];
    
    NSArray *compactLayoutLines = @[ compactLayoutLine1,
                                     compactLayoutLine2 ];
    
    BCOVPUIControlLayout *customLayout = [[BCOVPUIControlLayout alloc] initWithStandardControls:standardLayoutLines compactControls:compactLayoutLines];
    BCOVPUIPlayerViewOptions *options = [[BCOVPUIPlayerViewOptions alloc] init];
    options.presentingViewController = self;
    options.jumpBackInterval = 3;
    
    BCOVPUIPlayerView *playerView = [[BCOVPUIPlayerView alloc] initWithPlaybackController:self.playbackController options:options];
    playerView.delegate = self;
    self.playerView = playerView;
    self.playerView.controlsView.layout = customLayout;
    
    
    self.playerView.frame = mainView.bounds;
    self.playerView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    [mainView addSubview:self.playerView];
    [mainView addSubview:logoLayoutView1];
    [self requestContentFromCatalog];
    _assetTitleWidthConstraint.constant = self.view.frame.size.width/2;
    [self.assetDescriptionBar reloadInputViews];


}

/**
 *  Load video into controller
 */
- (void)requestContentFromCatalog
{
    // PRONTO-26 Update Brightcove Player
   // NSString *brightcoveID = [self.asset.uri_full substringFromIndex:13];
    NSString *brightcoveID = self.videoId;
    
    [self.service findVideoWithVideoID:brightcoveID  parameters:nil completion:^(BCOVVideo *video, NSDictionary *jsonResponse, NSError *error){
        
        if (video)
        {
            [self.playbackController setVideos:@[ video ]];
            
            // for tracking
            int videoDuration = [[video.properties valueForKey:@"duration"] intValue];
            NSString * videoshortDescription = [video.properties valueForKey:@"shortDescription"];
            NSString * videoID = [video.properties valueForKey:@"id"];
            
            _totalTimeVideo = videoDuration;
            NSString * stringVideoDuration = [self getTimeVideo:videoDuration];
            _duration = [stringVideoDuration intValue];
            
            int seconds = videoDuration / 1000;
            
            [ZMTracking settignMedia:self.asset.title length:seconds playerName:videoshortDescription playerID:videoID];
            
            [self trackingProgressVideo:@"tier2"];
            
            [self trackingVideo];
        }
        else
        {
            // error handling
            [AbbvieLogging logError:[NSString stringWithFormat:@"ViewController Debug - Error retrieving video: `%@`", [error userInfo]]];
            
            NSString* msg = @"The video could not be played; please verify your Internet connection and try again";
            
            
            // Alternative AlertView
            message = [UIAlertController alertControllerWithTitle:@""  message:msg preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction* okButton = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action) {
                                             //Handle OK button
                                             
                                         }];
        
            [message addAction:okButton];
        
            [self presentViewController:message animated:YES completion:nil];

        }
        
    }];
    
}

-(void)trackingVideo{
    
    self.tierTrackingValue = 2;
    [ZMTracking addEvarVideoName:[NSString stringWithFormat:@"%ld|%@", self.asset.assetID.longValue, self.asset.title]];
    
    //@Tracking - Video
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section
              withSubsection:@"assetview"
                    withName:[NSString stringWithFormat:@"%@",[ZMAssetViewController getAssetType:self.asset]]
                 withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{
                                                                               @"sctierlevel": @"tier2",
                                                                               @"sccollateralname":[NSString stringWithFormat:@"%@|%@",[ZMAssetViewController getAssetType:self.asset], self.asset.title],
                                                                               @"scrating" :[ZMAssetViewController getAssetType:self.asset] }]];
    
    
}

-(void)trackingProgressVideo:(NSString*)name{
    
    //@Tracking - Video
    [ZMTracking trackSection:[ZMUserActions sharedInstance].section
              withSubsection:@"assetview"
                    withName:[NSString stringWithFormat:@"%@",[ZMAssetViewController getAssetType:self.asset]]
                 withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{
                                                                               @"sctierlevel": name,
                                                                               @"scrating" :name }]];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   [self hideVideoControls];

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    
    if (!self.presentedViewController){
        [self removeAllNotifications];
        [_playbackController pause] ;
    }
}

- (void)resetVideo{
    
    if (_playbackController) {
        self.playbackController.view.frame = CGRectMake(0, 0, mainView.frame.size.width, mainView.frame.size.height);
        self.playbackController.view.autoresizingMask =  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    }
}

-  (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
}


- (void)addGestures {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoSelection:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    tapGestureRecognizer.delegate = self;
}


- (void)videoSelection:(UITapGestureRecognizer *)gesture {
    
    if(self.headerView.alpha < 1){
        [self showHideHeader:NO];
    }
    else {
        [self showHideHeader:YES];
    }
}

- (void)addNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadDidFinishWithError:) name:@"LoadDidFinishWithError" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerStateChange:) name:@"MPAVControllerPlaybackStateChangedNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:@"HIDE_ASSET_POPOVERS" object:nil];
}

-(NSString*)getTimeVideo:(int)timeInValue{
    
    NSDateComponentsFormatter *dcFormatter = [[NSDateComponentsFormatter alloc] init];
    dcFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
    dcFormatter.allowedUnits =  NSCalendarUnitMinute | NSCalendarUnitSecond;
    dcFormatter.unitsStyle = NSDateComponentsFormatterUnitsStyleAbbreviated;
    
    return [dcFormatter stringFromTimeInterval:timeInValue/1000];
}

#pragma mark BCOVPlaybackControllerDelegate Methods

- (void)playbackController:(id<BCOVPlaybackController>)controller didAdvanceToPlaybackSession:(id<BCOVPlaybackSession>)session
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"ViewController Debug - Advanced to new session. %@", session]];
    self.avpvc.player = session.player;
     [self.avpvc.player play];
}

/**
 *  Use PlayBackController:PlayBackSessionDidReceiveLifecycleEvent to track "Play" - "Stop" events into Brightcove document
 *
 *  @param controller     Current controller that is diplaying the video
 *  @param session        Manager session
 *  @param lifecycleEvent Event user performed
 */
-(void)playbackController:(id<BCOVPlaybackController>)controller playbackSession:(id<BCOVPlaybackSession>)session didReceiveLifecycleEvent:(BCOVPlaybackSessionLifecycleEvent *)lifecycleEvent{
    
    float time = CMTimeGetSeconds(session.player.currentTime);
    
    if ([kBCOVPlaybackSessionLifecycleEventEnd isEqualToString:lifecycleEvent.eventType]) {
        [self trackingProgressVideo:@"tier3"];
        [self requestContentFromCatalog];
        
    }
    
    [ZMTracking trackMediaEvent:lifecycleEvent.eventType named:self.asset.title offset:(double)time];
}

- (void)playerView:(BCOVPUIPlayerView *)playerView didTransitionToScreenMode:(BCOVPUIScreenMode)screenMode {
    [AbbvieLogging logInfo:@"ViewController Debug - Transitioned Screen mode."];
}

- (void)playbackSession:(id<BCOVPlaybackSession>)session didProgressTo:(NSTimeInterval)progress
{
    
    NSTimeInterval duration = CMTimeGetSeconds(session.player.currentItem.duration);
    float percent = progress / duration;
    float value = isnan(percent) ? 0.0f : percent;
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"NSInterval %f", value]];
}

- (void) displayVideoErrorMessage {

    [AbbvieLogging logError:@"Error in playing video"];
}

-(void) removeAllNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - gesture delegate


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - handle notifications

- (void) loadDidFinishWithError:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"LoadDidFinishWithError"]){
        [self displayVideoErrorMessage];
    }
}

- (void)recievedNotification:(NSNotification *) notification{
    
    if ([[notification name] isEqualToString:@"HIDE_ASSET_POPOVERS"]) {
    }
}

- (void)playerPlaybackFinish:(NSNotification *)notification{
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Notification %@", notification]];
    
}

- (void)playerStateChange:(NSNotification *)notification {
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"note.name=%@ state=%d", notification.name, [[notification.userInfo objectForKey:@"MPAVControllerNewStateParameter"] intValue]]];
    switch ([[notification.userInfo objectForKey:@"MPAVControllerNewStateParameter"] intValue]) {
        case 1:{//End
            self.tierTrackingValue  = 3;
            [self requestContentFromCatalog];
        }
            break;
        case 2:{ //Start
            [self requestContentFromCatalog];
        }
            break;
        case 3://Toogle
            
            break;
        default:
            
            break;
    }
}

-(BOOL) controlsVisible {
    BOOL isVisible = NO;
    for(id views in [[_playbackController view] subviews]){
        for(id subViews in [views subviews]){
            for (id controlView in [subViews subviews]){
                if ( [controlView isKindOfClass:NSClassFromString(@"MPVideoPlaybackOverlayView")] ) {
                    isVisible = ([controlView alpha] <= 0.0) ? (NO) : (YES);
                }
            }
        }
    }
    return isVisible;
}


-(void) showVideoControls
{
    for(id views in [[_playbackController view] subviews]){
        for(id subViews in [views subviews]){
            for (id controlView in [subViews subviews]){
                if ( [controlView isKindOfClass:NSClassFromString(@"MPVideoPlaybackOverlayView")] ) {
                    [controlView setAlpha:1];
                }
            }
        }
    }
}



-(void) hideVideoControls
{
    for(id views in [[_playbackController view] subviews]){
        for(id subViews in [views subviews]){
            for (id controlView in [subViews subviews]){
                if ( [controlView isKindOfClass:NSClassFromString(@"MPVideoPlaybackOverlayView")] ) {
                    [controlView setAlpha:0];
                }
            }
        }
    }
}

#pragma mark UI Styling

+ (void)addConstraintsForView:(UIView *)view
{
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutFormatOptions option = NSLayoutFormatDirectionLeadingToTrailing;
    NSDictionary *views = NSDictionaryOfVariableBindings(view);
    
    NSArray *horizontalConstrains = [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|" options:option metrics:nil views:views];
    NSArray *verticalConstrains = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:option metrics:nil views:views];
    
    [view.superview addConstraints:horizontalConstrains];
    [view.superview addConstraints:verticalConstrains];
}


#pragma mark - ZMHeaderDelegate

-(void)closeButtonDidPress{
    [ZMTracking trackMediaEvent:@"Closed" named:self.asset.title offset:0];
}

// for AVAudioPlayer
- (IBAction)volumeSliderMoved:(UISlider *)sender
{
    
    UISlider *slider = sender;
    float value = slider.value;
    self.avpvc.player.volume = value;
}


@end
