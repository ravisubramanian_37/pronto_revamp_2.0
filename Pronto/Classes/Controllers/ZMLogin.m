//
//  ZMLogin.m
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import "ZMLogin.h"
#import "ZMProntoTheme.h"
//#import <ASFKit/ASFKit.h>

@interface ZMLogin ()
{
    id <ZMProntoTheme> theme;
}
@end

@implementation ZMLogin

- (void)viewDidLoad{
    [super viewDidLoad];
    theme = [ZMProntoTheme sharedTheme];
    [theme styleMainView:self.view];
}
    
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //ASFSetLoggingEnabled(YES);
//    [[ASFSession sharedSession] startSession];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}
    
- (void)close{
    

}

@end
