//
//  ZMDocument.m
//  Pronto
//
//  Created by Sebastian Romero on 2/21/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//
#import "ZMDocument.h"
#import <WebKit/WebKit.h>
#import "Pronto-Swift.h"
#import "Constant.h"
#import "Encryptor.h"
#import "NSDate+DateDirectory.h"
#import "ProntoUserDefaults.h"

@interface ZMDocument ()
{
   // __weak IBOutlet UIWebView *webViewer;
    
//    IBOutlet WKWebView *webLinkViewer;
    
}

@property(strong,nonatomic) IBOutlet WKWebView *webLinkViewer;
@property(strong,nonatomic) IBOutlet UIWebView *sampleWebView;
@property (strong,nonatomic) UIView *overlayView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PreviousAssetWidthConstraint;
@end

@interface WKPreferences (WKPrivate)
@property (nonatomic, setter=_setDeveloperExtrasEnabled:) BOOL _developerExtrasEnabled;
@end

@implementation ZMDocument
@synthesize webLinkViewer;

+ (ZMDocument *)sharedInstance {
    
    @synchronized(self)
    {
        static ZMDocument *manager = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            manager = [[self alloc] init];
        });
        return manager;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    [self loadAsset];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    // to stop the embedded video/audio is playing from weblink
    if([self.asset.file_mime isEqualToString:@"weblink" ])
    {
        [self loadWebPageWithURL:@"about:blank" andWebview:self.webLinkViewer];
        self.webLinkViewer.configuration.allowsInlineMediaPlayback = TRUE;
    }
    
    self.webLinkViewer = nil;
    [self.webLinkViewer removeFromSuperview];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

- (void) loadAsset
{
    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    
    if([self.asset.file_mime isEqualToString:@"weblink" ])
    {
        if([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
        {
            WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];

            self.webLinkViewer = [[WKWebView alloc] initWithFrame:self.view.frame configuration:configuration];

            self.webLinkViewer.navigationDelegate = self;
            self.webLinkViewer.UIDelegate = self;
            self.webLinkViewer.contentMode = UIViewContentModeScaleAspectFit;
            [self.view addSubview:self.webLinkViewer];
            [self.webLinkViewer setTranslatesAutoresizingMaskIntoConstraints:NO];
            [self.webLinkViewer.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
            [self.webLinkViewer.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
            [self.webLinkViewer.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
            [self.webLinkViewer.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:168].active = YES;
            _PreviousAssetWidthConstraint.constant = self.view.frame.size.width/2;

            [self loadWebPageWithURL:self.asset.uri_full andWebview:self.webLinkViewer];
        }
        else
        {
            
            NSString* msg = @"Please verify your Internet connection and try again";
            
            // Alternative AlertView
            UIAlertController* message = [UIAlertController alertControllerWithTitle:@""  message:msg  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               //Handle OK button
                                           }];
            [message addAction:okButton];
            [self presentViewController:message animated:YES completion:nil];
            
        }
            
      
    }
    else if ([self.asset.file_mime rangeOfString:@"video" ].location)
    {
        
        // Fix for the crash - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** -[NSURL initFileURLWithPath:]: nil string parameter'
        if(self.asset.path)
        {
            [self loadWebPageWithURL:self.asset.path andWebview:self.webLinkViewer];
          
        }
    }
}

- (void)loadWebPageWithURL:(NSString *)urlString andWebview:(WKWebView *)webview
{
    if([urlString containsString:dlServerHostName])
    {
        NSDate *timeOfEncryption = [[ProntoUserDefaults userDefaults] getdlServerEncryptionTime];
        int timeDifference = 0;
        if(timeOfEncryption != nil)
        {
            timeDifference = [[NSDate date] timeIntervalSinceDate:timeOfEncryption];
        }
        timeDifference = ceil(timeDifference / 60);
        NSString* encryptedString = @"";
        if(timeDifference > 20 || timeDifference == 0)
        {
            NSString *dateString = [NSDate getDateAsString:[NSDate date]];
            NSString *stringToEncrypt = [kSecureValueToEncrypt stringByAppendingString:dateString];//"isFromSecuredApp"
            encryptedString = [Encryptor encryptedData:stringToEncrypt WithHexKey:keyToEncrypt];
            
            [[ProntoUserDefaults userDefaults] setdlServerEncryptionTime:[NSDate date]];
            [[ProntoUserDefaults userDefaults] setdlServerEncryptionValue:encryptedString];
        }
        else
        {
            encryptedString = [[ProntoUserDefaults userDefaults] getdlServerEncryptionValue];
        }
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"?encrypted=%@", encryptedString]];
    }
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [webview loadRequest:urlRequest];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation{
    
    // The web starts loading contents.
    NSLog(@"wURL:%@",webView.URL.absoluteString);
    _overlayView = [[UIView alloc]initWithFrame:webView.frame];
    _overlayView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_overlayView];
}


- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    NSLog(@"finish wURL:%@",webView.URL.absoluteString);
    if([self.view.subviews containsObject:_overlayView]){
       [_overlayView removeFromSuperview];
    }
}

-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    
    if (!navigationAction.targetFrame.isMainFrame || navigationAction.targetFrame == nil) {
        
        WKWebView *newWebview = [[WKWebView alloc] initWithFrame:self.webLinkViewer.frame configuration:configuration];
        newWebview.UIDelegate = self;
        newWebview.navigationDelegate = self;
        [newWebview loadRequest:navigationAction.request];
        [self.view addSubview:newWebview];
        
        return  newWebview;
    }
    
    return nil;
}

- (void)webView:(WKWebView *)webView
decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if (navigationAction.targetFrame == nil) {
        NSURL *url = navigationAction.request.URL;
        
        UIApplication *app = [UIApplication sharedApplication];
        if ([app canOpenURL:url]) {
            [app openURL:url options:@{} completionHandler:nil];
        }
    }
    decisionHandler(WKNavigationActionPolicyAllow);
    
}

//PRONTO-25 Web View - Ability to provide a Web link as an asset type.
- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"self.asset.file_mime:%@",self.asset.file_mime]];
    if([self.asset.file_mime isEqualToString:@"weblink" ])
    {

        _PreviousAssetWidthConstraint.constant = self.view.frame.size.width/2;
        [self.view updateConstraints];

        [self.assetDescriptionBar reloadInputViews];
    }
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    /*UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:@"Device in offline" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];*/
    
    
    UIAlertController  *alert  = [UIAlertController
                           alertControllerWithTitle:@"Device is offline"
                           message:@"Please check your internet connection."
                           preferredStyle:UIAlertControllerStyleAlert];

    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle OK button
                               }];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError*) err{
    
    if([self.navigationController.topViewController isKindOfClass:[ZMDocument class]]){
    UIAlertController  *alert  = [UIAlertController
                                  alertControllerWithTitle:@"Error"
                                  message:err.localizedDescription
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle OK button
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    }
}
- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Request~:%@",request]];
    return true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    //[self.webLinkViewer removeObserver:self forKeyPath:@"estimatedProgress"];
    
    // if you have set either WKWebView delegate also set these to nil here
    [self.webLinkViewer setNavigationDelegate:nil];
    [self.webLinkViewer setUIDelegate:nil];
}

@end
