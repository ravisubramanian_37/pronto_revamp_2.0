//
//  ZMProntoViewController.m
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import "ZMProntoViewController.h"

@interface ZMProntoViewController ()

@end

@implementation ZMProntoViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    
    //Check if setNeedsStatusBarAppearanceUpdate is defined this is for iOS 6
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
