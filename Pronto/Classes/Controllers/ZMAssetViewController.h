//
//  ZMAssetViewController.h
//  Pronto
//
//  Created by Sebastian Romero on 2/21/14.
//  Copyright (c) 2014 Abbvie Inc. All rights reserved.
//
#import "ZMProntoTheme.h"
#import "ZMAssetsLibrary.h"
#import "ZMAsset.h"
#import "ZMHeader.h"
#import "ZMProntoViewController.h"
#import "Asset.h"


//@class ZMLibrary;
//@class Competitors;
//@class ToolsTraining;
@class EventsViewController;
@class ZMAssetViewController;
//@class ZMPDFDocument;
@class FastPDFViewController;

/**
 *
 **/
@protocol ZMAssetViewControllerDelegate <NSObject>

-(void)assetViewController:(ZMAssetViewController*)assetVCView didTapLastViewedButton:(UIButton*)sender forAsset:(NSObject*)asset;
    
-(void)pdfDocument:(FastPDFViewController*)pdfDocumentView didTapLastViewedForAsset:(NSObject*)asset;


@required
/**
 *  Asset to load
 */
@property (strong, nonatomic) Asset *asset;
@property (strong,nonatomic) NSString * videoId;
/**
 *  Theme shared to all subclasses
 */
@property (weak, nonatomic) id <ZMProntoTheme> theme;


/**
 *  Detail header
 */
@property (strong, nonatomic) UIView *headerView;


/**
 *  Library reference
 */
//@property (weak, nonatomic) ZMLibrary *library;

/**
 *  Competitors reference
 */
//@property (weak, nonatomic) Competitors *competitors;

/**
 *  ToolsTraining reference
 */
//@property (weak, nonatomic) ToolsTraining *toolsTraining;

/**
 *  Events reference
 */
@property (weak, nonatomic) EventsViewController *eventsViewController;

/**
 *  CalendarDetailVC reference
 */

@property (weak, nonatomic) CalendarDetailVC *calendarDetailVC;


/**
 *  Flag to determine if a folder was or not created
 */
@property (nonatomic) BOOL *folderCreated;


@property (nonatomic) int tierTrackingValue;


@property (strong,nonatomic) NSString * userFranchise;


@property (strong, nonatomic) UIView *viewMain;

@property (strong, nonatomic) UIView *webviewScreen;
/**
 *  Send the events to the CMS after rate
 */
- (void)assetControllerDelegateSendEvent;


@optional

/**
 *  Header Container
 */
@property (weak, nonatomic) IBOutlet UIView *headerContainer;


@end

@interface ZMAssetViewController : ZMProntoViewController <ZMAssetViewControllerDelegate,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate, headerActions>


@property (strong, nonatomic) Asset *asset;
@property (weak, nonatomic) id <ZMProntoTheme> theme;
@property (strong, nonatomic) UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *headerContainer;
@property (weak, nonatomic) IBOutlet UIView *assetDetailBar;
@property (weak, nonatomic) IBOutlet UIView *assetDescriptionBar;
@property (weak, nonatomic) IBOutlet UILabel *assetTitle;
@property (weak, nonatomic) IBOutlet UILabel *previousAssetTitle;
@property (weak, nonatomic) IBOutlet UIView *previousAssetContainer;
@property (weak, nonatomic) IBOutlet UIButton *favouriteButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *previousAssetButton;
//@property (weak, nonatomic) ZMLibrary *library;
//@property (weak, nonatomic) Competitors *competitors;
//@property (weak, nonatomic) ToolsTraining *toolsTraining;
@property (weak, nonatomic) EventsViewController *eventsViewController;
@property (weak, nonatomic) CalendarDetailVC *calendarDetailVC;
@property (nonatomic) BOOL *folderCreated;
@property (nonatomic) int tierTrackingValue;
@property (strong,nonatomic) NSString * videoId;
        
@property (nonatomic, weak) id<ZMAssetViewControllerDelegate> delegate;

/**
 *  This value provides detail of type of asset being opened
 */
@property (nonatomic, assign) BOOL isEventOnlyAsset;


//-(void)sendEvent;

-(void)showHideHeader:(BOOL)hide;
- (void)addActivityIndicator;

+(NSString *)getAssetType:(Asset *)asset;

@end
