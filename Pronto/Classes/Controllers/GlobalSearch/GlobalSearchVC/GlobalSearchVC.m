 //
//  GlobalSearchVC.m
//  Pronto
//
//  Created by cccuser on 16/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "GlobalSearchVC.h"
#import "GlobalSearchViewModel.h"
#import "FranchiseTableViewCell.h"
#import "OfflineDownload+CoreDataClass.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "ZMAbbvieAPI.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "ZMUserActions.h"
#import "Asset.h"
#import "ZMAbbvieAPI.h"
#import "ZMProntoTheme.h"
#import "ViewUtils.h"
#import "AssetDownloadProvider.h"
#import "OfflineDownload+CoreDataClass.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "Pronto-Swift.h"
#import "ZMDocument.h"
//#import "ZMPDFDocument.h"
#import "ZMTracking.h"
#import "ExportMenu.h"
#import "FavFranchiseAsset.h"
#import "ZMContentHeader.h"
#import "ViewUtils.h"
#import "UIColor+ColorDirectory.h"
#import "ZMEmpty.h"
#import "ZMSortMenu.h"
#import "ZMLibrary.h"
#import "GSGridCollectionHeaderView.h"
#import "FeedbackView.h"
#import "FlatTabBarViewController.h"
#import "AssetDownloadProvider.h"
#import "NSMutableDictionary+Utilities.h"
#import "AppUtilities.h"
#import "EventsGlobalSearch.h"
#import "EventOnlyAssetHandler.h"
#import "EventAssetHandler.h"
#import "EventCoreDataManager.h"
#import "ProntoUserDefaults.h"
#import "EventsCalendarDetailsScreenVC.h"
#import "CalendarDetailVC.h"

#import "Session+CoreDataClass.h"
#import "Session+CoreDataProperties.h"

#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"

#import "EventDuration+CoreDataClass.h"
#import "EventDuration+CoreDataProperties.h"

#import "Event+CoreDataClass.h"
#import "Event+CoreDataProperties.h"
#import "HelpViewController.h"
#import "ZMNotifications.h"
#import "UIImage+ImageDirectory.h"

#import "EventCoreDataManager.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"
#import "EventsServiceManager.h"

#import "FastPDFViewController.h"
#import <FastPdfKit/FastPdfKit.h>

const CGFloat gridItemWidthForGS = 232.0;
const CGFloat heightForSection   = 54;
const CGFloat widthOfSingleTab   = 130;

static NSString * const GSLibraryViewStoryboardName = @"GlobalSearchView";
static NSString * const GSLibraryViewControllerIdentifier = @"GlobalSearchVC";

static NSString * const GSLibraryViewTypeTableView = @"List";
static NSString * const GSLibraryViewTypeCollectionView = @"GlobalSearchView";
static NSString * const GSHeaderViewNibName = @"GlobalSearchHeaderView";

@interface GlobalSearchVC ()<UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIPopoverPresentationControllerDelegate>
{
    NSInteger selectedItem;
    BOOL favIconTapped;
    id <ZMProntoTheme> theme;
    UIView *backgroundView;
    
    //For Audio Player
    AudioPlayer *audioPlayerView;
    Asset *assetForAudio;
    FranchiseTableViewCell *audioCurrentCell;
    ZMGridItem *audioCurrentGridCell;
    BOOL isScreenVisible;
}

@property(nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, assign) NSInteger previousAssetCount;
@property(nonatomic, strong) NSArray * initialDownloadDbArray;
@property (nonatomic, weak) AppDelegate *mainDelegate;
@property (nonatomic, assign) BOOL isDownloadingAsset;
@property (nonatomic, assign) BOOL isTapDownloadInitiated;
@property (nonatomic, strong) Asset *currentAssetDownload;

@property (nonatomic, strong) id popOverPresentingCell;
@property (strong, nonatomic) Asset *currentlySelectedAsset;
@property (nonatomic, strong) ZMContentHeader *contentHeader;
@property (nonatomic, weak) ZMAbbvieAPI *apiManager;

@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *listGridContainerView;
@property (weak, nonatomic) IBOutlet UIView *headerContainerView;
@property (weak, nonatomic) IBOutlet UIView *flatTabBarContainerView;
@property (nonatomic, strong) FlatTabBarViewController *flatTabBarViewController;
@property (strong, nonatomic) ZMUserActions *actions;
@property (nonatomic, strong) EventsGlobalSearch *eventsGlobalSearch;
@property (nonatomic, strong) EventOnlyAssetHandler *eventOnlyAssetHandler;
@property (nonatomic, strong) ZMNotifications *notificationsPannel;
@property (nonatomic, strong)  UIView *backgroundBlackView;

@property (nonatomic) int pageIndex;
@property (nonatomic, strong) ZMEmpty *emptyView;

//ZMHeader
@property (nonatomic, strong) ZMSortMenu *ttSortMenu;
@property (nonatomic, strong) UIPopoverPresentationController *ttSortMenuPopOver;
@property (nonatomic, strong) UIButton *ttSortMenuButton;

//Tab Bar Selected index
@property (nonatomic, assign) NSInteger selectedTab; // Selected TAB

@property (nonatomic, strong) Asset *currentSelectedAssetToSeeDetail;
@property (nonatomic, assign) BOOL isSyncAllEventsCalledFromBackgroundToForeground;

@end

@implementation GlobalSearchVC

+ (GlobalSearchVC*)GetGSLibraryViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:GSLibraryViewStoryboardName bundle:nil];
    GlobalSearchVC *gsLibraryVC = [sb instantiateViewControllerWithIdentifier:GSLibraryViewControllerIdentifier];
    return gsLibraryVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeData];
    [self setupUI];
    [self setupHeaderContainerViewFrame];
    [self loadZMContenHeaderView];
    [self addTabBar];
    [self.flatTabBarViewController selectItemAtIndex:0 andNotifyBack:NO];
    [self setupViewsFrames];
    
    [self syncAllEvents];
}

- (void)setupViewsFrames
{
    //Setting ContentHeader Container  View Size
    self.headerContainerView.frame = CGRectMake(0, 0, self.view.width, self.headerContainerView.height);
    
    //Setting ContentHeader   View Size
    self.contentHeader.frame = self.headerContainerView.bounds;
    
    // Setting OverLay View Size
    self.overlayView.frame = self.view.bounds;
    
    //Setting List Grid Container View Size
    self.listGridContainerView.frame = CGRectMake(16, self.headerContainerView.height, self.view.width - 32, self.view.height - self.headerContainerView.height);
    
    //Setting Flat Tab Bar Container View
    self.flatTabBarContainerView.frame = CGRectMake(0, 0, self.view.width, self.flatTabBarContainerView.height);
    
    //Setting Table View and Collection view Frame.
    self.gsLibraryTableView.frame       = CGRectMake(0, self.flatTabBarContainerView.height, self.listGridContainerView.width, self.listGridContainerView.bounds.size.height - self.flatTabBarContainerView.height);
    self.gsLibraryColectionView.frame   = self.gsLibraryTableView.frame;    
}

- (void)setupHeaderContainerViewFrame
{
    self.headerContainerView.frame = CGRectMake(0, 0, self.view.width, self.headerContainerView.height);
}

- (void)loadZMContenHeaderView
{
    if (self.contentHeader == nil)
    {
        self.contentHeader = (ZMContentHeader *)[[[NSBundle mainBundle] loadNibNamed:GSHeaderViewNibName owner:self options:nil] objectAtIndex:0];
        self.contentHeader.zmContentHeaderDelegate = (id<ZMContentHeaderDelegate>)self;
    }
    
    self.contentHeader.frame = self.headerContainerView.bounds;
    [self.headerContainerView addSubview:self.contentHeader];
    
    //update view
    [self.contentHeader updateSortByButton];
    [self.contentHeader updateFranchiselLabelForGlobalSearchWithText:nil];
}

- (void)prepareTransparentOverlayView
{
    self.overlayView.backgroundColor = [UIColor GetTransparentOverlayColor];
}

- (void) showTransparentOverlayView:(BOOL)yesOrNo
{
    if (yesOrNo == YES)
    {
        self.overlayView.backgroundColor = [UIColor GetTransparentOverlayColor];
        
        self.view.backgroundColor                    = [UIColor clearColor];
        self.headerContainerView.backgroundColor     = [UIColor clearColor];
        self.listGridContainerView.backgroundColor   = [UIColor clearColor];
        self.gsLibraryTableView.backgroundColor      = [UIColor clearColor];
        self.gsLibraryColectionView.backgroundColor  = [UIColor clearColor];
        self.flatTabBarContainerView.backgroundColor = [UIColor clearColor];
        
        self.overlayView.hidden             = NO;
        self.headerContainerView.hidden     = YES;
        self.listGridContainerView.hidden   = YES;
    }
    else
    {
        self.view.backgroundColor                    = [UIColor GetColorFromHexValuef2f2f2];
        self.headerContainerView.backgroundColor     = [UIColor whiteColor];
        self.listGridContainerView.backgroundColor   = [UIColor whiteColor];
        self.gsLibraryTableView.backgroundColor      = [UIColor whiteColor];
        self.gsLibraryColectionView.backgroundColor  = [UIColor whiteColor];
        self.flatTabBarContainerView.backgroundColor = [UIColor GetColorFromHexValuef2f2f2];
        
        self.overlayView.backgroundColor = [UIColor whiteColor];
        
        self.overlayView.hidden             = YES;
        self.headerContainerView.hidden     = NO;
        self.listGridContainerView.hidden   = NO;
    }
}
- (void)styleLibrary
{
    [[ZMProntoTheme sharedTheme] styleMainView:self.view];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if (self.overlayView.hidden == NO)
    {
        if(self.parentHeader.settingsMenuIsPresented == NO)
        {
            [self dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
        }
    }
}

/**
 *  Long press handler
 *  @param gestureRecognizer gestureRecognizer event
 */
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
}

-(void)configureGridView
{
    [self styleLibrary];
    
    self.gsLibraryColectionView.alwaysBounceVertical = YES;
    self.gsLibraryColectionView.allowsSelection = YES;
    [self.gsLibraryColectionView setDataSource:self];
    [self.gsLibraryColectionView setDelegate:self];
    self.gsLibraryColectionView.hidden = NO;
    [self.gsLibraryColectionView reloadData];
    
    if (self.longPress == nil)
    {
        self.longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        self.longPress.minimumPressDuration = .5;
        [self.gsLibraryColectionView addGestureRecognizer:self.longPress];
    }
    
    if (self.tapGestureRecognizer == nil)
    {
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
        [self.gsLibraryColectionView addGestureRecognizer:self.tapGestureRecognizer];
        self.tapGestureRecognizer.cancelsTouchesInView = NO;
    }
    
    self.gsLibraryTableView.hidden = YES;
}

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (void) configureListView
{
    [self styleLibrary];
    [self.gsLibraryTableView setDelegate: self];
    [self.gsLibraryTableView setDataSource: self];
    self.gsLibraryTableView.hidden = NO;
    
    self.gsLibraryColectionView.hidden = YES;
    [self.gsLibraryTableView reloadData];
}

- (void)showListOrGridView
{
    if([[[NSUserDefaults standardUserDefaults] objectForKey:keyUserDefaultGridFranchise] isEqualToString:@"NO"] || ![[NSUserDefaults standardUserDefaults] objectForKey:keyUserDefaultGridFranchise] )
    {
        [self configureListView];
    }
    else
    {
        //Configure Grid View
        [self configureGridView];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[ZMAssetsLibrary defaultLibrary] removeObserver:self];
    [self resetTextSearchValues];
    self.currentSelectedAssetToSeeDetail = nil;
    self.isSyncAllEventsCalledFromBackgroundToForeground = NO;
    isScreenVisible = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self stopAudio];
}

- (void)removeNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:name object:nil];
}

#pragma mark - Notifications

- (void)registerNotification:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:name object:nil];
}

- (void)recievedNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:kZMSortMenuDidSelectRowNotification])
    {
        [self sortMenuDidSelectItem];
    }
    else if ([[notification name] isEqualToString:kZMPDFDocumentDidCloseButtonPressedNotification])
    {
        if (self.navigationController.viewControllers.count > 1)
        {
            UIViewController *popToVC = [self getViewControllerOnNavigationHasToBePopped];
            if (popToVC != nil)
            {
                [self.navigationController popToViewController:popToVC animated:YES];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if ([[notification name] isEqualToString:kZMAssetViewControllerGoBackNotification])
    {
        if (self.navigationController.viewControllers.count > 1)
        {
            UIViewController *popToVC = [self getViewControllerOnNavigationHasToBePopped];
            if (popToVC != nil)
            {
                [self.navigationController popToViewController:popToVC animated:YES];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET"])
    {
        Asset *asset = [notification.userInfo objectForKey:@"asset"];
        if([ZMUserActions sharedInstance].countOfAssetDict == 0 && [ZMUserActions sharedInstance].globalSearchViewController)
        {
            [self validateAndShowAsset:asset];
            [ZMUserActions sharedInstance].countOfAssetDict += 1;
        }
    }
    else if ([[notification name] isEqualToString:@"OPEN_TEMP_ASSET"])
    {
//        [self.parentHeader.library openTempAsset:[notification.userInfo objectForKey:@"asset_link"]];
    }
    else if ([[notification name] isEqualToString:@"CMS_BRING_ALL_DATA"])
    {
        [[ZMProntoManager sharedInstance].search reset];
    }
    else if ([[notification name] isEqualToString:@"HIDE_LIBRARY_POPOVERS"])
    {
        [self.parentHeader.library.headerView hidePopovers];
        [self.parentHeader.library resetPopOvers];
    }
    else if ([[notification name] isEqualToString:@"OPEN_TEMP_ASSET"])
    {
//        self.parentHeader.library.resestFranchises = NO;
//        [self.parentHeader.library openTempAsset:[notification.userInfo objectForKey:@"asset_link"]];
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_APP_CLOSED"])
    {
        self.parentHeader.library.resestFranchises = NO;
        NSString * idAsset = [notification.userInfo objectForKey:@"assetString"];
        
        //PRONTO-37 Deeplinking to a Page in a PDF
        // same format but caseinsensitive
        NSInteger pageNumber = 0;
        NSArray* deeplinkPage = [idAsset componentsSeparatedByString:@"&"];
        NSArray* deeplinkPageNo = [idAsset componentsSeparatedByString:@"="];
        NSString* stringToCheck = [NSString stringWithFormat:@"%@%@",@"pageNum=",[deeplinkPageNo lastObject]];
        if(deeplinkPage.count >1 )
        {
            if(isShowing(deeplinkPage,[deeplinkPage lastObject]))
            {
                idAsset = [idAsset stringByReplacingOccurrencesOfString:[deeplinkPage lastObject] withString:stringToCheck];
                NSArray* pageNums = [idAsset componentsSeparatedByString:@"&pageNum="];
                
                if (pageNums.count > 1) {
                    pageNumber = [[pageNums lastObject] integerValue];
                    idAsset = [pageNums firstObject];
                }
            }
        }
        
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:idAsset];
        
        if (assetArray.count > 0) {
            Asset * asset = assetArray[0];
            //PRONTO-37 Deeplinking to a Page in a PDF
            [ZMUserActions sharedInstance].pageNo = pageNumber;
            [self validateAndShowAsset:asset];
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.mainDelegate showMessage:@"Asset not found" whithType:ZMProntoMessagesTypeWarning];
            });
        }
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_FOUND"])
    {
        self.parentHeader.library.resestFranchises = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            Asset *asset = [notification.userInfo objectForKey:@"asset"];
            enum ZMProntoFileTypes assetType = [Constant  GetFileTypeForAsset:asset];
            
            if (assetType != ZMProntoFileTypesBrightcove) {
                [self.mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                // asset.isUpdateAvailable = NO;
            }
            else
            {
                // for video file
                asset.isUpdateAvailable = NO;
            }
            
            [self validateAndShowAsset:asset];
        });
    }
    else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_NOT_FOUND"])
    {
        self.parentHeader.library.resestFranchises = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mainDelegate showMessage:@"Asset not found" whithType:ZMProntoMessagesTypeWarning];
        });
    }

}

- (UIViewController*)getViewControllerOnNavigationHasToBePopped
{
    UIViewController *ctrl = nil;
    NSEnumerator *enumarator = [self.navigationController.viewControllers reverseObjectEnumerator] ;
    for (UIView *vc in enumarator)
    {
        if ([vc isKindOfClass:[CalendarDetailVC class]])
        {
            ctrl = (CalendarDetailVC*)vc;
            break;
        }
        else if ([vc isKindOfClass:[EventsCalendarDetailsScreenVC class]])
        {
            ctrl = (EventsCalendarDetailsScreenVC*)vc;
            break;
        }
        else if ([vc isKindOfClass:[ProntoTabbarViewController class]])
        {
            ctrl = (EventsCalendarDetailsScreenVC*)vc;
            break;
        }
    }
    return ctrl;
}

- (void)sortMenuDidSelectItem
{
    [self.ttSortMenu dismissViewControllerAnimated:YES completion:NULL];
    [self reloadTableViewOrCollectionView];
    [self loadSearchedAssets];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setupViewsFrames];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Register for notification
    [self removeNotification:kZMPDFDocumentDidCloseButtonPressedNotification];
    [self registerNotification:kZMPDFDocumentDidCloseButtonPressedNotification];
    
    [self removeNotification:kZMAssetViewControllerGoBackNotification];
    [self registerNotification:kZMAssetViewControllerGoBackNotification];
    
    [self removeNotification:kZMSortMenuDidSelectRowNotification];
    [self registerNotification:kZMSortMenuDidSelectRowNotification];
    
//    [self registerNotification:@"OPEN_SCHEMA_ASSET"];
//    [self registerNotification:@"OPEN_SCHEMA_ASSET_APP_CLOSED"];
    [self registerNotification:@"CMS_BRING_ALL_DATA"];
//    [self registerNotification:@"OPEN_TEMP_ASSET"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET_FOUND"];
    [self registerNotification:@"OPEN_SCHEMA_ASSET_NOT_FOUND"];

    if(self.selectedTab != 3)
    {
        [self showListOrGridView];
    }
    [self setupViewsFrames];
    [[ZMAssetsLibrary defaultLibrary] addObserver:self];
    if (self.overlayView.isHidden == YES)
    {
        [self showTransparentOverlayView:NO];
        if (self.parentHeader.isShowingIndicator == YES)
        {
            [self.parentHeader hidingIndicator];
        }
    }
    else
    {
        [self prepareTransparentOverlayView];
        [self showTransparentOverlayView:YES];
    }
    
    self.parentHeader.sourceViewController = self;
    
    self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
    [self.eventsGlobalSearch reloadTableView];
    
    isScreenVisible = YES;
}

- (void)willAnimateRotationToInterfaceOrientation: (UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self setupViewsFrames];
    [self resetTheSourceRectValueOfTTSortMenuView];
    [self reloadTableViewOrCollectionView];
    [_feedback adjustFrame:self.view];
    [self.view layoutSubviews];
    [self.mainDelegate didOrientationChange];
    
    [self.parentHeader adjustGlobalSearchIndicator];
    
//    //Needs to reload the values of tableOrCollectionView which is getting displayed currently
//    if(self.eventsGlobalSearch != nil)
//    {
//        //It means Events Global search view is getting displayed.
//        [self.eventsGlobalSearch reloadTableView];
//    }
//    else
//    {
//         [self reloadTableViewOrCollectionView];
//    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];

    if (_exportMenuIsPresented) {
        [[_exportMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        if ([showingGridView isEqualToString:@"YES"])
        {
            ZMGridItem *tempCell = (ZMGridItem *)[_gsLibraryColectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (tempCell && tempCell.shareButton.isSelected)
            {
                [tempCell makeShareButtonSelected:NO];
            }
        }
        else
        {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_gsLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.shareButton.isSelected)
            {
                [cell makeShareButtonSelected:NO];
            }
        }
        
    }
    if (_infoMenuIsPresented)
    {
        [[_infoMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        if ([showingGridView isEqualToString:@"YES"])
        {
            ZMGridItem *tempCell = (ZMGridItem *)[_gsLibraryColectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (tempCell && tempCell.infoButton.isSelected)
            {
                [tempCell makeInfoButtonSelected:NO];
            }
        }
        else
        {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_gsLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.infoButton.isSelected)
            {
                [cell makeInfoButtonSelected:NO];
            }
        }
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if (_exportMenuIsPresented)
    {
        if([ZMUserActions sharedInstance].export_infoMenuValOnOrientation == 0)
        {
            if ([showingGridView isEqualToString:@"YES"])
            {
                ZMGridItem *tempCell = (ZMGridItem *)[_gsLibraryColectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (tempCell && !tempCell.shareButton.isSelected)
                {
                    [tempCell makeShareButtonSelected:YES];
                    [self openExportMenu:tempCell sender:tempCell.shareButton];
                }
            }
            else
            {
                FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_gsLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (cell && !cell.shareButton.isSelected)
                {
                    [cell makeShareButtonSelected:YES];
                    [self openExportMenu:cell sender:cell.shareButton];
                }
            }
        }
        else
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
            _exportMenuIsPresented = NO;
        }
    }
    
    if (_infoMenuIsPresented)
    {
        if([ZMUserActions sharedInstance].export_infoMenuValOnOrientation == 0)
        {
            if ([showingGridView isEqualToString:@"YES"])
            {
                ZMGridItem *tempCell = (ZMGridItem *)[_gsLibraryColectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (tempCell && !tempCell.infoButton.isSelected)
                {
                    [tempCell makeInfoButtonSelected:YES];
                    [self openImportMenu:tempCell sender:tempCell.infoButton];
                }
            }
            else
            {
                FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_gsLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
                if (cell && !cell.infoButton.isSelected)
                {
                    [cell makeInfoButtonSelected:YES];
                    [self openImportMenu:cell sender:cell.infoButton];
                }
            }
        }
        else
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
            _infoMenuIsPresented = NO;
        }
    }
    
    if(assetForAudio != nil)
    {
        if (!self.gsLibraryColectionView.hidden)
        {
            [self.gsLibraryColectionView reloadData];
        }
    }

}

- (void)resetTheSourceRectValueOfTTSortMenuView
{
    self.ttSortMenu.popoverPresentationController.sourceRect = self.ttSortMenuButton.frame;
    self.ttSortMenuPopOver.sourceRect = CGRectMake(self.ttSortMenuButton.frame.origin.x + (self.ttSortMenuButton.frame.size.width/2) ,self.ttSortMenuButton.frame.origin.y+104,1, 1);
}

- (void)setupUI
{
    [self registerTableViewCell];
    [self registerCollectionViewCell];
    self.gsLibraryTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.gsLibraryColectionView.dataSource = self;
    self.gsLibraryColectionView.delegate   = self;
    
    [self prepareTransparentOverlayView];
}
- (void)initializeData
{
    self.mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.apiManager   = [ZMAbbvieAPI sharedAPIManager];
    theme             = [ZMProntoTheme sharedTheme];
    self.actions      = [ZMUserActions sharedInstance];
    self.globalSearchViewModel = [[GlobalSearchViewModel alloc]initWithDelegate:(id<GlobalSearchViewModelDelegate>)self];
}

- (void)registerCollectionViewCell
{
    [self.gsLibraryColectionView registerNib:[UINib nibWithNibName:[ZMGridItem GetGridItemCellNibName] bundle:nil]
                   forCellWithReuseIdentifier:[ZMGridItem GetGridItemCellIdentifier]];
}

- (void)registerTableViewCell
{
    [self.gsLibraryTableView registerNib:[UINib nibWithNibName:[FranchiseTableViewCell GetFranchiseTableViewCellNibName] bundle:nil]
                  forCellReuseIdentifier:[FranchiseTableViewCell GetFranchiseTableViewCellIdentifier]];
}

- (void)initiateFeedback:(NSString*) selectedAsset openedFrom:(NSString*) choice {
    _feedback = (FeedbackView *)[[[NSBundle mainBundle] loadNibNamed:@"FeedbackViewController" owner:self options:nil] objectAtIndex:0];
    _feedback.assetId = selectedAsset;
    // display feedback view on top of the view
    backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [_feedback initWithParent:[UIApplication sharedApplication].keyWindow background:backgroundView openedFrom:choice];
        [[_exportMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        if(_exportMenuIsPresented) {
            NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
            if ([showingGridView isEqualToString:@"YES"]) {
                ZMGridItem *cell = (ZMGridItem *)[_gsLibraryColectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
                [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
                [cell.shareButton setSelected:NO];
            } else
            {
                FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[_gsLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
                [cell.shareButton setImage:[UIImage imageNamed:@"exportIcon"] forState:UIControlStateNormal];
                [cell.shareButton setSelected:NO];
            }
            [_exportMenu dismissViewControllerAnimated:NO completion:nil];
            _exportMenuIsPresented = NO;
        }
}

#pragma mark - Table View Data Source and Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 96 + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FranchiseTableViewCell *cell = (FranchiseTableViewCell *) [tableView dequeueReusableCellWithIdentifier:[FranchiseTableViewCell GetFranchiseTableViewCellIdentifier]];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:[FranchiseTableViewCell GetFranchiseTableViewCellNibName]
                                                     owner:self options:nil];
        
        [tableView registerNib:[UINib nibWithNibName:[FranchiseTableViewCell GetFranchiseTableViewCellNibName] bundle:nil] forCellReuseIdentifier:[FranchiseTableViewCell GetFranchiseTableViewCellIdentifier]];
        cell = (FranchiseTableViewCell*)[nib objectAtIndex:0];
    }
    cell.delegate = (id <ZMListItemDelegate>)self;
    if ([self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab] > 0 &&
        indexPath.row < [self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab] )
    {
        Asset * asset = [self.globalSearchViewModel getAssetForSection:self.selectedTab
                                                                andRow:indexPath.row];
        [cell prepareCellWithAsset:asset atIndexPath:indexPath];
        [cell createRactangleAroundView];
        cell.audioPlayerView.hidden = YES;
        if(asset.assetID == assetForAudio.assetID)
        {
            cell.audioPlayerView.hidden = NO;
            [audioPlayerView removeFromSuperview];
            audioPlayerView.frame = CGRectMake(0, 0,cell.audioPlayerView.frame.size.width, cell.audioPlayerView.frame.size.height);
            [cell.audioPlayerView addSubview:audioPlayerView];
        }

    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated: YES];

    if ([self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab] > 0 &&
        indexPath.row < [self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab])
    {
        Asset * asset = [self.globalSearchViewModel getAssetForSection:self.selectedTab
                                                                andRow:indexPath.row];
        if (asset == nil) {return;}

        if ([asset title].length > 0)
        {
            [self validateAndShowAsset:asset];
        }
    }
}

#pragma mark - Collection View Data Source and Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZMGridItem *cell = (ZMGridItem*)[collectionView dequeueReusableCellWithReuseIdentifier:[ZMGridItem GetGridItemCellIdentifier]
                                                                              forIndexPath:indexPath];
    if ([self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab] > 0 &&
        indexPath.row < [self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab] )
    {
        Asset * asset = [self.globalSearchViewModel getAssetForSection:self.selectedTab
                                                                andRow:indexPath.row];
        [cell prepareCellWithAsset:asset atIndexPath:indexPath];
        cell.delegate = (id<ZMGridItemDelegate>)self;
        cell.audioPlayerView.hidden = YES;
        if(asset.assetID == assetForAudio.assetID)
        {
            cell.audioPlayerView.hidden = NO;
            [audioPlayerView removeFromSuperview];
            audioPlayerView.frame = CGRectMake(0, 0,cell.audioPlayerView.frame.size.width, cell.audioPlayerView.frame.size.height);
            [cell.audioPlayerView addSubview:audioPlayerView];
        }

    }
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZMGridItem *cell = (ZMGridItem*)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell.type == ZMGridItemTypeAsset)
    {
        if ([self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab] > 0 &&
            indexPath.row < [self.globalSearchViewModel getNumberOfRowsInSection:self.selectedTab])
        {
            
            Asset * asset = [self.globalSearchViewModel getAssetForSection:self.selectedTab
                                                                    andRow:indexPath.row];
            if (asset == nil) {return;}
            
            if ([asset title].length > 0)
            {
                [self validateAndShowAsset:asset];
            }
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat baseLineWidth = 232.0;
    CGFloat divider = floor((self.gsLibraryColectionView.width - 10) / baseLineWidth);
    CGFloat itemWidth = 0.0;
    itemWidth = self.gsLibraryColectionView.width / divider - 10;
    return CGSizeMake(itemWidth, 160);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 10, 5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - helper Methods

-(void)header:(ZMHeader*)headerView presentSettings:(Settings*)settings
{
    settings.delegate = (id<settingsDelegate>)self;
    [self presentViewController:settings animated:YES completion:nil];
}

-(void)settings:(Settings*)settingsView displayFeedbackFor:(NSString*)selectionOption openedFrom:(NSString*)openOption
{
    //    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    self.feedback = nil;
    self.feedback = (FeedbackView *)[[[NSBundle mainBundle] loadNibNamed:@"FeedbackViewController"
                                                                       owner:self options:nil] objectAtIndex:0];
    self.feedback.assetId = @"999";
    // display feedback view on top of the view
    self.backgroundBlackView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.backgroundBlackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [self.feedback initWithParent:[UIApplication sharedApplication].keyWindow background:self.backgroundBlackView openedFrom:openOption];
}

-(void)header:(ZMHeader*)headerView didTapNotificationButton:(UIButton*)sender
{
    if (!self.notificationsPannel.parent)
    {
        self.notificationsPannel = nil;
        self.backgroundBlackView      = nil; // check whether we need to remove
        self.notificationsPannel = (ZMNotifications *)[[[NSBundle mainBundle] loadNibNamed:@"Notifications" owner:self options:nil] objectAtIndex:0];
        self.backgroundBlackView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.backgroundBlackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [self.notificationsPannel initWithParent:[UIApplication sharedApplication].keyWindow background:self.backgroundBlackView];
        self.parentHeader = headerView;
        self.notificationsPannel.tabBarTag = -1;
        self.notificationsPannel.delegate = (id<notificationDelegate>)self;
        //        self.notificationsPannel.events = self;
        [self.notificationsPannel populateNotifications:[[ZMAssetsLibrary defaultLibrary] getAllNotifications]];
        
        //Show arrow delayed
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self showHideArrow:NO];
        });
        
        [self.notificationsPannel adjustNotifications];
    }
}

- (void)showHideArrow:(BOOL) hide
{
    if (!hide)
    {
        self.parentHeader.arrow.hidden = NO;
        [self.parentHeader.messagesButton setImage:[UIImage GetMessageSelectIconImage]
                                                    forState:UIControlStateNormal];
        [self.parentHeader showHideBadge:YES];
    }
    else
    {
        self.parentHeader.arrow.hidden = YES;
        [self.parentHeader.messagesButton setImage:[UIImage GetMessageNormalIconImage]
                                                    forState:UIControlStateNormal];
        [self.parentHeader showHideBadge:NO];
    }
}

-(void)notification:(ZMNotifications*)notificationView didCloseLayout:(BOOL)closeLayout
{
    [self showHideArrow:closeLayout];
}

//Dismiss all notifications on button click
- (void)dismissAllNotifications:(NSArray *)notifications
{
    ZMLibrary* library = [self getLibraryRefrence];
    [library dismissAllNotifications:notifications];
}

/**
 *  Get notifications from server
 */
-(void) getNotifications
{
    ZMLibrary* library = [self getLibraryRefrence];
    [library getNotifications];
}

/**
 *  Changes the badge number, for both application icon and header badge
 *  @param badgeNumber
 */
-(void) changeBadgeNumber:(int)badgeNumber
{
    ZMLibrary* library = [self getLibraryRefrence];
    [library changeBadgeNumber:badgeNumber header:self.parentHeader];
}

//Get total notifications for badge count
- (void)geTotalOfNotifications
{
    int total = [[ZMAssetsLibrary defaultLibrary] getTotalUnreadNotifications];
    [[ZMAssetsLibrary defaultLibrary] setProperty:@"TOTAL_NOTIFICATIONS" value:[NSString stringWithFormat:@"%d", total]];
    [self changeBadgeNumber:total];
    
}

-(void)notification:(ZMNotifications*)notificationView actionForGetNotifications:(UIButton*)sender
{
    [self getNotifications];
}

-(void)notification:(ZMNotifications*)notificationView actionForGetTotalNotifications:(UIButton*)sender
{
    [self geTotalOfNotifications];
}

-(void)notification:(ZMNotifications*)notificationView actionForChangeBadgeNumber:(int)badgeNumber
{
    [self changeBadgeNumber:badgeNumber];
}

-(ZMLibrary*)getLibraryRefrence
{
    ProntoTabbarViewController *prontoTabVC = [[AppUtilities sharedUtilities] getTabBarController];
    return prontoTabVC.library;
}

-(void)header:(ZMHeader*)headerView didTapHelpButton:(UIButton*)sender
{
    UIStoryboard *storyboard        = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HelpViewController * controller = (HelpViewController*)[storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    controller.userFranchise = headerView.userFranchise.text;
//    controller.delegate = (id<HelpViewControllerDelegate>)self;
    [self.navigationController pushViewController:controller animated:NO];
}

- (void) reloadTableViewOrCollectionView
{
    [self updateTabBarView];

    //This condition handles the situation when there is no tab's item except
    // Events tab. There is only one tab and that is Event Tab.
    if ([self.globalSearchViewModel shouldHideTableOrCollectionView] == YES)
    {
        //Removing before adding to new one.
        [self.eventsGlobalSearch.view removeFromSuperview];
        self.eventsGlobalSearch  = nil;

        //Adding and removing will not happen
        self.eventsGlobalSearch = [EventsGlobalSearch GetEventsGSViewController];
        self.eventsGlobalSearch.delegate = (id<EventsGSDelegate>)self;
        self.eventsGlobalSearch.dataSourceFromGlobalSearchViewModal = [self.globalSearchViewModel getAssetArrayForSection:0];
        self.eventsGlobalSearch.view.frame = self.gsLibraryTableView.frame;
        [self.listGridContainerView addSubview:self.eventsGlobalSearch.view];

        self.gsLibraryColectionView.hidden = YES;
        self.gsLibraryTableView.hidden     = YES;
        
        [self.contentHeader shouldEnableRecentlyAddedAndGridSelectionControl:NO];

    }
    else
    {
        //When there are more than one item in tabs and selected tab is
        //Event tab. and that time we need to reload the event tab item not other.
        NSString *title = [self titleForTabAtIndex:self.selectedTab inTabBar:self.flatTabBarViewController];

        if([title containsString:keyMyEvents])
        {
            self.eventsGlobalSearch.dataSourceFromGlobalSearchViewModal = [self.globalSearchViewModel getAssetArrayForSection:self.selectedTab];
            [self.eventsGlobalSearch reloadTableView];
        }
        else
        {
            [self.eventsGlobalSearch.view removeFromSuperview];
            self.eventsGlobalSearch  = nil;

            self.gsLibraryColectionView.hidden = NO;
            self.gsLibraryTableView.hidden     = NO;
            
            [self.contentHeader shouldEnableRecentlyAddedAndGridSelectionControl:YES];

            NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
            if ([showingGridView isEqualToString:@"YES"])
            {
                self.gsLibraryTableView.hidden     = YES;
                
                [self.gsLibraryColectionView reloadData];
            }
            else
            {
                self.gsLibraryColectionView.hidden = YES;
                
                [self.gsLibraryTableView reloadData];
            }
        }
    }
}


//- (void) reloadTableViewOrCollectionView
//{
//    [self updateTabBarView];
//
//    //This condition handles the situation when there is no tab's item except
//    // Events tab. There is only one tab and that is Event Tab.
//    if ([self.globalSearchViewModel shouldHideTableOrCollectionView] == YES)
//    {
//        //Removing before adding to new one.
//        [self.eventsGlobalSearch.view removeFromSuperview];
//        self.eventsGlobalSearch  = nil;
//
//        //Adding and removing will not happen
//        self.eventsGlobalSearch = [EventsGlobalSearch GetEventsGSViewController];
//        self.eventsGlobalSearch.dataSourceFromGlobalSearchViewModal = [self.globalSearchViewModel getAssetArrayForSection:0];
//        self.eventsGlobalSearch.view.frame = self.gsLibraryTableView.frame;
//        [self.listGridContainerView addSubview:self.eventsGlobalSearch.view];
//
////        self.gsLibraryColectionView.hidden = YES;
////        self.gsLibraryTableView.hidden     = YES;
//
//    }
//    else
//    {
//        //When there are more than one item in tabs and selected tab is
//        //Event tab. and that time we need to reload the event tab item not other.
//        NSString *title = [self titleForTabAtIndex:self.selectedTab inTabBar:self.flatTabBarViewController];
//
//        if([title containsString:keyMyEvents])
//        {
//            [self.eventsGlobalSearch reloadTableView];
//        }
//        else
//        {
//            [self.eventsGlobalSearch.view removeFromSuperview];
//            self.eventsGlobalSearch  = nil;
//
//            if (self.gsLibraryTableView.hidden == NO) // Showing Table View
//            {
//                [self.gsLibraryTableView reloadData];
//            }
//            else // Showing Collection View
//            {
//                [self.gsLibraryColectionView reloadData];
//            }
//        }
//    }
//}



- (void)validateAndShowAsset:(Asset *)selectedAsset
{
    [AbbvieLogging logInfo:@"*****Intro validate Asset ********"];
    [self validateAssetBeforeOpen:selectedAsset completion:^(BOOL assetNeedsDownload)
     {
         if (assetNeedsDownload == YES)
         {
             NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:selectedAsset.assetID];
             for(OfflineDownload * data in offlineDownloadArray)
             {
                 if(data.assetID == selectedAsset.assetID)
                 {
                     if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"])
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             if(!self.isTapDownloadInitiated)
                             {
                                 [self downloadSelectedAsset:selectedAsset];
                                 [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                             }
                         });
                         self.isDownloadingAsset = NO;
                     }
                     else
                     {
                         if(!self.isTapDownloadInitiated)
                         {
                             [self downloadSelectedAsset:selectedAsset];
                             [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                         }
                     }
                 }
             }
             if(offlineDownloadArray.count == 0)
             {
                 if(!self.isTapDownloadInitiated)
                 {
                     [self downloadSelectedAsset:selectedAsset];
                     [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
                 }
             }
         }
         else
         {
             if(self.currentSelectedAssetToSeeDetail)
             {
                 //Return in case if open of any asset is in progress
                 return;
             }
             else
             {
                 self.currentSelectedAssetToSeeDetail = selectedAsset;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                     selectedAsset.isUpdateAvailable = NO;
                 });
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [AbbvieLogging logInfo:@"------Asset needs opened ------"];
                     [self openAsset:selectedAsset];
                 });
             }
         }
     } failure:^(NSString *messageFailure) {
         [self.mainDelegate showMessage:messageFailure whithType:ZMProntoMessagesTypeWarning];
     }];
}

-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    enum ZMProntoFileTypes assetType = [Constant  GetFileTypeForAsset:asset];
    
    if (!self.isDownloadingAsset)
    {
        self.isDownloadingAsset = YES;
        // check if asset is protected
        if (![UIApplication sharedApplication].protectedDataAvailable)
        {
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
            /// This should occur during application:didFinishLaunchingWithOptions:
            //[ASFFileProtectionManager stopProtectingLocation:asset.path];
            
            // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
            NSError *error;
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];
            
            if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
            {
                attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
                if (!success)
                    [AbbvieLogging logError:@"Set ~/Documents attrsAssetPath NOT successfull"];
            }
            
            /// ....
            NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
            [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
        }
        
        // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        if([[NSFileManager defaultManager] fileExistsAtPath:asset.path])
        {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"File Exists at %@", asset.path]];
            
        }
        
        if ((![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && assetType != ZMProntoFileTypesBrightcove) && (assetType != ZMProntoFileTypesWeblink))
        {
            if ([[ZMAbbvieAPI sharedAPIManager] isConnectionAvailable])
            {
                completion(YES);
            }
            else
            {
                self.isDownloadingAsset = NO;
                failure(@"The internet connection is not available, please check your network connectivity");
            }
        }
        else
        {
            //Check if the asset size matches the metada info meaning the asset is correct
            if (asset.file_size.intValue == (int)fileSize)
            {
                //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
                self.isDownloadingAsset = NO;
                completion(NO);
            }
            else
            {
                completion(YES);
            }
        }
    }
    else
    {
        if (![[ZMAbbvieAPI sharedAPIManager] isConnectionAvailable])
        {
            if(self.isDownloadingAsset == NO)
            {
                failure(@"The internet connection is not available, please check your network connectivity");
                self.isDownloadingAsset = NO;
            }
        }
        else
        {
            failure(@"Download in progress. Please wait and then try again.");
        }
    }
}

- (void)openAsset:(Asset *)selectedAsset
{
    if([selectedAsset.type isEqualToString:[EventOnlyAsset TypeAttributeValueAsString]])
    {
        EventOnlyAsset* eoaAsset = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:[selectedAsset.assetID stringValue]];
        [self eventOnlyAssetHandler:nil openAsset:eoaAsset];
        return;
    }
    
    //If Selected path or Selected path is not present then asset can't be opened.
    if (selectedAsset == nil || selectedAsset.path == nil) { return ;}
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
    self.isDownloadingAsset = NO;
    
    /*!
     * ABT-51
     */
    //enum ZMProntoFileTypes types = [self getAsset:selectedAsset];
    /*NSString *activityType = @"Pronto Object";
    if (assetType == ZMProntoFileTypesPDF) {
        activityType = @"PDF";
    }
    else if (assetType == ZMProntoFileTypesVideo) {
        activityType = @"VIDEO";
    }
    else if (assetType == ZMProntoFileTypesBrightcove) {
        activityType = @"VIDEO";
    }
    else if (assetType == ZMProntoFileTypesDocument) {
        activityType = @"DOCUMENT";
    }
    else if (assetType == ZMProntoFileTypesEpub) {
        activityType = @"EPUB";
    }
    else if(assetType == ZMProntoFileTypesWeblink) // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
    {
        activityType = @"WEBLINK";
    }*/
    
    //Tracking
//    [ASFActivityLog logActionForCurrentUser:@"viewed" withObjectName:[NSString stringWithFormat:@"%@",selectedAsset.title] ofType:[NSString stringWithFormat:@"%@",activityType] completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
//        if (!result){
//            //Ignore Errors
//        }
//    }];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController <ZMAssetViewControllerDelegate> *controller;
    controller.userFranchise = self.parentHeader.userFranchise.text;
    
    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
    if (assetType == ZMProntoFileTypesPDF) {
        if(document != nil)
        {
            [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
        }
        else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                self.currentSelectedAssetToSeeDetail = nil;
            });
        }
    }
    else if (assetType == ZMProntoFileTypesBrightcove)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
        controller.asset = selectedAsset;
        controller.userFranchise = self.parentHeader.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (assetType == ZMProntoFileTypesDocument)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
        controller.asset = selectedAsset;
        controller.userFranchise = self.parentHeader.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if(assetType == ZMProntoFileTypesWeblink)
    {
        // open the weblink URL
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
        controller.asset = selectedAsset;
        controller.userFranchise = self.parentHeader.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if(assetType == ZMProntoFileTypesAudio)
    {
        [self stopAudio];
        assetForAudio = selectedAsset;
        if(!audioPlayerView)
        {
            audioPlayerView = (AudioPlayer*)[[[NSBundle mainBundle] loadNibNamed:@"AudioPlayerView" owner:self options:nil] firstObject];
        }
        [audioPlayerView initialSetupWithAssetVal:selectedAsset];
        if (self.gsLibraryColectionView.hidden)
        {
            NSPredicate * predicateForList = [NSPredicate predicateWithFormat:@"uid == %@",assetForAudio.assetID];
            NSArray * cellsForList = [[self.gsLibraryTableView visibleCells]filteredArrayUsingPredicate:predicateForList];
            if (cellsForList.count > 0)
            {
                audioCurrentCell = cellsForList.firstObject;
                audioPlayerView.frame = CGRectMake(0, 0,audioCurrentCell.audioPlayerView.frame.size.width, audioCurrentCell.audioPlayerView.frame.size.height);
                [audioPlayerView.layer setMasksToBounds:YES];
//                [audioPlayerView initialSetupWithAssetVal:selectedAsset];
                audioCurrentCell.audioPlayerView.hidden = NO;
//                self.currentSelectedAssetToSeeDetail = nil;
                [audioCurrentCell.audioPlayerView addSubview:audioPlayerView];
            }
        }
        else
        {
            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@",assetForAudio.assetID];
            NSArray * cells = [[self.gsLibraryColectionView visibleCells]filteredArrayUsingPredicate:predicate];
            if (cells.count > 0) {
                audioCurrentGridCell = cells.firstObject;
                
                audioPlayerView.frame = CGRectMake(0, 0,audioCurrentGridCell.audioPlayerView.frame.size.width, audioCurrentGridCell.audioPlayerView.frame.size.height);
                [audioPlayerView.layer setMasksToBounds:YES];
//                [audioPlayerView initialSetupWithAssetVal:selectedAsset];
                audioCurrentGridCell.audioPlayerView.hidden = NO;
                
//                self.currentSelectedAssetToSeeDetail = nil;
                
                [audioCurrentGridCell.audioPlayerView addSubview:audioPlayerView];
            }
        }
        self.currentSelectedAssetToSeeDetail = nil;
    }

    else
    {
        // Fix - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** -[NSURL initFileURLWithPath:]: nil string parameter'
        if (assetType == ZMProntoFileTypesPDF)
        {
            if(selectedAsset.path)
            {
                MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
                if(document != nil)
                {
                    [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
                }
                else
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                        self.currentSelectedAssetToSeeDetail = nil;
                    });
                }
            }
        }
    }
}

- (void)stopAudio
{
    if(assetForAudio != nil)
    {
        [audioPlayerView stopAudio];
        
        if(audioCurrentCell)
        {
            audioCurrentCell.audioPlayerView.hidden = YES;
            audioCurrentCell = nil;
        }
        else if(audioCurrentGridCell)
        {
            audioCurrentGridCell.audioPlayerView.hidden = YES;
            audioCurrentGridCell = nil;
        }
        [audioPlayerView removeFromSuperview];
        assetForAudio = nil;
    }
}

- (void)openFastPDFWithAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
    controller.asset = asset;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    //    [view addSubview:controller.view];
    
    [viewController.view addSubview:view];
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    //    controller.view.frame = view.bounds;
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
    
    //    [controller didMoveToParentViewController:viewController];
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor GetColorFromHexValueE0E6ED];
    [viewController.view addSubview:webview];
    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    [self hideMessage];
    controller.userFranchise = self.parentHeader.userFranchise.text;
    controller.viewMain = view;
    controller.webviewScreen = webview;
    controller.webviewScreen.hidden = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (void)hideMessage
{
    [self.mainDelegate hideMessage];
}

- (void) downloadSelectedAsset:(Asset *)selected
{
    self.isTapDownloadInitiated = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    });
    [AbbvieLogging logInfo:@"------Asset needs download ------"];
    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    if(![selected.file_mime isEqualToString:@"weblink"])
    {
        [self downloadAsset:selected];
    }
}

- (void)updateOfflineDownloadCoreData:(Asset *)asset status:(NSString*)status
{
    if([asset valueForKey:@"assetID"] != nil)
    {
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
        [initialDownloadAsset setObject:status forKey:@"status"];
        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
        
//        if (offlineData.count > 0) {
//            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0]];
//        }
//        [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset];
        
        if (offlineData.count > 0)
        {
            //if  insertation and deletetion has to be happen, it will happen one by one
            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
            }];
        }
        else
        {
            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
        }
    }
}

- (void)downloadAsset:(Asset *)selectedAsset{
    
    self.currentAssetDownload = selectedAsset;
    self.isDownloadingAsset = YES;
    [self setProgress:0.001000];
    
    [[ZMAbbvieAPI sharedAPIManager] downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        [self downloadAssetDidFinish:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"completed"];
        // New revamp changes - remove the update label once the asset is downloaded successfully.
        selectedAsset.isUpdateAvailable = NO;
        if(![selectedAsset.file_mime isEqualToString:@"audio"])
        {
            [self reloadTableViewOrCollectionView];
        }
    } onError:^(NSError *error) {
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAssetOnFailure:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"failed"];
    }];
}

// Download asset for print
- (void)downloadOneAsset:(Asset *)selectedAsset completion:(void (^)(BOOL assetDownloaded))completion
                 failure:(void (^)(NSString *assetDownloadFail))failure{
    
    self.currentAssetDownload = selectedAsset;
    self.isDownloadingAsset = YES;
    [self setProgress:0.001000];
    
    [self.apiManager downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
        [self setProgress:progress];
    } completion:^{
        //[self downloadAssetDidFinish:selectedAsset];
//        [self.mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", selectedAsset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
        self.isDownloadingAsset = NO;
        completion(YES);
    } onError:^(NSError *error) {
        [self setProgress:2];
        [self downloadAssetDidNotFinish:selectedAsset error:error];
        self.isDownloadingAsset = NO;
        completion(NO);
    }];
}

- (void)setProgress:(float)progress
{
    if (self.gsLibraryTableView.isHidden == YES) // Collection view is visible
    {
        //Progress for Grid Item
        ZMGridItem *currentDownloadItem;
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@",self.currentAssetDownload.assetID];
        NSArray * cells = [[self.gsLibraryColectionView visibleCells]filteredArrayUsingPredicate:predicate];
        if (cells.count > 0)
        {
            currentDownloadItem = cells.firstObject;
            currentDownloadItem.progress = progress;
            currentDownloadItem.loaderBackgroundView.hidden = NO;
            [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
            [currentDownloadItem.loaderBackgroundView setAlpha:0.5];
        }
        
        if (progress >= 1)
        {
            //        currentAssetDownload = nil;
            [currentDownloadItem.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
            [currentDownloadItem.loaderBackgroundView setAlpha:1];
            currentDownloadItem.loaderBackgroundView.hidden = YES;
        }
    }
    else //Table view is visible
    {
        //Progress for list item
        FranchiseTableViewCell *currentCell;
        
        NSPredicate * predicateForList = [NSPredicate predicateWithFormat:@"uid == %@",self.currentAssetDownload.assetID];
        NSArray * cellsForList = [[self.gsLibraryTableView visibleCells]filteredArrayUsingPredicate:predicateForList];
        if (cellsForList.count > 0)
        {
            currentCell = cellsForList.firstObject;
            currentCell.progress = progress;
            
            currentCell.loaderBackgroundView.hidden = NO;
            currentCell.loaderView.hidden = NO;
            
            [currentCell.loaderBackgroundView setBackgroundColor:[UIColor blackColor]];
            [currentCell.loaderBackgroundView setAlpha:0.5];
        }
        if (progress >= 1)
        {
            self.currentAssetDownload = nil;
            currentCell.loaderBackgroundView.bounds = currentCell.bounds;
            currentCell.loaderView.center = currentCell.center;
            
            [currentCell.loaderBackgroundView setBackgroundColor:[UIColor clearColor]];
            [currentCell.loaderBackgroundView setAlpha:1];
            
            currentCell.loaderBackgroundView.hidden = YES;
            currentCell.loaderView.hidden = YES;
        }
    }
}

- (void)markLiked:(Asset *)selectedAsset type:(NSInteger)type{
    
    //If selectedAsset == nil, There is nothing to track
    if (selectedAsset == nil || selectedAsset.assetID == nil) { return; }
    
    __block NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    NSString *currentBucketName = [self.globalSearchViewModel getCurrentActiveTabOptionAsStringForSelectedIndex:self.selectedTab];
    NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:selectedAsset.assetID];
    
    [[[ZMUserActions sharedInstance] resultantAssets] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        Asset *asset = obj;
        if ([asset isKindOfClass:[Asset class]]) {
            if (asset.assetID.longLongValue == selectedAsset.assetID.longValue){
                
//                [self.assetsPage replaceObjectAtIndex:idx withObject:selectedAsset];
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:0];
                [indexPaths addObject:indexPath];
                
            }
        }
    }];
    NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
    NSMutableArray *favArray = [[NSMutableArray alloc]init];
    NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
    
    if (favoritefranchise.count == 0 && type == ZMEventLike) {
        [favArray addObject: selectedAsset.assetID];
        [favFranchiseObject setObject: selectedAsset.assetID forKey:@"asset_ID"];
        [favFranchiseObject setObject:[self.globalSearchViewModel getCurrentActiveTabOptionAsStringForSelectedIndex:self.selectedTab] forKey:@"bucketName"];
//        [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];
        
        [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];

        
        if (favfranchise.count > 0)
        {
            FavFranchise *favFranchise = favfranchise[0];
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
            favFranchise.bucketName = modifiedFranchiseBucketName;
            [[ZMUserActions sharedInstance] updatePersistentStoreWithCurrentContext];
        }
        else
        {
            [[ZMUserActions sharedInstance] insertNewFavouriteFranchise:favFranchiseObject complete:^() {}
                                                                  error:^(NSError *error) {
                                                                  }];
        }
        
    } else {
        int count = 0;
        for(FavFranchise *fav in favoritefranchise) {
            if([fav.asset_ID longValue] !=  selectedAsset.assetID.longValue && type == ZMEventLike) {
                count += 1;
            }
            else if ([fav.asset_ID longValue] ==  selectedAsset.assetID.longValue) {
                break;
            }
        }
        if(count == favoritefranchise.count) {
            [favArray addObject: selectedAsset.assetID];
            [favFranchiseObject setObject:  selectedAsset.assetID forKey:@"asset_ID"];
            [favFranchiseObject setObject:[self.globalSearchViewModel getCurrentActiveTabOptionAsStringForSelectedIndex:self.selectedTab] forKey:@"bucketName"];
//            [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];
            [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]
                                   forKey:@"bucketID"];
            
            if (favfranchise.count > 0)
            {
                FavFranchise *favFranchise = favfranchise[0];
                NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
                favFranchise.bucketName = modifiedFranchiseBucketName;
                [[ZMUserActions sharedInstance] updatePersistentStoreWithCurrentContext];
            }
            else
            {
                [[ZMUserActions sharedInstance] insertNewFavouriteFranchise:favFranchiseObject complete:^() {}
                                                                      error:^(NSError *error) {
                                                                      }];
            }
        }
    }
    ZMProntoManager.sharedInstance.favArrayIds = favArray;
    
    for(FavFranchise *fav in favoritefranchise) {
        if([fav.asset_ID longValue] == selectedAsset.assetID.longValue && type == ZMEventUnlike) {
            NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById: selectedAsset.assetID];
            if (favfranchise.count > 0) {
                
                FavFranchise *favFranchise = favfranchise.firstObject ;
                NSString *franchiseBucketName = favFranchise.bucketName;
                NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameFromBucket:franchiseBucketName byRemovingBucketName:currentBucketName];
                if (modifiedFranchiseBucketName == nil)
                {
                    [[ZMUserActions sharedInstance] deleteFavFranchise:favfranchise.firstObject complete:^{
                    } error:^(NSError *error) {
                    }];
                }
                else
                {
                    favFranchise.bucketName = modifiedFranchiseBucketName;
                    [[ZMUserActions sharedInstance] updatePersistentStoreWithCurrentContext];
                }

                // update the count
                if([ZMUserActions sharedInstance].assetsCount >= 1 && ([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kFranchiseFavorites]))
                {
                    [ZMUserActions sharedInstance].assetsCount = [ZMUserActions sharedInstance].assetsCount-1;
                }
            } else {
                [AbbvieLogging logInfo:@"favfranchise array is empty"];
            }
            
        }
    }
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"fav franchise:%@",[[ZMCDManager sharedInstance] getFavFranchise]]];
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if ([showingGridView isEqualToString:@"YES"]) {
        [_gsLibraryColectionView reloadData];
    } else {
        [_gsLibraryTableView reloadData];
    }
}

#pragma mark - ZMAssetsLibrary Observer methods for asset downloading

- (void)downloadAssetDidFinish:(Asset *)asset {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainDelegate hideMessage];
    });
    
    self.isDownloadingAsset = NO;
    self.isTapDownloadInitiated = NO;
    
    [self.mainDelegate showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", asset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self validateAndShowAsset:asset];
    });
}

- (void)downloadAssetDidNotFinish:(Asset *)asset error:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.mainDelegate.currentMessage)
        {
            [self.mainDelegate hideMessage];
        }
        
        self.isDownloadingAsset = NO;
        self.isTapDownloadInitiated = NO;
        
        NSString *message;
        enum ZMProntoMessages type = 1;
        // NSInteger errCode = ABS(error.code);
        if (error.code == 503)
        {
            type = ZMProntoMessagesTypeWarning;
            message = @"The server is currently under maintenance mode.";
        }
        
        else if (![[ZMAbbvieAPI sharedAPIManager] isConnectionAvailable])
        {
            type = ZMProntoMessagesTypeWarning;
            message = @"The Internet connection is not available, please check your network connectivity.";
        }
        else if(error.code == 401)
        {
            //Restoring the user session
            [self.mainDelegate initPronto];
            return;
        }
        else if (error.code == 404)
        {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"Asset not found %@", asset.title];
        }
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        else if (error.code == 5303 || error.code == -5303)
        {
            message = @"Error while retreiving assets, please try again later";
        }
        else if (error.code == 1009 || error.code == -1009)
        {
            message = @"Please check the network connection";
        }
        else if (error.code == 1003 || error.code == -1003)
        {
            message = @"A server with the specified hostname could not be found";
        }
        else
        {
            type = ZMProntoMessagesTypeAlert;
            message = [NSString stringWithFormat:@"There is a problem downloading %@", asset.title];
        }
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mainDelegate showMessage:message whithType:type withAction:^{
                [self.mainDelegate hideMessage];
                [self validateAndShowAsset:asset];
            }];
        });
        
        NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
        // PRONTO-28 Fixing Frequent Error Codes Appearing in Pronto 3.9
        if ([message containsString:@"ERROR_CODE"])
        {
            // errorcode consists of digits
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",@"Please check the network connection"] forKey:@"scerrorcode"];
        }
        else
        {
            [trackingOptions setObject: [NSString stringWithFormat:@"PAGEERROR|%@",message] forKey:@"scerrorcode"];
        }
        
        ZMGridItem *currentDownloadItem;
        for (ZMGridItem *cell in [self.gsLibraryColectionView visibleCells])
        {
            if(self.currentAssetDownload.assetID.longValue == cell.uid)
            {
                currentDownloadItem = cell;
                break;
            }
        }
        currentDownloadItem.progress = 1;
        self.currentAssetDownload = nil;
        
        /**
         * @Tracking
         * Download Did Not finished
         **/
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section withSubsection:@"assetview" withName:nil withOptions:trackingOptions];
    });
}

- (void) openExportMenu:(id)cell sender:(UIButton*) sender
{
    [AbbvieLogging logInfo:@"Export button clicked on cell"];
    self.popOverPresentingCell = cell;
    CGRect frame = sender.frame;
    CGRect rect = CGRectZero;
    NSNumber * assetId = 0;
    selectedItem = sender.tag;
    rect =  [cell convertRect:self.gsLibraryTableView.frame fromView: self.view];
    frame.origin.x = 68;
    if ([cell isKindOfClass:[FranchiseTableViewCell class]])
    {
        self.currentlySelectedAsset = ((FranchiseTableViewCell*)cell).currentAsset;
    }
    else
    {
        self.currentlySelectedAsset = ((ZMGridItem*)cell).currentAsset;
    }
    
    self.exportMenu = (ExportMenu *)[[[NSBundle mainBundle] loadNibNamed:@"ExportMenu" owner:self options:nil] objectAtIndex:0];
    self.exportMenu.exportMenuDelegate = (id<ExportMenuDelegate>)self;
    self.exportMenu.isPresentedFromGlobalSearch = YES;
    //    self.library.exportMenuIsPresented = YES; // As this value is being checked to dismiss the view.
    self.exportMenu.globalSearch = self;
    
    // values in Export and Info menu based on the cell selection
    if ([cell isKindOfClass:[FranchiseTableViewCell class]])
        assetId = [NSNumber numberWithLong:((FranchiseTableViewCell*)cell).uid];
    else
        assetId = [NSNumber numberWithLong:((ZMGridItem*)cell).uid];
    
    NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
    if (assetArray.count > 0)
    {
        self.currentlySelectedAsset = assetArray[0];
    }
    [self.exportMenu selectingAssetWithDetails:self.currentlySelectedAsset withSender:sender];
    NSMutableArray * exportMutableArray =[[NSMutableArray alloc]init];
    // check and display Print Option
    if(self.currentlySelectedAsset.field_print.length > 0 && [self.currentlySelectedAsset.field_print isEqualToString:@"1"])
    {
        [exportMutableArray addObject:@"Print"];
    }
    [exportMutableArray addObject:@"Share"];
    // check and display Email to self Option
    if(self.currentlySelectedAsset.field_email_to_self.length > 0 && [self.currentlySelectedAsset.field_email_to_self isEqualToString:@"1"])
    {
        [exportMutableArray addObject:@"Email to self"];
    }
    [exportMutableArray addObject:@"Feedback"];
    
    self.exportMenu.exportArray = exportMutableArray;
    self.exportMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.exportMenu.preferredContentSize = CGSizeMake(178, [exportMutableArray count]*44);
    if (fabs(rect.origin.x) < 300 ) {
        self.exportMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomPopoverBackgroundView class];
    } else {
        frame.origin.x = 0;
        self.exportMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomInfoPopoverBackgroundView class];
    }
    [self presentViewController:self.exportMenu animated:YES completion:nil];
    
    // configure the Popover presentation controller
    _exportMenuPopOver = [self.exportMenu popoverPresentationController];
    _exportMenuPopOver.delegate = self;
    _exportMenuPopOver.sourceView = [sender superview];
    _exportMenuPopOver.sourceRect = frame;
    
    CGFloat deviceHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat value = (fabs(rect.origin.y) + ((exportMutableArray.count) * 44) );
    value =  value + 67+8+42 +120; //conternt header height, top, margin
    if (deviceHeight > value)
    {
        _exportMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    else
    {
        _exportMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionDown;
    }
    
    int point = 500;
    int screenWidth = [UIScreen mainScreen].bounds.size.width;
    if(screenWidth == iPadPro10_width)
    {
        point = 575;
    }
    else if (screenWidth == iPadPro12_width)
    {
        point = 775;
    }
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown)
    {
        if(deviceHeight < value)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = value - deviceHeight;
        }
        if(fabs(rect.origin.y) > point)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 1;
        }
    }
    
    _exportMenuIsPresented = YES;
}

- (void) openImportMenu:(id)cell sender:(UIButton*) sender
{
    [AbbvieLogging logInfo:@"Info button clicked on cell"];
    self.popOverPresentingCell = cell;
    CGRect frame = sender.frame;
    CGRect rect = CGRectZero;
    selectedItem = sender.tag;
    rect =  [cell convertRect:self.gsLibraryTableView.frame fromView: self.view];
    frame.origin.x = 68;
    if ([cell isKindOfClass:[FranchiseTableViewCell class]])
    {
        self.currentlySelectedAsset = ((FranchiseTableViewCell*)cell).currentAsset;
        
    }
    else
    {
        self.currentlySelectedAsset = ((ZMGridItem*)cell).currentAsset;
    }
    
    if (!self.infoMenu)
    {
        self.infoMenu = (InfoMenu *)[[[NSBundle mainBundle] loadNibNamed:@"InfoMenu" owner:self options:nil]
                                     objectAtIndex:0];

        NSNumber * assetId = 0;
        // values in Export and Info menu based on the cell selection
        if ([cell isKindOfClass:[FranchiseTableViewCell class]])
            assetId = [NSNumber numberWithLong:((FranchiseTableViewCell*)cell).uid];
        else
            assetId = [NSNumber numberWithLong:((ZMGridItem*)cell).uid];
        
        // selecting menu from cell
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
        if (assetArray.count > 0) {
            self.currentlySelectedAsset = assetArray[0];
        }
        
        // expiredDate
        NSNumber* expiredOn = self.currentlySelectedAsset.unpublish_on;
        NSTimeInterval timeInterval = [expiredOn doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd/MM/yyyy"];
        NSString* dateString = [df stringFromDate:date];
        
        // lastUpdated Date
        NSNumber* lastUpdated = self.currentlySelectedAsset.changed;
        NSTimeInterval lastTimeInterval = [lastUpdated doubleValue];
        NSDate *lastDate = [NSDate dateWithTimeIntervalSince1970:lastTimeInterval];
        NSString* lastDateString = [df stringFromDate:lastDate];
        
        if (self.gsLibraryTableView.isHidden == NO && self.gsLibraryColectionView.isHidden == YES)
        {
            // Tableview is visible
            self.infoMenu.totalViewsCount = [NSString stringWithFormat:@"%d",self.currentlySelectedAsset.view_count.intValue];
        }
        else
        {
            // Grid View is visible
            self.infoMenu.totalViewsCount = [NSString stringWithFormat:@"%d",((ZMGridItem*)cell).totalViews];
        }
        self.infoMenu.expiredDate = dateString;
        self.infoMenu.lastUpdatedDate = lastDateString;
        self.infoMenu.medRegNo = self.currentlySelectedAsset.medRegNo;
    }
    
    NSMutableArray * infoMutableArray =[[NSMutableArray alloc]init];
    [infoMutableArray addObject:@"Number Of Views"];
    [infoMutableArray addObject:@"Med/Reg Number"];
    [infoMutableArray addObject:@"Last Updated"];
    [infoMutableArray addObject:@"Expiration Date"];
    self.infoMenu.infoArray = infoMutableArray;
    
    self.infoMenu.modalPresentationStyle                   = UIModalPresentationPopover;
    self.infoMenu.popoverPresentationController.sourceRect = [sender frame];
    self.infoMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomInfoPopoverBackgroundView class];
    self.infoMenu.preferredContentSize = CGSizeMake(158, [infoMutableArray count]*44);
    [self presentViewController:self.infoMenu animated:YES completion:nil];
    
    // configure the Popover presentation controller
    self.infoMenuPopOver = [self.infoMenu popoverPresentationController];
    CGFloat deviceHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat value = (fabs(rect.origin.y) + ((infoMutableArray.count) * 44));
    value =  value + 67+8+42 +120; //conternt header height, top, margin
    if (deviceHeight > value)
    {
        self.infoMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    else
    {
        self.infoMenuPopOver.permittedArrowDirections = UIPopoverArrowDirectionDown;
    }
    self.infoMenuPopOver.delegate = self;
    self.infoMenuPopOver.sourceView = [sender superview];
    self.infoMenuPopOver.sourceRect = sender.frame;
    
    int point = 500;
    int screenWidth = [UIScreen mainScreen].bounds.size.width;
    if(screenWidth == iPadPro10_width)
    {
        point = 575;
    }
    else if (screenWidth == iPadPro12_width)
    {
        point = 775;
    }
    
    if ([[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication]statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown)
    {
        if(deviceHeight < value)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = value - deviceHeight;
        }
        if(fabs(rect.origin.y) > point)
        {
            [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 1;
        }
    }
    
    _infoMenuIsPresented = YES;
}

- (void) favTappedAction:(id)cell sender:(UIButton*) sender
{
    [AbbvieLogging logInfo:@"favIconButtonAction -> Implement it properly"];
    FranchiseTableViewCell *listCell = nil;
    ZMGridItem *tempCell = nil;
    NSNumber * assetId = 0;
    
    // grid
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if ([showingGridView isEqualToString:@"YES"]) {
        tempCell = cell;
        assetId = [NSNumber numberWithLong:tempCell.uid];
    }
    
    // list
    else
    {
        listCell = cell;
        assetId = [NSNumber numberWithLong:listCell.uid];
    }
    
    NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId.stringValue];
    if (assetArray.count > 0) {
        self.currentSelectedAssetForFav = assetArray[0];
    }
    //Revamp(1/12) - Adding into core data
    favIconTapped = YES;
    NSMutableDictionary * favFranchiseObject = [[NSMutableDictionary alloc]init];
    [favFranchiseObject setObject:assetId forKey:@"asset_ID"];
    [favFranchiseObject setObject:[self.globalSearchViewModel getCurrentActiveTabOptionAsStringForSelectedIndex:self.selectedTab] forKey:@"bucketName"];
//    [favFranchiseObject setObject:[Constant GetCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];
        [favFranchiseObject setObject:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString] forKey:@"bucketID"];

    
    //End of revamp
    //Revamp(26/12) - Handling heart bounce animation
    NSString *currentBucketName = [self.globalSearchViewModel getCurrentActiveTabOptionAsStringForSelectedIndex:self.selectedTab];
//    bool justAdded = false;
    
    // Revamp changes - for like function service call
    ZMEvent *ratingEvent = [[ZMEvent alloc] init];
    ratingEvent.assetId = self.currentSelectedAssetForFav.assetID.longValue;
    ratingEvent.eventType = ZMEventLike;
    NSString *rating = @"liked";
    NSInteger rate = 100;
    NSNumber *k = [NSNumber numberWithInt:1];
    NSNumber *totalVotes = [NSNumber numberWithInt:([k intValue] + [self.currentSelectedAssetForFav.votes intValue])];
    
    
    if ([sender isSelected])
    {
        [sender setImage:[UIImage imageNamed:@"favIcon"] forState:UIControlStateNormal];
        [sender setSelected:NO];
        
        NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:assetId];
        if (favfranchise.count > 0)
        {
            //check wheather it belongs to other tab as well,
            //if yes, then remove only bucket name and update to database
            // if no, then directly remove it
            FavFranchise *favFranchise = favfranchise[0];
            NSString *franchiseBucketName = favFranchise.bucketName;
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameFromBucket:franchiseBucketName byRemovingBucketName:currentBucketName];
            if (modifiedFranchiseBucketName == nil)
            {
                // It means that It belongs from one bucket and therefore delete it
                [_actions deleteFavFranchise:favfranchise[0] complete:^{
                    
                } error:^(NSError *error) {
                }];
            }
            else
            {
                // It means that it belongs to other bucket as well and therefore
                //Remove it for current and keep it for others
                favFranchise.bucketName = modifiedFranchiseBucketName;
                [_actions updatePersistentStoreWithCurrentContext];
                
                [favFranchiseObject setObject:modifiedFranchiseBucketName forKey:@"bucketName"];
            }
            
            // update the count
            if(_actions.assetsCount >= 1 && [[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kFranchiseFavorites])
            {
                _actions.assetsCount = _actions.assetsCount-1;
                [self.contentHeader updateFranchiseLabelCount:_actions.assetsCount currentTab:ProntoTabOptionToolsAndTraining];
            }
        }
        else
        {
            [AbbvieLogging logInfo:@"favfranchise array is empty"];
        }
        
        // New Revamping changes - fav assets disappear on deletion
        NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
        NSMutableArray *favArray = [[NSMutableArray alloc]init];
        for(FavFranchise *fav in favoritefranchise)
        {
            BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:fav.bucketName];
            if (isPresent  && fav.asset_ID != nil)
            {
                [favArray addObject:fav.asset_ID];
            }
        }
        
        // display the asset count on selecting Promoted asset as Fav asset
        if([[ZMUserActions sharedInstance].selectLeftMenu isEqualToString:kFranchiseFavorites])
        {
            [self.contentHeader updateFranchiseLabelCount:(int)favArray.count currentTab:ProntoTabOptionToolsAndTraining];
        }
        
        // check the empty array
        if(favArray.count > 0)
        {
            ZMProntoManager.sharedInstance.favArrayIds = favArray;
        }
        else
        {
            ZMProntoManager.sharedInstance.favArrayIds = @[];
        }
        
        //Revamp(26/12) - Handling heart bounce animation
        // Revamp changes - Like Action during unselect
        if(self.currentSelectedAssetForFav.votes.intValue > 0)
        {
            self.currentSelectedAssetForFav.votes = [NSNumber numberWithInt:([self.currentSelectedAssetForFav.votes intValue]-[k intValue])];
        }
        else
        {
            self.currentSelectedAssetForFav.votes = [NSNumber numberWithInt:0];
        }
        self.currentSelectedAssetForFav.user_vote = @"0";
        ratingEvent.eventType = ZMEventUnlike;
        rating = @"disliked";
        rate = 0;
        //        sender.tag = 100;
    }
    else
    {
        /// two condition, if asset is available for favorite, it means it is favorite for other bucket
        // if not then, it is not favourite already, it should be added directly
        NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:assetId];
        if (favfranchise.count > 0)
        {
            FavFranchise *favFranchise = favfranchise[0];
            NSString *modifiedFranchiseBucketName = [Constant getModifiedBucketNameWithBucket:favFranchise.bucketName byAddingBucketName:currentBucketName];
            favFranchise.bucketName = modifiedFranchiseBucketName;
            [self.actions updatePersistentStoreWithCurrentContext];
        }
        else
        {
            [self.actions insertNewFavouriteFranchise:favFranchiseObject complete:^() {
                NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
                NSMutableArray *favArray = [[NSMutableArray alloc]init];
                for(FavFranchise *fav in favoritefranchise)
                {
                    BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:fav.bucketName];
                    if (isPresent  && fav.asset_ID != nil)
                    {
                        [favArray addObject:fav.asset_ID];
                    }
                }
                
                if(favArray.count > 0)
                {
                    ZMProntoManager.sharedInstance.favArrayIds = favArray;
                }
                else
                {
                    ZMProntoManager.sharedInstance.favArrayIds = @[];
                }
                                
                double delayInSeconds = 0.6;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    //[self updateFavGridList];
                });
            }
                                            error:^(NSError *error)
             {
             }];
        }
        
        //End of revamp
        [sender setImage:[UIImage imageNamed:@"favIconSelect"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        //Revamp(26/12) - Handling heart bounce animation
//        justAdded = true;
        
        // Revamp changes - Like Action during selection
        if(self.currentSelectedAssetForFav.votes.intValue >0)
        {
            self.currentSelectedAssetForFav.votes = totalVotes;
        }
        else
        {
            self.currentSelectedAssetForFav.votes = [NSNumber numberWithInt:0];
        }
        
        self.currentSelectedAssetForFav.user_vote = @"100";
        
    }
    
    // Revamp changes - like action - after the selection
    ratingEvent.eventValue = [NSString stringWithFormat:@"%ld", (long)rate];
    
    //Event created from bucket id
    ratingEvent.createdFromBucketId = [self.globalSearchViewModel getBucketIdForSelectedTab:self.selectedTab];
    
    //Saves the rating to the local DB
    [[ZMAssetsLibrary defaultLibrary] saveEvent:ratingEvent];
    
    [[ZMCDManager sharedInstance] updateAsset:_currentSelectedAssetForFav success:^{
        //Send the events to the server
        [self updateAssetAndSendEventToServer];
        
        //Tracking
        [ZMTracking trackSection:[ZMUserActions sharedInstance].section
                  withSubsection:@"assetview" withName:[NSString stringWithFormat:@"%@-rateit",[ZMAssetViewController getAssetType:_currentSelectedAssetForFav]]
                     withOptions:[[NSMutableDictionary alloc] initWithDictionary:@{@"sctierlevel":@"tier3", @"scrating":rating}]];
        
    } error:^(NSError * error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on ZMHeader %@",error.localizedDescription]];
    }];
    
    // bouncing animation of fav button
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.duration = .2;
    animation.repeatCount = 1;
    if ([showingGridView isEqualToString:@"YES"])
    {
        animation.fromValue = [NSValue valueWithCGPoint:tempCell.favButton.center];
        animation.toValue = [NSValue valueWithCGPoint:CGPointMake(tempCell.favButton.center.x, tempCell.favButton.center.y-10)];
        
    }
    else
    {
        animation.fromValue = [NSValue valueWithCGPoint:listCell.favButton.center];
        animation.toValue = [NSValue valueWithCGPoint:CGPointMake(listCell.favButton.center.x, listCell.favButton.center.y-10)];
    }
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    
    //Handling heart bounce animation
    if ([showingGridView isEqualToString:@"YES"])
    {
        [tempCell.favButton.layer addAnimation:animation forKey:@"position"];
    }
    else
    {
        [listCell.favButton.layer addAnimation:animation forKey:@"position"];
    }
    
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));

    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self reloadTableViewOrCollectionView];
    });
}

- (void) updateAssetAndSendEventToServer {
    
    [[ZMCDManager sharedInstance] updateAsset: _currentSelectedAssetForFav success:^{
        //Send the events to the server
        dispatch_async(dispatch_queue_create("Sending Events from ToolsTraining", NULL), ^{
            [[ZMAbbvieAPI sharedAPIManager] uploadEvents:^{
                [AbbvieLogging logInfo:@">>> Events Synced"];
            } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with uploadEvents: %@", error]];
            }];
        });
        
    } error:^(NSError * error) {
        [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on ToolsTraining %@",error.localizedDescription]];
    }];
    
}


#pragma mark - ZMContentHeader Delegate Method

- (void)conentHeader:(ZMContentHeader*)contentHeader didSelectGridButton:(LibraryViewType)viewType
{
    [AbbvieLogging logInfo:@"didSelectGridButton"];
    if (self.emptyView != nil && self.emptyView.isHidden == NO)
    {
        return;
    }
    
    if (viewType == LibraryViewTypeList)
    {
        self.gsLibraryTableView.hidden = NO;
        self.gsLibraryColectionView.hidden = YES;
    }
    else //LibraryViewTypeGrid
    {
        self.gsLibraryTableView.hidden = YES;
        self.gsLibraryColectionView.hidden = NO;
    }
    [self reloadTableViewOrCollectionView];
}

- (void)conentHeader:(ZMContentHeader*)contentHeader didSelectRecentlyButton:(UIButton*)sender
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"buttonname:%@",sender.titleLabel.text]];
    [[ZMProntoManager sharedInstance].search setTitle:self.parentHeader.searchTextbox.text];
    self.ttSortMenuButton = sender;

    if (self.ttSortMenu == nil)
    {
        self.ttSortMenu = (ZMSortMenu *)[[[NSBundle mainBundle] loadNibNamed:@"SortMenu" owner:self options:nil] objectAtIndex:0];
        self.ttSortMenu.library = self.parentHeader.library; //Verify this change
        self.ttSortMenu.parent  = self.parentHeader;
        self.ttSortMenu.sortArray = [ZMUserActions sharedInstance].sorts;
    }

    // 'UIPopoverController' is deprecated so using Popover presentation controller
    self.ttSortMenu.modalPresentationStyle  = UIModalPresentationPopover;
    self.ttSortMenu.popoverPresentationController.sourceRect = sender.frame;
    //self.sortMenu.popoverPresentationController.popoverBackgroundViewClass = [ZMCustomPopoverBackgroundView class];
    //present it from here
    [self presentViewController:self.ttSortMenu animated:YES completion:nil];

    // configure the Popover presentation controller
    self.ttSortMenuPopOver = [self.ttSortMenu popoverPresentationController];
    self.ttSortMenuPopOver.permittedArrowDirections = 0;
    self.ttSortMenuPopOver.delegate = self;
    self.ttSortMenuPopOver.sourceView = sender.superview;
    
    self.ttSortMenu.preferredContentSize = CGSizeMake(self.ttSortMenuButton.frame.size.width - 2, [self.ttSortMenu.sortArray count]*44);
    self.ttSortMenuPopOver.sourceRect = CGRectMake(self.ttSortMenuButton.frame.origin.x + (self.ttSortMenuButton.frame.size.width/2) ,self.ttSortMenuButton.frame.origin.y+104,1, 1);

    [self.ttSortMenu.sortMenuTableView reloadData];
}

- (void)conentHeader:(ZMContentHeader*)contentHeader didTapBackButtonSender:(UIButton*)sender
{
    [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingSearchResults];
    isScreenVisible = NO;
    [self dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
}

-(void)header:(ZMHeader*)headerView isDismissingSearchPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    [self dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
}

- (void)dismissOverlayViewFromSuperViewAndDoSetupWhichRequired
{
    if (self.gsVCDelegate != nil &&
        [self.gsVCDelegate respondsToSelector:@selector(enableAssetLibraryObserver)])
    {
        [self.gsVCDelegate enableAssetLibraryObserver];
    }

    [self.parentHeader resignFromFirstResponder];
    
    UIColor *color = [theme whiteFontColor];
    self.parentHeader.searchTextbox.backgroundColor = [UIColor clearColor];
    self.parentHeader.searchTextbox.font = [theme normalLato];
    self.parentHeader.searchTextbox.enabled = YES;
    self.parentHeader.searchTextbox.text = @"";
    self.parentHeader.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color}];
    self.parentHeader.searchTextbox.textColor = [UIColor whiteColor];
    [ZMUserActions sharedInstance].enableSearch = YES;
    [self.parentHeader.lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
    self.parentHeader.searchTextbox.textColor = color;
    [ZMProntoManager sharedInstance].assetsIdToFilter = @[];
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
    if (self.gsVCDelegate != nil &&
        [self.gsVCDelegate respondsToSelector:@selector(didRemovedFromSuperView)])
    {
        [self.gsVCDelegate didRemovedFromSuperView];
    }
}

#pragma mark - ZMListItem Delegate Methods
- (void)cell:(FranchiseTableViewCell*)cell  configureExportPopOver:(UIButton*) sender
{
    [self openExportMenu:cell sender:sender];
}

- (void)cell:(FranchiseTableViewCell*)cell  configureInfoPopOver:(UIButton*) sender
{
    [self openImportMenu:cell sender:sender];
}

- (void)cell:(FranchiseTableViewCell*)cell  configureFavButton:(UIButton*) sender
{
    [self favTappedAction:cell sender:sender];
}

#pragma mark - ZMGridItemDelegate Delegate Methods
- (void)gridCell:(ZMGridItem*)cell  configureExportPopOver:(UIButton*) sender
{
    [self openExportMenu:cell sender:sender];
}

- (void)gridCell:(ZMGridItem*)cell  configureInfoPopOver:(UIButton*) sender
{
    [self openImportMenu:cell sender:sender];
}

- (void)gridCell:(ZMGridItem*)cell  configureFavButton:(UIButton*) sender
{
    [self favTappedAction:cell sender:sender];
}

#pragma mark - Popover delegate Method
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    if (self.overlayView.hidden == NO)
    {
        [self dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
    }
    if (self.infoMenuIsPresented == YES)
    {
        [self doSetupAfterDismissalOfInfoMenuView];
        [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
        _infoMenuIsPresented = NO;
    }
    if (self.exportMenuIsPresented == YES)
    {
        [self doSetupAfterDismissalOfExportView];
        [ZMUserActions sharedInstance].export_infoMenuValOnOrientation = 0;
        _exportMenuIsPresented = NO;
    }
}

- (void)doSetupAfterDismissalOfInfoMenuView
{
    [self.infoMenu dismissViewControllerAnimated:NO completion:nil];
    self.infoMenu = nil;
    if (self.popOverPresentingCell != nil)
    {
        // Pop over were presented from list
        if ([self.popOverPresentingCell isKindOfClass:[FranchiseTableViewCell class]])
        {
            FranchiseTableViewCell *cell = (FranchiseTableViewCell*)self.popOverPresentingCell;
            [cell makeInfoButtonSelected:NO];
            
        }
        else if ([self.popOverPresentingCell isKindOfClass:[ZMGridItem class]])
        {
            ZMGridItem *cell = (ZMGridItem*)self.popOverPresentingCell;
            [cell makeInfoButtonSelected:NO];
        }
    }
}

- (void)doSetupAfterDismissalOfExportView
{
    [self.exportMenu dismissViewControllerAnimated:NO completion:nil];
    
    if (self.popOverPresentingCell != nil)
    {
        // Pop over were presented from list
        if ([self.popOverPresentingCell isKindOfClass:[FranchiseTableViewCell class]])
        {
            FranchiseTableViewCell *cell = (FranchiseTableViewCell*)self.popOverPresentingCell;
            [cell makeShareButtonSelected:NO];
            
        }
        else if ([self.popOverPresentingCell isKindOfClass:[ZMGridItem class]])
        {
            ZMGridItem *cell = (ZMGridItem*)self.popOverPresentingCell;
            [cell makeShareButtonSelected:NO];
        }
    }
}

#pragma mark - ZMAsset Library observer method
- (void)libraryProgressStep:(NSString *)progressText progress:(float)progress forVal:(NSString*)progressFor
{
    [AbbvieLogging logInfo:@"Search -> libraryProgressStep"];
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        [self showTransparentOverlayView:NO];
        
        if([progressText isEqualToString:@"Synchronizing User Data"])
        {
            [ZMUserActions sharedInstance].enableSearch = NO;
        }
        else if([progressText isEqualToString:@"Getting Assets 100%"])
        {
            [ZMUserActions sharedInstance].enableSearch = YES;
        }
        else
        {
            [ZMUserActions sharedInstance].enableSearch = NO;
        }
        
        UIColor *color = [theme whiteFontColor];
        self.parentHeader.searchTextbox.backgroundColor = [UIColor clearColor];
        self.parentHeader.searchTextbox.font = [theme normalLato];
        
        if(_apiManager.isConnectionAvailable && !_apiManager._syncingCompletion)
        {
            self.parentHeader.searchTextbox.enabled = NO;
            self.parentHeader.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Loading Assets..." attributes:@{NSForegroundColorAttributeName: color}];
            self.parentHeader.searchTextbox.text = @"";
            [self.parentHeader.lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
            self.parentHeader.searchTextbox.textColor = color;
            [ZMUserActions sharedInstance].enableSearch = NO;
        }
        else
        {
            self.parentHeader.searchTextbox.enabled = YES;
            self.parentHeader.searchTextbox.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color}];
            self.parentHeader.searchTextbox.textColor = [UIColor whiteColor];
            [ZMUserActions sharedInstance].enableSearch = YES;
        }
        if(self.parentHeader.searchTextbox.text.length >= 3 && (_apiManager.isConnectionAvailable || [ZMUserActions sharedInstance].isKeywordSaved))
        {
            [self loadSearchedAssets];
        }
        
        if ([progressFor isEqualToString:keySearchString] == YES && isScreenVisible)
        {
            [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingSearchResults];
        }
        else if ([progressFor isEqualToString:keyMainSync] == YES &&
                 self.isSyncAllEventsCalledFromBackgroundToForeground == YES)
        {
            [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingAssets];
        }
        
    });
}

- (void)libraryDidFinishWithError {
    
    dispatch_async(dispatch_get_main_queue(), ^{
      
        [AbbvieLogging logError:@"libraryDidFinishWithError "];
        self.isSyncAllEventsCalledFromBackgroundToForeground = NO;
       
    });
}

- (void)libraryFinishSync
{
    [AbbvieLogging logInfo:@"Search -> libraryFinishSync"];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self loadSearchedAssets];
        [ZMUserActions sharedInstance].enableSearch = YES;
        self.parentHeader.searchTextbox.enabled = YES;
        _apiManager._syncingCompletion = YES;
        self.parentHeader.searchTextbox.text = [ZMProntoManager sharedInstance].search.getTitle;
        
        if (self.isSyncAllEventsCalledFromBackgroundToForeground == YES)
        {
            //Force EventV3 sync
            [[ProntoUserDefaults userDefaults] setEventV3RequestMadeFromCMSValue:NO];
            [self syncAllEvents];
        }
        else
        {
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingAssets];
        }
        
        [self openAssetFromDeepLinkIfRequired];
    });
}

- (void)openAssetFromDeepLinkIfRequired
{
    if (self.actions.deepLinkUserInfo != nil)
    {
        // take page number as well
        NSString *assetId       = [self.actions.deepLinkUserInfo valueForKey:@"assetString"];
        NSInteger pageNumber    = [[self.actions.deepLinkUserInfo  valueForKey:@"pageNumer"] integerValue];
        NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:assetId];
        if (assetArray.count > 0) // It means asset available
        {
            Asset * asset = assetArray[0];
            //PRONTO-37 Deeplinking to a Page in a PDF
            [ZMUserActions sharedInstance].pageNo = pageNumber;
            //            [self validateAsset:asset];
            [self validateAndShowAsset:asset];
        }
        else
        {
            [[EventsServiceManager sharedServiceManager] syncEventsAndOpenAssetForAssetId:assetId userFranchise:self.parentHeader.userFranchise.text andDelegate:(id <EventOnlyAssetHandlerDelegate>)self];
        }
        self.actions.deepLinkUserInfo = nil;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        //NSLog(@"Lib  libraryFinishSync  from deepLinkUserInfo block");
    }
}

-(void) loadSearchedAssets
{
    [AbbvieLogging logInfo:@"Search -> loadSearchedAssets"];
    [ZMProntoManager sharedInstance].assetsIdToFilter = @[];
    self.parentHeader.searchTextbox.text = [[ZMProntoManager sharedInstance].search getTitle];
    
    // check the sql script
    [[ZMCDManager sharedInstance] getSearchedAssets:0 limit:-1 success:^(NSArray * totalAssets) {
        
        totalAssets = [ZMUserActions sharedInstance].searchedKeys;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
             [self.parentHeader showIndicatorFromGlobalSearch];
            
            if (self.emptyView != nil && self.emptyView.isHidden == NO)
            {
                [self hideEmpty];
            }
        });
        
        [[ZMCDManager sharedInstance ] getSearchedAssets:_pageIndex
                                                   limit:(unsigned long)totalAssets.count
                                                 success:^(NSArray * assets) {
                                                     
                                                     
                                                     if([ZMUserActions sharedInstance].searchedCount > 0)
                                                     {
                                                         [[ZMUserActions sharedInstance] setResultantAssets:assets];
                                                     }
                                                     else
                                                     {
                                                         [[ZMUserActions sharedInstance] setResultantAssets:nil];
                                                     }
                                                     
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         
                                                         NSMutableArray* checkingAssets = [[NSMutableArray alloc]initWithArray:[[ZMUserActions sharedInstance] resultantAssets]];
                                                         NSMutableArray *page = [[NSMutableArray alloc]initWithArray:[_apiManager sortedArray:[ZMUserActions sharedInstance] .sortingKey andArray: [NSMutableArray arrayWithArray:checkingAssets]]];
                                                         
                                                         // updating the datasource.
                                                         [self.globalSearchViewModel prepareDataSourceWithAssets:page];
                                                         
                                                         // display the count
                                                         [ZMUserActions sharedInstance].assetsCount  = (int)[[ZMUserActions sharedInstance] resultantAssets].count;
                                                         
                                                         ZMProntoManager.sharedInstance.assetsIdToFilter = ZMProntoManager.sharedInstance.assetArray;
                                                         
                                                         
                                                         if(_apiManager._currentPage == _apiManager.totalPages && [ZMUserActions sharedInstance].isFinalSearch)
                                                         {
                                                             // It means that search has been completed
                                                             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                                
                                                                 [self.parentHeader hidingIndicator];
                                                             });
                                                             
                                                             
                                                             if (page.count <= 0)
                                                             {
                                                                 //  we found no result.
                                                                 [self displayEmpty:NO];
                                                             }
                                                             else
                                                             {
                                                                 //  we found result.
                                                                 [self hideEmpty];
                                                             }
                                                             
                                                             [self reloadTableViewOrCollectionView];
                                                         }
                                                         else
                                                         {
                                                             // It means that search is still in progress.
                                                             [self reloadTableViewOrCollectionView];
                                                             [AbbvieLogging logInfo:@" // It means that search is still in progress."];
                                                         }
                                                         
                                                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                             
                                                             [self.parentHeader hidingIndicator];
                                                         });
                                                         
                                                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(7.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                             if ([self.globalSearchViewModel isDataSourceEmpty] == NO)
                                                             {
                                                                 [self dismissKeyboard];
                                                             }
                                                         });
                                                     });
                                                 }];
    }];
}

#pragma empty View

- (void)displayEmpty:(BOOL)isInAFolder
{
    if (self.emptyView == nil)
    {
        self.emptyView = [[ZMEmpty alloc] initWithGlobalSearchAsParent:self.gsLibraryTableView];
    }
    
    NSMutableDictionary *trackingOptions = [[NSMutableDictionary alloc] init];
    [trackingOptions setObject: [[ZMUserActions sharedInstance] getFiltersForTracking] forKey:@"scfiltersubcategory"];
    [trackingOptions setObject:@"None" forKey:@"scfilteraction"];
    [trackingOptions setObject:@"tier2" forKey:@"sctierlevel"];
    
    
    if(self.globalSearchViewModel.isDataSourceEmpty == YES)
    {
        NSString *message = @"";
        if([ZMUserActions sharedInstance].searchCompletion)
        {
            message = @"No search results found.";
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:ProntoMessageGettingAssets];
        }
        self.emptyView.subtitle.text = @"";
        self.emptyView.message.text = message;
    }
    
    [self.gsLibraryTableView addSubview:self.emptyView];
    [ZMEmpty transitionWithView:self.emptyView duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
    [self.emptyView setHidden:NO];
}

- (void)hideEmpty
{
    [self.emptyView removeFromSuperview];
    self.emptyView = nil;
}

-(void)header:(ZMHeader*)headerView editingChanged:(NSString*)searchtext
{
    if (self.emptyView != nil && self.emptyView.isHidden == NO)
    {
        [self hideEmpty];
    }
    
    if (searchtext.length <= 0)
    {
//        [self showTransparentOverlayView:YES];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [headerView.searchTextbox becomeFirstResponder];
//        });
        
        [self dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
    }
}

-(void)header:(ZMHeader*)headerView didTapShouldTextFieldReturn:(UITextField *)textField
{
    if (textField.text.length <= 0)
    {
        [self dismissOverlayViewFromSuperViewAndDoSetupWhichRequired];
    }
}

- (void)resetTextSearchValues
{
    [self.view endEditing:YES];
    [self.parentHeader.searchTextbox resignFirstResponder];
    [self.parentHeader resignFromFirstResponder];
    [self.parentHeader.lensButton setImage:[UIImage imageNamed:@"searchIcon"] forState:UIControlStateNormal];
}

- (void)showSearchedAssetFromOfflineMode
{
    [self showTransparentOverlayView:NO];
    [self loadSearchedAssets];
}

// same format but caseinsensitive
BOOL isShowing(NSArray* pageNums, NSString* stringToCheck)
{
    for (NSString* string in pageNums) {
        if ([string caseInsensitiveCompare:stringToCheck] == NSOrderedSame)
            return YES;
    }
    return NO;
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
    [self.parentHeader resignFromFirstResponder];
}
#pragma mark - Tab Bar Related Methods
-(void)addTabBar
{
    if(self.flatTabBarViewController == nil)
    {
        self.flatTabBarViewController = [[FlatTabBarViewController alloc]init];
        self.flatTabBarViewController.view.backgroundColor = [UIColor clearColor];
        [self.flatTabBarContainerView addSubview:self.flatTabBarViewController.view];
        self.flatTabBarViewController.view.frame = CGRectMake(0,0, self.flatTabBarContainerView.bounds.size.width, self.flatTabBarContainerView.bounds.size.height - 5);
        self.flatTabBarViewController.delegate = (id<FlatTabBarDelegate>)self;
        self.flatTabBarViewController.labelBottomSepratorColor = [UIColor GetColorFromHexValue0082ba];
    }
}

- (void)updateTabBarView
{
    NSInteger currentSelectedIndex = self.flatTabBarViewController.selectedIndex.integerValue;
    [self.flatTabBarViewController.view removeFromSuperview];
    self.flatTabBarViewController = nil;
    [self addTabBar];
    [self.flatTabBarViewController selectItemAtIndex:currentSelectedIndex andNotifyBack:NO];
    self.selectedTab = currentSelectedIndex;
}

#pragma mark FlatTabBarDelegate Members && FlatTabBarDelegate Members

-(void)didSelectedTabAtIndex:(NSInteger)index forTabBar:(FlatTabBarViewController*)tabBar
{
    [self stopAudio];
    self.selectedTab = index;
    
    NSString *title = [self titleForTabAtIndex:index inTabBar:tabBar];
    
    if([title containsString:keyMyEvents])
    {
        //Removing before adding to new one.
        [self.eventsGlobalSearch.view removeFromSuperview];
        self.eventsGlobalSearch  = nil;
        
        //Adding and removing will not happen
        self.eventsGlobalSearch = [EventsGlobalSearch GetEventsGSViewController];
        self.eventsGlobalSearch.delegate = (id<EventsGSDelegate>)self;
        self.eventsGlobalSearch.dataSourceFromGlobalSearchViewModal = [self.globalSearchViewModel getAssetArrayForSection:index];
        self.eventsGlobalSearch.view.frame = self.gsLibraryTableView.frame;
        [self.listGridContainerView addSubview:self.eventsGlobalSearch.view];
        
        self.gsLibraryColectionView.hidden = YES;
        self.gsLibraryTableView.hidden     = YES;
        
        [self.contentHeader shouldEnableRecentlyAddedAndGridSelectionControl:NO];
        
    }
    else
    {
        [self.eventsGlobalSearch.view removeFromSuperview];
        self.eventsGlobalSearch  = nil;
        
        self.gsLibraryColectionView.hidden = NO;
        self.gsLibraryTableView.hidden     = NO;
        
        [self.contentHeader shouldEnableRecentlyAddedAndGridSelectionControl:YES];
        
        NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
        if ([showingGridView isEqualToString:@"YES"])
        {
            self.gsLibraryTableView.hidden     = YES;
            [self.gsLibraryColectionView reloadData];
        }
        else
        {
            self.gsLibraryColectionView.hidden = YES;
            [self.gsLibraryTableView reloadData];
        }
    }
}

-(BOOL)hasSomeData
{
    return NO;
}
-(int)numberOfTabsInTabBar:(FlatTabBarViewController*) tabBar
{
    return (int)[self.globalSearchViewModel getSectionForTableViewOrCollectionView];
}

-(NSString*)titleForTabAtIndex:(NSInteger)index inTabBar:(FlatTabBarViewController*)tabBar
{
    return [self.globalSearchViewModel getTitleForHeaderInSection:index];
}

-(BOOL)shouldAutomanageTabWidthInTabBar:(FlatTabBarViewController*) tabBar
{
    return NO;
}

-(UIColor*)textColorForTabsinTabBar:(FlatTabBarViewController*)tabBar
{
    return [UIColor GetColorFromHexValueaba9a8];
}

-(UIColor*)highlightedTextColorForTabsinTabBar:(FlatTabBarViewController*)tabBar
{
    return [UIColor blackColor];
}

-(UIFont*)fontForTabTextinTabBar:(FlatTabBarViewController*)tabBar
{
    return  [[ZMProntoTheme sharedTheme] bigBoldLatoWithFontSize16];
}

-(UIFont*)highlightedFontForTabTextinTabBar:(FlatTabBarViewController*)tabBar
{
    return [[ZMProntoTheme sharedTheme] bigBoldLatoWithFontSize17];
}

-(float)widthForTabItemAtIndex:(NSInteger)index inTabBar:(FlatTabBarViewController*)tabBar
{
    NSString *tabText =  [self.globalSearchViewModel getTitleForHeaderInSection:index];
    return [self getWidthForText:tabText];
    return widthOfSingleTab;
}

-(float)spacingBetweenTabsForTabsInTabBar:(FlatTabBarViewController*)tabBar
{
    return 10; 
}

-(UIColor*)bottomSepratorColorForTabBar:(FlatTabBarViewController *)tabBar
{
    return [UIColor GetColorFromHexValuef2f2f2];
}

- (float)getWidthForText:(NSString*)tabText
{
    NSInteger width = widthOfSingleTab;
    if ([tabText containsString:keyMyCompetitors])
    {
        width+= 15;
    }
    else if ([tabText containsString:keyMyToolsTraing])
    {
         width+= 45;
    }
    return width;
}

- (NSString*) getBuckeNameFromSelectedTab
{
    return [self.globalSearchViewModel getCurrentActiveTabOptionAsStringForSelectedIndex:self.selectedTab];
}

- (NSInteger) getBuckeIdFromSelectedTab
{
    return [self.globalSearchViewModel getBucketIdForSelectedTab:self.selectedTab];
}

- (void)reloadTheSearchResultsAfterUpdatingTheLocalDatabase
{
    [self updateWithCMS];
    self.isSyncAllEventsCalledFromBackgroundToForeground = YES;
}

- (void)updateWithCMS
{
    @synchronized(self)
    {
        if (_apiManager._syncingInProgress == NO && [AOFLoginManager sharedInstance].checkForAOFLogin == YES)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [_apiManager syncWithCMS];
            });
        }
    }
}

- (void) reloadGlobalSearchResult
{
    [ZMUserActions sharedInstance].searchCompletion = YES;
    [self loadSearchedAssets];
}

- (void)setExportToNormal:(BOOL) dismissExport
{
    NSString *showingGridView = [[NSUserDefaults standardUserDefaults] valueForKey:keyUserDefaultGridFranchise];
    if (self.exportMenuIsPresented) {
        if(dismissExport == YES)
        {
            [[self.exportMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
            self.exportMenuIsPresented = NO;
        }
        if ([showingGridView isEqualToString:@"YES"])
        {
            ZMGridItem *tempCell = (ZMGridItem *)[self.gsLibraryColectionView cellForItemAtIndexPath: [NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (tempCell && tempCell.shareButton.isSelected)
            {
                [tempCell makeShareButtonSelected:NO];
            }
        }
        else
        {
            FranchiseTableViewCell * cell = (FranchiseTableViewCell*)[self.gsLibraryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedItem inSection:0]];
            if (cell && cell.shareButton.isSelected)
            {
                [cell makeShareButtonSelected:NO];
            }
        }
        
    }
}

#pragma mark -
#pragma mark Events Global search view controller

- (void)eventsGlobalSearchViewController:(EventsGlobalSearch*)eventsGlobalSearchVC didSelectItem:(NSObject*)sessionOrEoa andEventDuration:(EventDuration*)eventDuration
{
    
    
    if ([sessionOrEoa isKindOfClass:[Session class]])
    {
        Event *event  = [(Session*)sessionOrEoa getEventForCurrentSession];
        
        if (event != nil)
        {
            CalendarDetailVC *calendarDetailScreenVC = [CalendarDetailVC GetCalendarDetailsScreen];
            calendarDetailScreenVC.event = event;
            calendarDetailScreenVC.selectedDate  = eventDuration.eventDate;
            calendarDetailScreenVC.isComingFromGlobalSearchResult = YES;
            [self.navigationController pushViewController:calendarDetailScreenVC animated:YES];
        }
    }
    else if ([sessionOrEoa isKindOfClass:[EventOnlyAsset class]])
    {
        //It is assumed that from Event Global search only Event only Asset come
        //if it is other it will not be opened.
        if (self.eventOnlyAssetHandler == nil)
        {
            self.eventOnlyAssetHandler = [[EventOnlyAssetHandler alloc]init];
            self.eventOnlyAssetHandler.delegate      = (id <EventOnlyAssetHandlerDelegate>)self;
        }
        self.eventOnlyAssetHandler.userFranchise = self.parentHeader.userFranchise.text;
        [self.eventOnlyAssetHandler validateAndShowEventOnlyAsset:(EventOnlyAsset*)sessionOrEoa];
    }
}

- (void)eventsGlobalSearchViewController:(EventsGlobalSearch*)eventsGlobalSearchVC didSelectEOA:(EventOnlyAsset*)eventOnlyAsset
{
    //It is assumed that from Event Global search only Event only Asset come
    //if it is other it will not be opened.
    if (self.eventOnlyAssetHandler == nil)
    {
        self.eventOnlyAssetHandler = [[EventOnlyAssetHandler alloc]init];
        self.eventOnlyAssetHandler.delegate      = (id <EventOnlyAssetHandlerDelegate>)self;
    }
    self.eventOnlyAssetHandler.userFranchise = self.parentHeader.userFranchise.text;
    [self.eventOnlyAssetHandler validateAndShowEventOnlyAsset:eventOnlyAsset];
}

- (void)eventsGlobalSearchViewController:(EventsGlobalSearch*)eventsGlobalSearchVC didSelectAsset:(Asset*)asset
{
    if (asset == nil) {return;}
    
    if ([asset title].length > 0)
    {
        [self validateAndShowAsset:asset];
    }
}
#pragma mark - EventOnlyAssetHandlerDelegate starts

- (UIViewController*) getPresentViewControllerForEventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler
{
    return self;
}

- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler pushViewController:(UIViewController*)viewController
{
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler setProgress:(CGFloat)progress
{
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Progress: %f",progress]];
}

- (void)eventOnlyAssetHandler:(EventOnlyAssetHandler*)eventAssetHandler openAsset:(EventOnlyAsset*)selectedAsset
{
    //Asset Details accepts only Asset object and it is very tighly coupled with controller.
    //So, Instead of creating new asset detail controller, we are passing equivalent object of eventOnlyAsset.
    Asset *equivalentAssetForEventOnlyAsset = [selectedAsset getEquivalentAssetObject];
    
    if(self.parentHeader.categoryPopOver)
    {
        [[self.parentHeader.categoryPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    if(self.parentHeader.sortMenuPopOver)
    {
        [[self.parentHeader.sortMenuPopOver presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
    }
    
    [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.eoa_path]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.eventOnlyAssetHandler.mainDelegate hideMessage];
    });
    
    enum ZMProntoFileTypes assetType = [selectedAsset getFileTypeOfEventOnlyAsset];
    
    self.eventOnlyAssetHandler.isDownloadingAsset = NO;
    
    /*NSString *activityType = @"Pronto Object";
    if (assetType == ZMProntoFileTypesPDF) {
        activityType = @"PDF";
    }
    else if (assetType == ZMProntoFileTypesVideo) {
        activityType = @"VIDEO";
    }
    else if (assetType == ZMProntoFileTypesBrightcove) {
        activityType = @"VIDEO";
    }
    else if (assetType == ZMProntoFileTypesDocument) {
        activityType = @"DOCUMENT";
    }
    else if (assetType == ZMProntoFileTypesEpub) {
        activityType = @"EPUB";
    }
    else if(assetType == ZMProntoFileTypesWeblink)
    {
        activityType = @"WEBLINK";
    }*/
    
    //Tracking
    //    [ASFActivityLog logActionForCurrentUser:@"viewed" withObjectName:[NSString stringWithFormat:@"%@",selectedAsset.title] ofType:[NSString stringWithFormat:@"%@",activityType] completionHandler:^(ASFActivityLogStoreResult result, NSArray *statements, NSError *error) {
    //        if (!result){
    //            //Ignore Errors
    //        }
    //    }];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController <ZMAssetViewControllerDelegate> *controller;
    
    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
    if (assetType == ZMProntoFileTypesPDF)
    {
        if(document != nil)
        {
            [self openFastPDFForEquivalentEventOnlyAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.eventOnlyAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
            });
        }
    }
    else if (assetType == ZMProntoFileTypesBrightcove)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
        controller.asset = equivalentAssetForEventOnlyAsset;
        controller.userFranchise = self.parentHeader.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (assetType == ZMProntoFileTypesDocument)
    {
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
//        controller.calendarDetailVC = self;
        controller.asset = equivalentAssetForEventOnlyAsset;
        controller.userFranchise = self.parentHeader.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    else if(assetType == ZMProntoFileTypesWeblink)
    {
        // open the weblink URL
        controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
        ((ZMAssetViewController*)controller).isEventOnlyAsset = YES;
//        controller.calendarDetailVC = self;
        controller.asset = equivalentAssetForEventOnlyAsset;
        controller.userFranchise = self.parentHeader.userFranchise.text;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else
    {
        if (assetType == ZMProntoFileTypesPDF)
        {
            if(selectedAsset.eoa_path)
            {
                MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.eoa_path]];
                if(document != nil)
                {
                    [self openFastPDFForEquivalentEventOnlyAsset:equivalentAssetForEventOnlyAsset pdfController:controller pdfDocument:document];
                }
                else
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.eventOnlyAssetHandler.mainDelegate showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                        self.eventOnlyAssetHandler.currentSelectedAssetToSeeDetail = nil;
                    });
                }
            }
        }
    }
    
}

- (void)openFastPDFForEquivalentEventOnlyAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
    controller.asset = asset;
    
    ((FastPDFViewController*)controller).isEventOnlyAsset = YES;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    //    [view addSubview:controller.view];
    
    [viewController.view addSubview:view];
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    //    controller.view.frame = view.bounds;
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
    
    //    [controller didMoveToParentViewController:viewController];
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor GetColorFromHexValueE0E6ED];
    [viewController.view addSubview:webview];
    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    if (self.eventOnlyAssetHandler != nil)
    {
        [self.eventOnlyAssetHandler hideMessage];
    }
    
    controller.userFranchise = self.parentHeader.userFranchise.text;
    controller.viewMain = view;
    controller.webviewScreen = webview;
    controller.webviewScreen.hidden = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];

}

- (void)syncAllEvents
{
    BOOL hasEventV3RequestBeenMade = [[ProntoUserDefaults userDefaults] hasEventV3RequestBeenMadeFromCMS];
    if (hasEventV3RequestBeenMade ==  YES)
    {
        //EventV3 request already been made. So, Don't need to make it again.
        return;
    }
    
    //Sync in progress.
//    [ZMUserActions sharedInstance].enableSearch = NO;
//    self.parentHeader.searchTextbox.enabled = NO;
    
    float waitTime = self.isSyncAllEventsCalledFromBackgroundToForeground ? 0.1: 0.0;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(waitTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[AppUtilities sharedUtilities] showBottomIndicatorView:YES withMessage:ProntoMessageGettingAllEvents];
    });
    
    __weak GlobalSearchVC *weakSelf = self;
    
    [[ZMAbbvieAPI sharedAPIManager] syncAllEvents:^(NSDictionary *allEvents) {
        
        [[EventCoreDataManager sharedInstance] saveAllEvents:allEvents withCompletionHandler:^(BOOL success, NSArray *events) {
            
            [ZMUserActions sharedInstance].enableSearch = YES;
            weakSelf.parentHeader.searchTextbox.enabled = YES;
            weakSelf.parentHeader.searchTextbox.text = [ZMProntoManager sharedInstance].search.getTitle;
            
            if (weakSelf.isSyncAllEventsCalledFromBackgroundToForeground == YES)
            {
                [weakSelf loadSearchedAssets];
            }
            weakSelf.isSyncAllEventsCalledFromBackgroundToForeground = NO;
            if (success == YES && events != nil)
            {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"All Events data %@", events]];
            }
            
            //Remove Bottom indicator every operation completes.
            [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:nil];
            [[ProntoUserDefaults userDefaults] setEventV3RequestMadeFromCMSValue:YES];
        }];
        
    } error:^(NSError *error) {
        
        [ZMUserActions sharedInstance].enableSearch = YES;
        weakSelf.parentHeader.searchTextbox.enabled = YES;
        weakSelf.parentHeader.searchTextbox.text = [ZMProntoManager sharedInstance].search.getTitle;
        weakSelf.isSyncAllEventsCalledFromBackgroundToForeground = NO;
        
        [[AppUtilities sharedUtilities] showBottomIndicatorView:NO withMessage:@"Getting Events"];
    }];
}

@end
