//
//  GSGridCollectionHeaderView.m
//  Pronto
//
//  Created by m-666346 on 24/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "GSGridCollectionHeaderView.h"
#import "ZMProntoTheme.h"

@implementation GSGridCollectionHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
}

- (void)setupUI
{
    self.assetCountLabel.font       = [[ZMProntoTheme sharedTheme] bigBoldLato]; // make combination of normal bold
    self.assetCountLabel.textColor  = [[ZMProntoTheme sharedTheme] franchiseCountColor];
}

+ (NSString*)GetGSGridCollectionHeaderViewIdentifier
{
    return @"GSGridCollectionHeaderViewIdentifier";
}

+ (NSString*)GetGSGridCollectionHeaderViewNibName
{
    return @"GSGridCollectionHeaderView";
}

- (void)updateAssetCountLabelWithText:(NSString*)text
{
    self.assetCountLabel.text = text;
}

@end
