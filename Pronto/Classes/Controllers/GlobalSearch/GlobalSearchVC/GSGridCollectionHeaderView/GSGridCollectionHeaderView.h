//
//  GSGridCollectionHeaderView.h
//  Pronto
//
//  Created by m-666346 on 24/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSGridCollectionHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel * assetCountLabel;

- (void)updateAssetCountLabelWithText:(NSString*)text;
+ (NSString*)GetGSGridCollectionHeaderViewIdentifier;
+ (NSString*)GetGSGridCollectionHeaderViewNibName;
@end
