//
//  GlobalSearchVC.h
//  Pronto
//
//  Created by cccuser on 16/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.

#import <UIKit/UIKit.h>
#import "GlobalSearchViewModel.h"
#import "ExportMenu.h"
#import "InfoMenu.h"

//@class GolobalSearchViewModel;
@class GlobalSearchVC;
@class Asset;
@class FranchiseTableViewCell;
@class FeedbackView;

@protocol GSViewControllerDelegate<NSObject>

- (void)gsLibraryViewController:(GlobalSearchVC*)gsLibraryViewController openAsset:(Asset*)asset fromListOrGridAtIndex:(NSInteger)index ofType:(NSString*)type;
- (void)gsLibraryViewController:(GlobalSearchVC*)gsLibraryViewController cell:(id)cell  configureExportPopOver:(UIButton*) sender;
- (void)gsLibraryViewController:(GlobalSearchVC*)gsLibraryViewController cell:(id)cell  configureInfoPopOver:(UIButton*) sender;
- (void)gsLibraryViewController:(GlobalSearchVC*)gsLibraryViewController cell:(id)cell  configureFavButton:(UIButton*) sender;
- (void)didRefreshControlSelected;
@required
- (void)didRemovedFromSuperView;
- (void)enableAssetLibraryObserver;


@end

@interface GlobalSearchVC : UIViewController
@property (nonatomic, strong) GlobalSearchViewModel *globalSearchViewModel;
@property (nonatomic, strong) FeedbackView *feedback;
@property (nonatomic, assign) id<GSViewControllerDelegate>gsVCDelegate;
//@property (nonatomic, weak) ZMHeader *parentHeader;
@property (nonatomic, strong) ZMHeader *parentHeader; 

@property (weak, nonatomic) IBOutlet UITableView *gsLibraryTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *gsLibraryColectionView;

+ (GlobalSearchVC*)GetGSLibraryViewController;
- (void) reloadTableViewOrCollectionView;
- (void) showTransparentOverlayView:(BOOL)yesOrNo;
- (void)dismissOverlayViewFromSuperViewAndDoSetupWhichRequired;
- (void)initiateFeedback:(NSString*) selectedAsset openedFrom:(NSString*) choice;
- (void)markLiked:(Asset *)selectedAsset type:(NSInteger)type;
- (void)openAsset:(Asset *)selectedAsset;
- (void)showSearchedAssetFromOfflineMode;

// Export Menu
//@property (nonatomic, weak) ExportMenu *exportMenu;
@property (nonatomic, strong) ExportMenu *exportMenu;

@property (nonatomic) BOOL exportMenuIsPresented;
@property(nonatomic, strong) UIPopoverPresentationController* exportMenuPopOver;

// Info Menu
//@property (nonatomic, weak) InfoMenu *infoMenu;
@property (nonatomic, strong) InfoMenu *infoMenu;
@property (nonatomic, strong) UIPopoverPresentationController *infoMenuPopOver;
@property (nonatomic) BOOL infoMenuIsPresented;

@property (strong, nonatomic) Asset *currentSelectedAssetForFav;

- (NSString*) getBuckeNameFromSelectedTab;
- (NSInteger) getBuckeIdFromSelectedTab;
- (void)validateAndShowAsset:(Asset *)selectedAsset;
- (void)reloadTheSearchResultsAfterUpdatingTheLocalDatabase;
- (void) reloadGlobalSearchResult;
- (void)setExportToNormal:(BOOL) dismissExport;
- (void)downloadOneAsset:(Asset *)selectedAsset completion:(void (^)(BOOL assetDownloaded))completion
                 failure:(void (^)(NSString *assetDownloadFail))failure;
-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure;
@end
