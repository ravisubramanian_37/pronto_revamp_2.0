//
//  GlobalSearchViewModel.h
//  Pronto
//
//  Created by cccuser on 16/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
@class Asset;

@protocol GlobalSearchViewModelDelegate <NSObject>

@end

@interface GlobalSearchViewModel : NSObject

@property(nonatomic, weak) id<GlobalSearchViewModelDelegate> gsVMDelegate;
@property (nonatomic, strong) NSMutableArray *globalSearchDataSource;

- (instancetype)initWithDelegate:(id<GlobalSearchViewModelDelegate>)delegate;

- (NSString*)getTitleForHeaderInSection:(NSInteger)section;
- (NSInteger)getNumberOfRowsInSection:(NSInteger)section;
- (NSArray*)getAssetArrayForSection:(NSInteger)section;
- (Asset*)getAssetForSection:(NSInteger)section andRow:(NSInteger)row;
- (NSInteger)getSectionForTableViewOrCollectionView;
- (GlobalSearchSection)getGlobalSearchSectionEnumWithUISection:(NSInteger)section;

//It prepares the datasource once we get the data from CDManager
- (void)prepareDataSourceWithAssets:(NSArray*)assets;
- (BOOL)isDataSourceEmpty;
- (BOOL)shouldHideTableOrCollectionView;
- (NSInteger)getBucketIdForSelectedTab:(NSInteger)selectedIndex;
- (NSString*)getCurrentActiveTabOptionAsStringForSelectedIndex:(NSInteger)currentTabIndex;

@end
