//
//  GlobalSearchViewModel.m
//  Pronto
//
//  Created by cccuser on 16/04/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "GlobalSearchViewModel.h"
#import "Asset.h"
#import "Constant.h"
#import "EventCoreDataManager.h"
#import "EventsGlobalSearchViewModel.h"

@implementation GlobalSearchViewModel

- (instancetype)initWithDelegate:(id<GlobalSearchViewModelDelegate>)delegate
{
    if (self = [super init])
    {
        self.gsVMDelegate    = delegate;
        [self initializeGlobalSearchDataSource];
    }
    return self;
}

- (void)initializeGlobalSearchDataSource
{
    /*
        Whenever there is change in data source create a dictionary with respected key
        and values as array and insert it as proper index.
        At 0 index myFranchise
        At 1 index myCompetitos
        At 2 index myToolsTraining
        If count of Array is zero, it means there is no assets available for that section.
        And therefore hide the section.
     */
    self.globalSearchDataSource = [NSMutableArray array];

    NSDictionary *myFranchise     = @{keyMyFranchise:@[]};
    NSDictionary *myCompetitors   = @{keyMyCompetitors:@[]};
    NSDictionary *myToolsTraining = @{keyMyToolsTraing:@[]};
    NSDictionary *myEvents        = @{keyMyEvents:@[]};
    
    [self.globalSearchDataSource addObject:myFranchise];
    [self.globalSearchDataSource addObject:myCompetitors];
    [self.globalSearchDataSource addObject:myToolsTraining];
    [self.globalSearchDataSource addObject:myEvents];
}

- (void)prepareDataSourceWithAssets:(NSArray*)assets
{
    [self.globalSearchDataSource removeAllObjects];

    NSMutableArray *franchise = [NSMutableArray array];
    NSMutableArray *competitors = [NSMutableArray array];
    NSMutableArray *toolsTraining = [NSMutableArray array];
    
    //It fix the issue when globalsearch-> calendardetail-> globalsearch-> calendardetail-> globalsearch then  back
    NSMutableArray *events        = (NSMutableArray*)[[[EventCoreDataManager sharedInstance] getEventSearchedAssets] mutableCopy];
    
    for (Asset *asset in assets)
    {
        NSArray *bucketIDs = [self getBucketIDsOfAsset:asset];
        if ([bucketIDs containsObject:[Constant sharedConstant].ProntoTabOptionFranchiseID])
        {
            [franchise addObject:asset];
        }
        if ([bucketIDs containsObject:[Constant sharedConstant].ProntoTabOptionCompetitorID])
        {
            [competitors addObject:asset];
        }
        if ([bucketIDs containsObject:[Constant sharedConstant].ProntoTabOptionToolsAndTrainingID])
        {
            [toolsTraining addObject:asset];
        }
    }
    
    NSDictionary *myFranchise     = @{keyMyFranchise:franchise};
    NSDictionary *myCompetitors   = @{keyMyCompetitors:competitors};
    NSDictionary *myToolsTraining = @{keyMyToolsTraing:toolsTraining};
    NSDictionary *myEvents        = @{keyMyEvents:events};
    
    
    [self.globalSearchDataSource addObject:myFranchise];
    [self.globalSearchDataSource addObject:myCompetitors];
    [self.globalSearchDataSource addObject:myToolsTraining];
    [self.globalSearchDataSource addObject:myEvents];
    
}

- (NSArray*)getBucketIDsOfAsset:(Asset*)asset
{
    NSString *assetIDs = asset.assetBucketId;
    NSArray *arr = [assetIDs componentsSeparatedByString:@"_"];
    return arr;
}

#pragma mark - Helper Methods For Data Source
- (NSInteger)getSectionForTableViewOrCollectionView
{
    NSInteger count = 0;
    NSInteger addition = 0;
    NSDictionary *franchise     = self.globalSearchDataSource[0];
    NSDictionary *competitors   = self.globalSearchDataSource[1];
    NSDictionary *toolTraining  = self.globalSearchDataSource[2];
    NSDictionary *events        = self.globalSearchDataSource[3];
    count = count + addition;
    addition = ((NSArray*)franchise[keyMyFranchise]).count > 0 ? 1 : 0;
    count = count + addition;

    addition = ((NSArray*)competitors[keyMyCompetitors]).count  > 0 ? 1 : 0;
    count = count + addition;

    addition = ((NSArray*)toolTraining[keyMyToolsTraing]).count > 0 ? 1 : 0;
    count = count + addition;
    
    addition = ((NSArray*)events[keyMyEvents]).count > 0 ? 1 : 0;
    count = count + addition;

    return count;
}

- (Asset*)getAssetForSection:(NSInteger)section andRow:(NSInteger)row
{
    GlobalSearchSection enumSection = [self getGlobalSearchSectionEnumWithUISection:section];
    NSString *keyFromSection   = [self getKeyForSection:section];
    
    Asset *asset    = nil;
    NSDictionary *values = nil;
    if (enumSection == MyFranchise)
    {
        values  = self.globalSearchDataSource[0];
    }
    else if (enumSection == MyCompetitors)
    {
        values  = self.globalSearchDataSource[1];
    }
    else if (enumSection == MyToolsTraining)
    {
        values  = self.globalSearchDataSource[2];
    }
    else if (enumSection == MyEvents)
    {
        values  = self.globalSearchDataSource[3];
    }
    
    NSArray *assets = (NSArray*)values[keyFromSection];
    if (assets.count > 0 && assets.count > row)
    {
        asset = assets[row];
    }
    
    return asset;
}

-(NSArray*)getAssetArrayForSection:(NSInteger)section
{
    GlobalSearchSection enumSection = [self getGlobalSearchSectionEnumWithUISection:section];
    NSString *keyFromSection   = [self getKeyForSection:section];
    
    NSDictionary *values = nil;
    if (enumSection == MyFranchise)
    {
        values  = self.globalSearchDataSource[0];
    }
    else if (enumSection == MyCompetitors)
    {
        values  = self.globalSearchDataSource[1];
    }
    else if (enumSection == MyToolsTraining)
    {
        values  = self.globalSearchDataSource[2];
    }
    else if (enumSection == MyEvents)
    {
        values  = self.globalSearchDataSource[3];
    }
    return values[keyFromSection];
}

- (NSInteger)getNumberOfRowsInSection:(NSInteger)section
{
    GlobalSearchSection enumSection = [self getGlobalSearchSectionEnumWithUISection:section];
    NSString *keyFromSection   = [self getKeyForSection:section];
    
    NSDictionary *values = nil;
    if (enumSection == MyFranchise)
    {
        values  = self.globalSearchDataSource[0];
    }
    else if (enumSection == MyCompetitors)
    {
        values  = self.globalSearchDataSource[1];
    }
    else if (enumSection == MyToolsTraining)
    {
        values  = self.globalSearchDataSource[2];
    }
    else if (enumSection == MyEvents)
    {
        values  = self.globalSearchDataSource[3];
    }
    
    return  ((NSArray*)values[keyFromSection]).count;
}

- (NSString*)getTitleForHeaderInSection:(NSInteger)section
{
    GlobalSearchSection enumSection = [self getGlobalSearchSectionEnumWithUISection:section];
    NSString *keyFromSection   = [self getKeyForSection:section];
    NSInteger count  = [self getNumberOfRowsInSection:section];
    
    switch (enumSection)
    {
        case MyFranchise:
            return [NSString stringWithFormat:@"%@ (%02ld)", keyFromSection, (long)count];
            break;
        case MyCompetitors:
            return [NSString stringWithFormat:@"%@ (%02ld)",keyFromSection, (long)count];
            break;
        case MyToolsTraining:
            return [NSString stringWithFormat:@"%@ (%02ld)",keyFromSection, (long)count];
            break;
        case MyEvents:
        {
            NSMutableArray *events = [NSMutableArray array];
            for (NSDictionary *dict in self.globalSearchDataSource)
            {
                NSString *key = [[dict allKeys] lastObject];
                if ([key isEqualToString:keyFromSection])
                {
                    events = dict[keyFromSection];
                    break;
                }
            }
            
            EventsGlobalSearchViewModel *viewModel = [[EventsGlobalSearchViewModel alloc]init];
            [viewModel prepareEventGlobalResultsWithGlobalSearchResult:events];
            NSInteger  count = [viewModel getCountForEventsGlobalSearchHeader];
            if (count > 0)
            {
                 return [NSString stringWithFormat:@"%@ (%02ld)",keyFromSection, (long)count];
            }
            else
            {
                //This scenario should never come.
                 return [NSString stringWithFormat:@"%@",keyFromSection];
            }
            
            break;
        }
        default:
            break;
    }
    return @"";
}

- (NSString*)getKeyForSection:(NSInteger)section
{
    NSDictionary *myFranchise      = self.globalSearchDataSource[0];
    NSDictionary *myCompetitors    = self.globalSearchDataSource[1];
    NSDictionary *myToolsTraining  = self.globalSearchDataSource[2];
    NSDictionary *myEvents         = self.globalSearchDataSource[3];
    
    NSMutableArray *franchise      = myFranchise[keyMyFranchise];
    NSMutableArray *competitors    = myCompetitors[keyMyCompetitors];
    NSMutableArray *toolsTraining  = myToolsTraining[keyMyToolsTraing];
    NSMutableArray *events         = myEvents[keyMyEvents];
    
    NSString *key = nil;
    
    if (franchise.count > 0 && competitors.count > 0 && toolsTraining.count > 0 && events.count > 0) // 15 - 1111
    {
        if (section == 0)
        {
            key = keyMyFranchise;
        }
        else if (section == 1)
        {
            key = keyMyCompetitors;
        }
        else if (section == 2)
        {
            key = keyMyToolsTraing;
        }
        else if (section == 3)
        {
            key = keyMyEvents;
        }
    }
    else if (franchise.count > 0 && competitors.count > 0 && toolsTraining.count > 0) // 14 - 1110
    {
        if (section == 0)
        {
            key = keyMyFranchise;
        }
        else if (section == 1)
        {
            key = keyMyCompetitors;
        }
        else if (section == 2)
        {
            key = keyMyToolsTraing;
        }
    }
    else if (franchise.count > 0 && competitors.count > 0 && events.count > 0) // 13 - 1101
    {
        if (section == 0)
        {
            key = keyMyFranchise;
        }
        else if (section == 1)
        {
            key = keyMyCompetitors;
        }
        else if (section == 2)
        {
            key = keyMyEvents;
        }
    }
    else if (franchise.count > 0 && competitors.count > 0) // 12 - 1100
    {
        if (section == 0)
        {
            key = keyMyFranchise;
        }
        else if (section == 1)
        {
            key = keyMyCompetitors;
        }
    }
    else if (franchise.count > 0 && toolsTraining.count > 0 && events.count > 0) //11 - 1011
    {
        if (section == 0)
        {
            key = keyMyFranchise;
        }
        else if (section == 1)
        {
            key = keyMyToolsTraing;
        }
        else if (section == 2)
        {
            key = keyMyEvents;
        }
    }
    else if (franchise.count > 0 && toolsTraining.count > 0) //10 - 1010
    {
        if (section == 0)
        {
            key = keyMyFranchise;
        }
        else if (section == 1)
        {
            key = keyMyToolsTraing;
        }
    }
    else if (franchise.count > 0 && events.count > 0)//9 - 1001
    {
        if (section == 0)
        {
            key = keyMyFranchise;
        }
        else if (section == 1)
        {
            key = keyMyEvents;
        }
    }
    else if (franchise.count > 0) //8 - 1000
    {
        if (section == 0)
        {
            key = keyMyFranchise;
        }
    }
    else if (competitors.count > 0 && toolsTraining.count > 0 && events.count > 0) //7 - 0111
    {
        if (section == 0)
        {
            key = keyMyCompetitors;
        }
        else if (section == 1)
        {
            key = keyMyToolsTraing;
        }
        else if (section == 2)
        {
            key = keyMyEvents;
        }
    }
    else if (competitors.count > 0 && toolsTraining.count > 0 )//6 - 0110
    {
        if (section == 0)
        {
            key = keyMyCompetitors;
        }
        else if (section == 1)
        {
            key = keyMyToolsTraing;
        }
    }
    else if (competitors.count > 0 && events.count > 0)//5 - 0101
    {
        if (section == 0)
        {
            key = keyMyCompetitors;
        }
        else if (section == 1)
        {
            key = keyMyEvents;
        }
    }
    else if (competitors.count > 0)//4 - 0100
    {
        if (section == 0)
        {
            key = keyMyCompetitors;
        }
    }
    else if (toolsTraining.count > 0 && events.count > 0)//3 - 0011
    {
        if (section == 0)
        {
            key = keyMyToolsTraing;
        }
        else if (section == 1)
        {
            key = keyMyEvents;
        }
    }
    else if (toolsTraining.count > 0)//2 - 0010
    {
        if (section == 0)
        {
            key = keyMyToolsTraing;
        }
    }
    else if (events.count > 0)//1 - 0001
    {
        if (section == 0)
        {
            key = keyMyEvents;
        }
    }
    
    return key;
}

- (GlobalSearchSection)getGlobalSearchSectionEnumWithUISection:(NSInteger)section
{
    NSDictionary *myFranchise      = self.globalSearchDataSource[0];
    NSDictionary *myCompetitors    = self.globalSearchDataSource[1];
    NSDictionary *myToolsTraining  = self.globalSearchDataSource[2];
    NSDictionary *myEvents         = self.globalSearchDataSource[3];
    
    NSMutableArray *franchise      = myFranchise[keyMyFranchise];
    NSMutableArray *competitors    = myCompetitors[keyMyCompetitors];
    NSMutableArray *toolsTraining  = myToolsTraining[keyMyToolsTraing];
    NSMutableArray *events         = myEvents[keyMyEvents];
    
    GlobalSearchSection sectionEnum = MyFranchise;
    
    if (franchise.count > 0 && competitors.count > 0 && toolsTraining.count > 0 && events.count > 0) // 15 - 1111
    {
        if (section == 0)
        {
            sectionEnum = MyFranchise;
        }
        else if (section == 1)
        {
            sectionEnum = MyCompetitors;
        }
        else if (section == 2)
        {
            sectionEnum = MyToolsTraining;
        }
        else if (section == 3)
        {
            sectionEnum = MyEvents;
        }
    }
    else if (franchise.count > 0 && competitors.count > 0 && toolsTraining.count > 0) // 14 - 1110
    {
        if (section == 0)
        {
            sectionEnum = MyFranchise;
        }
        else if (section == 1)
        {
            sectionEnum = MyCompetitors;
        }
        else if (section == 2)
        {
            sectionEnum = MyToolsTraining;
        }

    }
    else if (franchise.count > 0 && competitors.count > 0 && events.count > 0) // 13 - 1101
    {
        if (section == 0)
        {
            sectionEnum = MyFranchise;
        }
        else if (section == 1)
        {
            sectionEnum = MyCompetitors;
        }
        else if (section == 2)
        {
            sectionEnum = MyEvents;
        }
    }
    else if (franchise.count > 0 && competitors.count > 0) // 12 - 1100
    {
        if (section == 0)
        {
            sectionEnum = MyFranchise;
        }
        else if (section == 1)
        {
            sectionEnum = MyCompetitors;
        }
    }
    else if (franchise.count > 0 && toolsTraining.count > 0 && events.count > 0) //11 - 1011
    {
        if (section == 0)
        {
            sectionEnum = MyFranchise;
        }
        else if (section == 1)
        {
            sectionEnum = MyToolsTraining;
        }
        else if (section == 2)
        {
            sectionEnum = MyEvents;
        }
    }
    else if (franchise.count > 0 && toolsTraining.count > 0) //10 - 1010
    {
        if (section == 0)
        {
            sectionEnum = MyFranchise;
        }
        else if (section == 1)
        {
            sectionEnum = MyToolsTraining;
        }
    }
    else if (franchise.count > 0 && events.count > 0)//9 - 1001
    {
        if (section == 0)
        {
            sectionEnum = MyFranchise;
        }
        else if (section == 1)
        {
            sectionEnum = MyEvents;
        }
    }
    else if (franchise.count > 0) //8 - 1000
    {
        if (section == 0)
        {
            sectionEnum = MyFranchise;
        }
    }
    else if (competitors.count > 0 && toolsTraining.count > 0 && events.count > 0) //7 - 0111
    {
        if (section == 0)
        {
            sectionEnum = MyCompetitors;
        }
        else if (section == 1)
        {
            sectionEnum = MyToolsTraining;
        }
        else if (section == 2)
        {
            sectionEnum = MyEvents;
        }
    }
    else if (competitors.count > 0 && toolsTraining.count > 0 )//6 - 0110
    {
        if (section == 0)
        {
            sectionEnum = MyCompetitors;
        }
        else if (section == 1)
        {
            sectionEnum = MyToolsTraining;
        }
    }
    else if (competitors.count > 0 && events.count > 0)//5 - 0101
    {
        if (section == 0)
        {
            sectionEnum = MyCompetitors;
        }
        else if (section == 1)
        {
            sectionEnum = MyEvents;
        }
    }
    else if (competitors.count > 0)//4 - 0100
    {
        if (section == 0)
        {
            sectionEnum = MyCompetitors;
        }
    }
    else if (toolsTraining.count > 0 && events.count > 0)//3 - 0011
    {
        if (section == 0)
        {
            sectionEnum = MyToolsTraining;
        }
        else if (section == 1)
        {
            sectionEnum = MyEvents;
        }
    }
    else if (toolsTraining.count > 0)//2 - 0010
    {
        if (section == 0)
        {
            sectionEnum = MyToolsTraining;
        }
    }
    else if (events.count > 0)//1 - 0001
    {
        if (section == 0)
        {
            sectionEnum = MyEvents;
        }
    }
    
    return sectionEnum;
}

- (BOOL)isDataSourceEmpty
{
    NSDictionary *myFranchise       = self.globalSearchDataSource[0];
    NSDictionary *myCompetitors     = self.globalSearchDataSource[1];
    NSDictionary *myToolsTraining   = self.globalSearchDataSource[2];
    NSDictionary *myEvents          = self.globalSearchDataSource[3];
    
    NSMutableArray *franchise   = myFranchise[keyMyFranchise];
    NSMutableArray *competitors = myCompetitors[keyMyCompetitors];
    NSMutableArray *toolsTraining = myToolsTraining[keyMyToolsTraing];
    NSMutableArray *events        = myEvents[keyMyEvents];
  
    if (franchise.count <= 0 && competitors.count <= 0 && toolsTraining.count <= 0 && events.count <= 0 )
    {
        return YES;
    }
    return NO;
}

- (BOOL)shouldHideTableOrCollectionView
{
    NSDictionary *myFranchise       = self.globalSearchDataSource[0];
    NSDictionary *myCompetitors     = self.globalSearchDataSource[1];
    NSDictionary *myToolsTraining   = self.globalSearchDataSource[2];
    NSDictionary *myEvents          = self.globalSearchDataSource[3];
    
    NSMutableArray *franchise   = myFranchise[keyMyFranchise];
    NSMutableArray *competitors = myCompetitors[keyMyCompetitors];
    NSMutableArray *toolsTraining = myToolsTraining[keyMyToolsTraing];
    NSMutableArray *events        = myEvents[keyMyEvents];
    
    if (franchise.count <= 0 && competitors.count <= 0 && toolsTraining.count <= 0 && events.count > 0 )
    {
        return YES;
    }
    return NO;
}

- (NSInteger)getBucketIdForSelectedTab:(NSInteger)selectedIndex
{
    if (selectedIndex == 0)
    {
        return [Constant sharedConstant].ProntoTabOptionFranchiseID.integerValue;
    }
    else if (selectedIndex == 1)
    {
        return [Constant sharedConstant].ProntoTabOptionCompetitorID.integerValue;
    }
    else if (selectedIndex == 2)
    {
        return [Constant sharedConstant].ProntoTabOptionToolsAndTrainingID.integerValue;
    }
    else if (selectedIndex == 3)
    {
        return [Constant sharedConstant].ProntoTabOptionEventID.integerValue;
    }
    return [Constant sharedConstant].ProntoTabOptionFranchiseID.integerValue;
}

- (NSString*)getCurrentActiveTabOptionAsStringForSelectedIndex:(NSInteger)currentTabIndex
{
    switch (currentTabIndex)
    {
        case 1:
            return ProntoTabOptionCompetitor;
            break;
        case 2:
            return ProntoTabOptionToolsAndTraining;
            break;
        case 3:
            return ProntoTabOptionEvent;
            break;
            
        default:
            return ProntoTabOptionFranchise;
            break;
    }
    return ProntoTabOptionFranchise;
}

@end
