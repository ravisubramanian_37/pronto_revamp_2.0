//
//  TTLibraryViewController.h
//  Pronto
//
//  Created by m-666346 on 23/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pronto-Swift.h"

@class TTLibraryViewModel;
@class TTLibraryViewController;
@class Asset;
@class FranchiseTableViewCell;


@protocol TTLibraryViewControllerDelegate<NSObject>
////[self openAssetFromListOrGrid: indexPath.row type:@"List"];
//- (void) openAssetFromListOrGrid:(NSInteger)row type:(NSString*)selectedType

- (void)ttLibraryViewController:(TTLibraryViewController*)ttLibraryViewController openAsset:(Asset*)asset fromListOrGridAtIndex:(NSInteger)index ofType:(NSString*)type;
- (void)ttLibraryViewController:(TTLibraryViewController*)ttLibraryViewController cell:(id)cell  configureExportPopOver:(UIButton*) sender;
- (void)ttLibraryViewController:(TTLibraryViewController*)ttLibraryViewController cell:(id)cell  configureInfoPopOver:(UIButton*) sender;
- (void)ttLibraryViewController:(TTLibraryViewController*)ttLibraryViewController cell:(id)cell  configureFavButton:(UIButton*) sender;
- (void)didRefreshControlSelected;

@end

@interface TTLibraryViewController : UIViewController
@property (nonatomic, strong) TTLibraryViewModel *ttLibraryViewModel;
@property (nonatomic, assign) id<TTLibraryViewControllerDelegate>ttLibraryVCDelegate;

@property (nonatomic, weak) IBOutlet UITableView *ttLibraryTableView;
@property (nonatomic, weak) IBOutlet UICollectionView *ttLibraryCollectionView;

+ (TTLibraryViewController*)GetTTLibraryViewController;
- (void) reloadTableViewOrCollectionView;
- (void) endRefreshControlRefreshing;
- (void) changeTintColor;

@property (nonatomic, strong) AudioPlayer *audioPlayerView;
@property (nonatomic, strong) Asset *assetForAudio;

@end
