//
//  TTLibraryViewController.m
//  Pronto
//
//  Created by m-666346 on 23/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "TTLibraryViewController.h"
#import "TTLibraryViewModel.h"
#import "FranchiseTableViewCell.h"
#import "OfflineDownload+CoreDataClass.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "ZMAbbvieAPI.h"
#import "Constant.h"
#import "AppDelegate.h"
#import "ZMUserActions.h"
#import "Asset.h"
#import "ZMAbbvieAPI.h"
#import "ZMProntoTheme.h"
#import "ViewUtils.h"
#import "AssetDownloadProvider.h"
#import "OfflineDownload+CoreDataClass.h"
#import "OfflineDownload+CoreDataProperties.h"
#import "Pronto-Swift.h"

//Remove not required header



CGFloat gridItemWidth = 232.0;

static NSString * const TTLibraryViewStoryboardName = @"TTLibraryView";
static NSString * const TTLibraryViewControllerIdentifier = @"TTLibraryViewController";

static NSString * const TTLibraryViewTypeTableView = @"List";
static NSString * const TTLibraryViewTypeCollectionView = @"TTLibraryView";


@interface TTLibraryViewController ()<UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

//@property (nonatomic, weak) IBOutlet UITableView *ttLibraryTableView;
//@property (nonatomic, weak) IBOutlet UICollectionView *ttLibraryCollectionView;
@property(nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, assign) NSInteger previousAssetCount;
@property(nonatomic, strong) NSArray * initialDownloadDbArray;


@end

@implementation TTLibraryViewController

+ (TTLibraryViewController*)GetTTLibraryViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:TTLibraryViewStoryboardName bundle:nil];
    TTLibraryViewController *ttLibraryVC = [sb instantiateViewControllerWithIdentifier:TTLibraryViewControllerIdentifier];
    return ttLibraryVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeData];
    [self setupUI];
}

- (void)styleLibrary
{
    [[ZMProntoTheme sharedTheme] styleMainView:self.view];
}

/**
 *  Long press handler
 *  @param gestureRecognizer gestureRecognizer event
 */
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
}

-(void)configureGridView
{
    [self styleLibrary];
    
    if (self.refreshControl != nil)
    {
        self.refreshControl = nil;
    }
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshAssets:)
                 forControlEvents:UIControlEventValueChanged];
    
    self.ttLibraryCollectionView.alwaysBounceVertical = YES;
    self.ttLibraryCollectionView.allowsSelection = YES;
    [self.ttLibraryCollectionView addSubview:self.refreshControl];
    [self.ttLibraryCollectionView setDataSource:self];
    [self.ttLibraryCollectionView setDelegate:self];
    self.ttLibraryCollectionView.hidden = NO;
//    self.ttLibraryCollectionView.backgroundColor = [UIColor clearColor];

    if (self.longPress == nil)
    {
        self.longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        self.longPress.minimumPressDuration = .5;
        [self.ttLibraryCollectionView addGestureRecognizer:self.longPress];
    }
    
    if (self.tapGestureRecognizer == nil)
    {
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
        [self.ttLibraryCollectionView addGestureRecognizer:self.tapGestureRecognizer];
        self.tapGestureRecognizer.cancelsTouchesInView = NO;
    }
    
    /*
     headerView = (ZMHeader *)[[[NSBundle mainBundle] loadNibNamed:@"Header" owner:self options:nil] objectAtIndex:0];
     [headerView initWithOptions:header];
    // If you want to have full selection list turn showResumeSelection to NO
    headerView.showResumeSelection = YES;
    headerView.library = self;
     */
    if(self.ttLibraryViewModel.ttLibraryAssets.count > 0)
    {
        NSIndexPath* top = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.ttLibraryCollectionView scrollToItemAtIndexPath:top atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
    
    self.ttLibraryTableView.hidden = YES;
}
- (void)hideKeyboard
{
    [self.view endEditing:YES];
}
- (void) configureListView
{
    if (self.refreshControl != nil)
    {
        [self.refreshControl removeFromSuperview];
        self.refreshControl = nil;
    }
    
    [self styleLibrary];
    [self.ttLibraryTableView setDelegate: self];
    [self.ttLibraryTableView setDataSource: self];
    self.ttLibraryTableView.hidden = NO;
//    self.ttLibraryTableView.backgroundColor = [UIColor clearColor];

    self.ttLibraryCollectionView.hidden = YES;
    
    // adding the refresh control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshAssets:)
                  forControlEvents:UIControlEventValueChanged];
    [self.ttLibraryTableView addSubview:self.refreshControl];
    
    NSIndexPath* top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
    [self.ttLibraryTableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    /** If you want to have full selection list turn showResumeSelection to NO */
//    self.too.showResumeSelection = YES;
//    headerView.library = self;
}

-(void) changeTintColor
{
    self.refreshControl.tintColor = [UIColor clearColor];
}

- (void)refreshAssets:(id)sender
{
    if ([(UIRefreshControl *)sender respondsToSelector:@selector(endRefreshing)])
    {
        [(UIRefreshControl *)sender endRefreshing];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Off_Start"];
    if (![[ZMAbbvieAPI sharedAPIManager] _syncingInProgress])
    {
        if (self.ttLibraryVCDelegate != nil &&
            [self.ttLibraryVCDelegate respondsToSelector:@selector(didRefreshControlSelected)])
        {
            [self.ttLibraryVCDelegate didRefreshControlSelected];
        }
    }
}

- (void)showListOrGridView
{
    if([[[NSUserDefaults standardUserDefaults] objectForKey:keyUserDefaultGridFranchise] isEqualToString:@"NO"] || ![[NSUserDefaults standardUserDefaults] objectForKey:keyUserDefaultGridFranchise] )
    {
        [self configureListView];
    }
    else
    {
        //Configure Grid View
        [self configureGridView];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [ZMUserActions sharedInstance].currentSelectedTabIndex = 2;
    [self showListOrGridView];
}


- (void)setupUI
{
    [self registerTableViewCell];
    [self registerCollectionViewCell];
    self.ttLibraryTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.ttLibraryCollectionView.dataSource = self;
    self.ttLibraryCollectionView.delegate   = self;
//    self.ttLibraryTableView.backgroundColor = [UIColor clearColor];
//    self.ttLibraryCollectionView.backgroundColor = [UIColor clearColor];
}

- (void)initializeData
{
    self.ttLibraryViewModel = [[TTLibraryViewModel alloc]initWithDelegate:(id<TTLibraryViewModelDelegate>)self];
}

- (void)registerCollectionViewCell
{
    [self.ttLibraryCollectionView registerNib:[UINib nibWithNibName:[ZMGridItem GetGridItemCellNibName] bundle:nil]
                   forCellWithReuseIdentifier:[ZMGridItem GetGridItemCellIdentifier]];
}
- (void)registerTableViewCell
{
    [self.ttLibraryTableView registerNib:[UINib nibWithNibName:[FranchiseTableViewCell GetFranchiseTableViewCellNibName] bundle:nil]
                       forCellReuseIdentifier:[FranchiseTableViewCell GetFranchiseTableViewCellIdentifier]];
}

//Offline download main function
-(void) offlineDownloadOfAssets
{
    __strong NSMutableArray* assetsPageDuplicate;
    assetsPageDuplicate = [[NSMutableArray alloc]init];
    [assetsPageDuplicate addObjectsFromArray:self.ttLibraryViewModel.ttLibraryAssets];
    // is it in background
    dispatch_async(dispatch_get_main_queue(), ^{
        _initialDownloadDbArray = [[ZMCDManager sharedInstance]getOfflineDownloadArray];
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        //If coredata not present
        if (self.initialDownloadDbArray.count != 0) {
            
            //Add assets which are newly added
            for (Asset * asset in assetsPageDuplicate) {
                bool dataPresent = NO;
                for(OfflineDownload * data in _initialDownloadDbArray) {
                    if ([[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"]) {
                        if(asset.assetID == data.assetID) {
                            dataPresent = YES;
                            break;
                        }
                    }
                    else {
                        dataPresent = YES;
                        break;
                    }
                }
                if(!dataPresent)
                {
                    [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
                    [initialDownloadAsset setObject:@"in_progress" forKey:@"status"];
                    [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                    
                    //Downloading the Asset added new
                    NSNumber * bucketId = [[ZMUserActions sharedInstance] getBucketID:asset];
                    if(asset != nil && [[bucketId stringValue] isEqualToString:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]])
                    {
                        [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
                    }
                }
            }
            
            //Download the data which is in progress
            for(OfflineDownload * data in _initialDownloadDbArray)
            {
                if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"] || [data.status isEqualToString:@"failed"]) {
                    
                    //Download initiate for failed assets
                    if([data.status isEqualToString:@"completed_partialy"] || [data.status isEqualToString:@"failed"]) {
                        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:data.assetID ];
                        [initialDownloadAsset setObject:data.assetID forKey:@"assetID"];
                        [initialDownloadAsset setObject:@"in_progress" forKey:@"status"];
                        if (offlineData.count > 0)
                        {
                            //if  insertation and deletetion has to be happen, it will happen one by one
                            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                            }];
                        }
                        else
                        {
                            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
                        }
                    }
                    
                    NSArray * assetVal = [[ZMCDManager sharedInstance]getAssetArrayById:data.assetID.stringValue];
                    if (assetVal.count > 0)
                    {
                        Asset * assetCache = assetVal[0];
                        NSNumber * bucketId = [[ZMUserActions sharedInstance] getBucketID:assetCache];
                        if(assetCache != nil && [[bucketId stringValue] isEqualToString:[[Constant sharedConstant] getCurrentActiveTabOptionIDAsString]])
                        {
                            [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:assetCache];
                        }
                        
                    }
                }
            }
        }
        //If coredata not present or first download
        else
        {
            NSMutableArray * assetArrayToDownload = [[NSMutableArray alloc]init];
            for (Asset * asset in assetsPageDuplicate) {
                if ([[[asset.uri_full pathExtension] lowercaseString] isEqualToString:@"pdf"]) {
                    [assetArrayToDownload addObject:asset];
                }
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset:%@ uri:%@",asset.assetID,asset.uri_full]];
            }
            [[ZMAbbvieAPI sharedAPIManager] offlineDownload:assetArrayToDownload];
            for(Asset * asset in assetArrayToDownload)
            {
                [[AssetDownloadProvider sharedAssetDownloadProvider] submitRequestToDownloadAsset:asset];
            }
        }
    });
}

-(void) backgroundDownload
{
    if(([self.ttLibraryViewModel.ttLibraryAssets count] > 0) &&
       [[[NSUserDefaults standardUserDefaults] objectForKey:@"Off_Start"] isEqualToString:@"NO"])
    {
        [self offlineDownloadOfAssets];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Off_Start"];
    }
    else if ((self.previousAssetCount != [self.ttLibraryViewModel.ttLibraryAssets count]) && ([self.ttLibraryViewModel.ttLibraryAssets count] > 0))
    {
        [self offlineDownloadOfAssets];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Off_Start"];
    }
}

#pragma mark - Table View Data Source and Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self backgroundDownload];
    self.previousAssetCount = [self.ttLibraryViewModel.ttLibraryAssets count];
    return self.ttLibraryViewModel.ttLibraryAssets.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Revamp(26/12) - To adjust left table view cell height;
//    return 96 + 5; //heightToIncrease = 5 as of now
    
    FranchiseTableViewCell *cell = (FranchiseTableViewCell *) [tableView dequeueReusableCellWithIdentifier:[FranchiseTableViewCell GetFranchiseTableViewCellIdentifier]];
    Asset * asset = [self.ttLibraryViewModel.ttLibraryAssets objectAtIndex:indexPath.row];
    return [cell getHeightOfCell:asset atIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        // Pronto new revamping changes
        //heightToIncrease = 0; // Do we need to increase the height here
    
        FranchiseTableViewCell *cell = (FranchiseTableViewCell *) [tableView dequeueReusableCellWithIdentifier:[FranchiseTableViewCell GetFranchiseTableViewCellIdentifier]];
    
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:[FranchiseTableViewCell GetFranchiseTableViewCellNibName]
                                                         owner:self options:nil];
            
            [tableView registerNib:[UINib nibWithNibName:[FranchiseTableViewCell GetFranchiseTableViewCellNibName] bundle:nil] forCellReuseIdentifier:[FranchiseTableViewCell GetFranchiseTableViewCellIdentifier]];
            cell = (FranchiseTableViewCell*)[nib objectAtIndex:0];
        }
    cell.delegate = (id <ZMListItemDelegate>)self;
        if ([self.ttLibraryViewModel.ttLibraryAssets count] > 0 &&
            indexPath.row < [self.ttLibraryViewModel.ttLibraryAssets count])
        {
            Asset * asset = [self.ttLibraryViewModel.ttLibraryAssets objectAtIndex:indexPath.row];
            
            cell.audioPlayerView.hidden = YES;
            if(asset.assetID == self.assetForAudio.assetID)
            {
                cell.audioPlayerView.hidden = NO;
                [self.audioPlayerView removeFromSuperview];
                self.audioPlayerView.frame = CGRectMake(0, 0,cell.audioPlayerView.frame.size.width, cell.audioPlayerView.frame.size.height);
                [cell.audioPlayerView addSubview:self.audioPlayerView];
            }
            
            [cell prepareCellWithAsset:asset atIndexPath:indexPath];
        }
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated: YES];
//    [self openAssetFromListOrGrid: indexPath.row type:@"List"];
    
    if (self.ttLibraryVCDelegate != nil &&
        [self.ttLibraryVCDelegate respondsToSelector:@selector(ttLibraryViewController:openAsset:fromListOrGridAtIndex:ofType:)])
    {
        if ([self.ttLibraryViewModel.ttLibraryAssets count] > 0 && indexPath.row < [self.ttLibraryViewModel.ttLibraryAssets count])
        {
            
            Asset * selectedAsset = [self.ttLibraryViewModel.ttLibraryAssets objectAtIndex:indexPath.row];
            [self.ttLibraryVCDelegate ttLibraryViewController:self
                                                    openAsset:selectedAsset
                                        fromListOrGridAtIndex:indexPath.row
                                                       ofType:TTLibraryViewTypeTableView];
        }
    }
}

#pragma mark - Collection View Data Source and Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //Offline download on app stop and run
    [self backgroundDownload];
    self.previousAssetCount = [self.ttLibraryViewModel.ttLibraryAssets count];
    return self.ttLibraryViewModel.ttLibraryAssets.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZMGridItem *cell = (ZMGridItem*)[collectionView dequeueReusableCellWithReuseIdentifier:[ZMGridItem GetGridItemCellIdentifier]
                                                                                          forIndexPath:indexPath];
    if ([self.ttLibraryViewModel.ttLibraryAssets count] > 0 &&
        indexPath.row < [self.ttLibraryViewModel.ttLibraryAssets count] )
    {
        
        Asset * asset = [self.ttLibraryViewModel.ttLibraryAssets objectAtIndex:indexPath.row];
        [cell prepareCellWithAsset:asset atIndexPath:indexPath];
        cell.delegate = (id<ZMGridItemDelegate>)self;
        
        cell.audioPlayerView.hidden = YES;
        if(asset.assetID == self.assetForAudio.assetID)
        {
            cell.audioPlayerView.hidden = NO;
            [self.audioPlayerView removeFromSuperview];
            self.audioPlayerView.frame = CGRectMake(0, 0,cell.audioPlayerView.frame.size.width, cell.audioPlayerView.frame.size.height);
            [cell.audioPlayerView addSubview:self.audioPlayerView];
        }

    }
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //        [self openAssetFromListOrGrid: indexPath.item type:@"Grid"];
    ZMGridItem *cell = (ZMGridItem*)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell.type == ZMGridItemTypeAsset)
    {
        if (self.ttLibraryVCDelegate != nil &&
            [self.ttLibraryVCDelegate respondsToSelector:@selector(ttLibraryViewController:openAsset:fromListOrGridAtIndex:ofType:)])
        {
            if ([self.ttLibraryViewModel.ttLibraryAssets count] > 0 && indexPath.row < [self.ttLibraryViewModel.ttLibraryAssets count])
            {
                
                Asset * selectedAsset = [self.ttLibraryViewModel.ttLibraryAssets objectAtIndex:indexPath.row];
                [self.ttLibraryVCDelegate ttLibraryViewController:self
                                                        openAsset:selectedAsset
                                            fromListOrGridAtIndex:indexPath.row
                                                           ofType:TTLibraryViewTypeCollectionView];
            }
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat itemWidth = 232.0;
//    if (([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait) ||
//       ([[UIApplication sharedApplication] statusBarOrientation]  == UIDeviceOrientationPortraitUpsideDown) )
//    {
//        itemWidth = self.ttLibraryCollectionView.width / 2 - 10;
//    }
//    else
//    {
//        itemWidth = self.ttLibraryCollectionView.width / 3 - 10;
//    }
    
    return CGSizeMake(itemWidth, 160);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 10, 5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - helper Methods
- (void) reloadTableViewOrCollectionView
{
    if (self.ttLibraryTableView.hidden == NO) // Showing Table View
    {
        [self.ttLibraryTableView reloadData];
        [self configureListView];
    }
    else // Showing Collection View
    {
        [self.ttLibraryCollectionView reloadData];
        [self configureGridView];
    }
}

- (void) endRefreshControlRefreshing
{
    if (self.ttLibraryTableView.hidden == NO) // Showing Table View
    {
        if (self.refreshControl != nil && [self.refreshControl respondsToSelector:@selector(endRefreshing)])
        {
            [self.refreshControl endRefreshing];
        }
    }
    else // Showing Collection View
    {
        if (self.refreshControl != nil && [self.refreshControl respondsToSelector:@selector(endRefreshing)])
        {
            [self.refreshControl endRefreshing];
        }
    }
}

#pragma mark - ZMListItem Delegate Methods
- (void)cell:(FranchiseTableViewCell*)cell  configureExportPopOver:(UIButton*) sender
{
    if (self.ttLibraryVCDelegate != nil &&
        [self.ttLibraryVCDelegate respondsToSelector:@selector(ttLibraryViewController:cell:configureExportPopOver:)])
    {
        [self.ttLibraryVCDelegate ttLibraryViewController:self cell:cell configureExportPopOver:sender];
    }
}

- (void)cell:(FranchiseTableViewCell*)cell  configureInfoPopOver:(UIButton*) sender
{
    if (self.ttLibraryVCDelegate != nil &&
        [self.ttLibraryVCDelegate respondsToSelector:@selector(ttLibraryViewController:cell:configureInfoPopOver:)])
    {
        [self.ttLibraryVCDelegate ttLibraryViewController:self cell:cell configureInfoPopOver:sender];
    }
}

- (void)cell:(FranchiseTableViewCell*)cell  configureFavButton:(UIButton*) sender
{
    if (self.ttLibraryVCDelegate != nil &&
        [self.ttLibraryVCDelegate respondsToSelector:@selector(ttLibraryViewController:cell:configureFavButton:)])
    {
        [self.ttLibraryVCDelegate ttLibraryViewController:self cell:cell configureFavButton:sender];
    }
}

#pragma mark - ZMGridItemDelegate Delegate Methods
- (void)gridCell:(ZMGridItem*)cell  configureExportPopOver:(UIButton*) sender
{
    if (self.ttLibraryVCDelegate != nil &&
        [self.ttLibraryVCDelegate respondsToSelector:@selector(ttLibraryViewController:cell:configureExportPopOver:)])
    {
        [self.ttLibraryVCDelegate ttLibraryViewController:self cell:cell configureExportPopOver:sender];
    }
}

- (void)gridCell:(ZMGridItem*)cell  configureInfoPopOver:(UIButton*) sender
{
    if (self.ttLibraryVCDelegate != nil &&
        [self.ttLibraryVCDelegate respondsToSelector:@selector(ttLibraryViewController:cell:configureInfoPopOver:)])
    {
        [self.ttLibraryVCDelegate ttLibraryViewController:self cell:cell configureInfoPopOver:sender];
    }
}

- (void)gridCell:(ZMGridItem*)cell  configureFavButton:(UIButton*) sender
{
    if (self.ttLibraryVCDelegate != nil &&
        [self.ttLibraryVCDelegate respondsToSelector:@selector(ttLibraryViewController:cell:configureFavButton:)])
    {
        [self.ttLibraryVCDelegate ttLibraryViewController:self cell:cell configureFavButton:sender];
    }
}

@end
