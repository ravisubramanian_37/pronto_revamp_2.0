//
//  TTLibraryViewModel.h
//  Pronto
//
//  Created by m-666346 on 23/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TTLibraryViewModelDelegate <NSObject>
@end

@interface TTLibraryViewModel : NSObject

@property (nonatomic, strong) NSMutableArray *ttLibraryAssets;
@property(nonatomic, assign) id<TTLibraryViewModelDelegate> ttLibraryVMDelegate;

- (instancetype)initWithDelegate:(id<TTLibraryViewModelDelegate>)delegate;
@end
