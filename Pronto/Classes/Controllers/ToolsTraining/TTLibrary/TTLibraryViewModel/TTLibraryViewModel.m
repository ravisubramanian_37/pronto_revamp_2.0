//
//  TTLibraryViewModel.m
//  Pronto
//
//  Created by m-666346 on 23/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "TTLibraryViewModel.h"

@implementation TTLibraryViewModel

- (instancetype)initWithDelegate:(id<TTLibraryViewModelDelegate>)delegate
{
    if (self = [super init])
    {
        self.ttLibraryVMDelegate    = delegate;
        self.ttLibraryAssets        = [NSMutableArray array];
    }
    return self;
}

@end
