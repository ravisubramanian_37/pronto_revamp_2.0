//
//  TTCategoryMenuVC.h
//  Pronto
//
//  Created by m-666346 on 19/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>


@class TTCategoryMenuViewModel;

@protocol TTCategoryMenuVCDelegate<NSObject>
- (void)didUpdatedDataSource;
- (void)stopPlayingAudio;
@end

@interface TTCategoryMenuVC : UIViewController
@property(nonatomic, strong) TTCategoryMenuViewModel *ttCategoryViewModel;
@property (nonatomic, assign) id<TTCategoryMenuVCDelegate> ttCategoryMenuVCDelegate;
@property (weak, nonatomic) IBOutlet UITableView *ttCategoryMenuTableView;

+ (TTCategoryMenuVC*)GetTTCategoryMenuViewController;
- (void)updateCategoryMenuTableView;
- (void)reloadTableView;
- (void)updateTTCategoryFavoriteCell;
- (void)showBouncingEffectOnFavouriteCountLabel;

@end
