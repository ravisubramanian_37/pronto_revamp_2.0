//
//  TTCategoryMenuVC.m
//  Pronto
//
//  Created by m-666346 on 19/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "TTCategoryMenuVC.h"
#import "TTCategoryMenuViewModel.h"
#import "TTCategoryMenuCell.h"
#import "ZMUserActions.h"
#import "Pronto-Swift.h"
#import "Constant.h"

typedef NS_ENUM(NSInteger, UserSelection)
{
    AllToolsTraingAssets = 0,
    ToolsTraingFavoriteAssets,
    Other
};

static NSString * const TTCategoryMenuStoryboardName = @"TTCategoryMenu";
static NSString * const TTCategoryMenuControllerIdentifier = @"TTCategoryMenuVC";

@interface TTCategoryMenuVC ()<TTCategoryMenuViewModelDelegate>

@end

@implementation TTCategoryMenuVC

+ (TTCategoryMenuVC*)GetTTCategoryMenuViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:TTCategoryMenuStoryboardName bundle:nil];
    TTCategoryMenuVC *ttCategoryMenuVC = [sb instantiateViewControllerWithIdentifier:TTCategoryMenuControllerIdentifier];
    return ttCategoryMenuVC;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeData];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.ttCategoryViewModel.toolsTrainingCategoryArray == nil || self.ttCategoryViewModel.toolsTrainingCategoryArray.count <= 0)
    {
        [self.ttCategoryViewModel prepareCategoryData];
    }
}

- (void)setupUI
{
    [self registerTableViewCell];
    self.ttCategoryMenuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)initializeData
{
    self.ttCategoryViewModel = [[TTCategoryMenuViewModel alloc]initWithDelegate:(id<TTCategoryMenuViewModelDelegate>)self];
}

- (void)registerTableViewCell
{
    [self.ttCategoryMenuTableView registerNib:[UINib nibWithNibName:[TTCategoryMenuCell GetTTCategoryMenuCellNibName] bundle:nil]
                       forCellReuseIdentifier:[TTCategoryMenuCell GetTTCategoryMenuCellIdentifier]];
}

- (void)reloadTableView {
    [self.ttCategoryMenuTableView reloadData];
}

# pragma mark -  TableView Data Source And Delegate Methods
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTCategoryMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:[TTCategoryMenuCell GetTTCategoryMenuCellIdentifier]];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:[TTCategoryMenuCell GetTTCategoryMenuCellNibName] owner:self options:nil];
        [tableView registerNib:[UINib nibWithNibName:[TTCategoryMenuCell GetTTCategoryMenuCellNibName] bundle:nil]
        forCellReuseIdentifier:[TTCategoryMenuCell GetTTCategoryMenuCellIdentifier]];
        cell = (TTCategoryMenuCell*)[nib objectAtIndex:0];
    }
    
    // Customize cell // check for less than one
    if (self.ttCategoryViewModel.toolsTrainingCategoryArray != nil &&
        self.ttCategoryViewModel.toolsTrainingCategoryArray.count >= indexPath.row)
    {
        NSDictionary *category = nil;
        category = self.ttCategoryViewModel.toolsTrainingCategoryArray[indexPath.row];
        [cell prepareCellWithCategory:category atIndexPath:indexPath];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat heightValue = [TTCategoryMenuCell DefaultHeightOfCell];
    if(indexPath.row <= 1)
    {
        heightValue = heightValue + [TTCategoryMenuCell HeightOfCellTitle];
    }
    return heightValue;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.ttCategoryViewModel.toolsTrainingCategoryArray.count;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTCategoryMenuCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == AllToolsTraingAssets)
    {
        [AbbvieLogging logInfo:@"All Assets selected"];
        [ZMUserActions sharedInstance].selectLeftMenu = ProntoTabOptionToolsAndTrainingAllAssets;
        // All Asset cell can't be deselected unless select other cells.
        [self.ttCategoryViewModel prepareCategoryDataWhenAllAssetSelected];
    }
    else if (indexPath.row == ToolsTraingFavoriteAssets)
    {
        [AbbvieLogging logInfo:@"Favorite Assets selected"];
        [ZMUserActions sharedInstance].selectLeftMenu = ProntoTabOptionToolsAndTrainingFavoriteAssets;
        // If favorite gets selected all other cells will be deselected
        [self.ttCategoryViewModel prepareFavoriteCategoryDataFor:cell.currentCategory];
    }
    else
    {
        [AbbvieLogging logInfo:@"Other Category selected"];
        //Other than all asset and favorite assets cell, Multiple selection allowed.
        [self.ttCategoryViewModel prepareCategoryDataForCategory:cell.currentCategory];
    }
    /*  Instead of reloading single cell, loading table view
        It will update the previous cell as well as per data. Not a heavy task */
    
    [self.ttCategoryMenuTableView reloadData];
}
- (void)updateCategoryMenuTableView
{
    [self.ttCategoryMenuTableView reloadData];
}

# pragma mark - TTCategoryMenuViewModelDelegate Methods
- (void)dataSourceUpdated
{
    [AbbvieLogging logInfo:@"Data source got updated"];
    // Notifying and Tools and Training Controller to update the Tools and Training Library View.
    [self.ttCategoryMenuTableView reloadData];
    if (self.ttCategoryMenuVCDelegate != nil &&
        [self.ttCategoryMenuVCDelegate respondsToSelector:@selector(didUpdatedDataSource)])
    {
        [self.ttCategoryMenuVCDelegate didUpdatedDataSource];
    }
    
    if (self.ttCategoryMenuVCDelegate != nil &&
        [self.ttCategoryMenuVCDelegate respondsToSelector:@selector(stopPlayingAudio)])
    {
        [self.ttCategoryMenuVCDelegate stopPlayingAudio];
    }
}

- (void)updateTTCategoryFavoriteCell
{
    //2nd Row is Favorite always
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.ttCategoryMenuTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)showBouncingEffectOnFavouriteCountLabel
{
    //2nd Row is Favorite always
    NSIndexPath *favouriteIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    TTCategoryMenuCell *cell = [self.ttCategoryMenuTableView cellForRowAtIndexPath:favouriteIndexPath];
    [cell showBouncingEffectOnFavouriteCountLabel];
}
@end
