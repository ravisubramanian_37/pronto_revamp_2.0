//
//  TTCategoryMenuCellTableViewCell.h
//  Pronto
//
//  Created by m-666346 on 19/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "CategoryMenuCell.h"

@interface TTCategoryMenuCell :UITableViewCell

@property(nonatomic, strong) NSDictionary *currentCategory;

+ (NSString*)GetTTCategoryMenuCellIdentifier;
+ (NSString*)GetTTCategoryMenuCellNibName;
+ (CGFloat)DefaultHeightOfCell;
+ (CGFloat)HeightOfCellTitle;

- (void)prepareCellWithCategory:(NSDictionary*)category atIndexPath:(NSIndexPath*)indexPath;
- (void)selectDeselectCategoriesImageViewWithCategory:(NSDictionary*)category;
- (void)showBouncingEffectOnFavouriteCountLabel;
@end
