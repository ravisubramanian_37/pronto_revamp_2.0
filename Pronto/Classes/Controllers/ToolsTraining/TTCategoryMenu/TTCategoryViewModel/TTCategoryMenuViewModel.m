//
//  TTCategoryMenuViewModel.m
//  Pronto
//
//  Created by m-666346 on 19/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "TTCategoryMenuViewModel.h"
#import "Pronto-Swift.h"
#import "Categories.h"
#import <MagicalRecord/MagicalRecord.h>
#import "ZMUserActions.h"
#import "Filter.h"
#import "Constant.h"
#import "FavFranchiseAsset.h"
#import "Constant.h"



@implementation TTCategoryMenuViewModel

- (instancetype)initWithDelegate:(id<TTCategoryMenuViewModelDelegate>)delegate
{
    if (self = [super init])
    {
        self.ttCategorryVMDelegate      = delegate;
        self.toolsTrainingCategoryArray = [NSMutableArray array];
        self.currentFilter              = [[Filter alloc]init];
    }
    return self;
}

- (void)prepareCategoryData
{
    
    [[ZMCDManager sharedInstance]getCategories:^(NSArray * data) {
        
        [[ZMCDManager sharedInstance] getActiveCategoriesWithCategories:data withCompletionHandler:^(NSArray *categoriesToFilter) {
            
             self.toolsTrainingCategoryArray = [NSMutableArray arrayWithArray:categoriesToFilter];
             [ZMUserActions sharedInstance].categoryTools = (NSMutableArray*)self.toolsTrainingCategoryArray;
             [self prepareCategoryDataWhenAllAssetSelected];
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"catDict:%@",[ZMUserActions sharedInstance].categoryTools]];
         }];
        
        
    }];
}


- (void)prepareSelectedCategoryData
{
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *dict in self.toolsTrainingCategoryArray)
    {
        if ([dict[@"selected"] isEqualToString:@"1"])
        {
            [tempArray addObject:dict[@"id"]];
        }
    }
    
    self.currentFilter.selectedCategories = tempArray;
}

- (void)prepareCategoryDataWhenAllAssetSelected
{
    self.selectedLefteMenu = ProntoTabOptionToolsAndTrainingAllAssets;
    NSString *allAssetTagName = [Constant GetAllAssetTagName];
    NSMutableArray *tempArray = [NSMutableArray array];
    NSMutableArray *tempIdsArray = [NSMutableArray array];
    
    for (NSDictionary *dict in self.toolsTrainingCategoryArray)
    {
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
        
        if ([dict[@"name"] isEqualToString:allAssetTagName])
        {
            tempDict[@"selected"] = @"1";
            [tempIdsArray addObject:dict[@"id"]];
        }
        else
        {
            tempDict[@"selected"] = @"0";
        }
        
        [tempArray addObject:tempDict];
    }
    
    self.toolsTrainingCategoryArray         = tempArray;
//    self.currentFilter.selectedCategories   = tempIdsArray;
    self.currentFilter.selectedCategories   = @[];
    // If all asset has been selected then favorite id will be empty
    self.currentFilter.favoriteIdsArray = @[];
    if (self.ttCategorryVMDelegate != nil && [self.ttCategorryVMDelegate respondsToSelector:@selector(dataSourceUpdated)])
    {
        [self.ttCategorryVMDelegate dataSourceUpdated];
    }
//    self.selectedLefteMenu = ProntoTabOptionToolsAndTrainingAllAssets;
}

- (void)prepareFavoriteCategoryDataFor:(NSDictionary*)category
{
    if ([category[@"selected"] isEqualToString:@"1"]) //It meanse Favorite is already selected
    {
        //Then select all assets
        [self prepareCategoryDataWhenAllAssetSelected];
    }
    else
    {
        //Adding Categories
        self.selectedLefteMenu = ProntoTabOptionToolsAndTrainingFavoriteAssets;
        NSMutableArray *catArray = [NSMutableArray array];
        NSString *allAssetTagName = [Constant GetAllAssetTagName];
        NSString *favoriteAssetTagName  = [Constant GetFavoritesAssetTagName];
        for (NSDictionary *dict in self.toolsTrainingCategoryArray)
        {
            if ([dict[@"selected"] isEqualToString:allAssetTagName])
            {
                [catArray addObject:dict[@"id"]];
            }
            if ([dict[@"name"] isEqualToString:favoriteAssetTagName])
            {
                [catArray addObject:dict[@"id"]];
            }
        }
        self.currentFilter.selectedCategories = catArray;
        //Adding Favourite ids
        NSString *currentBucketName = [Constant GetCurrentActiveTabOptionAsString];
        NSArray *favoritefranchise = [[ZMCDManager sharedInstance] getFavFranchise];
        NSMutableArray *favArray = [[NSMutableArray alloc]init];
        for(FavFranchise *fav in favoritefranchise)
        {
            BOOL isPresent  = [Constant doesContainSubstring:currentBucketName inString:fav.bucketName];
            if (isPresent  && fav.asset_ID != nil)
            {
                [favArray addObject:fav.asset_ID];
            }
        }
        
        self.currentFilter.favoriteIdsArray = favArray;
        //For Selection
        NSMutableArray *tempArray       = [NSMutableArray array];
        for (NSDictionary *dict in self.toolsTrainingCategoryArray)
        {
            NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
            if ([dict[@"name"] isEqualToString:favoriteAssetTagName])
            {
                tempDict[@"selected"] = @"1";
            }
            else
            {
                tempDict[@"selected"] = @"0";
            }
            [tempArray addObject:tempDict];
        }
        
        self.toolsTrainingCategoryArray         = tempArray;
        // If Favorite asset has been selected then selectedCategories id will be empty
//        self.currentFilter.selectedCategories   = @[];
//        self.currentFilter.favoriteIdsArray = @[]; //Get all favorite id's from MCDManager
        if (self.ttCategorryVMDelegate != nil && [self.ttCategorryVMDelegate respondsToSelector:@selector(dataSourceUpdated)])
        {
            [self.ttCategorryVMDelegate dataSourceUpdated];
        }
//        self.selectedLefteMenu = ProntoTabOptionToolsAndTrainingFavoriteAssets;
    }
}

- (void)prepareCategoryDataForCategory:(NSDictionary*)category
{
    NSString *favoriteAssetTagName  = [Constant GetFavoritesAssetTagName];
    NSString *allAssetTagName       = [Constant GetAllAssetTagName];
    
    NSMutableArray *tempArray       = [NSMutableArray array];
    for (NSDictionary *dict in self.toolsTrainingCategoryArray)
    {
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
        
        if ([dict[@"name"] isEqualToString:category[@"name"]])
        {
            if ([category[@"selected"] isEqualToString:@"1"])
            {
                tempDict[@"selected"] = @"0";
            }
            else
            {
                tempDict[@"selected"] = @"1";
            }
        }
        else if (([dict[@"name"] isEqualToString: allAssetTagName] ||
                  [dict[@"name"] isEqualToString: favoriteAssetTagName]))
        {
            // Restting the value of All assets and favorite assets dictionary value
            tempDict[@"selected"] = @"0";
        }
        
        [tempArray addObject:tempDict];
    }
    
    self.toolsTrainingCategoryArray = tempArray;
    
    //Getting selected category name
    NSMutableArray *tempIdsArray    = [NSMutableArray array];
    for (NSDictionary *dict in self.toolsTrainingCategoryArray)
    {
        /* Getting id's of selected category which would be other than
            All Assets and favorite asset */
        
        if (([dict[@"name"] isEqualToString: allAssetTagName] &&
            [dict[@"name"] isEqualToString: favoriteAssetTagName]) == NO)
        {
            if ([dict[@"selected"] isEqualToString:@"1"])
            {
                [tempIdsArray addObject:dict[@"id"]];
            }
        }
    }
    
    if (tempIdsArray.count > 0) // It means there is catogory selection
    {
        self.currentFilter.selectedCategories = tempIdsArray;
        
        if (self.ttCategorryVMDelegate != nil && [self.ttCategorryVMDelegate respondsToSelector:@selector(dataSourceUpdated)])
        {
            [self.ttCategorryVMDelegate dataSourceUpdated];
        }
    }
    else
    {
        //selects the All categories.
        [self prepareCategoryDataWhenAllAssetSelected];
    }
}

@end













