//
//  TTCategoryMenuViewModel.h
//  Pronto
//
//  Created by m-666346 on 19/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Filter;

@protocol TTCategoryMenuViewModelDelegate <NSObject>
- (void)dataSourceUpdated;
@end

@interface TTCategoryMenuViewModel : NSObject
@property (nonatomic, strong) NSArray *toolsTrainingCategoryArray;
@property (nonatomic, strong) Filter *currentFilter;
@property (nonatomic, strong) NSString *selectedLefteMenu;

@property(nonatomic, assign) id<TTCategoryMenuViewModelDelegate> ttCategorryVMDelegate;

- (instancetype)initWithDelegate:(id<TTCategoryMenuViewModelDelegate>)delegate;
- (void)prepareCategoryData;
- (void)prepareCategoryDataWhenAllAssetSelected;
- (void)prepareFavoriteCategoryDataFor:(NSDictionary*)category;
- (void)prepareCategoryDataForCategory:(NSDictionary*)category;

@end
