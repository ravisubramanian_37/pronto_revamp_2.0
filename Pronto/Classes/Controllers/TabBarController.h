//
//  TabBarController.h
//  Pronto
//
//  Created by cccuser on 29/11/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController

// Commenting the code to check for other alternative methods

//@property (nonatomic,strong) UIImageView* imgView;
//@property (nonatomic,strong) UIImageView* imgCompView;
//@property (nonatomic,strong) UIImageView* imgToolsView;
//@property (nonatomic,strong) UIImageView* imgEventsView;
//
//-(void) changeOrientation;
//-(void) updateTabSelection:(int)index;

@end
