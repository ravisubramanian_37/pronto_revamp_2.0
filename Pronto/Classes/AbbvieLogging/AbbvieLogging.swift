//
//  AbbvieLogging.swift
//  Pronto
//
//  Created by m-666346 on 02/07/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//


import UIKit
import CocoaLumberjack
import MessageUI


import class UIKit.UIDevice
import struct  Darwin.utsname
import func Darwin.uname
import func Darwin.round
import func Darwin.getenv


import Darwin
import Foundation


@objc(AbbvieLogging)
final class AbbvieLogging: NSObject
{
    static var sharedFileLogger:DDFileLogger? = nil
    
    static var singletonInstance:AbbvieLogging? = nil
    
    var appName:String! = nil
    var buildNumber:String! = nil
    var deviceType:String! = nil
    var deviceOSVersion:String! = nil
    var appVersion:String! = nil
    
    
    /**
     Provide the singleton instance of AbbvieLogging.
     
     @return singleton instance of AbbvieLogging.
     */
    class func sharedInstance()->AbbvieLogging
    {
        
        if singletonInstance == nil {
            
            singletonInstance  = AbbvieLogging()
        }
        return singletonInstance!
    }
    
    
    fileprivate func initializeProperties () {
        
        appName = ""
        buildNumber = ""
        deviceType = ""
        deviceOSVersion = ""
        appVersion = ""
    }
    
    
    @objc class func logError(_ formattedString:String)
    {
        DDLogError(formattedString)
    }
    
    @objc class func logWarning(_ formattedString:String)
    {
        DDLogWarn(formattedString)
    }
    
    @objc class func logInfo(_ formattedString:String)
    {
        DDLogInfo(formattedString)
    }
    
    @objc class func logDebug(_ formattedString:String)
    {
        DDLogDebug(formattedString)
    }
    
    @objc class func logVerbose(_ formattedString:String)
    {
        DDLogVerbose(formattedString)
    }

    @objc class func loadLogger()
    {
        DDLog.add(AbbvieLogging.shim_fileLogger(), with: .all)    // Writing all logs on file system.
        // DDLog.add(DDASLLogger.sharedInstance, with: .all)         // Writing all logs on apple logging system.
      // swiftlint:disable:next force_cast
        DDLog.add(DDTTYLogger.sharedInstance as! DDLogger, with: .all)         // Writing all logs on Console.
    }
    
    class func shim_fileLogger() -> DDFileLogger
    {
        let fileLoggerManager  = DDLogFileManagerDefault()
        fileLoggerManager.maximumNumberOfLogFiles=6;
        sharedFileLogger = DDFileLogger(logFileManager: fileLoggerManager);
        return sharedFileLogger!
    }
    
    
    internal func shareLogFilesFromViewController(_ viewController: UIViewController) throws {
        
        if (MFMailComposeViewController.canSendMail())
        {
            let filePath = self.getLogFilePath()
            let attachment = try? Data(contentsOf: URL(fileURLWithPath: filePath))
            let filePart =  filePath.components(separatedBy: ".")
            let  fileName = self.getAppName() + "." + filePart.last!
            let emailTitle = "Log file"
            let messageBody = AbbvieLogging.sharedInstance().getMessageBoddyForLogsMail()
            let toRecipents:[String] = [""]
            
            let mail:MFMailComposeViewController =  MFMailComposeViewController ()
            mail.mailComposeDelegate = AbbvieLogging.sharedInstance()
            mail.setToRecipients(toRecipents)
            mail.setMessageBody(messageBody, isHTML:false)
            mail.addAttachmentData(attachment!, mimeType: "text/plain", fileName: fileName)
            mail.title = emailTitle
            mail.setSubject(fileName)
            
            viewController.present(mail, animated: true, completion:
                {
                    print("other way")
            })
            
        } else {
            print("Device is not capable of sending device")
            throw NSError.init(domain: "Device can not send mail to external component", code: 400, userInfo:nil)
        }
        
        
    }
    
    fileprivate func getLogFilePath() -> String
    {
        let fileLogger = DDFileLogger().logFileManager
        let filePaths = fileLogger.sortedLogFilePaths
        let filePath = (filePaths.first)! as String
        return filePath
    }
    
    fileprivate func getAppName() -> String
    {
        if appName == nil  || appName.count <= 0
        {
            appName = AbbvieLogging.sharedInstance().getAppBundleInfo()["CFBundleName"] as? String
        }
        return appName
        
    }
    
    fileprivate func getAppBundleInfo () -> [String: AnyObject]
    {
        let appBundle = Bundle.main
        let info = appBundle.infoDictionary
        return info! as [String: AnyObject]
    }
    
    fileprivate func getMessageBoddyForLogsMail() -> String
    {
        var messagaBody: String! = nil
        messagaBody = "App Version: "   + AbbvieLogging.sharedInstance().getAppVersion() + "\n"
        messagaBody += "App Build Number: " + AbbvieLogging.sharedInstance().getAppBuildNumber() + "\n"
        messagaBody += "iOS: " + AbbvieLogging.sharedInstance().getDeviceOSVersion() + "\n"
        messagaBody += "Device Orientation: " + AbbvieLogging.sharedInstance().getDeviceOrientationAsString() + "\n"
        messagaBody += "Device: " + AbbvieLogging.sharedInstance().getDeviceType()
        
        // version , build number, os, orientation, iphone or ipad, cpu usage, available disk
        return messagaBody!
    }
    
    fileprivate func getAppVersion () -> String
    {
        if appVersion == nil || appVersion.isEmpty == true
        {
            let version = AbbvieLogging.sharedInstance().getAppBundleInfo()["CFBundleShortVersionString"]
            appVersion =  "\(version!)"
        }
        return appVersion
    }
    
    fileprivate func getAppBuildNumber () -> String
    {
        if buildNumber == nil  || buildNumber.isEmpty == true
        {
            buildNumber = AbbvieLogging.sharedInstance().getAppBundleInfo()["CFBundleVersion"] as? String
        }
        return buildNumber
    }
    
    fileprivate func getDeviceType () -> String
    {
        
        if deviceType == nil || deviceType.isEmpty == true
        {
            let device = Device()
            deviceType = device.description
        }
        return deviceType
    }
    
    fileprivate func getDeviceOSVersion() -> String
    {
        if deviceOSVersion == nil || deviceOSVersion.isEmpty == true
        {
            let deviceType = Device()
            deviceOSVersion = deviceType.systemName + " " + deviceType.systemVersion
        }
        return deviceOSVersion
    }
    
    fileprivate func getDeviceOrientation() ->  UIInterfaceOrientation
    {
        let deviceOrientaion = UIApplication.shared.statusBarOrientation
        return deviceOrientaion
    }
    
    fileprivate func getDeviceOrientationAsString () -> String {
        let orientation = AbbvieLogging.sharedInstance().getDeviceOrientation().isLandscape ? "Landscape": "Potrait"
        return orientation
    }
    
    fileprivate func getCPUUsage () -> Double
    {
        return 5.5
    }
    
    fileprivate func getAvailableMemory () -> Double
    {
        return 5.6
    }
}

extension AbbvieLogging: MFMailComposeViewControllerDelegate
{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        
        print("MFMailComposeViewControllerDelegate method called")
        controller.dismiss(animated: true, completion: nil)
    }
}



public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
                switch identifier {
                case "iPod5,1":                                 return "iPod Touch 5"
                case "iPod7,1":                                 return "iPod Touch 6"
                case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
                case "iPhone4,1":                               return "iPhone 4s"
                case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
                case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
                case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
                case "iPhone7,2":                               return "iPhone 6"
                case "iPhone7,1":                               return "iPhone 6 Plus"
                case "iPhone8,1":                               return "iPhone 6s"
                case "iPhone8,2":                               return "iPhone 6s Plus"
                case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
                case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
                case "iPhone8,4":                               return "iPhone SE"
                case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
                case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
                case "iPhone10,3", "iPhone10,6":                return "iPhone X"
                case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
                case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
                case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
                case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
                case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
                case "iPad6,11", "iPad6,12":                    return "iPad 5"
                case "iPad7,5", "iPad7,6":                      return "iPad 6"
                case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
                case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
                case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
                case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
                case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
                case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
                case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
                case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
                case "AppleTV5,3":                              return "Apple TV"
                case "AppleTV6,2":                              return "Apple TV 4K"
                case "AudioAccessory1,1":                       return "HomePod"
                case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
                default:                                        return identifier
                }
            #elseif os(tvOS)
                switch identifier {
                case "AppleTV5,3": return "Apple TV 4"
                case "AppleTV6,2": return "Apple TV 4K"
                case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
                default: return identifier
                }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}


public enum Device {
    #if os(iOS)
    /// Device is an [iPod Touch (5th generation)](https://support.apple.com/kb/SP657)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP657/sp657_ipod-touch_size.jpg)
    case iPodTouch5
    
    /// Device is an [iPod Touch (6th generation)](https://support.apple.com/kb/SP720)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP720/SP720-ipod-touch-specs-color-sg-2015.jpg)
    case iPodTouch6
    
    /// Device is an [iPhone 4](https://support.apple.com/kb/SP587)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP643/sp643_iphone4s_color_black.jpg)
    case iPhone4
    
    /// Device is an [iPhone 4s](https://support.apple.com/kb/SP643)
    ///
    /// ![Image](https://support.apple.com/library/content/dam/edam/applecare/images/en_US/iphone/iphone5s/iphone_4s.png)
    case iPhone4s
    
    /// Device is an [iPhone 5](https://support.apple.com/kb/SP655)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP655/sp655_iphone5_color.jpg)
    case iPhone5
    
    /// Device is an [iPhone 5c](https://support.apple.com/kb/SP684)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP684/SP684-color_yellow.jpg)
    case iPhone5c
    
    /// Device is an [iPhone 5s](https://support.apple.com/kb/SP685)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP685/SP685-color_black.jpg)
    case iPhone5s
    
    /// Device is an [iPhone 6](https://support.apple.com/kb/SP705)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP705/SP705-iphone_6-mul.png)
    case iPhone6
    
    /// Device is an [iPhone 6 Plus](https://support.apple.com/kb/SP706)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP706/SP706-iphone_6_plus-mul.png)
    case iPhone6Plus
    
    /// Device is an [iPhone 6s](https://support.apple.com/kb/SP726)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP726/SP726-iphone6s-gray-select-2015.png)
    case iPhone6s
    
    /// Device is an [iPhone 6s Plus](https://support.apple.com/kb/SP727)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP727/SP727-iphone6s-plus-gray-select-2015.png)
    case iPhone6sPlus
    
    /// Device is an [iPhone SE](https://support.apple.com/kb/SP738???) TODO: Spec page not posted yet
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/???) TODO: Image page not posted yet
    case iPhoneSE
    
    /// Device is an [iPad 2](https://support.apple.com/kb/SP622)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP622/SP622_01-ipad2-mul.png)
    case iPad2
    
    /// Device is an [iPad (3rd generation)](https://support.apple.com/kb/SP647)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP662/sp662_ipad-4th-gen_color.jpg)
    case iPad3
    
    /// Device is an [iPad (4th generation)](https://support.apple.com/kb/SP662)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP662/sp662_ipad-4th-gen_color.jpg)
    case iPad4
    
    /// Device is an [iPad Air](https://support.apple.com/kb/SP692)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP692/SP692-specs_color-mul.png)
    case iPadAir
    
    /// Device is an [iPad Air 2](https://support.apple.com/kb/SP708)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP708/SP708-space_gray.jpeg)
    case iPadAir2
    
    /// Device is an [iPad Mini](https://support.apple.com/kb/SP661)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP661/sp661_ipad_mini_color.jpg)
    case iPadMini
    
    /// Device is an [iPad Mini 2](https://support.apple.com/kb/SP693)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP693/SP693-specs_color-mul.png)
    case iPadMini2
    
    /// Device is an [iPad Mini 3](https://support.apple.com/kb/SP709)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP709/SP709-space_gray.jpeg)
    case iPadMini3
    
    /// Device is an [iPad Mini 4](https://support.apple.com/kb/SP725)
    ///
    /// ![Image](https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP725/SP725ipad-mini-4.png)
    case iPadMini4
    
    /// Device is an [iPad Pro](http://www.apple.com/ipad-pro/)
    ///
    /// ![Image](http://images.apple.com/v/ipad-pro/c/images/shared/buystrip/ipad_pro_large_2x.png)
    case iPadPro
    
    case iPhone7
    
    case iPhone7plus
    
    case iPadPro97
    
    case iPadPro129
    
    case iPadPro1122ndGeneration
    
    case iPadPro10
    
    #elseif os(tvOS)
    /// Device is an [Apple TV](http://www.apple.com/tv/)
    ///
    /// ![Image](http://images.apple.com/v/tv/c/images/overview/buy_tv_large_2x.jpg)
    case appleTV4
    #endif
    
    /// Device is [Simulator](https://developer.apple.com/library/ios/documentation/IDEs/Conceptual/iOS_Simulator_Guide/Introduction/Introduction.html)
    ///
    /// ![Image](https://developer.apple.com/assets/elements/icons/256x256/xcode-6.png)
    indirect case simulator(Device)
    
    /// Device is not yet known (implemented)
    /// You can still use this enum as before but the description equals the identifier (you can get multiple
  /// identifiers for the same product class (e.g. "iPhone6,1" or "iPhone 6,2" do both mean "iPhone 5s))
    case unknownDevice(String)
    
    public init() {
        var systemInfo = utsname()
        uname(&systemInfo)
        let mirror = Mirror(reflecting: systemInfo.machine)
        
        // I know that reduce is O(n^2) (see http://airspeedvelocity.net/2015/08/03/arrays-linked-lists-and-performance/)
//      but it's *so* nice ❤️ and since we are working with very short strings it shouldn't matter.
        let identifier = mirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 , value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
      // swiftlint:disable:next cyclomatic_complexity
        func mapIdentifierToDevice(_ identifier: String) -> Device {
            #if os(iOS)
                switch identifier {
                case "iPod5,1":                                 return .iPodTouch5
                case "iPod7,1":                                 return .iPodTouch6
                case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return .iPhone4
                case "iPhone4,1":                               return .iPhone4s
                case "iPhone5,1", "iPhone5,2":                  return .iPhone5
                case "iPhone5,3", "iPhone5,4":                  return .iPhone5c
                case "iPhone6,1", "iPhone6,2":                  return .iPhone5s
                case "iPhone7,2":                               return .iPhone6
                case "iPhone7,1":                               return .iPhone6Plus
                case "iPhone8,1":                               return .iPhone6s
                case "iPhone8,2":                               return .iPhone6sPlus
                case "iPhone8,4":                               return .iPhoneSE
                case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return .iPad2
                case "iPad3,1", "iPad3,2", "iPad3,3":           return .iPad3
                case "iPad3,4", "iPad3,5", "iPad3,6":           return .iPad4
                case "iPad4,1", "iPad4,2", "iPad4,3":           return .iPadAir
                case "iPad5,3", "iPad5,4":                      return .iPadAir2
                case "iPad2,5", "iPad2,6", "iPad2,7":           return .iPadMini
                case "iPad4,4", "iPad4,5", "iPad4,6":           return .iPadMini2
                case "iPad4,7", "iPad4,8", "iPad4,9":           return .iPadMini3
                case "iPad5,1", "iPad5,2":                      return .iPadMini4
                case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return .iPadPro
//                case "iPad6,3", "iPad6,4":                      return .iPad_Pro9_7
//                case "iPad6,7", "iPad6,8":                      return .iPad_Pro12_9
                case "iPad7,1", "iPad7,2":                      return .iPadPro1122ndGeneration
                case "iPad7,3", "iPad7,4":                      return .iPadPro10 // iPad Pro 10.5 Inch
                case "i386", "x86_64":                          return .simulator(mapIdentifierToDevice(String(validatingUTF8: getenv("SIMULATOR_MODEL_IDENTIFIER"))!))
                default:                                        return .unknownDevice(identifier)
                }
            #elseif os(tvOS)
                switch identifier {
                case "AppleTV5,3":                              return AppleTV4
                case "i386", "x86_64":                          return Simulator(mapIdentifierToDevice(String(UTF8String: getenv("SIMULATOR_MODEL_IDENTIFIER"))!))
                default:                                        return UnknownDevice(identifier)
                }
            #endif
        }
        self = mapIdentifierToDevice(identifier)
    }
    
    #if os(iOS)
    public static var allPods: [Device] {
        return [.iPodTouch5, .iPodTouch6]
    }
    
    /// All iPhones
    public static var allPhones: [Device] {
        return [.iPhone4, iPhone4s, .iPhone5, .iPhone5s, .iPhone6, .iPhone6Plus, .iPhone6s, .iPhone6sPlus, .iPhoneSE, .iPhone7, .iPhone7plus]
    }
    
    /// All iPads
    public static var allPads: [Device] {
        return [.iPad2, .iPad3, .iPad4, .iPadAir, .iPadAir2, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadPro, .iPadPro10, .iPadPro129, .iPadPro97, .iPadPro1122ndGeneration]
    }
    
    /// All simulator iPods
    public static var allSimulatorPods: [Device] {
        return allPods.map(Device.simulator)
    }
    
    /// All simulator iPhones
    public static var allSimulatorPhones: [Device] {
        return allPhones.map(Device.simulator)
    }
    
    /// All simulator iPads
    public static var allSimulatorPads: [Device] {
        return allPads.map(Device.simulator)
    }
    
    /// Return whether the device is an iPod (real or simulator)
    public var isPod: Bool {
        return self.isOneOf(Device.allPods) || self.isOneOf(Device.allSimulatorPods)
    }
    
    /// Return whether the device is an iPhone (real or simulator)
    public var isPhone: Bool {
        return self.isOneOf(Device.allPhones) || self.isOneOf(Device.allSimulatorPhones)
    }
    
    /// Return whether the device is an iPad (real or simulator)
    public var isPad: Bool {
        return self.isOneOf(Device.allPads) || self.isOneOf(Device.allSimulatorPads)
    }
    
    /// Return whether the device is any of the simulator
    /// Useful when there is a need to check and skip running a portion of code (location request or others)
    public var isSimulator: Bool {
        return self.isOneOf(Device.allSimulators)
    }
    
    
    #elseif os(tvOS)
    /// All TVs
    public static var allTVs: [Device] {
    return [.AppleTV4]
    }
    
    /// All simulator TVs
    public static var allSimulatorTVs: [Device] {
    return allTVs.map(Device.Simulator)
    }
    #endif
    
    /// All real devices (i.e. all devices except for all simulators)
    public static var allRealDevices: [Device] {
        #if os(iOS)
            return allPods + allPhones + allPads
        #elseif os(tvOS)
            return allTVs
        #endif
    }
    
    /// All simulators
    public static var allSimulators: [Device] {
        return allRealDevices.map(Device.simulator)
    }
    
    /**
     This method saves you in many cases from the need of updating your code with every new device.
     Most uses for an enum like this are the following:
     
     ```
     switch Device() {
     case .iPodTouch5, .iPodTouch6: callMethodOnIPods()
     case .iPhone4, iPhone4s, .iPhone5, .iPhone5s, .iPhone6, .iPhone6Plus, .iPhone6s, .iPhone6sPlus, .iPhoneSE: callMethodOnIPhones()
     case .iPad2, .iPad3, .iPad4, .iPadAir, .iPadAir2, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadPro: callMethodOnIPads()
     default: break
     }
     ```
     This code can now be replaced with
     
     ```
     let device = Device()
     if device.isOneOf(Device.allPods) {
     callMethodOnIPods()
     } else if device.isOneOf(Device.allPhones) {
     callMethodOnIPhones()
     } else if device.isOneOf(Device.allPads) {
     callMethodOnIPads()
     }
     ```
     
     - parameter devices: An array of devices.
     
     - returns: Returns whether the current device is one of the passed in ones.
     */
    public func isOneOf(_ devices: [Device]) -> Bool {
        return devices.contains(self)
    }
    
    /// The style of interface to use on the current device.
    /// This is pretty useless right now since it does not add any further functionality to the existing
    /// [UIUserInterfaceIdiom](https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIDevice_Class/#//apple_ref/c/tdef/UIUserInterfaceIdiom) enum.
    public enum UserInterfaceIdiom {
        
        /// The user interface should be designed for iPhone and iPod touch.
        case phone
        /// The user interface should be designed for iPad.
        case pad
        /// The user interface should be designed for TV
        case tv
        /// The user interface should be designed for Car
        case carPlay
        /// Used when an object has a trait collection, but it is not in an environment yet. For example, a view that is created, but not put into a view hierarchy.
        case unspecified
        
        fileprivate init() {
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:          self = .pad
            case .phone:        self = .phone
            case .tv:           self = .tv
            case .carPlay:      self = .carPlay
            default:            self = .unspecified
            }
        }
        
    }
    
    /// The name identifying the device (e.g. "Dennis' iPhone").
    public var name: String {
        return UIDevice.current.name
    }
    
    /// The name of the operating system running on the device represented by the receiver (e.g. "iPhone OS" or "tvOS").
    public var systemName: String {
        return UIDevice.current.systemName
    }
    
    /// The current version of the operating system (e.g. 8.4 or 9.2).
    public var systemVersion: String {
        return UIDevice.current.systemVersion
    }
    
    /// The model of the device (e.g. "iPhone" or "iPod Touch").
    public var model: String {
        return UIDevice.current.model
    }
    
    /// The model of the device as a localized string.
    public var localizedModel: String {
        return UIDevice.current.localizedModel
    }
    
}

// MARK: - CustomStringConvertible
extension Device: CustomStringConvertible {
    
    public var description: String {
        #if os(iOS)
            switch self {
            case .iPodTouch5:                   return "iPod Touch 5"
            case .iPodTouch6:                   return "iPod Touch 6"
            case .iPhone4:                      return "iPhone 4"
            case .iPhone4s:                     return "iPhone 4s"
            case .iPhone5:                      return "iPhone 5"
            case .iPhone5c:                     return "iPhone 5c"
            case .iPhone5s:                     return "iPhone 5s"
            case .iPhone6:                      return "iPhone 6"
            case .iPhone6Plus:                  return "iPhone 6 Plus"
            case .iPhone6s:                     return "iPhone 6s"
            case .iPhone6sPlus:                 return "iPhone 6s Plus"
            case .iPhoneSE:                     return "iPhone SE"
            case .iPad2:                        return "iPad 2"
            case .iPad3:                        return "iPad 3"
            case .iPad4:                        return "iPad 4"
            case .iPadAir:                      return "iPad Air"
            case .iPadAir2:                     return "iPad Air 2"
            case .iPadMini:                     return "iPad Mini"
            case .iPadMini2:                    return "iPad Mini 2"
            case .iPadMini3:                    return "iPad Mini 3"
            case .iPadMini4:                    return "iPad Mini 4"
            case .iPadPro:                      return "iPad Pro"
            case .iPhone7:                      return "iPhone 7"
            case .iPhone7plus:                  return "iPhone 7 Plus"
            case .iPadPro97:                  return "iPad Pro 9.7 Inch"
            case .iPadPro129:                 return "iPad Pro 12.9 Inch"
            case .iPadPro1122ndGeneration:  return "iPad Pro 12.9 Inch 2. Generation"
            case .iPadPro10:                 return "iPad Pro 10.5 Inch"
            case .simulator(let model):         return "Simulator (\(model))"
            case .unknownDevice(let identifier):return identifier
            }
        #elseif os(tvOS)
            switch self {
            case .AppleTV4:                     return "Apple TV 4"
            case .Simulator(let model):         return "Simulator (\(model))"
            case .UnknownDevice(let identifier):return identifier
            }
        #endif
    }
    
}

// MARK: - Equatable
extension Device: Equatable {}

public func == (lhs: Device, rhs: Device) -> Bool {
    return lhs.description == rhs.description
}
