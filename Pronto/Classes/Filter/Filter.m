//
//  Filter.m
//  Pronto
//
//  Created by m-666346 on 22/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import "Filter.h"

@implementation Filter

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.favoriteIdsArray   = @[];
        self.assetsIdsToFilter  = @[];
        self.searchTextString   = @"";
        self.selectedCategories = @[];
    }
    return self;
}
- (NSString*)description
{
    return [NSString stringWithFormat:@"Applied Filter:  \nSearched Text %@ \nSelected Categories: %@ \nSelected Favorite IdsArray: %@ \n Asset id to filter: %@", self.searchTextString, self.selectedCategories, self.favoriteIdsArray, self.assetsIdsToFilter];
}
@end
