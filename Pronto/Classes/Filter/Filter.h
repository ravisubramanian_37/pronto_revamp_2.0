//
//  Filter.h
//  Pronto
//
//  Created by m-666346 on 22/03/18.
//  Copyright © 2018 Abbvie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Filter : NSObject

@property (nonatomic, strong) NSArray *selectedCategories;
@property (nonatomic, strong) NSArray *favoriteIdsArray;
@property (nonatomic, strong) NSArray *assetsIdsToFilter;
@property (nonatomic, strong) NSString *searchTextString;

@end
