//
//  FastPDFKitToolbar.m
//  Pronto
//
//  Created by Praveen Kumar on 03/07/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

#import "ZMProntoTheme.h"
#import "Pronto-Swift.h"
#import "FastPDFKitToolbar.h"
#import "ZMUserActions.h"
#import "EventsViewController.h"
#import "ProntoUserDefaults.h"
#import "ViewUtils.h"
#import "EventOnlyAsset+CoreDataClass.h"
#import "EventOnlyAsset+CoreDataProperties.h"
#import "FastPDFViewController.h"
#import "UIColor+ColorDirectory.h"

@interface ZMCustomButtonItemBase : NSObject
+ (UIButton *)customView:(NSString *)imageName;

@end

@interface FastPDFKitToolbar ()
{
    id <ZMProntoTheme> theme;
    UIButton *nigthModeButton;
    UIButton *tileButtonForIcon;
    UIButton *bookmarkButtonForIcon;
    UIButton *searchButtonIcon;
}

@end

@implementation FastPDFKitToolbar

- (FastPDFKitToolbar *) initWithParent:(UIView *)parent andFrame:(CGRect)frame andAsset:(Asset*)asset PDFView:(UIView *)pdfView {
    
    self = (FastPDFKitToolbar *)[[[NSBundle mainBundle] loadNibNamed:@"FastPDFKitToolbar" owner:self options:nil] objectAtIndex:0];
    _container = parent;
    _pdfContainer = pdfView;
    _nightMode = NO;
    _asset = asset;
    
    if (_container) {
        self.frame = CGRectMake(0,frame.origin.y, UIScreen.mainScreen.bounds.size.width, frame.size.height);
        theme = [ZMProntoTheme sharedTheme];
        [_container addSubview:self];
        
    }
    
    // change the color of toolbar
    self.barTintColor = [UIColor colorWithRed:0.86 green:0.89 blue:0.93 alpha:1.0];
    
    [self adjustViewedBar];
    
    return self;
}

-(void) adjustViewedBar {
    // title view to display title of asset and last viewed asset
    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self.titleView setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0]];
    
    _assetTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.frame.size.width, self.frame.size.height)];
    _assetTitle.backgroundColor = [UIColor clearColor];
    
    _assetTitle.font = [UIFont fontWithName:@"Lato-Bold" size:16];
    [self.titleView addSubview:_assetTitle];
    
  UIView* titleViewLastViewed = [[UIView alloc] initWithFrame:CGRectMake(self.titleView.frame.size.width/2, 0, self.titleView.frame.size.width, self.frame.size.height)];
   // UIView* titleViewLastViewed = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 0, self.frame.size.width/2, self.frame.size.height)];
    [titleViewLastViewed setBackgroundColor:[UIColor colorWithRed:0.49 green:0.63 blue:0.77 alpha:1.0]];
    
    _lastViewAssetHeading = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, titleViewLastViewed.size.width, 21)];
    _lastViewAssetHeading.font = [UIFont fontWithName:@"Lato-Regular" size:12];
    _lastViewAssetHeading.textColor = [UIColor whiteColor];
    [titleViewLastViewed addSubview:_lastViewAssetHeading];
    
    _lastViewAsset = [[UILabel alloc] initWithFrame:CGRectMake(7, 18, titleViewLastViewed.size.width, 21)];
    _lastViewAsset.font = [UIFont fontWithName:@"Lato-Bold" size:16];
    _lastViewAsset.textColor = [UIColor whiteColor];
  _lastViewAssetHeading.backgroundColor = [UIColor clearColor];
  _lastViewAsset.backgroundColor = [UIColor clearColor];
    [titleViewLastViewed addSubview:_lastViewAsset];
    
    [self.titleView addSubview:titleViewLastViewed];
    
    [self populatePrevious_CurrentAssetTitle];
  //For last viewed in PDF
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)];
        //Adding userinteraction for label
        [titleViewLastViewed setUserInteractionEnabled:YES];
        //Adding label to tap gesture
        [titleViewLastViewed addGestureRecognizer:gesture];
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
  
  
  //Display previous asset
  NSDictionary* previousAsset = [[ProntoUserDefaults userDefaults] getPreviousAsset];
  if (previousAsset != nil && previousAsset.count > 0)
  {
    if(([previousAsset objectForKey:@"id"] != nil) &&
       [[previousAsset objectForKey:@"id"] integerValue] == [self.asset.assetID integerValue])
    {
      return;
    }
    NSNumber * idVal = [previousAsset objectForKey:@"id"];
    NSString * assetType = [previousAsset objectForKey:@"file_mime"];
//    Asset * savedAsset = [[ZMProntoManager sharedInstance]retrieveAssetWithAssetID: idVal];
    [[ProntoUserDefaults userDefaults] setPreviousAsset:_asset];
    NSArray * savedAsset = [[ZMCDManager sharedInstance]getDownloadedAsset:idVal.stringValue];
    if (savedAsset.count > 0 && [assetType isEqualToString: @"application/pdf"]) {
      [[ProntoAssetHandlingClass sharedInstance]openAsset:savedAsset[0]];
    } else {
      NSArray * assetVal = [[ZMCDManager sharedInstance]getAssetArrayById:idVal.stringValue];
      if(assetVal.count > 0) {
//        if (![[assetVal valueForKey: @"file_mime"] isEqualToString: @"application/pdf"]) {
          [[ProntoAssetHandlingClass sharedInstance]openAsset:assetVal[0]];
//        }
      }
    }
  }
}

-(UIBarButtonItem *) createSeparator
{
    
    UIButton *separator = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 50.f, 50.f)];
    [separator setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    return [[UIBarButtonItem alloc] initWithCustomView:separator];
}

- (void)setPdf:(FastPDFViewController *)pdf {
    
    _pdf = pdf;
    
    if (self.isEventOnlyAsset == YES)
    {
        [self setItems:nil];
    }
    
    // customize the toolbar based on the new UI changes
    [self setToolbarItems];
    [self setToolBarItemsAccordingToAssetType];
    
}

- (void)setToolBarItemsAccordingToAssetType
{
    if (self.isEventOnlyAsset == YES)
    {
        if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2)
        {
            
            [ZMUserActions sharedInstance].currentOrientation = @"portrait";
            
            [self setItems:@[[self createSeparator], _shareButtonItemFast,
                             [self createSeparator],_searchIconFast,
                             [self createSeparator],_printIconFast,
                             [self createSeparator],_bookIconFast,_spaceFast,_spaceFast, _closeBarButtonItemFast]];
        }
        else
        {
            [ZMUserActions sharedInstance].currentOrientation = @"landscape";
            
            
            [self setItems:@[_shareButtonItemFast,_shareButtonFast,
                             _searchIconFast,_searchButtonFast,
                             _printIconFast,_printButtonItemFast,
                             _bookIconFast,_bookButtonFast,_spaceFast,_spaceFast,_closeBarButtonItemFast]];
        }
        
    }
    else
    {
        if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2)
        {
            
            [ZMUserActions sharedInstance].currentOrientation = @"portrait";
            
            [self setItems:@[_spaceFast,_favBarButtonItemFast,
                             [self createSeparator],_shareButtonItemFast,
                             [self createSeparator],_searchIconFast,
                             [self createSeparator],_printIconFast,
                             //                             [self createSeparator],_pdf.thumbnailsButtonItem,
                             //                             [self createSeparator],_brightIcon,
                             [self createSeparator],_bookIconFast,
                             [self createSeparator],[self createSeparator],
                             [self createSeparator],[self createSeparator],
                             [self createSeparator],_closeBarButtonItemFast]];
        }
        else
        {
            [ZMUserActions sharedInstance].currentOrientation = @"landscape";
            
            
            [self setItems:@[_favBarButtonItemFast,_favButtonFast,
                             _shareButtonItemFast,_shareButtonFast,
                             _searchIconFast,_searchButtonFast,
                             _printIconFast,_printButtonItemFast,
                             //                             _tileIcon,_tileButton,
                             //                             _brightIcon,_brightButton,
                             _bookIconFast,_bookButtonFast,
                             _spaceFast,_spaceFast,_closeBarButtonItemFast]];
        }
    }
}

-(void)onChangingOrientation
{
    [self setToolbarItems];
    [self setToolBarItemsAccordingToAssetType];
    [self adjustViewedBar];
    if(self.isLastViewedShown == YES)
    {
        [self hideLastViewed];
        [self showLastViewed];
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *assetString = [prefs stringForKey:@"AssetTitle"];
    if(assetString.length > 0)
        _lastViewAsset.text = [NSString stringWithFormat:@"%@",assetString];
    
}

// Set the toolbar with required buttons and actions
-(void) setToolbarItems
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, UIScreen.mainScreen.bounds.size.width, self.frame.size.height);
    _spaceFast = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    
    // Share Icon
    UIView* shareView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    _closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 30.f, 30.f)];
    [_closeBtn setImage:[UIImage imageNamed:@"deletebutton"] forState:UIControlStateNormal];
    [_closeBtn addTarget:self  action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:_closeBtn];
    _closeBarButtonItemFast = [[UIBarButtonItem alloc] initWithCustomView:_closeBtn];
 
    
    // Fav Icon
    if (self.isEventOnlyAsset == NO) //for EventOnlyAsset these option will not be shown
    {
        UIButton *favBtn = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 30.f, 30.f)];
        if (_asset.assetID != nil)
        {
            if ([self isFavourite])
            {
                [favBtn setImage:[UIImage imageNamed:RedesignConstants.favoSelected] forState:UIControlStateNormal];
                [favBtn setSelected: YES];
            }
            else
            {
                [favBtn setImage:[UIImage imageNamed:RedesignConstants.favoUnSelected] forState:UIControlStateNormal];
                [favBtn setSelected: NO];
            }
        }
        else
        {
            [favBtn setImage:[UIImage imageNamed:RedesignConstants.favoUnSelected] forState:UIControlStateNormal];
        }
        
        [favBtn addTarget:self action:@selector(favButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [shareView addSubview:favBtn];
        _favBarButtonItemFast = [[UIBarButtonItem alloc] initWithCustomView:favBtn];// favView
        
        // get all barbuttons
        _favButtonFast = [[UIBarButtonItem alloc] initWithTitle:@"Add to Favorites" style:UIBarButtonItemStylePlain target:self action:@selector(addToFavButtonPressed:)];
        [self setButtonColor:_favButtonFast];
    }
    else
    {
    
        _favButtonFast        = nil;
        _favBarButtonItemFast = nil;
        
        self.favButtonFast =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
        self.favBarButtonItemFast = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];;
    }
    
    //Search
    searchButtonIcon = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 25.f, 25.f)];
    [searchButtonIcon setImage:[UIImage imageNamed:@"Search"] forState:UIControlStateNormal];
    [searchButtonIcon addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
    _searchIconFast = [[UIBarButtonItem alloc] initWithCustomView:searchButtonIcon];
    
    UIButton *searchButtonName = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 60.f, 30.f)];
    [searchButtonName addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
    [searchButtonName setTitle:@"Search Asset" forState:UIControlStateNormal];
    [searchButtonName setTitleColor:[[RedesignThemesClass sharedInstance] hexStringToUIColorWithHex:[RedesignThemesClass sharedInstance].royalBlueCode] forState:UIControlStateNormal];
    _searchButtonFast = [[UIBarButtonItem alloc] initWithCustomView:searchButtonName];
    
    // Bookmark
    bookmarkButtonForIcon = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 30.f, 30.f)];
    CGRect frame = bookmarkButtonForIcon.frame;
    frame.size.width = 30;
    frame.size.height = 35;
    bookmarkButtonForIcon.frame = frame;
    if ([ZMUserActions sharedInstance].isBookmarkTapped)
    {
        [bookmarkButtonForIcon setImage:[UIImage imageNamed:@"bookmark_Selected"] forState:UIControlStateNormal];
        [bookmarkButtonForIcon setSelected: YES];
    }
    else
    {
        [bookmarkButtonForIcon setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
        [bookmarkButtonForIcon setSelected: NO];
    }
    
    [bookmarkButtonForIcon addTarget:self action:@selector(bookmarkAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:bookmarkButtonForIcon];
    _bookIconFast = [[UIBarButtonItem alloc] initWithCustomView:bookmarkButtonForIcon];
    
    UIButton *bookmarkButtonName = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 60.f, 30.f)];
    [bookmarkButtonName addTarget:self action:@selector(bookmarkAction:) forControlEvents:UIControlEventTouchUpInside];
    [bookmarkButtonName setTitle:@" Bookmark" forState:UIControlStateNormal];
//    [bookmarkButtonName setTitleColor:[UIColor GetColorFromHexValue3880B5] forState:UIControlStateNormal];
    [bookmarkButtonName setTitleColor:[[RedesignThemesClass sharedInstance] hexStringToUIColorWithHex:[RedesignThemesClass sharedInstance].royalBlueCode] forState:UIControlStateNormal];
    _bookButtonFast = [[UIBarButtonItem alloc] initWithCustomView:bookmarkButtonName];
    
    [self setButtonColor:_bookButtonFast];
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.00 green:0.51 blue:0.73 alpha:1.0]];
    
    // Share button
    _shareButtonFast = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(shareAction:)];
    // Share button
    _shareButtonItemFast = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(shareAction:)];
    [_shareButtonItemFast setImage:[UIImage imageNamed:RedesignConstants.export_redesign]];
    [self setButtonColor:_shareButtonFast];
    [self setButtonColor:_shareButtonItemFast];
    
    // check and display Print Option
    if(_asset.field_print.length > 0 && [_asset.field_print isEqualToString:@"1"])
    {
        // Print button
        _printIconFast = [[UIBarButtonItem alloc] initWithTitle:@"Print" style:UIBarButtonItemStylePlain target:self action:@selector(printAction:)];
        [_printIconFast setImage:[UIImage imageNamed:@"printAssetDetail"]];
        _printButtonItemFast = [[UIBarButtonItem alloc] initWithTitle:@"Print" style:UIBarButtonItemStylePlain target:self action:@selector(printAction:)];
        [self setButtonColor:_printIconFast];
        [self setButtonColor:_printButtonItemFast];
    }
    else
    {
        _printIconFast =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
        _printButtonItemFast = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];;
        
    }
    
}

-(void)setButtonColor:(UIBarButtonItem*)button
{
    [button setTintColor:[[RedesignThemesClass sharedInstance]hexStringToUIColorWithHex:[RedesignThemesClass sharedInstance].royalBlueCode] ];
}

-(void)openLastViewed:(id)sender
{
  //Display previous asset
  NSDictionary* previousAsset = [[ProntoUserDefaults userDefaults] getPreviousAsset];
  if (previousAsset != nil && previousAsset.count > 0)
  {
    if(([previousAsset objectForKey:@"id"] != nil) &&
       [[previousAsset objectForKey:@"id"] integerValue] == [self.asset.assetID integerValue])
    {
      return;
    }
    
    NSNumber * idVal = [previousAsset objectForKey:@"id"];
    
    NSArray * assetVal = [[ZMCDManager sharedInstance]getAssetArrayById:idVal.stringValue];
    
    [[ProntoUserDefaults userDefaults] setPreviousAsset:_asset];
    
    if(assetVal.count > 0)
    {
      [[ProntoAssetHandlingClass sharedInstance]openAsset:assetVal[0]];
    }
  }
}

- (void)tileAction:(id)sender {
    [AbbvieLogging logInfo:@"Tile Selected"];
}

-(void)searchAction:(id)sender {
    [_pdf searchAction:sender];
}

-(void)bookmarkAction:(id)sender {
    [_pdf bookmarkAction:sender];
}

-(void)printAction:(id)sender {
    [_pdf printAction:sender];
}

- (void)shareAction:(id)sender {
    // Email to self button is pressed - showing mail client
    NSString* emailId = [AOFLoginManager sharedInstance].mail ;
    
//    if ([MFMailComposeViewController canSendMail])
//    {
//        if(emailId == nil || [emailId isEqual: [NSNull null]])
//        {
//            [self sendEmail:@""];
//        }
//        else{
//            [self sendEmail:@""];
            [self sendEmail:@""];
//        }
//    }
//    else
//    {
//        NSString* msg = @"Your device cannot send email";
//
//        UIAlertController* confirm = [UIAlertController alertControllerWithTitle:@""  message:msg  preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* okButton = [UIAlertAction
//                                   actionWithTitle:@"OK"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
//                                   }];
//
//        [confirm addAction:okButton];
//
//        [_pdf presentViewController:confirm animated:YES completion:nil];
//
//    }
}

// send mail on selecting "Share" by showing mail client
-(void) sendEmail:(NSString*)emailId
{
  NSString* mailSubject = [NSString stringWithFormat:@"%@ is Sharing a Pronto Asset",[AOFLoginManager sharedInstance].givenName];
  // format the mail body content
  NSString* wcUser = [NSString stringWithFormat:@"<div>Hello ,</div>\n"];
  NSString* deeplink = [NSString stringWithFormat:@"abbviepronto://%@",_asset.assetID];
  NSString* strHtmlBody = [NSString stringWithFormat:
                           @"\nPlease click <b><u><a href=\"%@\">here</a></u></b> to open the asset '%@' in Pronto.You will not be able to open the asset unless you have the Pronto app on your iPad.",
                           deeplink,_asset.title];
  NSMutableString *body = [NSMutableString string];
  [body appendString:wcUser];
  [body appendString:@"<div><br/></div>"];
  [body appendString:strHtmlBody];
  [body appendString:@"<div><br/></div>"];
  [body appendString:@"<div>Thanks</div>"];
  if ([MFMailComposeViewController canSendMail]) {
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    [mail setSubject:mailSubject];
    [mail setMessageBody:body isHTML:YES];
    [mail setToRecipients:@[emailId]];
    mail.delegate = self;
    [_pdf presentViewController:mail animated:YES completion:NULL];
  } else {
    [RedesignConstants sendByURLTo:emailId subject:mailSubject body:body isHtml:YES];
  }
}

// --------------------------------
//
// Function: didFinishWithResult
// Description: Used to return the result of the mail sent
//
// —————————————————------------------

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString* msg = nil;
    
    switch (result)
    {
        case MFMailComposeResultSent:
            if([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
                msg = RedesignConstants.mailSentSuccess;
            else
                msg = RedesignConstants.mailConnectionError;
            break;
        case MFMailComposeResultFailed:
            msg = RedesignConstants.mailSentError;
            break;
        default:
            break;
    }
    
    [_pdf dismissViewControllerAnimated:YES completion:NULL];
    
    // show the message at the bottom of the screen
    AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(msg != nil)
    {
        [mainDelegate showMessage:msg whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    }
}

-(void)closeAction:(id)sender {

    
    [ZMUserActions sharedInstance].isBookmarkTapped = NO;
    [ZMUserActions sharedInstance].isTileViewTapped = NO;
    [[ProntoUserDefaults userDefaults] setPreviousAsset:_asset];
    [ProntoAssetHandlingClass sharedInstance].selectedGridCell = nil;
    [_parent closeButtonDidPress];
}

- (BOOL)isFavourite
{
    BOOL favourite = NO;
    

    
    if (_asset.assetID != nil)
    {
        NSArray *favfranchisee = [[ZMCDManager sharedInstance] getFavFranchiseById:_asset.assetID];
        if(favfranchisee.count > 0)
        {

                favourite = YES;

        }
        else {
            favourite = NO;
        }
    }
    
    return  favourite;
}

- (void)addToFavButtonPressed:(id)sender
{

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self onChangingOrientation];
        [self populateAssetTitle];
    });
}

- (void)favButtonPressed:(id)sender
{
    if ([self isFavourite] == YES)
    {
        [sender setImage:[UIImage imageNamed:RedesignConstants.favoUnSelected] forState:UIControlStateNormal];
        [sender setSelected:NO];
        if(_asset){
            [[MyFavouritesViewModel sharedInstance] deleteAssetFromFavouritesWithAsset:_asset];
            [self addActivityIndicator];
        }
    }
    else
    {
        [sender setImage:[UIImage imageNamed:RedesignConstants.favoSelected] forState:UIControlStateNormal];
        [sender setSelected:YES];
        if(_asset){
            [[MyFavouritesViewModel sharedInstance] createFavObjectWithSelectedAsset:_asset];
            [self addActivityIndicator];
        }
    }
  [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.favouritesAssetUpdationNotification object:self userInfo:nil];
}

- (void)addActivityIndicator {

    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = self.container.center;
    spinner.color = [UIColor blueColor];
    [self.pdfContainer addSubview:spinner];
    self.closeBtn.userInteractionEnabled = NO;
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [spinner startAnimating];


    double delayInSeconds = 8.0; // set the time
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [spinner stopAnimating];
            self.closeBtn.userInteractionEnabled = YES;
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        });
}

-(void) populateAssetTitle
{
    _lastViewAssetHeading.text = @"Return To Last Viewed Asset";
    
    if (self.isEventOnlyAsset == YES) { return; } // If it is Event Only Asset then Fav icon will not be prepared.
    
    NSArray *favfranchise = [[ZMCDManager sharedInstance] getFavFranchiseById:_asset.assetID];
    if (favfranchise.count > 0)
    {
        UIButton *favBtn;
        [favBtn setImage:[UIImage imageNamed:RedesignConstants.favoSelected] forState:UIControlStateNormal];
        [favBtn addTarget:self action:@selector(favButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        _favBarButtonItemFast = [[UIBarButtonItem alloc] initWithCustomView:favBtn];// favView
    }
    else
    {
        UIButton *favBtn = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 20.f, 20.f)];
        [favBtn setImage:[UIImage imageNamed:RedesignConstants.favoUnSelected] forState:UIControlStateNormal];
        [favBtn addTarget:self action:@selector(favButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        _favBarButtonItemFast = [[UIBarButtonItem alloc] initWithCustomView:favBtn];
    }
}

- (void)populatePrevious_CurrentAssetTitle
{
    //Display present and previous asset
    NSDictionary* previousAsset = [[ProntoUserDefaults userDefaults] getPreviousAsset];
    if (previousAsset != nil && previousAsset.count > 0)
    {
        if([[previousAsset objectForKey:@"id"] integerValue]  == [self.asset.assetID integerValue])
        {
            self.lastViewAsset.text = PreviousAssetEmptyString;
        }
        else
        {
            self.lastViewAsset.text = [previousAsset objectForKey:@"name"];
        }
    }
    else
    {
        self.lastViewAsset.text = PreviousAssetEmptyString;
    }
    
    self.assetTitle.text = self.asset.title;
}


- (void)showLastViewed
{
    [_pdfContainer addSubview:self.titleView];
}

- (void)hideLastViewed
{
    [self removeAllSubviewsFromUIView:self.pdfContainer];
}

- (void)removeAllSubviewsFromUIView:(UIView *)parentView
{
    for (id child in [parentView subviews])
    {
        if ([child isMemberOfClass:[UIView class]])
        {
            [child removeFromSuperview];
        }
    }
}

- (void)setPageNumber:(NSString*)pageNumber
{
    
    for (id child in [self.pdfContainer subviews])
    {
        if ([child isMemberOfClass:[self.pageNumberView class]])
        {
            [child removeFromSuperview];
        }
    }
    
    self.pageNumberView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - 55, self.frame.size.height+10, 110, 20)];
    [self.pageNumberView setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0]];
    self.pageNumberView.layer.cornerRadius = 5;
    self.pageNumberView.layer.masksToBounds = true;
    
    self.pageNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.pageNumberView.frame.size.width, self.pageNumberView.frame.size.height)];
    self.pageNumberLabel.backgroundColor = [UIColor clearColor];
    
    self.pageNumberLabel.font = [UIFont fontWithName:@"Lato-Bold" size:14];
    self.pageNumberLabel.textAlignment = NSTextAlignmentCenter;
    [self.pageNumberView addSubview:self.pageNumberLabel];
    
    self.pageNumberLabel.text = pageNumber;
    [_pdfContainer addSubview:self.pageNumberView];
}

@end

@implementation ZMCustomButtonItemBase


+ (UIButton *)customView:(NSString *)imageName
{
    UIButton *_customView = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 32.f, 32.f)];
    [_customView setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    return _customView;
}

@end

