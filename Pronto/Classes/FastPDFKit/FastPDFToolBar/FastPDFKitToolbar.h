//
//  FastPDFKitToolbar.h
//  Pronto
//
//  Created by Praveen Kumar on 03/07/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ZMPDFDocument.h"

@class FastPDFKitToolbar;
@class FastPDFViewController;

@protocol toolbarActionsFast <NSObject>

-(void)toolbar:(FastPDFKitToolbar*)toolbarView didTapLastViewedButton:(UIButton*)sender forAsset:(NSObject*)asset;
-(void)toolbar:(FastPDFKitToolbar*)toolbarView TapLastViewedButton:(NSObject*)asset;
@end

@interface FastPDFKitToolbar : UIToolbar<MFMailComposeViewControllerDelegate,UINavigationControllerDelegate>

/**
 *  Class to add custom toolBar into FastPDFViewController
 */

@property (weak, nonatomic) UIView *container;

@property (weak, nonatomic) UIView *pdfContainer;

/**
 *  Document to add ToolBar
 */

@property (weak, nonatomic) FastPDFViewController *pdf;

@property (weak, nonatomic) Asset *asset;

//@property (strong, nonatomic) ZMHeader *header;

//@property (weak, nonatomic) ZMLibrary *library;

//@property (weak, nonatomic) Competitors *competitors;

//@property (weak, nonatomic) ToolsTraining *toolsTraining;

@property (weak, nonatomic) EventsViewController *eventsViewController;

@property (weak, nonatomic) CalendarDetailVC *calendarDetailVC;

@property (strong, nonatomic) FastPDFViewController *parent;

@property (nonatomic) BOOL nightMode;
@property (nonatomic) BOOL isTileViewTapped;
@property (nonatomic) BOOL isBookmarkTapped;
@property (nonatomic) UIBarButtonItem *brightnessControllerFast;
// customize the toolbar
@property (strong, nonatomic) NSString* currentOrientation;
@property (strong, nonatomic) UIBarButtonItem* favBarButtonItemFast;
@property (strong, nonatomic) UIBarButtonItem* favButtonFast;
@property (strong, nonatomic) UIBarButtonItem* searchIconFast;
@property (strong, nonatomic) UIBarButtonItem* searchButtonFast;
@property (strong, nonatomic) UIBarButtonItem* tileIconFast;
@property (strong, nonatomic) UIBarButtonItem* tileButtonFast;
@property (strong, nonatomic) UIBarButtonItem* printIconFast;
@property (strong, nonatomic) UIBarButtonItem* printButtonFast;
@property (strong, nonatomic) UIBarButtonItem* brightIconFast;
@property (strong, nonatomic) UIBarButtonItem* brightButtonFast;
@property (strong, nonatomic) UIBarButtonItem* bookIconFast;
@property (strong, nonatomic) UIBarButtonItem* bookButtonFast;
@property (strong, nonatomic) UIBarButtonItem* shareButtonFast;
@property (strong, nonatomic) UIBarButtonItem* shareButtonItemFast;
@property (strong, nonatomic) UIBarButtonItem* printButtonItemFast;
@property (strong, nonatomic) UIBarButtonItem* closeBarButtonItemFast;
@property (strong, nonatomic) UIBarButtonItem* spaceFast;
@property (strong, nonatomic) UIButton *closeBtn;
@property (strong, nonatomic) NSString* docName;
@property (strong, nonatomic) UILabel* assetTitle;
@property (strong, nonatomic) UILabel* lastViewAsset;
@property (strong, nonatomic) UILabel* lastViewAssetHeading;
@property (strong, nonatomic) UIView* titleView;

@property (strong, nonatomic) UIView* pageNumberView;
@property (strong, nonatomic) UILabel* pageNumberLabel;

@property (nonatomic, weak) id<toolbarActionsFast> toolbarDelegate;

/**
 *  This value provides detail of type of asset being opened
 */
@property (nonatomic, assign) BOOL isEventOnlyAsset;

@property (nonatomic, assign) BOOL isLastViewedShown;

/**
 * Inits the tool bar with the give container
 **/
-(FastPDFKitToolbar *) initWithParent:(UIView *)parent andFrame:(CGRect)frame andAsset:(Asset*)asset PDFView:(UIView *)pdfView;

-(void) populateAssetTitle;
// Customize the toolbar
-(void) setToolbarItems;
-(void)onChangingOrientation ;
- (void)populatePrevious_CurrentAssetTitle;
- (void)showLastViewed;
- (void)hideLastViewed;
- (void)setPageNumber:(NSString*)pageNumber;

@end

