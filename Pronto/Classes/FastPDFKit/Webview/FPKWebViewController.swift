//
//  FPKWebViewController.swift
//  Pronto
//
//  Created by Praveen Kumar on 13/08/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

import UIKit
import WebKit

class FPKWebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {

    @IBOutlet var webView: WKWebView!
    @objc static let sharedInstance = FPKWebViewController()
    var urlToLoad:String! = ""
    var userFranchise:String! = ""
    var defaultHexString:String! = "0D1C49"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        webView.uiDelegate = self
        let userFranchise = self.userFranchise
        var hexString = ""
        if(userFranchise == "In House" || userFranchise == "Home Office")
        {
            hexString = defaultHexString
        }
        else
        {
            hexString = (ZMUserActions.sharedInstance()?.franchiseColor)!
        }
        self.view.backgroundColor = UIColor.getFromHexString(hexString)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.urlToLoad = self.urlToLoad.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)

        self.openURL(withURL: self.urlToLoad)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    @objc func setURL(url:String)
    {
        urlToLoad = url
    }
    
    @objc func setUserFranchise(franchise:String)
    {
        userFranchise = franchise
    }
    
    @objc func openURL(withURL url:String)
    {
        let link = URL(string:url)
        if(link == nil)
        {
            let alert = UIAlertController(title: "Error", message: "The URL is not Proper, Please check and try again.", preferredStyle: .alert)
            let key: String! = "OK"
            alert.addAction(UIAlertAction(title: key, style: .default, handler: { action in
                self.closeButtonAction((Any).self)
            }))
            self.present(alert, animated: true)
        }
        else
        {
            let request = URLRequest(url: link!)
            self.webView.load(request)
        }
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    private func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
    }
    private func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        print("finish to load")
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        print(navigationAction)
        return nil;
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print(navigationAction)
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        print(navigationResponse)
        decisionHandler(.allow)
    }

}
