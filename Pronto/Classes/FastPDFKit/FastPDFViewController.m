//
//  FastPDFViewController.m
//  Pronto
//
//  Created by Praveen Kumar on 20/06/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

#import "FastPDFViewController.h"
#import "AppDelegate.h"
#import "FastPDFKitToolbar.h"
#import "ZMTracking.h"
#import "ZMUserActions.h"
//#import "ZMLibrary.h"
#import "EventCoreDataManager.h"
#import "ProntoAssetHandlingClass.h"
#import "Competitors.h"
#import "ToolsTraining.h"
#import "Pronto-Swift.h"
#import "Constant.h"
#import "Pronto-Swift.h"
#import "FeedbackView.h"
#import "ViewUtils.h"
#import "ProntoUserDefaults.h"

#import "Encryptor.h"
#import "NSDate+DateDirectory.h"

#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AssetsServiceClass.h"
#import "CalendarDetailVC.h"

#define FPK_REUSABLE_VIEW_NONE 0
#define FPK_REUSABLE_VIEW_SEARCH 1
#define FPK_REUSABLE_VIEW_TEXT 2
#define FPK_REUSABLE_VIEW_OUTLINE 3
#define FPK_REUSABLE_VIEW_BOOKMARK 4

static const NSUInteger FPKReusableViewNone = FPK_REUSABLE_VIEW_NONE;
static const NSUInteger FPKReusableViewSearch = FPK_REUSABLE_VIEW_SEARCH;
static const NSUInteger FPKReusableViewText = FPK_REUSABLE_VIEW_TEXT;
static const NSUInteger FPKReusableViewOutline = FPK_REUSABLE_VIEW_OUTLINE;
static const NSUInteger FPKReusableViewBookmarks = FPK_REUSABLE_VIEW_BOOKMARK;

#define FPK_SEARCH_VIEW_MODE_MINI 0
#define FPK_SEARCH_VIEW_MODE_FULL 1

static const NSUInteger FPKSearchViewModeMini = FPK_SEARCH_VIEW_MODE_MINI;
static const NSUInteger FPKSearchViewModeFull = FPK_SEARCH_VIEW_MODE_FULL;

#define PAGE_NUM_LABEL_TEXT(x,y) [NSString stringWithFormat:@"Page %lu of %lu",(x),(y)]
#define PAGE_NUM_LABEL_TEXT_PHONE(x,y) [NSString stringWithFormat:@"%lu / %lu",(x),(y)]

@interface FastPDFViewController ()<headerActions,UIPopoverPresentationControllerDelegate>
{
    FastPDFKitToolbar *toolBar;
    BOOL toolBarAdjusted;
}

@property (nonatomic, readwrite) NSUInteger currentReusableView;
@property (nonatomic, readwrite) NSUInteger currentSearchViewMode;

//@property (nonatomic, strong) FeedbackView *feedbackView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, readwrite) BOOL willFollowLink;
@property (nonatomic, readwrite) BOOL hudHidden;

@property (nonatomic, readwrite) BOOL bookmarkPopupActive;
@property (nonatomic, readwrite) BOOL searchPopupActive;
@property (nonatomic, readwrite) NSInteger originSize;
@property (nonatomic, readwrite) NSInteger widthToCompare;
@property (nonatomic, readwrite) WKWebView *webviewForLinks;
@property (nonatomic, readwrite) UIButton *webviewButton;
@property (nonatomic, readwrite) BOOL urlLoaded;

@property (nonatomic, strong) id searchButtonSender;
@property (nonatomic, strong) id bookmarkButtonSender;

@property (nonatomic, strong) UIView* statusBar;
@property (nonatomic, strong) HomeMainHeaderView *mainHeaderView;
@property (nonatomic, strong) Asset *currentSelectedAssetToSeeDetail;
@end

@implementation FastPDFViewController
BOOL _isDownloadignAsset;
static FastPDFViewController *sharedInstance = nil;
+ (FastPDFViewController *)sharedInstance{
    
    if(sharedInstance == nil){
        sharedInstance = [[super allocWithZone:NULL] init];
        [sharedInstance initializeData];
    }
    return sharedInstance;
  
}- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeData];
    self.view.frame = UIScreen.mainScreen.bounds;
    self.theme = [ZMProntoTheme sharedTheme];
    [self.theme styleMainView:self.view];
    self.hudHidden = YES;
    
    self.bookmarkPopupActive = NO;
    self.searchPopupActive = NO;
    
    self.webviewForLinks = [[WKWebView alloc] initWithFrame:self.view.frame];
    self.webviewButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    // Do any additional setup after loading the view.
}

- (void)initializeData
{
  [self registerNotification:@"OPEN_SCHEMA_ASSET_FOUND"];
  [self registerNotification:@"OPEN_SCHEMA_ASSET_NOT_FOUND"];
}


- (void)registerNotification:(NSString *)name {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recievedNotification:) name:name object:nil];
}

-(void)resetAssetValue{
    self.currentSelectedAssetToSeeDetail = nil;
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];

//    self.headerView.delegate = self;
 
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _viewMain.frame = UIScreen.mainScreen.bounds;
    if([[UIApplication sharedApplication] isStatusBarHidden])
    {
        [self prefersStatusBarHidden];
    }
    self.headerView = [UIView new];
    self.headerView.frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 64);
    _mainHeaderView = [[[NSBundle mainBundle] loadNibNamed:RedesignConstants.mainViewHeaderNib owner:nil options:nil] objectAtIndex:0];
    _mainHeaderView.frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 64);
    [self.headerView addSubview:_mainHeaderView];
    [_mainHeaderView setThemes];
    _viewMain.frame = UIScreen.mainScreen.bounds;
    [_viewMain addSubview:self.headerView];
    
    _mainHeaderView.frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 64);
    [_mainHeaderView configureHeaderForAssetPage];
    
    [self headerAdjust];
     
     [toolBar populatePrevious_CurrentAssetTitle];
     
     self.widthToCompare = 0;
     if(self.view.frame.size.width < self.view.frame.size.height)
     {
         self.widthToCompare = self.view.frame.size.width;
     }
     else
     {
         self.widthToCompare = self.view.frame.size.height;
     }
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
//Display popover correctly when rotating
-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
}

-(void) viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];

    
}


- (void)recievedNotification:(NSNotification *) notification{
  AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  Asset *asset = [notification.userInfo objectForKey:@"asset"];
  enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
  if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_FOUND"]) {
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          if (assetType != ZMProntoFileTypesBrightcove) {
              [mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
          } else {
              // for video file
              asset.isUpdateAvailable = NO;
          }
        NSString * path = [[ZMProntoManager sharedInstance]validatePdfWithAsset: asset];
        if (![[[ZMProntoManager sharedInstance]validatePdfWithAsset: asset] isEqualToString: @""]) {
          [self openAsset:asset];
        } else {
          [self validateAsset:asset];
        }
      });
  }
  else if ([[notification name] isEqualToString:@"OPEN_SCHEMA_ASSET_NOT_FOUND"]) {
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          [mainDelegate showMessage:@"Asset not found" whithType:ZMProntoMessagesTypeWarning];
      });
  }
}



-(void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    
    switch(self.currentReusableView)
    {
            
        case FPKReusableViewNone: // This should never happens.
            break;
            
        case FPKReusableViewOutline:
        case FPKReusableViewBookmarks:
            
            // The popover has been already dismissed, just set the flag accordingly.
            self.bookmarkButtonSender = nil;
            self.currentReusableView = FPKReusableViewNone;
            break;
            
        case FPKReusableViewSearch:
            self.searchButtonSender = nil;
            if(self.currentSearchViewMode == FPKSearchViewModeFull)
            {
                self.currentReusableView = FPKReusableViewNone;
            }
            break;
            // Same as above, but also cancel the search.
            
        default: break;
    }
}

-(void)dismissAlternateViewController {
    
    // This is just an utility method that will call the appropriate dismissal procedure depending
    // on which alternate controller is visible to the user.
    
    switch(self.currentReusableView) {
            
        case FPKReusableViewNone:
            break;
            
        case FPKReusableViewText:
            
            if(self.presentedViewController) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            
            self.currentReusableView = FPKReusableViewNone;
            
            break;
            
        case FPKReusableViewOutline:
        case FPKReusableViewBookmarks:
            
            // Same procedure for both outline and bookmark.
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                
                if([UIPresentationController class])
                {
                    if(self.presentedViewController) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                }
                else
                {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                
            } else {
                
                /* On iPad iOS 8 and iPhone whe have a presented view controller */
                
                if(self.presentedViewController) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
            }
            self.currentReusableView = FPKReusableViewNone;
            break;
            
        case FPKReusableViewSearch:
            
            if(self.currentSearchViewMode == FPKSearchViewModeFull) {
                
                [self dismissSearchViewController:_searchViewController];
                self.currentReusableView = FPKReusableViewNone;
                
            } else if (self.currentSearchViewMode == FPKSearchViewModeMini) {
                
                [self dismissMiniSearchView];
                self.currentReusableView = FPKReusableViewNone;
            }
            
            // Cancel search and remove the controller.
            
            break;
        default: break;
    }
}

-(void)presentViewController:(UIViewController *)controller
                    fromRect:(CGRect)rect
                  sourceView:(UIView *)view
                 contentSize:(CGSize)contentSize
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        if([UIPopoverPresentationController class]) {
            
            if(self.presentedViewController) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            
            controller.modalPresentationStyle = UIModalPresentationPopover;
            controller.preferredContentSize = contentSize;
            
            [self presentViewController:controller animated:YES completion:nil];
            
            UIPopoverPresentationController * popoverPresentationController = controller.popoverPresentationController;
            popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
            popoverPresentationController.sourceRect = rect;
            popoverPresentationController.sourceView = view;
            popoverPresentationController.delegate = self;
            
        } else {
            
            //Fix for warnings
            controller.modalPresentationStyle = UIModalPresentationPopover;
            controller.popoverPresentationController.sourceView = self.view;
            controller.popoverPresentationController.sourceRect = rect;
            [self presentViewController:controller animated:YES completion:nil];
            
        }
        
    } else {
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)presentViewController:(UIViewController *)controller
               barButtonItem:(UIBarButtonItem *)barButtonItem
                 contentSize:(CGSize)contentSize
                      sender:(UIButton*)sender
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if([UIPopoverPresentationController class]) {
            
            if(self.presentedViewController) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            
            controller.modalPresentationStyle = UIModalPresentationPopover;
            
            UIPopoverPresentationController * popoverPresentationController = controller.popoverPresentationController;
            
            popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
            popoverPresentationController.sourceView = sender;
            
            CGFloat senderXValue = sender.frame.origin.x;
            
            if(self.bookmarkPopupActive && self.view.frame.size.width != self.originSize)
            {
                if(self.view.frame.size.width > self.originSize)
                {
                    NSInteger divisionVal = 4;
                    if(self.widthToCompare == iPadProGlobalVal)
                    {
                        divisionVal = 9;
                    }
                    else if(self.widthToCompare == iPadPro10_widthVal)
                    {
                        divisionVal = 5;
                    }
                    else if (self.widthToCompare == iPadPro12_widthVal)
                    {
                        divisionVal = 3;
                    }
                    
                    senderXValue = sender.frame.origin.x - (self.view.frame.size.width/divisionVal);
                    
                }
                else
                {
                    if(sender.frame.size.width > 30)
                    {
                        if(self.widthToCompare == iPadProGlobalVal)
                        {
                            senderXValue = sender.frame.origin.x - (self.view.frame.size.width/5) - (sender.frame.size.width/2);
                        }
                        else if(self.widthToCompare == iPadPro10_widthVal)
                        {
                            senderXValue = sender.frame.origin.x - (self.view.frame.size.width/10) - (sender.frame.size.width/2);
                        }
                        else if(self.widthToCompare == iPadPro12_widthVal)
                        {
                            senderXValue = sender.frame.origin.x + (self.view.frame.size.width/8) - sender.frame.size.width + 20;
                        }
                    }
                    else
                    {
                        if(self.widthToCompare == iPadProGlobalVal)
                        {
                            senderXValue = sender.frame.origin.x - (self.view.frame.size.width/5) + 13;
                        }
                        else if(self.widthToCompare == iPadPro10_widthVal)
                        {
                            senderXValue = sender.frame.origin.x - (self.view.frame.size.width/10) + 13;
                        }
                        else if(self.widthToCompare == iPadPro12_widthVal)
                        {
                            senderXValue = sender.frame.origin.x + (self.view.frame.size.width/8) - (sender.frame.size.width/4);
                        }
                    }
                }
            }
            else if(self.searchPopupActive && self.view.frame.size.width != self.originSize)
            {
                if(self.view.frame.size.width > self.originSize)
                {
                    NSInteger divisionVal = 4;
                    if(self.widthToCompare == iPadProGlobalVal)
                    {
                        divisionVal = 9;
                    }
                    else if(self.widthToCompare == iPadPro10_widthVal)
                    {
                        divisionVal = 6;
                    }
                    else if (self.widthToCompare == iPadPro12_widthVal)
                    {
                        divisionVal = 3;
                    }
                    senderXValue = sender.frame.origin.x - (self.view.frame.size.width/divisionVal);
                    
                }
                else
                {
                    if(sender.frame.size.width > 30)
                    {
                        if(self.widthToCompare == iPadProGlobalVal)
                        {
                            senderXValue = sender.frame.origin.x - (self.view.frame.size.width/13) - sender.frame.size.width + 10;
                        }
                        else if(self.widthToCompare == iPadPro10_widthVal)
                        {
                            senderXValue = sender.frame.origin.x - sender.frame.size.width + 20;
                        }
                        else if(self.widthToCompare == iPadPro12_widthVal)
                        {
                            senderXValue = sender.frame.origin.x + (self.view.frame.size.width/5) - sender.frame.size.width + 20;
                        }
                    }
                    else
                    {
                        if(self.widthToCompare == iPadProGlobal)
                        {
                            senderXValue = sender.frame.origin.x - (self.view.frame.size.width/13) + 13;
                        }
                        else if(self.widthToCompare == iPadPro10_width)
                        {
                            senderXValue = sender.frame.origin.x - sender.frame.size.width;
                        }
                        else if(self.widthToCompare == iPadPro12_width)
                        {
                            senderXValue = sender.frame.origin.x + (self.view.frame.size.width/5) - (sender.frame.size.width/4);
                        }
                    }

                }
            }
            
            popoverPresentationController.sourceRect = CGRectMake(senderXValue , sender.frame.origin.y, sender.frame.size.width, sender.frame.size.height);
            
            popoverPresentationController.delegate = self;
            
            [self presentViewController:controller animated:YES completion:nil];
        }
        else
        {
                controller.modalPresentationStyle = UIModalPresentationPopover;
                controller.popoverPresentationController.sourceView = sender;
                controller.popoverPresentationController.sourceRect = sender.bounds;
                [self presentViewController:controller animated:YES completion:nil];
        }
    }
    else
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
}


# pragma mark - MFDocumentManager Initialize

-(id)initWithDocumentManager:(MFDocumentManager *)aDocumentManager {
    self = [super initWithDocumentManager:aDocumentManager];
    if(self) {
        
        self.popoverContentSize = CGSizeMake(372, 650);
        
        [self setDocumentDelegate:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleSearchUpdateNotification:)
                                                     name:kNotificationSearchResultAvailable
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleSearchGotCancelledNotification:)
                                                     name:kNotificationSearchGotCancelled
                                                   object:nil];
    }
//    [self addHeader];
    return self;
}

-(id)initWithDocumentManager:(MFDocumentManager *)aDocumentManager count:(NSUInteger)count {
    
    //    Here we call the superclass initWithDocumentManager passing the very same MFDocumentManager
    //    we used to initialize this class. However, since you probably want to track which document are
    //    handling to synchronize bookmarks and the like, you can easily use your own wrapper for the MFDocumentManager
    //    as long as you pass an instance of it to the superclass initializer.
    
    self = [super initWithDocumentManager:aDocumentManager count:count];
    if(self) {
        
        self.popoverContentSize = CGSizeMake(372, 650);
        
        [self setDocumentDelegate:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleSearchUpdateNotification:)
                                                     name:kNotificationSearchResultAvailable
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleSearchGotCancelledNotification:)
                                                     name:kNotificationSearchGotCancelled
                                                   object:nil];
    }
//    [self addHeader];
    
    return self;
}

# pragma mark - Custom for FastPDFKit

-(void) headerAdjust {
    //    UIApplication.sharedApplication.statusBarHidden = NO;
    
    self.statusBar = [[UIView alloc]init];
    if (@available(iOS 13.0, *)) {
        self.statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
    }
    else
    {
        self.statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    }

    _viewMain.frame = [UIScreen mainScreen].bounds;
    if(_asset) {

        _headerView.frame = CGRectMake(0, 20, UIScreen.mainScreen.bounds.size.width, 44);
        toolBar = [[FastPDFKitToolbar alloc] initWithParent:_viewMain andFrame:CGRectMake(0, 64, UIScreen.mainScreen.bounds.size.width, 40) andAsset:_asset PDFView:self.view];
        toolBar.toolbarDelegate = (id<toolbarActionsFast>)self;
        toolBar.isEventOnlyAsset = self.isEventOnlyAsset;
        toolBar.pdf = self;

        toolBar.asset           = _asset;

        toolBar.parent = self;
        toolBar.eventsViewController = self.eventsViewController;
        toolBar.calendarDetailVC     = self.calendarDetailVC;
        toolBar.isLastViewedShown = self.isLastViewedShown;
        [toolBar populateAssetTitle];
        [toolBar setToolbarItems];

    }

    [[UIApplication sharedApplication].keyWindow addSubview:self.statusBar];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    [_headerView settingsHideOnRotate];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, UIScreen.mainScreen.bounds.size.height, self.view.frame.size.height);
    _viewMain.frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.height, UIScreen.mainScreen.bounds.size.width);
    self.headerView.frame = CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, UIScreen.mainScreen.bounds.size.height, self.headerView.frame.size.height);
    self.mainHeaderView.frame = CGRectMake(self.mainHeaderView.frame.origin.x, self.mainHeaderView.frame.origin.y, UIScreen.mainScreen.bounds.size.height, self.mainHeaderView.frame.size.height);
    self.webviewScreen.frame = CGRectMake(self.webviewScreen.frame.origin.x, self.webviewScreen.frame.origin.y, UIScreen.mainScreen.bounds.size.height, self.webviewScreen.frame.size.height);
    self.webviewForLinks.frame = CGRectMake(self.webviewForLinks.frame.origin.x, self.webviewForLinks.frame.origin.y, UIScreen.mainScreen.bounds.size.height, self.webviewForLinks.frame.size.height);
    if(self.bookmarkButtonSender != nil)
    {
        self.bookmarkPopupActive = YES;
        [self dismissAlternateViewController];
    }
    if(self.searchButtonSender != nil)
    {
        self.searchPopupActive = YES;
        [self dismissAlternateViewController];
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
     self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, self.view.frame.size.height);
//    [_headerView adjustHeader];
    [toolBar onChangingOrientation];
    if(self.isLastViewedShown == YES)
    {
      [self updatePageNumberLabel];
      [toolBar showLastViewed];
      self.isLastViewedShown = YES;
    }
    [toolBar populateAssetTitle];
    // set the orientation for toolbar
    if ([[UIApplication sharedApplication]statusBarOrientation] == 1 || [[UIApplication sharedApplication]statusBarOrientation] == 2)
        [ZMUserActions sharedInstance].currentOrientation = @"portrait";
    else
        [ZMUserActions sharedInstance].currentOrientation = @"landscape";
//    [_headerView settingsShowOnRotate];
    if(self.bookmarkPopupActive == YES)
    {
        [self presentBookmarksViewController:self.bookmarkButtonSender];
        self.bookmarkPopupActive = NO;
    }
    if(self.searchPopupActive == YES)
    {
        [self presentFullSearchView:self.searchButtonSender];
        self.searchPopupActive = NO;
    }
    
    [self adjustWebview];
    [self adjustStatusBar];
//    [self.headerView adjustStatusBar];
//    [self.headerView hideForAssetDetail];
}

- (void)setAsset:(Asset *)asset{
    
    if (asset) {
        _asset = asset;
        
      AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
      _asset.view_count = [NSNumber numberWithInt:_asset.view_count.intValue+1];
        NSLog(@"Asset count updated %@", _asset.view_count);
      
        [ZMUserActions sharedInstance].countOfAssetDict = 0;
        
        //PRONTO-37 Deeplinking to a Page in a PDF
        if ([ZMUserActions sharedInstance].pageNo > 0) {
            [self setPage:([ZMUserActions sharedInstance].pageNo)];
            [ZMUserActions sharedInstance].pageNo = 0;
        }
        
        [[ZMCDManager sharedInstance]updateAsset:asset success:^{
            [[ZMAbbvieAPI sharedAPIManager]saveViewedAsset:asset.assetID type:asset.type complete:^(NSDictionary *savedAssetDetails){
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"savedAssetDetails:%@",savedAssetDetails]];}
                error:^(NSError *error) {
                [AbbvieLogging logInfo:[NSString stringWithFormat:@"error:%@",error]];
            } ];
            [self setViewEvent];
        } error:^(NSError * error) {
            [AbbvieLogging logError:[NSString stringWithFormat:@"Error updating asset on documment %@", error.localizedDescription]];
        }];
    }
    
//    [self setHeader];
}

- (void)setViewEvent{
    
    if (_asset) {
        ZMEvent *plus1view = [[ZMEvent alloc] init];
        plus1view.assetId = _asset.assetID.longValue;
        plus1view.eventType = ZMEventView;
        plus1view.eventValue = [NSString stringWithFormat:@"%d",1];
        [[ZMAssetsLibrary defaultLibrary] saveEvent:plus1view];
//        [_library markAssetAsRead:_asset];
        [self assetControllerDelegateSendEvent];
    }
}

- (void)assetControllerDelegateSendEvent {
    
    if (_asset) {
        
        dispatch_async(dispatch_queue_create("Sending Events", NULL), ^{
            [[AssetsServiceClass sharedAPIManager] uploadEvents:^{
                [AbbvieLogging logInfo:@">>> Events Synced"];
            } error:^(NSError *error) {
                [AbbvieLogging logError:[NSString stringWithFormat:@">>> Error with uploadEvents: %@", error]];
            }];
        });
    }
}



#pragma mark - ZMHeaderDelegate

-(void)closeButtonDidPress
{
//    _library.pageIndex = 0;
//    [_library refreshLibrary];
//    [[NSNotificationCenter defaultCenter] postNotificationName:kZMPDFDocumentDidCloseButtonPressedNotification object:nil];
    
    int count = (int)self.navigationController.viewControllers.count;
    for(int i= (count - 1);i>=0;i--){
        if([[ProntoRedesignSingletonClass sharedInstance]isFavouritesChosen]){
            if([self.navigationController.viewControllers[i] isKindOfClass:[MyFavouritesDetailsViewController class]]){
              [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.notificationforListAndGridChange object:self userInfo:nil];
              [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.favouritesRemovalNotification object:self userInfo:nil];
                [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                break;
            }else if([self.navigationController.viewControllers[i] isKindOfClass:[HomeViewController class]]){
                [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
            }
        }else{
            if([self.navigationController.viewControllers[i] isKindOfClass:[CategoriesViewController class]]){
                [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                break;
            }else if([self.navigationController.viewControllers[i] isKindOfClass:[CalendarDetailVC class]]){
                [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                               break;
            }else if([self.navigationController.viewControllers[i] isKindOfClass:[SearchResultsViewController class]]){
                [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
                               break;
            }
            else if([self.navigationController.viewControllers[i] isKindOfClass:[HomeViewController class]]){
                [self.navigationController popToViewController:self.navigationController.viewControllers[i] animated:YES];
            }
        }
    }
}

#pragma mark -
#pragma mark MFDocumentViewControllerDelegate methods implementation Support for Multimedia

- (CGFloat)edgeFlipWidth
{
    return 0.025;
}

-(Class<MFAudioPlayerViewProtocol>)classForAudioPlayerView{
    return [MFAudioPlayerViewImpl class];
}

-(BOOL) documentViewController:(MFDocumentViewController *)dvc doesHaveToAutoplayAudio:(NSString *)audioUri{
    return YES;
}

-(BOOL) documentViewController:(MFDocumentViewController *)dvc doesHaveToAutoplayVideo:(NSString *)videoUri{
    return YES;
}

-(void) documentViewController:(MFDocumentViewController *)dvc didReceiveTapOnPage:(NSUInteger)page atPoint:(CGPoint)point {
    
    //unused
    
}

-(BOOL) documentViewController:(MFDocumentViewController *)dvc didReceiveURIRequest:(NSString *)uri{
    
    if (![uri hasPrefix:@"#page="]) {
        
        if (![uri hasPrefix:@"mailto:"]) {
            
            //NSArray *arrayParameter = nil;
            NSString *uriType = nil;
            NSString *uriResource = nil;
            
            NSString * documentPath = nil;
            
            NSRange separatorRange = [uri rangeOfString:@"://"];
            
            if(separatorRange.location!=NSNotFound) {
                
                uriType = [uri substringToIndex:separatorRange.location];
                uriResource = [uri substringFromIndex:separatorRange.location + separatorRange.length];
                
                if ([uriType isEqualToString:@"fpke"]||[uriType isEqualToString:@"videomodal"]) {
                    
                    documentPath = [self.document.resourceFolder stringByAppendingPathComponent:uriResource];
                    
                    [self playVideo:documentPath local:YES];
                    
                    return YES;
                }
                
                if ([uriType isEqualToString:@"fpkz"]||[uriType isEqualToString:@"videoremotemodal"]) {
                    
                    documentPath = [@"http://" stringByAppendingString:uriResource];
                    
                    [self playVideo:documentPath local:NO];
                    
                    return YES;
                }
                
                if ([uriType isEqualToString:@"fpki"]||[uriType isEqualToString:@"htmlmodal"]){
                    
                    documentPath = [self.document.resourceFolder stringByAppendingPathComponent:uriResource];
                    
                    [self showWebView:documentPath];
                    
                    return YES;
                }
                
                if ([uriType isEqualToString:@"https"]||[uriType isEqualToString:@"http"]){
                    
                    [self loadVideoAsset:uri];
                    return YES;
                }
                
                if ([uri containsString:@"abbviepronto"]){
                    
                    NSURL *URL = [NSURL URLWithString:uri];
                    
                    [self openURLShema:URL];
                    
                    return YES;
                }
            }
        }
        else
        {
            [self shareThroughEmail:uri];
        }
        
    } else {
        
        // Chop the page parameters into an array and set is as current page parameters
        
        NSArray *arrayParameter = arrayParameter = [uri componentsSeparatedByString:@"="];
        [self setPage:[[arrayParameter objectAtIndex:1]intValue]];
    }
    
    return NO;
}

- (void)shareThroughEmail: (NSString*)recepientString {
    // Email to self button is pressed - showing mail client
    recepientString = [recepientString stringByReplacingOccurrencesOfString:@"mailto:" withString:@""];
    NSArray *mailArray = [recepientString componentsSeparatedByString:@"?"];
    NSString *emailId = [mailArray firstObject];
    if ([MFMailComposeViewController canSendMail])
    {
        if(emailId == nil || [emailId isEqual: [NSNull null]])
        {
            [self sendEmail:@"" withContent:@""];
        }
        else{
            [self sendEmail:@"" withContent:[mailArray lastObject]];
        }
    }
    else
    {
        NSString* msg = @"Your device cannot send email";
        
        UIAlertController* confirm = [UIAlertController alertControllerWithTitle:@""  message:msg  preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                   }];
        
        [confirm addAction:okButton];
        
        [self presentViewController:confirm animated:YES completion:nil];
        
    }
}

// send mail on selecting "Share" by showing mail client
-(void) sendEmail:(NSString*)emailId withContent:(NSString*)otherContent
{
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    [mail setToRecipients:@[emailId]];
    NSMutableArray *contentArray = [[otherContent componentsSeparatedByString:@"&"] mutableCopy];
    for(int i=0; i<contentArray.count; i++)
    {
        NSString *content = contentArray[i];
        if([content containsString:@"subject"])
        {
            content = [content stringByReplacingOccurrencesOfString:@"subject=" withString:@""];
            content = [content stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            [mail setSubject:content];
        }
        else if ([content containsString:@"body"])
        {
            content = [content stringByReplacingOccurrencesOfString:@"body=" withString:@""];
            content = [content stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            [mail setMessageBody:content isHTML:NO];
        }
        else if ([content containsString:@"bcc"])
        {
            content = [content stringByReplacingOccurrencesOfString:@"bcc=" withString:@""];
            NSMutableArray *bccArray = [NSMutableArray array];
            [bccArray addObject:content];
            [mail setCcRecipients:bccArray];
        }
        else
        {
            content = [content stringByReplacingOccurrencesOfString:@"cc=" withString:@""];
            NSMutableArray *ccArray = [NSMutableArray array];
            [ccArray addObject:content];
            [mail setBccRecipients:ccArray];
        }
    }

    mail.delegate = self;
    [self presentViewController:mail animated:YES completion:NULL];
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString* msg = nil;
    
    switch (result)
    {
        case MFMailComposeResultSent:
            if([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou])
                msg = RedesignConstants.mailSentSuccess;
            else
                msg = RedesignConstants.mailConnectionError;
            break;
        case MFMailComposeResultFailed:
            msg = RedesignConstants.mailSentError;
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    // show the message at the bottom of the screen
    AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(msg != nil)
    {
        [mainDelegate showMessage:msg whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    }
}

- (void)playVideo:(NSString *)videoPath local:(BOOL)isLocal{
    
    NSURL *url = nil;
    BOOL openVideo = NO;
    self.multimediaVisible = YES;
    
    if (isLocal) {
        
        NSFileManager * fileManager = [[NSFileManager alloc]init];
        
        if ([fileManager fileExistsAtPath:videoPath]) {
            
            openVideo = YES;
            url = [NSURL fileURLWithPath:videoPath];
        } else {
            
            openVideo = NO;
        }
        
    } else {
        
        url = [NSURL URLWithString:videoPath];
        openVideo = YES;
    }
    
    if (openVideo) {
        AVPlayerViewController * _moviePlayer1 = [[AVPlayerViewController alloc] init];
        
        AVURLAsset *asset = [AVURLAsset assetWithURL: url];
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
        
        _moviePlayer1.player = [[AVPlayer alloc] initWithPlayerItem: item];
        
        [self presentViewController:_moviePlayer1 animated:YES completion:^{
            [_moviePlayer1.player play];
        }];
    }
}

-(void)showWebView:(NSString *)url{
    
    NSURL *URL = [NSURL URLWithString:url];
    [[UIApplication sharedApplication] openURL:URL options:@{} completionHandler:nil];
    
}

-(void)adjustWebview
{
    
    self.webviewScreen.frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    self.webviewButton.frame = CGRectMake(self.view.frame.size.width - 44, 20, 40, 40);
    self.webviewForLinks.frame = CGRectMake(0,60, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-60);
}

-(void)adjustStatusBar
{
    CGRect frame = self.statusBar.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    self.statusBar.frame = frame;
}

-(void)loadVideoAsset:(NSString *)urlString
{
    self.webviewScreen.hidden = NO;
    self.webviewForLinks.hidden = NO;
    
    [self.webviewButton addTarget:self
                           action:@selector(buttonActionOfView:)
                 forControlEvents:UIControlEventTouchUpInside];
    [self.webviewButton setImage:[UIImage imageNamed:@"deletebutton"] forState:UIControlStateNormal];
    self.webviewButton.frame = CGRectMake(self.view.frame.size.width - 44, 20, 40, 40);
    
    NSString *urlToLoad = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

    
    if([urlToLoad containsString:dlServerHostName])
    {
        NSDate *timeOfEncryption = [[ProntoUserDefaults userDefaults] getdlServerEncryptionTime];
        int timeDifference = 0;
        if(timeOfEncryption != nil)
        {
            timeDifference = [[NSDate date] timeIntervalSinceDate:timeOfEncryption];
        }
        timeDifference = ceil(timeDifference / 60);
        NSString* encryptedString = @"";
        if(timeDifference > 20 || timeDifference == 0)
        {
            NSString *dateString = [NSDate getDateAsString:[NSDate date]];
            NSString *stringToEncrypt = [kSecureValueToEncrypt stringByAppendingString:dateString];
            encryptedString = [Encryptor encryptedData:stringToEncrypt WithHexKey:keyToEncrypt];
            
            [[ProntoUserDefaults userDefaults] setdlServerEncryptionTime:[NSDate date]];
            [[ProntoUserDefaults userDefaults] setdlServerEncryptionValue:encryptedString];
        }
        else
        {
            encryptedString = [[ProntoUserDefaults userDefaults] getdlServerEncryptionValue];
        }
        urlToLoad = [urlToLoad stringByAppendingString:[NSString stringWithFormat:@"?encrypted=%@", encryptedString]];
    }
    NSURL *url = [NSURL URLWithString:urlToLoad];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [self.webviewForLinks loadRequest:urlRequest];
    
    self.webviewForLinks.frame = CGRectMake(self.webviewScreen.frame.origin.x,self.webviewScreen.frame.origin.y + 60, self.webviewScreen.frame.size.width, self.webviewScreen.frame.size.height-60);
    if(self.urlLoaded == NO)
    {
        [self.webviewScreen addSubview:self.webviewButton];
        [self.webviewScreen addSubview:self.webviewForLinks];
        self.urlLoaded = YES;
    }
}

- (IBAction)buttonActionOfView:(id)sender
{
    [self.webviewForLinks loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    self.webviewScreen.hidden = YES;
    self.webviewForLinks.hidden = YES;
}


-(void) openURLShema:(NSURL*)pdfURL{
    
    if ([pdfURL isFileURL]) { //Local file
        [AbbvieLogging logInfo:@"file"];
    }
    else { //URL Schemas file
        
        //PRONTO-37 Deeplinking to a Page in a PDF
        
        NSString *idAsset = nil;
        
        NSString * selectedURL = pdfURL.absoluteString;
        // check the pageNum in the given URL
        if ([selectedURL rangeOfString:@"pageNum"].location != NSNotFound)
        {
            NSString* selectedPage = [[selectedURL componentsSeparatedByString:@"="] lastObject];
            
            NSInteger pageNum = selectedPage.integerValue;
            
            NSString* selectedAssetURL = [[selectedURL componentsSeparatedByString:@"&"] firstObject];
            
            [ZMUserActions sharedInstance].pageNo = pageNum;
                        
            idAsset = [[[[NSURL URLWithString:selectedAssetURL] host]  stringByRemovingPercentEncoding] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            
            if ([ZMUserActions sharedInstance].pageNo > 0) {
                [self setPage:([ZMUserActions sharedInstance].pageNo)];
            }
            
        }
        else
        {
            idAsset = [[[pdfURL host] stringByRemovingPercentEncoding] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        }
        
        if (idAsset) {
            
//            AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//            [self exitFromCurrentScreenWithAppDelegate:mainDelegate];

            NSArray * assetArray = [[ZMCDManager sharedInstance]getAssetArrayById:idAsset];

            if (assetArray.count > 0) {
                Asset * asset = assetArray[0];
              [[ProntoUserDefaults userDefaults] setPreviousAsset:self.asset];
                NSDictionary *userDic = @{@"asset":asset};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_SCHEMA_ASSET_FOUND" object:self userInfo:userDic];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN_SCHEMA_ASSET_NOT_FOUND" object:self userInfo:nil];
            }
        }
    }
    
}

//Download of an asset if not found in coredata / new

- (void) downloadSelectedAsset:(Asset *)selected {
    AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    _isTapDownloadInitiated = YES;
  _isDownloadignAsset = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [mainDelegate showMessage:@"Downloading asset" whithType:ZMProntoMessagesTypeSuccess];
    });
    [AbbvieLogging logInfo:@"------Asset needs download ------"];
    //PRONTO-25 Web View - Ability to provide a Web link as an asset type.
    if(![selected.file_mime isEqualToString:@"weblink"])
        [self downloadAsset:selected];
}

- (void)downloadAsset:(Asset *)selectedAsset{
//    _currentAssetDownload = selectedAsset;
    _isDownloadignAsset = YES;
//    [self setProgress:0.001000];
    
    [[AssetsServiceClass sharedAPIManager] downloadAsset:selectedAsset withProgress:^(CGFloat progress) {
//        [self setProgress:progress];
    } completion:^{
        selectedAsset.isDownloaded = true;
        selectedAsset.savedPath = selectedAsset.path;
        [self downloadAssetDidFinish:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"completed"];
        // New revamp changes - remove the update label once the asset is downloaded successfully.
        selectedAsset.isUpdateAvailable = NO;
//        [self reloadAssets];
    } onError:^(NSError *error) {
//        [self setProgress:2];
//        [self downloadAssetDidNotFinish:selectedAsset error:error];
        [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAssetOnFailure:selectedAsset];
        [self updateOfflineDownloadCoreData:selectedAsset status:@"failed"];
    }];
}
/**
 *  With the given asset validates if the assets is local or initiates the download
 *  @param selectedAsset
 */
- (void)validateAsset:(Asset *)selectedAsset {
  AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [AbbvieLogging logInfo:@"*****Intro validate Asset ********"];
    [self validateAssetBeforeOpen:selectedAsset completion:^(BOOL assetNeedsDownload) {
        if (assetNeedsDownload) {
            //Restricting user from opening asset if offline download is in progress
            NSArray * offlineDownloadArray = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:selectedAsset.assetID];
            for(OfflineDownload * data in offlineDownloadArray) {
                if(data.assetID == selectedAsset.assetID) {
                    if([data.status isEqualToString:@"in_progress"] || [data.status isEqualToString:@"completed_partialy"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                             [self.mainDelegate showMessage:@"Asset download in progress. Please wait and then try again." whithType:ZMProntoMessagesTypeWarning];
                            //                             if(![selectedAsset.file_mime isEqualToString:@"weblink"])
                            //                                 [self downloadAsset:selectedAsset];
//                            if(!_isTapDownloadInitiated)
//                            {
                                [self downloadSelectedAsset:selectedAsset];
                                [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
//                            }
                        });
//                        if(_apiManager.tappedAsset == 0) {
//                            _apiManager.tappedAsset = data.assetID;
//                        }
                        _isDownloadignAsset = NO;
                    }
                    else {
//                        if(!_isTapDownloadInitiated)
//                        {
                            [self downloadSelectedAsset:selectedAsset];
                            [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
//                        }
                    }
                }
            }
            if(offlineDownloadArray.count == 0) {
//                if(!_isTapDownloadInitiated)
//                {
                    [self downloadSelectedAsset:selectedAsset];
                    [[AssetDownloadProvider sharedAssetDownloadProvider] handleTapToDownloadScenarioForAsset:selectedAsset];
//                }
//                _isDownloadignAsset = NO;
            }
        }else
        {
//            if(self.currentSelectedAssetToSeeDetail)
//            {
//                //Return in case if open of any asset is in progress
//                return;
//            }
//            else
//            {
                self.currentSelectedAssetToSeeDetail = selectedAsset;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [mainDelegate showMessage:@"Opening Asset..." whithType:ZMProntoMessagesTypeSuccess];
                    selectedAsset.isUpdateAvailable = NO;
                });
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [AbbvieLogging logInfo:@"------Asset needs opened ------"];
                    [self openAsset:selectedAsset];
//                    promotedTappedForAudio = false;
                });
            }
//        }
    } failure:^(NSString *messageFailure) {
        [mainDelegate showMessage:messageFailure whithType:ZMProntoMessagesTypeWarning];
    }];
}

/**
 *  validateAssetBeforeOpen : Check if the assets is local or initiates the download
 *
 *  @param asset      asset to validate
 *  @param completion block completion to indicate if asset is valid, these return indicator know if asset needs download or not
 *  @param failure    block failure completion to indicate something was wrong
 */

-(void)validateAssetBeforeOpen:(Asset *)asset
                    completion:(void (^)(BOOL assetNeedsDownload))completion
                       failure:(void (^)(NSString *messageFailure))failure {
    
//    [self trackGetAsset:asset];
    
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:asset.path error:nil] fileSize];
    enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:asset];
    
    if (!_isDownloadignAsset) {
        _isDownloadignAsset = YES;
        // check if asset is protected
        if (![UIApplication sharedApplication].protectedDataAvailable) {
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Protected data is NOT available!!! \n\n !!!!!!!!!!!"];
            /// This should occur during application:didFinishLaunchingWithOptions:
            //[ASFFileProtectionManager stopProtectingLocation:asset.path];
            
            // PRONTO-22 iOS 10 Readiness - Upgrading the ASFKit framework
            NSError *error;
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSDictionary *attrsAssetPath = [fileManager attributesOfItemAtPath:asset.path error:&error];
            
            
            if(![[attrsAssetPath objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete])
            {
                attrsAssetPath = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
                BOOL success = [fileManager setAttributes:attrsAssetPath ofItemAtPath:asset.path error:&error];
                if (!success)
                    [AbbvieLogging logInfo:@"Set ~/Documents attrsAssetPath NOT successfull"];
            }
            
            /// ....
            NSDictionary *attributes = @{NSFileProtectionKey: NSFileProtectionCompleteUntilFirstUserAuthentication};
            [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:asset.path error:nil];
            [AbbvieLogging logInfo:@"!!!!!!!!!!! \n\n Un-protecting it!!! \n\n !!!!!!!!!!!"];
        }
        
        // PRONTO-25  - Web View - Ability to provide a Web link as an asset type.
        if([[NSFileManager defaultManager] fileExistsAtPath:asset.path]) {
            [AbbvieLogging logInfo:[NSString stringWithFormat:@"File Exists at %@", asset.path]];
            
        }
        
        if ((![[NSFileManager defaultManager] fileExistsAtPath:asset.path] && assetType != ZMProntoFileTypesBrightcove) && (assetType != ZMProntoFileTypesWeblink))
        {
            if ([ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
                //                _isDownloadignAsset = YES;
                completion(YES);
            }
            else {
                _isDownloadignAsset = NO;
                failure(@"The internet connection is not available, please check your network connectivity");
            }
        }else{
            //Check if the asset size matches the metada info meaning the asset is correct
            if (asset.file_size.intValue == (int)fileSize) {
                //Mark the asset as read since the asset will open. Has to be here because it needs to guarantee that it will open.
                _isDownloadignAsset = NO;
                completion(NO);
            }else{
                completion(YES);
            }
        }
    } else {
        // PRONTO-16 - Opening an Asset in AirPlane Mode
        if (![ProntoRedesignServerUtilityClass.sharedInstance isConnectionAvailableForYou]) {
            
            if(_isDownloadignAsset == NO)
            {
                
                failure(@"The internet connection is not available, please check your network connectivity");
                
                _isDownloadignAsset = NO;
            }
            
        }
        else
            failure(@"Download in progress. Please wait and then try again.");
    }
}


- (void)downloadAssetDidFinish:(Asset *)asset {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RedesignErrorHandling sharedInstance] hideMessage];
      [[NSNotificationCenter defaultCenter] postNotificationName:RedesignConstants.hideBottomViewNotification object:self userInfo:nil];
    });
    
    _isDownloadignAsset = NO;
//    _isTapDownloadInitiated = NO;
    [[RedesignErrorHandling sharedInstance] showMessage:[NSString stringWithFormat:@"\"%@\" downloaded successfully", asset.title] whithType:ZMProntoMessagesTypeSuccessWithoutIndicator];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self validateAsset:asset];
    });
}
- (void)updateOfflineDownloadCoreData:(Asset *)asset status:(NSString*)status
{
    if([asset valueForKey:@"assetID"] != nil)
    {
        NSMutableDictionary * initialDownloadAsset = [[NSMutableDictionary alloc]init];
        [initialDownloadAsset setObject:[asset valueForKey:@"assetID"] forKey:@"assetID"];
        [initialDownloadAsset setObject:status forKey:@"status"];
        NSArray *offlineData = [[ZMCDManager sharedInstance]getOfflineDownloadArrayById:[asset valueForKey:@"assetID"]];
        
        if (offlineData.count > 0)
        {
            //if  insertation and deletetion has to be happen, it will happen one by one
            [[ZMUserActions sharedInstance] deleteOfflineDownloadData:offlineData[0] withCompletion:^{
                [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
            }];
        }
        else
        {
            [[ZMUserActions sharedInstance] insertNewOfflineDownloadData:initialDownloadAsset withCompletionHandeler:nil];
        }
    }
}


- (void)openAsset:(Asset *)selectedAsset
{
  AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([selectedAsset.type isEqualToString:[EventOnlyAsset TypeAttributeValueAsString]])
    {
        EventOnlyAsset* eoaAsset = [[EventCoreDataManager sharedInstance] getEventOnlyAssetForID:[selectedAsset.assetID stringValue]];
//        [self eventOnlyAssetHandler:nil openAsset:eoaAsset];
    }
    else
    {
        [AbbvieLogging logInfo:[NSString stringWithFormat:@"Asset Path %@", selectedAsset.path]];
      
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [mainDelegate hideMessage];
      });
      
        
        enum ZMProntoFileTypes assetType = [Constant GetFileTypeForAsset:selectedAsset];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UIViewController <ZMAssetViewControllerDelegate> *controller;
        
        MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
        if (assetType == ZMProntoFileTypesPDF) {
            if(document != nil)
            {
                [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
            }
            else{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  [[RedesignErrorHandling sharedInstance] showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                    self.currentSelectedAssetToSeeDetail = nil;
                });
            }
        }
        else if (assetType == ZMProntoFileTypesBrightcove) {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier:@"ZMBrightcove"];
//            controller.library = self;
            controller.asset = selectedAsset;
//            controller.userFranchise = headerView.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
                [self.navigationController pushViewController:controller animated:YES];
        }
        else if (assetType == ZMProntoFileTypesDocument) {
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMDocument"];
//            controller.library = self;
            controller.asset = selectedAsset;
//            controller.userFranchise = headerView.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
                [self.navigationController pushViewController:controller animated:YES];
        }
        // PRONTO-25 Web View - Ability to provide a Web link as an asset type.
        //
        //
        else if(assetType == ZMProntoFileTypesWeblink)
        {
            // open the weblink URL
            controller = (ZMDocument*)[storyboard instantiateViewControllerWithIdentifier: @"ZMWeblink"];
//            controller.library = self;
              controller.asset = selectedAsset;
//            controller.userFranchise = headerView.userFranchise.text;
            //        controller.previousAsset = selectedAsset.title;
          [[ProntoUserDefaults userDefaults] setPreviousAsset:selectedAsset];
            [[HomeViewController sharedInstance] showSafariWithUri:selectedAsset.uri_full];
//                [self.navigationController pushViewController:controller animated:YES];
            
        }
        else if(assetType == ZMProntoFileTypesAudio)
        {

        }
        else
        {
            // Fix - Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** -[NSURL initFileURLWithPath:]: nil string parameter'
            if (assetType == ZMProntoFileTypesPDF) {
                
                if(selectedAsset.path)
                {
                    MFDocumentManager *document = [[MFDocumentManager alloc] initWithFileUrl:[NSURL fileURLWithPath:selectedAsset.path]];
                    if(document != nil)
                    {
                        [self openFastPDFWithAsset:selectedAsset pdfController:controller pdfDocument:document];
                    }else{
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [[RedesignErrorHandling sharedInstance] showMessage:@"Data file is corrupt." whithType:ZMProntoMessagesTypeWarning withTime:2];
                            self.currentSelectedAssetToSeeDetail = nil;
                        });
                    }
                }
            }
        }
    }
}

- (void)openFastPDFWithAsset:(Asset *)asset pdfController:(UIViewController <ZMAssetViewControllerDelegate> *)controller pdfDocument:(MFDocumentManager *)doc{
    
    // Create the PDF view controller.
    controller = [[FastPDFViewController alloc]initWithDocumentManager:doc];
    
    controller.asset = asset;
    
    UIViewController *viewController = [[UIViewController alloc] init];
    viewController.view.frame = CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, viewController.view.frame.size.height);
    [viewController addChildViewController:controller];
    
    UIView *view = [[UIView alloc] init];
    view.frame = viewController.view.bounds;
    view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, view.frame.size.height);
    
    [viewController.view addSubview:view];
    viewController.view.backgroundColor = [RedesignThemesClass sharedInstance].defaultThemeforUser;
    
    controller.view.frame = viewController.view.bounds;
    [viewController.view addSubview:controller.view];
    
    CGRect frame = controller.view.frame;
    frame.origin.y = 104;
    frame.size.height = frame.size.height - frame.origin.y;
    controller.view.frame = frame;
    
    CGRect frame1 = view.frame;
    frame1.origin.y = 0;
    frame1.size.height = 104;
    view.frame = frame1;
        
    UIView *webview = [[UIView alloc] init];
    webview.frame = viewController.view.bounds;
    webview.backgroundColor = [UIColor whiteColor];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]; [webview addSubview:spinner];
    spinner.center = webview.center;
    spinner.color = [UIColor grayColor];
    [viewController.view addSubview:webview];
    [spinner startAnimating];
    UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(viewController.view.center.x - 48, viewController.view.center.y + 24, UIScreen.mainScreen.bounds.size.width, 24)];
      labelView.backgroundColor = [UIColor whiteColor];
      labelView.text = @"Please wait...";
      [labelView setFont:[UIFont boldSystemFontOfSize:16.0]];
      labelView.textColor = [UIColor blackColor];
      [viewController.view addSubview:labelView];

    
    //Added to automatically remove the "opening asset" message when an asset opens from an URL schema
    [[RedesignErrorHandling sharedInstance] hideMessage];
    controller.userFranchise = AOFLoginManager.sharedInstance.salesFranchise;
    controller.viewMain = view;
    controller.viewMain.frame = CGRectMake(controller.viewMain.frame.origin.x, controller.viewMain.frame.origin.y, UIScreen.mainScreen.bounds.size.width, controller.viewMain.frame.size.height);
    controller.webviewScreen = webview;
    controller.webviewScreen.frame = CGRectMake(controller.webviewScreen.frame.origin.x, controller.webviewScreen.frame.origin.y, UIScreen.mainScreen.bounds.size.width, controller.webviewScreen.frame.size.height);
    controller.webviewScreen.hidden = NO;
    
    double delayInSeconds = 2.5; // set the time
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        controller.webviewScreen.hidden = YES;
        [spinner stopAnimating];
      [labelView removeFromSuperview];
    });
    
    //To perform push if the navigation controller is nil
    if(self.navigationController == nil)
    {
        [self getNavigationOfAppDelegate:viewController];
    } else {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}
  
  -(void)getNavigationOfAppDelegate:(UIViewController*)viewController
  {
    if (![[HomeViewController sharedInstance].deeplinkAsset isEqualToString:@""]) {
      [HomeViewController sharedInstance].deeplinkAsset = @"";
      [AOFLoadingController sharedInstance].fromDeeplink = NO;
      AppDelegate * mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController* navigationController = (UINavigationController*)(mainDelegate.window.rootViewController);
        navigationController.view.frame = CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.origin.y, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height);
      int count = (int)navigationController.viewControllers.count;
      for(int i= (count - 1);i>=0;i--){
        if([navigationController.viewControllers[i] isKindOfClass:[HomeViewController class]]){
          [navigationController pushViewController:viewController animated:YES];
          break;
        }
      }
    }
  }



-(void) exitFromCurrentScreenWithAppDelegate:(AppDelegate *)mainDelegate
{
    [[ProntoUserDefaults userDefaults] setPreviousAsset:self.asset];
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - Actions

-(void)pageSliderCancel:(UISlider *)slider {
    [super pageSliderCancel:slider];
    
    [self updatePageNumberLabel];
}

-(void)pageSliderSlided:(UISlider *)slider {
    
    [super pageSliderSlided:slider];
    
    // Get the slider value.
    NSNumber *number = [NSNumber numberWithFloat:[slider value]];
    NSUInteger pageNumber = [number unsignedIntValue];
    
    // Update the label.
    
    [self updatePageNumberLabelWithPage:pageNumber];
}

-(NSString *)pageLabelTitleForPage:(NSUInteger)page {
   
    return PAGE_NUM_LABEL_TEXT((unsigned long)page,(unsigned long)[[self document]numberOfPages]);

}

-(NSString *)pageLabelString {
    
    return [self pageLabelTitleForPage:[self page]];
}

/**
 * This method will update the page number label according to either the
 * pageLabelFormat, if not nil, or the default format and the page and
 * total number of pages.
 */
-(void)updatePageNumberLabel {
    
    //Uncomment to display page number on top
//    NSString *labelTitle = [self pageLabelString];
//    [toolBar setPageNumber:labelTitle];
}

/**
 * This method will update the page number label with an arbitrary page number,
 * for example while the slider is being dragged by the user.
 */
-(void)updatePageNumberLabelWithPage:(NSUInteger)page {
    
    //Uncomment to display page number on top
//    NSString *labelTitle = [self pageLabelTitleForPage:page];
//    [toolBar setPageNumber:labelTitle];
}


#pragma mark -
#pragma mark MFDocumentViewControllerDelegate methods implementation

-(void) documentViewController:(MFDocumentViewController *)dvc didGoToPage:(NSUInteger)page {
    
    //    Page has changed, either by user input or an internal change upon an event: update the label and the
    //    slider to reflect that. If you save the current page as a bookmark to it is a good idea to store the value
    //    in this callback.
    if(self.isLastViewedShown == YES)
    {
        [self updatePageNumberLabel];
    }
}

-(void)documentViewController:(MFDocumentViewController *)dvc willFollowLinkToPage:(NSUInteger)page {
    self.willFollowLink = YES;
}

-(void) documentViewController:(MFDocumentViewController *)dvc didReceiveTapAtPoint:(CGPoint)point {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Skip if we are going to move to a different page because the user tapped on the view to
        // over an internal link. Check the documentViewController:willFollowLinkToPage: callback.
        if(self.willFollowLink) {
            self.willFollowLink = NO;
            return;
        }
        
        if(self.hudHidden) {
            // Show
            [self showThumbnails];
            [toolBar showLastViewed];
            [self updatePageNumberLabel];
            self.isLastViewedShown = YES;
            self.hudHidden = NO;
        } else {
            // Hide
            [self hideThumbnails];
            [toolBar hideLastViewed];
            self.isLastViewedShown = NO;
            self.hudHidden = YES;
        }

    });
}

-(void)toolbar:(FastPDFKitToolbar*)toolbarView didTapLastViewedButton:(UIButton*)sender forAsset:(NSObject*)asset
{
    if (self.fastPDFDocumentDelegate != nil &&
        [self.fastPDFDocumentDelegate respondsToSelector:@selector(pdfDocument:didTapLastViewedForAsset:)])
    {
        [self.fastPDFDocumentDelegate pdfDocument:self didTapLastViewedForAsset:asset];
    }
}
-(void)toolbar:(FastPDFKitToolbar*)toolbarView TapLastViewedButton:(NSObject*)asset
{
    if (self.fastPDFDocumentDelegate != nil &&
        [self.fastPDFDocumentDelegate respondsToSelector:@selector(pdfDocument:didTapLastViewedForAsset:)])
    {
        [self.fastPDFDocumentDelegate pdfDocument:self didTapLastViewedForAsset:asset];
    }
}
#pragma mark - Print _Actions

-(void)printAction:(id)sender {
    UIPrintInteractionController *pc = [UIPrintInteractionController
                                        sharedPrintController];
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.orientation = UIPrintInfoOrientationPortrait;
    printInfo.jobName =@"Report";
    
    pc.printInfo = printInfo;
    pc.printingItem = [NSURL fileURLWithPath:self.asset.path];
    
    
    UIPrintInteractionCompletionHandler completionHandler =
    ^(UIPrintInteractionController *printController, BOOL completed,
      NSError *error) {
        if(!completed && error){
            NSLog(@"Print failed - domain: %@ error code %ld", error.domain,
                  (long)error.code);
        }
    };
    
    UIBarButtonItem *item = sender ;
    UIView *view = [item valueForKey:@"view"];
    
    [pc presentFromRect:view.frame inView:self.view animated:YES completionHandler:completionHandler];
}

#pragma mark - BookmarkViewController, _Delegate and _Actions

-(BookmarkViewController *)bookmarksViewController {
    if(!_bookmarksViewController) {
        _bookmarksViewController = [[BookmarkViewController alloc]initWithNibName:@"BookmarkView" bundle:[NSBundle bundleForClass:[self class]]];
    }
    return _bookmarksViewController;
}

-(void)dismissBookmarkViewController:(BookmarkViewController *)bvc {
    
    self.bookmarkButtonSender = nil;
    [self dismissAlternateViewController];
}

-(void)bookmarkViewController:(BookmarkViewController *)bvc didRequestPage:(NSUInteger)page{
    
    self.page = page;
    
    [self dismissAlternateViewController];
}

-(void)presentBookmarksViewController:(id)sender {
    
    //    We create an instance of the BookmarkViewController and push it onto the stack as a model view controller, but
    //    you can also push the controller with the navigation controller or use an UIActionSheet.
    
    if (self.currentReusableView == FPKReusableViewBookmarks) {
        
        [self dismissAlternateViewController];
        
    } else {
        
        self.currentReusableView = FPKReusableViewBookmarks;
        
        BookmarkViewController * bookmarksVC = self.bookmarksViewController;
        self.documentId = [self.asset.assetID stringValue];
        bookmarksVC.delegate = self;
        [self presentViewController:bookmarksVC barButtonItem:toolBar.bookIconFast contentSize:self.popoverContentSize sender:sender];
    }
}

-(void)bookmarkAction:(id)sender {
    self.bookmarkButtonSender = sender;
    self.originSize = self.view.frame.size.width;
    [self presentBookmarksViewController:sender];
}

#pragma mark - SearchViewControllerDelegate

-(MFDocumentManager *)documentForSearchViewController:(SearchViewController *)controller {
    return self.document;
}

-(SearchManager *)searchForSearchViewController:(SearchViewController *)controller {
    return [self searchManager];
}

-(void)searchViewController:(SearchViewController *)controller setPage:(NSUInteger)page withZoomOfLevel:(float)zoomLevel onRect:(CGRect)rect {
    [self setPage:page withZoomOfLevel:zoomLevel onRect:rect];
}

-(void)searchViewController:(SearchViewController *)controller addSearch:(SearchManager *)searchManager {
    
    [self addOverlayViewDataSource:searchManager name:@"FPKSearchManager"];
    [self reloadOverlay];
}

-(void)searchViewController:(SearchViewController *)controller removeSearch:(SearchManager *)searchManager {
    
    [self removeOverlayViewDataSourceWithName:@"FPKSearchManager"];
    [self reloadOverlay];
}

-(void)searchViewController:(SearchViewController *)controller switchToMiniSearchView:(FPKSearchMatchItem *)item {
    
    [self dismissSearchViewController:self.searchViewController];
    [self presentMiniSearchViewWithStartingItem:item];
}

-(NSUInteger)pageForSearchViewController:(SearchViewController *)controller {
    return self.page;
}

-(void)dismissSearchViewController:(SearchViewController *)aSearchViewController {
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        /* Dismiss the popover on iPad pre iOS 8 */
        
        if([UIPopoverPresentationController class]) {
            if(self.presentedViewController) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else
    {
        /* Dismiss the presented view controller on iPad iOS 8 and iPhone */
        if(self.presentedViewController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
    self.currentReusableView = FPKReusableViewNone;
}

#pragma mark - Search stuff

-(void)revertToFullSearchView:(UIBarButtonItem *)barButtonItem {
    
    [self dismissMiniSearchView];
    [self presentFullSearchView:barButtonItem];
}

-(void)handleSearchGotCancelledNotification:(NSNotification *)notification {
    
    if(self.currentReusableView == FPKReusableViewSearch) {
        [self dismissSearchViewController:self.searchViewController];
    }
}

-(void)handleSearchUpdateNotification:(NSNotification *)notification {
    
    NSDictionary * userInfo = notification.userInfo;
    NSInteger page = [userInfo[kNotificationSearchInfoPage] integerValue];
    NSInteger delta = page - self.page;
    
    if(self.isViewLoaded && (delta < 2)) {
        // We get up to two false 'current' page positives but it is good enogh for now.
        [self reloadOverlay];
    }
}

-(void)presentFullSearchView:(id)sender {
    
    // Get the full search view controller lazily, set it upt as the delegate for
    // the search manager and present it to the user modally.
    
    SearchViewController * controller = self.searchViewController;
    controller.delegate = self;
    controller.searchManager = [self searchManager];
    
    // Enable overlay and set the search manager as the data source for
    // overlay items.
    self.overlayEnabled = YES;
    
    self.currentReusableView = FPKReusableViewSearch;
    self.currentSearchViewMode = FPKSearchViewModeFull;
    
    CGSize popoverContentSize = CGSizeMake(450, 650);
    [self presentViewController:controller
                  barButtonItem:toolBar.searchIconFast
                    contentSize:popoverContentSize sender:sender];
}

-(MiniSearchView *)miniSearchView {
    if(!_miniSearchView) {
        // If nil, allocate and initialize it.
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            MiniSearchView * view = [[MiniSearchView alloc]initWithFrame:CGRectMake(0, -45, self.view.bounds.size.width, 44)];
            view.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
            [view setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin];
            self.miniSearchView = view;
        } else {
            
            MiniSearchView * view = [[MiniSearchView alloc]initWithFrame:CGRectMake(0, -45, self.view.bounds.size.width, 44)];
            view.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75];
            [view setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin];
            self.miniSearchView = view;
        }
    }
    
    return _miniSearchView;
}

-(void)presentMiniSearchViewWithStartingItem:(FPKSearchMatchItem *)item {
    
    // This could be rather tricky.
    
    // This method is called only when the (Full) SearchViewController. It first instantiate the
    // mini search view if necessary, then set the mini search view as the delegate for the current
    // search manager - associated until now to the full SVC - then present it to the user.
    
    if(self.miniSearchView.superview != nil) {
        [self.miniSearchView removeFromSuperview];
    }
    
    // Set up the connections
    
    self.miniSearchView.delegate = self;
    
    // Update the view with the right index.
    [self.miniSearchView reloadData];
    self.miniSearchView.currentSearchResultIndex = [[[self searchManager] allSearchResults] indexOfObject:item];
    
    // Add the subview and referesh the superview.
    [[self view]addSubview:self.miniSearchView];
    
    [UIView animateWithDuration:0.25f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                             
                             self.miniSearchView.frame = CGRectMake(0, toolBar.frame.size.height, self.view.bounds.size.width, 44);
                             self.miniSearchView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                             
                         } else {
                             
                             self.miniSearchView.frame = CGRectMake(0, toolBar.frame.size.height, self.view.bounds.size.width, 44);
                             self.miniSearchView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                         }
                     }
                     completion:^(BOOL finished){
                         
                         [self.view bringSubviewToFront:toolBar];
                         
                         self.currentReusableView = FPKReusableViewSearch;
                         self.currentSearchViewMode = FPKSearchViewModeMini;
                     }];
}

-(SearchManager *)searchManager {
    id<FPKOverlayViewDataSource> dataSource = [self overlayViewDataSourceWithName:@"FPKSearchManager"];
    if([dataSource isKindOfClass:[SearchManager class]]) {
        return (SearchManager *)dataSource;
    }
    return nil;
}

/**
 * SearchViewController, lazily allocated.
 */
-(SearchViewController *)searchViewController {
    
    // Lazily allocation when required.
    
    if(!_searchViewController) {
        
        // We use different xib on iPhone and iPad.
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            _searchViewController = [[SearchViewController alloc]initWithNibName:@"SearchView2_pad" bundle:[NSBundle bundleForClass:[self class]]];
        } else {
            _searchViewController = [[SearchViewController alloc]initWithNibName:@"SearchView2_phone" bundle:[NSBundle bundleForClass:[self class]]];
        }
    }
    return _searchViewController;
}

-(void)searchAction:(id)sender {
    
    // Get the instance of the Search Manager lazily and then present a full sized search view controller
    // to the user. The full search view controller will allow the user to type in a search term and
    // start the search. Look at the details in the utility method implementation.
    self.searchButtonSender = sender;
    self.originSize = self.view.frame.size.width;
    if(self.currentReusableView!= FPKReusableViewSearch) {
        
        if(self.currentSearchViewMode == FPKSearchViewModeMini) {
            
            [self revertToFullSearchView:sender];
            
        } else {
            
            [self presentFullSearchView:sender];
        }
        
    } else {
        
        if(self.currentSearchViewMode == FPKSearchViewModeMini) {
            
            [self revertToFullSearchView:sender];
            
        } else if (self.currentSearchViewMode == FPKSearchViewModeFull) {
            
            [self dismissAlternateViewController];
            
        }
    }
}

#pragma mark - MiniSearchViewDelegate

-(NSUInteger )numberOfSearchResults:(MiniSearchView *)view {
    
    NSUInteger count = [[[self searchManager] allSearchResults] count];
    return count;
}

-(FPKSearchMatchItem *)miniSearchView:(MiniSearchView *)view searchResultAtIndex:(NSUInteger)index {
    
    
    return [[[self searchManager] allSearchResults] objectAtIndex:index];
}

-(void)miniSearchView:(MiniSearchView *)view
              setPage:(NSUInteger)page
            zoomLevel:(float)zoomLevel
                 rect:(CGRect)rect {
    
    [self setPage:page withZoomOfLevel:zoomLevel onRect:rect];
}

-(void)dismissMiniSearchView:(MiniSearchView *)view {
    
    [self dismissMiniSearchView];
}

-(void)revertToFullSearchViewFromMiniSearchView:(MiniSearchView *)view {
    
    // Dismiss the minimized view and present the full one.
    [self revertToFullSearchView:_searchButtonSender];
}

-(void)cancelSearch:(MiniSearchView *)view {
    
    [self dismissMiniSearchView];
    self.searchButtonSender = nil;
    [[self searchManager] stopSearch];
    [self removeOverlayViewDataSourceWithName:@"FPKSearchManager"];
    [self reloadOverlay];
}

-(void)dismissMiniSearchView {
    
    // Animation.
    FastPDFViewController * __weak this = self;
    [UIView animateWithDuration:0.25f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                             
                             this.miniSearchView.frame = CGRectMake(0,-45 , this.view.bounds.size.width, 44);
                         } else {
                             
                             this.miniSearchView.frame = CGRectMake(0,-45 , this.view.bounds.size.width, 44);
                         }
                     }
                     completion:^(BOOL finished){
                         // Actual removal.
                         if(this.miniSearchView!=nil) {
                             
                             [this.miniSearchView removeFromSuperview];
                         }
                     }];
}

-(void)showMiniSearchView {
    
    // Remove from the superview and release the mini search view.
    
    FastPDFViewController * __weak this = self;
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            
            this.miniSearchView.frame = CGRectMake(0,66 , this.view.bounds.size.width, 44);
            this.miniSearchView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
            
        }else {
            
            this.miniSearchView.frame = CGRectMake(0,66 , this.view.bounds.size.width, 44);
            this.miniSearchView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        }
    } completion:NULL];
}
#pragma mark ZMAssetViewControllerDelegate Methods

-(void)assetViewController:(ZMAssetViewController*)assetVCView didTapLastViewedButton:(UIButton*)sender forAsset:(NSObject*)asset{
    
}
    
-(void)pdfDocument:(FastPDFViewController*)pdfDocumentView didTapLastViewedForAsset:(NSObject*)asset{
    
}
#pragma mark TextDisplayViewControllerDelegate Methods

-(void)dismissTextDisplayViewController:(TextDisplayViewController *)controller{
    
}
#pragma mark OutlineViewControllerDelegate Methods

-(void)dismissOutlineViewController:(OutlineViewController *)ovc{
    
}
@end
