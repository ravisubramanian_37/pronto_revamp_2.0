//
//  FastPDFViewController.h
//  Pronto
//
//  Created by Praveen Kumar on 20/06/19.
//  Copyright © 2019 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FastPdfKit/FastPdfKit.h>
#import "ZMAssetViewController.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@class BookmarkViewController;
@class SearchViewController;
@class MiniSearchView;

#define iPadPro10_widthVal 834.0
#define iPadProGlobalVal 768.0
#define iPadPro12_widthVal 920.0

@interface FastPDFViewController : MFDocumentViewController<UICollectionViewDelegate,MFDocumentViewControllerDelegate,ZMAssetViewControllerDelegate,TextDisplayViewControllerDelegate,SearchViewControllerDelegate,BookmarkViewControllerDelegate,OutlineViewControllerDelegate,MiniSearchViewControllerDelegate,UIPopoverControllerDelegate,WKNavigationDelegate,WKUIDelegate,UINavigationControllerDelegate,MFMailComposeViewControllerDelegate>

+ (FastPDFViewController *)sharedInstance;

/**
 *  Asset to Populate document on FastPDFDOCUMENT controller
 */
@property (strong, nonatomic) Asset *asset;

/**
 *  Theme controller
 */
@property (weak, nonatomic) id <ZMProntoTheme> theme;
/**
 *  Header to manage custom controls into FastPDFController
 */
@property (strong, nonatomic) UIView *headerView;
/**
 *  Reference to ZMLibrary controller to perfom custom methods
 */
//@property (weak, nonatomic) ZMLibrary *library;
/**
 *  Reference to Competitors controller to perfom custom methods
 */
//@property (weak, nonatomic) Competitors *competitors;
/**
 *  Reference to ToolsTraining controller to perfom custom methods
 */
//@property (weak, nonatomic) ToolsTraining *toolsTraining;

/**
 *  Reference to Events controller to perfom custom methods
 */
@property (weak, nonatomic) EventsViewController *eventsViewController;

/**
 *  Reference to Calendar Detail View controller to perfom custom methods
 */
@property (weak, nonatomic) CalendarDetailVC *calendarDetailVC;


/**
 *  Bool to indicate if a new folder was created
 */
@property (nonatomic) BOOL *folderCreated;
/**
 *  Tracking into Omniture
 */
@property (nonatomic) int tierTrackingValue;
/**
 *  Perform nigthMode into document
 */
@property (nonatomic) BOOL nightMode;

@property (strong,nonatomic) NSString * userFranchise;

@property (strong,nonatomic) NSString * previousAsset;

-(void)searchAction:(id)sender;

-(void)bookmarkAction:(id)sender;

-(void)printAction:(id)sender;

/**
 *  This value provides detail of type of asset being opened
 */
@property (nonatomic, assign) BOOL isEventOnlyAsset;

@property (nonatomic, assign) BOOL isLastViewedShown;

@property (strong, nonatomic) UIView *viewMain;

@property (strong, nonatomic) UIView *webviewScreen;

@property (nonatomic, weak) id<ZMAssetViewControllerDelegate> fastPDFDocumentDelegate;
// New Search Feature - commenting as of now and will have it as a new story
//-(void) highlightKeywords : (FastPDFDocument *)document;

@property (nonatomic, retain) BookmarkViewController * bookmarksViewController;
@property (nonatomic, retain) SearchViewController * searchViewController;
@property (nonatomic, retain) MiniSearchView * miniSearchView;
@property (nonatomic, readwrite) CGSize popoverContentSize;
@property (nonatomic, readwrite, getter = isMultimediaVisible) BOOL multimediaVisible;

//@property (nonatomic, retain) UIPopoverController * reusablePopover;
-(void)closeButtonDidPress;
- (void)validateAsset:(Asset *)selectedAsset;
- (void)openAsset:(Asset *)asset;
- (void)setAsset:(Asset *)asset;
- (void)resetAssetValue;
@end

NS_ASSUME_NONNULL_END
