//
//  ZMProntoTheme.m
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import "ZMProntoTheme.h"
#import "ZMProntoPhase3.h"

@implementation ZMProntoTheme

    
    + (id <ZMProntoTheme>)sharedTheme
    {
        static id <ZMProntoTheme> sharedTheme = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            /*
            * Change the allocated class to change the theme of the application
            * Check ZMProntoTheme (ZMProntoTheme.h) protocol to view the requiered and optional skin defined
            */
             sharedTheme = [[ZMProntoPhase3 alloc] init];
        });
        return sharedTheme;
    }
    
@end
