//
//  ZMProntoTheme.h
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMBadge.h"
#import "ZMHeader.h"

@protocol ZMProntoTheme <NSObject>
    
@required

/**
 *  Clears a view of color
 *  @param view reference to the view
 */
-(void) clear:(UIView *)view;

/**
 * @required
 * gives style to the main view
 **/
-(void) styleMainView:(UIView *)view;

/**
 * @required
 * Indicates a successful or positive action
 **/
- (void) successButton:(UIButton *)button;

/**
 * @required
 * Indicates caution should be taken with this action
 **/
- (void) warningButton:(UIButton *)button;

/**
 * @required
 * Indicates dark colour button
 **/
- (void) blackButton:(UIButton *)button;

/**
 * @required
 * Indicates white colour button
 **/
- (void) whiteButton:(UIButton *)button;

/**
* @required
* Indicates dark colour button
**/
- (void) goldButton:(UIButton *)button;

/**
 * @required
 * Tints a button with an image
 **/
-(void) tintButton:(UIButton *)button;

/**
 *  @required
 *  Tints the Primary button
 *  @param button reference to the button
 */
-(void) tintPrimaryButton:(UIButton *)button;

/**
 * Tints the image button and the label
 **/
-(void) toggleButton:(UIButton *)button;

/**
 * Tints the image button and the label when is selected
 **/
-(void) toggleButtonSelected:(UIButton *)button;

/**
 *  Styling primary buttons
 *  @param button button uibutton to style
 */
-(void) stylePrimaryButton:(UIButton *)button;

/**
 *  Styling secondary buttons
 *  @param button button uibutton to style
 */
-(void) styleSecondaryButton:(UIButton *)button;

/**
 *  Styles the item frame
 *  @param view view
 */
-(void) styleItemFrame:(UIView *)view;

/**
 *  Styles the rating element with a certain color
 *  @param color UIColor
 *  @return reference of the image
 */
-(UIImage *) ratingElementWithColor:(UIColor*)color;

/**
 *  Styles the rating element
 *  @return reference of the styled image
 */
-(UIImage *) ratingElementSelected;

/**
 *  Style the not selected element
 *  @return reference of the image with the given styling
 */
-(UIImage *) ratingElementNotSelected;

/**
 *  Style the breadcub on the application
 *  @return NSFontAttributeName with the attributes
 */
-(NSDictionary *) breadCumbBrandStyle;

/**
 *  Style of the pipe
 *  @return NSFontAttributeName with the attributes for the pipe separator
 */
-(NSDictionary *) breadCumbPipeStyle;

/**
 *  General style for the breadcumb
 *  @return NSFontAttributeName for the general styles
 */
-(NSDictionary *) breadCumbGeneralStyle;

/**
 *  Styles the main total on the breadcumb
 *  @return NSFontAttributeName with the styles for the total
 */
-(NSDictionary *) breadCumbTotalStyle;

/**
 *  Tints the arrow
 *  @param imageView reference to the image
 */
-(void) tintArrow:(UIImageView *) imageView;

//Revamp(13/12)
/**
 *  Tints the arrow as for notification
 *  @param imageView reference to the image
 */
-(void) notificationsArrow:(UIImageView *) imageView;

/**
 *  Tints the setting button
 *  @param button reference to the button
 */
-(void) tintSettingsButton:(UIButton *)button;

/**
 *  Tints the close card image
 *  @param imageView reference to the image
 */
-(void) tintCloseCard:(UIImageView *)imageView;

/**
 *  Logo card styling
 *  @param imageView reference to the pronto logo card
 */
-(void) tintProntoLogoCard:(UIImageView *)imageView;

/**
 *  Logo card styling
 *  @param imageView reference to the pronto logo card
 */
-(void) tintProntoLogoCardDark:(UIImageView *)imageView;

/**
 *  Logo card styling
 *  @param imageView reference to the pronto logo card
 */
-(void) tintProntoLogoCardGold:(UIImageView *)imageView;

/**
 *  style the label with light color
 *  @param label reference to the label
 */
-(void) styBrightLabel:(UIView *)label;

//Revamp - Colour Adding
/**
 *  style the label with dark color
 *  @param label reference to the label
 */
-(void) styDarkLabel:(UIView *)label;

/**
 *  style the label with grey color
 *  @param label reference to the label
 */
-(void) styGrayLabel:(UIView *)label;

/**
 *  Styling the blue background pannel for notifications
 *  @param view reference to the uiview pannel
 */
-(void) styleBlueNotificationPannel:(UIView *)view;
//End of Revamp

/**
 *  Stles the divider with a certain style
 *  @param view reference to the uiview to syle
 */
-(void) styleDivider:(UIView *)view;

/**
 *  Styling the transparente pannel
 *  @param view reference to the uiview pannel
 */
-(void) styleTransparentPannel:(UIView *)view;

/**
 *  Style refresh controll
 *  @param refresh reference to the refresh control
 */
-(void) brightRefresh:(UIRefreshControl *)refresh;

/**
 *  Tints the arrow with a dark color
 *  @param imageView reference to the image
 */
-(void) tintArrowDark:(UIImageView *) imageView;

/**
 *  Style the warning message
 *  @param view reference to the main view
 *  @param label reference to the uilabel of the warning message
 */
-(void) styleWarningMessage:(UIView *)view label:(UILabel*)label;

/**
 *  Styles the Alert message
 *  @param view reference to the main view
 *  @param label reference to the uilabel of the warning message
 */
-(void) styleAlertMessage:(UIView *)view label:(UILabel*)label;

/**
 *  Styles the Success message
 *  @param view reference to the main view
 *  @param label reference to the uilabel of the warning message
 */
-(void) styleSuccessMessage:(UIView *)view label:(UILabel*)label;

/**
 *  Styles the Success message
 *  @param view reference to the activity indicator view
 */
-(void) styleActivityIndicatorColor:(UIActivityIndicatorView *)view;

/**
 *  Styles the Success message
 *  @param view reference to the activity indicator view
 */
-(void) styleActivityIndicatorColorWhite:(UIActivityIndicatorView *)view;

/**
 *  Styles the Sync message
 *  @param view reference to the main view
 *  @param label reference to the uilabel of the warning message
 */
-(void) styleSyncMessage:(UIView *)view label:(UILabel*)label;

/**
 *  Styles the Reload message
 *  @param view reference to the main view
 *  @param label reference to the uilabel of the warning message
 */
-(void) styleReloadMessage:(UIView *)view label:(UILabel*)label;


/**
 *  Styles the transparent pannel
 *  @param view reference to the view container
 */
-(void) styleTransparentPannelFilters:(UIView *)view;

/**
 *  Styles the transparent pannel
 *  @param view reference to the view container
 */
-(void) styleTransparentBrandPannelFilters:(UIView *)view;

/**
 *  Tints the the reset button
 *  @param button styles the reset button
 */
-(void) tintButtonReset:(UIButton *)button;

/**
 *  Search box styling
 *  @param view reference of the box container
 */
-(void) searchBoxContainer:(UIView *)view;

// Pronto new revamp chnages: lato font style for sort button
/**
 *  Sort Button style
 */
-(NSDictionary *) sortButtonTitleStyle;
// End:
/**
 *  Button style
 *  @return reference syle for the reset button
 */
-(NSDictionary *) buttonTitleStyle;

/**
 *  Button title main style
 *  @return reference style for the main title
 */
-(NSDictionary *) buttonTitleMainStyle;

/**
 *  Gives the reference for the rating elemente when is selected
 *  @return image reference
 */
-(UIImage *) ratingBigElementSelected;

/**
 *  Gives the reference for the rating elemente when is not selected
 *  @return image reference
 */
-(UIImage *) ratingBigElementNotSelected;

/**
 *  Gives the reference for the rating elemente when is selected with contrast
 *  @return image reference
 */
-(UIImage *) ratingBigElementNotSelectedContrast;

/**
 *  Gives style the unread and read icon
 *  @param view reference to the icon provided on the interface
 */
-(void) readUnreadIcon:(UIView *)view;

/**
 *  styles the read label
 *  @param label reference to the label
 */
-(void) styReadLabel:(UILabel *)label;

//Revamp - Dark grey label define
/**
 *  styles the read label dark
 *  @param label reference to the label
 */
-(void) styReadLabelDark:(UILabel *)label;
//End of Revamp

/**
 *  Styles an image with a bright color
 *  @param image referece
 *  @return reference
 */
-(UIImage *) brightImage:(UIImage*)image;

/**
 *  Styles an image with a bright color
 *  @param image referece
 *  @return reference
 */
-(void) tintLinkIcon:(UIImageView *)imageView;


/**
 *  Styles an image with a bright color
 *  @param image referece
 *  @return reference
 */
-(void) tintLinkIconVisited:(UIImageView *)imageView;


@optional
/**
 * @optional
 * Indicates a dangerous or potentially negative action
 **/
- (void) dangerButton:(UIButton *)button;

/**
 * @optional
 * styles the badge
 **/
-(void) styleBadge:(ZMBadge *)badge franchise:(NSString*) franchiseName;

/**
 * @optional
 * styles the header
 **/
-(void) styleHeader:(UIView *)header;

//Revamp - Function Declaration
/**
 * @optional
 * styles the header
 **/
-(void) styleHOUser:(UIView *)headerView;

//Add parameter for FG
/**
 * @optional
 * styles the header
 **/
-(void) styleFGUser:(UIView *)headerView franchise:(NSString*) franchiseName colorCode:(NSString*) franchiseColor;

//New function def for getting colour
-(NSString *) getColorForFranchise:(NSString*) franchiseName;

//End of Revamp

/**
 * @optional
 * styles the header
 **/
-(void) styleHeaderSecond:(UIView *)header;

/**
 * @optional
 * styles the header third pannel
 **/
-(void) styleHeaderThird:(UIView *)headerView;

/**
 *  Styles the primary view
 *  @param view reference to the view
 */
-(void) stylePrimaryView:(UIView *)view;


/**
 *  Styles the separator
 *  @param view reference to the view
 */
-(void) tintSeparator:(UIButton *)button;


-(void) brightLabelDisable:(UILabel *)label;

// New Revamping - Logo Image Colour - White
-(void) logoImage:(UIImageView *) imageView;
-(void) logoImageHO:(UIImageView *) imageView;
-(void) logoImageFG:(UIImageView *) imageView;

-(void) labelColorHO:(UILabel *)label;

-(void) labelColorFU:(UILabel *)label colorCode:(NSString*) franchiseColor;

// New Revamping changes - Font Color
- (UIColor *) whiteFontColor;
- (UIFont*) normalLato;
- (UIFont*) boldLato;
- (UIFont*) smallLato;
- (UIFont*) bigBoldLato;
- (UIFont*) bigNormalLato;
- (UIColor *) franchiseCountColor;
- (UIFont*) bigBoldLatoWithFontSize16;
- (UIFont*) bigBoldLatoWithFontSize17;

@end

@interface ZMProntoTheme : NSObject
    /**
     * @public static
     * Returns the current theme instance
     **/
    + (id <ZMProntoTheme>)sharedTheme;

@end
