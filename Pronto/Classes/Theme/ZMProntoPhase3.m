//
//  ZMProntoPhase3.m
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import "ZMProntoPhase3.h"
#import "ZMBadge.h"
#import "ZMHeader.h"
#import "UIImage+Util.h"
#import "UIColor+ColorDirectory.h"

@implementation ZMProntoPhase3

/**
 * White
 **/
+ (UIColor *)brightColor
{
    return [UIColor darkTextColor];
//    return [UIColor whiteColor];
}

/**
 * gold
 **/
+ (UIColor *)goldColor
{
    return [UIColor GetColorFromHexValueaa8b40];
}

//Revamp - Adding colours
/**
 * Black
 **/
+ (UIColor *)darkColor
{
    return [UIColor blackColor];
}

/**
 * Grey
 **/
+ (UIColor *)greyColor
{
    return [UIColor grayColor];
}

+ (UIColor *)popOverSelectedItemBlueColor
{
    return [UIColor colorWithRed:14.0/255 green:57.0/255 blue:143.0/255 alpha:1];
}

/**
 * blue
 **/
+ (UIColor *)blueColor
{
    return [UIColor colorWithRed:67.0/255.0 green:137.0/255.0 blue:192.0/255.0 alpha:1];
}
//End of Revamp

/**
 * White with transparency
 **/
+ (UIColor *)brightColorWhitTransparency
{
    return [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:.3];
}


/**
 * Black with transparency
 **/
+ (UIColor *)darkColorWhitTransparency
{
    return [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:.8];
}

//Revamp - Notification blue
/**
 * Blue color for notification BG
 **/
+ (UIColor *)blueForNotification
{
    return [UIColor colorWithRed:218.0/255.0 green:225.0/255.0 blue:234.0/255.0 alpha:1];
}
//End of Revamp

/**
 * almost dark
 **/
+ (UIColor *)almostDarkColor
{
    return [UIColor colorWithRed:10.0/255.0 green:10.0/255.0 blue:10.0/255.0 alpha:1];
}



/**
 * Dark gray
 **/
+ (UIColor *)applicationBackground
{
    return [UIColor colorWithRed:36.0/255.0 green:36.0/255.0 blue:36.0/255.0 alpha:1];
}

//Revamp - colors for header
+ (UIColor *)fieldUserBG:(NSString*) hexString
{
    return [self colorWithHexString:hexString];
}

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert
{
    //remove the #
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    // remove + and $
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]];
    
    unsigned hex;
    if (![scanner scanHexInt:&hex]) return nil;
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0f];
}

/**
 * Home Office Blue
 **/
+ (UIColor *)homeOfficeBG
{
    return [UIColor colorWithRed:13.0/255.0 green:28.0/255.0 blue:73.0/255.0 alpha:1];
}
//End of Revamp

/**
 * Mid gray
 **/
+ (UIColor *)thirdBackground
{
    //return [UIColor colorWithRed:56.0/255.0 green:56.0/255.0 blue:56.0/255.0 alpha:1];
    return [UIColor clearColor];
}



/**
 * Mid gray
 **/
+ (UIColor *)serarchBoxColor
{
    return [UIColor colorWithRed:222.0/255.0 green:220.0/255.0 blue:224.0/255.0 alpha:1];
}



/**
 * Low gray
 **/
+ (UIColor *)secondaryBackground
{
    //return [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];
    return [UIColor clearColor];
}



/**
 * Grid Item frame color
 **/
+ (UIColor *)frameColor
{
    return [UIColor clearColor];
    //[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:1];
    // To avoid yellow shade in the assets
    //[UIColor colorWithRed:254.0/255.0 green:191.0/255.0 blue:1.0/255.0 alpha:1];
}

/**
 * Pronto's orange
 **/
+ (UIColor *)applicationColor
{
    return [UIColor colorWithRed:244.0/255.0 green:122.0/255.0 blue:1.0/255.0 alpha:1];
}

/**
 * Pronto's orange
 **/
+ (UIColor *)warningColour
{
    return [UIColor colorWithRed:241.0/255.0 green:180.0/255.0 blue:52.0/255.0 alpha:1];
}

// New Revamping
/**
 * Pronto's checkBox blue
 **/
+ (UIColor *)checkBoxColor
{
    return [UIColor colorWithRed:53.0/255.0 green:173.0/255.0 blue:237.0/255.0 alpha:1];
    
}

//Revamp(13/12)
/**
 * Pronto's notification detail color
 **/
+ (UIColor *)notificationsArrow
{
    return [UIColor colorWithRed:218.0/255.0 green:223.0/255.0 blue:232.0/255.0 alpha:1];
    
}


/**
 * Pronto's orange
 **/
+ (UIColor *)alertColor
{
    return [UIColor colorWithRed:237.0/255.0 green:29.0/255.0 blue:39.0/255.0 alpha:1];
}

//Revamp(12/12)
/**
 * Field user green
 **/
+ (UIColor *)alertColorGreen
{
    return [UIColor colorWithRed:127.0/255.0 green:255.0/255.0 blue:0/255.0 alpha:1];
}


/**
 * Gray pipe color
 **/
+ (UIColor *)pipeColor
{
    return [UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1];
}



/**
 * Gray pipe color
 **/
+ (UIColor *)viewedColor
{
    return [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1];
}

//Revamp - Dark gray color
/**
 * Dark Gray pipe color
 **/
+ (UIColor *)viewedColorDark
{
    return [UIColor darkGrayColor];
}
//End of Revamp


/**
 * red alert color
 **/
+ (UIColor *)alertMessageColor
{
    return [UIColor whiteColor];
//    return [UIColor colorWithRed:242.0/255.0 green:24.0/255.0 blue:35.0/255.0 alpha:1];
}



/**
 * blue message color
 **/
+ (UIColor *)successColor
{
    return [UIColor colorWithRed:53.0/255.0 green:173.0/255.0 blue:237.0/255.0 alpha:1];
}

/**
 * gold view color
 **/
+ (UIColor *)syncColor
{
    return [UIColor GetColorFromHexValuefaf1de];
}

/**
 * separator color
 **/
+ (UIColor *)separatorColor
{
    return [UIColor grayColor];
    //[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1];
}


+ (UIColor *)disableLabelColor
{
//    return [UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:.8];
    return [UIColor grayColor];
}

#pragma mark - Rating Item

-(UIImage *) brightImage:(UIImage*)image
{
    return [image imageWithColor:[ZMProntoPhase3 brightColor]];
}


-(UIImage *) ratingBigElementWithColor:(UIColor*)color
{
    return [[UIImage imageNamed:@"startBig"] imageWithColor:color];
}


-(UIImage *) ratingElementWithColor:(UIColor*)color
{
    return [[UIImage imageNamed:@"rateIcon"] imageWithColor:color];
}


-(UIImage *) ratingElementSelected
{
    return [self ratingElementWithColor:[ZMProntoPhase3 applicationColor]];
}


-(UIImage *) ratingElementNotSelected
{
    return [self ratingElementWithColor:[ZMProntoPhase3 thirdBackground]];
}


-(UIImage *) ratingBigElementSelected
{
    return [self ratingBigElementWithColor:[ZMProntoPhase3 applicationColor]];
}


-(UIImage *) ratingBigElementNotSelected
{
    return [self ratingBigElementWithColor:[ZMProntoPhase3 thirdBackground]];
}

-(UIImage *) ratingBigElementNotSelectedContrast
{
    return [self ratingBigElementWithColor:[ZMProntoPhase3 brightColor]];
}




#pragma mark - Grid Item


-(UIImage *) shadowView:(UIView *)view withColor:(UIColor*)color
{
    return [[UIImage imageNamed:@"shadow"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 12)];
}


-(void) addShadowToView:(UIView *)view
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.5f, 3.0f);
    view.layer.shadowOpacity = 0.3f;
    view.layer.shadowPath = shadowPath.CGPath;
}


-(void) styleItemFrame:(UIView *)view
{
    // color for tile - blue
    view.backgroundColor = [UIColor colorWithRed:0.06 green:0.15 blue:0.36 alpha:1.0];
    //[UIColor greenColor];
    //[ZMProntoPhase3 frameColor];
}


-(void) tintSettingsButton:(UIButton *)button
{
    [button setImage:
     [self tintImage:[button imageForState:UIControlStateNormal]
               color:[ZMProntoPhase3 brightColor]] forState:UIControlStateNormal];
}


-(void) tintSeparator:(UIButton *)button
{
    [button setImage:
     [self tintImage:[button imageForState:UIControlStateNormal]
               color:[ZMProntoPhase3 separatorColor]] forState:UIControlStateNormal];
}


-(void) tintProntoLogoCard:(UIImageView *)imageView
{
    [imageView setImage:[self tintImage:imageView.image color:[ZMProntoPhase3 brightColor]]];
}

-(void) tintProntoLogoCardDark:(UIImageView *)imageView
{
    [imageView setImage:[self tintImage:imageView.image color:[ZMProntoPhase3 darkColor]]];
}

-(void) tintProntoLogoCardGold:(UIImageView *)imageView
{
    [imageView setImage:[self tintImage:imageView.image color:[ZMProntoPhase3 goldColor]]];
}


-(void) tintCloseCard:(UIImageView *)imageView
{
    [imageView setImage:[self tintImage:imageView.image color:[ZMProntoPhase3 applicationColor]]];
}


-(void) tintLinkIcon:(UIImageView *)imageView
{
    //Revamp - Change clour to blue
    [imageView setImage:[self tintImage:imageView.image color:[ZMProntoPhase3 blueColor]]];
    //End of Revamp
}


-(void) tintLinkIconVisited:(UIImageView *)imageView
{
    [imageView setImage:[self tintImage:imageView.image color:[ZMProntoPhase3 viewedColor]]];
}


-(void) readUnreadIcon:(UIView *)view
{
    view.backgroundColor = [ZMProntoPhase3 successColor];
    view.layer.cornerRadius = 5;
    view.layer.masksToBounds = YES;
}



-(void) searchBoxContainer:(UIView *)view
{
    //Revamp - Clear color for search
    view.backgroundColor = [UIColor clearColor];//[ZMProntoPhase3 serarchBoxColor];
    //End of Revamp
    view.layer.cornerRadius = 5;
    view.layer.masksToBounds = YES;
}






# pragma mark - Global


-(UIImage *) tintImage:(UIImage *)image color:(UIColor*)color
{
    return [image imageWithColor:color];
}


-(UIButton *) tintButtonWithColor:(UIButton *)button color:(UIColor*)color
                            state:(UIControlState)state
{
    UIImage *stateNormalBackground = [UIImage imageNamed:@"primaryButton"];
    stateNormalBackground = [[self tintImage:stateNormalBackground color:color] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)];
    [button setBackgroundImage:stateNormalBackground
                      forState:state];
    return button;
}



-(void) stylePrimaryButton:(UIButton *)button
{
    [self tintButtonWithColor:button color:[ZMProntoPhase3 brightColor] state:UIControlStateNormal];
    [button setTitleColor:[ZMProntoPhase3 brightColor] forState:UIControlStateNormal];
}


-(void) styleSecondaryButton:(UIButton *)button
{
    [self tintButtonWithColor:button color:[ZMProntoPhase3 applicationColor] state:UIControlStateNormal];
    [button setTitleColor:[ZMProntoPhase3 applicationColor] forState:UIControlStateNormal];
}


-(void) styleBadge:(ZMBadge *)badge franchise:(NSString*) franchiseName
{
    //Revamp(12/12) - Changing badge color for franchise
    if([franchiseName  isEqual: @"In House"] || [franchiseName  isEqual: @"Home Office"]) {
        badge.backgroundColor = [ZMProntoPhase3 alertColor];
        badge.label.textColor = [ZMProntoPhase3 brightColor];
    } else {
        badge.backgroundColor = [ZMProntoPhase3 alertColorGreen];
        badge.label.textColor = [ZMProntoPhase3 darkColor];
    }
    //End of Revamp
}

-(void) tintPrimaryButton:(UIButton *)button
{
    [button setImage:
     [self tintImage:[button imageForState:UIControlStateNormal]
               color:[ZMProntoPhase3 brightColor]] forState:UIControlStateNormal];
}


-(void) tintButton:(UIButton *)button
{
    [button setImage:
     [self tintImage:[button imageForState:UIControlStateNormal]
               color:[ZMProntoPhase3 applicationColor]] forState:UIControlStateNormal];
}

-(void) toggleButton:(UIButton *)button
{
    [self tintButton:button];
    [button setTitleColor:[ZMProntoPhase3 applicationColor] forState:UIControlStateNormal];
}


-(void) toggleButtonSelected:(UIButton *)button
{
    [button setImage:
     [self tintImage:[button imageForState:UIControlStateNormal]
               color:[ZMProntoPhase3 brightColor]] forState:UIControlStateNormal];
    [button setTitleColor:[ZMProntoPhase3 brightColor] forState:UIControlStateNormal];
}


# pragma mark - Abbvie Main view

-(void) styleMainView:(UIView *)view
{
    [view setBackgroundColor:[ZMProntoPhase3 applicationBackground]];
}


# pragma mark - Header
//Revamp(7/12)
//Get set the final colour
-(NSString *) getColorForFranchise:(NSString*) franchiseName {
    for (NSDictionary* set in _colorSet) {
        if ([franchiseName  isEqual:[set objectForKey:@"name"]]) {
            return [set objectForKey:@"color"] ;
        }
    }
    return @"#FFFFFF";
}

//Saving colour list into array
-(void) addIntoColorSet {
    _colorSet = [[NSMutableArray alloc] init];
    [_colorSet addObject:@{@"name":@"GI Care", @"color":@"#7B68EE"}];
    [_colorSet addObject:@{@"name":@"Immunology - Patient Outreach", @"color":@"#94014C"}];
    [_colorSet addObject:@{@"name":@"GMA Hepatology", @"color":@"#2E8B57"}];
}
//End of Revamp

-(void) styleHeader:(UIView *)headerView
{
    headerView.backgroundColor = [ZMProntoPhase3 applicationBackground];
}

//Revamp - Function calling for header color
-(void) styleFGUser:(UIView *)headerView franchise:(NSString*) franchiseName colorCode:(NSString*) franchiseColor
{
//    if(_colorSet.count == 0){
//        [self addIntoColorSet];
//    }
//    NSString * colourCode;
//    //get colour code
//    if(![franchiseName  isEqual: @""]){
//    colourCode = [self getColorForFranchise:franchiseName];
//    }
 
         //Pass colour code
        headerView.backgroundColor = [ZMProntoPhase3 fieldUserBG:franchiseColor];
   
}

-(void) labelColorHO:(UILabel *)label
{
    label.textColor = [ZMProntoPhase3 homeOfficeBG];
}

-(void) labelColorFU:(UILabel *)label colorCode:(NSString*) franchiseColor
{
    label.textColor = [ZMProntoPhase3 fieldUserBG:franchiseColor];
}

-(void) styleHOUser:(UIView *)headerView
{
    headerView.backgroundColor = [ZMProntoPhase3 homeOfficeBG];
}
//End of Revamp

-(void) styleHeaderButton:(UIView *)headerButton
{
    headerButton.backgroundColor = [UIColor grayColor];
}

-(void) clear:(UIView *)view
{
    view.backgroundColor = [UIColor clearColor];
}


-(void) stylePrimaryView:(UIView *)view
{
    view.backgroundColor = [ZMProntoPhase3 applicationColor];
}

-(void) styleHeaderSecond:(UIView *)headerView
{
    headerView.backgroundColor = [ZMProntoPhase3 thirdBackground];
}


-(void) styleHeaderThird:(UIView *)headerView
{
    headerView.backgroundColor = [ZMProntoPhase3 secondaryBackground];
}


- (void) successButton:(UIButton *)button;
{
    [button setBackgroundColor:[UIColor greenColor]];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
}

- (void) warningButton:(UIButton *)button
{
    [button setBackgroundColor:[UIColor yellowColor]];
}

- (void) blackButton:(UIButton *)button
{
    [button setTitleColor:[ZMProntoPhase3 darkColor] forState:UIControlStateNormal];
}

- (void) whiteButton:(UIButton *)button
{
    [button setTitleColor:[ZMProntoPhase3 brightColor] forState:UIControlStateNormal];
}

- (void) goldButton:(UIButton *)button
{
    [button setTitleColor:[ZMProntoPhase3 goldColor] forState:UIControlStateNormal];
}

-(NSDictionary *) buttonTitleMainStyle
{
    return [NSDictionary dictionaryWithObjectsAndKeys: [UIFont boldSystemFontOfSize:15],
            NSFontAttributeName, [ZMProntoPhase3 applicationColor], NSForegroundColorAttributeName, nil];
}


-(NSDictionary *) breadCumbTotalStyle
{
    return [NSDictionary dictionaryWithObjectsAndKeys: [UIFont boldSystemFontOfSize:15],
            NSFontAttributeName, [ZMProntoPhase3 brightColor], NSForegroundColorAttributeName, nil];
}


-(NSDictionary *) breadCumbBrandStyle
{
    return [NSDictionary dictionaryWithObjectsAndKeys: [UIFont systemFontOfSize:15],
            NSFontAttributeName, [ZMProntoPhase3 brightColor], NSForegroundColorAttributeName, nil];
}


-(NSDictionary *) breadCumbPipeStyle
{
    return [NSDictionary dictionaryWithObjectsAndKeys: [UIFont systemFontOfSize:15],
            NSFontAttributeName, [ZMProntoPhase3 pipeColor], NSForegroundColorAttributeName, nil];
}

// Pronto new revamp changes: button title style of sortings
-(NSDictionary *) sortButtonTitleStyle
{
    UIColor *fontColor = [UIColor colorWithRed: 171.0/255.0 green: 169.0/255.0 blue: 168.0/255.0 alpha:1];
    UIFont *font = [UIFont fontWithName:@"Lato-Light" size:14.0];
    return [NSDictionary dictionaryWithObjectsAndKeys: font,
            NSFontAttributeName, fontColor, NSForegroundColorAttributeName, nil];
}
// End:

-(NSDictionary *) buttonTitleStyle
{
    return [NSDictionary dictionaryWithObjectsAndKeys: [UIFont boldSystemFontOfSize:15],
            NSFontAttributeName, [ZMProntoPhase3 applicationBackground], NSForegroundColorAttributeName, nil];
}


-(NSDictionary *) breadCumbGeneralStyle
{
    return [NSDictionary dictionaryWithObjectsAndKeys: [UIFont systemFontOfSize:15],
            NSFontAttributeName, [ZMProntoPhase3 applicationColor], NSForegroundColorAttributeName, nil];
}

// New Revamping - Checked Image Colour
-(void) tintArrow:(UIImageView *) imageView
{
    imageView.image = [self tintImage:imageView.image color:[ZMProntoPhase3 checkBoxColor]];
}

//Revamp(12/12) - Arrow for Notifications
-(void) notificationsArrow:(UIImageView *) imageView
{
    imageView.image = [self tintImage:imageView.image color:[ZMProntoPhase3 notificationsArrow]];
}

// New Revamping - Logo Image Colour - White
-(void) logoImage:(UIImageView *) imageView
{
    imageView.image = [self tintImage:imageView.image color:[ZMProntoPhase3 brightColor]];
}

-(void) logoImageHO:(UIImageView *) imageView
{
    imageView.image = [self tintImage:[UIImage imageNamed:@"hoLogo"] color:[ZMProntoPhase3 brightColor]];
}
-(void) logoImageFG:(UIImageView *) imageView
{
       imageView.image = [self tintImage:[UIImage imageNamed:@"fgLogo"] color:[ZMProntoPhase3 brightColor]];
}

-(void) tintArrowDark:(UIImageView *) imageView
{
    imageView.image = [self tintImage:imageView.image color:[ZMProntoPhase3 applicationBackground]];
}

-(void) styBrightLabel:(UILabel *)label
{
    label.textColor = [ZMProntoPhase3 brightColor];
}

//Revamp - Colour Adding
-(void) styDarkLabel:(UILabel *)label
{
    label.textColor = [ZMProntoPhase3 darkColor];
}

-(void) styGrayLabel:(UILabel *)label
{
    label.textColor = [ZMProntoPhase3 greyColor];
}
//End of Reavamp

-(void) brightLabelDisable:(UILabel *)label
{
    label.textColor = [ZMProntoPhase3 disableLabelColor];
}


-(void) styReadLabel:(UILabel *)label
{
    label.textColor = [ZMProntoPhase3 viewedColor];
}

//Revamp - Dark grey
-(void) styReadLabelDark:(UILabel *)label
{
    label.textColor = [ZMProntoPhase3 viewedColorDark];
}
//End of Revamp

-(void) styleDivider:(UIView *)view
{
    //Revamp - Colour change for divider
    view.backgroundColor = [ZMProntoPhase3 blueForNotification];
    //End of Revamp
}


-(void) styleTransparentPannel:(UIView *)view
{
    view.backgroundColor = [ZMProntoPhase3 darkColorWhitTransparency];
    [self addShadowToView:view];
}

//Revamp - Notification blue
-(void) styleBlueNotificationPannel:(UIView *)view
{
    view.backgroundColor = [ZMProntoPhase3 blueForNotification];
}
//End of Revamp


-(void) styleTransparentPannelFilters:(UIView *)view
{
    view.backgroundColor = [ZMProntoPhase3 darkColorWhitTransparency];
}


-(void) styleTransparentBrandPannelFilters:(UIView *)view
{
    view.backgroundColor = [ZMProntoPhase3 almostDarkColor];
}



-(void) brightRefresh:(UIRefreshControl *)refresh
{
    refresh.tintColor = [ZMProntoPhase3 brightColor];
}


-(void) styleWarningMessage:(UIView *)view label:(UILabel*)label
{
    view.backgroundColor = [ZMProntoPhase3 warningColour];
    label.textColor = [ZMProntoPhase3 darkColor];
}


-(void) styleAlertMessage:(UIView *)view label:(UILabel*)label
{
    view.backgroundColor = [ZMProntoPhase3 alertMessageColor];
    label.textColor = [ZMProntoPhase3 brightColor];
}


-(void) styleSuccessMessage:(UIView *)view label:(UILabel*)label
{
    view.backgroundColor = [ZMProntoPhase3 successColor];
    label.textColor = [ZMProntoPhase3 brightColor];
    [self addShadowToView:view];
}

-(void) styleReloadMessage:(UIView *)view label:(UILabel*)label
{
    view.backgroundColor = [ZMProntoPhase3 warningColour];
    label.textColor = [ZMProntoPhase3 darkColor];
}

-(void) styleSyncMessage:(UIView *)view label:(UILabel*)label
{
    view.backgroundColor = [ZMProntoPhase3 syncColor];
    label.textColor = [ZMProntoPhase3 goldColor];
    [self addShadowToView:view];
}

-(void) styleActivityIndicatorColorWhite:(UIActivityIndicatorView *)view
{
    view.color = [ZMProntoPhase3 brightColor];
}

-(void) styleActivityIndicatorColor:(UIActivityIndicatorView *)view
{
    view.color = [ZMProntoPhase3 goldColor];
}

-(void) tintButtonReset:(UIButton *)button
{
    [button setTitleColor:[ZMProntoPhase3 applicationColor] forState:UIControlStateNormal];
}

// Font White Colour
- (UIColor *) whiteFontColor
{
    return [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
}

// Normal Lato Font
- (UIFont*) normalLato
{
    return [UIFont fontWithName:@"Lato-Regular" size:14.0];
}
// Bold Lato Font
- (UIFont*) boldLato
{
    return [UIFont fontWithName:@"Lato-Bold" size:14.0];
}
// Small Lato Font
- (UIFont*) smallLato
{
    return [UIFont fontWithName:@"Lato-Regular" size:12.0];
}
// Big bold Lato Font
- (UIFont*) bigBoldLato
{
    return [UIFont fontWithName:@"Lato-Bold" size:17.0];
}
// Big Regular Lato Font
- (UIFont*) bigNormalLato
{
    return [UIFont fontWithName:@"Lato-Regular" size:17.0];
}
// Franchise count color
- (UIColor *) franchiseCountColor
{
    return [UIColor colorWithRed:0.18 green:0.16 blue:0.15 alpha:1.0];
}

- (UIFont*) bigBoldLatoWithFontSize16
{
    return [UIFont fontWithName:@"Lato-Bold" size:16.0];
}

- (UIFont*) bigBoldLatoWithFontSize17
{
    return [UIFont fontWithName:@"Lato-Bold" size:17.0];
}

@end
