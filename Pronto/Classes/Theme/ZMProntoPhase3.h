//
//  ZMProntoPhase3.h
//  Pronto
//
//  Created by Sebastian Romero on 11/28/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMProntoTheme.h"

@interface ZMProntoPhase3 : NSObject <ZMProntoTheme>

//Revamp(7/12) - Object to save dictionary of colours
@property (strong, nonatomic) NSMutableArray *colorSet;
//End of Revamp

+ (UIColor *)popOverSelectedItemBlueColor;
+ (UIColor *)homeOfficeBG;

@end
