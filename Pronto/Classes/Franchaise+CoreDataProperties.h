//
//  Franchaise+CoreDataProperties.h
//  Pronto
//
//  Created by Oscar Robayo on 10/19/15.
//  Copyright © 2015 Zemoga. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Franchaise.h"

NS_ASSUME_NONNULL_BEGIN

@interface Franchaise (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *field_active;
@property (nullable, nonatomic, retain) NSNumber *field_private;
@property (nullable, nonatomic, retain) NSString *franchaiseID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *tid;
@property (nullable, nonatomic, retain) NSNumber *vid;
@property (nullable, nonatomic, retain) NSSet<Asset *> *assets;
@property (nullable, nonatomic, retain) NSSet<Brand *> *brands;

@end

@interface Franchaise (CoreDataGeneratedAccessors)

- (void)addAssetsObject:(Asset *)value;
- (void)removeAssetsObject:(Asset *)value;
- (void)addAssets:(NSSet<Asset *> *)values;
- (void)removeAssets:(NSSet<Asset *> *)values;

- (void)addBrandsObject:(Brand *)value;
- (void)removeBrandsObject:(Brand *)value;
- (void)addBrands:(NSSet<Brand *> *)values;
- (void)removeBrands:(NSSet<Brand *> *)values;

@end

NS_ASSUME_NONNULL_END
