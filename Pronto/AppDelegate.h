//
//  AppDelegate
//  Pronto
//
//  Created by Sebastian Romero on 11/27/13.
//  Copyright (c) 2013 Abbvie Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMMessage.h"
#import "ZMAbbvieAPI.h"
#import "ZMBriefcaseMenu.h"
#import <UserNotifications/UserNotifications.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define StageEnvironment @"stage"
#define PreProdEnvironment @"preprod"
#define ProductionEnvironment @"production"

@interface AppDelegate : UIResponder <UIApplicationDelegate, ZMProntoMessagesDelegate,UNUserNotificationCenterDelegate> {

    ZMAbbvieAPI *_apiManager;
    NSURL *_assetToOpenURL;
    NSInteger currentIndex;
}

/**
 *  Enumerates the message type
 */
enum ZMProntoNotificationAlert
{
    ZMProntoNotificationAlertTypeURL = 1,
    ZMProntoNotificationAlertTypeAsset = 2,
    ZMProntoNotificationAlertTypeApp = 3,
    ZMProntoNotificationAlertTypeAssetNotFound = 4
};

@property (nonatomic) BOOL userHasChanged;
@property (nonatomic) BOOL openedDeeplinkAsset;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong,nonatomic) ZMMessage *currentMessage;
@property (nonatomic)BOOL featureSwitchkey;
@property (strong, nonatomic) UIView *topview;
@property (strong, nonatomic) UILabel* lblView;
@property (strong, nonatomic) UIButton *yesButton;
@property (strong, nonatomic) UIButton *closeButton;
/**
 *  Shows a messages with a given type
 *  @param message The message to display
 *  @param type the type of the message
 */
-(void) showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type;

/**
 *  Shows a messages with a given type and hides after delay
 *  @param message The message to display
 *  @param type the type of the message
 *  @param time to close the message
 */
-(void) showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withTime:(NSTimeInterval)time;

/**
 *  Handles the notification action
 *  @param action action to perform
 *  @param type notification type
 */
-(void) handlerNotificationAction:(id)action withType:(enum ZMProntoNotificationAlert)type;

/**
 *  Shows a messages with a given type
 *  @param message The message to display
 *  @param type the type of the message
 *  @param action the action for the retry button
 */
-(void) showMessage:(NSString *)message whithType:(enum ZMProntoMessages)type withAction:(void(^)(void))mAction;


/**
 *  Hides the message
 */
-(void) hideMessage;

/**
 * Initialize session with the server if the session is broken
 **/
-(void) initPronto;
-(void) redirectToLibrary;
/*!
 *Checks for the current type of network connection
 */
- (NSString *)checkNetwork;

- (BOOL)hasConnectivity;

- (void)validateUserStatus;
- (NSString *)getPathResource:(NSString*)root;

- (void)reSyncLibrary;
- (void)resetLibraryView;
// Pronto - revamp changes - Overlay while loading assets
- (void) showHideOverlay:(BOOL)hide;
- (void) showHideOverlayForTabBar:(BOOL)hide;
-(void) adjustTabBarOverlay;
// Pronto - revamp changes - to fetch the tabbed index
-(NSInteger)  currentTabIndex :(NSInteger) tabIndex;
/**
  * function is meant for doing set up for orientation changes related to App delegate class
 **/
- (void)didOrientationChange;
- (BOOL)isAppAlreadyLaunchedOnce;
- (void)removeTopviews;
- (void)registerDeeplinkNotification;
- (void)openAssetDetailForDeeplinkAsset:(Asset*)asset;
@end
