//
//  ProntoTabbarViewController.m
//  Pronto
//
//  Created by CCC on 28/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import "ProntoTabbarViewController.h"
#import "ZMLibrary.h"

const CGFloat tabBarItemWidth = 104;
const CGFloat tabBarItemHeight = 54;
const CGFloat numberOfTabBarItems = 4;


@interface ProntoTabbarViewController () {

__weak UIViewController* currentController;
}

@property (weak, nonatomic) IBOutlet UIView *tabBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *franchiseTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *competitorTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *toolsTrainingTabbarItem;
@property (weak, nonatomic) IBOutlet UIButton *eventsTabbarItem;
@end

@implementation ProntoTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setController:@"ZMLibrary"];
    [ZMUserActions sharedInstance].shouldShowFranchiseListView = YES;
    [self setTabBarItemFrame];

}

-(void)viewDidLayoutSubviews {
    [self hideContentController: currentController];
    [self displayContentController: currentController];
    [self setTabBarItemFrame];
}

- (void)setController:(NSString*)nameOfController {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    currentController = [sb instantiateViewControllerWithIdentifier:nameOfController];
    [self displayContentController: currentController];
}

- (IBAction)tabBarItemTapped:(UIButton*)sender {
    
    switch (sender.tag) {
        case 0: {
            [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseSelect"] forState: UIControlStateNormal];
            [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compUnselect"] forState: UIControlStateNormal];
            [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsUnselect"] forState: UIControlStateNormal];
            [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventUnselect"] forState: UIControlStateNormal];
            _franchiseTabbarItem.backgroundColor = [UIColor whiteColor];
            _competitorTabbarItem.backgroundColor = [UIColor clearColor];
            _toolsTrainingTabbarItem.backgroundColor = [UIColor clearColor];
            _eventsTabbarItem.backgroundColor = [UIColor clearColor];
            [self hideContentController: currentController];
            [self setController:@"ZMLibrary"];
        }
            break;
        case 1: {
            [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseUnselect"] forState: UIControlStateNormal];
            [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compSelect"] forState: UIControlStateNormal];
            [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsUnselect"] forState: UIControlStateNormal];
            [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventUnselect"] forState: UIControlStateNormal];
            _franchiseTabbarItem.backgroundColor = [UIColor clearColor];
            _competitorTabbarItem.backgroundColor = [UIColor whiteColor];
            _toolsTrainingTabbarItem.backgroundColor = [UIColor clearColor];
            _eventsTabbarItem.backgroundColor = [UIColor clearColor];
            [self hideContentController: currentController];
            [self setController:@"Competitors"];
        }
            break;
        case 2:
        {
            [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseUnselect"] forState: UIControlStateNormal];
            [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compUnselect"] forState: UIControlStateNormal];
            [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsSelect"] forState: UIControlStateNormal];
            [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventUnselect"] forState: UIControlStateNormal];
            _franchiseTabbarItem.backgroundColor = [UIColor clearColor];
            _competitorTabbarItem.backgroundColor = [UIColor clearColor];
            _toolsTrainingTabbarItem.backgroundColor = [UIColor whiteColor];
            _eventsTabbarItem.backgroundColor = [UIColor clearColor];
            [self hideContentController: currentController];
            [self setController:@"ToolsTraining"];
        }
            break;
        case 3:
        {
            [_franchiseTabbarItem setBackgroundImage:[UIImage imageNamed:@"franchiseUnselect"] forState: UIControlStateNormal];
            [_competitorTabbarItem setBackgroundImage:[UIImage imageNamed:@"compUnselect"] forState: UIControlStateNormal];
            [_toolsTrainingTabbarItem setBackgroundImage:[UIImage imageNamed:@"toolsUnselect"] forState: UIControlStateNormal];
            [_eventsTabbarItem setBackgroundImage:[UIImage imageNamed:@"eventSelect"] forState: UIControlStateNormal];
            _franchiseTabbarItem.backgroundColor = [UIColor clearColor];
            _competitorTabbarItem.backgroundColor = [UIColor clearColor];
            _toolsTrainingTabbarItem.backgroundColor = [UIColor clearColor];
            _eventsTabbarItem.backgroundColor = [UIColor whiteColor];
            [self hideContentController: currentController];
            [self setController:@"Events"];
        }
            break;
        default:
            break;
    }
}

- (void) displayContentController: (UIViewController*) content {
    [self addChildViewController:content];
    content.view.frame = CGRectMake(0, _contentView.frame.origin.y, _contentView.frame.size.width, self.view.frame.size.height - ( 54) );
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
}

- (void) hideContentController: (UIViewController*) content {
    [content willMoveToParentViewController:nil];
    [content.view removeFromSuperview];
    [content removeFromParentViewController];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
}
    
- (void) setTabBarItemFrame {
    CGFloat xOrigin =  ([UIScreen mainScreen].bounds.size.width - (tabBarItemWidth * numberOfTabBarItems)) / 2;
    self.franchiseTabbarItem.frame = CGRectMake(xOrigin, 0, tabBarItemWidth, tabBarItemHeight);
    self.competitorTabbarItem.frame = CGRectMake(xOrigin + tabBarItemWidth, 0, tabBarItemWidth, tabBarItemHeight);
    self.toolsTrainingTabbarItem.frame = CGRectMake(xOrigin + (tabBarItemWidth * 2), 0, tabBarItemWidth, tabBarItemHeight);
    self.eventsTabbarItem.frame = CGRectMake(xOrigin + (tabBarItemWidth * 3), 0, tabBarItemWidth, tabBarItemHeight);
}

- (void)cycleFromViewController: (UIViewController*) oldVC
               toViewController: (UIViewController*) newVC {
    // Prepare the two view controllers for the change.
    [oldVC willMoveToParentViewController:nil];
    [self addChildViewController:newVC];
    
    // Get the start frame of the new view controller and the end frame
    // for the old view controller. Both rectangles are offscreen.
    //    newVC.view.frame = [self newViewStartFrame];
    //    CGRect endFrame = [self oldViewEndFrame];
    
    // Queue up the transition animation.
    [self transitionFromViewController: oldVC toViewController: newVC
                              duration: 0.25 options:0
                            animations:^{
                                // Animate the views to their final positions.
                                //                                newVC.view.frame = oldVC.view.frame;
                                //                                oldVC.view.frame = endFrame;
                            }
                            completion:^(BOOL finished) {
                                // Remove the old view controller and send the final
                                // notification to the new view controller.
                                [oldVC removeFromParentViewController];
                                [newVC didMoveToParentViewController:self];
                            }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateTabSelection:(int)index
{
    self.tabBarController.tabBar.selectedItem.tag = index;
    NSLog(@"Tab index = %d", index);
    
    AppDelegate* mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [mainDelegate currentTabIndex:index];
    // [self tabBar:self.tabBarController.tabBar didSelectItem:self.tabBarController.tabBar.selectedItem];
    // NSLog(@"all title:%@",self.tabBarController.tabBar.selectedItem.title);
    
    switch (index) {
        case 0:
        {
            
            NSLog(@"Landing screen");
            [ZMUserActions sharedInstance].isLoginSuccess = YES;
            
        }
            break;
        case 1:
        {
            NSLog(@"Comp screen");
            
        }
            break;
        case 2:
        {
            NSLog(@"Tools screen");
        }
            break;
        case 3:
        {
            NSLog(@"Events screen");
        }
            break;
        default:
            break;
    }
}


@end
