//
//  ASFSession+Private.h
//  ASFKit
//
//  Copyright (c) 2014 AbbVie Inc. North Chicago, Illinois, U.S.A. All rights reserved.
//

//#import <ASFKit/ASFKit.h>
@interface ASFSession ()
/// This is a "private" property on ASFSession. When set to YES, ASFURLProtocol does not check that requests are made over https.
/// This is here just in case built-in frameworks make insecure network requests and the secure checking needs to be disabled (all requests to *.apple.com are already allowed).
/// @since 2.1
@property (nonatomic, readwrite) BOOL shouldAllowInsecureNetworkRequests;

@end
