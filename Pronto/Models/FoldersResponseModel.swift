//
//  FoldersResponseModel.swift
//  Pronto
//
//  Created by Ravi Subramanian on 09/03/22.
//  Copyright © 2022 Abbvie. All rights reserved.
//

import Foundation
//class FolderResponseModel: NSObject, Mappable, Decodable {
//  var assetCount: Int?
//  var name: String?
//  var categoryId: Int?
//  var assets: [Asset]?
//
//  required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//      assetCount <- map["asset_count"]
//      name <- map["name"]
//      categoryId <- map["categoryId"]
//      assets <- map["assets"]
//    }
//}

struct FolderResponseModel: Decodable {
  var assetCount: Int?
   var name: String?
   var categoryId: Int?
   var assets: [Asset]?

        init() {}

        enum CodingKeys: String, CodingKey {
            case assetCount
            case name
            case retailerMode
            case categoryId
            case assets
        }

        init(from decoder: Decoder) {
            do {
                let values = try decoder.container(keyedBy: CodingKeys.self)
              assetCount = (try? values.decode(Int.self, forKey: .assetCount)) ?? 0
              name = (try? values.decode(String.self, forKey: .name)) ?? ""
              categoryId = (try? values.decode(Int.self, forKey: .categoryId)) ?? 0
//              assets = (try? values.decode(Asset.self, forKey: .assets)) ?? Asset()
            } catch {}
        }
}
