//
//  ProntoTabbarViewController.h
//  Pronto
//
//  Created by CCC on 28/12/17.
//  Copyright © 2017 Abbvie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProntoTabbarViewController.h"
#import "ZMProntoViewController.h"

@interface ProntoTabbarViewController : UIViewController
{
    
}

-(void) updateTabSelection:(int)index;

@end
